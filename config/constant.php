<?php

return array(
  'created' => 'Created',
  'updated' => 'Updated',
  'deleted' => 'Deleted',
  'renamed' => 'Renamed',
  'migrated'=> 'Migrated',
  'admin'=>'Admin',
  'dealer'=> 'Dealer',
  'user'=> 'User',
  'group'=>'Group',
  'organization'=> 'Organization',
  'vehicle'=> 'Vehicle',
  'autocalibrate'=> 'AutoCalibrate',
  'manualcalibrate'=>'ManualCalibrate',
  'vehicleid_migrated' => 'Vehicleid Migrated',
  'deviceid_migrated' => 'Deviceid Migrated',
  'auto_calibration'=>'Auto Calibration',
  'manual_calibration'=>'Manual Calibration',
  'disabled'=>'Disabled',
  'enabled'=>'Enabled',
  'reportsEdited'=> 'Reports Edited',
  'notificationEdited'=> 'Notification Edited',
  'editAlerts' => 'Edit Alerts',
  'licenceALC' => 'Licences Allocation',
  'shuffle' => 'Shuffled Licences',
  'new_vehicle_added' => 'New Vehicle Added',
  'VAMOSdb' => 'VAMOSYS',
  'endpoint' => 'https://sgp1.digitaloceanspaces.com',
  'bucket'  => 'sittesting'

);