<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use Exception;
use Illuminate\Support\Facades\Route;

/* 
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// Auth::routes();

Route::post('/changeLang', function () {
    session()->put('phplang', request()->get('lang'));
    app()->setLocale(request()->get('lang'));
    return redirect()->back()->withInput();
})->name('changeLang');

// Route::get('/', [LoginController::class, 'showLoginForm']);
// Route::get('/login', [LoginController::class, 'showLoginForm']);
Route::get('/', [HomeController::class, 'showLogin']);
Route::get('/login', [HomeController::class, 'showLogin']);
Route::post('/dologin', [HomeController::class, 'dologin'])->name('dologin');
Route::post('login/google', [LoginController::class, 'googleLogin'])->name('googleLogin');
Route::get('/register', [RegisterController::class, 'showRegister'])->name('regitser');
Route::get('/allreg', [RegisterController::class, 'allreg']);
Route::post('/register', [RegisterController::class, 'register'])->name('doregister');


Route::get('HomeController/getVehicle', [HomeController::class, 'getVehicle']);

Route::post('userIds', [HomeController::class, 'getApi'])->name('ajax.apiKeyAcess');
Route::post('userIdsss', [HomeController::class, 'getDealer'])->name('ajax.dealerAcess');
Route::post('userIdss', [HomeController::class, 'getFcode'])->name('ajax.fcKeyAcess');
Route::post('userIP', [HomeController::class, 'getUserIP'])->name('ajax.userIP');

Route::get('loginpass/{param}/{param1}/{param2}', [HomeController::class, 'byPassUsers']);
Route::get('switchLogin/{param}', [HomeController::class, 'switchLogin']);
Route::post('doLoginTry', [HomeController::class, 'doLogin1'])->name('ajax.doLoginTry');
// Route::post('login', array('before' => 'csrf', 'uses' => 'HomeController::class,doLogin]);
Route::get('login1', [HomeController::class, 'doLogin']);

Route::post('mobleverify', [HomeController::class, 'mobileVerify'])->name('ajax.mobleverisy');
Route::post('cancel', [HomeController::class, 'otpcancel'])->name('ajax.cancel');
Route::post('resendbtn', [HomeController::class, 'resendftn'])->name('ajax.resent');
Route::post('otpverify', [HomeController::class, 'otpverify'])->name('ajax.otpverify');

Route::post('getApiKeys', [HomeController::class, 'getApi']);
Route::post('fileUpload', [HomeController::class, 'fileUpload']);
Route::post('getFiles', [HomeController::class, 'getFileNames']);
Route::get('aUthName', [HomeController::class, 'authName']);
Route::get('dealerName', [HomeController::class, 'getDealerName']);
Route::get('logout', [HomeController::class, 'doLogout'])->name('logout');

Route::get('honda9964', [HomeController::class, 'admin']);
Route::get('adhocMail', [HomeController::class, 'adhocMail']);

Route::post('sendAdhocMail', ['before' => 'csrf', HomeController::class, 'sendAdhocMail']);

Route::get('ipAddressManager', [HomeController::class, 'ipAddressManager']);

Route::get('reverseGeoLocation', [HomeController::class, 'reverseGeoLocation']);

Route::get('nearByVehicles', [HomeController::class, 'nearByVehicles']);


Route::middleware("adminauth")->group(function () {

    Route::get('/lang/{lang}', function($lang){
        app()->setLocale($lang);
        return redirect()->back();
    });
    Route::get('home', [HomeController::class, 'index'])->name('home');

    Route::get('dashboard', [DashBoardController::class, 'index'])->name('dashboard');

    // BusinessController 
    Route::get('onBoardDevice', [BusinessController::class, 'onBoardDevice'])->name('onBoardDevice');
    Route::post('Business/adddevice', [BusinessController::class, 'adddevice'])->name('Business/adddevice');
    Route::resource('Business', BusinessController::class);
    Route::post('select', [BusinessController::class, 'checkvehicle'])->name('ajax.checkvehicle');
    Route::post('select1', [BusinessController::class, 'checkDevice'])->name('ajax.checkDevice');
    Route::post('select3', [BusinessController::class, 'checkUser'])->name('ajax.checkUser');
    Route::post('select2', [BusinessController::class, 'getGroup'])->name('ajax.getGroup');
    Route::post('Business/batchSale', [BusinessController::class, 'batchSale'])->name('Business/batchSale');
    // Route::post('Business/adddevice', BusinessController::class,'adddevice');

    //billing
    Route::resource('Billing', VdmBillingController::class);
    Route::get('billing/prerenewal', [VdmBillingController::class, 'preRenewal'])->name('billing/prerenewal');
    Route::get('billing/preRenewalList', [VdmBillingController::class, 'preRenewalList'])->name('billing/preRenewalList');
    Route::get('billing/expired', [VdmBillingController::class, 'expiredList'])->name('billing/expired');
    Route::get('billing/convert', [VdmBillingController::class, 'licConvert'])->name('billing/convert');
    Route::post('convert/update', [VdmBillingController::class, 'licUpdate']);

    Route::get('billing/Renewal/{param}', [VdmBillingController::class, 'renewal']);
    Route::get('billing/Cancel/{param}', [VdmBillingController::class, 'LicenceCancel']);
    Route::post('licence/search', [VdmBillingController::class, 'licSearch']);

    // ExcelController
    Route::resource('Excel', ExcelController::class);
    Route::post('import/Excel', [ExcelController::class, 'importExcel'])->name('import/Excel');
    Route::post('downloadExcel/xls', [ExcelController::class, 'downloadExcel'])->name('downloadExcel/xls');
    Route::post('import/DealerExcel', [ExcelController::class, 'importDealerExcel'])->name('import/DealerExcel');
    Route::post('downloadDealerExcel/xls', [ExcelController::class, 'downloadDealerExcel'])->name('downloadDealerExcel/xls');

    // RemoveController
    Route::resource('Remove', RemoveController::class);
    Route::post('Remove/removedevices', [RemoveController::class, 'removedevice'])->name('Remove/removedevices');
    Route::post('select4', [RemoveController::class, 'checkvehicle'])->name('ajax.checkvehicle1');
    Route::post('select5', [RemoveController::class, 'checkDevice'])->name('ajax.checkDevice1');

    // onboard devices and VdmVehicleViewControllre
    Route::resource('vdmVehiclesView', VdmVehicleViewController::class);
    Route::get('vdmVehicles/move_vehicle/{param}', [VdmVehicleViewController::class, 'move_vehicle']);
    Route::post('vdmVehicles/moveVehicle', [VdmVehicleViewController::class, 'moveVehicleUpdate']);
    Route::get('vehicleStatus', [VdmVehicleViewController::class, 'vehicleStatus'])->name('vehicleStatus');
    Route::get('vehicleStatusXls', [VdmVehicleViewController::class, 'vehicleStatusXls'])->name('vehicleStatusXls');
    // Calibrate Excel
    Route::post('export/calibrate', [VdmVehicleViewController::class, 'exportExcel'])->name('export/calibrate');
    Route::post('import/calibrate', [VdmVehicleViewController::class, 'uploadCalibrate'])->name('import/calibrate');
    Route::post('export/autocalibrate', [VdmVehicleViewController::class,'downloadAutoCalibrate'])->name('downloadAutoCalibrate');


    ///onboard search
    Route::resource('DeviceScan', DeviceControllerScan::class);
    Route::get('deviceMove{param}', [DeviceControllerScan::class, 'movedVehicle']);
    Route::get('DeviceScan/sendExcel', [DeviceControllerScan::class, 'sendExcel']);

    Route::get('vdmVehicles/changeExpireDate', [VdmVehicleController::class, 'changeExpireDate']);

    // VdmVehicleController
    Route::get('vdmVehicles/dealerSearchModel', [VdmVehicleController::class, 'dealerSearchModel'])->name('vdmVehicles/dealerSearchModel');
    Route::get('vdmVehicles/dealerSearch', [VdmVehicleController::class, 'dealerSearch'])->name('vdmVehicles/dealerSearch');

    Route::post('vehicleList', [VdmVehicleController::class, 'vehicleListAjax'])->name('ajax.vehicleList');
    Route::post('vehicleStatus', [VdmVehicleViewController::class, 'vehicleStatusAjax'])->name('ajax.vehicleStatus');
    Route::post('vdmVehicles/findDealerList', [VdmVehicleController::class, 'findDealerList'])->name("vdmVehicles/findDealerList");

    Route::resource('vdmVehicles', VdmVehicleController::class);
    Route::get('vdmVehicles/edit/{param1}', [VdmVehicleController::class, 'edit']);
    Route::get('vdmVehicles/stops/{param}/{param1}', [VdmVehicleController::class, 'stops']);
    Route::get('vdmVehicles/removeStop/{param}/{param1}', [VdmVehicleController::class, 'removeStop']);
    Route::get('vdmVehicles/migration/{param1}', [VdmVehicleController::class, 'migration']);
    Route::post('vdmVehicles/migrationUpdate', [VdmVehicleController::class, 'migrationUpdate'])->name('migrationUpdate');
    Route::post('vdmVehicles/generate', [VdmVehicleController::class, 'generate']);
    Route::post('vdmVehicles/moveDealer', [VdmVehicleController::class, 'moveDealer']);
    // rename device
    Route::get('vdmVehicles/rename/{param}', [VdmVehicleController::class, 'rename']);
    Route::post('vdmVehicles/renameUpdate', [VdmVehicleController::class, 'renameUpdate']);
    // calibrateType
    Route::get('vdmVehicles/calibrate/{vid}/{count?}', [VdmVehicleController::class, 'calibrateType'])->name('calibrate');
    Route::post('vdmVehicles/calibrateUpdate', [VdmVehicleController::class, 'calibrateTypeUpdate'])->name('calibrateUpdate');
    Route::post('calibrateget', [VdmVehicleController::class, 'calibrateGet'])->name('ajax.calibrateget');

    Route::get('vdmVehicles/create/{param1}', [VdmVehicleController::class, 'create']);

    // s
    Route::get('vdmVehicles/calibrateOil/{param}', [VdmVehicleController::class, 'calibrate']);
    Route::get('vdmVehicles/calibrate/{param}/{param1}/{param2}', [VdmVehicleController::class, 'calibrateCount']);

    Route::get('vdmVehicles/calibrateOil/{param}/{param1}', [VdmVehicleController::class, 'calibrate']);
    Route::post('vdmVehicles/updateCalibration', [VdmVehicleController::class, 'updateCalibration']);
    Route::get('vdmVehicles/multi', [VdmVehicleController::class, 'multi']);
    Route::get('vdmVehicles/index1', [VdmVehicleController::class, 'index1']);

    Route::get('vdmVehicles/{param}/edit1', [VdmVehicleController::class, 'edit1']);
    Route::post('vdmVehicles/update1', [VdmVehicleController::class, 'update1']);


    Route::get('vdmVehicles/stops1/{param}/{param1}', [VdmVehicleController::class, 'stops1']);

    Route::get('vdmVehicles/removeStop1/{param}/{param1}', [VdmVehicleController::class, 'removeStop1']);

    Route::post('vdmVehicles/storeMulti', [VdmVehicleController::class, 'storeMulti']);

    Route::get('vdmfuel/{param1}', [VdmVehicleController::class, 'fuelConfig']);
    Route::post('select0', [VdmVehicleController::class, 'getVehicleDetails'])->name('ajax.details');
    Route::post('vdmfuel/fuelUpdate', [VdmVehicleController::class, 'fuelUpdate']);


    // VdmVehicleScanController
    Route::resource('VdmVehicleScan', VdmVehicleScanController::class);
    Route::get('VdmVehicleScan{param}', [VdmVehicleScanController::class, 'scanNew'])->name('VdmVehicleScan');
    Route::get('VehicleScan/sendExcel', [VdmVehicleScanController::class, 'sendExcel'])->name('VehicleScan/sendExcel');

    // RfidController
    Route::post('downloadTagExcel', [RfidController::class, 'downloadTags'])->name('downloadTagExcel');
    Route::post('uploadTags', [RfidController::class, 'uploadTags'])->name('uploadTagsExcel');
    Route::get('rfid/showAddTags', [RfidController::class, 'showAddTags'])->name('showAddTags');
    Route::get('rfid/showTags', [RfidController::class, 'showTags'])->name('showTags');
    Route::get('rfid/showTag/{orgId}', [RfidController::class, 'showTag'])->name('showTag');
    Route::resource('rfid', RfidController::class);
    Route::get('rfid/{param}/destroy', [RfidController::class, 'destroy']);
    Route::get('rfid/editRfid/{param}', [RfidController::class, 'edit1']);
    Route::post('rfid/index1', [RfidController::class, 'index1']);
    Route::post('rfid/addTags', [RfidController::class, 'addTags'])->name('addTags');
    Route::post('rfid/update', [RfidController::class, 'update']);
    Route::post('user_select', [RfidController::class, 'getVehicle'])->name('ajax.user_select');
    

    //AuditController
    Route::get('archive/{model}', [AuditController::class, 'auditShow']);
    Route::get('audit/{type}/{name?}', [AuditNewController::class, 'index'])->name('audit');
    Route::get('dropTable/{type?}/{name?}', [AuditNewController::class, 'dropTable'])->name('dropTable');

    // AuditReportController
    Route::get('auditReport/{model}', [AuditReportController::class, 'auditShow']);

    ///Advance scan for GROUPS      
    Route::resource('vdmGroups', VdmGroupController::class);

    Route::get('groups/createNew{parem}', [VdmGroupController::class, 'createNew']);
    Route::post('groups/downloadUploadVehicle', [VdmGroupController::class, 'downloadVehicleTemplate'])->name('downloadUploadVehicle');
    Route::post('groups/uploadVehicle', [VdmGroupController::class, 'uploadVehicles'])->name('uploadVehicles');
    Route::get('vdmGroupsScan/Search{parem}', [VdmGroupController::class, 'groupScanNew']);
    Route::get('vdmGroup/sendExcel', [VdmGroupController::class, 'sendExcel'])->name('vdmGroup/sendExcel');
    Route::get('vdmGroups/Search', [VdmGroupController::class, 'groupSearch'])->name('vdmGroups/Search');
    Route::post('vdmGroupsScan/Search', [VdmGroupController::class, 'groupScan'])->name('vdmGroupsScan/Search');
    // ajax calls
    Route::post('groupId', [VdmGroupController::class, 'groupIdCheck'])->name('ajax.groupIdCheck');


    ///Advance scan for USERS    
    Route::resource('vdmUsers', VdmUserController::class);

    Route::get('user/createNew{parem}', [VdmUserController::class, 'createNew']);
    Route::get('vdmuser/sendExcel', [VdmUserController::class, 'sendExcel'])->name('vdmuser/sendExcel');
    Route::get('vdmUserScan/user{parem}', [VdmUserController::class, 'scanNew']);
    Route::get('vdmUserSearch/Scan', [VdmUserController::class, 'search'])->name('vdmUserSearch/Scan');
    Route::post('vdmUserScan/user', [VdmUserController::class, 'scan']);
    Route::get('vdmUsers/reports/{param}', [VdmUserController::class, 'reports']);
    Route::post('vdmUsers/updateReports', [VdmUserController::class, 'updateReports']);
    Route::get('userPreference/{param}', [VdmUserController::class, 'userPreference']);
    Route::post('updateUserPre', [VdmUserController::class, 'updateUserPreference']);
    Route::get('vdmUsers/notification/{param}', [VdmUserController::class, 'notification']);
    Route::post('vdmUsers/updateNotification', [VdmUserController::class, 'updateNotification']);
    // ajax call
    Route::post('userId', [VdmUserController::class, 'userIdCheck'])->name('ajax.userIdCheck');


    ///Advance scan for DEALERS  
    Route::resource('vdmDealers', VdmDealersController::class);
    Route::get('dealer/createNew{param}', [VdmDealersController::class, 'createNew']);
    Route::get('vdmDealers/reports/{param}', [VdmUserController::class, 'reports']);
    Route::get('vdmDealersScan/Search{param}', [VdmDealersScanController::class, 'dealerScanNew']);
    Route::get('vdmDealersSearch/Scan', [VdmDealersScanController::class, 'dealerSearch'])->name('vdmDealersSearch/Scan');
    Route::post('vdmDealersScan/Search', [VdmDealersScanController::class, 'dealerScan']);
    Route::get('vdmDealers/enable/{param}', [VdmDealersController::class, 'enable']);
    Route::get('vdmDealers/disable/{param}', [VdmDealersController::class, 'disable']);
    // ajax call
    Route::post('dealerId', [VdmDealersController::class, 'dealerCheck'])->name('ajax.dealerCheck');
    //licence Allocation
    Route::get('dealers/licence', [VdmDealersController::class, 'licenceAlloc'])->name('dealers/licence');
    Route::post('dealers/licenceUpdate', [VdmDealersController::class, 'licenceUpdate']);
    Route::post('dealers/enabledStarterDealer', [VdmDealersController::class, 'enabledStarterDealer'])->name('ajax.enabledStarterDealer');


    Route::get('vdmDealers/editDealer/{param}', [VdmDealersController::class, 'editDealer']);
    Route::get('dealer/sendExcel', [VdmDealersController::class, 'sendExcel'])->name('dealer/sendExcel');
    Route::resource('vdmSchools', VdmSchoolController::class);


    ///Advance scan for ORGANIZATION
    Route::get('org/createNew{param}', [VdmOrganizationController::class, 'createNew']);
    Route::get('vdmOrganization/adhi{param}', [VdmOrganizationController::class, 'ScanNew']);
    Route::get('vdmOrganization/Scan', [VdmOrganizationController::class, 'Search'])->name('vdmOrganization/Scan');
    Route::post('vdmOrganization/adhi', [VdmOrganizationController::class, 'Scan']);
    Route::get('vdmOrganization/{param}/poiEdit', [VdmOrganizationController::class, 'poiEdit']);
    Route::get('vdmOrganization/{param}/poiDelete', [VdmOrganizationController::class, 'poiDelete']);
    Route::get('vdmOrganization/{param}/getSmsReport', [VdmOrganizationController::class, 'getSmsReport']);
    Route::get('vdmOrganization/{param}/pView', [VdmOrganizationController::class, 'pView']);
    Route::get('vdmOrganization/{param}/editAlerts', [VdmOrganizationController::class, 'editAlerts']);
    Route::get('vdmOrganization/{param}/siteNotification', [VdmOrganizationController::class, 'siteNotification']);
    Route::get('vdmOrganization/placeOfInterest', [VdmOrganizationController::class, 'placeOfInterest'])->name('placeOfInterest');
    Route::post('vdmOrganization/addpoi', [VdmOrganizationController::class, 'addpoi']);
    Route::post('vdmOrganization/updateNotification', [VdmOrganizationController::class, 'updateNotification']);
    Route::post('vdmOrganization/siteUpdate', [VdmOrganizationController::class, 'siteUpdate']);
    Route::post('getOrgSite', [VdmOrganizationController::class, 'getOrgSite'])->name('ajax.getOrgSite');
    Route::resource('vdmOrganization', VdmOrganizationController::class);
    //smsconfig
    Route::get('orgsms/{param}', [VdmOrganizationController::class, 'smsconfig']);
    //smsconfig
    Route::get('orgsmsconfig/{param}', [VdmOrganizationController::class, 'smsconfig']);
    Route::post('vdmsmsconfig/update', [VdmOrganizationController::class, 'smsconfigUpdate']);
    //org Track
    Route::get('vdmOrganization/{param}/orgTrack', [VdmOrganizationController::class, 'orgTrack']);
    Route::post('vdmOrganization/orgUpdate', [VdmOrganizationController::class, 'orgUpdate']);
    Route::get('vdmOrganization/{param}/orgTrackList', [VdmOrganizationController::class, 'orgTrackList']);
    Route::post('vdmOrganization/orgTrackEditUpdate', [VdmOrganizationController::class, 'orgTrackEditUpdate']);
    Route::get('vdmOrganization/{param1}/orgTrackEdit/{param2}', [VdmOrganizationController::class, 'orgTrackEdit']);
    Route::get('vdmOrganization/{param1}/orgTrackDelete/{param2}', [VdmOrganizationController::class, 'orgTrackDelete']);
    // invoke from javascript
    Route::get('storeOrgValues/val', [VdmOrganizationController::class, 'storedOrg']);
    Route::get('storeOrgValues/editRoutes', [VdmOrganizationController::class, '_editRoutes']);
    Route::get('storeOrgValues/deleteRoutes', [VdmOrganizationController::class, '_deleteRoutes']);
    Route::get('storeOrgValues/mapHistory', [VdmOrganizationController::class, '_mapHistory']);
    Route::get('VdmOrg/getStopName', [VdmOrganizationController::class, 'getStopName']);
    Route::get('getOrgveh', [VdmOrganizationController::class, 'getOrgveh']);
    // ajax call
    Route::post('orgId', [VdmOrganizationController::class, 'ordIdCheck'])->name('ajax.ordIdCheck');

    // VdmSmsController
    Route::get('vdmSmsReportFilter', [VdmSmsController::class, 'filter'])->name('vdmSmsReportFilter.filter');
    Route::post('vdmSmsReport', [VdmSmsController::class, 'show']);

    Route::get('vdmLoginCustom', [VdmLoginController::class, 'show']);

    // UploadController

    // for image upload
    Route::post('Upload', [UploadController::class, 'upload']);
    Route::post('sourceCode', [UploadController::class, 'sourceData']);

    Route::get('loginCustom/{param}', [UploadController::class, 'loginCustom'])->name('loginCustom');
    Route::get('loginCustom/temp1/{param}', [UploadController::class, 'template1']);
    Route::get('loginCustom/temp2/{param}', [UploadController::class, 'template2']);
    Route::get('loginCustom/temp3/{param}', [UploadController::class, 'template3']);
    Route::get('loginCustom/temp4/{param}', [UploadController::class, 'template4']);
    Route::get('loginCustom/temp5/{param}', [UploadController::class, 'template5']);
    // Resources
    Route::resource('Device', DeviceController::class);
    Route::post('DeviceListAjax', [DeviceController::class, 'deviceListAjax'])->name('ajax.deviceList');

    Route::resource('Licence', LicenceController::class);

    Route::post('reprocessVehicle', [VdmVehicleController::class, 'reprocessVehicle'])->name('reprocess.vehicle');
});
// admin auth end

Route::get('Adminaudit/{type}/{name?}', [AuditNewController::class, 'index'])->name('Adminaudit');
Route::get('vdmFranchises/licConvert', [VdmFranchiseController::class, 'licConvert'])->name('vdmFranchises/licConvert');
Route::post('vdmFranchises/licConvert', [VdmFranchiseController::class, 'updateLicConvert'])->name('updateLicConvert');
Route::get('vdmFranchises/disable/{param}', [VdmFranchiseController::class, 'disable']);
Route::get('vdmFranchises/Enable/{param}', [VdmFranchiseController::class, 'Enable']);
Route::get('vdmFranchises/cnvert/{param}', [VdmFranchiseController::class, 'prePaidCnv']);
Route::get('vdmFranchises/licenceType/convertion', [VdmFranchiseController::class, 'licenceType']);
Route::get('fransOnboard', [VdmFranchiseController::class, 'fransOnboard']);
Route::get('vdmFranchises/fransOnboard', [VdmFranchiseController::class, 'fransOnboard']);
Route::post('fransOnUpdate', [VdmFranchiseController::class, 'fransOnUpdate']);

Route::get('vdmGeoFence/{token}', [VdmGeoFenceController::class, 'show']);
Route::get('vdmGeoFence/{token}/view', [VdmGeoFenceController::class, 'view']);

Route::get('vdmFranchises/fransearch', [VdmFranchiseController::class, 'fransearch']);
Route::get('vdmFranchises/users', [VdmFranchiseController::class, 'users']);
Route::get('vdmFranchises/buyAddress', [VdmFranchiseController::class, 'buyAddress']);
Route::get('vdmFranchises/expiry', [VdmFranchiseController::class, 'dateModification']);
Route::get('vdmFranchises/licList', [VdmFranchiseController::class, 'getLicenceCount']);

Route::resource('vdmFranchises', VdmFranchiseController::class);
Route::post('vdmFranchises/updateAddCount', [VdmFranchiseController::class, 'updateAddCount']);
Route::post('vdmFranchises/findFransList', [VdmFranchiseController::class, 'findFransList']);
Route::post('vdmFranchises/findUsersList', [VdmFranchiseController::class, 'findUsersList']);
Route::get('vdmFranchises/reports/{param}', [VdmFranchiseController::class, 'reports']);
Route::post('vdmFranchises/updateReports', [VdmFranchiseController::class, 'updateReports']);
Route::get('vdmFranchises/{param}/Sensoer', [VdmFranchiseController::class, 'loadRemove']);
Route::get('licencelist/{param}', [VdmFranchiseController::class, 'getDealerList']);
Route::get('vdmFranchises/EnableVehicleExpiry/{param}/{param1}', [VdmFranchiseController::class, 'enableVehicleExpiry']);
Route::get('vehicle/getcount', [VdmFranchiseController::class, 'liveVehicleCount']);
Route::get('vdmFranchise/updateapn', [VdmFranchiseController::class, 'updateapn']);
Route::get('vdmFranchise/updatetimezone', [VdmFranchiseController::class, 'updatetimezone']);

// fallback route
Route::fallback(fn () => redirect()->route('Business.create'));
