app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', function($scope, $http, vamoservice, $filter, GLOBAL){
	
  //global declaration

    $scope.trvShow       =  sessionStorage.getItem('trackNovateView');
	$scope.uiDate 	     =	{};
	$scope.uiValue	 	 = 	{};
  //$scope.sort                 = sortByDate('startTime');

    todayDate();
	$scope.fromMonthss   =  getNewMonthData();
    //$scope.lenMon      =  daysInThisMonth();
    $scope.lenMon        =  $scope.todayDates-1;

    var tab = getParameterByName('tn');
       
	function getParameterByName(name) {
    	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	//global declartion
	$scope.locations = [];
	$scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
	$scope.gIndex =0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
	$scope.trimColon = function(textVal){
		return textVal.split(":")[0].trim();
	}

	function sessionValue(vid, gname){
		sessionStorage.setItem('user', JSON.stringify(vid+','+gname));
		$("#testLoad").load("../public/menu");
	}
	
	function getTodayDate(date) {
     	var date = new Date(date);
    	return date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2);
    };

    function todayDate(){
	  var today = new Date();
	  var dd    = today.getDate();
	  var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      
      $scope.todayDates    = dd;  
      $scope.currentMonths = mm;
      $scope.currentYears  = yyyy;
    }

    function convert_to_24h(time_str) {
		//console.log(time_str);
 		var str		=	time_str.split(' ');
 		var stradd	=	str[0].concat(":00");
 		var strAMPM	=	stradd.concat(' '+str[1]);
 		var time = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
	    var hours = Number(time[1]);
	    var minutes = Number(time[2]);
	    var seconds = Number(time[2]);
	    var meridian = time[4].toLowerCase();
	
	    if (meridian == 'p' && hours < 12) {
	      hours = hours + 12;
	    }
	    else if (meridian == 'a' && hours == 12) {
	      hours = hours - 12;
	    }	    
	    var marktimestr	=''+hours+':'+minutes+':'+seconds;	    
	    return marktimestr;
    };

    // millesec to day, hours, min, sec
    $scope.msToTime = function(ms) 
    {
        days = Math.floor(ms / (24 * 60 * 60 * 1000));
	  	daysms = ms % (24 * 60 * 60 * 1000);
		hours = Math.floor((ms) / (60 * 60 * 1000));
		hoursms = ms % (60 * 60 * 1000);
		minutes = Math.floor((hoursms) / (60 * 1000));
		minutesms = ms % (60 * 1000);
		seconds = Math.floor((minutesms) / 1000);
		// if(days==0)
		// 	return hours +" h "+minutes+" m "+seconds+" s ";
		// else
			return hours +":"+minutes+":"+seconds;
	}
   	
	var delayed4 = (function () {
  		var queue = [];

	  	function processQueue() {
		    if (queue.length > 0) {
		      setTimeout(function () {
		        queue.shift().cb();
		        processQueue();
		      }, queue[0].delay);
		    }
	  	}

	  	return function delayed(delay, cb) {
	    	queue.push({ delay: delay, cb: cb });

	    	if (queue.length === 1) {
	      	processQueue();
	    	}
	  	};
	}());

   	function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
   		vamoservice.getDataCall(tempurlEvent).then(function(data) {
			$scope.addressEvent[index4] = data.results[0].formatted_address;
			//console.log(' address '+$scope.addressEvent[index4])
			// var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
		})
	};

	$scope.recursiveEvent 	= 	function(locationEvent, indexEvent)
	{
		var index4 = 0;
		angular.forEach(locationEvent, function(value ,primaryKey){
			//console.log(' primaryKey '+primaryKey)
			index4 = primaryKey;
			if(locationEvent[index4].address == undefined)
			{
				var latEvent		 =	locationEvent[index4].latitude;
			 	var lonEvent		 =	locationEvent[index4].longitude;
				var tempurlEvent =	"https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";
				delayed4(2000, function (index4) {
				      return function () {
				        google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
				      };
				    }(index4));
			}
		})
	}

	function getDaysInMonthBio(month,year) {
       return new Date(year, month, 0).getDate();
    }

    function daysInThisMonth() {
		  var now = new Date();
		 // console.log(now);
		  var mm = now.getMonth()+1;
		   /* if(mm==0){
		        mm=12;
		     }*/
		  var nowNew = new Date(now.getFullYear(), mm, 0).getDate();

	 return nowNew;
    }


	function formatAMPM(date) {
    	  var date = new Date(date);
		  var hours = date.getHours();
		  var minutes = date.getMinutes();
		  var ampm = hours >= 12 ? 'PM' : 'AM';
		  hours = hours % 12;
		  hours = hours ? hours : 12; // the hour '0' should be '12'
		  minutes = minutes < 10 ? '0'+minutes : minutes;
		  var strTime = hours + ':' + minutes + ' ' + ampm;
		  return strTime;
	}

    function getNewMonthData() {
       $scope.monthsValss=["January", "February","March","April","May","June","July","August","September","October","November","December"]; 

       var dates = new Date();
       var monsValss = dates.getMonth()+1;
       var yearValss = dates.getFullYear(); 

            /*if(monsValss == 0){
                monsValss=12;
                yearValss=dates.getFullYear()-1; 
                $scope.monValss     = monsValss;
                $scope.yearValss    = yearValss;
                $scope.monValFront  = $scope.monthsValss[monsValss-1];
                $scope.curYearFront = " - "+yearValss;
            } else {*/
                $scope.monValss     = monsValss;
                $scope.yearValss    = yearValss;
                $scope.monValFront  = $scope.monthsValss[monsValss-1];
                $scope.curYearFront = " - "+yearValss;
           
                if(monsValss<10){
                   monsValss = "0"+monsValss; 
                }
           // }
   
        return monsValss+'/'+yearValss;
     }



    $scope.submitMon=function(val){

		  var fromMon = $('#monthFrom').val();
		//console.log(fromMon);

		  var newmonVals       = fromMon.split('/');
		  $scope.monValss      = parseInt(newmonVals[0]);
		  $scope.yearValss     = newmonVals[1];

          var selecMonth = parseInt($scope.monValss);
          var selecYear  = parseInt($scope.yearValss);

		  if(selecYear==$scope.currentYears && selecMonth==$scope.currentMonths) {

              $scope.lenMon = $scope.todayDates-1;
		  } else {

              $scope.lenMon = getDaysInMonthBio($scope.monValss,$scope.yearValss);
		  }

		 // console.log($scope.lenMon);

		   $scope.colValss = $scope.lenMon+2;

		   $scope.monValFront  = $scope.monthsValss[newmonVals[0]-1];
		   $scope.curYearFront = " - "+newmonVals[1];

		     //console.log(parseInt(newmonVal[0]));
		       //console.log($scope.yearFuelVal);
		   startLoading();
		   $scope.showMonTable = false;
		   webCall();
    } 


	function bioMetric(data){
		console.log(data);

		var retArr = [];

	    for(var i=0;i<data.length;i++){

			retArr.push({vehiId:data[i].vehicleId,vehiNam:data[i].vehicleName});
            retArr[i].attendDriver = [];
            retArr[i].attendHelper = [];
            var j = 0; 

     	    angular.forEach(data[i].getTagData, function(val, key){  
	       	 //console.log(key+' '+val);
	       	   angular.forEach(val.attendanceDetail, function(vals, keys){ 
	         	// console.log(j);
	         		if(j===0) {
                       if(keys==0){
                       	 retArr[i].attendDriver.push({atnVal:val.designation});
                         retArr[i].attendDriver.push({atnVal:val.tagName});
                       }

                       if(vals==0){
                    	  retArr[i].attendDriver.push({atnVal:"A"});
                       } else if(vals==1){
                          retArr[i].attendDriver.push({atnVal:"P"});
                       }
	           	      
		           	    if(keys===val.attendanceDetail.length-1) {
		           	    	retArr[i].attendDriver.push({atnVal:val.totalPresent});
		           	    	retArr[i].attendDriver.push({atnVal:val.totalAbsent});
	                        //console.log('first complete...');
	                        j=j+1;
		           	    }
	             	
	             	} else if(j===1){
	             		if(keys==0){
	             		  retArr[i].attendHelper.push({atnVal:val.designation});	
	             		  retArr[i].attendHelper.push({atnVal:val.tagName});
	             		}

                        if(vals==0){
	           	          retArr[i].attendHelper.push({atnVal:"A"});
                        } else if(vals==1){
                          retArr[i].attendHelper.push({atnVal:"P"}); 
                        }

	           	        if(keys===val.attendanceDetail.length-1) {
	           	        	retArr[i].attendHelper.push({atnVal:val.totalPresent});
	           	        	retArr[i].attendHelper.push({atnVal:val.totalAbsent});
	                       // console.log('first complete...');
	                        j=j+1;
		           	    }
                    }     
                  //console.log(vals);
	          	});
	        });
     	}
       //console.log(retArr);
      return retArr;
	}


    function webCall(){

      //var empUrl='http://188.166.244.126:9000/biametricAttendanceReport?userId=Ejectalive&group=BHARATGROUPS:EJT&year=2018&month=04';
        var empUrl= GLOBAL.DOMAIN_NAME+'/biametricAttendanceReport?&group='+$scope.gName+'&year='+$scope.yearValss+'&month='+$scope.monValss;
        console.log(empUrl);

        $scope.empData = [];

        $http.get(empUrl).success(function(data){

           $scope.monthDates = [];

           for(var i=0;i<$scope.lenMon;i++){
               $scope.monthDates.push(i+1);
           }	
                
          $scope.empData2      =  bioMetric(data);
          $scope.empDatas      =  [$scope.empData2];
          $scope.empData       =  $scope.empDatas[0];
          $scope.showMonTable  =  true;

        //console.log($scope.empData);
          stopLoading();
		}); 
    }

	//get the value from the ui
	function getUiValue(){
		$scope.uiDate.fromdate   =	$('#dateFrom').val();
	  	$scope.uiDate.fromtime	 =	$('#timeFrom').val();
	  	$scope.uiDate.todate	 =	$('#dateTo').val();
	  	$scope.uiDate.totime 	 =	$('#timeTo').val();
    }

// service call for the event report

/*	function webServiceCall(){
		$scope.siteData = [];
		if((checkXssProtection($scope.uiDate.fromdate) == true) && (checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true)) {
			
			var url 	= GLOBAL.DOMAIN_NAME+"/getActionReport?vehicleId="+$scope.vehiname+"&fromDate="+$scope.uiDate.fromdate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+$scope.uiDate.todate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&interval="+$scope.interval+"&stoppage="+$scope.uiValue.stop+"&stopMints="+$scope.uiValue.stopmins+"&idle="+$scope.uiValue.idle+"&idleMints="+$scope.uiValue.idlemins+"&notReachable="+$scope.uiValue.notreach+"&notReachableMints="+$scope.uiValue.notreachmins+"&overspeed="+$scope.uiValue.speed+"&speed="+$scope.uiValue.speedkms+"&location="+$scope.uiValue.locat+"&site="+$scope.uiValue.site+'&fromDateUTC='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUTC='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
			vamoservice.getDataCall(url).then(function(responseVal){
				$scope.recursiveEvent(responseVal, 0);
				$scope.eventData = responseVal;
				var entry=0,exit=0; 
				angular.forEach(responseVal, function(val, key){
					if(val.state == 'SiteExit')
						exit++ 
					else if (val.state == 'SiteEntry')
						entry++
				})
				$scope.siteEntry 	=	entry;
				$scope.siteExit 	=	exit;
				stopLoading();
			});
		}
		stopLoading();
	}*/

	// initial method

	$scope.$watch("url", function (val) {
		vamoservice.getDataCall($scope.url).then(function(data) {
           
          //startLoading();
            $scope.selectVehiData = [];
			$scope.vehicle_group=[];
			$scope.vehicle_list = data;

			if(data.length){
				$scope.vehiname	= getParameterByName('vid');
				$scope.uiGroup 	= $scope.trimColon(getParameterByName('vg'));
				$scope.gName 	= getParameterByName('vg');
				angular.forEach(data, function(val, key){
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
					if($scope.gName == val.group){
						$scope.gIndex = val.rowId;
 
						angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

                            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

							if($scope.vehiname == value.vehicleId)
							$scope.shortNam	= value.shortName;
						})
						
					}
			    })

		  //console.log($scope.selectVehiData);
		    sessionValue($scope.vehiname, $scope.gName)
			}
			var dateObj 			   = 	new Date();
			$scope.fromNowTS		   =	new Date(dateObj.setDate(dateObj.getDate()-1));
			$scope.uiDate.fromdate 	   =	getTodayDate($scope.fromNowTS);
		  	$scope.uiDate.fromtime	   =	'12:00 AM';
		  	$scope.uiDate.todate	   =	getTodayDate($scope.fromNowTS);
		  //$scope.uiDate.totime 	   =	formatAMPM($scope.fromNowTS.getTime());
            $scope.uiDate.totime 	   =    '11:59 PM';
		  //webServiceCall();
		    startLoading();
		    webCall();
		  //stopLoading();
		});	
	});

    
     $scope.groupSelection 	= function(groupName, groupId) {
		startLoading();
		$scope.gName 	= 	groupName;
		$scope.uiGroup 	= 	$scope.trimColon(groupName);
		$scope.gIndex	=	groupId;
		var url  		= 	GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

		vamoservice.getDataCall(url).then(function(response){
		
			$scope.vehicle_list = response;
			$scope.shortNam		= response[$scope.gIndex].vehicleLocations[0].shortName;
			$scope.vehiname		= response[$scope.gIndex].vehicleLocations[0].vehicleId;
			sessionValue($scope.vehiname, $scope.gName);
            $scope.selectVehiData=[];
            //console.log(response);
            	angular.forEach(response, function(val, key){
					if($scope.gName == val.group){
					//	$scope.gIndex = val.rowId;
                        angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys){

                            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

							if($scope.vehiname == value.vehicleId)
							$scope.shortNam	= value.shortName;
						})
				    }
				})

			//getUiValue();
			webCall();
		  //webServiceCall();
	      //stopLoading();
		});

	}


/*	$scope.genericFunction 	= function (vehid, index){
		startLoading();
		$scope.vehiname		= vehid;
		sessionValue($scope.vehiname, $scope.gName)
		angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
			if(vehid == val.vehicleId)
				$scope.shortNam	= val.shortName;
		})
		getUiValue();
	//	webServiceCall();

	}*/

  	/*$scope.submitFunction 	=	function(){
	  startLoading();
	  getUiValue();
	  webCall();
	//webServiceCall();
    //stopLoading();
	}*/

	$scope.exportData = function (data) {
		// console.log(data);
		var blob = new Blob([document.getElementById(data).innerHTML], {
           	type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, data+".xls");
    };

    $scope.exportDataCSV = function (data) {
		// console.log(data);
		CSV.begin('#'+data).download(data+'.csv').go();
    };

	$('#minus').click(function(){
		$('#menu').toggle(1000);
	})

}]);
