app.controller('mainCtrl',['$scope','$http','vamoservice','$filter','_global', function($scope,$http,vamoservice, $filter,GLOBAL) {

 // global declaration

  $scope.fileName        =  '';  
//$scope.fileExtn        =  false;
  $scope.fileNameShow    =  false;
  $scope.fileUploadsBox  =  true;  
  $scope.tripSearchBox   =  true;   
  $scope.tripBaseShow    =  false;   
  $scope.vehiBaseShow    =  false; 
  $scope.backBtnShow     =  false;
  $scope.organId         =  '';

  $scope.sort    = sortByDate('Zone');
    


  $('#notifyMsg').hide();



  $scope.vehUrl  =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations';

   
  stopLoading();

  function sessionValue(vid, gname){
    $("#testLoad").load("../public/menu");
  }
  sessionValue();
  $scope.groupId = 0;

  $scope.$watch("vehUrl", function (val) {
    
        $http({
            method : "GET",
            url : $scope.vehUrl
        }).then(function mySuccess(response) {

            console.log(response.data);

           // $scope.vehiName   = response.data[0].vehicleId;
            $scope.grpName  = response.data[$scope.groupId].group;

            $scope.data     = response.data;

            $scope.grpList  = [];
            $scope.vehiList = [];

            angular.forEach(response.data, function(value, key){

               //var spltVal = value.group.split(":");
                 $scope.grpList.push({gName:value.group});

                  if($scope.grpName == value.group) { 

                     $scope.groupId=value.rowId;

                     angular.forEach(value.vehicleLocations, function(val, k){

                        $scope.vehiList.push({'vehiId' : val.vehicleId, 'vName' : val.shortName});

                            if(val.vehicleId == $scope.vehiName){
                                $scope.shortVehiId  = val.shortName;
                                $scope.vehiId  = val.vehicleId;
                            }
                    });
                  } 

            });     

             console.log( $scope.grpList );
                console.log( $scope.vehiList );

                createData();
                  callApiFunc();

        }, function errorCallback(response) {
              
          console.log(response.status);

    });

  });


  $scope.gMapLink1 = function(val) {

    var splitValue    = val.split(',');

    return splitValue[0];
  }

  $scope.gMapLink2 = function(val) {

    var splitValue    = val.split(',');

    return splitValue[1];
  }

    
   $scope.groupSelection = function(val) {

    console.log(val);

    var grpVal     =  val;
    $scope.grpName =  val;

    var grpVehUrl  =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+grpVal;
    console.log(grpVehUrl);

        $http({
            method : "GET",
            url : grpVehUrl
        }).then(function mySuccess(response) {

            console.log(response.data);

            //$scope.vehiName = response.data[0].vehicleId;
            //$scope.grpName  = response.data[$scope.groupId].group;

            $scope.data     = response.data;

            $scope.grpList  = [];
            $scope.vehiList = [];

            angular.forEach(response.data, function(value, key){

               //var spltVal = value.group.split(":");

                $scope.grpList.push({gName:value.group});

                  if($scope.grpName == value.group) { 

                    $scope.groupId = value.rowId;

                    angular.forEach(value.vehicleLocations, function(val, k){

                        $scope.vehiList.push({'vehiId' : val.vehicleId, 'vName' : val.shortName});

                            if(val.vehicleId == $scope.vehiName){
                                $scope.shortVehiId  = val.shortName;
                                $scope.vehiId  = val.vehicleId;
                            }
                    });
                  } 

            });  

            callApiFunc();

             console.log( $scope.grpList );
                console.log( $scope.vehiList );

        }, function errorCallback(response) {
            console.log(response.status);
    });
  }  

  function createData(){

    var retArr=[];

    
       retArr.push({Zone:'Chennai', RO:1, AO:2, Transporter:'ALY',Month:'April', Dealer:'SMP', ShipmentQty:2, Transit:'No', Delivery:'Yes'});  
       retArr.push({Zone:'Pune', RO:1, AO:2, Transporter:'ALY',Month:'May', Dealer:'SMP', ShipmentQty:5, Transit:'No', Delivery:'No'});  
       retArr.push({Zone:'Hydrebad', RO:1, AO:2, Transporter:'ALY',Month:'April', Dealer:'SMP', ShipmentQty:3, Transit:'Yes', Delivery:'No'});  
       retArr.push({Zone:'New Delhi', RO:2, AO:3, Transporter:'ALY',Month:'February', Dealer:'SMP', ShipmentQty:5, Transit:'No', Delivery:'Yes'});  
       retArr.push({Zone:'Bangalore', RO:1, AO:2, Transporter:'ALY',Month:'September', Dealer:'SMP', ShipmentQty:4, Transit:'No', Delivery:'No'});  
 

    $scope.dummyData = retArr;

  }

 $scope.tabChange = function(val){

  if(val=="report"){

    $scope.sort = sortByDate('vehicleName');

  } else if(val=="dash"){

    $scope.sort = sortByDate('Zone');
  }

 }



  function callApiFunc() {
    
      var fmsUrl   =  GLOBAL.DOMAIN_NAME+'/getDistanceForRoutes?groupName='+$scope.grpName;
    //var fmsUrl   =  'http://128.199.159.130:9000/getDistanceForRoutes?userId=ALY&groupName=ALY:SMP';

        $http({
          method: 'GET',
          url: fmsUrl
        }).then(function successCallback(response) {

            console.log(response.data);

            $scope.fmsData = response.data;



            stopLoading();

        }, function errorCallback(response) {
              console.log(response.status);
        });
  }

  


}]);
