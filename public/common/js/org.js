$(document).ready(function() {
    let timeInput = $('.time')
    $(timeInput).each(function(index) {
        let id = $(this).attr("id");
        $(this).closest('div.form-group').append(`
                    <div class="input-group ${id}" >
                        <div class="input-group-prepend border border-left-0">
                            <label for="${id}" class="input-group-text m-0" id="inputGroupPrepend">
                            <i class="fa fa-clock" aria-hidden="true"></i>
                            </label>
                        </div>
                    </div>
        `)
        $(this).prependTo(`.${id}`);
    })
    $('.time').timepicker();
});