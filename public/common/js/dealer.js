$('.close').on('click',function(){
    img = $(this).attr('data-img')
    $(`.${img}`).fadeOut('slow')
    $(`.${img}`).siblings('.custom-file').find('.custom-file-input').val('')
    label = $(`.${img}`).siblings('.custom-file').find('.custom-file-label').attr('data-def'); 
    $(`.${img}`).siblings('.custom-file').find('.custom-file-label').html(label)
})

// img-prv
$('.custom-file-input').on('change',function(){
    const  input  = this;
    let file = input.files[0];
    if(!file) return
    let fileName = file.name.split('.');
    let fileType = fileName.at(-1).toUpperCase();
    const imageFormat = ['NONE','PNG','JPG','APNG','JPEG','WEBP','SVG','GIF'];
    let size ={
        width: $(input).attr('data-width'),
        height:$(input).attr('data-height')
    }
    if(!imageFormat.includes(fileType)){
        $(input).parents('.custom-file').parents('.col-md-4').find('i.close').trigger('click')
        wornigAlert(fileType+' Format not support.')
        return
    }else{
        let img = new Image();
        url = URL.createObjectURL(file);
        img.onload = function() {
            // console.log(this.width,this.height)
            if(this.width != size.width || this.height != size.height){
                $(input).parents('.custom-file').parents('.col-md-4').find('i.close').trigger('click')
                wornigAlert(`Image must be ${size.width}x${size.height}`)
                return
            }else{
                imgPrev = $(input).attr('data-img')
                $(`.${imgPrev}`).removeClass('d-none').addClass('mt-3').fadeIn()
                $(`.${imgPrev}>.card>.card-body>img`).attr('src',url)
            }
        }
        img.src = url;
    }
})