function setVehicle(id) {
    let date = $('input[name="reprocessDate"]').val();
    Swal.fire({
        title: titleText,
        html: `<br><input class="form-control date" placeholder="dd-mm-yyyy" id="ReprocessDate" type="text" value="${date}" max="${date}">`,
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonText:submitText,
        confirmButtonAriaLabel: submitText,
        cancelButtonText:cancelText,
        cancelButtonAriaLabel: cancelText,
        confirmButtonColor: '#31CE36',
        cancelButtonColor: '#d33',
        willOpen:function(){
            $('#ReprocessDate').datepicker({
                format: 'dd-mm-yyyy', 
                endDate: date,
                clearBtn:true,
            });
        }
        
    }).then((value) => {
        date = $('#ReprocessDate').val()
        if (!value.isConfirmed) return false;
        handleReprocessData(id, date)
    });
}
function handleReprocessData(id, date = "", reSubmit = "false") {
    if (date != "") {
        let token = $('input[name="_token"').val();
        var postValue = {
            "_token": token,
            'date': date,
            'vehicleId': id,
            'reSubmit': reSubmit,
        };
        let reprocessUrl = $("input[name='reprocessUrl']").val()
        $.post(reprocessUrl, postValue)
            .done(function (data) {
                const {
                    message,
                    status
                } = data
                if (status == "yes") {
                    Swal.fire({
                        title: 'Reprocess',
                        text: message,
                        type: 'success',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonAriaLabel:"Reprocess again",
                        cancelButtonText:cancelText,
                        cancelButtonAriaLabel: cancelText,
                        confirmButtonColor: '#31CE36',
                        cancelButtonColor: '#d33',
                    }).then((reSubmit) => {
                        if (reSubmit.isConfirmed) {
                            handleReprocessData(id, date, 'true')
                        } else {
                            Swal.close();
                        }
                    });
                } else {
                    Swal.fire("", message, "success");
                }
            }).fail(function () {
                console.log("fail");
            });
    } else {
        data = "Please select valid date";
        Swal.fire("", data, "warning").then(function () {
            setVehicle(id)
        });
    }
}


function auditVehicle(vehicleId,aTag) {
    var encodedString = (Intl.DateTimeFormat().resolvedOptions().timeZone);
    var time1 = window.btoa(encodedString);
    $(aTag).attr('href', 'auditReport/AuditVehicle&' + time1 + '&' +
        vehicleId);
}