
$(function () {

	if ($('.selectpicker').length !== 0) {
		$('.selectpicker').css('width', '100%');
		$('.selectpicker').select2();
		$('span.selection>.select2-selection.select2-selection--single').addClass(
			'form-control py-select-1');
		$('.select2-selection.select2-selection--single>.select2-selection__arrow').attr('style',
			"top:10px !important;right:10px !important");
	}
	if ($('#example1').length !== 0) {
		let dataTableLang = $("#langJson").val()
		$('#example1').DataTable({
			// scrollX: true,
			stateSave: true,
			"language": {
				"url": dataTableLang
			},
		});
	}
	if ($('.date').length !== 0) {
		$(".date").datepicker({
			format: 'DD-MM-YYYY',
		});
	}
	if ($('.time').length !== 0) {
		$('.time').timepicker();
	}
	setTimeout(function () {
		$('.alert').hide(1000);
	}, 10000);

	$('input[name="address"],input[name="fullAddress"]').each(function () {
		attr = $(this).attrs()
		attr.rows = 3;
		$(this).replaceWith($('<textarea></textarea>').attr(attr).val(attr.value))

	})
});

$(function () {
	$(':file').css({ 'cursor': 'pointer' });
	// We can attach the `fileselect` event to all file inputs on the page
	$(document).on('change', ':file', function () {
		var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [numFiles, label]);

	});

	// We can watch for our custom `fileselect` event like this
	$(':file').on('fileselect', function (event, numFiles, label) {
		var labelchange = $(this).parents('.custom-file').find('.custom-file-label'),
			log = numFiles > 1 ? numFiles + ' files selected' : label;
		$(labelchange).html(log)

	});
	function passwordValidate(event) {
		$(this).removeClass('is-invalid');
		removeAfterErr(this)
		let length = $(this).val().length;
		if (length <= 5) {
			$(this).aftererr('Please, Enter minimum 6 characters or more');
		}
	}
	$('input[name="password"]').keyup(passwordValidate);
});
// checkbox css
$('.customCheckbox').click(function () {
	$(this).parent('.switch').children('.hidden').val($(this).is(':checked') ? 'yes' : 'no');
});
// end checkbox

function mouseEnter() {
	$(this).attr('type', 'text');
}
function mouseLeave() {
	$(this).attr('type', 'password');
}
$('.passhover').mouseenter(mouseEnter).mouseleave(mouseLeave);

function caps(element) {
	let value = element.value.toUpperCase();
	let pattern = /[a-zA-Z0-9\-\_]/g;
	value = value.match(pattern) ?? [];
	$(element).val(value.join(''));
}

$('.caps').on('change', function () {
	caps(this)
});
function wornigAlert(title = "", message = "") {
	Swal.fire({
		title: title,
		text: message,
		icon: "warning",
		button: "cancel",
	});
	return false;
}

function confirmAlert(message) {

	return new Promise((res, rej) => {
		Swal.fire({
			title: "Are you sure?",
			text: message,
			icon: "warning",
			showCloseButton: true,
			showCancelButton: true,
			focusConfirm: false,
			confirmButtonText:
				'Yes',
			confirmButtonAriaLabel: 'Yes',
			cancelButtonText:
				'No',
			cancelButtonAriaLabel: 'No'

		}).then((value) => {
			res(value.isConfirmed);
		})
	});
}


// get all attributes in element 
(function (old) {
	$.fn.attrs = function () {
		if (arguments.length === 0) {
			if (this.length === 0) {
				return null;
			}

			var obj = {};
			$.each(this[0].attributes, function () {
				if (this.specified) {
					obj[this.name] = this.value;
				}
			});
			return obj;
		}

		return old.apply(this, arguments);
	};
})($.fn.attrs);

(function (old) {
	$.fn.aftererr = function (errMsg = false) {
		if (!errMsg) {
			errMsg = 'Enter valid value';
		}
		// $('input').removeClass('is-invalid')
		$(this).addClass('is-invalid').after($('<span  class="text-danger aftererr"></span>').html(errMsg));
	};
})($.fn.aftererr);

function removeAfterErr(input) {
	$(input).removeClass('is-invalid').next('.aftererr').remove();
}

// delete user,dealer,group

$('.deleteBtn').on('click', function () {
	let data = $(this).attrs()
	ConfirmDelete(data)
})

async function ConfirmDelete(data) {
	var x = await confirmAlert("It will removes all " + data.formtype + " based informations? ");
	if (x)
		$('#deleteForm' + data.id).submit();
	else
		return false;
}


// number prevent
$('input[name="mobile"],input[name="mobileNo"]').not('.rfid').keydown(mobileValidate).change(mobileminmax);
function mobileminmax() {
	let length = $(this).val().length;
	if (length <= 7 || length >= 15) {
		$(this).aftererr('Please, Enter valid mobile number');
	}
}
$('.numberOnly').keydown(mobileValidate)
function mobileValidate(event) {
	removeAfterErr(this)
	var invalidChars = ["-", "+", "e", ".", "E"];
	if (invalidChars.includes(event.key)) {
		event.preventDefault();
	}
}

function formValidation(element, validate) {
	// element is form
	let inputs = $(element).find('input');
	for (let index = 0; index < inputs.length; index++) {
		if(!validate) break;
		let input = $(inputs[index]);
		var required = input.attr('required') ?? false; 
		var type = input.attr('type').toLowerCase();
		if (!required && type !== "number") continue;
		if(required && type == "text"){
			if (input.val().trim() == '') {
				input.val('').addClass('is-invalid').focus()
				validate = false
			}
		}
		if (type == "number") {
			var min = +input.attr('min');
			if(!min) continue;
			var value = +input.val();
			if (min > value) {
				validate = false
			}
		}
	}
	return validate
}

function downloadFileViaAjax(url, fileName) {
	$.ajax({
		url: url,
		cache: false,
		xhr: function () {
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function () {
				if (xhr.readyState == 2) {
					if (xhr.status == 200) {
						xhr.responseType = "blob";
					} else {
						xhr.responseType = "text";
					}
				}
			};
			return xhr;
		},
		success: function (data) {
			var blob = new Blob([data], { type: "application/octetstream" });
			var isIE = false || !!document.documentMode;
			if (isIE) {
				window.navigator.msSaveBlob(blob, fileName);
			} else {
				var url = window.URL || window.webkitURL;
				link = url.createObjectURL(blob);
				var a = $("<a  />");
				a.attr("download", fileName);
				a.attr("href", link);
				$("body").append(a);
				a[0].click();
				$("body").remove(a);
			}
		}
	})
}

$('.readonly').on("keydown",function (event) {
	event.preventDefault();
})