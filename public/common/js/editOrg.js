function setEndTime(element) {
    let id = $(element).attr('data-id');
    $('.endTime' + id).attr({
        'min': $(element).val()
    });
}

function removeRow(input) {
    let shiftDataCount = $('#shiftDataCount');
    shiftDataCount.val(parseInt(shiftDataCount.val()) - 1);
    $(input).closest('.dataRow').remove();
    showSeperateDetails();
}

function removereportRow(input){
    let reportsDataCount = $('#reportsDataCount');
    reportsDataCount.val(parseInt(reportsDataCount.val()) - 1);
    $(input).closest('.dataRow').remove();
    showReportsDetails()
}

function showSeperateDetails() {
    $('.shiftField').removeAttr('required');
    if ($('#seperateDetails').is(":checked")) {
        $('.shiftField').attr('required', 'required')
        $('#addshift').fadeIn();
        if ($('#shiftDataCount').val() != 0) {
            $('#seperateDetailsValues').fadeIn();
        } else {
            $('#seperateDetailsValues').fadeOut();
        }
    } else {
        $('#addshift,#seperateDetailsValues').fadeOut();
    }
}

function showReportsDetails() {
    $('.reportsField').removeAttr('required');
    if ($('#EndofDayDetails').is(":checked")) {
        $('.reportsField').attr('required', 'required')
        $('#addreports').fadeIn();
        if ($('#reportsDataCount').val() != 0) {
            $('#reportsDetailsValues').fadeIn();
        } else {
            $('#reportsDetailsValues').fadeOut();
        }
    } else {
        $('#addreports,#reportsDetailsValues').fadeOut();
    }
}



function showSmsProvider() {
    const smsProvider = $('#smsProvider').val();
    $('#textLocalSmsKeyFiled,#smsEntityNameFiled').fadeOut();
    $("#textLocalSmsKey,#smsEntityName").removeAttr('required');

    if (smsProvider === "TextLocal") {
        $('#textLocalSmsKeyFiled,#smsEntityNameFiled').fadeIn();
        $("#textLocalSmsKey,#smsEntityName").attr('required', 'required');
    }
}

function showIpAddress() {
    $("#ip-address-field").fadeOut();
    $("#ip-address").removeAttr('required');
    if ($('#sendDataToGovtip').is(":checked")) {
        $("#ip-address-field").fadeIn();
        $("#ip-address").attr('required', 'required');
    }
}

function showFuelValueFiled() {
    $("#fuelValueFiled").fadeOut();
    $("#fValue").removeAttr('required');
    if ($('#fuelLevelBelow').is(":checked")) {
        $("#fuelValueFiled").fadeIn();
        $("#fValue").attr('required', 'required');
    }
}

function showGeoFenceEntryExit() {
    let sendGeoFenceSMS = $('#sendGeoFenceSMS').val();
    let SchoolGeoFence = $('#SchoolGeoFence').is(":checked");

    $('#Entry,#Exit').fadeOut();
    if (!SchoolGeoFence) {
        $('#sendGeoFenceSMS').val('no')
        return
    }
    if (sendGeoFenceSMS == 'yes') {
        $('#Entry,#Exit').fadeIn();
        return
    }
    // sendGeoFenceSMS have Entry,Exit,yes,no
    $(`#${sendGeoFenceSMS}`).fadeIn();
}
function showEscalatesms() {
    $('#escalateList').fadeOut();
    if ($('[name="escalatesms"]').is(":checked")) {
        $('#escalateList').fadeIn();
    }
}

function showSafemove() {
    $("#UnauthorisedMovementHrs").hide()
    if ($('input[name="safemove"]').is(":checked")) {
        $('#UnauthorisedMovementHrs').fadeIn();
    }
}

function showIsrfid() {
    $('#pickup,#drop').fadeOut();
    if ($('#isrfid').is(":checked")) {
        $('#pickup,#drop').fadeIn();
    }
}

function random() {
    return Math.round(Math.random() * 10) ** 2;
}

function education_fields() {
    room = Date.now() + random();
    $("#education_fields").append(`
    <div class="form-group removeclass${room}">
        <div class="row">
            <div class="col-md-3 nopadding ml-auto">
                <div class="form-group">
                    <input type="number" class="form-control" min=1 max=24 id="duration${room}" name="duration[]"  required value="" multiple placeholder="Duration">
                </div>
            </div>
            <div class="col-md-5 nopadding">
                <div class="form-group">
                    <input type="number" class="form-control numberOnly" id="smsmobile${room}" required name="smsmobile[]" value="" placeholder="Mobile number">
                </div>
            </div>
            <div class="col-md-3 nopadding">
                <div class="form-group">
                    <div class="input-group"> 
                        <div class="input-group-btn">
                            <button class="btn btn-danger btn-sm" type="button"  onclick="remove_education_fields('${room}');">
                                <i class="fas fa-minus"></i> 
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `)
}

function remove_education_fields(rid) {
    $('.removeclass' + rid).remove();
}

$(function () {
    var orgSite = [];
    multipleSelectpickerInitiation();
    function multipleSelectpickerInitiation() {
        $('.multipleSelectpicker').select2({
            data: orgSite
        });
    }

    var data = {
        'orgId': $('input[name="organizationId"]').val(),
        '_token': $("input[name='_token']").val()
    };

    $.post(getOrgSite, data)
        .done(function (data, textStatus, xhr) {
            if (data.length === 0) {
                orgSite = [];
                return false
            }
            data.map((val, i) => {
                orgSite[i] = {
                    id: val,
                    text: val
                }
            })
            multipleSelectpickerInitiation();
        });

    showSeperateDetails();
    $('#seperateDetails').on('change', showSeperateDetails);

    showReportsDetails()
    $("#EndofDayDetails").on('click',showReportsDetails)

    $('#addSeperateDetailsValues').on("click", function () {
        let index = parseInt($('#shiftDataCount').val()) + 1;
        $('#seperateDetailsValues').fadeIn();
        $('#seperateDetailsValues').append(`
            <div class="row p-2 dataRow">
                <div class="col-md-2 col-4 px-0 px-md-2">
                    <input type="text"  name="shiftname${index}" class="form-control" value="shift${index}" required>
                </div>
                <div class="col-md-2 col-4 px-0 px-md-2">
                    <div class="input-group" >
                        <input type="text"  name="startTime${index}" id="startTime${index}startTime" class="form-control entrytime time shiftField" data-id="${index}" required onchange="setEndTime(this)">
                        <div class="input-group-prepend border border-left-0">
                            <label for="startTime${index}startTime" class="input-group-text m-0" id="inputGroupPrepend">
                            <i class="fa fa-clock" aria-hidden="true"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-4 px-0 px-md-2">
                    <div class="input-group" >
                        <input type="text" name="endTime${index}" id="endTime${index}endTime" class="form-control endTime${index} time shiftField" required>
                        <div class="input-group-prepend border border-left-0">
                            <label for="endTime${index}endTime" class="input-group-text m-0" id="inputGroupPrepend">
                            <i class="fa fa-clock" aria-hidden="true"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-8 px-0 px-md-2 mt-2 mt-md-0">
                    <select name="sitename${index}[]"  class="form-control multipleSelectpicker shiftField" multiple="multiple" required>
                        </select>
                </div>
                <div class="col-md-2 col-4 px-0 px-md-2 text-center mt-2 mt-md-0">
                    <button class="btn btn-sm btn-outline-danger" type="button" onclick="removeRow(this)">${RemoveLabel}</button>
                </div>
            </div>
        `);
        $('#shiftDataCount').val(index)
        index++;
        $('.multipleSelectpicker').css('width', '100%');
        multipleSelectpickerInitiation();
        $('.time').timepicker();

    })

    $("#addReportsDetailsValues").on('click',function(){
        let index = parseInt($('#reportsDataCount').val()) + 1;
        $('#reportsDetailsValues').fadeIn();
        $('#reportsDetailsValues').append(`
            <div class="row p-2 dataRow">
                <div class="col-md-3 col-4 px-0 px-md-2">
                    <div class="input-group" >
                        <input type="text"  name="fromTime[]" id="fromTime${index}startTime" class="form-control entrytime time shiftField" data-id="${index}" required onchange="setEndTime(this)">
                        <div class="input-group-prepend border border-left-0">
                            <label for="fromTime${index}startTime" class="input-group-text m-0" id="inputGroupPrepend">
                            <i class="fa fa-clock" aria-hidden="true"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-4 px-0 px-md-2">
                    <div class="input-group" >
                        <input type="text" name="toTime[]" id="toTime${index}endTime" class="form-control endTime${index} time shiftField" required>
                        <div class="input-group-prepend border border-left-0">
                            <label for="toTime${index}endTime" class="input-group-text m-0" id="inputGroupPrepend">
                            <i class="fa fa-clock" aria-hidden="true"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-4 px-0 px-md-2 text-center">
                    <button class="btn btn-sm btn-outline-danger" type="button" onclick="removereportRow(this)">${RemoveLabel}</button>
                </div>
            </div>
        `);
        $('#reportsDataCount').val(index)
        index++;
        $('.time').timepicker();

    })

    // showSmsProvider
    showSmsProvider();
    $('#smsProvider').on('change', showSmsProvider);

    // showFuelValueFiled
    showFuelValueFiled();
    $('#fuelLevelBelow').on('change', showFuelValueFiled);

    // showIpAddress
    showIpAddress();
    $('#sendDataToGovtip').on('change', showIpAddress);

    // showGeoFenceEntryExit
    showGeoFenceEntryExit();
    $('#SchoolGeoFence,#sendGeoFenceSMS').on('change', showGeoFenceEntryExit);

    //escalate
    showEscalatesms();
    $('[name="escalatesms"]').on('change', showEscalatesms);

    // showIsrfid
    showIsrfid();
    $('#isrfid').on('change', showIsrfid);

    // showSafemove
    showSafemove();
    $('input[name="safemove"]').on('change', showSafemove);
    $('.education_fields').on('click', education_fields)

});


$('#submit').on('click', function () {

    // rfid check
    if ($('#isrfid').is(":checked")) {
        $('#PickupStartTime,#PickupEndTime,#DropStartTime,#DropEndTime')
            .attr('required', 'required');
    } else {
        $('#PickupStartTime,#PickupEndTime,#DropStartTime,#DropEndTime')
            .removeAttr('required');
    }

    // entry exit - time value check
    if ($('#SchoolGeoFence').is(":checked")) {
        sendGeoFenceSMS = $('#sendGeoFenceSMS').val()
        if (sendGeoFenceSMS == 'Entry') {
            $(`
                #morningEntryStartTime,
                #morningEntryEndTime,
                #eveningEntryStartTime,
                #eveningEntryEndTime
            `).attr('required', 'required');

            $(`
                #morningExitStartTime,
                #morningExitEndTime,
                #eveningExitStartTime,
                #eveningExitEndTime
            `).removeAttr('required').val('');
        } else if (sendGeoFenceSMS == 'Exit') {
            $(`
                #morningExitStartTime,
                #morningExitEndTime,
                #eveningExitStartTime,
                #eveningExitEndTime
            `).attr('required', 'required');
            $(`
                #morningEntryStartTime,
                #morningEntryEndTime,
                #eveningEntryStartTime,
                #eveningEntryEndTime
            `).removeAttr('required').val('');

        } else if (sendGeoFenceSMS == 'yes') {
            $(`
                #morningExitStartTime,
                #morningExitEndTime,
                #eveningExitStartTime,
                #eveningExitEndTime,
                #morningEntryStartTime,
                #morningEntryEndTime,
                #eveningEntryStartTime,
                #eveningEntryEndTime
            `).attr('required', 'required');
        } else {
            $(`
                #morningExitStartTime,
                #morningExitEndTime,
                #eveningExitStartTime,
                #eveningExitEndTime,
                #morningEntryStartTime,
                #morningEntryEndTime,
                #eveningEntryStartTime,
                #eveningEntryEndTime
            `).removeAttr('required').val('');
        }
    } else {
        $(`
            #morningExitStartTime,
            #morningExitEndTime,
            #eveningExitStartTime,
            #eveningExitEndTime,
            #morningEntryStartTime,
            #morningEntryEndTime,
            #eveningEntryStartTime,
            #eveningEntryEndTime
        `).removeAttr('required').val('');
    }
});