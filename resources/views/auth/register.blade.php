@extends('layouts.guest')

@section('content')
<div class="container">
    <div class="pt-5 row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header h2">{{ __('Register') }}</div>

                <div class="card-body">
                    @if($errors->any())
                        <strong class="text-danger error-message"> 
                            {{ Html::ul($errors->all()) }} 
                        </strong>
                    @endif
                    <form method="POST" action="{{ route('doregister') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @if($errors->first('name')) is-invalid @endif" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus  placeholder="Name">
                                @if($errors->first('name') !=='')
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <div class=" d-flex">
                                    <input id="password-field" type="password" name="password" class="form-control  @if($errors->first('password')) is-invalid @endif" placeholder="Password" required>
                                    <span toggle="#password-field" class="fa fa-fw field-icon toggle-password fa-eye"></span>
                                </div>
                                @if($errors->first('password') !=='')
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{-- <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <div class=" d-flex">
                                    <input id="password-field" type="password" name="password_confirmation" class="form-control" placeholder="Password" required>
                                    <span toggle="#password-field" class="fa fa-fw field-icon toggle-password fa-eye"></span>
                                </div>
                            </div>
                        </div> --}}

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
@endpush
