@extends('layouts.guest')

@section('content')


    <div class="container">
        <div class="pt-5 row justify-content-center">

            <div class="col-md-4">
                <div class="login-wrap p-4 p-md-3 my-5">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="fa fa-user-o"><i class="fas fa-user"></i></span>
                    </div>
                    {{ app()->setLocale(old('lang', 'en')) }}
                    <h3 class="text-center mb-4">@lang('messages.SignIn')</h3>
                    <form class="login-form " method="POST" action="{{ route('dologin') }}" id="loginForm">
                        @csrf
                        @if ($errors->any())
                            <strong class="text-danger error-message">
                                {{ HTML::ul($errors->all()) }}
                            </strong>
                        @endif
                        @if (session()->has('flash_notice'))
                            <strong class="text-danger error-message">
                                {{ HTML::ul([session('flash_notice')]) }}
                            </strong>
                        @endif
                        <input type="hidden" name="token" value="" id="token">
                        <div class="form-group">
                            <input type="text" class="form-control rounded-left" placeholder="{{ __('messages.UserName') }}"
                                required="true" value="{{ old('username') }}" name='userName'>
                            <input type="hidden" name="lang" value="{{ old('lang', 'en') }}">
                        </div>
                        <div class="form-group d-flex">
                            <input id="password-field" type="password" name="password" class="form-control"
                                placeholder="{{ __('messages.Password') }}" required autocomplete="on">
                            <span toggle="#password-field" class="fa fa-fw field-icon toggle-password fa-eye"></span>
                        </div>
                        <div class="form-check ml-1">
                            <label class="form-check-label">
                                <input name="remember_me" type="checkbox" class="form-check-input">
                                <span class="form-check-sign">
                                    @lang('messages.RememberMe')
                                </span>
                            </label>
                        </div>
                        <div class="form-group">
                            <button type="submit"
                                class="form-control btn btn-primary rounded submit px-3">@lang('messages.Login')</button>
                        </div>
                    </form>
                    <div class="form-group d-flex">
                        <div class="w-50 wrap-select">
                            <i class="fas fa-globe"></i>
                            <form action="{{ route('changeLang') }}" method="POST" id="langform" class="m-0">
                                @csrf
                                {{ Form::select('lang', ['en' => 'English', 'pt' => 'português'], old('lang', 'en'), ['class' => 'selectpicker', 'id' => 'lang']) }}
                            </form>

                        </div>
                        <div class="w-50 text-md-right">
                            {{-- <a href="#">@lang('messages.ForgotPassword')</a> --}}
                        </div>

                    </div>
                    {{-- <div class="separator align-items-center justify-content-center">Or login with</div>
                    <div class="row mt-4 justify-content-center">
                        <div class="col-12 text-center pb-3">
                            <a href="{{ route('regitser') }}">Register</a>
                        </div><br />
                        <div class="social-sign" id="google">
                            <button class="form-control btn rounded">
                                <i class="fab fa-google"></i>
                                <span class="ml-2">Google</span>
                            </button>
                        </div>
                    </div> --}}

                    {{-- <div class="alert alert-danger" id="error" style="display: none;"></div>

                    <div class=" enterNumber">
                        <div class="card-header">
                            Enter Phone Number
                        </div>
                        <div class="card-body">

                            <div class="alert alert-success" id="sentSuccess" style="display: none;"></div>

                            <form>
                                <label>Phone Number:</label>
                                <input type="text" id="number" class="form-control" placeholder="+91********">
                                <div id="recaptcha-container"></div>
                                <button type="button" class="btn btn-success" onclick="phoneSendAuth();">SendCode</button>
                            </form>
                        </div>
                    </div>

                    <div class=" verifyCode" style="margin-top: 10px;display: none;">
                        <div class="card-header">
                            Enter Verification code
                        </div>
                        <div class="card-body">

                            <div class="alert alert-success" id="successRegsiter" style="display: none;"></div>

                            <form>
                                <input type="text" id="verificationCode" class="form-control"
                                    placeholder="Enter verification code">
                                <button type="button" class="btn btn-success" onclick="codeverify();">Verify code</button>

                            </form>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>

    </div>
@endsection


@push('js')
    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.4.1/firebase-app.js"></script>

    <!-- TODO: Add SDKs for Firebase products that you want to use
        https://firebase.google.com/docs/web/setup#available-libraries -->
    <script src="https://www.gstatic.com/firebasejs/8.4.1/firebase-analytics.js"></script>
    <!-- firebase -->
    <script src="https://www.gstatic.com/firebasejs/6.2.4/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/6.2.4/firebase-auth.js"></script>
    <!-- firebaseUI -->
    <script src="https://cdn.firebase.com/libs/firebaseui/4.0.0/firebaseui.js"></script>

    <script>
        $(function(){
            // Your web app's Firebase configuration
            // For Firebase JS SDK v7.20.0 and later, measurementId is optional
            // var firebaseConfig = {
            //     apiKey: "AIzaSyAGTq9GhiNP8R4p9yAnb1NjWY04JAM1LQg",
            //     authDomain: "revamp-cpanel-3865b.firebaseapp.com",
            //     projectId: "revamp-cpanel-3865b",
            //     storageBucket: "revamp-cpanel-3865b.appspot.com",
            //     messagingSenderId: "136491674005",
            //     appId: "1:136491674005:web:f7cb14cdfb59f115d46e9c"
            // };
            // // Initialize Firebase
            // firebase.initializeApp(firebaseConfig);
            // var googleProvider = new firebase.auth.GoogleAuthProvider();
    
            $('#google').click(async function(){
                $('#loginForm').attr('action', '{{ route('googleLogin') }}');
                firebase.auth().signInWithPopup(googleProvider).then(function(result) {
                    result.user.getIdToken().then(function(result) {
                        $('#token').val(result);
                        $('#loginForm').submit();
                        // const csrfToken = getCookie('csrfToken')
                        // return postIdTokenToSessionLogin('/sessionLogin', result, csrfToken);
                    });
                }).catch(function(error) {
                    console.log(error);
                });
            });
            $(".toggle-password").click(function() {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
            $('#lang').on('change', function() {
                // window.localStorage.setItem('lang',$(this).val())
                $('#langform').submit();
            });
            window.onload = function() {
                // render();
            };
    
            function render() {
                window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
                recaptchaVerifier.render();
            }
    
            function phoneSendAuth() {
    
                var number = $("#number").val();
    
                firebase.auth().signInWithPhoneNumber(number, window.recaptchaVerifier).then(function(confirmationResult) {
    
                    window.confirmationResult = confirmationResult;
                    coderesult = confirmationResult;
    
                    $("#sentSuccess").text("Message Sent Successfully.");
                    $("#sentSuccess").show();
                    $('.enterNumber').hide()
                    $(".verifyCode").show();
    
                }).catch(function(error) {
                    $("#error").text(error.message);
                    $("#error").show();
                });
    
            }
    
            function codeverify() {
    
                var code = $("#verificationCode").val();
    
                coderesult.confirm(code).then(function(result) {
                    result.user.getIdToken().then(function(result) {
                        $('#loginForm').attr('action', '{{ route('googleLogin') }}');
                        $('#token').val(result);
                        $('#loginForm').submit();
                        // const csrfToken = getCookie('csrfToken')
                        // return postIdTokenToSessionLogin('/sessionLogin', result, csrfToken);
                    });
                    // var user = result.user;
                    // $('#token').val(result.user.ra);
                    // $('#loginForm').submit();
    
                    $("#successRegsiter").text("you are register Successfully.");
                    $("#successRegsiter").show();
    
                }).catch(function(error) {
                    $("#error").text(error.message);
                    $("#error").show();
                });
            }
            
        })
    </script>
@endpush
