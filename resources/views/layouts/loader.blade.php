<!-- Simple splash screen-->
@if (!Request::is('audit*'))
<div class="splash">
    {{-- <div class="color-line"></div> --}}
    <div class="splash-title">
        <h1>
            @lang('messages.Loading')
        </h1>
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>
@endif