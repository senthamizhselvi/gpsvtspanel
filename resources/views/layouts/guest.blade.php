
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Login</title>
	<meta name="theme-color" content="#999999" />
	<meta name="description" content="Admin panel of GPSVTS"/>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="{{ asset('assets/imgs/favicon.ico') }}" type="image/x-icon"/>

	<!-- Fonts and icons -->
    <script src="{{ asset('common/js/plugin/webfont/webfont.min.js') }}"></script>
    <script src="{{ asset('common/js/core/jquery.3.2.1.min.js') }}"></script>
    <script src="{{ asset('scripts/homer.js') }}"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    
    <script>
        WebFont.load({
            google: {
                "families": ["Lato:300,400,700,900"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular",
                    "Font Awesome 5 Brands", "simple-line-icons"
                ],
                urls: ['{{ asset('common/css/fonts.min.css') }}']
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });

    </script>
	
	<!-- CSS Files -->
	<link rel="stylesheet" href="{{asset ('common/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset ('common/css/atlantis.css') }}">
	<link rel="stylesheet" href="{{asset ('common/css/guest.css') }}">
	@stack("css")
</head>
<body>
	<div class="login-page">
		@yield("content")
	</div>
	<script src="{{ asset('js/app.js') }}"></script>
	<script src="{{ asset('common/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('common/js/core/popper.min.js') }}"></script>
	@stack('js')
</body>
</html>