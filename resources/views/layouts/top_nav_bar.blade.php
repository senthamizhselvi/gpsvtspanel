<div class="main-header">
    <!-- Logo Header -->
    <div class="logo-header" data-background-color="blue">

        <a href="{{ route('Business.create') }}" class="logo d-inline-block text-truncate" style="margin-left: -10px">
            <span class="text-white ">{{ strtoupper(auth()->id()) }}</span>
        </a>
        <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse"
            data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon">
                <i class="icon-menu"></i>
            </span>
        </button>
        @if (!Request::is('*loginCustom*'))
        <button class="topbar-toggler more">
            <i class="icon-options-vertical"></i>
        </button>
        @endif
        <div class="nav-toggle">
            <button class="btn btn-toggle toggle-sidebar" aria-label="menu">
                <i class="icon-menu"></i>
            </button>
        </div>
    </div>
    <!-- End Logo Header -->

    {{-- main nav --}}
    <nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">

        <div class="container-fluid">
            @if (!Request::is('*loginCustom*'))
                <div class="collapse" id="search-nav" style="max-width: 450px">
                    <div class="row">
                        <div class="col-4 px-0">
                            <select class="form-control" id="findSelect">
                                <option value="device">
                                    @lang('messages.Vehicles')
                                </option>
                                <option value="group">
                                    @lang('messages.Groups')
                                </option>
                                <option value="user">
                                    @lang('messages.Users')
                                </option>
                                @if (session('cur') == 'admin' && session('cur1') != 'prePaidAdmin' && session('cur') !== 'dealer')
                                    <option value="dealer">
                                        @lang('messages.Dealers')
                                    </option>
                                @endif
                                <option value="org">
                                    @lang('messages.Organization')
                                </option>
                            </select>
                        </div>
                        <div class="col-8 pl-1">
                            <div class="navbar-left navbar-form nav-search mr-md-3">
                                <div class="input-group">
                                    <input type="text" placeholder="{{trans('messages.Search')}}" class="form-control caps" id="findInput">
                                    <div class="input-group-prepend">
                                        <button type="submit" id="findBtn" class="btn btn-search" aria-label="search">
                                            <i class="fa fa-search search-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <p class="h3 text-white b">
                    @lang('messages.LoginCustomisation')
                </p>
            @endif
            {{-- @if (session('cur1') != 'prePaidAdmin')
                <div class="collapse ml-2" id="search-nav">
                    <div class="row">
                        <div class="col-4 px-0">
                            <select class="form-control" id="addSelect">
                                @if (session('cur') == 'admin')
                                    <option value="device">Vehicles</option>
                                    <option value="group">Groups</option>
                                    <option value="user">Users</option>
                                    @if (session('cur') == 'admin' && session('cur1') != 'prePaidAdmin')
                                        <option value="dealer">Dealers</option>
                                    @endif
                                    <option value="org">Organization</option>
                                @endif
                                @if (session('cur') == 'dealer')
                                    <option value="device">Vehicles</option>
                                    <option value="group">Groups</option>
                                    <option value="user">Users</option>
                                    <option value="org">Organization</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-8 pl-1">
                            <div class="navbar-left navbar-form nav-search mr-md-3">
                                <div class="input-group">
                                    <input type="number" placeholder="Enter Quantity to add" class="form-control"
                                        min="1" id="addInput" onkeyup='cap(this)'>
                                    <div class="input-group-prepend">
                                        <button type="submit" id="addBtn" class="btn btn-search" aria-label="search">
                                            <i class="fas fa-plus-circle fa-2x text-white search-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif --}}
            {{-- user details --}}
            {{-- <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                <li class="nav-item toggle-nav-search hidden-caret">
                    <a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
                        <i class="fa fa-search"></i>
                    </a>
                </li>
                <li class="nav-item dropdown hidden-caret">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                        <div class="avatar-sm">
                            <img src="https://ui-avatars.com/api/?name={{ Auth::user()->displayName }}&bold=true&background=fff&color=1572e8" alt="..." class="avatar-img rounded-circle">
                        </div>
                    </a>
                    <ul class="dropdown-menu dropdown-user animated fadeIn">
                        <div class="dropdown-user-scroll scrollbar-outer">
                            <li>
                                <div class="user-box">
                                    <div class="avatar-lg">
                                        <img src="https://ui-avatars.com/api/?name={{ Auth::user()->displayName }}&bold=true&background=1572e8&color=fff" alt="image profile" class="avatar-img rounded">
                                    </div>
                                    <div class="u-text">
                                        <h4>{{ Auth::user()->displayName }}</h4>
                                        <p class="text-muted">{{ Auth::user()->displayName.'@vamo.com' }}</p><a href="profile.html" class="btn btn-xs btn-secondary btn-sm">View Profile</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">My Profile</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('logout')}}">Logout</a>
                            </li>
                        </div>
                    </ul>
                </li>
            </ul> --}}
        </div>
    </nav>
</div>
@push('js')
    <script>
        $('#findInput').keypress(function(e) {
            if (e.which == 13) {
                $('#findBtn').click();
            }
        });
        $('#findBtn').on('click', function() {
            var data1 = {
                'val1': $('#findSelect').val(),
                'val2': $('#findInput').val()

            };
            var locate;
            if (data1.val1 != '' && data1.val2 == '') {
                wornigAlert('Please enter search value or text');
                $('#findInput').focus();
            } else {
                if (data1.val1 == 'device') {
                    locate = '/VdmVehicleScan' + data1.val2;
                } else if (data1.val1 == 'group') {
                    locate = '/vdmGroupsScan/Search' + data1.val2;
                } else if (data1.val1 == 'user') {
                    locate = '/vdmUserScan/user' + data1.val2;
                } else if (data1.val1 == 'dealer') {
                    locate = '/vdmDealersScan/Search' + data1.val2;
                } else if (data1.val1 == 'org') {
                    locate = '/vdmOrganization/adhi' + data1.val2;
                } else {
                    wornigAlert('Please Select Your Search Option');
                    // locate = '/Business';
                }
            }
            if (locate) {
                window.location.pathname = '/cpanel/public' + locate;
            }
        });
    </script>
@endpush
