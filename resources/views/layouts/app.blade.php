<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"  xml:lang="{{app()->getLocale()}}" lang="{{app()->getLocale()}}">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Admin Panel</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <meta name="description" content="Admin panel of GPSVTS"/>
    <link rel="icon" href="{{ asset('assets/imgs/favicon.ico') }}" type="image/x-icon" />
    <link rel="manifest" href="{{asset('manifest.json')}}" />
    <!-- Fonts and icons -->
    <script src="{{ asset('common/js/plugin/webfont/webfont.min.js') }}"></script>
    <script src="{{ asset('common/js/core/jquery.3.2.1.min.js') }}"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    
    <script>
        WebFont.load({
            google: {
                "families": ["Lato:300,400,700,900"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular",
                    "Font Awesome 5 Brands", "simple-line-icons"
                ],
                urls: ['{{ asset('common/css/fonts.min.css') }}']
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });

    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}" /> --}}
    <link rel="stylesheet" href="{{ asset('common/css/atlantis.css') }}">


    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="{{ asset('common/css/demo.css') }}">
    <link rel="stylesheet" href="{{ asset('common/css/guest.css') }}">
    @stack("css")
</head>

<body data-background-color='bg1'>
    <div class="wrapper {{
        Request::is('*vdmVehicles/calibrate*')||
        Request::is('vdmVehicles/edit*') || 
        Request::is('vdmVehicles')|| 
        Request::is('vehicleStatus')|| 
        Request::is('vdmVehiclesView')|| 
        Request::is('VdmVehicleScan*')|| 
        Request::is('dashboard')|| 
        Request::is('audit*') ||
        Request::is('Device') ? "sidebar_minimize" : "" }}">

        <!-- Navbar Header -->
        @include('layouts.top_nav_bar')
        <!-- End Navbar -->

        {{-- nav Side Bar --}}
        @include('layouts.side_nav_bar')

        <div class="main-panel">
            @include('layouts.loader')
            @if(!Request::is("*Device*"))
            @endif
            <div class="content">
                @yield('content')
            </div>
            {{-- @include('layouts.footer') --}}
        </div>
        @include('vdm.modal.Addmodal')
        @stack("modal")
        {{-- cdn.datatables.net/plug-ins/9dcbecd42ad/i18n --}}
        <input id="langJson" type="hidden" value="{{ asset('assets/dataTable/'.app()->getLocale().'.json') }}">
    </div>
    <!--   Core JS Files   -->
    <script src="{{ asset('common/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('common/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('scripts/homer.js') }}"></script>
    {{-- <script src="{{ asset('common/js/validation.min.js') }}"></script> --}}

    <!--- Moment -->
    <script src="{{ asset('common/js/plugin/moment/moment.min.js') }}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('common/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('common/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>

    <!-- jQuery Scrollbar -->
    <script src="{{ asset('common/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>

    <!-- datetimepicker JS -->
    {{-- <script src="{{ asset('common/js/bootstrap-datetimepicker.min.js') }}"></script>   --}}
    <script src="{{ asset('common/js/DateTimePicker/datepicker.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('common/js/DateTimePicker/datepicker.min.css') }}">
    <script src="{{ asset('common/js/DateTimePicker/timepicker.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('common/js/DateTimePicker/timepicker.min.css') }}">

    <!-- Datatables -->
    <script src="{{ asset('common/js/plugin/datatables/datatables.min.js') }}"></script>

    <!-- Sweet Alert -->
    <script src="{{ asset('common/js/plugin/sweetalert/sweetalert.min.js') }}"></script>


    <!-- Atlantis JS -->
    <script src="{{ asset('common/js/atlantis.min.js') }}"></script>

    <!-- selecct2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}" />
    <script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
    <script src="{{ asset('common/js/script.js') }}"></script>
    @if (request()->server('HTTP_HOST') !== "localhost")
        {{-- <script src="{{ asset('common/js/serviceWorker.js') }}"></script> --}}
        {{-- <script src="{{ asset('common/js/inspect.js') }}"></script> --}}
    @endif
    @stack("js")
</body>

</html>
