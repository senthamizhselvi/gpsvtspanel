<!-- Sidebar -->
<div class="sidebar sidebar-style-2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="https://ui-avatars.com/api/?name={{ auth()->id() }}&bold=true&background=1572e8&color=fff"
                        alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            <span class="d-inline-block text-truncate">
                                {{ strtoupper(auth()->id()) }}
                            </span>
                            <span class="user-level m-0"> {{ucwords(trans('messages.'.session('cur')))}}</span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li class="nav-item px-0">
                                <form action="{{ route('changeLang') }}" method="POST" id="langform" class="m-0">
                                    @csrf
                                    {{ Form::select('lang', ['en' => 'English', 'pt' => 'português'], session('phplang'), ['class' => 'form-control selectpicker', 'id' => 'lang']) }}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav nav-primary">
                <li class="nav-item {{ \Request::is('*dashboard') ? 'active' : '' }}">
                    <a href="{{ route('dashboard') }}">
                        <i class="fas fa-home"></i>
                        <span class="sub-item">
                            @lang('messages.Dashboard')
                        </span>
                    </a>
                </li>

                <li
                    class="nav-item {{ \Request::is('*Business*') || \Request::is('*Device*') || \Request::is('*DeviceScan*') || \Request::is('*Business/create') || \Request::is('*Remove*') ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#base" id="Business">
                        <i class="fas fa-user-tie"></i>
                        <p>
                            @lang('messages.Business')
                        </p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse {{ \Request::is('*Business*') || \Request::is('*Device*') || \Request::is('*DeviceScan*') || \Request::is('*Business/create') || \Request::is('*Remove*') ? 'show' : '' }}"
                        id="base">
                        <ul class="nav nav-collapse">
                            @if (Session::get('cur') == 'admin')
                                <li
                                    class="{{ \Request::is('*Business*') || \Request::is('*addDevice*') ? 'active' : '' }}">
                                    <a href="{{ route('Business.create') }}">
                                        <span class="sub-item">
                                            @lang('messages.AddDevice')
                                        </span>
                                    </a>
                                </li>
                                @if (Session::get('cur1') != 'prePaidAdmin')
                                    <li class="{{ \Request::is('*Remove*') ? 'active' : '' }}">
                                        <a href="{{ route('Remove.create') }}">
                                            <span class="sub-item">
                                                @lang('messages.RemoveDevice')
                                            </span>
                                        </a>
                                    </li>
                                @endif
                                @if (Session::get('cur2') == 'ktrackAdmin')
                                    <li class="{{ \Request::is('*ktot*') ? 'active' : '' }}">
                                        <a href="ktot">
                                            <span class="nav-label">
                                                @lang('messages.PreOnboardDevices')
                                            </span>
                                        </a>
                                    </li>
                                @endif
                                <li class="{{ \Request::is('*Device') ? 'active' : '' }}">
                                    <a href="{{ route('Device.index') }}">
                                        <span class="sub-item">
                                            @lang('messages.OnboardDevices')
                                        </span>
                                    </a>
                                </li>
                                <li class="{{ \Request::is('*DeviceScan*') ? 'active' : '' }}">
                                    <a href="{{ route('DeviceScan.index') }}">
                                        <span class="sub-item">
                                            @lang('messages.OnboardDevicesSearch')    
                                        </span>
                                    </a>
                                </li>
                            @endif
                            @if (Session::get('cur') == 'dealer' && Session::get('cur1') != 'prePaidAdmin')
                                <li class="{{ \Request::is('*Business') ? 'active' : '' }}">
                                    <a href="{{ route('Business.index') }}">
                                        <span class="sub-item">
                                            @lang('messages.DeviceList')
                                        </span>
                                    </a>
                                </li>
                            @endif
                            @if (Session::get('cur') == 'dealer' && Session::get('cur1') == 'prePaidAdmin')
                                <li class="{{ \Request::is('*Business') ? 'active' : '' }}">
                                    <a href="{{ route('Business.index') }}">
                                        <span class="nav-label">
                                            @lang('messages.AddDevice')
                                        </span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </li>
                @if (Session::get('cur1') == 'prePaidAdmin')
                    <li
                        class="nav-item {{ \Request::is('*Billing') || \Request::is('*billing*') || \Request::is('*licence') && !Request::is('dealers/licence') ? 'active' : '' }}">
                        <a href="{{ route('Billing.index') }}">
                            <i class="fas fa-comment-alt"></i>
                            <span class="sub-item">
                                @lang('messages.Billing')
                            </span>
                        </a>
                    </li>
                @endif
                <li class="nav-item {{ \Request::is('*audit*') ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#AuditsidebarLayoutsNew" id="Audit">
                        <i class="fas fa-clipboard-list"></i>
                        <p>
                            @lang('messages.Audit')
                        </p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse {{ \Request::is('*audit*') ? 'show' : '' }}" id="AuditsidebarLayoutsNew">
                        <ul class="nav nav-collapse">
                            @if (Session::get('cur') == 'admin' && Session::get('cur') != 'dealer')
                                <li class="{{ \Request::is('*audit/admin*') ? 'active' : '' }}">
                                    <a href="{{route('audit',['type'=>'admin'])}}">
                                        <span class="sub-item">
                                            @lang('messages.Admin')
                                        </span>
                                    </a>
                                </li>
                            @endif
                            @if (Session::get('cur') == 'admin')
                                <li class="{{ \Request::is('*audit/delaer*') ? 'active' : '' }}">
                                    <a href="{{route('audit',['type'=>'dealer'])}}">
                                        <span class="sub-item">
                                            @lang('messages.Dealers')
                                        </span>
                                    </a>
                                </li>
                            @else
                                <li class="{{ \Request::is('*audit/dealer*') ? 'active' : '' }}">
                                    <a href="{{route('audit',['type'=>'dealer'])}}">
                                        <span class="sub-item">
                                            @lang('messages.Auditing')
                                        </span>
                                    </a>
                                </li>
                            @endif
                            <li class="{{ \Request::is('*audit/user*') ? 'active' : '' }}">
                                <a href="{{route('audit',['type'=>'user'])}}" >
                                    <span class="sub-item">
                                        @lang('messages.Users')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*audit/vehicle*') ? 'active' : '' }}">
                                <a href="{{route('audit',['type'=>'vehicle'])}}" >
                                    <span class="sub-item">
                                        @lang('messages.Vehicles')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*audit/manualcalibrate*') ? 'active' : '' }}">
                                <a href="{{route('audit',['type'=>'manualcalibrate'])}}" id="auditManualCalibrate">
                                    <span class="sub-item">
                                        @lang('messages.ManualCalibrate')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*audit/autocalibrate*') ? 'active' : '' }}">
                                <a href="{{route('audit',['type'=>'autocalibrate'])}}" id="auditAutoCalibrate">
                                    <span class="sub-item">
                                        @lang('messages.AutoCalibrate')
                                    </span>
                                </a>
                            </li>

                            @if (Session::get('cur1') == 'prePaidAdmin')
                                <li class="{{ \Request::is('*audit/renewal*') ? 'active' : '' }}">
                                    <a href="#" id="renewal">
                                        <span class="sub-item">
                                            @lang('messages.RenewalDetails')
                                        </span>
                                    </a>
                                </li>
                            @endif
                            <li class="{{ \Request::is('*audit/group*') ? 'active' : '' }}">
                                <a href="{{route('audit',['type'=>'group'])}}">
                                    <span class="sub-item">
                                        @lang('messages.Groups')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*audit/organisation*') ? 'active' : '' }}">
                                <a href="{{route('audit',['type'=>'organization'])}}" >
                                    <span class="sub-item">
                                        @lang('messages.Organizations')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*audit/loginCustomisation*') ? 'active' : '' }}">
                                <a href="#" id="loginCustomisation">
                                    <span class="sub-item">
                                        @lang('messages.LoginCustomisation')
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item {{ \Request::is('*archive*') ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#AuditsidebarLayouts" id="Audit">
                        <i class="fas fa-clipboard-list"></i>
                        <p>
                            @lang('messages.Archive')
                        </p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse {{ \Request::is('*archive*') ? 'show' : '' }}" id="AuditsidebarLayouts">
                        <ul class="nav nav-collapse">
                            @if (Session::get('cur') == 'admin' && Session::get('cur') != 'dealer')
                                <li class="{{ \Request::is('*archive/AuditFrans*') ? 'active' : '' }}">
                                    <a href="#" id="archiveFrans">
                                        <span class="sub-item">
                                            @lang('messages.Admin')
                                        </span>
                                    </a>
                                </li>
                            @endif
                            @if (Session::get('cur') == 'admin')
                                <li class="{{ \Request::is('*archive/AuditDealer*') ? 'active' : '' }}">
                                    <a href="#" id="archiveDealer">
                                        <span class="sub-item">
                                            @lang('messages.Dealers')
                                        </span>
                                    </a>
                                </li>
                            @else
                                <li class="{{ \Request::is('*archive/AuditDealer*') ? 'active' : '' }}">
                                    <a href="#" id="archiveDealer">
                                        <span class="sub-item">
                                            @lang('messages.Auditing')
                                        </span>
                                    </a>
                                </li>
                            @endif
                            <li class="{{ \Request::is('*archive/AuditUser*') ? 'active' : '' }}">
                                <a href="#" id="archiveUser">
                                    <span class="sub-item">
                                        @lang('messages.Users')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*archive/AuditVehicle*') ? 'active' : '' }}">
                                <a href="#" id="archiveVehicle">
                                    <span class="sub-item">
                                        @lang('messages.Vehicles')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*archive/AuditManualCalibrate*') ? 'active' : '' }}">
                                <a href="#" id="archiveManualCalibrate">
                                    <span class="sub-item">
                                        @lang('messages.ManualCalibrate')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*archive/AuditAutoCalibrate*') ? 'active' : '' }}">
                                <a href="#" id="archiveAutoCalibrate">
                                    <span class="sub-item">
                                        @lang('messages.AutoCalibrate')
                                    </span>
                                </a>
                            </li>

                            @if (Session::get('cur1') == 'prePaidAdmin')
                                <li class="{{ \Request::is('*archive/RenewalDetails*') ? 'active' : '' }}">
                                    <a href="#" id="renewal">
                                        <span class="sub-item">
                                            @lang('messages.RenewalDetails')
                                        </span>
                                    </a>
                                </li>
                            @endif
                            <li class="{{ \Request::is('*archive/AuditGroup*') ? 'active' : '' }}">
                                <a href="#" id="archiveGroup">
                                    <span class="sub-item">
                                        @lang('messages.Groups')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*archive/AuditOrg*') ? 'active' : '' }}">
                                <a href="#" id="archiveOrg">
                                    <span class="sub-item">
                                        @lang('messages.Organizations')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*archive/loginCustomisation*') ? 'active' : '' }}">
                                <a href="#" id="loginCustomisation">
                                    <span class="sub-item">
                                        @lang('messages.LoginCustomisation')
                                    </span>
                                </a>
                            </li>
                            {{-- <li class="{{ \Request::is('*archive/CustomizeLandingPage*') ? 'active' : '' }}">
                                <a href="#" id="customizeLandingPage">
                                    <span class="sub-item">
                                        @lang('messages.CustomizeLandingPage')
                                    </span>
                                </a>
                            </li> --}}
                        </ul>
                    </div>
                </li>
                <li
                    class="nav-item {{ (\Request::is('*VdmVehicleScan*') || \Request::is('*vdmVehicles*') || \Request::is('*vdmVehiclesView*') || \Request::is('*rfid*') || \Request::is('*vehicleStatus*')) && !\Request::is('*vdmVehicles/dealerSearch')  ||\Request::is('*showTags') ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#sidebarLayouts" id="Vehicle">
                        <i class="fas fa-truck"></i>
                        <p>
                            @lang('messages.Vehicles')
                        </p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse {{ \Request::is('*VdmVehicleScan*') || \Request::is('*vdmVehicles') || \Request::is('*vdmVehiclesView*') || \Request::is('*rfid*') || \Request::is('*vehicleStatus*') || \Request::is('*vdmVehicles/stops*') ||\Request::is('*showTags') ? 'show' : '' }}"
                        id="sidebarLayouts">
                        <ul class="nav nav-collapse">
                            <li class="{{ \Request::is('*VdmVehicleScan') ? 'active' : '' }}">
                                <a href="{{ route('VdmVehicleScan.index') }}">
                                    <span class="sub-item">
                                        @lang('messages.VehiclesSearch')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*vdmVehicles') ? 'active' : '' }}">
                                <a href="{{ route('vdmVehicles.index') }}">
                                    <span class="sub-item">
                                        @lang('messages.VehiclesList')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*vdmVehiclesView') ? 'active' : '' }}">
                                <a href="{{ route('vdmVehiclesView.index') }}">
                                    <span class="sub-item">
                                        @lang('messages.ViewVehicles')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*vehicleStatus*') ? 'active' : '' }}">
                                <a href="{{ route('vehicleStatus') }}">
                                    <span class="sub-item">
                                        @lang('messages.VehicleStatuses')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*rfid/create') ? 'active' : '' }}" data-toggle="modal"
                                data-target="#addTagModal" id="addTagPopUp">
                                <a href="#">
                                    <span class="sub-item">
                                        @lang('messages.AddTags')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('*rfid*') ? 'active' : '' }}" id="showTags">
                                <a href="{{ route('showTags') }}">
                                    <span class="sub-item">
                                        @lang('messages.ViewTags')
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item {{ \Request::is('*vdmGroups') || \Request::is('*vdmGroups*') ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#forms" id="Groups">
                        <i class="fas fa-users"></i>
                        <p>
                            @lang('messages.Groups')
                        </p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse {{ \Request::is('*vdmGroups') || \Request::is('*vdmGroups*') ? 'show' : '' }}"
                        id="forms">
                        <ul class="nav nav-collapse">
                            <li class="{{ \Request::is('vdmGroups') ? 'active' : '' }}">
                                <a href="{{ route('vdmGroups.index') }}">
                                    <span class="sub-item">
                                        @lang('messages.GroupsList')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('vdmGroups/create') ? 'active' : '' }}">
                                <a href="{{ route('vdmGroups.create') }}">
                                    <span class="sub-item">
                                        @lang('messages.AddGroup')
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li
                    class="nav-item {{ \Request::is('vdmUserSearch*') || \Request::is('*vdmUsers*') || \Request::is('*vdmUserScan*') ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#tables" id="Users">
                        <i class="fas fa-user-friends"></i>
                        <p>
                            @lang('messages.Users')
                        </p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse {{ \Request::is('vdmUserSearch*') || \Request::is('*vdmUsers*') || \Request::is('*vdmUserScan*') ? 'show' : '' }}"
                        id="tables">
                        <ul class="nav nav-collapse">
                            <li class="{{ \Request::is('vdmUsers') ? 'active' : '' }}">
                                <a href="{{ route('vdmUsers.index') }}">
                                    <span class="sub-item">
                                        @lang('messages.UsersList')
                                    </span>
                                </a>
                            </li>
                            <li class="{{ \Request::is('vdmUsers/create') ? 'active' : '' }}">
                                <a href="{{ route('vdmUsers.create') }}">
                                    <span class="sub-item">
                                        @lang('messages.AddUser')
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                @if (Session::get('cur') == 'admin')
                    <li
                        class="nav-item {{ \Request::is('vdmDealersSearch/*') || \Request::is('vdmDealers*') || \Request::is('dealers/licence') ? 'active' : '' }}">
                        <a data-toggle="collapse" href="#maps" id="Dealers">
                            <i class="fas fa-user"></i>
                            <p>
                                @lang('messages.Dealers')
                            </p>
                            <span class="caret"></span>
                        </a>
                        <div class="collapse {{ \Request::is('vdmDealersSearch/*') || \Request::is('vdmDealers*') || \Request::is('dealers/licence') ? 'show' : '' }}"
                            id="maps">
                            <ul class="nav nav-collapse">
                                <li class="{{ \Request::is('vdmDealers') ? 'active' : '' }}">
                                    <a href="{{ route('vdmDealers.index') }}">
                                        <span class="sub-item">
                                            @lang('messages.DealersList')
                                        </span>
                                    </a>
                                </li>
                                <li class="{{ \Request::is('vdmDealers/create') ? 'active' : '' }}">
                                    <a href="{{ route('vdmDealers.create') }}">
                                        <span class="sub-item">
                                            @lang('messages.AddDealer')
                                        </span>
                                    </a>
                                </li>
                                @if (Session::get('cur1') == 'prePaidAdmin')
                                    <li class="{{ \Request::is('dealers/licence') ? 'active' : '' }}">
                                        <a href="{{ route('dealers/licence') }}">
                                            <span class="sub-item">
                                                @lang('messages.AllocateLicence')
                                            </span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif
                <li class="nav-item {{ \Request::is('vdmOrganization*') ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#charts" id="Organisation">
                        <i class="far fa-chart-bar"></i>
                        <p>
                            @lang('messages.Organizations')
                        </p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse {{ \Request::is('vdmOrganization*') ? 'show' : '' }}" id="charts">
                        <ul class="nav nav-collapse">

                            <li class="{{ \Request::is('vdmOrganization') ? 'active' : '' }}">
                                <a href="{{ route('vdmOrganization.index') }}">
                                    <span class="sub-item">
                                        @lang('messages.OrganizationList')
                                    </span>
                                </a>
                            </li>
                            <li class=" {{ \Request::is('vdmOrganization/create') ? 'active' : '' }}">
                                <a href="{{ route('vdmOrganization.create') }}">
                                    <span class="sub-item">
                                        @lang('messages.AddOrganization')
                                    </span>
                                </a>
                            </li>
                            {{-- <li class="{{ \Request::is('vdmOrganization/placeOfInterest') ? 'active' : '' }}">
                                <a href="{{ route('vdmOrganization/placeOfInterest') }}">
                                    <span class="sub-item">Add POI</span>
                                </a>
                            </li> --}}
                        </ul>
                    </div>
                </li>
                {{-- <li class="nav-item">
                    <a data-toggle="collapse" href="#maps">
                        <i class="fas fa-route"></i>
                        <p>Routes</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="maps">
                        <ul class="nav nav-collapse">
                            <li>
                                <a href="vdmBusRoutes/create">
                                    <span class="sub-item">Create Bus Routes with Stops</span>
                                </a>
                            </li>
                            <li>
                                <a href="vdmBusRoutes/roadSpeed">
                                    <span class="sub-item">Road Speed</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li> 
                <li class="nav-item {{ \Request::is('vdmSmsReportFilter') ? 'active' : '' }}">
                    <a href="{{ route('vdmSmsReportFilter.filter') }}">
                        <i class="fas fa-comment-alt"></i>
                        <span class="sub-item">SMS Reports</span>
                    </a>
                </li> --}}
                @if (Session::get('cur') == 'admin')
                    <li class="nav-item {{ \Request::is('vdmVehicles/dealerSearch') ? 'active' : '' }}"
                        id="SwitchLogin" data-toggle="modal" data-target="#SwitchLoginModal">
                        <a href="#">
                            <i class="fas fa-random"></i>
                            <span class="sub-item">
                                @lang('messages.SwitchLogin')
                            </span>
                        </a>
                    </li>
                @endif

                <li class="nav-item {{ \Request::is('*vdmLoginCustom') ? 'active' : '' }}">
                    <a href="{{ route('loginCustom', [auth()->id()]) }}">
                        <i class="fas fa-edit"></i>
                        <span class="sub-item">
                            @lang('messages.LoginCustomization')
                        </span>
                    </a>
                </li>
                @if (Session::get('cur') == 'dealer')
                    <li class="nav-item {{ \Request::is('*vdmDealers/editDealer*') ? 'active' : '' }}">
                        <a href="{{ URL::to('vdmDealers/editDealer/' . Session::get('cur')) }}">
                            <i class="fas fa-user-circle"></i>
                            <span class="sub-item">
                                @lang('messages.MYPROFILE')
                            </span>
                        </a>
                    </li>
                @endif
                <li class="nav-item">
                    <a href="{{ route('logout') }}">
                        <i class="fas fa-sign-out-alt"></i>
                        <span class="sub-item">@lang('messages.Logout')</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- End Sidebar -->
@push('js')
    <script>
        $('#lang').on('change', function() {
            $('#langform').submit();
        });
        var encodedString = (Intl.DateTimeFormat().resolvedOptions().timeZone);
        var time1 = window.btoa(encodedString);
        $('#archiveVehicle').on('click', function() {
            $('#archiveVehicle').attr('href', '/cpanel/public/archive/AuditVehicle-' + time1);
        });
        $('#archiveManualCalibrate').on('click', function() {
            $('#archiveManualCalibrate').attr('href', '/cpanel/public/archive/AuditManualCalibrate-' + time1);
        });
        $('#archiveAutoCalibrate').on('click', function() {
            $('#archiveAutoCalibrate').attr('href', '/cpanel/public/archive/AuditAutoCalibrate-' + time1);
        });
        $('#archiveGroup').on('click', function() {
            $('#archiveGroup').attr('href', '/cpanel/public/archive/AuditGroup-' + time1);
        });
        $('#archiveDealer').on('click', function() {
            $('#archiveDealer').attr('href', '/cpanel/public/archive/AuditDealer-' + time1);
        });
        $('#archiveUser').on('click', function() {
            $('#archiveUser').attr('href', '/cpanel/public/archive/AuditUser-' + time1);
        });
        $('#archiveOrg').on('click', function() {
            $('#archiveOrg').attr('href', '/cpanel/public/archive/AuditOrg-' + time1);
        });
        $('#archiveFrans').on('click', function() {
            $('#archiveFrans').attr('href', '/cpanel/public/archive/AuditFrans-' + time1);
        });
        $('#loginCustomisation').on('click', function() {
            $('#loginCustomisation').attr('href', '/cpanel/public/archive/loginCustomisation-' + time1);
        });
        $('#customizeLandingPage').on('click', function() {
            $('#customizeLandingPage').attr('href', '/cpanel/public/archive/CustomizeLandingPage-' + time1);
        });
        $('#renewal').on('click', function() {
            $('#renewal').attr('href', '/cpanel/public/archive/RenewalDetails-' + time1);
        });
    </script>
@endpush
