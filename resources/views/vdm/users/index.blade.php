@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            <div class="hpanel">
                <div class="pull-right">
                    <a href="{{ route('vdmuser/sendExcel') }}">
                        <img class="pull-right" width="40px" height="50px" src="{{ asset('assets/imgs/xls.svg') }}"
                            alt="xls" method="get" /></a>
                </div>
                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.UserList')
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.UserList')
                            </a>
                        </li>
                    </ul>
                </div>
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif
                <div class="page-body">
                    <div class="card">
                        <div class="card-body">
                            <span class="text-danger error-message">
                                {{ HTML::ul($errors->all()) }}
                            </span>
                            <div class="row overflow-auto">
                                <div class="col-sm-12">
                                    <table id="example1"
                                        class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;">
                                                    @lang('messages.ID')
                                                </th>
                                                <th style="text-align: center;">
                                                    @lang('messages.UserName')
                                                </th>
                                                <th style="text-align: center;">
                                                    @lang('messages.UserGroups')
                                                </th>
                                                <th style="text-align: center;">
                                                    @lang('messages.Code')
                                                </th>
                                                <th style="text-align: center;">
                                                    @lang('messages.Action')
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($userList as $key => $value)
                                                <tr>
                                                    <td>{{ ++$key }}</td>
                                                    <td>{{ $value }}</td>
                                                    <td>
                                                        @if (Arr::get($userGroupsArr, $value))
                                                            @foreach (Arr::get($userGroupsArr, $value) as $userGroups)
                                                                <span>{{ Arr::first(explode(':', $userGroups)) }}</span><br>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td>{{ $fcode }}</td>
                                                    <td>
                                                        <div class="dropdown dropleft">
                                                            <button class="btn btn-info text-white btn-sm dropdown-toggle"
                                                                type="button" id="dropdownMenuButton{{ $key }}"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">

                                                                @lang('messages.Action')
                                                            </button>
                                                            <div class="dropdown-menu"
                                                                aria-labelledby="dropdownMenuButton{{ $key }}">
                                                                <a class="btn btn-sm btn-success text-white dropdown-item mb-1"
                                                                    href="{{ URL::to('vdmUsers/' . $value) }}">
                                                                    @lang('messages.ViewUser')
                                                                </a>

                                                                <a class="btn btn-sm btn-info text-white dropdown-item mb-1"
                                                                    href="{{ URL::to('vdmUsers/' . $value . '/edit') }}">
                                                                    @lang('messages.EditUser')
                                                                </a>
                                                                <a class="btn btn-sm btn-success text-white dropdown-item mb-1"
                                                                    href="{{ URL::to('vdmUsers/notification/' . $value) }}">
                                                                    @lang('messages.EditNotification')
                                                                </a>
                                                                <a class="btn btn-sm btn-info text-white dropdown-item mb-1"
                                                                    href="{{ URL::to('vdmUsers/reports/' . $value) }}">
                                                                    @lang('messages.EditReports')
                                                                </a>
                                                                <a class="btn btn-sm btn-warning text-white dropdown-item mb-1"
                                                                    href="{{route('audit',['type'=>'User','name'=>$value ])}}" id="auditReport"
                                                                    onClick="setGroup('{{ $value }}',this);">
                                                                    @lang('messages.AuditReport')
                                                                </a>
                                                                @if (session()->get('userSwithDmn') != null)
                                                                    <a class="btn btn-sm btn-warning text-white dropdown-item mb-1"
                                                                        target="_blank"
                                                                        href="http://{{ session()->get('userSwithDmn') }}/gps/public/switchLogin/{{ $value }}">{{ trans('Switch Login') }}</a>
                                                                @endif
                                                                {{ Form::open(['url' => 'vdmUsers/' . $value, 'class' => '', 'id' => 'deleteForm' . $key]) }}
                                                                {{ Form::hidden('_method', 'DELETE') }}
                                                                {{ Form::button(__('messages.DeleteUser'), ['class' => 'btn btn-danger btn-sm text-white dropdown-item deleteBtn', 'id' => $key, 'formType' => 'User']) }}
                                                                {{ Form::close() }}
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('js')

    <script>
        function setGroup(user, aTag) {
            var encodedString = (Intl.DateTimeFormat().resolvedOptions().timeZone);
            var time1 = window.btoa(encodedString);
            // $(aTag).attr('href', 'auditReport/AuditUser&' + time1 + '&' + user);
        }
    </script>

@endpush
