@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            <div class="hpanel">
                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.AddUser')
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('vdmUsers.index') }}">
                            @lang('messages.UserList')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.AddUser')                                
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="page-body">
                    <div class="card">
                        <div class="card-body">
                            <span class="text-danger error-message">{{ HTML::ul($errors->all()) }}</span>
                            {{ Form::open(['url' => 'vdmUsers', 'id' => 'userForm']) }}
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('userId', __('messages.UserName')) }}<span class="rq"></span>
                                        <i class="fa fa-info-circle fa-lg text-warning ml-auto" data-toggle="tooltip" data-placement="right" title="User name is case sensitive and space is not allowed"></i> <br>
                                        {{ Form::text('userId', $userName, ['class' => 'form-control caps', 'placeholder' => 'User Name', 'required' => 'required', 'id' => 'userID']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('email', __('messages.Email')) }}<span class="rq"></span>
                                        {{ Form::Email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('cc_email', __('messages.AlternateEmail')) }}
                                        <input type="email" id="cc_email" class="form-control" name="cc_email" multiple
                                            placeholder="Alternate Email">
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('companyName', __('messages.CompanyName')) }}
                                        {{ Form::text('companyName', old('companyName'), ['class' => 'form-control', 'placeholder' => 'Company Name']) }}
                                    </div>
                                    <div class="form-group">
                                        <h4 class="text-primary ml-1">
                                            @lang('messages.UserMode') :
                                        </h4>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        {{ Form::checkbox('assetuser', 'value', false, ['class' => 'form-check-input field check1']) }}
                                                        <span class="form-check-sign">
                                                            @lang('messages.AssetUser')
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        {{ Form::checkbox('virtualaccount', 'value', true, ['class' => 'form-check-input field check1']) }}
                                                        <span class="form-check-sign">
                                                            @lang('messages.VirtualAccount')
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('mobileNo', __('messages.MobileNumber')) }}<span class="rq"></span>
                                        {{ Form::tel('mobileNo', old('mobileNo'), ['class' => 'form-control', 'placeholder' => 'Mobile Number', 'required' => 'required', 'id' => 'mobileNo','minlength'=>'8','maxlength'=>'15']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('password', __('messages.Password')) }}<span class="rq"></span>
                                        {{ Form::text('password', old('password'), ['class' => 'form-control passhover', 'placeholder' => 'Password', 'required' => 'required', 'id' => 'userpass','minlength'=>'6']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('zoho', __('messages.Zoho')) }}<span class="rq"></span>
                                        {{ Form::text('zoho', old('zoho'), ['class' => 'form-control', 'placeholder' => __('messages.Zoho'), 'required' => 'required']) }}
                                    </div>
                                   
                                    <div class="form-group">
                                        {{ Form::label('enable', __('messages.EnableDebugs')) }}
                                        {{ Form::select('enable', ['Disable' => 'Disable', 'Enable' => 'Enable'], null, ['class' => 'form-control', 'id' => 'enable', 'required' => 'required']) }}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class=" my-2 text-center">
                                        {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary']) }}
                                    </div>

                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-12 d-flex">
                                    <div class="col-6 d-flex align-items-center form-check">
                                        <label class="form-check-label">
                                            {{ Form::checkbox('$vehicleGroups', 'value', false, ['class' => 'form-check-input field check']) }}
                                            <span class="form-check-sign">
                                                @lang('messages.SelectAllGroups')
                                            </span>
                                        </label>
                                    </div>
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input type="text" placeholder="Search..." id="search" name="searchtext" class="form-control searchkey">
                                            <div class="input-group-prepend">
                                                <div class="btn btn-primary rounded-right" aria-label="search">
                                                    <i class="fa fa-search search-icon"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row p-2">
                                <h5 class="col-12">
                                    {{ Form::label('vehicleGroups', __('messages.SelecttheGroups').':', ['class' => 'text-primary font-weight-bold']) }}
                                </h5>
                            </div>
                            <div class="row">
                                <div class="col-md-12 boarder-bottom" id="selectedItems"></div>
                                <div class="col-md-12" id="unSelectedItems">
                                    <div class="row px-3">
                                        @if (isset($vehicleGroups))
                                            @foreach ($vehicleGroups as $key => $value)
                                                <div class="col-md-3 col-6 form-check vehiclelist">
                                                    <label class="form-check-label d-inline-block text-truncate w-100">
                                                        {{ Form::checkbox('vehicleGroups[]', $key, null, ['class' => 'field form-check-input', 'id' => 'questionCheckBox']) }}
                                                        <span class="form-check-sign">{{ Arr::first(explode(":",$value)) }}</span>
                                                    </label>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::hidden('value', json_encode($vehicleGroups, true), ['id' => 'value']) }}
    </div>
@endsection
@push('js')

    <script type="text/javascript">

        list = [];
        // var value = {{ json_encode($vehicleGroups, true) }}
        var value = $('#value').val();

        function usercheck() {
            $('#validation').text('');
            var postValue = {
                'id': $('#userID').val(),
                "_token": '{{ csrf_token() }}'
            };
            $("#userID").removeClass('is-invalid');
            $("#userID").siblings('.aftererr').remove()
            $.post('{{ route('ajax.userIdCheck') }}', postValue)
                .done(function({data}) {
                    if (data && data.trim() != '') {
                        $("#userID").aftererr(postValue['id'] + ' ' + data);
                        return false;
                    }
                }).fail(function() {
                    console.log("fail");
                });

        }
        $('#userID').on('change', function() {
            usercheck();
        });

        $('#mobileNo').on('change', function() {
            usercheck();
        });

        $('#userForm').submit(function(){
            $('.aftererr').remove();
            $('input').removeClass('is-invalid')

            let passwd = $('input[name="password"]');
            if(passwd.val() <= 5){
                passwd.focus().aftererr('Please, Enter minimum 6 characters or more');
                return false
            }
            let mobileNo = $('#mobileNo')
            if(mobileNo.val().length <= 7){
                mobileNo.focus().aftererr('Please, Enter valid number');
                return false
            }
            return true
        });


        function validate(element) {
            var word = element.value
            var new1 = word.replace(/[\'\/~`\!@#\$%\^&\*\(\)\\\+=\{\}\[\]\|;:"\<\>,\?\\\']/, "")
            element.value = new1
        }

    </script>
    @include('includes.js_footer')
    @include('includes.js_create')

@endpush
