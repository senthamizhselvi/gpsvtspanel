@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animate-panel">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.EditUser')
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('vdmUsers.index') }}">
                                @lang('messages.UserList')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.EditUser')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">{{ $userId }}</a>
                        </li>
                    </ul>
                </div>
                <div class="card animated zoomIn">
                    <div class="card-body">
                        <span class="text-danger error-message"> {{ HTML::ul($errors->all()) }}</span>
                        {{ Form::model($userId, ['route' => ['vdmUsers.update', $userId], 'method' => 'PUT', 'id' => 'userForm']) }}
                        <div class="row">
                            <div class="ml-auto mr-4">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('userId', __('messages.UserName')) }}<span
                                        class="rq"></span>
                                    {{ Form::label('userId', $userId, ['class' => 'form-control m-0']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('email', __('messages.Email')) }}<span class="rq" ></span>
                                    {{ Form::Email('email', $email, ['class' => 'form-control', 'placeholder' => 'Email','required'=>'required']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('companyName', __('messages.CompanyName')) }}
                                    {{ Form::text('companyName', $companyName, ['class' => 'form-control', 'placeholder' => 'Company Name', 'onkeyup' => 'validate(this)']) }}
                                </div>
                                <h4 class="text-primary ml-2 p-2">{{ __('messages.UserMode') }} :</h4>
                                <div class="row p-2">
                                    <div class="col-6 px-0 px-md-2">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                {{ Form::checkbox('assetuser', 'value', $assetuser, ['class' => 'form-check-input']) }}
                                                <span class="form-check-sign">
                                                    @lang('messages.AssetUser')
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-6 px-0 px-md-2">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                {{ Form::checkbox('virtualaccount', 'value', $value, ['class' => 'form-check-input']) }}
                                                <span class="form-check-sign">
                                                    @lang('messages.VirtualAccount')
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('mobileNo', __('messages.MobileNumber')) }}<span
                                        class="rq"></span>
                                    {{ Form::number('mobileNo', $mobileNo, ['class' => 'form-control', 'placeholder' => 'Mobile Number','id'=>'mobileNo', 'minlength' => '8', 'maxlength' => '15','required'=>'required']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('cc_emails', __('messages.AlternateEmail')) }}
                                    {{ Form::Email('cc_email', $cc_email, ['class' => 'form-control', 'placeholder' => 'Alternate Email', 'id' => 'cc_email']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('zoho', __('messages.Zoho')) }} <span class="rq"></span>
                                    {{ Form::text('zoho', $zoho, ['class' => 'form-control', 'placeholder' => __('messages.Zoho'),'required'=>'required']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('enable', __('messages.EnableDebugs')) }}
                                    {{ Form::select('enable', ['Disable' => 'Disable', 'Enable' => 'Enable'], $enable, ['class' => 'form-control', 'id' => 'enable', 'required' => 'required']) }}
                                </div>
                                <br>
                            </div>
                            <div class="col-md-12 text-center mt-4">
                                {{ Form::submit(__('messages.UpdateUser'), ['class' => 'btn btn-primary btn-md']) }}
                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-12 ">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                {{ Form::checkbox('group', 'value', false, ['class' => 'form-check-input field check']) }}
                                                <span class="form-check-sign">
                                                    @lang('messages.SelectAllGroups')
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input type="text" placeholder="Search..." id="search" name="searchtext"
                                                class="form-control searchkey">
                                            <div class="input-group-prepend">
                                                <div class="btn btn-search btn-primary rounded-right" aria-label="search">
                                                    <i class="fa fa-search search-icon"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="col-12">
                            <h3 id="SelectedGroups">
                                {{ Form::label('vehicleList', __('messages.SelectedGroups') . ':', ['class' => 'mt-2 text-primary']) }}
                            </h3>
                            <div class="row mb-2 " id="selectedItems">

                            </div>
                        </div>
                        <div class="col-md-12" id="unSelectedItems">
                            <h3 id="SelecttheGroups">
                                {{ Form::label('vehicleList', __('messages.SelecttheGroups') . ':', ['class' => 'mt-2 text-primary']) }}
                            </h3>

                            <div class="row SelecttheGroups px-3">
                                @if (isset($vehicleGroups))
                                    @foreach ($vehicleGroups as $key => $value)
                                        <div class="col-md-3 form-check vehiclelist">
                                            <label class="form-check-label d-inline-block text-truncate w-100">
                                                {{ Form::checkbox('vehicleGroups[]', $key, in_array($value, $selectedGroups), ['class' => 'field form-check-input', 'id' => 'questionCheckBox']) }}
                                                <span class="form-check-sign" data-toggle="tooltip" data-placement="top"
                                                    title="{{ Arr::first(explode(':', $value)) }}">{{ Arr::first(explode(':', $value)) }}</span>
                                            </label>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            {{ Form::hidden('value', json_encode($vehicleGroups, true)) }}
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        list = [];
        var value = $('input[name="value"]').val();
        $(window).bind("load", function() {
            if ($('.SelecttheGroups>.vehiclelist').length == 0) {
                $('#SelecttheGroups').fadeOut();
            }
        })

        $('#userForm').submit(function() {
            $('.aftererr').remove();
            $('input').removeClass('is-invalid')

            let mobileNo = $('#mobileNo')
            if (mobileNo.val().length <= 7) {
                mobileNo.focus().aftererr('Please, Enter valid number');
                return false
            }
            return true
        });
    </script>
    @include('includes.js_footer')
@endpush
