@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            <div class="page-header">
                <div class="page-title">
                    @lang('messages.EditReport')
                </div>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    @if (Request::is('*vdmDealers*'))
                        <li class="nav-item">
                            <a href="{{ route('vdmDealers.index') }}">
                                @lang('messages.DealerList')
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a href="{{ route('vdmUsers.index') }}">
                                @lang('messages.UserList')
                            </a>
                        </li>
                    @endif
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                    <li class="nav-item">
                        <a href="#">
                            @lang('messages.EditReport')
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                            {{ $userId }}
                        </a>
                    </li>
                </ul>
            </div>
            <div class="page-body">
                {{ Form::open(['url' => 'vdmUsers/updateReports']) }}
                <div class="card">
                    {{ Form::hidden('userId', $userId) }}
                    <div>
                        <button class="btn btn-outline-success btn-sm float mt-3" type="button" id="select">
                            @lang('messages.SelectAll')
                        </button>
                        <button class="btn btn-outline-danger btn-sm float mt-3" type="button" id="unselect">
                            @lang('messages.UnselectAll')
                        </button>
                        {{ Form::submit(__('messages.UpdateUser'), ['class' => 'btn btn-primary btn-sm float']) }}
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach ($totalList as $keys => $reportName)
                                <br>
                                <div class="col-md-12">
                                    <div class="d-flex">
                                        <h3 class="text-primary">
                                            {{ $keys }}
                                        </h3>
                                        <button type="button" class="btn btn-primary btn-sm ml-auto"
                                            onclick="toggle('{{ $keys }}',true)">
                                            <span class="fa fa-check"></span>
                                        </button>
                                        <button type="button" class="btn btn-danger btn-sm ml-3"
                                            onclick="toggle('{{ $keys }}',false)">
                                            <span class="fa fa-times"></span>
                                        </button>
                                    </div>
                                    <hr>
                                    <div class="row p-2">
                                        @foreach ($reportName as $value)
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        {{ Form::checkbox('reportName[]', $value, in_array($value, $userReports), ['class' => "$keys  checkbox form-check-input"]) }}
                                                        <span
                                                            class="form-check-sign">{{ head(explode(':', $value)) }}</span>
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <br />
                                </div>

                            @endforeach
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
@push('css')
    <style>
        .float {
            position: fixed;
            width: 100px;
            height: 40px;
            bottom: 1.5rem;
            right: 1.5rem;
            border-radius: 50px;
            text-align: center;
            z-index: 9999;
            box-shadow: 2px 2px 3px #999;
            background: #fff;
        }

        
        #select {
            margin-right: 14rem !important;
        }

        #unselect {
            margin-right: 7rem !important;
        }

    </style>
@endpush
@push('js')
    <script>
        function toggle(val, fn) {
            var className = '.' + val;
            $(className).prop('checked', fn);
        }
        $('#select').click(() => toggle('checkbox', true))
        $('#unselect').click(() => toggle('checkbox', false))
    </script>
@endpush
