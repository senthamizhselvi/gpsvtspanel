@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animate-panel">
            <div class="hpanel">
                <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">
                            @lang('messages.ViewUser')
                        </h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="#">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('vdmUsers.index') }}">
                                @lang('messages.UserList')
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">
                                    @lang('messages.ViewUser')
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="page-body">
                        <div class="card animated zoomIn">
                            <div class="card-body">
                                <p class="h3 text-primary">
                                   @lang('messages.UserDetails')
                                </p>
                                <hr>
                                <div class="row">
                                    <div class="col-md-2">
                                        {{ Form::label('userName', __('messages.UserName')) }}
                                    </div>
                                    <div class="col-md-4">
                                        {{ Form::label('userName', $userId, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-2">
                                        {{ Form::label('mobileNo', __('messages.MobileNo')) }}
                                    </div>
                                    <div class="col-md-4">
                                        {{ Form::label('mobileNo', $mobileNo, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-2">
                                        {{ Form::label('Groupsname', __('messages.GroupName')) }}
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row" id='show'>
                                            @foreach ($vehicleGroups as $vehicleGroup)
                                                <div class='col-md-3 py-1'>
                                                    <i class="fas fa-arrow-circle-right text-primary"></i>
                                                    {{ Arr::first(explode(':', $vehicleGroup)) }}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
