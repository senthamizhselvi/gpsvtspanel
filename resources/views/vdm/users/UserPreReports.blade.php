@extends('includes.vdmheader')
@section('mainContent')
    <div id="wrapper" onload="hideButton();">

        <div class="container-fluid">

            <div class="hpanel">



                <div class="navbar navbar-inverse navbar-fixed-top">
                    {{ Form::open(['url' => 'updateUserPre']) }}
                    <div class="row">

                        <div class=" heading col">{{ Form::label('userId', 'UserId :') }}
                            {{ Form::label('userId', $userId) }}
                            {{ Form::hidden('userId', $userId) }}</div>
                        <div class="PlaceButton col"><a href="{{ URL::to('logout') }}" class="glyphicon glyphicon-log-out"
                                style="padding: 10px 15px;color: #fff; text-align: center;"></a></div>
                        <div class="PlaceButton col"><a class="btn btn-small btn-info"
                                href="{{ URL::to('track') }}">Back</a></div>
                        <div class="PlaceButton col">{{ Form::submit('Save', ['class' => 'btn btn-success']) }}</div>
                    </div>


                </div>
                <div class="row" style="margin-top: 57px;">
                    <p style="font-style: oblique;font-family: cursive;margin-left: 1%;font-size: larger;">Choose your
                        preferred landing page and see what exactly you wish to see first when you login to your account,
                        Dashboard by default.</p>
                </div>

                <div class="row">
                    @if (session()->has('success'))
                        <div clasclass="alert {{ session()->get('alert-class', 'alert-sucess') }}"
                            style="margin-top: 0px;margin-bottom: -50;">
                            <strong
                                style="color: darkgreen;font-size: medium;font-style: oblique;margin-left: 1%;">{{ session()->get('success') }}</strong>
                        </div>

                    @endif
                    <?php $rowCount = 0; ?>
                    @foreach ($totalList as $keys => $reportName)
                        <div class="col-sm-4" style="margin-top: 2%; ">
                            <div class="reportHeading row" style="padding: 2% 1%;margin: 0 1%;">
                                <!-- <p class="btn btn-success col" onclick = "toggle('{{ $keys }}',1)"> <span  onclick = "toggle('{{ $keys }}',1)" class="glyphicon glyphicon-ok"></span></p> 
           <p class="btn btn-danger col" onclick="toggle('{{ $keys }}',2)" ><span class="glyphicon glyphicon-remove">	</p> -->
                                &nbsp;&nbsp;
                                <span class="headingFont col"
                                    style="padding-top: 5px;padding-left: 28px;">{{ Form::label('ReportName ', $keys) }}</span>
                            </div>
                            <div class="card card-cascade" style="margin-left: 34px;">
                                <label>
                                    @if ($keys == 'Tracking')
                                        @if ($userPrePage == '')
                                            <input type='radio' name='userPrePage' value="" checked>
                                        @endif
                                        @if ($userPrePage != '')
                                            <input type='radio' name='userPrePage' value="">
                                        @endif
                                        <b><span class="basicFont">Track</span></b>
                                </label>

                    @endif
                </div>
                @foreach ($reportName as $value)
                    <div class="card card-cascade" style="margin-left: 34px;">


                        <label>
                            @if ($value == $userPrePage)
                                <input type='radio' name='userPrePage' value="<?php echo $value; ?>" checked>
                            @endif
                            @if ($value != $userPrePage)
                                <input type='radio' name='userPrePage' value="<?php echo $value; ?>">
                            @endif
                            </span>
                            <b><span class="basicFont"><?php echo explode(':', $value)[0]; ?></span></b>
                        </label>
                    </div>
                @endforeach
                <br />

            </div>
            <?php $rowCount++; ?>
            @if ($rowCount % 3 == 0)
        </div>
        <div class="row">
            @endif

            @endforeach

        </div>



    </div>
    {{ Form::close() }}
    </div>
    <!-- <div class="reportHeading row"   style="padding: 9px 1% 5px;margin: -17.75% 35%;margin-right: 3%;margin-left: 67%;">
           <p class="btn btn-success col" onclick = "toggle('{{ $keys }}',1)"> <span  onclick = "toggle('{{ $keys }}',1)" class="glyphicon glyphicon-ok"></span></p> 
           <p class="btn btn-danger col" onclick="toggle('{{ $keys }}',2)" ><span class="glyphicon glyphicon-remove">	</p>
           &nbsp;&nbsp;
          <span class="headingFont col" style="padding-top: 5px;padding-left: 28px;">{{ Form::label('Default Page') }}</span>
          </div>
          <div class="card card-cascade"  style="margin-left: 70%;margin-top: 18%;" >
          <label><input type='radio' name='userPrePage' value="Track" ><b><span class="basicFont">&nbsp;Track</span></b></label>
          </div> -->
    </div>
    </div>

    <script language="JavaScript">
        // function toggle(val,fn) {

        // 	// alert(fn);

        //     var defult =`.`;
        //     var className = defult+val;
        //     if (fn == 1){


        //         $(className).prop('checked', true); 

        //     }
        //     if (fn == 2){
        //         $(className).attr('checked', false); 
        //     }

        //     if (fn == 3)
        //     {
        //     	$("input[id='questionCheckBox']:checkbox").prop('checked',true);
        //     	// document.getElementById('togglee').style.visibility = 'hidden';
        //     	 document.getElementById('toggleeOff').style.visibility = '';

        //     }
        //      if (fn == 4)
        //     {
        //     	$("input[id='questionCheckBox']:checkbox").prop('checked',false);
        //     	// document.getElementById('toggleeOff').style.visibility = 'hidden';
        //     	 document.getElementById('togglee').style.visibility = '';

        //     }
        //  // document.getElementById('questionCheckBox').checked = true;
        //     console.log("Hello "+className);
        // }
    </script>

    <style type="text/css">
        .heading,
        .UserId {

            float: left;
            margin-left: 1%;
            margin-top: 10px;
            font-size: 18px;
            color: white;
            font-family: 'Quicksand', sans-serif;
        }

        .navbar {

            min-height: 40px !important;
        }

        .PlaceButton {

            margin-top: 10px;
            font-family: 'Quicksand', sans-serif;
            font-size: 15px;
            float: right;
            margin-right: 2%;

        }

        .headingFont {

            font-family: 'Quicksand', sans-serif;
            font-size: 13px;
        }

        .basicFont {

            font-family: 'Quicksand', sans-serif;
            font-size: 13px;
            font-weight: 500;
        }

        @media (min-width: 768px) {
            .navbar {
                border-radius: 4px;
                margin-right: 19%;
                height: 48px;
                width: 100%;
                padding-left: 10px;
            }
        }

        .reportHeading {

            background-color: #d3d3d3;
            border-radius: 5px;
            color: black;
            padding-left: 10px;
            border-color: black;
            border-style: solid;
            border-width: 2px;
        }

    </style>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
