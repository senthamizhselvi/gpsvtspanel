@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            <div class="hpanel">
                <div class="pull-right">
                    <a href="{{ route('vdmuser/sendExcel') }}"><img class="pull-right" width="40px" height="50px"
                            src="{{ asset('assets/imgs/xls.svg') }}" method="get" /></a>
                </div>

                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.UserSearch')
                    </h4>
                </div>
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}</p>
                @endif
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            {{ Form::open(['url' => 'vdmUserScan/user', 'method' => 'post']) }}
                            <div class="row p-2">
                                <div class="col-md-6 form-group">
                                    {{ Form::label('User Search') }}
                                    <i class="fa fa-info-circle fa-lg text-warning ml-auto" data-toggle="tooltip"
                                        data-placement="right" title=" Use * for getting all Users in the search"></i> <br>
                                    {{ Form::text('text_word', request()->old('text_word'), ['class' => 'form-control', 'placeholder' => 'Search User']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::submit('Search', ['class' => 'btn btn-primary mt-0 mt-md-4']) }}
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                        <div id="tabNew" class="card-body overflow-auto">
                            <table id="example1"
                                class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th>
                                            @lang('messages.ID')
                                        </th>
                                        <th>
                                            @lang('messages.Username')
                                        </th>
                                        <th>
                                            @lang('messages.UserGroups')
                                        </th>
                                        <th>
                                            @lang('messages.Code')
                                        </th>
                                        <th>
                                            @lang('messages.Action')
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($userList as $key => $value)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $value }}</td>
                                            <td>
                                                @foreach (Arr::get($userGroupsArr, $value) as $userGroups)
                                                    <span>{{ $userGroups }}</span><br />
                                                @endforeach
                                            </td>
                                            <td>{{ $fcode }}</td>
                                            <td style=" ">
                                                <div class="dropdown dropleft">
                                                    <button class="btn btn-info text-white btn-sm dropdown-toggle"
                                                        type="button" id="dropdownMenuButton{{ $key }}"
                                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        @lang('messages.Action')
                                                    </button>
                                                    <div class="dropdown-menu"
                                                        aria-labelledby="dropdownMenuButton{{ $key }}">

                                                        <a class="btn btn-sm btn-success dropdown-item mb-1 text-white"
                                                            href="{{ URL::to('vdmUsers/' . $value) }}">
                                                        @lang('messages.ViewUser')
                                                        </a>

                                                        <a class="btn btn-sm btn-info dropdown-item mb-1 text-white"
                                                            href="{{ URL::to('vdmUsers/' . $value . '/edit') }}">
                                                        @lang('messages.EditUser')
                                                        </a>
                                                        <a class="btn btn-sm btn-success dropdown-item mb-1 text-white"
                                                            href="{{ URL::to('vdmUsers/notification/' . $value) }}">
                                                        @lang('messages.EditNotification')
                                                        </a>
                                                        <a class="btn btn-sm btn-info dropdown-item mb-1 text-white"
                                                            href="{{ URL::to('vdmUsers/reports/' . $value) }}">
                                                        @lang('messages.EditReports')
                                                        </a>
                                                        <a class="btn btn-sm btn-warning dropdown-item mb-1 text-white"
                                                            href="" id="auditReport"
                                                            onClick="setGroup('{{$value}}');">
                                                        @lang('messages.AuditReport')
                                                        </a>
                                                        {{ Form::open(['url' => 'vdmUsers/' . $value, 'class' => '', 'id' => 'deleteForm' . $key]) }}
                                                        {{ Form::hidden('_method', 'DELETE') }}
                                                        {{ Form::button(__('messages.DeleteUser'), ['class' => 'btn btn-danger btn-sm text-white dropdown-item deleteBtn', 'id' => $key, 'formType' => 'User']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- @include('includes.js_create') --}}
@endsection
@push('js')
    <script>
        var encodedString = (Intl.DateTimeFormat().resolvedOptions().timeZone);
        var time1 = window.btoa(encodedString);

        function setGroup(user) {
            $('#auditReport').attr('href', '../auditReport/AuditUser&' + time1 + '&' + user);
        }
    </script>
@endpush
