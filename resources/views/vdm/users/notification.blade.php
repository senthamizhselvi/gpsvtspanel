@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animate-panel">
            <div class="page-inner">
                <div class="hpanel">
                    <div class="page-header">
                        <h4 class="page-title">
                            @lang('messages.EditNotification')
                        </h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="#">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('vdmUsers.index') }}">
                                    @lang('messages.UserList')
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">
                                    @lang('messages.EditNotification')
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">
                                    {{$userId}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="page-body">
                        <span class="text-danger error-message">{{ HTML::ul($errors->all()) }}</span>
                        {{ Form::open(['url' => 'vdmUsers/updateNotification']) }}
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-3 form-group ml-0 ml-md-3">
                                        {{ Form::label('orgId1', __('messages.UserId'), ['class' => 'b  mt-2']) }} :  
                                        {{ Form::label('userId', $userId,['class'=>'text-primary']) }}
                                        {{ Form::hidden('userId', $userId, ['class' => 'form-control']) }}
                                    </div>
                                    <div class="col-md-3 form-group ml-auto">
                                        {{ Form::submit(__('messages.UpdateUser'), ['class' => 'btn btn-primary  mr-3']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row mx-0 mx-md-0">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 form-check">
                                                <label class="form-check-label">
                                                    {{ Form::checkbox('group', 'value', false, ['class' => 'form-check-input field check']) }}
                                                    <span class="form-check-sign">
                                                        @lang('messages.SelectAllNotification')
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="text" placeholder="Search..." id="search" name="searchtext"
                                                        class="form-control searchkey">
                                                    <div class="input-group-prepend">
                                                        <div class="btn btn-search btn-primary rounded-right"
                                                            aria-label="search">
                                                            <i class="fa fa-search search-icon"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <h4 id="SelectedNotification">
                                        {{ Form::label('vehicleList', __('messages.SelectedNotification') . ' :', ['class' => 'text-primary']) }}
                                    </h4>
                                </div>
                                <div class="row border-bottom my-2  m-1 SelectedNotification" id="selectedItems">
                                </div>
                                <div class="col-md-12">
                                    <h4 id="SelecttheNotification">
                                        {{ Form::label('vehicleList', __('messages.SelecttheNotification') . ' :', ['class' => 'mt-2 text-primary']) }}
                                    </h4>
                                </div>
                                <div class="col-md-12 my-2" id="unSelectedItems">
                                    <div class="row SelecttheNotification">
                                        @if (isset($notificationGroups))
                                            @foreach ($notificationGroups as $key => $value)
                                                <div class="col-md-3 form-check vehiclelist">
                                                    <label class="form-check-label">
                                                        {{ Form::checkbox('notificationGroups[]', $key, in_array($value, $notificationArray), ['class' => 'field form-check-input', 'id' => 'questionCheckBox']) }}
                                                        <span class="form-check-sign">{{ $value }}</span>
                                                    </label>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::hidden('value', json_encode($notificationGroups, true)) }}
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        list = [];
        var value = $('input[name="value"]').val();
        $(window).bind("load", function() {
            if ($('.SelecttheNotification>.vehiclelist').length == 0) {
                $('#SelecttheNotification').hide();
            }
            if ($('.SelectedNotification>.vehiclelist').length == 0) {
                $('#SelectedNotification').hide();
            }

        })
    </script>
    @include('includes.js_footer')
@endpush
