@extends('includes.vdmheader')
@section('mainContent')
<h1>Add Place of Interest/GeoFence</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'vdmGeoFence')) }}
<div class="row">
    <div class="col-md-4">   
    <div class="form-group">
        {{ Form::label('vehicleId', 'Vehicle Id') }}
        {{ Form::select('vehicleId', $vehicleList,request()->old('vehicleId'),array('class' => 'form-control')) }}
    </div>   
     
       
    <div class="form-group">
        {{ Form::label('geoFenceId', 'GeoFence Id :')  }}
        {{ Form::text('geoFenceId', request()->old('geoFenceId'), array('class' => 'form-control')) }}

    </div>
   <div class="form-group">
        {{ Form::label('poiName', 'Place of Interest') }}
        {{ Form::text('poiName', request()->old('[poiName'), array('class' => 'form-control')) }}

    </div>
    <div class="form-group">
        {{ Form::label('mobileNos', 'mobile Numbers') }}
        {{ Form::textarea('mobileNos', request()->old('mobileNos'), array('class' => 'form-control')) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ Form::text('email', request()->old('email'), array('class' => 'form-control')) }}
    </div>
    
    
    </div>
    <div class="col-md-4">
    
    <div class="form-group">
        {{ Form::label('direction', 'Direction') }}
       {{ Form::select('direction', array('pickup' => 'pickup', 'drop' => 'drop', 'NA'=>'Not Applicable'), request()->old('direction'), array('class' => 'form-control')) }}             


    </div>
    <div class="form-group">
        {{ Form::label('geoLocation', 'Geo Location') }}
        {{ Form::text('geoLocation', request()->old('geoLocation'), array('class' => 'form-control')) }}

    </div>
    <div class="form-group">
        {{ Form::label('geoAddress', 'Geo Address') }}
        {{ Form::text('geoAddress', request()->old('geoAddress'), array('class' => 'form-control')) }}

    </div>
    <div class="form-group">
        {{ Form::label('proximityLevel', 'Proximity Level') }}
        {{ Form::select('proximityLevel', array( '10' => '10 m','20' => '20 m','50' => '50 m', '100' => '100 m','200' => '200 m','300' => '300 m',  '1000' => '1 km', '5000' => '5 km', '10000' => '10 km', '25000' => '25 km','50000' => '50 km','100000' => '100 km'), request()->old('proximityLevel'), array('class' => 'form-control')) }}             
    </div>
    <div class="form-group">
        {{ Form::label('geoFenceType', 'Geo Fence Type') }}
        <br/>
        {{ Form::select('geoFenceType', array('school' => 'School', 'trip' => 'Trip', 'exit' => 'On Exit'), request()->old('geoFenceType'),array('class' => 'form-control')) }}
        
    </div>
    <div class="form-group">
        {{ Form::label('message', 'Message') }}
        {{ Form::text('message', request()->old('message'), array('class' => 'form-control')) }}
            
    </div>
 </div>
  </div>

    {{ Form::submit('Create the GeoFence/POI!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@stop
