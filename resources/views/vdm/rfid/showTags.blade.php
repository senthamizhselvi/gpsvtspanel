<div class="modal-content">
    {{ Form::open(['url' => 'rfid/index1']) }}
    <div class="modal-header">
        <h4>
            @lang('messages.ShowTags')
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
        <br>
        <div class="row">
            <div class="col-md-4">
             @lang('messages.SelectOrganizationName')   :
            </div>
            <div class="col-md-4">
                {{ Form::select('org', $orgList, old('orgname'), ['id' => 'orgid', 'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search' => 'true']) }}
            </div>
        </div>
        <br>
    </div>
    <div class="modal-footer justify-content-center">
        {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary btn-sm']) }}
    </div>
    {{ Form::close() }}
</div>