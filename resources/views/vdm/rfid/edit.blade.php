@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="page-inner">
            <div class="hpanel">
                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.EditTags')
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{route("showTag",['orgId'=>$orgname])}}">
                            @lang('messages.ViewTags')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.EditTags')
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="page-body">
                    <div class="card">
                        <div class="card-body">
                            <span class="text-danger error-message">
                                {{ HTML::ul($errors->all()) }}
                            </span>
                            {{ Form::open(['url' => 'rfid/update']) }}
                            <div class="row p-3">
                                <div class="col-md-6 form-group">
                                    {{ Form::label('tagid', __('messages.TagId')) }}
                                    {{ Form::text('tagid', $tagid, ['class' => 'form-control','required'=>'required']) }}
                                    {{ Form::hidden('tagidtemp', $tagid, ['class' => 'form-control']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('tagname', __('messages.TagName')) }}
                                    {{ Form::text('tagname', $tagname, ['class' => 'form-control']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('mobile', __('messages.Mobilenumber')) }}
                                    {{ Form::number('mobile', $mobile, ['class' => 'form-control rfid','required'=>'required']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('org', __('messages.OrgName')) }}
                                    {{ Form::select('org', $orgList, $orgname, ['class' => 'selectpicker form-control', 'data-live-search ' => 'true']) }}
                                    {{ Form::hidden('orgT', $orgname, ['id' => 'orgid']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('designation', __('messages.Designation')) }}
                                    {{ Form::select('designation', ['' => 'Select', 'Driver' => 'Driver', 'Helper' => 'Helper', 'Staff' => 'Staff', 'Student' => 'Student'], $designation, ['class' => 'form-control']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('employeeId', __('messages.EmployeeId')) }}
                                    {{ Form::text('employeeId', $employeeId, ['class' => 'form-control']) }}
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    {{ Form::label('department', __('messages.Department')) }}
                                    {{ Form::text('department', $department, ['class' => 'form-control']) }}
                                </div>
                                
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12 mx-auto text-center">
                                    <a href="{{route('showTag',['orgId'=>"$orgname"])}}" class="btn btn-danger">
                                        @lang('messages.cancel')
                                    </a>
                                    {{ Form::submit(__('messages.Update'), ['class' => 'btn btn-primary']) }}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
