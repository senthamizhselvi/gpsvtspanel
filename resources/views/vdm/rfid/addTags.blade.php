@extends("layouts.app")
@section('content')
    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="content page-inner">
            <div class="hpanel">
                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.AddTags')
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.AddTags')
                            </a>
                        </li>
                    </ul>
                </div>
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </p>
                @endif
                <div class="page-body">
                    <div class="card animated zoomIn">
                        <span class="text-danger error-message">
                            {{ HTML::ul($errors->all()) }}
                        </span>
                        {{ Form::open(['method' => 'post', 'id' => 'submit']) }}
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-3 font-weight-bold d-flex align-items-center">
                                    @lang('messages.SelectOrganisationName') :
                                </div>
                                <div class="col-md-3 select2-input mt-2">
                                    {{ Form::select('org', $orgList, old('orgname'), ['id' => 'orgid', 'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search' => 'true']) }}
                                </div><br>
                                {{ Form::hidden('numberoftags', $tags, ['class' => 'form-control']) }}
                                {{ Form::hidden('tags', $tags) }}
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 mx-auto" id="m">
                                    <div class="row text-center">
                                        <div class="col-md-6 col-6 form-check">
                                            <label class="form-radio-label" id="excel">
                                                {{ Form::radio('type3', 'excel', ['class' => 'form-radio-input']) }}
                                                <span class="form-radio-sign">
                                                    @lang('messages.UploadTags')
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col col-md-6 form-check">
                                            <label class="form-radio-label" id="direct">
                                                {{ Form::radio('type3', 'detail', ['class' => 'form-radio-input', 'value' => 'true', 'id' => 'ondetails']) }}
                                                <span class="form-radio-sign">
                                                    @lang('messages.TagsDetails')
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <!-- upload Excel -->
                            <div class="row my-4" Id="showexcel" style="display: none">
                                <div class="col-md-4 d-flex justify-content-center align-items-end ">
                                    <a href="#">
                                        <button type="button" class="btn btn-sm btn-primary" id="dexcel">
                                            @lang('messages.DOWNLOADTEMPLATE')
                                        </button>
                                    </a>
                                </div>
                                <div class="col-md-4 mt-2 mt-md-0">
                                    <label for="no">
                                        @lang('messages.noOfRFIDTags') : {{$tags}}
                                    </label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="Upload" name="import_file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                        <label class="custom-file-label" for="customFile"
                                            id="fileName1">Choose</label>
                                    </div>
                                </div>
                                <div class="col-md-4 d-flex justify-content-center align-items-end mt-2 mt-md-0">
                                    <a id="import">
                                        <button type="button" class="btn btn-sm btn-primary">
                                            @lang('messages.IMPORTEXCEL')                                            
                                        </button>
                                    </a>
                                </div>

                            </div>
                            <div class="row" id="details">
                                <div class="col-md-12 table-responsive">
                                    <table id="example1"
                                        class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;width=10px;">No</th>
                                                <th style="text-align: center;">
                                                @lang('messages.TagId')
                                                </th>
                                                <th style="text-align: center;">
                                                @lang('messages.TagName/EmployeeName')
                                                </th>
                                                <th style="text-align: center;width:20%;">
                                                @lang('messages.Designation')
                                                </th>
                                                <th style="text-align: center;">
                                                @lang('messages.EmployeeId')
                                                </th>
                                                <th style="text-align: center;">
                                                @lang('messages.Mobilenumber')
                                                </th>
                                                <th style="text-align: center;">
                                                @lang('messages.Department')
                                                </th>
                                                @if ($tags != 1)
                                                    <th style="text-align: center;">
                                                        @lang('messages.Action')
                                                    </th>
                                                @endif
                                            </tr>
                                        </thead>

                                        <tbody>

                                            @for ($i = 1; $i <= $tags; $i++)
                                                <tr style="text-align: center;">
                                                    <td>{{ $i }}</td>
                                                    <td>{{ Form::text('tagid[]', old('tagid[]'), ['class' => 'form-control','required'=>'required']) }}
                                                    </td>
                                                    <td>{{ Form::text('tagname[]', old('tagname[]'), ['class' => 'form-control','required'=>'required']) }}
                                                    </td>
                                                    <td>{{ Form::select('designation[]', ['' => 'Select', 'Driver' => 'Driver', 'Helper' => 'Helper', 'Staff' => 'Staff', 'Student' => 'Student'], old('designation[]'), ['class' => 'form-control','required'=>'required']) }}
                                                    </td>
                                                    <td>{{ Form::text('empid[]', old('empid[]'), ['class' => 'form-control','required'=>'required']) }}
                                                    </td>
                                                    <td>{{ Form::number('mobile[]', old('mobile[]'), ['class' => 'form-control mobile','required'=>'required']) }}
                                                    </td>
                                                    <td>{{ Form::text('department[]', old('department[]'), ['class' => 'form-control','required'=>'required']) }}
                                                    </td>
                                                    @if ($tags != 1)
                                                    <td>
                                                        @if ($i !== 1)<i class="fas fa-window-close text-danger"></i>@endif
                                                    </td>
                                                    @endif
                                                </tr>
                                            @endfor
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12 text-center">
                                    {{ Form::submit(__('messages.Submit'), ['id' => 'submit1', 'class' => 'btn btn-primary ']) }}
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#details').show();
            let moblie;
            $('.mobile').keyup(function(event) {
                let length = $(this).val().length;
                if (length <= 15) {
                    moblie = $(this).val()
                }else{
                    $(this).val(moblie)
                }
            });

            $("#submit1").click(function() {
                var formAction = '{{route('addTags')}}';
                $('#submit').attr('action', formAction);
            });

            $(".fa-window-close").click(function() {
                $(this).closest('tr').remove();
            });
            // Excel changes

            $("#excel").click(function() {
                $("#showexcel").show();
                $("#details").hide();
            });

            $("#direct").click(function() {
                $("#details").show();
                $("#showexcel").hide();
            });

            $("#import").click(function() {
                var formAction1 = '{{ route('uploadTagsExcel') }}';
                if($('#Upload').val() ==""){
                    return wornigAlert('Need to select the file');
                }
                // wornigAlert(excel);
                var enc = 'multipart/form-data';
                //enctype="multipart/form-data"
                $('#submit').attr('action', formAction1);
                $('#submit').attr('enctype', enc);
                $('#submit').submit();
            });
            $("#dexcel").click(function() {
                var formAction = '{{ route('downloadTagExcel') }}';
                $('#submit').attr('action', formAction);
                //submit form
                $('#submit').submit();
            });

        });
    </script>
@endpush
