@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            <div class="page-header">
                <h4 class="page-title">
                    @lang('messages.ViewTags')
                </h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                            @lang('messages.ViewTags')
                        </a>
                    </li>
                </ul>
            </div>
            @if (session()->has('message'))
                <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                    {{ session()->get('message') }}</p>
            @endif
            <div class="hpanel">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            {{ Form::open(['url' => 'rfid/index1']) }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="org">
                                            @lang('messages.SelectOrganizationName')
                                        </label>
                                        {{ Form::select('orgid', $orgList, $orgId, ['id' => 'orgid', 'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search' => 'true', 'required' => 'required']) }}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary mt-md-4']) }}
                                    </div>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                        <div class="card-body">
                            <span class="text-danger error-message">{{ HTML::ul($errors->all()) }}</span>
                            <div class="table-responsive">
                                <table id="example1"
                                    class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                    <thead>
                                        <tr class="text-center">
                                            <th class="text-center">
                                                @lang('messages.TagID')
                                            </th>
                                            <th class="text-center">
                                                @lang('messages.TagName/EmployeeName')
                                            </th>
                                            <th class="text-center">
                                                @lang('messages.MobileNumber')
                                            </th>
                                            <th class="text-center">
                                                @lang('messages.Designation')
                                            </th>
                                            <th class="text-center">
                                                @lang('messages.EmployeeId')
                                            </th>
                                            <th class="text-center">
                                                @lang('messages.Department')
                                            </th>
                                            <th class="text-center">
                                                @lang('messages.OrgName')
                                            </th>
                                            <th class="text-center">
                                                @lang('messages.Action')
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (!$show)
                                            @if ($form == 'show')
                                                @if (isset($values))
                                                    @foreach ($values as $key => $value)
                                                        <tr class="text-center">
                                                            <td class="text-center">
                                                                {{ $key }}
                                                            </td>
                                                            <td class="text-center">
                                                                {{ Arr::get($tagnameList, $key) }}
                                                            </td>
                                                            <td class="text-center">
                                                                {{ Arr::get($mobileList, $key) }}
                                                            </td>
                                                            <td class="text-center">
                                                                {{ Arr::get($designationList, $key) }}
                                                            </td>
                                                            <td class="text-center">
                                                                {{ Arr::get($empIdList, $key) }}
                                                            </td>
                                                            <td class="text-center">
                                                                {{ Arr::get($deptList, $key) }}
                                                            </td>
                                                            <td>{{ $orgId }}</td>
                                                            <td>
                                                                <a class="btn btn-success btn-sm "
                                                                    href="{{ URL::to('rfid/' . $key . ';' . $orgId . '/edit') }}">
                                                                @lang('messages.Edit')
                                                                </a>
                                                                <a class="btn btn-danger btn-sm"
                                                                    href="{{ URL::to('rfid/' . $key . ';' . $orgId . '/destroy') }}">
                                                                @lang('messages.Delete')
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            @else
                                                <tr class="text-center">
                                                    <td class="text-center">{{ $tagid }}
                                                    </td>
                                                    <td class="text-center">{{ $tagname }}
                                                    </td>
                                                    <td class="text-center">{{ $mobile }}
                                                    </td>
                                                    <td class="text-center">{{ $designation }}
                                                    </td>
                                                    <td class="text-center">{{ $employeeId }}
                                                    </td>
                                                    <td class="text-center">{{ $department }}
                                                    </td>
                                                    <td>{{ $orgId }}</td>
                                                    <td>
                                                        <a class="btn btn-success btn-sm"
                                                            href="{{ URL::to('rfid/' . $tagid . ';' . $orgId . '/edit') }}">
                                                            @lang('messages.Edit')
                                                        </a>
                                                        <a class="btn btn-danger btn-sm"
                                                            href="{{ URL::to('rfid/' . $tagid . ';' . $orgId . '/destroy') }}">
                                                            @lang('messages.Delete')
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
