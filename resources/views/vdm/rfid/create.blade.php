@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            <div class="hpanel">
                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.CreateTags')
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.CreateTags')
                            </a>
                        </li>
                    </ul>
                </div>
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif
                <div class="card">
                    <div class="card-body">
                        <span class="text-danger error-message">
                            {{ HTML::ul($errors->all()) }}
                        </span>
                        {{ Form::open(['url' => 'rfid']) }}
                        <div class="row py-4">
                            <div class="col-md-6">
                                <div class="form-group pb-0">
                                    {{ Form::label('tags', __('messages.NumberOfRFIDTags') . ':', ['class' => 'font-weight-bold']) }}
                                    {{ Form::Number('tags', request()->old('tags'), ['class' => $errors->any() ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Quantity']) }}
                                </div>
                            </div>
                            <div class=" col-md-6 mt-3">
                                {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary mt-4']) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- @include('includes.js_create') --}}
@endsection
