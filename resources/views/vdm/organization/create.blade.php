@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="page-inner">
            @if (session()->has('message'))
                <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                    {{ session()->get('message') }}</p>
            @endif
            <div class="page-header">
                <h4 class="page-title">
                    @lang('messages.AddOrganization')
                </h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('vdmOrganization.index') }}">
                            @lang('messages.Organization')
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                            @lang('messages.AddOrganization')
                        </a>
                    </li>
                </ul>
            </div>
            <div class="page-body">
                <div class="card">
                    <div class="card-body">
                        <span class="text-danger error-message">
                            {{ HTML::ul($errors->all()) }}
                        </span>
                        {{ Form::open(['url' => 'vdmOrganization']) }}
                        <div class="panel-body">
                            <h4 class="text-primary">
                                @lang('messages.General')
                            </h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('organizationId', __('messages.OrganizationName')) }}<span
                                            class="rq"></span>
                                        <i class="fa fa-info-circle fa-lg text-warning ml-auto" data-toggle="tooltip"
                                            data-placement="right"
                                            title="Organization name is case sensitive and space is not allowed"></i> <br>
                                        {{ Form::text('organizationId', $orgName1, ['class' => 'form-control', 'required' => 'required', 'placeholder' =>__('messages.OrganizationName'), 'id' => 'orgId', 'onkeyup' => 'caps(this)']) }}
                                        <span id="validation" class="b text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('mobile', __('messages.MobileNumber')) }}<span
                                            class="rq"></span>
                                        {{ Form::number('mobile', old('mobile'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => __("messages.MobileNumber")]) }}
                                    </div>
                                    <div class="form-group mt-2">
                                        <div class="form-row">
                                            {{ Form::label('ShowLiveSite', __('messages.ShowSites(InWeb)'), ['class' => 'col-md-6']) }}
                                            <label class="switch">
                                                {{ Form::checkbox('live', 'yes', old('live'), ['class' => 'col-md-6']) }}
                                                <span class="slider"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('email', __('messages.Email')) }}<span
                                            class="rq"></span>
                                        {{ Form::email('email', old('email'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => __('messages.Email'), 'id' => 'email']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('description', __('messages.Description')) }}
                                        {{ Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Description']) }}
                                    </div>
                                    <br>
                                   
                                </div>
                            </div>
                            <br>
                            <h4 class="text-primary">
                                @lang('messages.AlertConfiguration')
                            </h4>
                            <hr>
                            <div class="row mx-0 mx-md-2">
                                <div class="col-md-6">
                                    <div class=" form-group">
                                        <div class="form-row mt-3 ">
                                            {{ Form::label('ParkingAlert', __('messages.ParkingAlert'), ['class' => 'col-md-6']) }}
                                            <label class="switch">
                                                {{ Form::checkbox('parkingAlert', 'yes', old('parkingAlert'), ['class' => 'col-md-6']) }}
                                                <span class="slider"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="form-row mt-3 ">
                                            {{ Form::label('IdleAlert', __('messages.IdleAlert'), ['class' => 'col-md-6']) }}
                                            <label class="switch">
                                                {{ Form::checkbox('idleAlert', 'yes', old('idleAlert'), ['class' => 'col-md-6']) }}
                                                <span class="slider"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="form-row mt-3 ">
                                            {{ Form::label('OverSpeedAlert', __('messages.OverSpeedAlert'), ['class' => 'col-md-6']) }}
                                            <label class="switch">
                                                {{ Form::checkbox('overspeedalert', 'yes', old('overspeedalert'), ['class' => 'col-md-6']) }}
                                                <span class="slider"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="form-row mt-3">
                                            {{ Form::label('SafetyMovementAlert', __('messages.UnauthorisedMovementAlert'), ['class' => 'col-md-6']) }}
                                            <label class="switch">
                                                {{ Form::checkbox('safemove', 'yes', old('safemove'), ['class' => 'col-md-6']) }}
                                                <span class="slider"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="form-row  mt-3">
                                            {{ Form::label('SOSAlert', __('messages.SOSAlert'), ['class' => 'col-md-6']) }}
                                            <label class="switch">
                                                {{ Form::checkbox('sosAlert', 'yes', old('sosAlert'), ['class' => 'col-md-6']) }}
                                                <span class="slider"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="form-row mt-3">
                                            {{ Form::label('HarshBrakingAlert', __('messages.HarshBrakingAlert'), ['class' => 'col-md-6']) }}
                                            <label class="switch">
                                                {{ Form::checkbox('harshBreak', 'yes', old('harshBreak'), ['class' => 'col-md-6']) }}
                                                <span class="slider"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="form-row mt-3">
                                            {{ Form::label('DailySummary', __('messages.DailySummary'), ['class' => 'col-md-6']) }}
                                            <label class="switch">
                                                {{ Form::checkbox('dailySummary', 'yes', old('dailySummary'), ['class' => 'col-md-6']) }}
                                                <span class="slider"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        <div class="form-row mt-3">
                                            {{ Form::label('TripPlannedTime', __('messages.TripPlannedTime'), ['class' => 'col-md-6']) }}
                                            <label class="switch">
                                                {{ Form::checkbox('tripPlannedTime', 'yes', old('tripPlannedTime'), ['class' => 'col-md-6']) }}
                                                <span class="slider"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class=" form-group">
                                        {{ Form::label('parkDuration', __('messages.ParkDuration(Mins)')) }}
                                        {{ Form::text('parkDuration', old('parkDuration'), ['class' => 'form-control', 'placeholder' => __('messages.ParkDuration(Mins)')]) }}
                                    </div>
                                    <div class=" form-group">
                                        {{ Form::label('idleDuration', __('messages.IdleDuration(Mins)')) }}
                                        {{ Form::text('idleDuration', old('idleDuration'), ['class' => 'form-control', 'placeholder' => __('messages.IdleDuration(Mins)')]) }}
                                    </div>
                                    <div class=" form-group">
                                        {{ Form::label('overspeedDuration', __('messages.OverSpeedAlertDuration(Mins)')) }}
                                        {{ Form::text('overSpeedDuration', old('overSpeedDuration'), ['class' => 'form-control', 'placeholder' =>  __('messages.OverSpeedAlertDuration(Mins)')]) }}
                                    </div>
                                    <div class=" form-group" id="UnauthorisedMovementHrs">
                                        {{ Form::label('startTime', __('messages.UnauthorisedMovementHrs')) }}
                                        <div class="row px-2">
                                            <div class="col-md-6  form-group">
                                                {{ Form::label('startTime', __('messages.StartTime')) }}
                                                {{ Form::input('text', 'time1', old('time1'), ['class' => 'form-control  time', 'placeholder' => __('messages.StartTime'), 'id' => 'time1']) }}
                                            </div>
                                            <div class="col-md-6  form-group">
                                                {{ Form::label('endTime', __('messages.EndTime')) }}
                                                {{ Form::input('text', 'time2', old('time2'), ['class' => 'form-control  time', 'placeholder' => __('messages.EndTime'), 'id' => 'time2']) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" form-group">
                                        {{ Form::label('noDataDuration', __('messages.NoDataDuration')) }}
                                        {{ Form::select('noDataDuration', ['' =>  __('messages.Select'), '30' => '30 Mins', '45' => '45 Mins', '60' => '1 hr', '120' => ' 2 hrs', '240' => '4 hrs', '360' => '6 hrs', '480' => '8 hrs', '720' => '12 hrs', '960' => '16 hrs', '1080' => '18 hrs', '1200' => '20 hrs', '1440' => '24 hrs'], old('noDataDuration'), ['class' => 'form-control']) }}
                                    </div>
                                                                        
                                    
                                    <div class=" form-group">
                                        {{ Form::label('powerCutOffAlarmDuration', __('messages.PowerCutOffAlarmDuration(Mins)')) }}
                                        {{ Form::number('powerCutOffAlarmDuration', old('powerCutOffAlarmDuration'), ['class' => 'form-control', 'placeholder' => __('messages.PowerCutOffAlarmDuration(Mins)')]) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('routeDeviation', __('messages.RouteDeviation(Meters)')) }}
                                        {{ Form::select('routeDeviation',[''=>__('messages.Select'),'100'=>'100m','500'=>'500m','1000'=>'1000m'], old('routeDeviation'), ['class' => 'form-control']) }}
                                    </div>
                                   
                                </div>
                                
                            </div>
                        </div>
                        <br>
                        <h4 class="text-primary">
                            @lang('messages.FuelAlertsConfiguration')
                        </h4>
                        <hr>
                        <div class="row mx-0 mx-md-2">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-row">
                                        {{ Form::label('DailyDieselSummary', __('messages.DailyDieselSummary'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('dailyDieselSummary', 'yes', old('dailyDieselSummary'), ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row mt-3">
                                        {{ Form::label('ExcessConsumptionFilter', __('messages.OngoingFuelAlerts'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('fuelApproximate', 'yes', old('fuelApproximate'), ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        {{ Form::label('FuelAlarm', __('messages.FuelAlarm'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('fuelAlarm', 'yes', old('fuelAlarm'), ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        {{ Form::label('ExcessConsumptionFilter', __('messages.ExcessConsumptionFilter'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('excessConsumptionFilter', 'yes', old('excessConsumptionFilter'), ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        {{ Form::label('InstantFuelAlerts', __('messages.InstantFuelAlerts'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('instantFuel', 'yes', old('instantFuel'), ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-row">
                                        {{ Form::label('FuelLevelBelow', __('messages.FuelLevelBelow'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('fuelLevelBelow', 'yes', old('fuelLevelBelow'), ['class' => 'col-md-6', 'id' => 'fuelLevelBelow']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                               
                                <div class="form-group" id='fuelValueFiled'>
                                    {{ Form::label('fuelLevelBelowValue', __('messages.FuelLevelBelow') . ' (%)') }}
                                    {{ Form::select('fuelLevelBelowValue', ['5' => '5', '10' => '10', '15' => '15'], old('fuelLevelBelowValue'), ['class' => 'form-control', 'id' => 'fValue']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('enableFuelNotification', __('messages.FuelNotificationAlert')) }}
                                    {{ Form::select('enableFuelNotification', ['always' => 'Always', 'idleOrParked' => 'Idle/Parked'], old('enableFuelNotification'), ['class' => 'form-control']) }}
                                </div>
                                
                                <div class="form-group">
                                    {{ Form::label('fuelAlertGeoFence', __('messages.GeoFenceFuelAlert')) }}
                                    {{ Form::select('fuelAlertGeoFence',  array("default"=>"Both","Inside"=>"Site Inside","Outside"=>"Site OutSide"), old("fuelAlertGeoFence"), array('class' => 'form-control','id'=>'fuelAlertGeoFence')) }}
                                </div>
                            </div>
                        </div>
                        <br>
                        <h4 class="text-primary">
                            @lang('messages.SMSAlertsConfiguration')
                        </h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('smsSender', __('messages.SMSSender')) }}
                                    {{ Form::text('smsSender', old('smsSender'), ['class' => 'form-control', 'placeholder' => 'Sms Sender']) }}
                                </div>
                                <div class="form-group" id='textLocalSmsKeyFiled'>
                                    {{ Form::label('textLocalSmsKey', __('messages.TextLocalSmsKey')) }}<span
                                        class="rq"></span>
                                    {{ Form::text('textLocalSmsKey', old('textLocalSmsKey'), ['class' => 'form-control', 'id' => 'textLocalSmsKey','placeholder'=>__('messages.TextLocalSmsKey')]) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('providerUserName', __('messages.ProviderUserName')) }}
                                    {{ Form::text('providerUserName', old('providerUserName'), ['class' => 'form-control', 'placeholder' =>  __('messages.ProviderUserName')]) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('smsPattern', __('messages.SMSPattern')) }}
                                    {{ Form::select('smsPattern', ['nill' => '-- Select --', 'type1' => 'Type 1', 'type2' => 'Type 2', 'type3' => 'Type 3'], old('smsPattern'), ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('smsProvider', __('messages.SMSProvider')) }}
                                    {{ Form::select('smsProvider', $smsP, old('smsProvider'), ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true', 'placeholder' => 'SMS Provider']) }}
                                </div>
                                <div class="form-group" id="smsEntityNameFiled">
                                    {{ Form::label('smsEntityName', __('messages.SMSEntityName')) }}<span
                                        class="rq"></span>
                                    {{ Form::text('smsEntityName', old('smsEntityName'), ['class' => 'form-control', 'placeholder' => 'SMS Entity Name', 'id' => 'smsEntityName']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('providerPassword', __('messages.ProviderPassword')) }}
                                    {{ Form::input('password', 'providerPassword', old('providerPassword'), ['class' => 'form-control', 'placeholder' => 'Provider Password']) }}
                                </div>
                                <div class=" form-group">
                                    <div class="form-row mt-3">
                                        {{ Form::label('SmsEscalation', __('messages.SmsEscalation'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('escalatesms', 'yes', old('escalatesms'), ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h4 class="text-primary">
                            @lang('messages.RFIDConfiguration')
                        </h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-row">
                                        {{ Form::label('ISRFID', __('messages.ISRFID'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('isRfid', 'yes', old('isRfid'), ['class' => 'col-md-6', 'id' => 'isrfid']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="pickup" style="display: none;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('pickup', __('messages.PickUp'),['class'=>'badge badge-primary text-white']) }}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('StartTime', __('messages.StartTime')) }}<span
                                                    class="rq"></span>
                                                {{ Form::input('text', 'PickupStartTime', null, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'PickupStartTime']) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('EndTime', __('messages.EndTime')) }}<span
                                                    class="rq"></span>
                                                {{ Form::input('text', 'PickupEndTime', null, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'PickupEndTime']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="drop" style="display: none;">
                                    {{ Form::label('drop', __('messages.Drop'),['class'=>'badge badge-primary text-white']) }}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('StartTime', __('messages.StartTime')) }}<span
                                                    class="rq"></span>
                                                {{ Form::input('text', 'DropStartTime', null, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'DropStartTime']) }}

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('EndTime', __('messages.EndTime')) }}<span
                                                    class="rq"></span>
                                                {{ Form::input('text', 'DropEndTime', null, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'DropEndTime']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h4 class="text-primary">
                            @lang('messages.GeofenceConfiguration')
                        </h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-row">
                                        {{ Form::label('SchoolGeofence', __('messages.SchoolGeofence'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('SchoolGeoFence', 'yes', old('SchoolGeoFence'), ['class' => 'col-md-6', 'id' => 'SchoolGeoFence']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('geofence', __('messages.Geofence')) }}
                                    {{ Form::select('geofence', ['nill' => '-- Select --', 'yes' => 'Site Entry','no' => 'Site Exit'], old('geofence'), ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('sendGeoFenceSMS', __('messages.SendGeofenceSMS')) }}
                                    {{ Form::select('sendGeoFenceSMS', ['no' => '-- Select --', 'yes' => 'Both', 'Entry' => 'Entry', 'Exit' => 'Exit'], old('sendGeoFenceSMS'), ['class' => 'form-control', 'id' => 'sendGeoFenceSMS']) }}
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12" id='Entry'>
                                        <div class="form-group">
                                            {{ Form::label('Entry', __('messages.Entry')) }}
                                            <hr>
                                            <div class="form-group">
                                                {{ Form::label('slot1', __('messages.Slot1'),['class'=>'badge badge-primary text-white']) }}
                                                <div class="row" id='rowtime1'>
                                                    <div class="col-md-6 form-group">
                                                            {{ Form::label('morningEntryStartTime', __('messages.MorningEntryStartTime')) }}<span
                                                                class="rq"></span>
                                                            {{ Form::input('text', 'morningEntryStartTime', null, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'morningEntryStartTime']) }}
                                                    </div>
                                                    <div class="col-md-6 form-group">
                                                            {{ Form::label('morningEntryEndTime', __('messages.MorningEntryEndTime')) }}<span
                                                                class="rq"></span>
                                                            {{ Form::input('text', 'morningEntryEndTime', null, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'morningEntryEndTime']) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                {{ Form::label('slot2', __('messages.Slot2'),['class'=>'badge badge-primary text-white']) }}
                                                <div class="row">
                                                    <div class="col-md-6 form-group">
                                                            {{ Form::label('eveningEntryStartTime', __('messages.EveningEntryStartTime')) }}<span
                                                                class="rq"></span>
                                                            {{ Form::input('text', 'eveningEntryStartTime', null, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'eveningEntryStartTime']) }}
                                                    </div>
                                                    <div class="col-md-6 form-group">
                                                            {{ Form::label('eveningEntryEndTime', __('messages.EveningEntryEndTime')) }}<span
                                                                class="rq"></span>
                                                            {{ Form::input('text', 'eveningEntryEndTime', null, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'eveningEntryEndTime']) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id='Exit'>
                                        <div class="form-group">
                                            {{ Form::label('Exit', __('messages.Exit')) }}
                                            <hr>
                                            <div class="form-group">
                                                {{ Form::label('slot1', __('messages.Slot1'),['class'=>'badge badge-primary text-white']) }}
                                                <div class="row" id='rowtime1'>
                                                    <div class="col-md-6 form-group">
                                                            {{ Form::label('morningExitStartTime', __('messages.MorningExitStartTime')) }}<span
                                                                class="rq"></span>
                                                            {{ Form::input('text', 'morningExitStartTime', null, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'morningExitStartTime']) }}
                                                    </div>
                                                    <div class="col-md-6 form-group">
                                                            {{ Form::label('morningExitEndTime', __('messages.MorningExitEndTime')) }}<span
                                                                class="rq"></span>
                                                            {{ Form::input('text', 'morningExitEndTime', null, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'morningExitEndTime']) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                {{ Form::label('slot2',  __('messages.Slot2'),['class'=>'badge badge-primary text-white']) }}
                                                <div class="row">
                                                    <div class="col-md-6 form-group">
                                                            {{ Form::label('eveningExitStartTime', __('messages.EveningExitStartTime')) }}<span
                                                                class="rq"></span>
                                                            {{ Form::input('text', 'eveningExitStartTime', null, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'eveningExitStartTime']) }}
                                                    </div>
                                                    <div class="col-md-6 form-group">
                                                            {{ Form::label('eveningExitEndTime', __('messages.EveningExitEndTime')) }}<span
                                                                class="rq"></span>
                                                            {{ Form::input('text', 'eveningExitEndTime', null, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'eveningExitEndTime']) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>                        
                        <br>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                {{ Form::submit(__('messages.SaveOrganization'), ['class' => 'btn btn-primary', 'id' => 'submit']) }}
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('common/js/org.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#textLocalSmsKeyFiled,#smsEntityNameFiled')
                .fadeOut();

            $('#smsProvider').on('change', function() {
                $('#textLocalSmsKeyFiled,#smsEntityNameFiled').fadeOut();
                $("#textLocalSmsKey,#smsEntityName").removeAttr('required');
                if ($(this).val() === "TextLocal") {
                    $('#textLocalSmsKeyFiled,#smsEntityNameFiled').fadeIn();
                    $("#textLocalSmsKey,#smsEntityName").attr('required', 'required');
                }
            });

            $("#fuelValueFiled").fadeOut();
            $('#fuelLevelBelow').on('change', function() {
                $("#fuelValueFiled").fadeOut();
                $("#fValue").removeAttr('required');

                if ($(this).is(":checked")) {
                    $("#fuelValueFiled").fadeIn();
                    $("#fValue").attr('required', 'required');
                }
            });

            // showGeoFenceEntryExit
            showGeoFenceEntryExit();
            $('#SchoolGeoFence,#sendGeoFenceSMS').on('change', function() {
                showGeoFenceEntryExit();
            });

            function showGeoFenceEntryExit() {
                const sendGeoFenceSMS = $('#sendGeoFenceSMS').val();
                const SchoolGeoFence = $('#SchoolGeoFence').is(":checked");

                $('#Entry,#Exit').fadeOut();
                if(!SchoolGeoFence) $('#sendGeoFenceSMS').val('no')
                if (!SchoolGeoFence) return
                if (sendGeoFenceSMS == 'yes') {
                    $('#Entry,#Exit').fadeIn();
                    return
                }
                // sendGeoFenceSMS have Entry,Exit,yes,no
                $(`#${sendGeoFenceSMS}`).fadeIn();
            }

            showSafemove()
            $('input[name="safemove"]').on('change', showSafemove);

            function showSafemove() {
                $("#UnauthorisedMovementHrs").hide()
                if ($('input[name="safemove"]').is(":checked")) {
                    $('#UnauthorisedMovementHrs').fadeIn();
                }
            }
            //escalate
            showEscalatesms();
            $('#escalatesms').on('change', function() {
                showEscalatesms();
            });

            function showEscalatesms() {
                $('#escalatebtn').fadeOut();
                if ($('#escalatesms').is(":checked")) {
                    $('#escalatebtn').fadeIn();
                }
            }

            // showIsrfid
            showIsrfid();
            $('#isrfid').on('change', function() {
                showIsrfid();
            });

            function showIsrfid() {
                $('#pickup,#drop').fadeOut();
                if ($('#isrfid').is(":checked")) {
                    $('#pickup,#drop').fadeIn();
                }
            }

            $('#submit').on('click', function() {

                if ($('#isrfid').is(":checked")) {
                    $('#PickupStartTime,#PickupEndTime,#DropStartTime,#DropEndTime')
                        .attr('required', 'required');
                } else {
                    $('#PickupStartTime,#PickupEndTime,#DropStartTime,#DropEndTime')
                        .removeAttr('required');
                }

                if ($('#SchoolGeoFence').is(":checked")) {
                    sendGeoFenceSMS = $('#sendGeoFenceSMS').val()
                    if (sendGeoFenceSMS == 'Entry') {
                        $(`
                            #morningEntryStartTime,
                            #morningEntryEndTime,
                            #eveningEntryStartTime,
                            #eveningEntryEndTime
                        `).attr('required', 'required');

                        $(`
                            #morningExitStartTime,
                            #morningExitEndTime,
                            #eveningExitStartTime,
                            #eveningExitEndTime
                        `).removeAttr('required').val('');
                    } else if (sendGeoFenceSMS == 'Exit') {
                        $(`
                            #morningExitStartTime,
                            #morningExitEndTime,
                            #eveningExitStartTime,
                            #eveningExitEndTime
                        `).attr('required', 'required');
                        $(`
                            #morningEntryStartTime,
                            #morningEntryEndTime,
                            #eveningEntryStartTime,
                            #eveningEntryEndTime
                        `).removeAttr('required').val('');

                    } else if (sendGeoFenceSMS == 'yes') {
                        $(`
                            #morningExitStartTime,
                            #morningExitEndTime,
                            #eveningExitStartTime,
                            #eveningExitEndTime,
                            #morningEntryStartTime,
                            #morningEntryEndTime,
                            #eveningEntryStartTime,
                            #eveningEntryEndTime
                        `).attr('required', 'required');
                    } else {
                        $(`
                            #morningExitStartTime,
                            #morningExitEndTime,
                            #eveningExitStartTime,
                            #eveningExitEndTime,
                            #morningEntryStartTime,
                            #morningEntryEndTime,
                            #eveningEntryStartTime,
                            #eveningEntryEndTime
                        `).removeAttr('required').val('');
                    }
                } else {
                    $(`
                        #morningExitStartTime,
                        #morningExitEndTime,
                        #eveningExitStartTime,
                        #eveningExitEndTime,
                        #morningEntryStartTime,
                        #morningEntryEndTime,
                        #eveningEntryStartTime,
                        #eveningEntryEndTime
                    `).removeAttr('required').val('');
                }

            });
        });
        $('#orgId').on('change', function() {
            orgcheck();
        });
        $('#email').on('change', function() {
            orgcheck();
        });

        function orgcheck() {
            $('#orgId').removeClass('is-invalid')
            $('.aftererr').remove()
            var postValue = {
                'id': $('#orgId').val(),
                '_token': $('input[name="_token"]').val()

            };
            // alert($('#groupName').val());
            $('#orgId').removeClass('is-invalid');
            $.post('{{ route('ajax.ordIdCheck') }}', postValue)
                .done(function(data) {
                    if (data == null) data = '';
                    if (data.trim() != '') {
                        $('#orgId').addClass('is-invalid').focus().aftererr(data);
                        return false;
                    }

                }).fail(function() {
                    console.log("fail");
                });
        }
    </script>
@endpush
