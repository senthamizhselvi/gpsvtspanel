@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="page-inner">
            @if (session()->has('message'))
                <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                    {{ session()->get('message') }}
                </p>
            @endif
            <div class="page-header">
                <h4 class="page-title">
                    @lang('messages.SiteNotification')
                </h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('vdmOrganization.index') }}">
                            @lang('messages.Organization')
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                            @lang('messages.SiteNotification')
                        </a>
                    </li>
                </ul>
            </div>
            <div class="page-body">
                <div class="card">
                    <span class="text-danger error-message">
                        {{ HTML::ul($errors->all()) }}
                    </span>
                    {{ Form::open(['url' => 'vdmOrganization/siteUpdate']) }}
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-3">
                                {{ Form::label('orgId1', __('messages.OrganizationName'), ['class' => 'b']) }} :
                                {{ Form::label('orgId1', $orgId, ['class' => 'text-primary']) }}
                                {{ Form::hidden('orgId', $orgId, ['class' => 'form-control']) }}
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <table id="record-table"
                                class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">
                                            @lang('messages.ID')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.SiteName')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.Time')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.Enable')
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for ($i = 0; $i < $counts; $i++)
                                        <tr style="text-align: center;">
                                            <td>{{ ++$tmp }}</td>
                                            <td>{{ $alertList[$i] }}</td>
                                            <td>{{ Form::select('time[]', $time, $times[$i], ['class' => 'form-control']) }}
                                            </td>
                                            <td>{{ Form::select('enable[]', $enable, $enables[$i], ['class' => 'form-control']) }}
                                            </td>
                                        </tr>
                                    @endfor

                                </tbody>
                            </table>
                            <div class="col-md-4 mx-auto text-center">
                                @if ($counts != 0)
                                    {{ Form::submit(__('messages.UpdateSite'), ['class' => 'btn btn-primary']) }}
                                @else
                                    {{ Form::button(__('messages.UpdateSite'), ['class' => 'btn btn-primary', 'disabled' => 'disabled', 'style' => 'cursor: not-allowed;']) }}
                                @endif
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $('#record-table').DataTable();
    </script>
@endpush
