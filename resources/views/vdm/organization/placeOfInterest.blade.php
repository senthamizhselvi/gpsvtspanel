@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animated zoomIn">
            <div class="page-inner">
                <div class="hpanel">
                    <div class="page-header">
                        <h4 class="page-title">
                            @lang('messages.PlaceofInterest')
                        </h4><br>
                    </div>
                    <div class="page-body">
                        <div class="card">
                            <div class="card-body">
                                <span class="text-danger"> {{ HTML::ul($errors->all()) }}</span>

                                {{ Form::open(['url' => 'vdmOrganization/addpoi']) }}
                                <div class="row">
                                    <div class="col-md-4 ">
                                        {{ Form::label('orgId', __('messages.Organization')) }}
                                        {{ Form::select('orgId', $orgList, request()->old('orgId'), ['class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true']) }}
                                    </div>
                                    <div class="col-md-4 ">
                                        {{ Form::label('radiusrange', __('messages.RadiusRange')) }}
                                        {{ Form::Number('radiusrange', request()->old('radiusrange'), ['class' => 'form-control', 'placeholder' => 'Radius Range (Km)', 'min' => '1']) }}
                                    </div>
                                    <div class="col-md-2"><br>
                                        <h4>{{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-sm btn-primary']) }}
                                        </h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        {{ Form::label('Filter', __('messages.Filter')) }}
                                        {{ Form::input('text', 'searchtext', '', ['class' => 'searchkey  form-control']) }}
                                    </div>
                                    <div class="col-md-3 form-check ">
                                        <div class="mt-3">
                                            <label class="form-check-label">
                                                {{ Form::checkbox('$userplace', 'value', false, ['class' => 'form-check-input field check']) }}
                                                <span class="form-check-sign">
                                                    @lang('messages.SelectAll')
                                                </span>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 boarder-bottom form-check" id="selectedItems">
                                    </div>

                                    <br>
                                    <div class="col-md-12" id="unSelectedItems">
                                        <div class="row form-check">
                                            @if (isset($userplace))
                                                @foreach ($userplace as $key => $value)
                                                    <label class="form-check-label vehiclelist col-md-3">
                                                        {{ Form::checkbox('poi[]', $userplace[$key], null, ['class' => 'form-check-input field', 'id' => 'questionCheckBox']) }}
                                                        <span class="form-check-sign d-inline-block text-truncate w-100"
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="{{ $value }}">{{ $value }}
                                                        </span>
                                                    </label>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        {{ Form::hidden('value', json_encode($userplace, true)) }}
    </div>
@endsection
@push('js')
    <script>
        list = [];
        var value = $('input[name="value"]').val();
    </script>
    @include('includes.js_create')
@endpush
