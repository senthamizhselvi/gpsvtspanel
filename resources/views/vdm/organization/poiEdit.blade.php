@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animate-panel page-inner">
            <div class="page-header">
                <h4 class="page-title"><b>
                        <span>Edit POI</span>
                    </b>
                </h4><br>
                <span class="text-danger"> {{ HTML::ul($errors->all()) }}</span>
            </div>
            <div class="card">
                {{ Form::open(['url' => 'vdmOrganization/addpoi']) }}
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    {{ Form::label('orgId', 'OrgId Id :') }}
                                </div>
                                <div class="col-md-12">
                                    {{ Form::text('orgId', $orgId, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    {{ Form::label('radius', 'Radius:') }}
                                </div>
                                <div class="col-md-12">
                                    {{ Form::Number('radiusrange', $radiusrange, ['class' => 'form-control', 'placeholder' => 'Radius Range (Km)', 'min' => '1', 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <br>
                    <div class="row">
                        <div class="col-md-1">{{ Form::label('Filter', 'Filter :') }}</div>
                        <div class="col-md-3">
                            {{ Form::input('text', 'searchtext', null, ['class' => 'form-control searchkey', 'placeholder' => 'Filter']) }}
                        </div>
                        <div class="col-md-3 form-check">
                            <label class="form-check-label">
                                {{ Form::checkbox('$userplace', 'value', false, ['class' => 'form-check-input field check']) }}
                                <span class="form-check-sign">
                                    Select All
                                </span>
                            </label>
                        </div>
                        <div class="col-md-3">
                            {{ Form::submit('Update the POI!', ['class' => 'btn btn-primary btn-sm']) }}</div>
                    </div>
                    <br>


                    <div>
                        {{ Form::label('userplace', 'Select the Places:') }}
                        <div class="row form-check" id="selectedItems" style="border-bottom: 1px solid #a6a6a6;">
                        </div>
                        <br>
                        <div class="row form-check">
                            @if (isset($userplace))
                                @foreach ($userplace as $key => $value)
                                    <label class="form-check-label vehiclelist col-md-3">
                                        {{ Form::checkbox('poi[]', $userplace[$key], in_array($value, $selectedVehicles), ['class' => 'form-check-input field', 'id' => 'questionCheckBox']) }}
                                        <span class="form-check-sign d-inline-block text-truncate w-100" data-toggle="tooltip" data-placement="top" title="{{ $userplace[$key] }}">{{ $userplace[$key] }}
                                        </span>
                                    </label>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
            {{ Form::hidden('value', json_encode($userplace, true)) }}

        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        list = [];
        var value = $('input[name="value"]').val();
    </script>
    @include('includes.js_footer')
@endpush
@push('css')
    <style>
        label {
            font-weight: 600;
        }

    </style>
@endpush
