@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="page-inner">
            <div class="hpanel">
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif
                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.OrganizationTrack')
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('vdmOrganization.index') }}">
                                @lang('messages.Organization')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ URL::to('vdmOrganization/' . $organizationId . '/orgTrack') }}">
                                @lang('messages.OrganizationTrack')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                               {{$organizationId}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-left">
                                <a href="{{ URL::to('vdmOrganization/' . $organizationId . '/orgTrackList') }}">
                                    <i class="fas fa-arrow-circle-left fa-2x"></i>
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <span class="text-danger"> {{ HTML::ul($errors->all()) }}</span>
                            {{ Form::open(['url' => 'vdmOrganization/orgTrackEditUpdate']) }}
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-3">{{ Form::label('orgId', __('messages.OrganizationName')) }}
                                    </div>
                                    <div class="col-md-4">
                                        {{ Form::text('organizationId', $organizationId, ['class' => 'form-control', 'id' => 'orgId', 'required' => 'required', 'readonly' => 'true']) }}
                                    </div>
                                </div> <br />
                                <div hidden>
                                    {{ Form::select('vehicleIdOld[]', $vehicleIdList, $vehicleId, ['id' => 'vehicleList', 'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true', 'multiple' => 'multiple']) }}
                                </div>

                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-3">{{ Form::label('vehicleId', __('messages.VehicleName')) }}
                                    </div>
                                    <div class="col-md-4">
                                        {{ Form::select('vehicleId[]', $vehicleIdList, $vehicleId, ['id' => 'vehicleList', 'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true', 'required' => 'required', 'multiple' => 'multiple']) }}

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-3">{{ Form::label('trackName', __('messages.TrackName')) }}
                                    </div>
                                    <div class="col-md-4">
                                        {{ Form::text('trackName', $trackName, ['class' => 'form-control', 'placeholder' => 'Track Name', 'id' => 'trackNameId', 'required' => 'required', 'onkeyup' => 'validate(this)']) }}
                                        {{ Form::hidden('trackNameOld', $trackName, ['id' => 'vehicleList', 'class' => 'form-control ', 'required' => 'required', 'readonly' => 'true']) }}
                                    </div>
                                </div>
                                <div class="col-md-12 pt-4 text-center">
                                    {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary ', 'id' => 'submit']) }}
                                    <br />
                                </div>
                            </div>
							{{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        function validate(element) {
            var word = element.value
            var new1 = word.replace(/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/, "")
            element.value = new1
        }
        $(document).ready(function() {
            $("#trackNameId").on("keydown", function(e) {
                return e.which !== 32;
            });
        });

    </script>
@endpush
