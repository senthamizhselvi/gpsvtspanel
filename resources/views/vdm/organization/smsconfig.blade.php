@extends("layouts.app")
@section('content')
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">
                SMS Configuration
            </h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">{{ __('messages.EditOrganization') }}</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">{{ $orgId }}</a>
                </li>
            </ul>
            <br>
            <span class="text-danger"> {{ HTML::ul($errors->all()) }}</span>
        </div>
        <div class="page-body">
            <div class="card">
                <div class="card-body">
                    <div class="panel-body">
                        {{ Form::open(['url' => 'vdmsmsconfig/update', 'id' => 'testForm']) }}
                        {{ Form::hidden('orgId', $orgId, ['class' => 'form-control', 'id' => 'orgId', 'required' => 'required', 'readonly' => 'true']) }}
                        <br />
                        <div class="row" style="margin-left: 15%;">
                            <div class="col-sm-3">
                                <h4><b>Duration (In Hours)</b></h4>
                            </div>
                            <div class="col-sm-5">
                                <h4><b>Mobile Number</b></h4>
                            </div>
                            <div class="col-sm-3">
                                <h4>
                                    <b>
                                        <button class="btn btn-success btn-sm" type="button"
                                            onclick="education_fields();">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </b>
                                </h4>
                            </div>
                        </div>
                        <div id="education_fields">

                        </div>
                        @foreach ($duration as $key => $value)
                        <div class="form-group removeclass{{$value}}">
                            <div class="row " style="margin-left: 15%;"
                                id="{{$value}}">
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <input type="number" class="form-control" id="duration" required="required"
                                            name="duration[]" min=1 max=24
                                            value="{{$value}}"
                                            multiple placeholder="Duration">
                                    </div>
                                </div>
                                <div class="col-sm-5 nopadding">
                                    <div class="form-group">
                                        <input type="text" class="form-control mobile_number" id="mobile"
                                            required="required" name="mobile[]"
                                            value="{{Arr::get($mobile, $value)}}"
                                            placeholder="Mobile Number">
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button class="btn btn-danger btn-sm" type="button"
                                                    onclick="remove_education_fields('{{$value}}');">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="clear"></div>
                    </div>
                    <div class="row  mt-4">
                        <div class="col-md-3 mx-auto">
                            <a class="btn btn-warning " href="{{ URL::to('vdmOrganization/' . $orgId . '/edit') }}">Cancel</a>
                            <input id="sub" class="btn btn-primary  enableOnInput" type="submit" value="Update">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}
@endsection
@push('js')
    <script type="text/javascript">
        var room = 1;

        function education_fields() {

            room++;
            $("#education_fields").append(`
            <div class="form-group removeclass${room}">
                '<div class="row" style="margin-left: 15%;">
                    <div class="col-sm-3 nopadding">
                        <div class="form-group">
                            <input type="number" class="form-control" min=1 max=24 id="duration" name="duration[]"  required="required" value="" multiple placeholder="Duration">
                        </div>
                    </div>
                    <div class="col-sm-5 nopadding">
                        <div class="form-group">
                            <input type="text" class="form-control mobile_number" id="mobile" required="required" name="mobile[]" value="" placeholder="Mobile number">
                        </div>
                    </div>
                    <div class="col-sm-3 nopadding">
                        <div class="form-group">
                            <div class="input-group"> 
                                <div class="input-group-btn">
                                    <button class="btn btn-danger btn-sm" type="button"  onclick="remove_education_fields('${room}');">
                                        <i class="fas fa-minus"></i> 
                                    </button>
                                    </div>
                                    </div></div></div></div><div class="clear"></div>
            `)
        }

        function remove_education_fields(rid) {
            $('.removeclass' + rid).remove();
        }

        function remove_education_onval(key) {
            var elem = document.getElementById(key);
            return elem.parentNode.removeChild(elem);
        }

        $('#testForm').on('submit', function(e) {
            $('.error').remove();
            $(".mobile_number").each(function() {
                element = this;
                $.each(($(this).val()).split(','), function(key, value) {
                    if (!/^\d{10}$/.test(value)) {
                        $(".error", $(element).parent()).remove();
                        $(element).parent().append(
                            '<div class="error text-danger">Enter valid mobile number</div>');
                        if ($(element).parent() != false) {
                            e.preventDefault();
                            return false;
                        }
                    };
                })
            })

        });

    </script>
@endpush
