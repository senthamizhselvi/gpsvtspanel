@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            <div class="hpanel">
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                        {{ session()->get('message') }}</p>
                @endif
                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.EditAlert')
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('vdmOrganization.index') }}">
                            @lang('messages.Organization')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.EditAlert')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                {{$orgId}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="page-body">
                    <div class="card">
                        <span class="text-danger error-message">
                            {{ HTML::ul($errors->all()) }}
                        </span>
                        {{ Form::open(['url' => 'vdmOrganization/updateNotification']) }}
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-3 form-group ml-0 ml-md-3">
                                    {{ Form::label('orgId1', __('messages.OrganizationName'), ['class' => 'b  mt-2']) }} :  
                                    {{ Form::label('orgId1', $orgId,['class'=>'text-primary']) }}
                                    {{ Form::hidden('orgId', $orgId, ['class' => 'form-control']) }}
                                </div>
                                <div class="col-md-3 ml-auto">
                                    {{ Form::submit(__('messages.UpdateAlerts'), ['class' => 'btn btn-primary  mr-5']) }}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            {{-- @foreach ($List as $key => $value)
                                <div class="mx-3">
                                    <br>
                                    <h3 class="text-primary">
                                        {{ strtoupper($key) }}
                                    </h3>
                                    <hr>
                                    @foreach ($value as $itemKey => $itemValue)
                                        <div class="row p-1">
                                            <div class="form-group col-md-3">
                                                <label for="{{ $itemValue }}">{{ strtoupper($itemValue) }}</label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    {{ Form::checkbox('emailList[]', strtoupper($itemKey), in_array(strtoupper($itemKey), $emailArray), ['class' => ' form-check-input field']) }}
                                                    <span class="form-check-sign">
                                                        @lang('messages.Email')
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    {{ Form::checkbox('smsList[]', strtoupper($itemKey), in_array(strtoupper($itemKey), $smsArray), ['class' => 'field form-check-input']) }}
                                                    <span class="form-check-sign">
                                                        @lang('messages.SMS')
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach --}}
                                <div class="table-responsive">
                                    <table id="record-table"
                                        class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;">S.NO</th>
                                                <th style="text-align: center;">Alerts </th>
                                                <th style="text-align: center;">Email</th>
                                                <th style="text-align: center;">SMS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($alertList as $key => $value)
                                                <tr style="text-align: center;">
                                                    <td>{{ ++$tmp }}</td>
                                                    <td>{{ $value }}</td>
                                                    <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                {{ Form::checkbox('emailList[]', $value, in_array($value, $emailArray), ['class' => ' form-check-input field']) }}
                                                                <span class="form-check-sign"></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                {{ Form::checkbox('smsList[]', $value, in_array($value, $smsArray), ['class' => 'field form-check-input']) }}
                                                                <span class="form-check-sign"></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        // $('#record-table').DataTable();
    </script>
@endpush
