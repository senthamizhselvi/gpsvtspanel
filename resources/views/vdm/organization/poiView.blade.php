@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="page-inner">
            <div class="hpanel">
                <div class="page-header">
                    <h4 class="page-title"><b>
                            <span>View Place Of Interest</span>
                        </b>
                    </h4><br>
                    <span class="text-danger"> {{ HTML::ul($errors->all()) }}</span>
                </div>
                <div class="page-body">
                    <div class="card h-100">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-3">
                                    {{ Form::label('orgId', 'Organisation:', ['class' => 'font-weight-bold']) }}
                                    {{ Form::label('orgId', $orgId, ['class' => 'form-control']) }}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::label('radius', 'Radius Range:', ['class' => 'font-weight-bold']) }}
                                    {{ Form::label('radius', $radius, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-3">
                                    <h4 class="text-primary font-weight-bold">
                                        Place Of Interest :
                                    </h4>
                                </div>
                            </div>
                            <div class="row">
                                @if (isset($userplace))
                                    @foreach ($userplace as $key => $value)
                                        <div class="col-md-5" class="">
											<i class="fas fa-arrow-circle-right text-primary"></i>
                                            {{ Form::label($userplace[$key],$userplace[$key],['class'=>'text-dark']) }}
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
