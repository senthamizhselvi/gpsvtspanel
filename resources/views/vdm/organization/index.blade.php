@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content">
            <div class="page-inner">

                <div class="hpanel">

                    <div class="page-header">
                        <h4 class="page-title">{{__('messages.OrganizationList')}}</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="#">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('vdmOrganization.index') }}">{{__('messages.OrganizationList')}}</a>
                            </li>
                        </ul>
                    </div>
                    @if (session()->has('message'))
                        <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                            {{ session()->get('message') }}</p>
                    @endif
                    @if (session()->has('smsConfigMessage'))
                        <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                            {{ session()->get('smsConfigMessage') }}
                        </p>
                    @endif
                    <div class="page-body">
                        <div class="card animated zoomIn">
                            <div class="card-body">
                                <span class="text-danger error-message">
                                    {{ HTML::ul($errors->all()) }}
                                </span>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1"
                                            class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center;">{{__('messages.SNO')}}</th>
                                                    <th style="text-align: center;">{{__('messages.OrganizationName')}}</th>
                                                    <th style="text-align: center;">{{__('messages.Actions')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @foreach ($orgList as $key => $value)
                                                    <tr>
                                                        <td>{{ $key }}</td>
                                                        <td>{{ $value }}</td>

                                                        <!-- we will also add show, edit, and delete buttons -->
                                                        <td class="text-center">
                                                                    <div class="dropdown dropright">
                                                                        <button
                                                                            class="btn btn-info text-white dropdown-toggle btn-sm"
                                                                            type="button"
                                                                            id="dropdownMenuButton{{ $key }}"
                                                                            data-toggle="dropdown" aria-haspopup="true"
                                                                            aria-expanded="false">
                                                                           {{__('messages.Actions')}}
                                                                        </button>
                                                                        <div class="dropdown-menu"
                                                                            aria-labelledby="dropdownMenuButton{{ $key }}">
                                                                            <a class="btn btn-success btn-sm dropdown-item mb-1 text-white"
                                                                                href="{{ URL::to('vdmOrganization/' . $value . '/edit') }}">{{__('messages.EditOrganization')}} </a>
                                                                            <a class="btn btn-info btn-sm dropdown-item mb-1 text-white"
                                                                                href="{{ URL::to('vdmOrganization/' . $value . '/editAlerts') }}">{{__('messages.EditAlerts')}}</a>
                                                                            <a class="btn btn-warning btn-sm dropdown-item mb-1 text-white"
                                                                                href="{{ URL::to('vdmOrganization/' . $value . '/siteNotification') }}"> {{__('messages.SiteNotification')}}</a>
                                                                            <a class="btn btn-sm btn-info dropdown-item text-white"
                                                                                href="{{ URL::to('vdmOrganization/' . $value . '/orgTrack') }}"
                                                                                style="margin-top: 2%;margin-bottom: 2%;">
                                                                                {{__('messages.OrganizationTrack')}}</a>
                                                                            {{ Form::open(['url' => 'vdmOrganization/' . $value, 'id' => 'deleteForm' . $key]) }}
                                                                            {{ Form::hidden('_method', 'DELETE') }}
                                                                            {{ Form::button(__('messages.DeleteOrganization'), ['class' => 'btn btn-sm btn-danger dropdown-item text-white  deleteBtn', 'id' => $key, 'formtype' => 'Organization']) }}
                                                                            {{ Form::close() }}
                                                                        </div>
                                                                    </div>
                                                               
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
