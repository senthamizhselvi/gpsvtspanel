@extends("layouts.app")
@section('content')
    <div id="wrapper">

        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{ __('messages.EditOrganization') }}</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">{{ __('messages.EditOrganization') }}</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">{{ $organizationId }}</a>
                    </li>
                </ul>
                {{-- <a class="btn btn-info ml-auto text-white btn-sm" href="{{ URL::to('orgsmsconfig/' . $organizationId) }}"
                    id="escalatebtn">{{ __('messages.SMSConfiguration') }}</a> --}}
            </div>
            @if (session()->has('message'))
                <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                    {{ session()->get('message') }}
                </p>
            @endif
            <div class="page-body">
                <div class="card">
                    <div class="card-body">
                        <span class="text-danger error-message">
                            {{ HTML::ul($errors->all()) }}
                        </span>
                        {{ Form::model($organizationId, ['route' => ['vdmOrganization.update', $organizationId], 'method' => 'PUT', 'id' => 'updateForm']) }}
                        <h4 class="text-primary">
                            @lang('messages.General')
                        </h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('organizationId', __('messages.OrganizationName')) }}<span
                                        class="rq"></span>
                                    {{ Form::text('organizationId', $organizationId, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('mobile', __('messages.MobileNumber')) }}<span
                                        class="rq"></span>
                                    {{ Form::number('mobile', $mobile, ['class' => 'form-control', 'placeholder' => __('messages.MobileNumber'), 'required' => 'required']) }}
                                </div>
                                <div class="form-group mt-2">
                                    <div class="form-row mt-3">
                                        {{ Form::label('ShowLiveSite', __('messages.ShowSites(InWeb)'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('live', 'yes', $live == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('email', __('messages.Email')) }}<span class="rq"></span>
                                    {{ Form::email('email', $email, ['class' => 'form-control', 'placeholder' => __('messages.Email'), 'id' => 'email', 'required' => 'required']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('description', __('messages.Description')) }}
                                    {{ Form::text('description', $description, ['class' => 'form-control', 'placeholder' => __('messages.Description')]) }}
                                </div>
                                <br>

                            </div>
                        </div>
                        <br>
                        <h4 class="text-primary">{{ __('messages.AlertConfiguration') }}</h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class=" form-group">
                                    <div class="form-row mt-3 ">
                                        {{ Form::label('ParkingAlert', __('messages.ParkingAlert'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('parkingAlert', 'yes', $parkingAlert == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="form-row mt-3 ">
                                        {{ Form::label('IdleAlert', __('messages.IdleAlert'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('idleAlert', 'yes', $idleAlert == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="form-row mt-3 ">
                                        {{ Form::label('OverSpeedAlert', __('messages.OverSpeedAlert'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('overspeedalert', 'yes', $overspeedalert == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="form-row mt-3">
                                        {{ Form::label('SafetyMovementAlert', __('messages.UnauthorisedMovementAlert'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('safemove', 'yes', $safemove == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="form-row  mt-3">
                                        {{ Form::label('SOSAlert', __('messages.SOSAlert'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('sosAlert', 'yes', $sosAlert == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="form-row mt-3">
                                        {{ Form::label('HarshBrakingAlert', __('messages.HarshBrakingAlert'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('harshBreak', 'yes', $harshBreak == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="form-row mt-3">
                                        {{ Form::label('DailySummary', __('messages.DailySummary'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('dailySummary', 'yes', $dailySummary == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="form-row mt-3">
                                        {{ Form::label('TripPlannedTime', __('messages.TripPlannedTime'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('tripPlannedTime', 'yes', $tripPlannedTime == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class=" form-group">
                                    {{ Form::label('parkDuration', __('messages.ParkDuration(Mins)')) }}
                                    {{ Form::text('parkDuration', $parkDuration, ['class' => 'form-control', 'placeholder' => 'Park Duration (mins)']) }}
                                </div>
                                <div class=" form-group">
                                    {{ Form::label('idleDuration', __('messages.IdleDuration(Mins)')) }}
                                    {{ Form::text('idleDuration', $idleDuration, ['class' => 'form-control', 'placeholder' => 'Idle Duration (mins)']) }}
                                </div>
                                <div class=" form-group">
                                    {{ Form::label('overspeedDuration', __('messages.OverSpeedAlertDuration(Mins)')) }}
                                    {{ Form::text('overSpeedDuration', $overSpeedDuration, ['class' => 'form-control', 'placeholder' => 'OverSpeed Duration (mins)']) }}
                                </div>
                                <div class=" form-group" id="UnauthorisedMovementHrs">
                                    {{ Form::label('startTime', __('messages.UnauthorisedMovementHrs')) }}
                                    <div class="row px-2">
                                        <div class="col-md-6  form-group">
                                            {{ Form::label('startTime', __('messages.StartTime')) }}
                                            {{ Form::input('text', 'time1', $time1, ['class' => 'form-control  time', 'placeholder' => 'time', 'id' => 'time1']) }}
                                        </div>
                                        <div class="col-md-6  form-group">
                                            {{ Form::label('endTime', __('messages.EndTime')) }}
                                            {{ Form::input('text', 'time2', $time2, ['class' => 'form-control  time', 'placeholder' => 'time', 'id' => 'time2']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class=" form-group">
                                    {{ Form::label('noDataDuration', __('messages.NoDataDuration')) }}
                                    {{ Form::select('noDataDuration', ['' => 'Select Duration', '30' => '30 Mins', '45' => '45 Mins', '60' => '1 hr', '120' => ' 2 hrs', '240' => '4 hrs', '360' => '6 hrs', '480' => '8 hrs', '720' => '12 hrs', '960' => '16 hrs', '1080' => '18 hrs', '1200' => '20 hrs', '1440' => '24 hrs'], $noDataDuration, ['class' => 'form-control']) }}
                                </div>


                                <div class=" form-group">
                                    {{ Form::label('powerCutOffAlarmDuration', __('messages.PowerCutOffAlarmDuration(Mins)')) }}
                                    {{ Form::number('powerCutOffAlarmDuration', $powerCutOffAlarmDuration, ['class' => 'form-control', 'placeholder' => 'Power Cut Off Alarm Duration']) }}
                                </div>


                                <div class="form-group">
                                    {{ Form::label('routeDeviation', __('messages.RouteDeviation(Meters)')) }}
                                    {{ Form::select('routeDeviation', ['' => '--- Select ---', '100' => '100m', '500' => '500m', '1000' => '1000m'], $routeDeviation, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <br>
                        <h4 class="text-primary"> @lang('messages.FuelAlertsConfiguration')</h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-row ">
                                        {{ Form::label('DailyDieselSummary', __('messages.DailyDieselSummary'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('dailyDieselSummary', 'yes', $dailyDieselSummary == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row ">
                                        {{ Form::label('FuelAlarm', __('messages.FuelAlarm'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('fuelAlarm', 'yes', $fuelAlarm == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row ">
                                        {{ Form::label('InstantFuelAlerts', __('messages.InstantFuelAlerts'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('instantFuel', 'yes', $instantFuel == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row ">
                                        {{ Form::label('ExcessConsumptionFilter', __('messages.ExcessConsumptionFilter'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('excessConsumptionFilter', 'yes', $excessConsumptionFilter == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row ">
                                        {{ Form::label('ExcessConsumptionFilter', __('messages.OngoingFuelAlerts'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('fuelApproximate', 'yes', $fuelApproximate == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-row ">
                                        {{ Form::label('FuelLevelBelow', __('messages.FuelLevelBelow'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('fuelLevelBelow', 'yes', $fuelLevelBelow == 'yes' ? true : false, ['class' => 'col-md-6', 'id' => 'fuelLevelBelow']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group" id='fuelValueFiled'>
                                    {{ Form::label('fuelLevelBelowValue', __('messages.FuelLevelBelow')) }}
                                    {{ Form::select('fuelLevelBelowValue', ['5' => '5', '10' => '10', '15' => '15'], $fuelLevelBelowValue, ['class' => 'form-control', 'id' => 'fValue']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('enableFuelNotification', 'Fuel Notification Alert') }}
                                    {{ Form::select('enableFuelNotification', ['always' => 'Always', 'idleOrParked' => 'Idle/Parked'], $enableFuelNotification, ['class' => 'form-control']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('fuelAlertGeoFence', __('messages.GeoFenceFuelAlert')) }}
                                    {{ Form::select('fuelAlertGeoFence', ['default' => 'Both', 'Inside' => 'Site Inside', 'Outside' => 'Site OutSide'], $fuelAlertGeoFence, ['class' => 'form-control', 'id' => 'fuelAlertGeoFence']) }}
                                </div>

                            </div>

                        </div>
                        <br>
                        <h4 class="text-primary">{{ __('messages.SMSAlertsConfiguration') }}</h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('smsSender', __('messages.SMSSender')) }}
                                    {{ Form::text('smsSender', $smsSender, ['class' => 'form-control', 'placeholder' => 'Sms Sender']) }}
                                </div>
                                <div class="form-group" id='textLocalSmsKeyFiled'>
                                    {{ Form::label('textLocalSmsKey', __('messages.TextLocalSmsKey')) }}<span
                                        class="rq"></span>
                                    {{ Form::text('textLocalSmsKey', $textLocalSmsKey, ['class' => 'form-control', 'id' => 'textLocalSmsKey', 'placeholder' => __('messages.TextLocalSmsKey')]) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('providerUserName', __('messages.ProviderUserName')) }}
                                    {{ Form::text('providerUserName', $providerUserName, ['class' => 'form-control', 'placeholder' => 'Provider Name']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('smsPattern', __('messages.SMSPattern')) }}
                                    {{ Form::select('smsPattern', ['nill' => 'Select SMS Pattern', 'type1' => 'Type 1', 'type2' => 'Type 2', 'type3' => 'Type 3'], $smsPattern, ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('smsProvider', __('messages.SMSProvider')) }}
                                    {{ Form::select('smsProvider', $smsP, $smsProvider, ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true', 'placeholder' => 'SMS Provider']) }}
                                </div>
                                <div class="form-group" id="smsEntityNameFiled">
                                    {{ Form::label('smsEntityName', __('messages.SMSEntityName')) }}<span
                                        class="rq"></span>
                                    {{ Form::text('smsEntityName', $smsEntityName, ['class' => 'form-control', 'placeholder' => 'SMS Entity Name', 'id' => 'smsEntityName']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('providerPassword', __('messages.ProviderPassword')) }}
                                    {{ Form::input('password', 'providerPassword', $providerPassword, ['class' => 'form-control', 'placeholder' => 'Provider Password']) }}
                                </div>
                                <div class="form-group">
                                    <div class="form-row mt-3">
                                        {{ Form::label('SmsEscalation', __('messages.SmsEscalation'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('escalatesms', 'yes', $escalatesms == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" id="escalateList">
                                <hr>
                                <div class="row px-2">
                                    <div class="col-sm-3 ml-auto text-center">
                                        <div class="form-group">
                                            <h5 class="b">
                                                @lang('messages.Duration(InHours)')
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 text-center">
                                        <div class="form-group">
                                            <h5 class="b">
                                                @lang('messages.MobileNumber')
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <button class="btn btn-dark btn-sm education_fields" type="button">
                                                <i class="fas fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div id="education_fields"> </div>
                                @foreach ($duration as $key => $value)
                                    <div class="form-group removeclass{{ $value }}">
                                        <div class="row" id="{{ $value }}">
                                            <div class="col-sm-3 nopadding ml-auto">
                                                <div class="form-group">
                                                    <input type="number" class="form-control" id="duration"
                                                        required="required" name="duration[]" min=1 max=24
                                                        value="{{ $value }}" multiple placeholder="Duration">
                                                </div>
                                            </div>
                                            <div class="col-sm-5 nopadding">
                                                <div class="form-group">
                                                    <input type="number" class="form-control numberOnly" id="smsmobile"
                                                        required="required" name="smsmobile[]"
                                                        value="{{ Arr::get($smsmobile, $value) }}"
                                                        placeholder="Mobile Number">
                                                </div>
                                            </div>
                                            <div class="col-sm-3 nopadding">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-btn">
                                                            <button class="btn btn-danger btn-sm" type="button"
                                                                onclick="remove_education_fields('{{ $value }}');">
                                                                <i class="fas fa-minus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <br>
                        <h4 class="text-primary">{{ __('messages.RFIDConfiguration') }}</h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-row mt-3">
                                        {{ Form::label('ISRFID', __('messages.ISRFID'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('isRfid', 'yes', $isRfid == 'yes' ? true : false, ['class' => 'col-md-6', 'id' => 'isrfid']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="pickup" style="display: none;">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('pickup', __('messages.PickUp'), ['class' => 'badge badge-primary text-white']) }}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {{ Form::label('StartTime', __('messages.StartTime')) }}<span
                                                            class="rq"></span>
                                                        {{ Form::input('text', 'PickupStartTime', $PickupStartTime, ['class' => 'form-control time', 'id' => 'PickupStartTime']) }}

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {{ Form::label('EndTime', __('messages.EndTime')) }}<span
                                                            class="rq"></span>
                                                        {{ Form::input('text', 'PickupEndTime', $PickupEndTime, ['class' => 'form-control time', 'id' => 'PickupEndTime']) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="drop" style="display: none;">
                                            {{ Form::label('drop', __('messages.Drop'), ['class' => 'badge badge-primary text-white']) }}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {{ Form::label('StartTime', __('messages.StartTime')) }}<span
                                                            class="rq"></span>
                                                        {{ Form::input('text', 'DropStartTime', $DropStartTime, ['class' => 'form-control time', 'id' => 'DropStartTime']) }}

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {{ Form::label('EndTime', __('messages.EndTime')) }}<span
                                                            class="rq"></span>
                                                        {{ Form::input('text', 'DropEndTime', $DropEndTime, ['class' => 'form-control time', 'id' => 'DropEndTime']) }}

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>
                        <h4 class="text-primary">{{ __('messages.GeofenceConfiguration') }}</h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-row mt-3">
                                        {{ Form::label('SchoolGeoFencesd', __('messages.SchoolGeoFence'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('SchoolGeoFence', 'yes', $SchoolGeoFence == 'yes' ? true : false, ['class' => 'col-md-6', 'id' => 'SchoolGeoFence']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('geofence', __('messages.Geofence')) }}
                                    {{ Form::select('geofence', ['nill' => '-- Select --', 'yes' => 'Site Entry', 'no' => 'Site Exit'], $geofence, ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('sendGeoFenceSMS', __('messages.SendGeofenceSMS')) }}
                                    {{ Form::select('sendGeoFenceSMS', ['no' => '-- Select  --', 'yes' => 'Both', 'Entry' => 'Entry', 'Exit' => 'Exit'], $sendGeoFenceSMS, ['class' => 'form-control', 'id' => 'sendGeoFenceSMS']) }}
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group" id='Entry'>
                                    {{ Form::label('Entry', __('messages.Entry')) }}
                                    <hr>
                                    <div class="form-group">
                                        {{ Form::label('slot1', __('messages.Slot1'), ['class' => 'badge badge-primary text-white']) }}
                                        <div class="row" id='rowtime1'>
                                            <div class="col-md-6 form-group">
                                                {{ Form::label('morningEntryStartTime', __('messages.MorningEntryStartTime')) }}<span
                                                    class="rq"></span>
                                                {{ Form::input('text', 'morningEntryStartTime', $morningEntryStartTime, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'morningEntryStartTime']) }}
                                            </div>
                                            <div class="col-md-6 form-group">
                                                {{ Form::label('morningEntryEndTime', __('messages.MorningEntryEndTime')) }}<span
                                                    class="rq"></span>
                                                {{ Form::input('text', 'morningEntryEndTime', $morningEntryEndTime, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'morningEntryEndTime']) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('slot2', __('messages.Slot2'), ['class' => 'badge badge-primary text-white']) }}
                                        <div class="row">
                                            <div class="col-md-6 form-group">
                                                {{ Form::label('eveningEntryStartTime', __('messages.EveningEntryStartTime')) }}<span
                                                    class="rq"></span>
                                                {{ Form::input('text', 'eveningEntryStartTime', $eveningEntryStartTime, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'eveningEntryStartTime']) }}
                                            </div>
                                            <div class="col-md-6 form-group">
                                                {{ Form::label('eveningEntryEndTime', __('messages.EveningEntryEndTime')) }}<span
                                                    class="rq"></span>
                                                {{ Form::input('text', 'eveningEntryEndTime', $eveningEntryEndTime, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'eveningEntryEndTime']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id='Exit'>
                                    {{ Form::label('Exit', __('messages.Exit')) }}
                                    <hr>
                                    <div class="form-group">
                                        {{ Form::label('slot1', __('messages.Slot1'), ['class' => 'badge badge-primary text-white']) }}
                                        <div class="row" id='rowtime1'>
                                            <div class="col-md-6 form-group">
                                                {{ Form::label('morningExitStartTime', __('messages.MorningExitStartTime')) }}<span
                                                    class="rq"></span>
                                                {{ Form::input('text', 'morningExitStartTime', $morningExitStartTime, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'morningExitStartTime']) }}
                                            </div>
                                            <div class="col-md-6 form-group">
                                                {{ Form::label('morningExitEndTime', __('messages.MorningExitEndTime')) }}<span
                                                    class="rq"></span>
                                                {{ Form::input('text', 'morningExitEndTime', $morningExitEndTime, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'morningExitEndTime']) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('slot2', __('messages.Slot2'), ['class' => 'badge badge-primary text-white']) }}
                                        <div class="row">
                                            <div class="col-md-6 form-group">
                                                {{ Form::label('eveningExitStartTime', __('messages.EveningExitStartTime')) }}<span
                                                    class="rq"></span>
                                                {{ Form::input('text', 'eveningExitStartTime', $eveningExitStartTime, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'eveningExitStartTime']) }}
                                            </div>
                                            <div class="col-md-6 form-group">
                                                {{ Form::label('eveningExitEndTime', __('messages.EveningExitEndTime')) }}<span
                                                    class="rq"></span>
                                                {{ Form::input('text', 'eveningExitEndTime', $eveningExitEndTime, ['class' => 'form-control time', 'placeholder' => 'time', 'id' => 'eveningExitEndTime']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h4 class="text-primary">{{ __('messages.OtherDetails') }}</h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-row mt-3">
                                        {{ Form::label('SendDataToGovernmentIP', __('messages.SendDataToGovernmentIP'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('sendDataToGovtip', 'yes', $sendDataToGovtip == 'yes' ? true : false, ['class' => 'col-md-6', 'id' => 'sendDataToGovtip']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-row mt-3">
                                        {{ Form::label('DeleteHistoryEod', __('messages.DeleteHistoryEOD'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('deleteHistoryEod', 'yes', $deleteHistoryEod == 'yes' ? true : false, ['class' => 'col-md-6']) }}
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row " id="ip-address-field">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('ipAddress', __('messages.IPAddress')) }}<span
                                                class="rq"></span>
                                            {{ Form::text('ipAddress', $ipAddress, ['class' => 'form-control', 'id' => 'ip-address', 'placeholder' => 'Enter IP Address ']) }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('ipPort', __('messages.IPPort')) }}
                                            {{ Form::text('ipPort', $ipPort, ['class' => 'form-control', 'id' => 'ip-port', 'placeholder' => 'Enter Port Number']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h4 class="text-primary">{{ __('messages.ShiftTimingConfiguration') }}
                        </h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-row mt-3 d-flex justify-content-right align-items-center">
                                        {{ Form::label('Shiftdetails', __('messages.Shiftdetails'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('seperateDetails', 'yes', $seperateDetails == 'yes' ? true : false, ['class' => 'col-md-6 customCheckbox', 'id' => 'seperateDetails']) }}
                                            <span class="slider"></span>
                                        </label>
                                        <span class="ml-5" id="addshift">
                                            <label for="addshift" class="h4 badge badge-primary text-white p-2"
                                                id="addSeperateDetailsValues">
                                                {{ __('messages.AddShift') }}
                                                <i class="fas fa-plus-circle fa-lg text-white ml-2"
                                                    style="font-size: 1.333em !important;"></i>
                                            </label>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" id="seperateDetailsValues">
                                <div class="row p-2">
                                    <div class="col-4 col-md-2 text-center">
                                        <label for="some" class="h3 text-primary b">
                                            {{ __('messages.ShiftName') }}
                                        </label>
                                    </div>
                                    <div class="col-4 col-md-2  text-center">
                                        <label for="some" class="h3 text-primary b">
                                            {{ __('messages.EntryTime') }}
                                        </label>
                                    </div>
                                    <div class="col-4 col-md-2  text-center">
                                        <label for="some" class="h3 text-primary b">
                                            {{ __('messages.ExitTime') }}
                                        </label>
                                    </div>
                                    <div class="col-8 col-md-4  text-center">
                                        <label for="some" class="h3 text-primary b">
                                            {{ __('messages.SiteName') }}
                                        </label>
                                    </div>
                                    <div class="col-4 col-md-2  text-center">
                                        <label for="some" class="h3 text-primary b">
                                            {{ __('messages.Action') }}
                                        </label>
                                        {{ Form::hidden('shiftDataCount', count($shiftData), ['id' => 'shiftDataCount']) }}
                                    </div>
                                </div>
                                @foreach ($shiftData as $key => $shift)
                                    <div class="row p-2 dataRow">
                                        <div class="col-4 col-md-2 px-0 px-md-2">
                                            <input type="text" name="shiftname{{ $key }}"
                                                value="{{ $shift['shiftName'] }}" class="form-control">
                                        </div>
                                        <div class="col-4 col-md-2 px-0 px-md-2">
                                            <input type="text" name="startTime{{ $key }}"
                                                class="form-control entrytime time shiftField"
                                                value="{{ $shift['startTime'] }}" data-id="{{ $key }}"
                                                required onchange="setEndTime(this)">
                                        </div>
                                        <div class="col-4 col-md-2 px-0 px-md-2">
                                            <input type="text" name="endTime{{ $key }}"
                                                class="form-control endTime{{ $key }} time shiftField"
                                                value="{{ $shift['endTime'] }}" required>
                                        </div>
                                        <div class="col-8 col-md-4 px-0 px-md-2 mt-2 mt-md-0">
                                            <select name="sitename{{ $key }}[]"
                                                class="form-control multipleSelectpicker shiftField" multiple="multiple"
                                                value="{{ $shift['siteName'] }}" required style="width: 100%">

                                                @foreach ($getSites as $site)
                                                    <option value="{{ $site }}"
                                                        {{ in_array($site, explode(',', $shift['siteName'])) ? 'selected' : '' }}>
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-4 col-md-2  px-0 px-md-2 mt-2 mt-md-0">
                                            <button class="btn btn-sm btn-outline-danger" type="button"
                                                onclick="removeRow(this)">
                                                @lang('messages.Remove')
                                            </button>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <br>
                        <h4 class="text-primary">{{ __('messages.ReportsTimingConfiguration') }}
                        </h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-row mt-3 d-flex justify-content-right align-items-center">
                                        {{ Form::label('Reportsdetails', __('messages.Reportsdetails'), ['class' => 'col-md-6']) }}
                                        <label class="switch">
                                            {{ Form::checkbox('EndofDayDetails', 'yes', $EndofDayDetails == 'yes' , ['class' => 'col-md-6 customCheckbox', 'id' => 'EndofDayDetails']) }}
                                            <span class="slider"></span>
                                        </label>
                                        <span class="ml-5" id="addreports">
                                            <label for="addshift" class="h4 badge badge-primary text-white p-2"
                                                id="addReportsDetailsValues">
                                                @lang('messages.AddReports')
                                                <i class="fas fa-plus-circle fa-lg text-white ml-2"
                                                    style="font-size: 1.333em !important;"></i>
                                            </label>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" id="reportsDetailsValues">
                                <div class="row p-2">
                                    
                                    <div class="col-md-3 col-4 px-0 px-md-2 text-center">
                                        <label for="some" class="h3 text-primary b">
                                            {{ __('messages.StartTime') }}
                                        </label>
                                    </div>
                                    <div class="col-md-3 col-4 px-0 px-md-2 text-center">
                                        <label for="some" class="h3 text-primary b">
                                            {{ __('messages.EndTime') }}
                                        </label>
                                    </div>
                                    
                                    <div class="col-md-3 col-4 px-0 px-md-2 text-center">
                                        <label for="some" class="h3 text-primary b">
                                            {{ __('messages.Action') }}
                                        </label>
                                        {{ Form::hidden('reportsDataCount', count($reportsData), ['id' => 'reportsDataCount']) }}
                                    </div>
                                </div>
                                @foreach ($reportsData as $key => $reports)
                                    <div class="row p-2 dataRow">
                                        <div class="col-md-3 col-4 px-0 px-md-2">
                                            <input type="text" name="fromTime[]"
                                                class="form-control entrytime time reportsField"
                                                value="{{ $reports['fromTime'] }}" data-id="{{ $key }}"
                                                required onchange="setEndTime(this)">
                                        </div>
                                        <div class="col-md-3 col-4 px-0 px-md-2">
                                            <input type="text" name="toTime[]"
                                                class="form-control endTime{{ $key }} time reportsField"
                                                value="{{ $reports['toTime'] }}" required>
                                        </div>
                                        <div class="col-md-3 col-4 px-0 px-md-2 text-center">
                                            <button class="btn btn-sm btn-outline-danger" type="button"
                                                onclick="removereportRow(this)">
                                                @lang('messages.Remove')
                                            </button>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-2 mx-auto">
                                {{ Form::submit(__('messages.UpdateOrganization'), ['class' => 'btn btn-primary', 'id' => 'submit']) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        let getOrgSite = "{{ route('ajax.getOrgSite') }}";
        let RemoveLabel = "{{ __('messages.Remove') }}";
    </script>
    <script src="{{ asset('common/js/org.js') }}"></script>
    <script src="{{ asset('common/js/editOrg.js') }}"></script>
@endpush
