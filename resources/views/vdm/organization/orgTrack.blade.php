@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="page-inner">
            <div class="hpanel">
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}</p>
                @endif
				<div class="page-header">
					<h4 class="page-title">
						@lang('messages.OrganizationTrack')
					</h4>
					<ul class="breadcrumbs">
						<li class="nav-home">
							<a href="#">
								<i class="flaticon-home"></i>
							</a>
						</li>
						<li class="separator">
							<i class="flaticon-right-arrow"></i>
						</li>
						<li class="nav-item">
							<a href="{{ route('vdmOrganization.index') }}">
							@lang('messages.Organization')
							</a>
						</li>
						<li class="separator">
							<i class="flaticon-right-arrow"></i>
						</li>
						<li class="nav-item">
							<a href="#">
								@lang('messages.OrganizationTrack')
							</a>
						</li>
					</ul>
				</div>
				<div class="page-body animated zoomIn">
					<div class="card">
						<span class="text-danger error-message">
							{{ HTML::ul($errors->all()) }}
						</span>
						<div class="card-header">
							<div class="float-left">
								<a href="{{ route('vdmOrganization.index') }}">
									<i class="fas fa-arrow-circle-left fa-2x"></i>
								</a>
							</div>
							<div class="float-right" >
								<a class="btn btn-info btn-sm text-white"
									href="{{ URL::to('vdmOrganization/' . $organizationId . '/orgTrackList') }}">
								@lang('messages.ViewOrganizationTrack')	
								</a>
							</div>
		
						</div>
						<div class="card-body">
							{{ Form::open(['url' => 'vdmOrganization/orgUpdate']) }}
							<div class="panel-body">
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-3">{{ Form::label('orgId', __('messages.OrganizationName')) }}</div>
									<div class="col-md-4">
										{{ Form::text('organizationId', $organizationId, ['class' => 'form-control', 'id' => 'orgId', 'required' => 'required', 'readonly' => 'true']) }}
									</div>
								</div>
								<br />
			
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-3">{{ Form::label('vehicleId', __('messages.VehicleID')) }}</div>
									<div class="col-md-4">
										{{ Form::select('vehicleId[]', $vehicleIdList, null, ['id' => 'vehicleList', 'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true', 'required' => 'required', 'multiple' => 'multiple']) }}
									</div>
								</div>
								<br />
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-3">{{ Form::label('trackName', __('messages.TrackName')) }}</div>
									<div class="col-md-4">
										{{ Form::text('trackName', null, ['class' => 'form-control', 'placeholder' => 'Track Name', 'id' => 'trackNameId', 'required' => 'required', 'onkeyup' => 'validate(this)']) }}
									</div>
								</div>
								<div class="col-md-12 pt-4 text-center" style="">
									{{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary', 'id' => 'submit']) }}
									<br />
								</div>
							</div>
							{{ Form::close() }}
						</div>
					</div>
				</div>
            </div>
        </div>
		@include('includes.js_create')
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        function validate(element) {
            var word = element.value
            var new1 = word.replace(/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/, "")
            element.value = new1
        }

        $(document).ready(function() {
            $("#trackNameId").on("keydown", function(e) {
                return e.which !== 32;
            });
			$('#vehicleList').selectpicker();
        });
		
		</script>
@endpush
