@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animated zoomIn">
            <div class="page-inner">
                    <div class="hpanel">
                        @if (session()->has('message'))
                            <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                                {{ session()->get('message') }}</p>
                        @endif
                        <div class="page-header">
                            <h4 class="page-title">
                                @lang('messages.OrganizationSearch')
                            </h4>
                            <ul class="breadcrumbs">
                                <li class="nav-home">
                                    <a href="#">
                                        <i class="flaticon-home"></i>
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="flaticon-right-arrow"></i>
                                </li>
                                <li class="nav-item">
                                    <a href="#">
                                        @lang('messages.OrganizationSearch')
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="page-body">
                            <div class="card">
                                <span class="text-danger error-message"> {{ HTML::ul($errors->all()) }}</span>

                                <div class="card-header">
                                    {{ Form::open(['url' => 'vdmOrganization/adhi', 'method' => 'post']) }}
                                    <div class="row p-2">
                                        <div class="col-md-6 form-group">
                                            {{ Form::label(__('messages.OrganizationSearch')) }}
                                            <i class="fa fa-info-circle fa-lg text-warning ml-auto" data-toggle="tooltip"
                                                data-placement="right" title=" Use * for getting all Organizations in the search"></i> <br>
                                            {{ Form::text('text_word', old('text_word'), ['class' => 'form-control', 'placeholder' => 'Search Organization']) }}
                                        </div>
                                        <div class="col-md-6 form-group">
                                            {{ Form::submit('Submit', ['class' => 'btn btn-primary mt-4']) }}
                                        </div>
                                    </div>
                                    
                                    {{ Form::close() }}
                                </div>
                                <div class="card-body">
                                    <div id="tabNew">
                                        <table id="example1" class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center;">
                                                    @lang('messages.ID')
                                                    </th>
                                                    <th style="text-align: center;">
                                                    @lang('messages.OrganizationID')
                                                    </th>
                                                    <th style="text-align: center;">
                                                    @lang('messages.Actions')
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($orgList as $key => $value)
                                                    <tr>
                                                        <td>{{ ++$key }}</td>
                                                        <td>{{ $value }}</td>
                                                        <!-- we will also add show, edit, and delete buttons -->
                                                        <td>
                                                            <div class="dropdown dropleft text-center">
                                                                <button
                                                                    class="btn btn-info btn-sm text-white dropdown-toggle"
                                                                    type="button" id="dropdownMenuButton{{ $key }}"
                                                                    data-toggle="dropdown" aria-haspopup="true"
                                                                    aria-expanded="false">
                                                                    @lang('messages.Action')
                                                                </button>
                                                                <div class="dropdown-menu"
                                                                    aria-labelledby="dropdownMenuButton{{ $key }}">
                                                                    <a class="btn btn-sm btn-success text-white dropdown-item mb-1"
                                                                        href="{{ URL::to('vdmOrganization/' . $value . '/edit') }}">
                                                                    @lang('messages.EditOrganization')
                                                                    </a>
                                                                    <a class="btn btn-sm btn-warning text-white dropdown-item mb-1"
                                                                        href="{{ URL::to('vdmOrganization/' . $value . '/editAlerts') }}">
                                                                    @lang('messages.EditAlerts')
                                                                    </a>
                                                                    <a class="btn btn-sm btn-success text-white dropdown-item mb-1"
                                                                        href="{{ URL::to('vdmOrganization/' . $value . '/siteNotification') }}">
                                                                    @lang('messages.SiteNotification')
                                                                    </a>
                                                                    <a class="btn btn-sm btn-info text-white dropdown-item mb-1"
                                                                        href="{{ URL::to('vdmOrganization/' . $value . '/orgTrack') }}"
                                                                        style="margin-bottom: 2%;margin-top: 2%;">
                                                                        @lang('messages.OrganizationTrack')
                                                                    </a>
                                                                    {{ Form::open(['url' => 'vdmOrganization/' . $value, 'id'=>'deleteForm'.$key]) }}
                                                                    {{ Form::hidden('_method', 'DELETE') }}
                                                                    {{ Form::button(__('messages.DeleteOrganization'), ['class' => 'btn btn-sm btn-danger text-white dropdown-item  deleteBtn','id'=>$key,'dataType'=>'Organization']) }}
                                                                    {{ Form::close() }}
                                                                </div>
                                                            </div>
                                                        </td>

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
