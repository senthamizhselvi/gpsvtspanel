@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animate-panel">

            <div class="page-inner">
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif
                <div class="page-header">
                    <h4 class="page-title">
                             @lang('messages.OrganizationTrackList')
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('vdmOrganization.index') }}">
                                @lang('messages.Organization')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ URL::to('vdmOrganization/' . $organizationId . '/orgTrack') }}">
                                @lang('messages.OrganizationTrack')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                               {{$organizationId}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="page-body">
                    <div class="card animated zoomIn">
                        <div class="card-header">
                            <div class="float-left">
                                <a href="{{ URL::to('vdmOrganization/' . $organizationId . '/orgTrack') }}">
                                    <i class="fas fa-arrow-circle-left fa-2x"></i>
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <span class="text-danger"> {{ HTML::ul($errors->all()) }}</span>
                            <div id="tabNew">
                                <table id="example1" class="table table-bordered dataTable table-head-bg-info">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">
                                                @lang('messages.TrackName')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.VehicleID')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.Actions')
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($trkNameList as $key => $value)
                                            <tr>
                                                <td>{{ $value }}</td>
                                                <td>{{ Arr::get($trkvehicleList, $value) }}</td>
                                                <!-- we will also add show, edit, and delete buttons -->
                                                <td>

                                                    <a class="btn btn-warning btn-sm "
                                                        href="{{ URL::to('vdmOrganization/' . $organizationId . '/orgTrackEdit/' . $value) }}">
                                                        @lang('messages.Edit')
                                                    </a>
                                                    <a class="btn btn-danger btn-sm"
                                                        href="{{ URL::to('vdmOrganization/' . $organizationId . '/orgTrackDelete/' . $value) }}">
                                                        @lang('messages.Delete')
                                                    </a>
                                                </td>

                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        // $('#example1').DataTable();
    </script>
@endpush
