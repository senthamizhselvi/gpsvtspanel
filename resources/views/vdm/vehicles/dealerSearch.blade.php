@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animated zoomIn">
            <div class="page-inner">

                <div class="hpanel">
                    @if (session()->has('message'))
                        <p class="alert {{ session()->get('alert-class', 'alert-warning') }}">
                            {{ session()->get('message') }}
                        </p>
                    @endif
                    <div class="page-body">
                        <div class="card">
                            <div class="card-header">
                                @lang('messages.SelecttheDealername')
                            </div>
                            <div class="card-body">
                                <span class="text-danger error-message">
                                    {{ HTML::ul($errors->all()) }}
                                </span>
                                {{ Form::open(['url' => 'vdmVehicles/findDealerList']) }}
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-2">
                                        {{ Form::label('dealerId', __('messages.DealersId').':', ['class' => 'font-weight-bold']) }}
                                    </div>
                                    <div class="col-md-3">
                                        <div class="select2-input">
                                            {{ Form::select('dealerId', $dealerArr, request()->old(''), ['class' => 'form-control selectpicker', 'data-live-search ' => 'true']) }}
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center pt-4">
                                        {{ Form::submit('Submit', ['class' => 'btn btn-md btn-primary']) }}
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function() {
            $('.selectpicker').select2();
        });
    </script>
@endpush
