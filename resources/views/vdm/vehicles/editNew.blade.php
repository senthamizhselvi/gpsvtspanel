@extends("layouts.app")
@section('content')
    
    <div id="wrapper">
        <div class="content animate-panel">
            <div class="page-inner">
                <span class="text-danger">{{ HTML::ul($errors->all()) }}</span>
                    <div class="card">
                        <div class="card-header">
                            <p class="h2">Edit Vehicle
                            </p>
                        </div>
                        <div class="card-body" style="padding: 10px 20px">
                            {{ Form::model($vehicleId, ['route' => ['vdmVehicles.update', $vehicleId], 'method' => 'PUT']) }}

                            <div class="accordion accordion-secondary" id="accordion" aria-multiselectable="true">
                                <div class="card mb-2 border">
                                    <div class="card-header py-1 px-2" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <div class="span-icon">
                                            <i class="fas fa-truck"></i>
                                        </div>
                                        <div class="span-title">
                                            Vehicle Information
                                        </div>
                                        <div class="span-mode"></div>
                                    </div>
                            
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body bg-white   p-1">
                                            <div class="panel-body">
                                                <div class="row" style="padding: 10px 20px">
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('vehicleId', 'AssetID ') }}
                                                        {{ Form::text('vehicleId', $vehicleId, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('VehicleName', 'Vehicle Name') }}
                                                        {{ Form::text('VehicleName', $refData['shortName'], ['class' => 'form-control']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('regNo', 'Vehicle Registration Number') }}
                                                        {{ Form::text('regNo', $refData['regNo'], ['class' => 'form-control']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('vehicleType', 'Vehicle Type', ['style' => 'margin-bottom:11px;']) }}
                                                        @if (session()->get('cur2') != 'ktrackAdmin')
                                                            {{ Form::select('vehicleType', ['Ambulance' => 'Ambulance', 'Bike' => 'Bike', 'Bull' => 'Bull', 'Bus' => 'Bus', 'Car' => 'Car', 'heavyVehicle' => 'Heavy Vehicle', 'PassengerVan' => 'Passenger Van', 'Pickup' => 'Pick-up', 'Truck' => 'Truck', 'Van' => 'Van','JCB'=>'JCB','Generator'=>'Generator'], $refData['vehicleType'], ['class' => 'form-control selectpicker', 'data-live-search ' => 'true']) }}
                                                        @else
                                                            {{ Form::select('vehicleType', ['' => '-- Select --', 'Bag' => 'Bag', 'Bike' => 'Bike', 'Car' => 'Car', 'PersonalTracker' => 'Personal Tracker', 'PetTracker' => 'Pet Tracker', 'Watch' => 'Watch'], 'select', ['class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true', 'required' => 'required']) }}
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('deviceModel', 'Device Model', ['style' => 'margin-bottom:11px;']) }}
                                                        {{ Form::select('deviceModel', $protocol, $refData['deviceModel'], ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('deviceId', 'Device Id / IMEI No') }}
                                                        <br />
                                                        {{ Form::text('deviceId', $refData['deviceId'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('gpsSimNo', 'GPS Sim Number') }}
                                                        {{ Form::number('gpsSimNo', $refData['gpsSimNo'], ['class' => 'form-control', 'maxlength' => 15, 'minlength' => 10]) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('oprName', 'Telecom Operator Name') }}
                                                        {{ Form::select('oprName', ['airtel' => 'Airtel', 'idea' => 'Idea', 'reliance' => 'Reliance', 'vodafone' => 'Vodafone'], $refData['oprName'], ['class' => 'form-control']) }}
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('installationDate', 'Installation Date') }}
                                                        @if ($enableVehicleExpiry == 'no' || session()->get('cur') == 'admin')
                                                            <script>
                                                                $(function() {
                                                                    $("#calendar1").datepicker({
                                                                        dateFormat: 'yy-mm-dd',
                                                                        maxDate: 0
                                                                    });
                                                                });

                                                            </script>
                                                        @endif
                                                        {{ Form::text('installationDate', $refData['installationDate'], ['id' => 'calendar1', 'class' => 'form-control', 'placeholder' => 'Installation Date']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('orgId', 'Org/College Name', ['style' => 'margin-bottom:11px;']) }}
                                                        {{ Form::select('orgId', [$orgList], $refData['orgId'], ['class' => 'form-control selectpicker w-100', 'data-live-search ' => 'true']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('onboardDate', 'Onboard Date') }}
                                                        {{ Form::text('onboardDate', $refData['onboardDate1'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('Payment_Mode1', 'Payment Mode') }}
                                                        {{ Form::select('Payment_Mode1', $Payment_Mode, $refData['Payment_Mode'], ['class' => 'form-control']) }}
                                                    </div>

                                                    @if (session()->get('cur1') != 'prePaidAdmin')
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('vehicleExpiry', 'Vehicle Expiration Date') }}
                                                            @if (($enableVehicleExpiry == 'yes' && session()->get('cur') == 'dealer') || (session()->get('cur') == 'admin' && $enableVehicleExpiry == 'yes'))
                                                                <script>
                                                                    $(function() {
                                                                        $("#calendar").datepicker({
                                                                            dateFormat: 'yy-mm-dd',
                                                                            minDate: 0
                                                                        });
                                                                    });

                                                                </script>
                                                                {{ Form::text('vehicleExpiry', $refData['vehicleExpiry'], ['id' => 'calendar', 'class' => 'form-control']) }}
                                                            @else
                                                                {{ Form::text('vehicleExpiry', $refData['vehicleExpiry'], ['id' => 'calendar', 'class' => 'form-control', 'readonly' => 'true']) }}
                                                            @endif
                                                        </div>
                                                    @else
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('vehicleExpiry1', 'Licence Expiration Date') }}
                                                            {{ Form::text('vehicleExpiry', $refData['vehicleExpiry'], ['id' => 'calendar', 'class' => 'form-control', 'readonly' => 'true']) }}
                                                        </div>
                                                    @endif
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('paymentType', 'Payment Type') }}
                                                        {{ Form::text('paymentType', $refData['paymentType'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('licenceissuedDate', 'Licence Issued Date') }}
                                                        {{ Form::text('licenceissuedDate', $refData['licenceissuedDate'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('License1', 'Licence') }}
                                                        {{ Form::select('Licence1', $Licence, $refData['Licence'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('License1', 'Kalman Filter') }}
                                                        {{ Form::select('kalmanFilter',['yes' => 'Yes', 'no' => 'No'], $refData['kalmanFilter'], ['class' => 'form-control']) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if ((session()->get('cur1') == 'prePaidAdmin' && $refData['Licence'] != 'Basic') || session()->get('cur1') != 'prePaidAdmin')
                                    <div class="card mb-2 border">
                                        <div class="card-header collapsed py-1 px-2" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <div class="span-icon">
                                                <i class="fas fa-gas-pump"></i>
                                            </div>
                                            <div class="span-title">
                                                Fuel Sensor Details
                                            </div>
                                            <div class="span-mode"></div>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="card-body bg-white   p-1  ">
                                                <div class="panel-body">
                                                    <div class="row" style="padding: 10px 20px">
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('fuelMode', __('messages.fuelMode')) }}
                                                            {{ Form::select('fuelMode', $fuelModeList, $refData['fuelMode'], ['class' => 'form-control selectpicker', 'data-live-search ' => 'true']) }}
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('sensorCount', 'Sensor Count') }}
                                                            {{ Form::number('sensorCount', $refData['sensorCount'], ['id' => 'sensorcount', 'class' => 'form-control', 'readonly' => 'true']) }}
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('noOfTank', 'Number of Tank') }}
                                                            {{ Form::number('noOfTank', $refData['noOfTank'], ['id' => 'noOfTank', 'class' => 'form-control', 'readonly' => 'true']) }}
                                                        </div>
                                                        <div class="form-group col-md-6" id="fuelType">
                                                            {{ Form::label('fuelType', 'Fuel Type') }}<br>
                                                            {{ Form::select('fuelType', ['' => 'None', 'analog' => 'Analog', 'serial1' => 'LLS Fuel 1(Serial)', 'serial2' => 'LLS Fuel 2'], $refData['fuelType'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                                        </div>
                                                        <div class="form-group col-md-6" id="vehicleMode">
                                                            {{ Form::label('vehicleMode', 'Mode of Vehicle') }}
                                                            {{ Form::select('vehicleMode', ['' => 'None', 'MovingVehicle' => 'Moving Vehicle', 'Machinery' => 'Machinery', 'Dispenser' => 'Dispenser', 'DG' => 'Diesel Generator(DG)'], $refData['vehicleMode'], ['class' => 'form-control', 'disabled' => 'true']) }}
                                                        </div>
                                                        <div class="form-group col-md-6" id="showtanksize">
                                                            {{ Form::label('tankSize', 'Tank Size') }}
                                                            {{ Form::number('tankSize', isset($refData['tankSize']) ? $refData['tankSize'] : '', ['id' => 'tanksize', 'class' => 'form-control', 'readonly' => 'true']) }}
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('ignitionSpeed', 'Speed ') }}
                                                            {{ Form::select('ignitionSpeed', ['' => '-- Select --', '5' => '5', '10' => '10', '15' => '15', '20' => '20'], $refData['ignitionSpeed'], ['class' => 'form-control']) }}
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('tankInterlinked', 'Tank Interlinked') }}<br>
                                                            {{ Form::select('tankInterlinked', ['yes' => 'Yes', 'no' => 'No'], isset($refData['tankInterlinked']) ? $refData['tankInterlinked'] : 'no', ['class' => 'form-control']) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="card mb-2 border">
                                    <div class="card-header collapsed py-1 px-2" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <div class="span-icon">
                                            <i class="fas fa-clipboard-check"></i>
                                        </div>
                                        <div class="span-title">
                                            Configuration Details
                                        </div>
                                        <div class="span-mode"></div>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body bg-white  p-1  ">
                                            <div class="panel-body">
                                                <div class="row" style="padding: 10px 20px">
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('odoDistance', 'Odometer Reading') }}
                                                        {{ Form::text('odoDistance', $refData['odoDistance'], ['class' => 'form-control']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('overSpeedLimit', 'OverSpeed Limit') }}
                                                        {{ Form::number('overSpeedLimit', $refData['overSpeedLimit'], ['class' => 'form-control', 'min' => '1']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('expectedMileage', 'Expected Mileage') }}
                                                        {{ Form::text('expectedMileage', $refData['expectedMileage'], ['class' => 'form-control', 'placeholder' => 'Expected Mileage Value']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('DistManipulationPer', 'Distance Manipulation (%)') }}
                                                        {{ Form::number('DistManipulationPer', $refData['DistManipulationPer'], ['class' => 'form-control', 'placeholder' => 'Distance Manipulation Percentage', 'step' => '0.001']) }}
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group col-md-4">
                                                            {{ Form::label('defaultMileage', 'Device Odo') }}<br />
                                                            {{ Form::radio('defaultMileage', 'yes', $refData['defaultMileage'] == 'yes') }}
                                                            Yes
                                                            &nbsp;&nbsp;&nbsp;
                                                            {{ Form::radio('defaultMileage', 'no', $refData['defaultMileage'] == 'no') }}
                                                            No
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            {{ Form::label('assetTracker', 'Asset Tracker') }}<br>
                                                            {{ Form::radio('assetTracker', 'yes', $refData['assetTracker'] == 'yes') }}
                                                            Yes
                                                            &nbsp;&nbsp;&nbsp;
                                                            {{ Form::radio('assetTracker', 'no', $refData['assetTracker'] == 'no') }}
                                                            No
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            {{ Form::label('safetyParking', 'Safety Parking') }}<br>
                                                            {{ Form::radio('safetyParking', 'yes', $refData['safetyParking'] == 'yes') }}
                                                            Yes
                                                            &nbsp;&nbsp;&nbsp;
                                                            {{ Form::radio('safetyParking', 'no', $refData['safetyParking'] == 'no') }}
                                                            No
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group col-md-12">
                                                            {{ Form::label('fuelmode', 'Engine ON') }}</br>
                                                            {{ Form::radio('VolIg', 'Voltage', $vol == 'Voltage') }}
                                                            Voltage
                                                            &nbsp;&nbsp;&nbsp;
                                                            {{ Form::radio('VolIg', 'Ignition', $vol == 'Ignition') }}
                                                            Ignition
                                                            &nbsp;&nbsp;&nbsp;
                                                            {{ Form::radio('VolIg', 'Voltage_Ignition', $vol == 'Voltage_Ignition') }}
                                                            Voltage+Ignition
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('ac', 'Secondary Engine ') }}
                                                        {{ Form::select('ac', ['digital1' => 'Digital Input1', 'analog1' => 'Analog Input1'], $ac, ['class' => 'form-control', 'id' => 'acvalue']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('batteryvolt', 'Vehicle Battery Voltage') }}
                                                        {{ Form::number('batteryvolt', $batteryvolt, ['class' => 'form-control', 'min' => '1', 'step' => '0.1']) }}
                                                    </div>
                                                    <div class="form-group col-md-6" id="acvolt1">
                                                        {{ Form::label('acVolt', 'Voltage') }}
                                                        {{ Form::number('acVolt', $acVolt, ['class' => 'form-control', 'min' => '1', 'step' => '0.01', 'id' => 'acVoltvalue']) }}
                                                    </div>
                                                    @if ((session()->get('cur1') == 'prePaidAdmin' && $refData['Licence'] != 'Basic') || session()->get('cur1') != 'prePaidAdmin')
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('analog1', 'Analog input 1 / Enable Fuel') }}
                                                            {{ Form::select('analog1', ['no' => 'No', 'fuel' => 'Fuel', 'load' => 'Load'], $refData['analog1'], ['id' => 'analog', 'class' => 'form-control']) }}
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('analog2', 'Analog input 2') }}
                                                            {{ Form::select('analog2', ['no' => 'No', 'fuel' => 'Fuel', 'load' => 'Load'], $refData['analog2'], ['class' => 'form-control']) }}
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('digital1', 'Digital input 1') }}
                                                            {{ Form::select('digital1', ['no' => 'No', 'ac' => 'Ac', 'hire' => 'Hire', 'door' => 'Door'], $refData['digital1'], ['class' => 'form-control']) }}
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('digital2', 'Digital input 2') }}
                                                            {{ Form::select('digital2', ['no' => 'No', 'ac' => 'Ac', 'hire' => 'Hire', 'door' => 'Door'], $refData['digital2'], ['class' => 'form-control']) }}
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('serial1', 'Serial input 1') }}
                                                            {{ Form::select('serial1', ['no' => 'No', 'temperature' => 'Temperature', 'rfid' => 'Rfid', 'camera' => 'Camera', 'passenger' => 'Passenger'], $refData['serial1'], ['class' => 'form-control']) }}
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('serial2', 'Serial input 2') }}
                                                            {{ Form::select('serial2', ['no' => 'No', 'temperature' => 'Temperature', 'rfid' => 'Rfid', 'camera' => 'Camera', 'passenger' => 'Passenger'], $refData['serial2'], ['class' => 'form-control']) }}
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('digitalout', 'Digital output') }}
                                                            {{ Form::select('digitalout', ['no' => 'No', 'yes' => 'Yes'], $refData['digitalout'], ['class' => 'form-control']) }}
                                                        </div>
                                                    @endif
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('enable', 'Enable debugs') }}
                                                        {{ Form::select('enable', ['Disable' => 'Disable', 'Enable' => 'Enable'], $enable, ['class' => 'form-control', 'id' => 'enable', 'required' => 'required']) }}
                                                    </div>
                                                    @if ((session()->get('cur1') == 'prePaidAdmin' && ($refData['Licence'] == 'Premium' || $refData['Licence'] == 'PremiumPlus')) || session()->get('cur1') != 'prePaidAdmin')
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('isRF', 'Is RFID') }}<br>
                                                            {{ Form::select('isRfid', ['yes' => 'Yes', 'no' => 'No'], isset($refData['isRfid']) ? $refData['isRfid'] : 'no', ['class' => 'form-control']) }}
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('rfidType', 'Rfid Type') }}<br>
                                                            {{ Form::select('rfidType', ['type1' => 'Type1', 'type2' => 'Type2', 'type3' => 'Type3 (234 reverse)'], isset($refData['rfidType']) ? $refData['rfidType'] : 'no', ['class' => 'form-control']) }}
                                                        </div>
                                                    @endif
                                                    @if ((session()->get('cur1') == 'prePaidAdmin' && $refData['Licence'] != 'Basic') || session()->get('cur1') != 'prePaidAdmin')
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('mintem', 'Minimum Temperature') }}
                                                            {{ Form::number('mintemp', $refData['mintemp'], ['class' => 'form-control', 'placeholder' => 'Quantity', 'min' => '-100']) }}
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            {{ Form::label('maxtem', 'Maximum Temperature') }}
                                                            {{ Form::number('maxtemp', $refData['maxtemp'], ['class' => 'form-control', 'placeholder' => 'Quantity', 'min' => '-100']) }}
                                                        </div>
                                                    @endif

                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('ipAddress', 'IP Address') }}
                                                        {{ Form::text('ipAddress', $refData['ipAddress'], ['class' => 'form-control']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('portNo', 'Port Number') }}
                                                        {{ Form::text('portNo', $refData['portNo'], ['class' => 'form-control']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('communicatingPortNo', 'Communicating Port No') }}
                                                        {{ Form::text('communicatingPortNo', $refData['communicatingPortNo'], ['class' => 'form-control', 'readonly' => 'true']) }}
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('countryTimezone', 'Country Timezone', ['style' => 'margin-bottom:11px;']) }}
                                                        <br>
                                                        {{ Form::select('countryTimezone', $countryTimezoneList, $refData['countryTimezone'], ['class' => 'selectpicker form-control', 'data-live-search ' => 'true', 'style' => 'width:100%;']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('morningTripStartTime', 'DCODE') }}
                                                        {{ Form::text('morningTripStartTime', $refData['morningTripStartTime'], ['class' => 'form-control']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('eveningTripStartTime', 'TIMEZONE') }}
                                                        {{ Form::select('eveningTripStartTime', ['TIMEZONE' => 'TIMEZONE', 'INDIA' => 'INDIA', 'CHINA' => 'CHINA', 'GMT' => 'GMT'], $refData['eveningTripStartTime'], ['class' => 'form-control']) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-2 border">
                                    <div class="card-header collapsed py-1 px-2" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                        <div class="span-icon">
                                            <i class="far fa-id-card"></i>
                                        </div>
                                        <div class="span-title">
                                            Driver Details
                                        </div>
                                        <div class="span-mode"></div>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                        <div class="card-body bg-white p-1 ">
                                            <div class="panel-body">
                                                <div class="row" style="padding: 10px 20px">
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('driverName', 'Driver Name') }}
                                                        {{ Form::text('driverName', $refData['driverName'], ['class' => 'form-control']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('driverMobile', 'Driver Mobile No') }}
                                                        {{ Form::number('driverMobile', $refData['driverMobile'], ['class' => 'form-control']) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-2 border">
                                    <div class="card-header collapsed py-1 px-2" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                                        <div class="span-icon">
                                            <i class="fas fa-share"></i>
                                        </div>
                                        <div class="span-title">
                                            Other Details
                                        </div>
                                        <div class="span-mode"></div>
                                    </div>
                                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                        <div class="card-body bg-white   p-1  ">
                                            <div class="panel-body">
                                                <div class="row" style="padding: 10px 20px">
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('madeIn', 'Made In') }}<span
                                                            class="badge badge-primary pull-right">NEW</span>
                                                        {{ Form::text('madeIn', $refData['madeIn'], ['class' => 'form-control', 'min' => '1']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('manufacturingDate', 'Manufacturing date') }}<span
                                                            class="badge badge-primary pull-right">NEW</span>
                                                        <script>
                                                            $(function() {
                                                                // $( "#calendar" ).datepicker();   
                                                                $("#calendardate").datepicker({
                                                                    dateFormat: 'yy-mm-dd',
                                                                    maxDate: 0
                                                                });
                                                            });

                                                        </script>
                                                        {{ Form::text('manufacturingDate', $refData['manufacturingDate'], ['id' => 'calendardate', 'class' => 'form-control']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('chassisNumber', 'Chassis Number') }}<span
                                                            class="badge badge-primary pull-right">NEW</span>
                                                        {{ Form::text('chassisNumber', $refData['chassisNumber'], ['class' => 'form-control', 'min' => '1']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('gpsSimICCID', 'GPS Sim ICCID') }}<span
                                                            class="badge badge-primary pull-right">NEW</span>
                                                        {{ Form::number('gpsSimICCID', $refData['gpsSimICCID'], ['class' => 'form-control', 'maxlength' => 20, 'minlength' => 10]) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('altShort', 'Alternate Vehicle Name') }}
                                                        {{ Form::text('altShortName', $refData['altShortName'], ['class' => 'form-control']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('route', 'Route Name') }}
                                                        {{ Form::select('routeName', $routeName, $refData['routeName'], ['class' => 'form-control']) }}
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {{ Form::label('descriptionStatus', 'Description / Remarks') }}
                                                        {{ Form::text('descriptionStatus', $refData['descriptionStatus'], ['class' => 'form-control']) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-md-3 mx-md-auto d-flex " >
                                    <a class="btn btn-warning mx-1"
                                        href="{{ URL::to('VdmVehicleScan' . $vehicleId) }}"
                                        style="">Back</a>
                                        
                                    {{ Form::submit('Update Vehicle', ['id' => 'sub', 'class' => 'btn btn-primary mx-1']) }}
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
            </div>
        </div>
    </div>
    @if ((session()->get('cur1') == 'prePaidAdmin' && $refData['Licence'] != 'Basic') || session()->get('cur1') != 'prePaidAdmin')
        <script type="text/javascript">
            $(document).ready(() => {
                var datas = {
                    'val1': $('#enable').val(),
                    'val2': $('#noOfTank').val(),
                    'val3': $('#sensorcount').val()
                }
                document.getElementById("showtanksize").style.display = datas.val2 == '1' ? 'block' : 'none';
                document.getElementById("fuelType").style.display = datas.val2 == '1' ? 'block' : 'none';
                document.getElementById("vehicleMode").style.display = datas.val2 == '1' ? 'block' : 'none';
            })

        </script>
    @endif
@endsection
@push('js')
    <script type="text/javascript">
        $(function() {
            $('.selectpicker').select2();
        });

        $('#sub').on('click', function() {

            var datas = {
                'val': $('#tanksize').val(),
                'val1': $('#analog').val(),
                'val2': $('#acvalue').val()
            }
            if (datas.val != '0' && datas.val != '') {
                document.getElementById("analog").value = "fuel";
            } else if (datas.val2 == 'analog1') {
                document.getElementById("acVoltvalue").required = "true";
            } else if (datas.val2 == 'digital1') {
                document.getElementById("acVoltvalue").required = "";
                document.getElementById("acvolt1").style.display = "none";


            }
        });


        $('#analog').on('change', function() {
            var input = {
                'val': $('#analog').val()
            }

            if (input.val == 'no' || input.val == 'load') {

                document.getElementById("tanksize").readOnly = true;
                document.getElementById("tanksize").value = '';
            } else {
                document.getElementById("tanksize").readOnly = false;
                document.getElementById("tanksize").value = '';
            }

        });
        $('#acvalue').on('change', function() {

            var datas = {
                'val': $('#acvalue').val()
            }
            if (datas.val == 'analog1') {

                document.getElementById("acVoltvalue").required = "true";
                document.getElementById("acvolt1").style.display = "block";
            } else if (datas.val == 'digital1') {
                document.getElementById("acVoltvalue").required = "";
                document.getElementById("acvolt1").style.display = "none";

            }
        });

        $('#noOfTank').on('change', function() {
            var datas = {
                'val': $('#noOfTank').val()
            }
            if (datas.val == '1') {
                document.getElementById("vehicleMode").style.display = "block";
                document.getElementById("showtanksize").style.display = "block";
                document.getElementById("fuelType").style.display = "block";
            } else {
                document.getElementById("vehicleMode").style.display = "none";
                document.getElementById("showtanksize").style.display = "none";
                document.getElementById("fuelType").style.display = "none";
            }
        });

        window.onload = function() {
            var datas = {
                'val': $('#acvalue').val(),
                'val1': $('#enable').val(),
                'val2': $('#noOfTank').val(),
                'val3': $('#sensorcount').val()
            }

            document.getElementById("acvolt1").style.display = datas.val !== 'digital1' ? 'block' : 'none';
        }

    </script>
@endpush
