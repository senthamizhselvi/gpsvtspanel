@extends("layouts.app")
@section('content')


    <div id="wrapper">
        <div class="page-inner">
            <div class="hpanel">
                <div class="pull-right">
                    <a href="{{ route('vehicleStatusXls') }}">
                        <img class="pull-right" width="40px" height="50px" src="assets/imgs/xls.svg" method="get" />
                    </a>
                </div>
                <div class="panel-heading">
                    @if (session()->has('message'))
                        <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                            {{ session()->get('message') }}
                        </p>
                    @endif
                    <div class="page-header">
                        <h4 class="page-title">
                            @lang('messages.VehiclesStatus')
                        </h4>
                    </div>
                    <span class="text-danger">
                        {{ HTML::ul($errors->all()) }}
                    </span>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="tabNew" style="font-size: 11px; overflow-y: auto;">
                        <table id="vehicleStatus" class="table table-bordered dataTable table-head-bg-info w-100">
                            <thead>
                                <tr>
                                    <th style="text-align: center;" data-name="AssetID">
                                        @lang('messages.AssetID')
                                    </th>
                                    <th style="text-align: center;" data-name="Vehicle Name">
                                        @lang('messages.DeviceID')
                                    </th>
                                    <th style="text-align: center;" data-name="Device ID">
                                        @lang('messages.DeviceID')
                                    </th>
                                    <th style="text-align: center;" data-name="Org Name">
                                        @lang('messages.OrgName')
                                    </th>
                                    <th style="text-align: center;" data-name="Reg No">
                                        @lang('messages.RegNo')
                                    </th>
                                    <th style="text-align: center;" data-name="Groups">
                                        @lang('messages.Groups')
                                    </th>
                                    <th style="text-align: center;" data-name="Position">
                                        @lang('messages.Position')
                                    </th>
                                    <th style="text-align: center;" data-name="Ignition Status">
                                        @lang('messages.IgnitionStatus')
                                    </th>
                                    <th style="text-align: center;" data-name="Last Comm">
                                        @lang('messages.LastComm')
                                    </th>
                                    <th style="text-align: center;" data-name="Last Loc">
                                        @lang('messages.LastLoc')
                                    </th>
                                    <th style="text-align: center;" data-name="G-Map">
                                        @lang('messages.G-Map')
                                    </th>
                                    <th style="text-align: center;" data-name="Address">
                                        @lang('messages.Address')
                                    </th>
                                    <th style="text-align: center;" data-name="Speed">
                                        @lang('messages.Speed')
                                    </th>
                                </tr>
                            </thead>
                            <tbody style="word-break: break-all;">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@push('js')
    <script>
        $(document).ready(function() {
            let data = [];
            $('th').each(function(i) {
                tagName = $(this).attr('data-name')
                data[i] = {
                    data: tagName,
                    name: tagName
                };
            });
            $('#vehicleStatus').DataTable({
                // 'oLanguage': {
                //             'sProcessing': "<img src='assets/imgs/ajax-load.gif'>"
                //         },
                "oLanguage": {
                    "sEmptyTable": "{{ __('site.no_data', ['attr' => 'user']) }}"
                },
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'data': {
                        "_token": "{{ csrf_token() }}",
                    },
                    'url': '{{ route('ajax.vehicleStatus') }}',
                },
                'columns': data,
                "columnDefs": [{ // set default column settings
                        'orderable': false,
                        'targets': [-1]
                    },
                    {
                        "searchable": false,
                        "targets": [-1]
                    }
                ],
                "order": [
                    [4, "desc"]
                ]
            });
        });




        function ConfirmDelete() {
            var x = confirm("Confirm to remove?");
            if (x)
                return true;
            else
                return false;
        }
    </script>
    {{-- @include('includes.js_create') --}}

@endpush
