@extends("layouts.app")
@section('content')
    <div id='wrapper'>
        <div class="page-inner">
            <div class="page-header">
                <div class="page-title">
                    @lang('messages.EditCalibrate')
                </div>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('vdmVehicles.index') }}">
                            @lang('messages.VehicleList')
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                            @lang('messages.EditCalibrate')
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                            {{ $vehicleId }}
                        </a>
                    </li>
                </ul>
            </div>
            <div id='alertMsg' class="mt-3"></div>
            <div class="card">
                <div class="card-body">
                    <span class="text-danger error-message">
                        {{ HTML::ul($errors->all()) }}
                    </span>
                    {{ Form::open(['url' => route('calibrateUpdate'), 'method' => 'post', 'id' => 'submit']) }}
                    <div class="">
                        @if ($calibrateType !== 'direct')
                            <div class="mx-2">
                                <!-- tank -->
                                <div class="hpanel" id="tankConfigTab">

                                    <div class="topnav mb-3" id="tankConfig">
                                        @if ($calibrateType == 'auto')
                                            <nav class="navbar navbar-expand-lg bg-primary rounded">
                                                <a class="navbar-brand" href="#"></a>
                                                <button class="navbar-toggler" type="button" data-toggle="collapse"
                                                    data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                                    aria-label="Toggle navigation">
                                                    <span class="navbar-toggler-icon"></span>
                                                </button>
                                                <div class="collapse navbar-collapse" id="navbarNav">
                                                    <ul class="navbar-nav">
                                                        @for ($j = 1; $j < $tankCount + 1; $j++)
                                                            <li class="nav-item ">
                                                                <a id="tank{{ $j }}" name='tank{{ $j }}'
                                                                    class='nav-link checkCalibrateChanges' data-type="auto" data-count="{{$j}}" >
                                                                    @lang('messages.Tank') {{ $j }}
                                                                </a>
                                                            </li>
                                                        @endfor
                                                    </ul>
                                                    <div class="navbar-left navbar-form nav-search ml-auto" data-toggle="modal"
                                                        data-target="#caliSet">
                                                        <button type="button" class="btn btn-sm btn-danger popup"> @lang('messages.FuelConfigurationDetails') </button>
                                                    </div>
                                                </div>
                                            </nav>
                                        @endif
                                    </div>
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-md-4 form-group">
                                                {{ Form::label('vehicletype', __('messages.ModeofVehicle')) }}
                                                {{ Form::select('vehicletype', ['' => 'Select', 'MovingVehicle' => 'Moving Vehicle', 'Machinery' => 'Machinery', 'Dispenser' => 'Dispenser', 'DG' => 'Diesel Generator(DG)'], $vehicletype, ['class' => 'form-control']) }}
                                            </div>
                                            <div class="col-md-4 form-group">
                                                {{ Form::label('fuelType', __('messages.FuelType')) }}
                                                {{ Form::select('fuelType', ['' => '-- SELECT --', 'analog' => 'Analog', 'serial1' => 'LLS Fuel 1(Serial)', 'serial2' => 'LLS Fuel 2'], $fuelInput, ['class' => 'form-control', 'id' => 'fueltype']) }}
                                            </div>
                                            {{-- <div class="col-md-4 form-group">
                                                {{ Form::label('vehicletype', __('messages.sensorNumbers')) }}
                                                <select name="sensorNumbers[]" class="form-control multipleSelectpicker"
                                                    multiple="multiple" required style="width: 100%">
                                                    @for ($i = 1; $i <= $sensorCount; $i++)
                                                        <option value="{{ $i }}" @foreach ($sensorNumbers as $item)
                                                            @if ($item == $i){{ 'selected' }}@endif
                                                    @endforeach
                                                    >{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div> --}}
                                            <div class="col-md-4 form-group">
                                                {{ Form::label('maxval', __('messages.Tanksize')) }}
                                                <span style="font-size: 9px;"><b>(Note:</b> In Litres)</span>
                                                {{ Form::number('maxval', $maxval, ['class' => 'form-control', 'step' => '0.001', 'id' => 'maxval']) }}
                                            </div>
                                            <div class="col-md-4 form-group">
                                                {{ Form::label('minvolt', __('messages.Minimum(RSvalue/Voltage)')) }}
                                                {{ Form::number('minvolt', $minvolt, ['class' => 'form-control', 'step' => '0.01']) }}
                                            </div>
                                            <div class="col-md-4 form-group">
                                                {{ Form::label('maxvolt', __('messages.Maximum(RSvalue/Voltage)')) }}
                                                {{ Form::number('maxvolt', $maxvolt, ['class' => 'form-control', 'step' => '0.001', 'id' => 'maxvolt']) }}
                                            </div>

                                            <div class="col-md-4 form-group">
                                                {{ Form::label('tankshape', __('messages.FuelTankShape')) }}
                                                {{ Form::select('tankshape', ['' => '-- SELECT --', 'Rectangle' => 'Rectangle', 'Cylinder' => 'Cylinder'], $tankshape == 'Abnormal' ? '' : $tankshape, ['class' => 'form-control', 'id' => 'tankshape']) }}
                                            </div>
                                            <div class="col-md-4 form-group">
                                                {{ Form::label('tankposition', __('messages.FuelTankPosition')) }}
                                                {{ Form::select('tankposition', ['' => '-- SELECT --', 'vertical' => 'Vertical', 'horizontal' => 'Horizontal'], $tankposition, ['class' => 'form-control', 'id' => 'tankposition']) }}
                                            </div>
                                            <div class="col-md-4 form-group" id="calculateDiv">
                                                {{ Form::label('calculate', __('messages.calculate')) }}
                                                {{ Form::select('rectangelCalculate', ['no' => 'No', 'yes' => 'Yes'], $rectangelCalculate, ['class' => 'form-control', 'id' => 'calculate']) }}
                                            </div>

                                            <div class="col-md-4" id="rectangle">
                                                <div class="row">
                                                    <div class="col-4 form-group">
                                                        <label for="sl">{{ __('messages.Length') }}</label><span
                                                            style="font-size: 9px;">  (In mm)</span>
                                                        <input type="number" name="recLength" id="recLength" value="{{$recLength}}"
                                                            class="form-control" min='1' placeholder="Length">
                                                    </div>
                                                    <div class="col-4 form-group">
                                                        <label for="sl">{{ __('messages.Height') }}</label>
                                                        <span style="font-size: 9px;">  (In mm)</span>
                                                        <input type="number" name="recHeight" id="recHeight" value="{{$recHeight}}"
                                                            class="form-control" min='1' placeholder="Height">
                                                    </div>
                                                    <div class="col-4 form-group">
                                                        <label for="sl">{{ __('messages.Width') }}</label>
                                                        <span style="font-size: 9px;"> (In mm)</span>
                                                        <input type="number" name="recWidth" id="recWidth" value="{{$recWidth}}"
                                                            class="form-control" min='1' placeholder="Width">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-4" id="cylinder">
                                                <div class="row">
                                                    <div class="col-6 form-group">
                                                        {{ Form::label('radius', __('messages.Radius')) }} <span
                                                            style="font-size: 9px;">(In mm)</span>
                                                        {{ Form::number('radius', $radius, ['class' => 'form-control', 'min' => '1', 'id' => 'r']) }}
                                                    </div>
                                                    <div class="col-6 form-group">
                                                        {{ Form::label('Cylength', __('messages.Length')) }} <span
                                                            style="font-size: 9px;">(In mm)</span>
                                                        {{ Form::number('Cylength', $Cylength, ['class' => 'form-control', 'min' => '1', 'id' => 'cl']) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 showFrequencyBtn ">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="button" value="{{__("messages.ShowFrequency")}}" id="showFrequencyBtn"
                                                        class="btn btn-primary btn-sm mt-1 showFrequencyBtn btn-round" style="display: none">
                                                    <button type="button" class="btn btn-dark btn-sm mt-1 showFrequencyBtn btn-round"
                                                    id="showFrequencySync"><i class="fas fa-sync-alt"></i></button>
                                                    <button type="button" class="btn btn-sm mt-1 btn-success btn-round" id="autoExcel">
                                                        <i class="fas fa-download"></i>
                                                    </button>
                                                </div>
                                                <div class="col-md-12 mt-2">
                                                    <div class="literVoltList" style="display: none">
                                                        <p class="h3 text-primary">
                                                            @lang('messages.LitreandVolt')
                                                        </p>
                                                        <div id="listCalculate" class="row px-3">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 border-left ml-auto">
                                            <div>
                                                <div class="tankdraw mt-5">
                                                    <div class="Cylinder tank-anime" style="display: none;">
                                                        <div class="vertical-tank position-relative">
                                                            <div class="tank">
                                                                <span class="fuel-value"></span>
                                                                <div class="fuel"></div>
                                                            </div>
                                                            <div class="length-value"></div>
                                                            <div class="radius-value"></div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="Rectangle tank-anime" style="display: none;">
                                                        <div class="vertical-tank">
                                                            <div class="tank">
                                                                <span class="fuel-value"></span>
                                                                <div class="fuel"></div>
                                                            </div>
                                                        </div>
                                                        <div class="horizontal-tank">
                                                            <div class="tank">
                                                                <span class="fuel-value"></span>
                                                                <div class="fuel"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                   
                                    <div class="row mt-3">
                                        <div class="col-md-4 text-center mx-auto">
                                            <a class="btn btn-danger" href="{{ URL::to('VdmVehicleScan' . $vehicleId) }}">
                                                @lang('messages.Cancel')
                                            </a>
                                            {{ Form::submit(__("messages.Save"), ['id' => 'sub', 'class' => 'btn btn-primary ml-1']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <!-- sensor -->
                        @if ($calibrateType == 'manual')
                            <div class="hpanel" id="sensorConfigTab">
                                <div class="topnav mb-3" id="sensorConfig">
                                    <nav class="navbar navbar-expand-lg bg-primary rounded">
                                        <a class="navbar-brand" href="#"></a>
                                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                            data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                            aria-label="Toggle navigation">
                                            <span class="navbar-toggler-icon"></span>
                                        </button>
                                        <div class="collapse navbar-collapse" id="navbarNav">
                                            <ul class="navbar-nav">
                                                @for ($k = 1; $k < $sensorCount + 1; $k++)
                                                    <li class="nav-item" >
                                                        <a id="sensor{{ $k }}" name='sensor{{ $k }}' 
                                                        class=' nav-link checkCalibrateChanges' data-type="manual" data-count="{{$k}}">
                                                            @lang('messages.Sensor')
                                                            {{ $k }}
                                                        </a>
                                                    </li>
                                                @endfor
                                            </ul>
                                            <div class="navbar-left navbar-form nav-search ml-auto" data-toggle="modal"
                                                data-target="#caliSet">
                                                <button type="button" class="btn btn-sm btn-danger popup"> @lang('messages.FuelConfigurationDetails') </button>
                                            </div>
                                        </div>
                                    </nav>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('tankId', __('messages.SelectTankId')) }}
                                            {{ Form::select('tankId', $tankList, $tankId, ['class' => 'form-control', 'id' => 'sensorNo']) }}
                                            {{ Form::hidden('sensorNo', $sensorNo) }}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('sensorType', __('messages.SensorType')) }}
                                            {{ Form::select('sensorType', ['' => '-- SELECT --', 'analog' => 'Analog', 'serial1' => 'LLS Fuel 1(Serial)', 'serial2' => 'LLS Fuel 2', 'serial3' => 'LLS Fuel 3'], $sensorType, ['class' => 'form-control', 'id' => 'sensorType']) }}
                                            {{ Form::hidden('sensorTypeOld', $sensorType) }}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('sensorMode', __('messages.ModeofSensor')) }}
                                            {{ Form::select('sensorMode', ['' => '-- SELECT --', 'MovingVehicle' => 'Moving Vehicle', 'Machinery' => 'Machinery', 'Dispenser' => 'Dispenser', 'DG' => 'Diesel Generator(DG)'], $sensorMode, ['class' => 'form-control']) }}
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-3 d-flex align-items-center text-center">
                                        <a class="btn btn-secondary btn-md mx-auto" id="export">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                            @lang('messages.CalibrationTemplate')
                                        </a>
                                    </div> --}}
                                </div>

                                <hr>
                                <br>
                                <div class="row mb-2">
                                    <div class="col-md-8">
                                        <div class="form-check d-flex justify-content-around mt-4">
                                            <label class="form-radio-label" id="excel">
                                                <input class="form-radio-input" type="radio" name="optionsRadios" value="">
                                                <span class="form-radio-sign">
                                                    @lang('messages.UploadCalibration')
                                                </span>
                                            </label>
                                            <label class="form-radio-label" id="direct">
                                                <input class="form-radio-input" type="radio" name="optionsRadios" value=""
                                                    checked>
                                                <span class="form-radio-sign">
                                                    @lang('messages.CalibrationDetails')
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 adddiv">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label for="">
                                                        @lang('messages.AddCount')
                                                    </label>
                                                    <div class="input-group">
                                                        {{ Form::hidden('noofLitreVolt', count($place), ['id' => 'noofLitreVolt']) }}
                                                        {{ Form::Number('count', '', ['class' => 'form-control countbtn ', 'placeholder' => 'Count', 'id' => 'counting', 'min' => '0']) }}
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text btn btn-outline-dark px-3 rounded-right" id="addCalibrate">
                                                                <i class="fas fa-plus-circle fa-lg"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <br>
                                <!-- upload Excel -->
                                <div class="row mt-3" Id="showexcel" style="display: none">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-2 d-flex align-items-center text-mt-center ">
                                        <a class="btn btn-secondary btn-sm " id="export">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                            @lang('messages.CalibrationTemplate')
                                        </a>
                                    </div>
                                    <div class="col-md-3 d-flex align-items-center mt-2 mt-md-0">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="Upload" name="import_file">
                                            <label class="custom-file-label" for="customFile" id="fileName1">Select a
                                                file</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mt-3 mt-md-0">
                                        <a class="btn btn-primary btn-sm" id="import">
                                            @lang('messages.IMPORTEXCEL')
                                        </a>
                                    </div>
                                   
                                </div>
                                {{-- manual litre volt entry --}}
                                <div class="row/ text-center mx-5 " id="details">                                    
                                    
                                </div>
                                <div id='rowvalues' class="p-0 border">
                                    <div class="row rowHead text-center mx-0 bg-white border-bottom">
                                        <div class="col-3">
                                            <p class="m-0 font-weight-bold bg-info/ text-primary/ py-2 btn-round">
                                                @lang('messages.Litre')
                                            </p>
                                        </div>
                                        <div class="col-3">
                                            <p class="m-0 font-weight-bold bg-info/ text-primary/ py-2 btn-round">
                                                @lang('messages.Volt')
                                            </p>
                                        </div>
                                        <div class="col-3">
                                            <p class="m-0 font-weight-bold bg-info/ text-primary/ py-2 btn-round">
                                                @lang('messages.Frequency')
                                            </p>
                                        </div>
                                        <div class="col-3">
                                            <p class="m-0 font-weight-bold bg-info/ text-primary/ py-2 btn-round">
                                                @lang('messages.Action') 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="mx-2 p-0 row-body">
                                        @foreach ($place as $value)
                                            {{-- <br id='delbr{{ $loop->index }}'> --}}
                                            <div class="row py-1 mx-0 mx-md-2" id='delrow{{ $loop->index }}'>
                                                <div class="col-3 px-0 px-md-2 "><input type='number' name='litre[]'
                                                        value={{ $value['liter'] ?? (($loop->index + 1) * 5 )}}
                                                        id='liter{{ $loop->index }}' class='form-control' step='5' min='5'
                                                        onchange="myChangeFunction(this)"></div>
                                                <div class=" col-3 px-0 px-md-2"><input type='number' name='volt[]'
                                                        value={{ $value['volt'] ?? (($loop->index + 1) * 5 )}}
                                                        id='volt{{ $loop->index }}' class='form-control volt' step='0.001'
                                                        onchange="myChangeFunction(this)"></div>
                                                <div class=" col-3 px-0 px-md-2"><input type='number'
                                                        name='frequency[]'
                                                        value={{ $value['freq'] ?? (($loop->index + 1) * 5 )}}
                                                        id='frequency{{ $loop->index }}' class='form-control frequency'
                                                        step='0.001' onchange="myChangeFunction(this)"></div>
                                                <div class=" col-3 px-0 px-md-2 text-center">
                                                    <a class="btn btn-sm btn-info text-white" id='rem{{ $loop->index }}'
                                                        onclick="getvalues('{{ $loop->index }}')">
                                                        @lang('messages.GetVolt')
                                                    </a>
                                                    <a class="btn btn-sm text-white btn-danger"
                                                        id='remove{{ $loop->index }}'
                                                        onclick="delvalues('{{ $loop->index }}')">
                                                        @lang('messages.Remove')
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <br>
                                <div class="row mt-4">
                                    <div class="col-6 mx-auto d-flex justify-content-center">
                                        <div id="cancelId">
                                            <a class="btn btn-danger"
                                                href="{{ URL::to('VdmVehicleScan' . $vehicleId) }}">
                                                @lang('messages.Cancel')
                                            </a>
                                        </div>
                                        <div id="calibrateId" class="ml-2">
                                            {{ Form::submit(__('messages.Save'), ['id' => 'sub', 'class' => 'btn btn-primary']) }}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        @endif
                        <!-- direct -->
                        @if ($calibrateType == 'direct')
                            <div class="hpanel" id="directConfigTab">
                                <div class="modal-body">
                                    <span class="h3 text-primary">
                                        @lang('messages.General')
                                    </span>
                                    <div class="float-right" data-toggle="modal"
                                            data-target="#caliSet">
                                            <button type="button" class="btn btn-sm btn-danger popup"> @lang('messages.FuelConfigurationDetails') </button>
                                        </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('vehicleId', __('messages.VehicleName'))  }}
                                                {{ Form::text('vehicleName1', $vehicleName, array('class' => 'form-control','disabled')) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('vehicleId', __('messages.AssetID')) }}
                                                {{ Form::text('vehicleId2', $vehicleId, ['class' => 'form-control','disabled']) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('deviceId', __('messages.DeviceId/IMEINo')) }}
                                                {{ Form::text('deviceId2', $deviceId, ['class' => 'form-control','disabled']) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row m-0">
                                                <div class="col-md-6 form-group">
                                                    {{ Form::label('Nooftank', __('messages.No.ofTank')) }}
                                                    <span class="rq"></span>
                                                    {{ Form::text('tankCount2', $tankCount, ['class' => 'form-control','disabled']) }}
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    {{ Form::label('Noofsensor', __('messages.No.ofSensor')) }}
                                                    <span class="rq"></span>
                                                    {{ Form::text('sensorCount2', $sensorCount, ['class' => 'form-control','disabled']) }}

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('maxitheft', __('messages.MinimumTheft')) }}
                                                <span style="font-size: 9px;"><b>(Note:</b> In litres)</span>
                                                {{ Form::text('sensorCount2', $maxitheft, ['class' => 'form-control','disabled']) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('minifilling', __('messages.MinimumFilling')) }}
                                                <span style="font-size: 9px;"><b>(Note:</b> In litres)</span>
                                                {{ Form::text('sensorCount2', $minifilling, ['class' => 'form-control','disabled']) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('tankSize', __('messages.Tanksize')) }}
                                                <span style="font-size: 9px;"><b>(Note:</b> In Litres)</span>
                                                <span class="rq"></span>
                                                {{ Form::text('tanksize2', $tanksize, ['class' => 'form-control','disabled']) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('CalibrateType', __('messages.CalibrateType'), ['class' => 'text-center']) }}
                                                <div class="d-flex">
                                                    <button
                                                        class="btn btn-round border mx-1 {{ $calibrateType == 'auto' ? 'btn-primary' : '' }}"
                                                        type="button"
                                                        style="background: #f1f1f1;"
                                                        data-label="auto">
                                                        @lang('messages.Auto')
                                                    </button>
                                                    <button
                                                        class="btn btn-round border mx-1 {{ $calibrateType == 'manual' ? 'btn-primary' : '' }}"
                                                        type="button" 
                                                        style="background: #f1f1f1;"                                                        
                                                        data-label="manual">
                                                        @lang('messages.Manual')
                                                    </button>
                                                    <button
                                                        class="btn btn-round border mx-1 {{ $calibrateType == 'direct' ? 'btn-primary' : '' }}"
                                                       type="button" 
                                                        data-label="direct">
                                                        @lang('messages.Direct')
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                {{ Form::label('sensorMode', 'Mode of Sensor') }}
                                                {{ Form::text('directsensorModse', ['' => '-- SELECT --', 'MovingVehicle' => 'Moving Vehicle', 'Machinery' => 'Machinery', 'Dispenser' => 'Dispenser', 'DG' => 'Diesel Generator(DG)'][$sensorMode], ['class' => 'form-control','disabled']) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('descriptionStatus',  __('messages.SensorDetails')) }}
                                                {{ Form::textarea('redisabledmak', $descriptionStatus, ['class' => 'form-control ','rows' => 4, 'cols' => 54,'disabled']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 mx-auto d-flex justify-content-center">
                                    <div id="cancelId">
                                        <a class="btn btn-danger"
                                            href="{{ URL::to('VdmVehicleScan' . $vehicleId) }}">
                                            @lang('messages.Cancel')
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="modal fade caliSet " id="caliSet" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" style="background: rgb(11 11 11 / 75%)" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered animated fadeIn" role="document">
                        <div class="modal-content mt-5">
                            <div class="modal-header">
                                <p class="modal-title text-primary h4" id="exampleModalLabel">@lang('messages.General')
                                </p>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('vehicleId', __('messages.VehicleName'))  }}
                                            {{ Form::text('vehicleName', $vehicleName, array('class' => 'form-control','id'=>'vehicleName','disabled')) }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('vehicleId', __('messages.AssetID')) }}
                                            {{ Form::hidden('tankDetails', json_encode($tankDetails, true), ['id' => 'tankDetails','disabled']) }}
                                            {{ Form::hidden('vehicleId', $vehicleId, ['class' => 'form-control']) }}
                                            {{ Form::hidden('tankCount', $tankCount, ['id' => 'tankCount']) }}
                                            {{ Form::hidden('navTabcount', $navTabcount, ['id' => 'navTabcount']) }}
                                            {{ Form::hidden('sensorCount', $sensorCount, ['id' => 'sensorCount']) }}
                                            {{ Form::hidden('calibrateType', $calibrateType, ['id' => 'calibrateType']) }}
                                            {{ Form::text('vehicleIddisable', $vehicleId, ['class' => 'form-control','disabled']) }}
                                            {{ Form::hidden('vehicleId1', $vehicleId, ['class' => 'form-control', 'id' => 'vehicleid']) }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('deviceId', __('messages.DeviceId/IMEINo')) }}
                                            {{ Form::text('deviceIddisable', $deviceId, ['class' => 'form-control','disabled']) }}
                                            {{ Form::hidden('deviceId', $deviceId) }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row m-0">
                                            <div class="col-md-6 form-group">
                                                {{ Form::label('Nooftank', __('messages.No.ofTank')) }}
                                                <span class="rq"></span>
                                                <input class='form-control' type="number" id='Nooftank' name="Nooftank"
                                                    min='1' value="{{ $tankCount }}" max="4" required
                                                    onkeydown="validateval(event)" onchange="myChangeFunction(this)">
                                            </div>
                                            <div class="col-md-6 form-group">
                                                {{ Form::label('Noofsensor', __('messages.No.ofSensor')) }}
                                                <span class="rq"></span>
                                                <input class="form-control" type="number" id='Noofsensor'
                                                    name="Noofsensor" min='1' value="{{ $sensorCount }}" max="4"
                                                    required onkeydown="validateval(event)"
                                                    onchange="myChangeFunction(this)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('maxitheft', __('messages.MinimumTheft')) }}
                                            <span style="font-size: 9px;"><b>(Note:</b> In litres)</span>
                                            {{ Form::number('maxitheft', $maxitheft, ['class' => 'form-control', 'onkeydown' => 'validateval(event)']) }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('minifilling', __('messages.MinimumFilling')) }}
                                            <span style="font-size: 9px;"><b>(Note:</b> In litres)</span>
                                            {{ Form::number('minifilling', $minifilling, ['class' => 'form-control', 'onkeydown' => 'validateval(event)']) }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('tankSize', __('messages.Tanksize')) }}
                                            <span style="font-size: 9px;"><b>(Note:</b> In Litres)</span>
                                            <span class="rq"></span>
                                            {{ Form::number('tanksize', $tanksize, ['class' => 'form-control', 'id' => 'tanksize', 'required' => 'required',($calibrateType == 'auto'?"readonly":"")]) }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('CalibrateType', __('messages.CalibrateType'), ['class' => 'text-center']) }}
                                            <div class="d-flex">
                                                <input type="hidden" name="typecali" id="typecali"
                                                    value="{{ $calibrateType }}">
                                                <input type="hidden" name="modelSubmit" id="modelSubmit" value="false">
                                                <button
                                                    class="btn btn-round border mx-1 caliTypebtn {{ $calibrateType == 'auto' ? 'btn-primary' : '' }}"
                                                    id="autobtn" type="button"
                                                    data-label="auto">
                                                    @lang('messages.Auto')
                                                </button>
                                                <button
                                                    class="btn btn-round border mx-1 caliTypebtn {{ $calibrateType == 'manual' ? 'btn-primary' : '' }}"
                                                    id="manualbtn" type="button"
                                                    data-label="manual">
                                                    @lang('messages.Manual')
                                                </button>
                                                <button
                                                    class="btn btn-round border mx-1 caliTypebtn {{ $calibrateType == 'direct' ? 'btn-primary' : '' }}"
                                                    id="directbtn" type="button"
                                                    data-label="direct">
                                                    @lang('messages.Direct')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('descriptionStatus',  __('messages.SensorDetails')) }}
                                            {{ Form::textarea('descriptionStatus', $descriptionStatus, ['class' => 'form-control','rows' => 4, 'cols' => 54]) }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group directsensorMode" style="display:{{$calibrateType == 'direct' ? 'block':'none'}} " >
                                            {{ Form::label('sensorMode', 'Mode of Sensor') }}
                                            {{ Form::select('directsensorMode', ['MovingVehicle' => 'Moving Vehicle', 'Machinery' => 'Machinery', 'Dispenser' => 'Dispenser', 'DG' => 'Diesel Generator(DG)'], $sensorMode, ['class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">
                                    @lang('messages.Close')
                                </button>
                                <input type="submit" class="btn btn-primary popupsubmit" value="{{__('messages.Savechanges')}}" >
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{asset('css/calibrate.css')}}">
@endpush

@push('js')
    @if ($newVehicle)
        <script>
            $(document).ready(function(){
                setTimeout(function() {
                    $('.popup').trigger('click')
                }, 300);
            })
        </script>
    @endif

    <script>
        // popup things
        $('.popup').on("click",function() {
            setTimeout(function() {
                $('.modal-backdrop').not("#caliSet").remove();
            }, 300);
        })

        $('.caliTypebtn').on('click',function(){
            calichange(this)
        })

        function calichange(element) {
            $('.caliTypebtn').removeClass('btn-primary');
            let value = $(element).attr('data-label');
            $('#typecali').val(value)
            $(element).addClass('btn-primary');
            
            // show option
            $('.directsensorMode').removeAttr('required').hide()
            if (value == "direct") {
                $(".directsensorMode").attr('required','required').fadeIn()
            }
        }


        // form submit
        function calibrateFormSubmit(popup=true){
            $('input').removeClass('is-invalid')
            removeAfterErr('input')
            let tanksize = $('#tanksize')
            let validation = ["0", ""];
            let Nooftank = $("#Nooftank")
            let Noofsensor = $("#Noofsensor")

            let valid = true;
            if(Nooftank.val() > 4){
                valid = false;
                Nooftank.aftererr("Number Of Tank Must be less or equal 4")
            }
            if(Noofsensor.val() > 4){
                valid = false;
                Noofsensor.aftererr("Number Of Sensor Must be less or equal 4")
            }
            if (validation.includes(tanksize.val())) {
                tanksize.aftererr()
                valid = false;
            }
            if (validation.includes(Nooftank.val())) {
                Nooftank.aftererr()
                valid = false
            }
            if (validation.includes(Noofsensor.val())) {
                Noofsensor.aftererr()
                valid = false
            }
            if( Nooftank.val() > Noofsensor.val()){
                Nooftank.aftererr("Number of Tank count cannot be more than Number of Sensor")
                valid = false
            }
            let sensorType = $('.sensorType')
            if (sensorType.val() == "") {
                sensorType.aftererr()
                valid = false
            }
            if (!valid) return valid;
            if(popup){
                $('#modelSubmit').val('true');
                $('#submit').submit();
                $('.splash').fadeIn()
            } 
            return  valid;
        }
        $('.popupsubmit').on('click',calibrateFormSubmit)
        $("#submit").find("input:submit").not('.popupsubmit').on('click',function(){
            let validate  = calibrateFormSubmit(false);
            if(!validate){
                $('.popup').trigger('click')
                return false;
            }
            validate = formValidation("#submit",true)
            if(validate){
                $('#submit').submit();
                $('.splash').fadeIn()
                return true;
            }
        })


        // initial
        // $('.multipleSelectpicker').select2();
        let Noofsensor, Nooftank;
        let calibrateType = $('#calibrateType').val();
        let tankCount = $('#tankCount').val();
        let sensorCount = $('#sensorCount').val();
        let navTabcount = $('#navTabcount').val();
        if (navTabcount === "") {
            navTabcount = 1;
        }
        let tankDetails = JSON.parse($('#tankDetails').val());
        let checkBodyChanges = false;
        let checkFormChanges = false;
        let fueltype = $('#fueltype').val();

        //initial render
        $('.hpanel,#showexcel').hide();
        $(':submit').not('.popupsubmit').attr('disabled', true);

        $(function(){
            if (calibrateType === 'auto') {
                $('#tankConfigTab').fadeIn();
                if(tankCount < navTabcount) validate();
                $(`#tank${navTabcount}`).parent('li').addClass('active rounded');
                $('#tankshape,#tankposition,#tanksize,#fueltype,#vehicletype,#maxval').attr('required', 'required');
                $('#tanksize').attr({
                    'min': '1',
                    'max': '8000'
                });
                $('#cylinder,#rectangle').hide();
                fueltypeRequired();
                tankshape();
                showFrequencyBtn();
            } else if (calibrateType === 'manual') {
                $('#tanksize,#sensorType').attr('required', 'required');
                $('#sensorConfigTab').fadeIn();
                $(`#sensor${navTabcount}`).parent('li').addClass('active rounded');
                sensorTypeRequired();
                sensorNoChange();
                rowvaluesShow();
            } else if (calibrateType === 'direct'){
                $('#sensorConfigTab,#tankConfigTab').hide();
                $('#Nooftank,#Noofsensor').val('1');
                // $('input').removeAttr('required');
                $('#directConfigTab').fadeIn();
                $(':submit').attr('disabled', false);
            }
        })

        //oncheges  
        $('#sensorType').on('change', sensorTypeRequired);
        $('#fueltype').on('change', fueltypeRequired);
        $('#tankshape,#tankposition').on('change',tankshape);
        $('#sensorNo').on('change',sensorNoChange);
        $('#submit').find('input,select').one('keypress change',function() {
            $(':submit').removeAttr('disabled');
            checkBodyChanges = true
            checkFormChanges = true
            // debugger
        });

        // onclick
        $("#addCalibrate").on('click',getadd);
        $('.checkCalibrateChanges').on('click',checkCalibrateChanges)


        function fueltypeRequired(){
            $('#minvolt,#maxvolt').removeAttr('required');
            if ($('#fueltype').val() === "analog") {
                $('#minvolt,#maxvolt').attr('required', 'required');
            }
        }

        function sensorTypeRequired() {
            let sensorValue = $('#sensorType').val();
            $('.frequency,.volt').prop('min', '0');
            if(sensorValue === "analog" || sensorValue === "analog2"){
                $('.volt').prop('min', '0.01');
            }else{
                $('.frequency').prop('min','0.01');
            }
        }
       
        // sensorNo changes
        function sensorNoChange() {
            let count = +$('#sensorNo').val();
            let val = tankDetails[count];
            val = val !== null ? val : {
                maxitheft: '',
                minifilling: '',
                tanksize: ''
            };
            const {
                maxitheft,
                minifilling,
                tanksize
            } = val;
            $('#maxitheft').val(maxitheft);
            $('#minifilling').val(minifilling);
            $('#tanksize').val(tanksize);
        }

        //alertMsg 
        function alertMsg(val) {
            return $('#alertMsg').html(
                `<div class="alert alert-warning alert-dismissible text-center" role="alert">
                        <strong>${val}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            &times;
                        </button>
                    </div>`);
        }

        //validate and route the page
        function validate({type = 'manual', count = 1, submitForm = false}) {
            let sensorCount = $('#Noofsensor').val();
            let tankCount = $('#Nooftank').val();
            let message = "";
            $('#Nooftank,#Noofsensor').removeClass('is-invalid');
            if ((sensorCount !== '' && sensorCount !== '0') && (tankCount !== '' && tankCount !== '0')) {
                if (tankCount <= sensorCount) {
                    if (parseInt(tankCount) <= 4 && parseInt(sensorCount) <= 4) {
                        if(!submitForm){
                            window.location = `{{route('calibrate',[$vehicleId])}}/${count}`;
                        }
                        return
                            // `../{{ $vehicleId }}/${count}`;
                    } else {
                        message = 'Tank and sensor values cannot be more than 4';
                        $('#Nooftank,#Noofsensor').addClass('is-invalid');
                    }
                } else {
                    message = 'No.of Tank count cannot be more than No.of Sensor';
                    $('#Nooftank').addClass('is-invalid');
                }
            } else {
                if ((sensorCount == '' || sensorCount == '0') && (tankCount == '' || tankCount == '0')) {
                    message = 'Please fill the required fields';
                    $('#Nooftank,#Noofsensor').addClass('is-invalid');
                } else if (sensorCount == "" || sensorCount == '0') {
                    message = 'Sensor value must be greater than or equal to 1';
                    $('#Noofsensor').addClass('is-invalid');
                } else if (tankCount == "" || tankCount == '0') {
                    message = 'Tank and Sensor value must be greater than or equal to 1';
                    $('#Nooftank').addClass('is-invalid');
                }
            }
            alertMsg(message)
            window.scrollTo(0, 10);
        }

        //checkCalibrate body chages
        async function checkCalibrateChanges() {
            let type = $(this).attr('data-type');
            let count = $(this).attr('data-count');

            if (checkBodyChanges) {
                if (await confirmAlert('You have unsaved changes. Click OK to continue without Saving or CANCEL ')) {
                    validate({count});
                }
                return null;
            }
            validate({count});
            
        }

        //add a liter and volt values
        function getadd() {
            let countNo = parseInt($('.countbtn').val());
            let newCount = countNo;
            let index, prevCount;
            $('.countbtn').val(0);
            prevCount  = parseInt($("#noofLitreVolt").val());
            prevCount = prevCount ? prevCount : 0;
            index = prevCount;
            addlitreVolt=prevCount;
            for (index; index < newCount + prevCount; index++) {
                addLiterVolt(index, 5 * (index + 1), '0');
                addlitreVolt++;
            }
            
            $('#rowvalues').fadeIn()
            
            sensorTypeRequired();
            $("#noofLitreVolt").val(addlitreVolt);
            $(".row-body").animate({
                    scrollDown: $('.row-body').height()
                }, 2000);
            // $(".row-body").scrollTop($('row-body').height());
        }

        function addLiterVolt(index, key, value) {
            uuid = Date.now() + key;
            $('.row-body').append(`
                    <div class="row py-1 mx-0 mx-md-2" id='delrow${uuid}'>
                        <div class=" col-3 px-0 px-md-2 "><input type='number' name='litre[]' value='${key}' id='liter${uuid}' class='form-control liter' step='5' min='5' onchange="myChangeFunction(this)"></div>
                        <div class=" col-3 px-0 px-md-2"><input type='number' name='volt[]'value='${value}' id=volt${uuid} class='form-control volt' step='0.001'  onchange="myChangeFunction(this)"></div>
                        <div class=" col-3 px-0 px-md-2"><input type='number' name='frequency[]'  value=${value}  id='frequency${uuid}' class='form-control frequency' step='0.001'  onchange="myChangeFunction(this)"></div>
                        <div class=" col-3 px-0 px-md-2 text-center">
                            <a class="btn btn-sm btn-info text-white" id='rem${uuid}' onclick="getvalues('${uuid}')">Get Volt</a>
                            <a class="btn btn-sm text-white btn-danger" id='remove${uuid}' onclick="delvalues('${uuid}')">Remove</a>
                        </div>
                    </div>`);
        }

        // remove the liter and volt values
        function delvalues(d) {
            $(':submit').removeAttr('disabled');
            $(`#delrow${d},#delbr${d}`).remove();
            prevCount  = parseInt($("#noofLitreVolt").val());
            prevCount = prevCount ? prevCount : 0;
            $("#noofLitreVolt").val(prevCount - 1);
            checkForChanges = true;
            rowvaluesShow();
        }

        function rowvaluesShow(){
            count = $('.row-body>.row').length
            if(count == 0){
                $('#rowvalues').fadeOut()
            }
        }
        // save input val
        function myChangeFunction(input) {
            val = input.value !== "" ? input.value : 0;
            $(input).attr('value', val);
        }

        function validateval(event) {
            var val = event.currentTarget.value;
            var invalidChars = ["-", "+", "e", "."];
            if (invalidChars.includes(event.key)) {
                event.preventDefault();
            }
        }

        // 
   
        $("#excel").on('click',function() {
            $('#calibrateId,#cancelId,#rowvalues,#submit1,#details,.adddiv').hide();
            $("#showexcel,#import").fadeIn();
        });

        $("#direct").on('click',function() {
            $("#showexcel,#import").hide();
            $('#calibrateId,#cancelId,#export,#rowvalues,#submit1,#details,.adddiv').fadeIn();
        });

        $("#export").on('click',function() {
            const exportCalibrate = "{{ route('export/calibrate') }}";
            $('#submit').attr('action', exportCalibrate);
            $('#submit').submit();
            $('#submit').attr('action', "{{ route('calibrateUpdate') }}");
        });

        $("#autoExcel").on('click',function(){
            var formAction = '{{route('downloadAutoCalibrate')}}';
            $('#submit').attr('action', formAction);
            $('#submit').submit();
            $('#submit').attr('action','{{route("calibrateUpdate")}}' );
        });

        $("#import").on('click',function() {
            const importCalibrate = "{{ route('import/calibrate') }}";
            const enctype = 'multipart/form-data';
            $('#submit').attr({
                'action': importCalibrate,
                'enctype': enctype
            });
            $('#submit').submit();
        });
        

        function getvalues(j) {
            var data = {
                'vehicleId': $('#vehicleid').val(),
                'volt': $(`#volt${j}`).val(),
                '_token': $("input[name='_token']").val()
            };
            $.post('{{ route('ajax.calibrateget') }}', data)
                .done(function(data, textStatus, xhr) {

                    if (data.volt == "" && data.frequency == "") {
                        wornigAlert('There is no Volt and Frequency Found !');
                        return false;
                    } else {
                        $(`#volt${j}`).val(data.volt);
                        $(`#frequency${j}`).val(data.frequency);
                    }
                });
        }

        // for auto cali function -----------------------

        // change
        showTank(true);
        $('#showFrequencyBtn').on('click',showLiterVoltList);
        $('#showFrequencySync').on('click',showFrequencyCalc);
        $('#maxvolt').on('keyup',showFrequencyBtn)
        $('#fueltype').on('change',showFrequencyBtn);
        $('#tankposition,#tankshape,#calculate').on('change',showTank);

        $('#r,#cl').on('keyup',function() {
            cylinderCapacity()
            showFrequencyBtn()
        });
        $('#recLength,#recWidth,#recHeight').on('keyup',function() {
            rectangleCapacity()
            showFrequencyBtn()
        });

        //tankshape oncheges
        function tankshape() {
            let shape = $('#tankshape').val();
            let tankposition = $('#tankposition').val();
            let maxvolt = $('#maxvolt').val();
            $('#rectangle,#cylinder').hide();
            $('#recLength,#recWidth,#recHeight,#r,#cl').removeAttr('required');

            if (shape == 'Cylinder') {
                $('#cylinder').fadeIn();
                $('#r,#cl').attr('required', 'required');
            }
            if (shape !== 'Rectangle' || tankposition == "") {
                $('#calculateDiv').hide();
                return
            }
            $('#calculateDiv').fadeIn();
            let calculate = $('#calculate').val();
            if (calculate !== 'yes') return
            $('#rectangle').fadeIn();
            $('#recLength,#recWidth,#recHeight').attr('required', 'required');
        }

        function cylinderCapacity(initial = false) {
            let pi = Math.PI;
            let radius = parseFloat($('#r').val()) || 0;
            let length = parseFloat($('#cl').val()) || 0;
            if (radius == 0 || length == 0) return
            let height = radius * 2;
            let tankSize = 0;

            let position = $('#tankposition').val();
            if (position === 'horizontal') {
                let leftside = (radius - height) * Math.sqrt((2 * radius * height) - (height * height))
                let rightside = ((radius * radius) * Math.acos((radius - height) / radius));
                let area = rightside - leftside;
                tankSize = (area * length) / 1000;
            }
            if (position === 'vertical') {
                tankSize = (pi * (radius * radius) * length) / 1000;
            }
            tankSize = Math.round(tankSize / 1000)

            $('.Cylinder>.vertical-tank .radius-value').text(radius + ' radius')
            $('.Cylinder>.vertical-tank .length-value').text(length + ' height')
            $('.Cylinder>.horizontal-tank .radius-value').text(height + ' height')
            $('.Cylinder>.horizontal-tank .length-value').text(length + ' length')
            cylinderFuelAnimation(tankSize);
            $('.fuel-value').text(tankSize + ' Liters');
            if(!initial){
                $('#tanksize,#maxval').val(tankSize)
            }
        }

        function rectangleCapacity(initial = false) {
            tankSize = getRectangleLiter();
            tankSize = Math.round(tankSize)
            rectangleFuelAnimation(tankSize)
            $('.fuel-value').text(tankSize + ' Liters');
            if(!initial){
                $('#tanksize,#maxval').val(tankSize)
            }
        }

        function cylinderFuelAnimation(vol) {
            // let tankposition = $("#tankposition").val()
            $('.Cylinder>.vertical-tank>.tank>.fuel').removeClass('horizontalFuelRaise');
            if (!vol) return
            setTimeout(function() {
                $('.Cylinder >.vertical-tank>.tank>.fuel').addClass('horizontalFuelRaise');
            }, 1000)

        }

        function rectangleFuelAnimation(vol) {
            $('.Rectangle .tank>.fuel').removeClass('spinning');
            $('.Rectangle .fuel-value').attr('style', '');
            if (vol == 0) {
                $('.Rectangle .fuel-value').css('color', 'black')
            }
            if (!vol) return
            setTimeout(function() {
                $('.Rectangle .tank>.fuel').addClass('spinning')
            }, 1000)
        }


        function showTank(initial = false) {
            $('.tankdraw').hide()
            showFrequencyBtn();
            let position = $('#tankposition').val()
            let shape = $("#tankshape").val()
            $('.tank-anime').hide();
            if(shape !== "") {
                $(`.${shape}`).fadeIn();
            }
            if (shape == "Cylinder") {
                cylinderCapacity(initial)
                if(position !== ""){
                    $('.tankdraw').show()
                }
                if(position == "horizontal"){
                    $(`.Cylinder>.vertical-tank`).addClass('horizontal-tank');
                }
                if(position == "vertical"){
                    $(`.Cylinder>.vertical-tank`).removeClass('horizontal-tank');
                }
            }
            if (shape == "Rectangle") {
                let calculate = $('#calculate').val();
                // show /hide div
                tankshape()
                $(`.${shape}>div`).hide();
                if (calculate == 'yes') {
                    if(position !== ""){
                        $('.tankdraw').show()
                    }
                    $(`.${shape}>.${position}-tank`).fadeIn()
                }
                rectangleCapacity(initial)
                
            }
        }

        function showLiterVoltList() {
            if ($('#showFrequencyBtn').attr('show') !== 'true') {
                $('#showFrequencyBtn').attr('show', 'true');
                showFrequencyCalc()
                $('.literVoltList').slideDown('slow')
            } else {
                $('#showFrequencyBtn').attr('show', 'false');
                $('.literVoltList').slideUp('fast')
            }
        }

        function showFrequencyBtn() {
            let maxvolt = $('#maxvolt').val();
            let tanksize = $('#tanksize').val();
            let fuelType = $('#fueltype').val();
            let tankshape = $('#tankshape').val();
            if (maxvolt == '' || tanksize == '' || tanksize == '0' || fuelType == '' || tankshape == '') {
                $('.showFrequencyBtn,.literVoltList').fadeOut();
            } else {
                $('.showFrequencyBtn').fadeIn();
            }
        }

        function showFrequencyCalc() {
            let maxvolt = parseFloat($('#maxvolt').val()) || 0;
            let minvolt = parseFloat($('#minvolt').val()) || 0;
            let tanksize = +$('#tanksize').val() || 0;
            // debugger
            if (maxvolt == 0 || tanksize == 0) return

            let tankshape = $('#tankshape').val();
            let fueltype = $('#fueltype').val();
            let increment = 0.1;
            if (fueltype == "analog") increment = 0.0001;
            let res = {};
            let index = minvolt;
            index = 0;
            if (tankshape === "Cylinder") {
                for (index; index < maxvolt; index += increment) {
                    volt = index.toFixed(2);
                    fuelLiter = getCylinderLiterVolt(volt).toFixed(2);
                    debugger
                    if (fueltype == "analog") fuelLiter = Math.round(fuelLiter)
                    if (fuelLiter % 5 === 0) {
                        if (typeof res[fuelLiter.toString()] !== "undefined") continue;
                        res[fuelLiter.toString()] = volt;
                    }
                }
            }
            if (tankshape === "Rectangle") {
                const fuelLiter = getRectangleLiter();
                let dividedFuel = +(fuelLiter / 5).toFixed(2);
                incrementVolt = +((maxvolt - minvolt) / dividedFuel).toFixed(2);
                fuelIncrement = 5;
                debugger
                for (index; index < maxvolt; index += incrementVolt) {
                    fuel = fuelIncrement;
                    if (fuelLiter < fuel) continue;
                    volt = index + incrementVolt;
                    res[fuel.toString()] = volt.toFixed(2);
                    if (volt.toFixed(2) > maxvolt) {
                        res[fuel.toString()] = maxvolt;
                    }
                    fuelIncrement += 5;
                }
            }
            if (tanksize % 5 !== 0){
                tanksize = tanksize;
                res[tanksize] = maxvolt;
            }
            // remove 0 liter/volt 
            delete res["0"];
            delete res["0.00"];
            listCalculate(res)
        }

        function listCalculate(list) {
            $('#listCalculate').html('')
            $('.literVoltList').slideDown()
            count = 0;
            const keys = Object.keys(list);
            ordered=[];
            for(let i=0; i <= keys.length - 1; i++){
                key =keys[i];
                ordered[key] = list[key] ;
                count += 1;
            }

            // const ordered = Object.keys(list).sort().reduce(
            //     (obj, key) => {
            //         count += 1;
            //         obj[key] = list[key];
            //         return obj;
            //     }, {}
            // );
            let split = count / 10;
            let round = Math.round(split);
            let remaind = Math.abs(split - round) * 10;
            let col = 6;
            let numberOfCol = split / col;
            let divKey = Date.now();
            debugger
            for (let index = 0; index < split; index++) {
                let breakCount = 0;
                $('#listCalculate').append($(
                    '<div class ="col-md-2 p-0" ><table class="table border"><thead><tr><th> Litre </th><th> Volt </th></tr></thead><tbody class="litervolttableBody' +
                    index + '"></tbody></table></div>'
                ))
                // console.log(ordered)
                for (const key in ordered) {
                    if (breakCount == 10) break                    
                    // key = Math.round(key);
                    $('.litervolttableBody' + index).append($('<tr class="tr' + divKey + '"></tr>'));
                    $('.litervolttableBody' + index + '>.tr' + divKey).append('<td>' + key + '</td><td>' + ordered[key] +
                        '</td>')
                    delete ordered[key]
                    breakCount += 1;
                    divKey +=1;
                }

            }


        }

        function getCylinderLiterVolt(volt) {
            let pi = Math.PI;
            let radius = parseFloat($('#r').val()) || 0;
            let length = parseFloat($('#cl').val()) || 0;
            let maxvolt = parseFloat($('#maxvolt').val()) || 0;
            let minvolt = parseFloat($('#minvolt').val()) || 0;
            let height = radius * 2;
            let fuelLiter = 0;

            let position = $('#tankposition').val();

            if (position === 'horizontal') {
                if (volt > maxvolt) {
                    height = 2 * radius;
                } else {
                    height = (volt / maxvolt) * (2 * radius);
                }
                let leftside = (radius - height) * Math.sqrt((2 * radius * height) - (height * height))
                let rightside = ((radius * radius) * Math.acos((radius - height) / radius));
                let area = rightside - leftside;
                fuelLiter = (area * length) / 1000;
            }
            if (position === 'vertical') {
                if (volt > maxvolt) {
                    height = length;
                } else {
                    height = (volt / maxvolt) * length;
                }
                fuelLiter = (pi * (radius * radius) * height) / 1000;
            }
            fuelLiter = fuelLiter / 1000;
            return fuelLiter;
        }

        function getRectangleLiter() {
            let height = parseFloat($('#recHeight').val()) || 0;
            let width = parseFloat($('#recWidth').val()) || 0;
            let length = parseFloat($('#recLength').val()) || 0;
            fuelLiter = length * width * height / 1000;
            if (fuelLiter == 0) fuelLiter = $('#tanksize').val();
            return fuelLiter
        }
        // -------------------------------------------------------

    </script>
@endpush
