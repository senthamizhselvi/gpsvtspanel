@extends("layouts.app")
@section('content')
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">@lang('messages.Dashboard')</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">@lang('messages.Dashboard')</a>
                </li>
            </ul>
        </div>
        <div class="page-body">
            <div class="row row-card-no-pd mt--2">
                <div class="col-sm-6 col-md-4">
                    <div class="card card-stats card-round">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5">
                                    <div class="icon-big text-center">
                                        <i class="fas fa-truck text-danger"></i>
                                    </div>
                                </div>
                                <div class="col-7 col-stats">
                                    <div class="numbers">
                                        <p class="card-category">
                                            @lang('messages.TotalVehiclesOnBoard')
                                        </p>
                                       <h4 class="card-title text-center">{{ $count }}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="card card-stats card-round">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5">
                                    <div class="icon-big text-center">
                                        <i class="fas fa-calendar-alt text-danger"></i>
                                    </div>
                                </div>
                                <div class="col-7 col-stats">
                                    <div class="numbers">
                                        <p class="card-category">@lang('messages.ThisMonth')</p>
                                        <h4 class="card-title text-center">{{ $prsentMonthCount }}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="card card-stats card-round">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">
                                    <div class="icon-big text-center">
                                        <i class="fas fa-calendar-alt text-danger"></i>
                                    </div>
                                </div>
                                <div class="col-7 col-stats">
                                    <div class="numbers">
                                        <p class="card-category">@lang('messages.LastMonth')</p>
                                        <h4 class="card-title text-center">{{ $prevMonthCount }} </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card animated zoomIn">
                <div class="card-body">
                    <div class="card-title text-primary">@lang('messages.VehiclesOnBoardwitheachDealers')</div>
                    <div class="row mt-4">
                        <div class="col-12  overflow-auto">
                            <table id="example1"
                                class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                <thead class="thead-light">
                                    <tr>
                                        <th style="text-align: center;">@lang('messages.DealerID')</th>
                                        <th style="text-align: center;">@lang('messages.NumberOfVehicles')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($dealerId))
                                        @foreach ($dealerId as $key => $value)
                                            <tr style="text-align: center; font-size: 11px;">
                                                <td> {{ Form::label('li', $key) }}</td>
                                                <td> {{ Form::label('l', $value . ' ' . ' ') }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
