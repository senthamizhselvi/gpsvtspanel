@extends("layouts.app")
@section('content')

    {{ HTML::ul($errors->all()) }}
    {{ Form::open(['url' => 'vdmVehicles/moveDealer']) }}
    <div id="wrapper">
        <div class="content animate-panel">
            <div class="hpanel page-inner">
                <div class="pull-right">
                    <a href="{{ route('VehicleScan/sendExcel') }}">
                        <img class="pull-right" width="40px" height="50px" src="{{ asset('assets/imgs/xls.svg') }}"
                            alt="xls" method="get" />
                    </a>
                </div>
                <div class="page-header">
                    <h4 class="page-title">
                        @if (session()->get('vCol') == '1')
                            @lang('messages.VehiclesList')
                        @endif
                        @if (session()->get('vCol') == '2')
                            @lang('messages.ViewVehicles')
                        @endif
                    </h4>
                </div>
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif

                <div class="card">
                    <div class="card-body">
                        <span class="text-danger error-message"> {{ HTML::ul($errors->all()) }}</span>
                        <div class="table-responsive">
                            <table id="vehicleTable"
                                class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                <thead>
                                    <tr>
                                        @if (session()->get('cur1') == 'prePaidAdmin')
                                            <th data-label="Licence ID">
                                                @lang('messages.LicenceID')
                                            </th>
                                        @endif
                                        <th data-label="AssetID">
                                            @lang('messages.AssetID')
                                        </th>
                                        <th data-label="Vehicle Name">
                                            @lang('messages.VehicleName')
                                        </th>
                                        <th data-label="Org Name">
                                            @lang('messages.OrgName')
                                        </th>
                                        <th data-label="Device ID">
                                            @lang('messages.DeviceID')
                                        </th>
                                        <th data-label="Server Name">
                                            @lang('messages.ServerName')
                                        </th>
                                        <th data-label="Last Loc Time">
                                            @lang('messages.LastLocTime')
                                        </th>
                                        <th data-label="Last Comm Time">
                                            @lang('messages.LastCommTime')
                                        </th>

                                        @if (session()->get('vCol') == '2')
                                            <th data-label="Status">
                                                @lang('messages.Status')
                                            </th>
                                            <th data-label="Device Model">
                                                @lang('messages.DeviceModel')
                                            </th>
                                            <th data-label="Version">
                                                @lang('messages.Version')
                                            </th>
                                            <th data-label="GPS Sim No">
                                                @lang('messages.GPSSimNo')
                                            </th>
                                            <th data-label="TimeZone">
                                                @lang('messages.TimeZone')
                                            </th>
                                            <th data-label="APN">
                                                @lang('messages.APN')
                                            </th>
                                            <th data-label="Licence Issued Date">
                                                @lang('messages.LicenceIssuedDate')
                                            </th>
                                            <th data-label="Onboard Date">
                                                @lang('messages.OnboardDate')
                                            </th>
                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                <th data-label="Licence Expire Date">
                                                    @lang('messages.LicenceExpireDate')
                                                </th>
                                            @endif
                                            @if (session()->get('cur1') != 'prePaidAdmin' && session()->get('cur1') != 'prePaidDealer')
                                                <th data-label="Vehicle Expire Date">
                                                    @lang('messages.VehicleExpireDate')
                                                </th>
                                            @endif

                                        @endif
                                        @if (session()->get('vCol') == '1')
                                            <th data-label="Licence Issued Date">
                                                @lang('messages.LicenceIssuedDate')
                                            </th>
                                            <th data-label="Onboard Date">
                                                @lang('messages.OnboardDate')
                                            </th>
                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                <th data-label="Licence Expire Date">
                                                    @lang('messages.LicenceExpireDate')
                                                </th>
                                            @endif
                                            @if (session()->get('cur1') != 'prePaidAdmin' && session()->get('cur1') != 'prePaidDealer')
                                                <th data-label="Vehicle Expire Date">
                                                    @lang('messages.VehicleExpireDate')
                                                </th>
                                            @endif
                                            <th data-label="Action">
                                                @lang('messages.Action')
                                            </th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody style="word-break: break-all;">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{ Form::hidden('reprocessDate', date('d-m-Y', strtotime('-1 days'))) }}
                    {{ Form::hidden('reprocessUrl', route('reprocess.vehicle')) }}
                    {{ Form::hidden('_token', csrf_token()) }}
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('common/js/vehicleReprocess.js') }}"></script>
    <script type='text/javascript'>
        let titleText = "{{ __('messages.Reprocessforvehicle') }}"
        let submitText = "{{ __('messages.submitText') }}"
        let cancelText = "{{ __('messages.cancelText') }}"
        $(document).ready(function() {

            let data = [];
            $('th').each(function() {
                let val = $(this).attr("data-label");
                data.push({
                    data: val,
                    name: val
                })
            })
            $('#vehicleTable').DataTable({
                'stateSave': true,
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'data': {
                        "_token": "{{ csrf_token() }}",
                    },
                    'url': '{{ route('ajax.vehicleList') }}',
                },
                'columns': data,
                stateSave: true,
                "language": {
                    "url":  $("#langJson").val()
                },
            });
        });
    </script>
@endpush
