@extends("layouts.app")
@section('content')
    <div id="wrapper" class="page-inner">
            <div class="pull-right">
                <a href="{{ route('VehicleScan/sendExcel') }}">
                    <img class="pull-right" width="40px" height="50px" src="{{ asset('assets/imgs/xls.svg') }}" alt="xls"
                        method="get" />
                </a>
            </div>
            @if (session()->has('message'))
                <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                    {{ session()->get('message') }}
                </p>
            @endif
            <div class="page-header">
                <h4 class="page-title">
                    @if (session()->get('vCol') == '1')
                        @lang('messages.VehiclesList')
                    @endif
                    @if (session()->get('vCol') == '2')
                        @lang('messages.ViewVehicles')
                    @endif
                </h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                            @if (session()->get('vCol') == '1')
                                @lang('messages.VehiclesList')
                            @endif
                            @if (session()->get('vCol') == '2')
                                @lang('messages.ViewVehicles')
                            @endif
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card">
                <div class="card-body px-1">
                    <span class="text-danger"> {{ HTML::ul($errors->all()) }}</span>
                    <div class="table-responsive">
                        <table id="indexNew" class="display table table-striped table-hover w-100 table-head-bg-primary">
                            <thead>
                                <tr>
                                    @if (session()->get('cur1') == 'prePaidAdmin')
                                        <th style="text-align: center;">
                                            @lang('messages.LicenceID')
                                        </th>
                                    @endif
                                    <th style="text-align: center;">
                                        @lang('messages.AssetID')
                                    </th>
                                    <th style="text-align: center;">
                                        @lang('messages.VehicleName')
                                    </th>
                                    <th style="text-align: center;">
                                        @lang('messages.OrgName')
                                    </th>
                                    <th style="text-align: center;">
                                        @lang('messages.DeviceID')
                                    </th>
                                    <th style="text-align: center;">
                                        @lang('messages.ServerName')
                                    </th>
                                    <th style="text-align: center;">
                                        @lang('messages.LastCommTime')
                                    </th>
                                    <th style="text-align: center;">
                                        @lang('messages.GPSSimNo')
                                    </th>
                                    @if (session()->get('vCol') == '2')
                                        <th style="text-align: center;">
                                            @lang('messages.Status')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.DeviceModel')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.Version')
                                        </th>

                                        <th style="text-align: center;">
                                            @lang('messages.TimeZone')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.APN')
                                        </th>
                                        
                                    @endif
                                        <th style="text-align: center;">
                                            @lang('messages.LicenceIssuedDate')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.OnboardDate')
                                        </th>
                                        @if (session()->get('cur1') == 'prePaidAdmin')
                                            <th style="text-align: center;">
                                                @lang('messages.LicenceExpireDate')
                                            </th>
                                        @endif
                                        @if (session()->get('cur1') != 'prePaidAdmin' && session()->get('cur1') != 'prePaidDealer')
                                            <th style="text-align: center;">
                                                @lang('messages.VehicleExpireDate')
                                            </th>
                                        @endif
                                    @if (session()->get('vCol') == '1')
                                        <th style="text-align: center;">
                                            @lang('messages.Actions')
                                        </th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody style="word-break: break-all;">
                                @php
                                    $statusSearch = ["P", "M", "S", "U", "N"];
                                    $statusList = ["Parking", "Moving", "Idle", "NoData", "NewDevice"];
                                    $statusColor = ["#8e8e7b", "#00b374", "#ff6500", "#fe068d", "#0a85ff"];
                                @endphp
                                @foreach ($vehicleList as $key => $vehicle)
                                    @if (session()->get('cur') == 'dealer' && in_array($vehicle,$onboardVehicleList))
                                        @continue
                                    @endif

                                    @php
                                        $refData = json_decode($refDataList[$key]);
                                        $licType = $refData->Licence ?? "";

                                        $proData = explode(',', $proDataList[$key]);
                                        $status = isset($proData[7]) ? $proData[7] : 'N';
                                        $lastComm = isset($proData[36]) ? $proData[36] : '';
                                        if ($lastComm == '' || $lastComm == null) {
                                            $comm = '-';
                                        } else {
                                            $sec = $lastComm / 1000;
                                            $datt = date("Y-m-d", $sec);
                                            $time1 = date("H:i:s", $sec);
                                            $comm = "$datt  $time1";
                                            // $comm = date("Y-m-d H:i:s",$sec);
                                        }
                                        
                                        $statusIndex = array_search($status, $statusSearch, true);

                                        $timezoneRefData = json_decode($deviceDetailList[$key]);
                                        $timezone = $timezoneRefData->timezone ?? '-';
                                        $apn = $timezoneRefData->apn ?? '-';

                                        $licExpireDetail = json_decode($licenceList[$key])
                                    @endphp
                                    <tr class="text-center">

                                        @if (session()->get('cur1') == 'prePaidAdmin')
                                            <td>{{ Arr::get($licenceIdList, $key) }}</td>
                                        @endif
                                        <td>{{ $vehicle }}</td>
                                        <td>{{ $refData->shortName ?? $vehicle }}</td>
                                        <td>{{ $refData->orgId ?? "-" }}</td>
                                        <td>{{ $refData->deviceId ?? "-"}}</td>
                                        <td>{{ Arr::get($servernameList, $key) ?? "-" }}</td>
                                        <td>{{ $comm }}</td>
                                        <td>{{ $refData->gpsSimNo ?? 99999 }}</td>

                                        @if (session()->get('vCol') == '2')

                                            <td>
                                                <div style="color: {{$statusColor[$statusIndex]}}">
                                                    @lang('messages.'.$statusList[$statusIndex])
                                                </div>
                                            </td>
                                            <td>{{ $refData->deviceModel ?? "nill" }}</td>
                                            <td>{{ Arr::get($versionList, $key) ?? "-" }}</td>
                                            <td>{{ $timezone }}</td>
                                            <td>{{ $apn }}</td>
                                        @endif
                                        @if (session('cur1') == "prePaidAdmin")
                                            <td>{{ $licExpireDetail->LicenceissuedDate ?? "-" }}</td>
                                        @else
                                            <td>{{$refData->licenceissuedDate ?? "-"}}</td>
                                        @endif
                                        <td>{{ $refData->onboardDate ?? "-" }}</td>
                                        @if (session()->get('cur1') == 'prePaidAdmin')
                                            <td>{{ $licExpireDetail->LicenceExpiryDate ?? "-" }}</td>
                                        @endif
                                        @if (session()->get('cur1') != 'prePaidAdmin' && session()->get('cur1') != 'prePaidDealer')
                                            <td>{{ $refData->vehicleExpiry ?? "-" }}</td>
                                        @endif
                                        @if (session()->get('vCol') == '1')
                                            <td>
                                                <div class="dropdown dropleft">
                                                    <button class="btn btn-info text-white btn-sm dropdown-toggle"
                                                        type="button" id="dropdownMenuButton{{ $vehicle }}"
                                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        @lang('messages.Action')
                                                    </button>
                                                    <div class="dropdown-menu"
                                                        aria-labelledby="dropdownMenuButton{{ $vehicle }}">
                                                        @if ($licType !== 'StarterPlus')
                                                            <a class="btn btn-sm btn-success text-white dropdown-item mb-1"
                                                                href="{{ URL::to('vdmVehicles/migration/' . $vehicle) }}">
                                                                @lang('messages.Migration')
                                                            </a>
                                                        @endif

                                                        <a class="btn btn-sm btn-primary text-white dropdown-item mb-1"
                                                            href="{{ URL::to('vdmVehicles/edit/' . $vehicle) }}">
                                                            @lang('messages.Edit')
                                                        </a>
                                                        @if (($licType !== 'Basic' && $licType !== 'Starter' && $licType !== 'StarterPlus') || session()->get('cur1') !== 'prePaidAdmin')
                                                            <a class="btn btn-sm btn-warning dropdown-item mb-1"
                                                                href="{{ route('calibrate', ['vid' => $vehicle, 'count' => '1']) }}">
                                                                @lang('messages.Calibrate')
                                                            </a>
                                                        @endif
                                                        <a class="btn btn-sm btn-success dropdown-item text-white mb-1"
                                                            href="{{route('audit',['type'=>'vehicle','name'=>$vehicle ])}}">
                                                            @lang('messages.AuditReport')
                                                        </a>
                                                        <a class="btn btn-sm btn-info text-white dropdown-item mb-1"
                                                            href="{{ URL::to('vdmVehicles/rename/' . $vehicle) }}">
                                                            @lang('messages.Rename')
                                                        </a>
                                                        <a class="btn btn-sm btn-secondary text-white dropdown-item mb-1"
                                                            onClick="setVehicle('{{ $vehicle }}',this)">
                                                            @lang('messages.Reprocess')
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::hidden('reprocessDate', date('d-m-Y', strtotime('-1 days'))) }}
        {{ Form::hidden('reprocessUrl', route('reprocess.vehicle')) }}
        {{ Form::hidden('_token', csrf_token()) }}
    </div>
@endsection

@push('js')
    <script>
        let titleText = "{{ __('messages.Reprocessforvehicle') }}"
        let submitText = "{{ __('messages.submitText') }}"
        let cancelText = "{{ __('messages.cancelText') }}"
    </script>
    <script src="{{ asset('common/js/vehicleReprocess.js') }}"></script>
    <script>
        $(function(){
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-uk-pre": function (a) {
                    var ukDatea = a.split('-');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                },

                "date-uk-asc": function (a, b) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-uk-desc": function (a, b) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });

            if({{session()->get('vCol')}} ==1){
                list =[
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        { "sType": "date-uk" },
                        { "sType": "date-uk" },
                        { "sType": "date-uk" },
                        null
                    ];
            }else{
                list=[
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        { "sType": "date-uk" },
                        { "sType": "date-uk" },
                        { "sType": "date-uk" },
                    ]
            }
            var table = $('#indexNew').dataTable( {
                    'stateSave': true,
                    // 'scrollX': true,
                    "aoColumns":list
                });
        })
    </script>
@endpush
