@extends("layouts.app")
@section('content')

    <div class="page-inner">
        <div class="page-header">
            <div class="page-title">
               @lang('messages.EditVehicle')
            </div>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">
                        @lang('messages.VehicleList')
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">
                        @lang('messages.VehicleEdit')
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">{{ $vehicleId }}</a>
                </li>
            </ul>
        </div>
        <div class="page-body">
            <div class="card">
                
                <div class="card-body">
                    <span class="text-denger error-message">{{ HTML::ul($errors->all()) }}</span>
                    {{ Form::open(['url' => 'vdmVehicles/update1']) }}
                    <p class="h3 text-primary">
                        @lang('messages.VehicleInformation')
                    </p>
                    <hr>
                    <div class="row mx-auto">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('vehicleId', __('messages.VehicleId')) }}
                                {{ Form::text('vehicleId', $vehicleId, ['class' => 'form-control caps']) }}
                                {{ Form::hidden('vehicleIdOld', $vehicleId) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('deviceId', __('messages.DeviceId')) }}
                                <br />
                                {{ Form::text('deviceId', $refData['deviceId'], ['class' => 'form-control caps']) }}
                                {{ Form::hidden('deviceIdOld', $refData['deviceId']) }}</font>

                            </div>
                            <div class="form-group">
                                {{ Form::label('VehicleName', __('messages.VehicleName')) }}
                                {{ Form::text('VehicleName', $refData['shortName'], ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('deviceModel', __('messages.DeviceModel')) }}
                                {{ Form::select('deviceModel', $protocol, $refData['deviceModel'], ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('regNo', __('messages.VehicleRegistrationNumber')) }}
                                {{ Form::text('regNo', $refData['regNo'], ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('vehicleType', __('messages.VehicleType')) }}
                                {{ Form::select('vehicleType', $vehicleTypeList, $refData['vehicleType'], ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('onboardDate', __('messages.OnboardDate')) }}
                                {{ Form::text('onboardDate', $refData['onboardDate'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('licenceissuedDate', __('messages.LicenceIssuedDate')) }}
                                {{ Form::text('licenceissuedDate', $refData['licenceissuedDate'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('overSpeedLimit', __('messages.OverSpeedLimit')) }}
                                {{ Form::number('overSpeedLimit', $refData['overSpeedLimit'], ['class' => 'form-control', 'min' => '1']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('morningTripStartTime', __('messages.MorningTripStartTime')) }}
                                {{ Form::text('morningTripStartTime', $refData['morningTripStartTime'], ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('eveningTripStartTime', __('messages.EveningTripStartTime')) }}
                                {{ Form::text('eveningTripStartTime', $refData['eveningTripStartTime'], ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('expiredPeriod', __('messages.ExpiredPeriod')) }}
                                {{ Form::text('expiredPeriod', $refData['expiredPeriod'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('isRF', __('messages.IsRFID')) }}<br>
                                {{ Form::select('isRfid', $yesOrNo, isset($refData['isRfid']) ? $refData['isRfid'] : 'no', ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('rfidType', __('messages.RfidType')) }}<br>
                                {{ Form::select('rfidType', $rfidList, isset($refData['rfidType']) ? $refData['rfidType'] : 'no', ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('descriptionStatus', __('messages.Description/Remarks')) }}
                                {{ Form::text('descriptionStatus', $refData['descriptionStatus'], ['class' => 'form-control']) }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('countryTimezone', __('messages.CountryTimezone')) }}<br>
                                {{ Form::select('countryTimezone', $countryTimezoneList, $refData['countryTimezone'], ['class' => 'selectpicker form-control', 'data-live-search ' => 'true']) }}
                            </div>

                        </div>

                        <div class="col-md-6">
                            {{ Form::hidden('orgId', 'Default', old('orgId'), ['class' => 'form-control']) }}
                            <div class="form-group">
                                {{ Form::label('oprName', __('messages.TelecomOperatorName')) }}
                                {{ Form::select('oprName', $oprNameList , $refData['oprName'], ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('mobileNo', __('messages.MobileNumberforAlerts')) }}
                                {{ Form::text('mobileNo', $refData['mobileNo'], ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('odoDistance', __('messages.OdometerReading')) }}
                                {{ Form::text('odoDistance', $refData['odoDistance'], ['class' => 'form-control']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('driverName', __('messages.DriverName')) }}
                                {{ Form::text('driverName', $refData['driverName'], ['class' => 'form-control']) }}

                            </div>

                            <div class="form-group">
                                {{ Form::label('driverMobile', __('messages.DriverMobileNo')) }}
                                {{ Form::number('driverMobile', $refData['driverMobile'], ['class' => 'form-control']) }}

                            </div>

                            <div class="form-group">
                                {{ Form::label('gpsSimNo', __('messages.GPSSimNumber')) }}
                                {{ Form::text('gpsSimNo', $refData['gpsSimNo'], ['class' => 'form-control', 'maxlength' => 15, 'minlength' => 10]) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('email', __('messages.EmailforNotification')) }}
                                {{ Form::text('email', $refData['email'], ['class' => 'form-control']) }}
                            </div>


                            <div class="form-group">
                                {{ Form::label('parkingAlert', __('messages.ParkingAlert')) }}
                                {{ Form::select('parkingAlert', $yesOrNo, $refData['parkingAlert'], ['class' => 'form-control']) }}

                            </div>


                            <div class="form-group">
                                {{ Form::label('sendGeoFenceSMS', __('messages.SendGeoFenceSMS')) }}
                                {{ Form::select('sendGeoFenceSMS', $yesOrNo, $refData['sendGeoFenceSMS'], ['class' => 'form-control']) }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('altShort', __('messages.AlternateShortName')) }}
                                {{ Form::text('altShortName', $refData['altShortName'], ['class' => 'form-control']) }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('paymentType', __('messages.PaymentType')) }}
                                {{ Form::text('paymentType', $refData['paymentType'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('fuel', __('messages.Fuel')) }}
                                {{ Form::select('fuel', $yesOrNo, $refData['fuel'], ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('License1', __('messages.Licence')) }}
                                {{ Form::select('Licence1', $Licence, $refData['Licence'], ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('Payment_Mode1', __('messages.PaymentMode')) }}
                                {{ Form::select('Payment_Mode1', $Payment_Mode, $refData['Payment_Mode'], ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('mintem', __('messages.MinimumTemperature')) }}
                                {{ Form::number('mintemp', $refData['mintemp'], ['class' => 'form-control', 'placeholder' => 'Quantity', 'min' => '-100']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('maxtem', __('messages.MaximumTemperature')) }}
                                {{ Form::number('maxtemp', $refData['maxtemp'], ['class' => 'form-control', 'placeholder' => 'Quantity', 'min' => '-100']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('safetyParking', __('messages.SafetyParking')) }}<br>
                                {{ Form::select('safetyParking', $yesOrNo, isset($refData['safetyParking']) ? $refData['safetyParking'] : 'no', ['class' => 'form-control']) }}
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-4">
                        {{ Form::submit(__('messages.Update'), ['class' => 'btn btn-primary']) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection