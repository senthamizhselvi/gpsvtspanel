@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="page-inner">
            <div class="page-header">
                <div class="page-title">
                    Vehicles Create
                </div>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">{{ $deviceId }}</a>
                    </li>
                </ul>
            </div>
			<div class="card">
				<div class="card-body">
					<span class="text-danger error-message">
						{{ HTML::ul($errors->all()) }}
					</span>
					{{ Form::open(['url' => 'vdmVehicles']) }}
					<div class="row mx-2">
						<div class="col-md-6 form-group">
							{{ Form::label('vehicleId', 'Vehicle ID') }}
							{{ Form::text('vehicleId', request()->old('vehicleId'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('deviceId', 'Device ID') }}
							{{ Form::text('deviceId1', $deviceId, ['class' => 'form-control', 'disabled' => 'disabled']) }}
							{{ Form::hidden('deviceId', $deviceId, request()->old('deviceId'), ['class' => 'form-control', 'disabled' => 'disabled']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('shortName', 'Short Name') }}
							{{ Form::text('shortName', request()->old('shortName'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('deviceModel', 'Device Model') }}
							{{ Form::text('deviceModel1', $deviceModel, ['class' => 'form-control', 'disabled' => 'disabled']) }}
							{{ Form::hidden('deviceModel', $deviceModel, request()->old('deviceModel'), ['class' => 'form-control', 'disabled' => 'disabled']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('regNo', 'Vehicle Registration Number') }}
							{{ Form::text('regNo', request()->old('regNo'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('vehicleType', 'Vehicle Type') }}
							{{ Form::select('vehicleType', ['Car' => 'Car', 'Truck' => 'Truck', 'Bus' => 'Bus'], request()->old('vehicleType'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('overSpeedLimit', 'OverSpeed Limit') }}
							{{ Form::select('overSpeedLimit', ['10' => '10', '20' => '20', '30' => '30', '40' => '40', '50' => '50', '60' => '60', '70' => '70', '80' => '80', '90' => '90', '100' => '100', '110' => '110', '120' => '120', '130' => '130', '140' => '140', '150' => '150'], request()->old('overSpeedLimit'), ['class' => 'form-control']) }}
	
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('morningTripStartTime', 'Morning Trip Start Time') }}
							{{ Form::text('morningTripStartTime', request()->old('morningTripStartTime'), ['class' => 'form-control']) }}
	
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('eveningTripStartTime', 'Evening Trip Start Time') }}
							{{ Form::text('eveningTripStartTime', request()->old('eveningTripStartTime'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('orgId', 'org/College Name') }}
							{{ Form::select('orgId', $orgList, request()->old('orgId'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('oprName', 'Telecom Operator Name') }}
							{{ Form::select('oprName', ['airtel' => 'airtel', 'reliance' => 'reliance', 'idea' => 'idea'], request()->old('oprName'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('mobileNo', 'Mobile Number for Alerts') }}
							{{ Form::text('mobileNo', request()->old('mobileNo'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('odoDistance', 'Odometer Reading') }}
							{{ Form::text('odoDistance', request()->old('odoDistance'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('driverName', 'Driver Name') }}
							{{ Form::text('driverName', request()->old('driverName'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('gpsSimNo', 'GPS Sim Number') }}
							{{ Form::text('gpsSimNo', request()->old('gpsSimNo'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('email', 'Email for Notification') }}
							{{ Form::text('email', request()->old('email'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('fuel', 'Fuel') }}
							{{ Form::select('fuel', ['no' => 'No', 'yes' => 'Yes'], request()->old('fuel'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							{{ Form::label('isRfi', 'IsRfid') }}
							{{ Form::select('isRfid', ['no' => 'No', 'yes' => 'Yes'], request()->old('isRfid'), ['class' => 'form-control']) }}
						</div>
	
						<div class="col-md-6 form-group">
							{{ Form::label('altShort', 'Alternate Short Name') }}
							{{ Form::text('altShortName', request()->old('altShortName'), ['class' => 'form-control']) }}
						</div>
						<div class="col-md-12 form-group text-center">
							{{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
						</div>
					</div>
					{{ Form::close() }}
				</div>
			</div>
        </div>
    </div>
@endsection
