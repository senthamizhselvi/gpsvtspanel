@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="page-inner">

            <div class="pull-right">
                <a href="{{ route('VehicleScan/sendExcel') }}">
                    <img class="pull-right" width="40px" height="50px" src="{{ asset('assets/imgs/xls.svg') }}" alt="xls"
                        method="get" />
                </a>
            </div>
            @if (session()->has('message'))
                <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                    {{ session()->get('message') }}
                </p>
            @endif
            <div class="page-header">
                <h4 class="page-title">
                    @if (session()->get('vCol') == '1')
                        @lang('messages.VehiclesList')
                    @endif
                    @if (session()->get('vCol') == '2')
                        @lang('messages.ViewVehicles')
                    @endif
                </h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                        @if (session()->get('vCol') == '1')
                            @lang('messages.VehiclesList')
                        @endif
                        @if (session()->get('vCol') == '2')
                            @lang('messages.ViewVehicles')
                        @endif
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card">
                <div class="card-body">
                    <span class="text-danger"> {{ HTML::ul($errors->all()) }}</span>
                    <div class="table-responsive">
                        <table id="example1"
                            class="display table table-striped table-hover w-100 table-head-bg-primary">
                            <thead>
                                <tr>
                                    @if (session()->get('cur1') == 'prePaidAdmin')
                                        <th style="text-align: center;">
                                            @lang('messages.LicenceID')
                                        </th>
                                    @endif
                                    <th style="text-align: center;">
                                        @lang('messages.AssetID')
                                    </th>
                                    <th style="text-align: center;">
                                        @lang('messages.VehicleName')
                                    </th>
                                    <th style="text-align: center;">
                                        @lang('messages.OrgName')
                                    </th>
                                    <th style="text-align: center;">
                                        @lang('messages.DeviceID')
                                    </th>
                                    <th style="text-align: center;">
                                        @lang('messages.ServerName')
                                    </th>
                                    <th style="text-align: center;">
                                        @lang('messages.LastCommTime')
                                    </th>
                                    <th style="text-align: center;">
                                        @lang('messages.GPSSimNo')
                                    </th>
                                    @if (session()->get('vCol') == '2')
                                        <th style="text-align: center;">
                                            @lang('messages.Status')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.DeviceModel')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.Version')
                                        </th>

                                        <th style="text-align: center;">
                                            @lang('messages.TimeZone')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.APN')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.LicenceIssuedDate')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.OnboardDate')
                                        </th>
                                        @if (session()->get('cur1') == 'prePaidAdmin')
                                            <th style="text-align: center;">
                                                @lang('messages.LicenceExpireDate')
                                            </th>
                                        @endif
                                        @if (session()->get('cur1') != 'prePaidAdmin' && session()->get('cur1') != 'prePaidDealer')
                                            <th style="text-align: center;">
                                                @lang('messages.VehicleExpireDate')
                                            </th>
                                        @endif

                                    @endif
                                    @if (session()->get('vCol') == '1')
                                        <th style="text-align: center;">
                                            @lang('messages.LicenceIssuedDate')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.OnboardDate')
                                        </th>
                                        @if (session()->get('cur1') == 'prePaidAdmin')
                                            <th style="text-align: center;">
                                                @lang('messages.LicenceExpireDate')
                                            </th>
                                        @endif
                                        @if (session()->get('cur1') != 'prePaidAdmin' && session()->get('cur1') != 'prePaidDealer')
                                            <th style="text-align: center;">
                                                @lang('messages.VehicleExpireDate')
                                            </th>
                                        @endif
                                        <th style="text-align: center;">
                                            @lang('messages.Actions')
                                        </th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody style="word-break: break-all;">
                                @foreach ($vehicleList as $key => $value)
                                    <tr style="text-align: center;">

                                        @if (session()->get('cur1') == 'prePaidAdmin')
                                            <td>{{ Arr::get($licenceIdList, $value) }}</td>
                                        @endif
                                        <td>{{ $value }}</td>
                                        <td>{{ Arr::get($shortNameList, $value) }}</td>
                                        <td>{{ Arr::get($orgIdList, $value) }}</td>
                                        <td>{{ Arr::get($deviceList, $value) }}</td>
                                        <td>{{ Arr::get($servernameList, $value) }}</td>
                                        <td>{{ Arr::get($commList, $value) }}</td>
                                        <td>{{ Arr::get($mobileNoList, $value) }}</td>

                                        @if (session()->get('vCol') == '2')

                                            <td>
                                                @if (Arr::get($statusList, $value) == 'P')
                                                    <div style="color: #8e8e7b">
                                                        @lang('messages.Parking')
                                                    </div>
                                                @endif
                                                @if (Arr::get($statusList, $value) == 'M')
                                                    <div style="color: #00b374">
                                                        @lang('messages.Moving')
                                                    </div>
                                                @endif
                                                @if (Arr::get($statusList, $value) == 'S')
                                                    <div style="color: #ff6500">
                                                        @lang('messages.Idle')
                                                    </div>
                                                @endif
                                                @if (Arr::get($statusList, $value) == 'U')
                                                    <div style="color: #fe068d">
                                                        @lang('messages.NoData')
                                                    </div>
                                                @endif
                                                @if (Arr::get($statusList, $value) == 'N')
                                                    <div style="color: #0a85ff">
                                                        @lang('messages.NewDevice')
                                                    </div>
                                                @endif
                                            </td>


                                            <td>{{ Arr::get($deviceModelList, $value) }}</td>
                                            <td>{{ Arr::get($versionList, $value) }}</td>
                                            <td>{{ Arr::get($timezoneList, $value) }}</td>
                                            <td>{{ Arr::get($apnList, $value) }}</td>
                                            <td>{{ Arr::get($licenceList, $value) }}</td>
                                            <td>{{ Arr::get($onboardDateList, $value) }}</td>
                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                <td>{{ Arr::get($LicexpiredPeriodList, $value) }}</td>
                                            @endif
                                            @if (session()->get('cur1') != 'prePaidAdmin' && session()->get('cur1') != 'prePaidDealer')
                                                <td>{{ Arr::get($expiredList, $value) }}</td>
                                            @endif
                                        @endif
                                        @if (session()->get('vCol') == '1')
                                            <td>{{ Arr::get($licenceList, $value) }}</td>
                                            <td>{{ Arr::get($onboardDateList, $value) }}</td>
                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                <td>{{ Arr::get($LicexpiredPeriodList, $value) }}</td>
                                            @endif
                                            @if (session()->get('cur1') != 'prePaidAdmin' && session()->get('cur1') != 'prePaidDealer')
                                                <td>{{ Arr::get($expiredList, $value) }}</td>
                                            @endif
                                            <td>
                                                <div class="dropdown dropleft">
                                                    <button class="btn btn-info text-white btn-sm dropdown-toggle"
                                                        type="button" id="dropdownMenuButton{{ $key }}"
                                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        @lang('messages.Action')
                                                    </button>
                                                    <div class="dropdown-menu"
                                                        aria-labelledby="dropdownMenuButton{{ $key }}">
                                                        @if (Arr::get($licTypeList, $value) !== 'StarterPlus')
                                                            <a class="btn btn-sm btn-success text-white dropdown-item mb-1"
                                                                href="{{ URL::to('vdmVehicles/migration/' . $value) }}">
                                                                @lang('messages.Migration')
                                                            </a>
                                                        @endif

                                                        <a class="btn btn-sm btn-primary text-white dropdown-item mb-1"
                                                            href="{{ URL::to('vdmVehicles/edit/' . $value) }}">
                                                            @lang('messages.Edit')
                                                        </a>
                                                        @if ((Arr::get($licTypeList, $value) !== 'Basic' && Arr::get($licTypeList, $value) !== 'Starter' && Arr::get($licTypeList, $value) !== 'StarterPlus') || session()->get('cur1') !== 'prePaidAdmin')
                                                            <a class="btn btn-sm btn-warning dropdown-item mb-1"
                                                                href="{{ route('calibrate', ['vid' => $value, 'count' => '1']) }}">
                                                                @lang('messages.Calibrate')
                                                            </a>
                                                        @endif
                                                        <a class="btn btn-sm btn-info text-white dropdown-item mb-1"
                                                            href="{{ URL::to('vdmVehicles/rename/' . $value) }}">
                                                            @lang('messages.Rename')
                                                        </a>
                                                        <a class="btn btn-sm btn-secondary text-white dropdown-item mb-1"
                                                            onClick="setVehicle('{{$value}}',this)">
                                                            @lang('messages.Reprocess')
                                                        </a>

                                                        {{-- <a class="btn btn-sm btn-info text-white dropdown-item mb-1"
                                                            href="{{ URL::to('vdmVehicles/stops/' . $value, 'normal') }}">Show
                                                            Stops</a>

                                                        <a class="btn btn-sm btn-danger text-white dropdown-item "
                                                            href="{{ URL::to('vdmVehicles/removeStop/' . $value, 'normal') }}">Remove
                                                            Stops</a> --}}
                                                    </div>
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{ Form::hidden('reprocessDate', date('d-m-Y', strtotime('-1 days'))) }}
                {{ Form::hidden('reprocessUrl', route('reprocess.vehicle')) }}
                {{ Form::hidden('_token', csrf_token()) }}
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        let titleText = "{{ __('messages.Reprocessforvehicle') }}"
        let submitText = "{{ __('messages.submitText') }}"
        let cancelText = "{{ __('messages.cancelText') }}"
    </script>
    <script src="{{ asset('common/js/vehicleReprocess.js') }}"></script>
@endpush
