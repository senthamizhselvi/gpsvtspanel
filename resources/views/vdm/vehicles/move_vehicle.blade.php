@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content  page-inner">
            <div class="hpanel">
                <div class="page-header">
                    <div class="page-title">
                        @lang('messages.MoveVehicle')
                    </div>
                </div>
                <div class="page-body">
                    <div class="card animated zoomIn">
                        <div class="card-body">
                            {{ HTML::ul($errors->all()) }}
                            {{ Form::open(['url' => 'vdmVehicles/moveVehicle']) }}
                            <div class="panel-body mt-3">
                                <div class="row">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-2">
                                        {{ Form::label('vehicleId', __('messages.AssetId/VehicleId').' :') }}
                                    </div>
                                    <div class="col-md-3">
                                        {{ Form::text('vehicleId', $vehicleId, ['class' => 'form-control', 'required' => 'required', 'disabled' => 'disabled']) }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-3">
                                        {{ Form::hidden('vehicleIdOld', $vehicleId, ['class' => 'form-control']) }}
                                        {{ Form::hidden('expiredPeriodOld', $expiredPeriod, ['class' => 'form-control']) }}{{ Form::hidden('deviceIdOld', $deviceId, ['class' => 'form-control']) }}
                                    </div>

                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">{{ Form::label('dealerId', __('messages.DealerName').' :') }}</div>
                                    <div class="col-md-3">
                                        {{ Form::select('dealerId', $dealerId, [$OWN], ['class' => 'form-control  selectpicker show-menu-arrow', 'data-live-search' => 'true', 'selected' => $OWN]) }}
                                    </div>
                                </div>
                                <br>

                                <br>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        {{ Form::submit(__('messages.confirm'), ['class' => 'btn  btn-sm btn-success']) }}
                                        <a class="btn btn-sm btn-danger"
                                         href="{{ URL::to('Device') }}">
                                            @lang('messages.Cancel')
                                        </a>
                                    </div>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
