<style>
.topnav {
  overflow: hidden;
  background-color: #2fd0c1;
  overflow: hidden;
  border-radius: 17px;
}

.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #111;
  color: black;
}
.btn-inf{
  background-color: #ffb606;
    border-color: #ffb606;
    color: #FFFFFF;
}
.active {
    background-color: #3a8a83;
    color: #34455eb8;
}

.topnav .icon {
  display: none;
}

@media screen and (max-width: 600px) {
  .topnav a:not(:first-child) {display: none;}
  .topnav a.icon {
    float: right;
    display: block;
  }
}

@media screen and (max-width: 600px) {
  .topnav.responsive {position: relative;}
  .topnav.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
}
</style>

<div class="topnav" id="fuelConfig">
  @for($i=1;$i<=$numberofColumn;$i++)
  <a id="tank<?php echo $i ?>" onclick="getvalues2('<?php echo $vehicleId ?>','<?php echo $i ?>')" style="margin-left: 1%;color:white;">Tank <?php echo $i ?> </a>
  @endfor
   <h4 style="padding-top:5px;" align="center"><font size="5px" color="#ffffff" face="Georgia" >Fuel Configuration</font></h4>
  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}
  
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
 

});

function getvalues2(vehicle,tankId)  {
//  alert(" Hai "+vehicle+" sensorId "+sensorId);
  //'vdmVehicles/calibrateOil/' . $
  var formAction = "../vdmfuel/"+vehicle+"&"+tankId;
		window.location =  formAction;
}


</script>
