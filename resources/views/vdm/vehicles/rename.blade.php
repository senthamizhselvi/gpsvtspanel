@extends('layouts.app')
@section('content')


    <div id="wrapper">
        <div class="content page-inner">
            <div class="page-header">
                <h4 class="page-title">
                    @lang('messages.VehicleRename')
                </h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('vdmVehicles.index') }}">
                            @lang('messages.VehicleList')
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                            @lang('messages.VehicleRename')
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">{{ $vehicleId }}</a>
                    </li>
                </ul>
            </div>
            @if (session()->has('message'))
                <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                    {{ session()->get('message') }}
                </p>
            @endif

            <div class="page-body">
                <div class="card">
                    <div class="card-body">
                        <span class="text-danger error-message">
                            {{ HTML::ul($errors->all()) }}
                        </span>
                        {{ Form::open(['url' => 'vdmVehicles/renameUpdate', 'id' => 'testForm']) }}
                        <div class="panel-body">
                            <br>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-3">
                                    {{ Form::label('vehicleId', __('messages.AssetId/VehicleId')) }} 
                                </div>
                                <div class="col-md-4">
                                    {{ Form::text('vehicleId', $vehicleId, ['class' => 'form-control caps', 'required' => 'required', 'id' => 'vehicleId']) }}
                                    <span id="span1" class="text-danger"></span>
                                </div>
                            </div>
                            {{ Form::hidden('vehicleIdOld', $vehicleIdOld, ['class' => 'form-control','id'=>'vehicleIdOld']) }}
                            {{ Form::hidden('deviceIdOld', $deviceIdOld, ['class' => 'form-control']) }}
                            {{ Form::hidden('expiredPeriodOld', $expiredPeriodOld, ['class' => 'form-control']) }}
                            <br>
                            <div class="row mt-4">
                                <div class="col-md-3 mx-auto text-center">
                                    {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary']) }}
                                    <a class="btn btn-danger" href="{{ URL::to('VdmVehicleScan' . $vehicleId) }}">
                                        @lang('messages.Cancel')
                                    </a>
                                </div>

                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $('#vehicleId').on('change', function() {
            $("#span1").text("");
        });
        $('#testForm').on('submit', function(e) {

            vehicleIdOld = $('#vehicleIdOld').val();
            vehicleId = $('#vehicleId').val();
            if(vehicleIdOld == vehicleId){
                wornigAlert(null,"Newly entered Vehicle Id is same as Old Vehicle Id. Please change.")
                return false;
            }
            var minLength = 5;
            var maxLength = 30;
            var datas = {
                'vehicleId': $('#vehicleId').val()
            }
            if (datas.vehicleId.length < minLength) {
                e.preventDefault();
                $("#span1").text("vehicle Id is short");
                return false;
            }
        });
    </script>
@endpush
