@extends("layouts.app")
@section('content')

    <div id="wrapper">
        <div class="content animated zoomIn">
            <div class="">
                <div class=" page-inner">
                <div class="hpanel">
                    <div class="pull-right">
                        <a href="{{ route('VehicleScan/sendExcel') }}"><img class="pull-right" width="40px" height="50px"
                                src="{{ asset('assets/imgs/xls.svg') }}" alt="xls" method="get" /></a>
                    </div>
                    <div class="page-header">
                        <h4 class="page-title">
                            <b>
                                @lang('messages.VehiclesSearch')
                            </b>
                        </h4>
                    </div>
                    @if (session()->has('message'))
                        <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                            {{ session()->get('message') }}</p>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            {{ Form::open(['url' => 'VdmVehicleScan', 'method' => 'post']) }}
                            <div class="row p-1">
                                <div class="col-md-4 form-group">
                                    {{ Form::label(__('messages.VehiclesSearch')) }}
                                    <i class="fa fa-info-circle fa-lg text-warning ml-auto" data-toggle="tooltip"
                                        data-placement="right" title=" Use * for getting all Vehicles in the search"></i>
                                    <br>
                                    {{ Form::text('text_word',old('text_word'), ['class' => 'form-control', 'placeholder' => 'Search Vehicle','required']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::submit(__('messages.Search'), ['class' => 'btn btn-primary mt-2 mt-md-4']) }}
                                </div>
                            </div>

                            {{ Form::close() }}
                        </div>
                        <div class="card-body p-2">
                            <span class="text-danger error-message"> {{ HTML::ul($errors->all()) }}</span>
                            <div id="tabNew" class="table-responsive pt-4">
                                <table id="example1"
                                    class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer mt-5">
                                    <thead>
                                        <tr>
                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                <th style="text-align: center;">
                                                    @lang('messages.LicenceID')
                                                </th>
                                            @endif
                                            <th style="text-align: center;">
                                                @lang('messages.AssetID')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.VehicleName')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.OrgName')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.DeviceID')
                                            </th>
                                            @if (session()->get('cur') == 'admin')
                                                <th style="text-align: center;">
                                                    @lang('messages.DeviceName')
                                                </th>
                                            @endif
                                            <th style="text-align: center;">
                                                @lang('messages.GpsSimNo')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.TimeZone')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.APN')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.LastCommTime')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.status')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.DeviceModel')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.LicenceIssuedDate')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.OnBoardDate')
                                            </th>
                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                <th style="text-align: center;">
                                                    @lang('messages.LicExpDate')
                                                </th>
                                            @endif
                                            @if (session()->get('cur1') != 'prePaidAdmin')
                                                <th style="text-align: center;">
                                                    @lang('messages.VehExpDate')
                                                </th>
                                            @endif
                                            <th style="text-align: center;">
                                                @lang('messages.Action')
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody style="word-break: break-all;">
                                        @foreach ($vehicleList as $key => $value)
                                            <tr>
                                                @if (session()->get('cur1') == 'prePaidAdmin')
                                                    <td>{{ Arr::get($licenceIdList, $value) }}</td>
                                                @endif
                                                <td style="text-overflow: clip !important;">{{ $value }}</td>
                                                <td>{{ Arr::get($shortNameList, $value) }}</td>
                                                <td>{{ Arr::get($orgIdList, $value) }}</td>
                                                <td>{{ Arr::get($deviceList, $value) }}</td>
                                                @if (session()->get('cur') == 'admin')
                                                    <td>{{ Arr::get($owntype, $value) }}</td>
                                                @endif
                                                <td>{{ Arr::get($mobileNoList, $value) }}</td>
                                                <td>{{ Arr::get($timezoneList, $value) }}</td>
                                                <td>{{ Arr::get($apnList, $value) }}</td>
                                                <td>{{ Arr::get($commList, $value) }}</td>



                                                <td>
                                                    @if (Arr::get($statusList, $value) == 'P')
                                                        <div style="color: #8e8e7b">
                                                            @lang('messages.PARKING')
                                                        </div>
                                                    @endif
                                                    @if (Arr::get($statusList, $value) == 'M')
                                                        <div style="color: #00b374">
                                                            @lang('messages.Moving')
                                                        </div>
                                                    @endif
                                                    @if (Arr::get($statusList, $value) == 'S')
                                                        <div style="color: #ff6500">
                                                            @lang('messages.Idle')
                                                        </div>
                                                    @endif
                                                    @if (Arr::get($statusList, $value) == 'U')
                                                        <div style="color: #fe068d">
                                                            @lang('messages.NoData')
                                                        </div>
                                                    @endif
                                                    @if (Arr::get($statusList, $value) == 'N')
                                                        <div style="color: #0a85ff">
                                                            @lang('messages.NewDevice')
                                                        </div>
                                                    @endif
                                                </td>


                                                <td>{{ Arr::get($deviceModelList, $value) }}</td>
                                                <td>{{ Arr::get($licenceissuedDate, $value) }}</td>
                                                <td>{{ Arr::get($onboardDateList, $value) }}</td>
                                                @if (session()->get('cur1') == 'prePaidAdmin')
                                                    <td>{{ Arr::get($LicexpiredPeriodList, $value) }}</td>
                                                @endif
                                                @if (session()->get('cur1') != 'prePaidAdmin')
                                                    <td style="text-align: center;">
                                                        {{ Arr::get($expiredList, $value) }}
                                                    </td>
                                                @endif

                                                @if (session()->get('vCol') == '1')
                                                    <td>
                                                        <div class="dropdown dropleft">
                                                            <button class="btn btn-info btn-sm text-white dropdown-toggle"
                                                                type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                                aria-haspopup="true" aria-expanded="false">
                                                                @lang('messages.Action')
                                                            </button>
                                                            <div class="dropdown-menu"
                                                                aria-labelledby="dropdownMenuButton">

                                                                @if (Arr::get($licTypeList, $value) !== 'StarterPlus')
                                                                    <a class="btn btn-sm text-white btn-success dropdown-item mb-1"
                                                                        href="{{ URL::to('vdmVehicles/migration/' . $value) }}">
                                                                        @lang('messages.Migration')
                                                                    </a>
                                                                @endif

                                                                <a class="btn btn-sm text-white btn-primary dropdown-item mb-1"
                                                                    href="{{ URL::to('vdmVehicles/edit/' . $value) }}">
                                                                    @lang('messages.Edit')
                                                                </a>
                                                                <a class="btn btn-sm btn-success dropdown-item text-white mb-1"
                                                                    href="{{route('audit',['type'=>'vehicle','name'=>$value ])}}">
                                                                    @lang('messages.AuditReport')
                                                                </a>
                                                                {{-- <a class="btn btn-sm text-white btn-info dropdown-item mb-1"
                                                                    href="" id="auditReport"
                                                                    onclick="auditVehicle('{{ $value }}');">
                                                                    @lang('messages.AuditReport')
                                                                </a> --}}
                                                                @if ((Arr::get($licTypeList, $value) !== 'Basic' && Arr::get($licTypeList, $value) !== 'Starter' && Arr::get($licTypeList, $value) !== 'StarterPlus') || session()->get('cur1') !== 'prePaidAdmin')
                                                                    <a class="btn btn-sm text-white btn-warning dropdown-item mb-1"
                                                                        href="{{ route('calibrate', ['vid' => $value , 'count' => '1']) }}">
                                                                        @lang('messages.Calibrate')
                                                                    </a>
                                                                @endif
                                                                <a class="btn btn-sm text-white btn-info dropdown-item mb-1"
                                                                    href="{{ URL::to('vdmVehicles/rename/' . $value) }}">
                                                                    @lang('messages.Rename')
                                                                </a>

                                                                <a class="btn btn-sm text-white btn-secondary dropdown-item mb-1"
                                                                    onClick="setVehicle('{{ $value }}',this)">
                                                                    @lang('messages.Reprocess')
                                                                </a>
                                                            </div>
                                                        </div>

                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{ Form::hidden('reprocessDate', date('d-m-Y', strtotime('-1 days'))) }}
                            {{ Form::hidden('reprocessUrl', route('reprocess.vehicle')) }}
                            {{ Form::hidden('_token', csrf_token()) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


@endsection
@push('js')
    <script>
        let titleText = "{{ __('messages.Reprocessforvehicle') }}"
        let submitText = "{{ __('messages.submitText') }}"
        let cancelText = "{{ __('messages.cancelText') }}"
    </script>
    <script src="{{ asset('common/js/vehicleReprocess.js') }}"></script>
@endpush
