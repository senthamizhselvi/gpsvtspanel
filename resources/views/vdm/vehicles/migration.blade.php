@extends('layouts.app')
@section('content')

    <div id="wrapper">
        <div class="content page-inner">
            <div class="hpanel">

                <div class="page-header">
                    <div class="page-title">
                        @lang('messages.VehicleMigration')
                    </div>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('vdmVehicles.index') }}">
                                @lang('messages.VehicleList')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.VehicleMigration')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">{{ $vehicleId }}</a>
                        </li>
                    </ul>
                </div>
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif
                <div class="page-body">
                    <div class="card animated zoomIn">
                        <div class="card-header">
                            <p class="h3">
                                @lang('messages.VehicleMigration')
                            </p>
                        </div>
                        <div class="card-body">
                            <span class="text-danger error-message">
                                {{ HTML::ul($errors->all()) }}
                            </span>
                            {{ Form::open(['url' => route('migrationUpdate'), 'id' => 'testForm']) }}
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        {{ Form::label('vehicleId', __('messages.AssetId/VehicleId') . ' :') }}</div>
                                    <div class="col-md-4">
                                        {{ Form::text('vehicleId', $vehicleId, ['class' => 'form-control ', 'required' => 'required', 'id' => 'vehicleId']) }}
                                        {{ Form::hidden('vehicleIdOld', $vehicleIdOld, ['class' => 'form-control']) }}
                                        {{ Form::hidden('deviceIdOld', $deviceIdOld, ['class' => 'form-control']) }}
                                        {{ Form::hidden('expiredPeriodOld', $expiredPeriodOld, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        {{ Form::label('deviceId', __('messages.DeviceId/IMEINo') . ' :') }}</div>
                                    <div class="col-md-4">
                                        {{ Form::text('deviceId', $deviceId, ['class' => 'form-control caps', 'required' => 'required', 'id' => 'deviceId']) }}
                                    </div>
                                </div>
                                <br>
                                <div class="row mt-3">
                                    <div class="col-md-12 text-center">
                                        <a class="btn btn-danger" href="{{ URL::to('VdmVehicleScan' . $vehicleId) }}">
                                            @lang('messages.Cancel')
                                        </a>
                                        {{ Form::submit(__('messages.MigrateVehicle'), ['class' => 'btn btn-primary']) }}

                                    </div>

                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $('#testForm :submit').attr('disabled', true);
        $("#testForm input").one('change keydown', function() {
            $('#testForm :submit').attr('disabled', false);
        })
        $('#vehicleId').on('change', function() {
            $("#span1").text("");
        });
        $('#deviceId').on('change', function() {
            $("#span2").text("");

        });
        $('#testForm').on('submit', function(e) {
            var minLength = 5;
            var maxLength = 30;
            var submit = true;
            var datas = {
                'vehicleId': $('#vehicleId').val(),
                'deviceId': $('#deviceId').val()
            }
            if (datas.vehicleId.length < minLength) {
                $('#vehicleId').aftererr('Vehicle Id is short');
                submit = false;
            }
            if (datas.deviceId.length < minLength) {
                $('#deviceId').aftererr('Device Id is short');
                submit = false;
            }
            return submit;
        });
    </script>
@endpush
