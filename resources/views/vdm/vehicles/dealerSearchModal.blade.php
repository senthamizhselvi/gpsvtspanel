<div class="row">
    <div class="col-md-9 form-group mx-auto">
        {{ Form::label('dealerId', __("messages.DealerName"), ['class' => 'b h3']) }}
        {{ Form::select('dealerId', $dealerArr, old('dealerId'), ['class' => 'form-control selectpicker', 'data-live-search ' => 'true']) }}
        <span class="text-danger error-message switchErr"> </span>
    </div>
</div>
