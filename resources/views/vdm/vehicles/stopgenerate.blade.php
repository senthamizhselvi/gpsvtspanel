@extends('layouts.app')
@section('content')

    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">
                @lang('messages.GenerateStops')
            </h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="{{ route('vdmVehicles.index') }}">
                    @lang('messages.VehicleList')
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">
                        @lang('messages.GenerateStops')
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">{{ $vehicleId }}</a>
                </li>
            </ul>
        </div>
        @if (session()->has('message'))
            <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                {{ session()->get('message') }}</p>
        @endif

        <div class="page-body">
            <div class="card ">
                <div class="card-body">
                    <span class="text-danger error-message"> {{ HTML::ul($errors->all()) }}</span>
                    {{ Form::open(['url' => 'vdmVehicles/generate']) }}
                    <h3 class="text-primary">
                       @lang('messages.General')
                    </h3>
                    <hr>
                    <div class="row p-2">
                        <div class="col-md-6 form-group">
                            {{ Form::label('vehicle', __('messages.VehicleId')) }}
                            {{ Form::label('vehicleId', $vehicleId, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                            {{ Form::hidden('vehicleId', $vehicleId) }}
                        </div>
                        <div class="col-md-6 form-group">
                            {{ Form::label('Date', __('messages.Date')) }}
                            {{ Form::text('date', request()->old('date'), ['id' => 'calendar', 'placeholder' => 'dd-mm-yyyy', 'class' => 'form-control date', 'required' => 'required']) }}
                        </div>
                        <div class="col-md-6 form-group">
                            {{ Form::label('spd', __('messages.Speed')) }}
                            {{ Form::select('type', ['0' => 'Strict', '5' => 'Medium', '10' => 'Relax'], request()->old('type'), ['class' => 'form-control']) }}
                        </div>
                        <div class="col-md-6 form-group">
                            {{ Form::label('demo', __('messages.Type')) }}
                            {{ Form::text('demo', $demo, ['class' => 'form-control', 'readonly' => 'true']) }}
                        </div>
                    </div><br>
                    <h3 class="text-primary">
                        @lang('messages.StartEndTime')
                    </h3>
                    <hr>
                    <div class="row p-2">
                        <div class="col-md-6 form-group ">
                            {{ Form::label('MST', __('messages.MorningStartTime')) }}
                            {{ Form::text('mst', request()->old('mst'), ['placeholder' => 'Hour : Min', 'class' => 'form-control time', 'required' => 'required']) }}
                        </div>
                        <div class="col-md-6 form-group ">
                            {{ Form::label('MET', __('messages.MorningEndTime')) }}
                            {{ Form::text('met', request()->old('met'), ['placeholder' => 'Hour : Min', 'class' => 'form-control time', 'required' => 'required']) }}
                        </div>
                        <div class="col-md-6 form-group ">
                            {{ Form::label('EST', __('messages.EveningStartTime')) }}
                            {{ Form::text('est', request()->old('est'), ['placeholder' => 'Hour : Min', 'class' => 'form-control time', 'required' => 'required']) }}
                        </div>
                        <div class="col-md-6 form-group">
                            {{ Form::label('EET', __('messages.EveningEndTime')) }}
                            {{ Form::text('eet', request()->old('eet'), ['placeholder' => 'Hour : Min', 'class' => 'form-control time', 'required' => 'required']) }}
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-3 mx-auto">{{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary']) }}
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

    </div>
@endsection
@push('js')
    <script>
        $(function() {
            $(".date").datepicker({
                format: 'DD-MM-YYYY',
            });
            $('.time').timepicker();
        });
    </script>
@endpush
