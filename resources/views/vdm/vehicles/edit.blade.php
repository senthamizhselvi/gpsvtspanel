@extends("layouts.app")
@section('content')

    <div id="wrapper">
        <div class="content animate-panel">
            <div class="page-inner">
                <div class="page-header">
                    <div class="page-title">
                        @lang('messages.EditVehicle')
                    </div>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('vdmVehicles.index') }}">
                                @lang('messages.VehicleList')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.VehicleEdit')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">{{ $vehicleId }}</a>
                        </li>
                    </ul>
                </div>
                <div class="card">
                    <div class="card-body" style="padding: 10px 20px">
                        <span class="text-danger error-message">{{ HTML::ul($errors->all()) }}</span>
                        {{ Form::model($vehicleId, ['route' => ['vdmVehicles.update', $vehicleId], 'method' => 'PUT']) }}
                        <br>
                        <h3 class="text-primary" id="v1">
                            @lang('messages.VehicleInformation')
                        </h3>
                        <hr>
                        <div class="row p-2">
                            <div class="form-group col-md-6">
                                {{ Form::label('vehicleId', __('messages.AssetID')) }}
                                {{ Form::label('vehicleIdl', $vehicleId, ['class' => 'form-control']) }}
                                {{ Form::hidden('vehicleId', $vehicleId, ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('VehicleName', __('messages.VehicleName')) }}
                                {{ Form::text('VehicleName', $refData['shortName'], ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('regNo', __('messages.VehicleRegistrationNumber')) }}
                                {{ Form::text('regNo', $refData['regNo'], ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('vehicleType', __('messages.VehicleType'), ['style' => 'margin-bottom:11px;']) }}
                                @if (session()->get('cur2') != 'ktrackAdmin')
                                    {{ Form::select('vehicleType', $vehicleTypeList1, $refData['vehicleType'], ['class' => 'form-control selectpicker', 'data-live-search ' => 'true']) }}
                                @else
                                    {{ Form::select('vehicleType', $vehicleTypeList2, 'select', ['class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true', 'required' => 'required']) }}
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('deviceModel', __('messages.DeviceModel'), ['style' => 'margin-bottom:11px;']) }}
                                {{ Form::select('deviceModel', $protocol, $refData['deviceModel'], ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('vehicleModel', __('messages.VehicleModel'), ['style' => 'margin-bottom:11px;']) }}
                                {{ Form::select('vehicleModel', $vehicleModelList, $refData['vehicleModel'], ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true','id'=>"vehicleModel"]) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('deviceId', __('messages.DeviceId/IMEINo')) }}
                                {{ Form::text('deviceId', $refData['deviceId'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                            </div>
                            <div class="form-group col-md-6" id="otherVehicleModel">
                                {{ Form::label('deviceId',  __('messages.OtherVehicleModel')) }}
                                {{ Form::text('otherVehicleModel', "",array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('gpsSimNo', __('messages.GPSSimNumber')) }}
                                {{ Form::number('gpsSimNo', $refData['gpsSimNo'], ['class' => 'form-control', 'maxlength' => 15, 'minlength' => 10]) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('oprName', __('messages.TelecomOperatorName')) }}
                                {{ Form::select('oprName', $oprNameList, $refData['oprName'], ['class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true','id'=>'oprName']) }}
                            </div>

                            <div class="form-group col-md-6">
                                {{ Form::label('installationDate', __('messages.InstallationDate')) }}
                                @if ($enableVehicleExpiry == 'no' || session()->get('cur') == 'admin')
                                    <script>
                                        $(function() {
                                            $("#calendar1").datepicker({
                                                format: 'yyyy-mm-dd',
                                                endDate:'now',
                                            });
                                        });
                                    </script>
                                @endif
                                {{ Form::text('installationDate', $refData['installationDate'], ['id' => 'calendar1', 'class' => 'form-control', 'placeholder' => 'Installation Date']) }}
                            </div>
                            <div class="form-group col-md-6" id="otherTeleComProvider">
                                {{ Form::label('deviceId',__('messages.OtherTelecomOperator')) }}
                                {{ Form::text('otherTeleComProvider', "",array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('orgId', __('messages.OrganizationName'), ['style' => 'margin-bottom:11px;']) }}
                                {{ Form::select('orgId', $orgList, $refData['orgId'], ['class' => 'form-control selectpicker w-100', 'data-live-search ' => 'true']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('onboardDate', __('messages.OnboardDate')) }}
                                {{ Form::text('onboardDate', $refData['onboardDate1'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                            </div>

                            @if (session()->get('cur1') != 'prePaidAdmin')
                                <div class="form-group col-md-6">
                                    {{ Form::label('vehicleExpiry', __('messages.VehicleExpirationDate')) }}
                                    @if (($enableVehicleExpiry == 'yes' && session()->get('cur') == 'dealer') || (session()->get('cur') == 'admin' && $enableVehicleExpiry == 'yes'))
                                        <script>
                                            $(function() {
                                                $("#calendar").datepicker({
                                                    format: 'yy-mm-dd',
                                                    endDate:'now',
                                                });
                                            });
                                        </script>
                                        {{ Form::text('vehicleExpiry', $refData['vehicleExpiry'], ['id' => 'calendar', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::text('vehicleExpiry', $refData['vehicleExpiry'], ['id' => 'calendar', 'class' => 'form-control', 'readonly' => 'true']) }}
                                    @endif
                                </div>
                            @else
                                <div class="form-group col-md-6">
                                    {{ Form::label('vehicleExpiry1', __('messages.LicenceExpirationDate')) }}
                                    {{ Form::text('vehicleExpiry', $refData['vehicleExpiry'], ['id' => 'calendar', 'class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('licenceissuedDate', __('messages.LicenceIssuedDate')) }}
                                    {{ Form::text('licenceissuedDate', $refData['licenceissuedDate'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('License1', __('messages.Licence')) }}
                                    {{ Form::select('Licence1', $Licence, $refData['Licence'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                </div>
                                
                            @endif
                        </div>
                        @if ((session()->get('cur1') == 'prePaidAdmin' && $refData['Licence'] != 'Basic') || session()->get('cur1') != 'prePaidAdmin')
                            <br>
                            <h3 class="text-primary" id="v2">
                                @lang('messages.FuelSensorDetails')
                            </h3>
                            <hr>
                            <div class="row p-2">
                                <div class="form-group col-md-6">
                                    {{ Form::label('fuelMode', __('messages.FuelMode')) }}
                                    {{ Form::select('fuelModeSelect', ['manual' => 'Manual Calibrate', 'auto' => 'Auto Calibrate', 'direct' => 'Direct Fuel'], $refData['fuelMode'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                    {{ Form::hidden('fuelMode', $refData['fuelMode'], ['class' => 'form-control']) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('sensorCount', __('messages.SensorCount')) }}
                                    {{ Form::number('sensorCount', $refData['sensorCount'], ['id' => 'sensorcount', 'class' => 'form-control', 'readonly' => 'true']) }}
                                </div>

                                <div class="form-group col-md-6">
                                    {{ Form::label('noOfTank', __('messages.NumberofTank')) }}
                                    {{ Form::number('noOfTank', $refData['noOfTank'], ['id' => 'noOfTank', 'class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="form-group col-md-6" id="fuelType">
                                    {{ Form::label('fuelType', __('messages.FuelType')) }}<br>
                                    {{ Form::select('fuelType', $fuelTypeList, $refData['fuelType'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                </div>
                                <div class="form-group col-md-6" id="vehicleMode">
                                    {{ Form::label('vehicleMode', __('messages.ModeofVehicle')) }}
                                    {{ Form::select('vehicleMode', $vehicleModeList, $refData['vehicleMode'], ['class' => 'form-control', 'disabled' => 'true']) }}
                                </div>
                                <div class="form-group col-md-6" id="showtanksize">
                                    {{ Form::label('tankSize', __('messages.Tanksize')) }}
                                    {{ Form::number('tankSize', isset($refData['tankSize']) ? $refData['tankSize'] : '', ['id' => 'tanksize', 'class' => 'form-control', 'readonly' => 'true']) }}
                                </div>

                                <div class="form-group col-md-6">
                                    {{ Form::label('ignitionSpeed', __('messages.Speed')) }}
                                    {{ Form::select('ignitionSpeed', $ignitionSpeedList, $refData['ignitionSpeed'], ['class' => 'form-control']) }}
                                </div>

                                <div class="form-group col-md-6">
                                    <div class="row">
                                        <div class="col-md-3 col-12">
                                            <div class="d-flex flex-column">
                                                {{ Form::label('tankInterlinked', __('messages.TankInterlinked')) }}
                                                <label class="switch">
                                                    {{ Form::checkbox('customCheckbox', 'yes', $refData['tankInterlinked'] == 'no' ? false : true, ['class' => 'col-6 customCheckbox']) }}
                                                    <span class="slider"></span>
                                                    {{ Form::hidden('tankInterlinked', $refData['tankInterlinked'], ['class' => 'form-control hidden']) }}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="d-flex flex-column">
                                                {{ Form::label('fuelBatteryVolt', __('messages.FuelBatteryVolt')) }}
                                                <label class="switch">
                                                    {{ Form::checkbox('customCheckbox', 'yes', $refData['fuelBatteryVolt'] == 'no' ? false : true, ['class' => 'col-6 customCheckbox']) }}
                                                    <span class="slider"></span>
                                                    {{ Form::hidden('fuelBatteryVolt', $refData['fuelBatteryVolt'], ['class' => 'form-control hidden']) }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        @endif
                        <br>
                        <h3 class="text-primary" id="v3">
                            @lang('messages.ConfigurationDetails')
                        </h3>
                        <hr>
                        <div class="row p-2">
                            <div class="form-group col-md-6">
                                <div class="row m-0">
                                    <div class="col-md-3 col-12 ">
                                        <div class="d-flex flex-column">
                                            {{ Form::label('defaultMileage', __('messages.DeviceOdo')) }}
                                            <label class="switch DeviceOdo">
                                                {{ Form::checkbox('customCheckbox', 'yes', $refData['defaultMileage'] == 'no' ? false : true, ['class' => 'col-6 customCheckbox']) }}
                                                <span class="slider"></span>
                                                {{ Form::hidden('defaultMileage', $refData['defaultMileage'], ['class' => 'form-control hidden','id'=>'defaultMileage']) }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="d-flex flex-column">
                                            {{ Form::label('assetTracker', __('messages.AssetTracker')) }}
                                            <label class="switch">
                                                {{ Form::checkbox('customCheckbox', 'yes', $refData['assetTracker'] == 'no' ? false : true, ['class' => 'col-6 customCheckbox']) }}
                                                <span class="slider"></span>
                                                {{ Form::hidden('assetTracker', $refData['assetTracker'], ['class' => 'form-control hidden']) }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="d-flex flex-column">
                                            {{ Form::label('safetyParking', __('messages.SafetyParking')) }}
                                            <label class="switch">
                                                {{ Form::checkbox('customCheckbox', 'yes', $refData['safetyParking'] == 'no' ? false : true, ['class' => 'col-6 customCheckbox']) }}
                                                <span class="slider"></span>
                                                {{ Form::hidden('safetyParking', $refData['safetyParking'], ['class' => 'form-control hidden']) }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="d-flex flex-column">
                                            {{ Form::label('RegMode', __('messages.RigMode')) }}
                                            <label class="switch rigMode">
                                                {{ Form::checkbox('customCheckbox', 'Enable', $refData['rigMode'] == 'Enable', ['class' => 'col-6 customCheckbox','option'=>'Disable']) }}
                                                <span class="slider"></span>
                                                {{ Form::hidden('rigMode', $refData['rigMode'], ['class' => 'form-control hidden', 'id' => 'rigMode']) }}
                                            </label>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="row m-0">
                                <div class="col-md-3 col-12">
                                            <div class="d-flex flex-column">
                                                {{ Form::label('isAc', __('messages.Ac')) }}
                                                <label class="switch">
                                                    {{ Form::checkbox('isAc', 'rfid', $refData['isAc'] == 'no' ? false : true, ['class' => 'col-6 customCheckbox']) }}
                                                    <span class="slider"></span>
                                                    {{ Form::hidden('isAc', $refData['isAc'], ['class' => 'form-control hidden']) }}
                                                </label>
                                            </div>
                                        </div>
                                    @if ((session()->get('cur1') == 'prePaidAdmin' && ($refData['Licence'] == 'Premium' || $refData['Licence'] == 'PremiumPlus')) || session()->get('cur1') != 'prePaidAdmin')
                                    <div class="col-md-3 col-12">
                                            <div class="d-flex flex-column">
                                                {{ Form::label('isRF', __('messages.RFID')) }}
                                                <label class="switch">
                                                    {{ Form::checkbox('customCheckbox', 'rfid', $refData['serial1'] == 'no' ? false : true, ['class' => 'col-6 customCheckbox']) }}
                                                    <span class="slider"></span>
                                                    {{ Form::hidden('serial1', $refData['serial1'], ['class' => 'form-control hidden']) }}
                                                </label>
                                            </div>
                                        </div>
                                        @endif
                                        @if ((session()->get('cur1') == 'prePaidAdmin' && ($refData['Licence'] == 'Advance' ||$refData['Licence'] == 'Premium' || $refData['Licence'] == 'PremiumPlus')) || session()->get('cur1') != 'prePaidAdmin')
                                    <div class="col-md-3 col-12">
                                            <div class="d-flex flex-column">
                                                {{ Form::label('isRF', __('messages.Fuel')) }}
                                                <label class="switch">
                                                    {{ Form::checkbox('customCheckbox', 'fuel', $refData['analog1'] == 'no' ? false : true, ['class' => 'col-6 customCheckbox']) }}
                                                    <span class="slider"></span>
                                                    {{ Form::hidden('analog1', $refData['analog1'], ['class' => 'form-control hidden']) }}
                                                </label>
                                            </div>
                                        </div>
                                        @endif
                                        @if ((session()->get('cur1') == 'prePaidAdmin' && ($refData['Licence'] == 'Premium' || $refData['Licence'] == 'PremiumPlus')) || session()->get('cur1') != 'prePaidAdmin')
                                    <div class="col-md-3 col-12">
                                            <div class="d-flex flex-column">
                                                {{ Form::label('isRF', __('messages.Temperature')) }}
                                                <label class="switch">
                                                    {{ Form::checkbox('customCheckbox', 'temperature', $refData['serial2'] == 'no' ? false : true, ['class' => 'col-6 customCheckbox']) }}
                                                    <span class="slider"></span>
                                                    {{ Form::hidden('serial2', $refData['serial2'], ['class' => 'form-control hidden']) }}
                                                </label>
                                            </div>
                                        </div>
                                        @endif
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('ac', __('messages.SecondaryEngine')." (AC)") }}
                                {{ Form::select('ac', $acList, $ac, ['class' => 'form-control', 'id' => 'acvalue']) }}
                            </div>

                            <div class="form-group col-md-6" id="acvolt1">
                                {{ Form::label('acVolt', __('messages.Voltage')) }}
                                {{ Form::number('acVolt', $acVolt, ['class' => 'form-control', 'min' => '1', 'step' => '0.01', 'id' => 'acVoltvalue']) }}
                            </div>
                            <div class="col-md-6 form-group" id="secondaryEngineHours">
                                {{ Form::label('SecondaryEngineHours', __('messages.SecondaryEngineHours')) }}
                                {{ Form::text('secondaryEngineHours', $refData['secondaryEngineHours'], array('class' => 'form-control secondaryEngineHours') ) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('fuelmode', __('messages.EngineON')) }}
                                {{ Form::select('VolIg', $VolIgList, $vol, ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('batteryvolt', __('messages.VehicleBatteryVoltage')) }}
                                {{ Form::number('batteryvolt', $batteryvolt, ['class' => 'form-control', 'min' => '1', 'step' => '0.1']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('odoDistance', __('messages.OdometerReading')) }}
                                {{ Form::text('odoDistance', $refData['odoDistance'], ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('overSpeedLimit', __('messages.OverSpeedLimit')) }}
                                {{ Form::number('overSpeedLimit', $refData['overSpeedLimit'], ['class' => 'form-control', 'min' => '1']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('expectedMileage', __('messages.ExpectedMileage')) }}
                                {{ Form::text('expectedMileage', $refData['expectedMileage'], ['class' => 'form-control', 'placeholder' => 'Expected Mileage Value']) }}
                            </div>
                            <div class="form-group col-md-6 DistManipulationPerDiv">
                                {{ Form::label('DistManipulationPer', __('messages.DistanceManipulation(%)')) }}
                                {{ Form::number('DistManipulationPer', $refData['DistManipulationPer'], ['class' => 'form-control', 'placeholder' => 'Distance Manipulation Percentage', 'step' => '0.001']) }}
                            </div>
                            @if ((session()->get('cur1') == 'prePaidAdmin' && $refData['Licence'] != 'Basic') || session()->get('cur1') != 'prePaidAdmin')
                                <div class="form-group col-md-6">
                                    {{ Form::label('mintem', __('messages.MinimumTemperature')) }}
                                    {{ Form::number('mintemp', $refData['mintemp'], ['class' => 'form-control', 'placeholder' => 'Quantity', 'min' => '-100']) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('maxtem', __('messages.MaximumTemperature')) }}
                                    {{ Form::number('maxtemp', $refData['maxtemp'], ['class' => 'form-control', 'placeholder' => 'Quantity', 'min' => '-100']) }}
                                </div>
                            @endif
                            <div class="form-group col-md-6">
                                {{ Form::label('enable', __('messages.Enabledebugs')) }}
                                {{ Form::select('enable', $enableDebugList, $enable, ['class' => 'form-control', 'id' => 'enable', 'required' => 'required']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('countryTimezone', __('messages.CountryTimezone'), ['style' => 'margin-bottom:11px;']) }}
                                {{ Form::select('countryTimezone', $countryTimezoneList, $refData['countryTimezone'], ['class' => 'selectpicker form-control', 'data-live-search ' => 'true', 'style' => 'width:100%;']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('eveningTripStartTime', __('messages.Timezone')) }}
                                {{ Form::select('eveningTripStartTime', $timezoneList, $refData['eveningTripStartTime'], ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('ipAddress', __('messages.IPAddress')) }}
                                {{ Form::text('ipAddress', $refData['ipAddress'], ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('communicatingPortNo', __('messages.CommunicatingPortNo')) }}
                                {{ Form::text('communicatingPortNo', $refData['communicatingPortNo'], ['class' => 'form-control', 'readonly' => 'true']) }}
                            </div>
                            @if ((session()->get('cur1') == 'prePaidAdmin' && ($refData['Licence'] == 'Premium' || $refData['Licence'] == 'PremiumPlus')) || session()->get('cur1') != 'prePaidAdmin')
                                <div class="form-group col-md-6">
                                    {{ Form::label('rfidType', __('messages.RfidType')) }}
                                    {{ Form::select('rfidType', $rfidTypeList, isset($refData['rfidType']) ? $refData['rfidType'] : 'no', ['class' => 'form-control']) }}
                                </div>
                            @endif                            
                    
                        </div>
                        <br>
                        <h3 class="text-primary" id="v4">
                            @lang('messages.DriverDetails')
                        </h3>
                        <hr>
                        <div class="row p-2">
                            <div class="form-group col-md-6">
                                {{ Form::label('driverName', __('messages.DriverName')) }}
                                {{ Form::text('driverName', $refData['driverName'], ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('driverMobile', __('messages.DriverMobileNo')) }}
                                {{ Form::number('driverMobile', $refData['driverMobile'], ['class' => 'form-control']) }}
                            </div>
                        </div><br>
                        <h3 class="text-primary" id="v5">
                            @lang('messages.OtherDetails')
                        </h3>
                        <hr>
                        <div class="row" style="padding: 10px 20px">
                            <div class="form-group col-md-6">
                                {{ Form::label('madeIn', __('messages.MadeIn')) }}
                                {{ Form::text('madeIn', $refData['madeIn'], ['class' => 'form-control', 'min' => '1']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('manufacturingDate', __('messages.Manufacturingdate')) }}
                                <script>
                                    $(function() {
                                        $("#calendardate").datepicker({
                                            format: 'yy-mm-dd',
                                            endDate:'now',
                                        });
                                    });
                                </script>
                                {{ Form::text('manufacturingDate', $refData['manufacturingDate'], ['id' => 'calendardate', 'class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('chassisNumber', __('messages.ChassisNumber')) }}
                                {{ Form::text('chassisNumber', $refData['chassisNumber'], ['class' => 'form-control', 'min' => '1']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('gpsSimICCID', __('messages.GPSSimICCID')) }}
                                {{ Form::number('gpsSimICCID', $refData['gpsSimICCID'], ['class' => 'form-control', 'maxlength' => 20, 'minlength' => 10]) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('altShort', __('messages.AlternateVehicleName')) }}
                                {{ Form::text('altShortName', $refData['altShortName'], ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('descriptionStatus', __('messages.Description/Remarks')) }}
                                {{ Form::text('descriptionStatus', $refData['descriptionStatus'], ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="row my-3">
                            <div class="col-md-12 text-center">
                                <a class="btn btn-danger" href="{{ URL::to('VdmVehicleScan' . $vehicleId) }}" style="">
                                    @lang('messages.Back')
                                </a>
                                {{ Form::submit(__('messages.Update'), ['id' => 'sub', 'class' => 'btn btn-primary ']) }}
                                @if (($refData['Licence'] !== 'Basic' && $refData['Licence'] !== 'Starter' && $refData['Licence'] !== 'StarterPlus') || session()->get('cur1') !== 'prePaidAdmin')
                                    <a class="btn btn-secondary"
                                        href="{{ route('calibrate', ['vid' => $vehicleId, 'count' => '1']) }}">
                                        @lang('messages.FuelCalibration')
                                    </a>
                                @endif
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <button class="scroll" id="upbtn" title="Go to top" class="btn btn-sm rounded-circle btn-grey">
                <i class="fas fa-angle-up 2x"></i>
            </button>
            <button class="scroll" id="downbtn" title="Go to down" class="btn btn-sm rounded-circle btn-grey">
                <i class="fas fa-angle-down 2x"></i>
            </button>
        </div>
    </div>
    @if ((session()->get('cur1') == 'prePaidAdmin' && $refData['Licence'] != 'Basic') || session()->get('cur1') != 'prePaidAdmin')
        <script type="text/javascript">
            $(document).ready(function() {
                var datas = {
                    'val1': $('#enable').val(),
                    'val2': $('#noOfTank').val(),
                    'val3': $('#sensorcount').val()
                }
                $("#showtanksize,#fuelType,#vehicleMode").hide();
                if (datas.val2 == '1') {
                    $("#showtanksize,#fuelType,#vehicleMode").show();
                }
            })
        </script>
    @endif
@endsection
@push('css')
    <style>
        .scroll {
            position: fixed;
            bottom: 20px;
            right: 30px;
            z-index: 99;
            font-size: 18px;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 1px 10px;
            border-radius: 25px;
        }

        #upbtn {
            bottom: 55px;
        }

        .scroll:hover {
            background-color: rgb(54, 97, 238);
            color: whitesmoke;
        }

    </style>
@endpush
@push('js')
    <script type="text/javascript">
        // for scroller
        let pageHeight = $(document).height();
        let divid = 5;
        let moveing = pageHeight / divid;
        let scrollPosition = pageHeight / divid;
        const startScroll = 200;
        const endScroll = pageHeight - startScroll;
        const lastScroll = pageHeight - 1000;
        const scrollTimeing = 700;

        function scroller() {
            if ($(this).prop('id') == 'upbtn') {
                scrollPosition = scrollPosition - moveing;
                if (scrollPosition <= startScroll) scrollPosition = 0;
            } else {
                scrollPosition = scrollPosition + moveing;
                if (scrollPosition >= endScroll) scrollPosition = endScroll;
            }
            $('html, body').animate({
                scrollTop: scrollPosition - 100
            }, scrollTimeing);
        }

        $("#upbtn,#downbtn").on('click', scroller);

        $(window).scroll(function() {
            $("#downbtn,#upbtn").show()
            scrollPosition = document.documentElement.scrollTop;
            if (document.documentElement.scrollTop <= startScroll) {
                $('#upbtn').hide();
            }
            if (document.documentElement.scrollTop >= lastScroll) {
                $("#downbtn").hide();
            }
        })
        // end scroller
        // checkbox css
        $('.customCheckbox').click(function() {
            let option = $(this).attr('option') ?? 'no';
            let value = $(this).is(':checked') ? $(this).val() : option;
            $(this).parent('.switch').children('.hidden').val(value);
        });
        // end checkbox

        $('#sub').on('click', function() {
            var datas = {
                'val': $('#tanksize').val(),
                'val1': $('#analog').val(),
                'val2': $('#acvalue').val()
            }
            if (datas.val != '0' && datas.val != '') {
                $("analog").val('fuel');
            } else if (datas.val2 == 'analog1') {
                $("acVoltvalue").prop('required', "true");
            } else if (datas.val2 == 'digital1') {
                $("acVoltvalue").prop('required', "false");
                $("#acvolt1").hide();
            }
        });
        
        $(".DeviceOdo").on("click",DistManipulationPerDiv)
        DistManipulationPerDiv()
        function DistManipulationPerDiv() {
            $(".DistManipulationPerDiv").hide()
            if($("#defaultMileage").val() !== 'yes'){
                $(".DistManipulationPerDiv").show()
            }
        }



        $('#analog').on('change', function() {
            var input = {
                'val': $('#analog').val()
            }
            if (input.val == 'no' || input.val == 'load') {
                $("tanksize").attr('readOnly', true);
                $("tanksize").val('');
            } else {
                $("tanksize").attr('readOnly', false);
                $("tanksize").val('');
            }

        });
        $('#acvalue').on('change', function() {
            var datas = {
                'val': $('#acvalue').val()
            }
            if (datas.val == 'analog1') {

                $("acVoltvalue").prop('required', true);
                $("#acvolt1").show();
            } else if (datas.val == 'digital1') {
                $("acVoltvalue").prop('required', false);
                $("#acvolt1").hide();

            }
        });

        $(document).ready(function() {

            var datas = {
                'val': $('#acvalue').val(),
                'val1': $('#enable').val(),
                'val2': $('#noOfTank').val(),
                'val3': $('#sensorcount').val()
            }
            $("#acvolt1").hide();
            if(datas.val !== 'digital1'){
                $("#acvolt1").show();
            }

            $('.switch.rigMode').on('click',showSecondaryEngineHours)
            showSecondaryEngineHours()
            function showSecondaryEngineHours(){
                $('#secondaryEngineHours').hide()
                if($('#rigMode').val() === 'Enable'){
                    $('#secondaryEngineHours').show()
                }
            }

            // $("#vehicleModel,#oprName,input[name='rigMode']").trigger('change')
            $("#vehicleModel").on('change',vehicleModel)
            vehicleModel();
            function vehicleModel(){
                $("#otherVehicleModel").hide();
                $("#otherVehicleModel>input").removeAttr('required');
                if( $("#vehicleModel").val().toLowerCase() == "other"){
                    $("#otherVehicleModel>input").attr({"required":"required"});
                    $("#otherVehicleModel").show();
                }
            }

            $("#oprName").on('change',otherTeleComProvider)
            otherTeleComProvider();
            function otherTeleComProvider() {
                $("#otherTeleComProvider").hide();
                $("#otherTeleComProvider>input").removeAttr('required');
                if($('#oprName').val().toLowerCase() == "other"){
                    $("#otherTeleComProvider>input").attr({"required":"required"});
                    $("#otherTeleComProvider").show();
                } 
            }
        })
    </script>
@endpush
