@extends("includes.adminheader")
@section('mainContent')
    <div id="wrapper">
        <div class="content page-inner p-0">
           
            <div class="page-header d-flex">
                <div class="page-title">
                    @lang('messages.EditReport')
                </div>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('vdmFranchises.index') }}">Franchise</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                            @lang('messages.EditReport')
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">{{ $fcode }}</a>
                    </li>
                </ul>
            </div>
            <div class="page-body">
                {{ Form::open(['url' => 'vdmFranchises/updateReports']) }}
                <div class="card">
					{{ Form::hidden('fcode', $fcode) }}
                    <div>
                        <button class="btn btn-outline-success btn-sm float mt-3" type="button" id="select" >
                            @lang('messages.SelectAll')
                        </button>
                        <button class="btn btn-outline-danger btn-sm float mt-3" type="button" id="unselect">
                            @lang('messages.UnselectAll')
                        </button>
                        {{ Form::submit("Update User", ['class' => 'btn btn-primary btn-sm float']) }}
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach ($totalList as $keys => $reportName)
                                <br>
                                <div class="col-md-12">
                                    <div class="d-flex">
                                        <h3 class="text-primary">
                                            {{ $keys }}
                                        </h3>
                                        <button type="button" class="btn btn-primary btn-sm ml-auto"
                                            onclick="toggle('{{ $keys }}',true)">
                                            <span class="fa fa-check"></span>
                                        </button>
                                        <button type="button" class="btn btn-danger btn-sm ml-3"
                                            onclick="toggle('{{ $keys }}',false)">
                                            <span class="fa fa-times"></span>
                                        </button>
                                    </div>
                                    <hr>
                                    <div class="row p-2">
                                        @if ($reportName !=null)
                                        @foreach ($reportName as $value)
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        {{ Form::checkbox('reportName[]', $value, in_array($value, $userReports), ['class' => "$keys checkbox form-check-input"]) }}
                                                        <span class="form-check-sign">{{  head(explode(":",$value))}}</span>
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                        @endif
                                    </div>
                                    <br />
                                </div>

                            @endforeach
                        </div>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
    <style>
        .float {
            position: fixed;
            width: 115px;
            height: 40px;
            bottom: 60px;
            right: 50px;
            border-radius: 50px;
            text-align: center;
            z-index: 9999;
            box-shadow: 2px 2px 3px #999;
            background: #fff;
        }

        #select {
            margin-right: 250px !important;
        }

        #unselect {
            margin-right: 125px !important;
        }

    </style>
    <script>
        function toggle(val, fn) {
            var className = '.' + val;
            $(className).prop('checked', fn);
        }
        $('#select').click(function(){toggle('checkbox',true)})
        $('#unselect').click(function(){toggle('checkbox',false)})
    </script>
@endsection
