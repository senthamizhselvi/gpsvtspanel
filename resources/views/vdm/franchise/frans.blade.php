<div>

    <!-- Modal -->
    <div class="modal fade" id="myModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-center  modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLongTitle">
                        Select the Franchise name
                    </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {{ Form::open(['url' => 'vdmFranchises/findFransList', 'id' => 'fransub']) }}
                <div class="modal-body">
                    <div class="form-group">
                        {{ Form::label('frans', 'Normal Franchise') }}
                        {{ Form::select('frans', [], null, ['id' => 'franList', 'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('prePaidFrans', 'Prepaid Franchises') }}
                        {{ Form::select('prePaidFrans', [], null, ['id' => 'franList1', 'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true']) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-primary mx-auto" type="submit" value="Submit" id="onsubmit">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>



    <!-- if there are creation errors, they will show here -->

    <script type="text/javascript">
        $(document).ready(function() {
            $('#switchLogin').one('click',function() {
                $('#fransub .form-group').hide();
                $('#fransub .modal-body').addClass('is-loading is-loading-lg py-5');
                $.get("{{ URL::to('vdmFranchises/fransearch') }}", function(data) {
                    let {
                        fransId,
                        prePaidFransId
                    } = data;
                    $('#fransub .modal-body').removeClass('is-loading is-loading-lg  py-5');
                    $('#fransub .form-group').fadeIn();
                    appandOption('#franList', fransId);
                    appandOption('#franList1', prePaidFransId);
                    $('.selectpicker').css('width', '100%');
                    $('.selectpicker').select2({
                        dropdownParent: $('#myModel .modal-content')
                    });
                    $('span.selection>.select2-selection.select2-selection--single').addClass(
                        'form-control py-select-1');
                    $('.select2-selection.select2-selection--single>.select2-selection__arrow').attr('style',
                        "top:10px !important;right:10px !important");
                });
            });

            function appandOption(id, list) {
                if (list.length == 0) return false;
                for (const key in list) {
                    $(id).append(`<option value='${key}'>${list[key]}</option>`)
                }
            }
            $('#fransub').on('submit', function() {
                var data1 = {
                    'val1': $('#franList').val(),
                    'val2': $('#franList1').val()
                };
                if (data1.val1 != '' && data1.val2 != '') {
                    wornigAlert("Please select anyone type of franchise");
                    return false;
                } else if (data1.val1 == '' && data1.val2 == '') {
                    wornigAlert("Please select franchise");
                    return false;
                }
            });
        });
    </script>
</div>
