@extends('includes.adminheader')
@section('mainContent')

    <div class="page-header d-flex">
        <div class="page-title">
            Show Franchise
        </div>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="#">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="{{ route('vdmFranchises.index') }}">Franchise</a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="#">View </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="#">{{ $fcode }}</a>
            </li>
        </ul>
    </div>
    <div class="page-body">
        <div class="card">
            <div class="card-body">
                <div class="row p-2">
                    <div class="col-md-4">
                        <div class="h3 text-primary b">
                            {{ $fname }}
                        </div>
                        <div class="b">
                            {{ $description }}
                        </div>
                    </div>
                </div>
                <div class="row p-2" style="font-size: 14px">
                    <div class="col-md-6">
                        <strong class="h3 text-primary b ">Franchise Details </strong>
                        <hr>
                        <p>
                            <strong>User Id : </strong>{{ $userId }}<br>
                            <strong>FCODE : </strong>{{ $fcode }}<br>
                            <strong>Website : </strong>{{ $websites }}<br>
                            <strong>Track Page : </strong>{{ $trackPage }}
                        </p>
                        <br>
                    </div>
                    
                    <div class="col-md-6">
                        <strong class="text-primary b h3">DataBase Details </strong>
                        <hr>
                        <p>
                            <strong>DB IP : </strong>{{ $dbIp }}<br>
                            <strong> DB Type :</strong>{{ $dbType }}<br>
                            <strong>Backup Days : </strong>{{ $backUpDays }}
                        </p>
                        <br>
                    </div>
                    <div class="col-md-6"> 
                        <strong class="text-primary b h3">Licence Details </strong>
                        <hr>
                        <p>
                            @if ($prepaid != 'yes')
                                <strong>Number of Licence : </strong>{{ $numberofLicence }}<br>
                                <strong>Available Licence : </strong>{{ $availableLincence }}
                            @else
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <td><strong>Type</strong></td>
                                            <td><strong>Number of Licence</strong></td>
                                            <td><strong>Available</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody id="myTable">
                                        <tr>
                                            <td>Starte Plus</td>
                                            <td>{{ $numberofStarterPlusLicence }}</td>
                                            <td>{{ $availableStarterPlusLicence }}</td>
                                        </tr>
                                        <tr>
                                            <td>Starter</td>
                                            <td>{{ $numberofStarterLicence }}</td>
                                            <td>{{ $availableStarterLicence }}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>Basic</td>
                                            <td>{{ $numberofBasicLicence }}</td>
                                            <td>{{ $availableBasicLicence }}</td>
                                        </tr>
                                        <tr>
                                            <td>Advance</td>
                                            <td>{{ $numberofAdvanceLicence }}</td>
                                            <td>{{ $availableAdvanceLicence }}</td>
                                        </tr>
                                        <tr>
                                            <td>Premium</td>
                                            <td>{{ $numberofPremiumLicence }}</td>
                                            <td>{{ $availablePremiumLicence }}</td>
                                        </tr>
                                        <tr>
                                            <td>Premium Plus</td>
                                            <td>{{ $numberofPremPlusLicence }}</td>
                                            <td>{{ $availablePremPlusLicence }}</td>
                                        </tr>
                                    </tbody>
                                </table>

                            @endif
                        </p>
                        <br>
                    </div>
                    <div class="col-md-6"> 
                        <strong class="text-primary b h3">Other Details </strong>
                        <hr>
                        <div class="">
                            <strong> MAP API Key : </strong>{{ $apiKeys }}<br><br>
                            <strong>Address Key : </strong>{{ $addressKey }}<br><br>
                            <strong>Notification Key : </strong>{{ $notificationKey }}<br><br>
                            <strong>Mobile App Package Name : </strong>{{ $gpsvtsAppKey }}<br><br>
                            @if ($zoho !== "")
                                 <strong> ZOHO : </strong>{{ $zoho }}<br><br>
                            @endif
                            @if ($auth !== "")
                                <strong>Auth : </strong>{{ $auth }}<br><br>
                            @endif
                            <strong>Mobile No : </strong> {{ $mobileNo1 . ', ' . $mobileNo2 }}<br><br>
                            <strong>Email : </strong> {{ $email1 . ', ' . $email2 }}<br><br>
                            <strong>Address : </strong> {{ $fullAddress }}<br><br>
                            <strong>Landline.:</strong> {{ $landline }}<br>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
