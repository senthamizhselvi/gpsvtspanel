<!DOCTYPE html>
@include('vdm.franchise.licenceList_index')
<html>
<head>
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>GPS Admin</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="../vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="../vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="../vendor/bootstrap/dist/css/bootstrap.css" />

    <!-- search box -->
    <!-- <link href="vendor/dropDown/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="../assets/dropDown/css/bootstrap-select.min.css">

    <!-- App styles -->
    <link rel="stylesheet" href="../fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="../fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="../styles/style.css">
    <link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="../plugins/datatables/jquery.dataTables.min.css">
    <!-- validation -->
</head>

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Welcome To GPS Admin</h1> </div> </div>

<!-- Header -->
<div >

{{ Form::close() }} 


