@include('vdm.franchise.header_create')
<div id="wrapper" style='margin:0;'>
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-14">
                <div class="hpanel">
                    <div class="panel-heading">
                    </div>

                    <div class="panel-body" style="margin-top: -2%; ">
                        <div class="row well" style="background: #14cfed6b; padding: 0%;">
                            <div class="col-lg-3">
                                <h5 style="padding: 3%;">Basic Licence Count:{{ $noOfBasicLicence }}</h5>
                            </div>
                            <div class="col-lg-3">
                                <h5 style="padding: 3%;">Advance Licence Count:{{ $noOfAdvanceLicence }}</h5>
                            </div>
                            <div class="col-lg-3">
                                <h5 style="padding: 3%;">Premium Licence Count:{{ $numOfPremiumLicence }}</h5>
                            </div>
                            <div class="col-lg-3">
                                <h5 style="padding: 3%;">PremiumPlus Licence Count:{{ $numOfPremPlusLicence }}</h5>
                            </div>


                        </div>
                        <hr>
                        <div class="row well" style="background: #14cfed6b; padding: 0%;">
                            <div class="col-lg-3">
                                <h5 style="padding: 3%;">Basic Available Licence:{{ $availableBasicLicence }}</h5>
                            </div>
                            <div class="col-lg-3">
                                <h5 style="padding: 3%;">Advance Available Licence:{{ $availableAdvanceLicence }}</h5>
                            </div>
                            <div class="col-lg-3">
                                <h5 style="padding: 3%;">Premium Available Licence:{{ $availablePremiumLicence }}</h5>
                            </div>
                            <div class="col-lg-3">
                                <h5 style="padding: 3%;">PremiumPlus Available Licence:{{ $availablePremPlusLicence }}
                                </h5>
                            </div>


                        </div>
                        <hr>
                        <table id="example1" class="table table-bordered dataTable">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">S.No</th>
                                    <th style="text-align: center;">Licence Id</th>
                                    <th style="text-align: center;">Licence Type</th>
                                    <th style="text-align: center;">Vehicle Id</th>
                                    <th style="text-align: center;">Device Id</th>
                                    <th style="text-align: center;">Licence Issued Date</th>
                                    <th style="text-align: center;">Licence Onboard Date</th>
                                    <th style="text-align: center;">Licence Expire Date</th>
                                    <th style="text-align: center;">Status</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @if ($LicenceList != null)
                                    @foreach ($LicenceList as $key => $value)
                                        @if (in_array($value, $ExpiredLicenceList))
                                            <tr style="text-align: center; color: #ff2f00;">
                                            @else
                                            <tr style="text-align: center; ">
                                        @endif
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $value }}</td>
                                        <td>{{ Arr::get($LtypeList, $value) }}</td>
                                        <td>{{ Arr::get($vehicleList, $value) }}</td>
                                        <td>{{ Arr::get($deviceList, $value) }}</td>
                                        <td>{{ Arr::get($licenceissuedList, $value) }}</td>
                                        <td>{{ Arr::get($LicenceOnboardList, $value) }}</td>
                                        <td>{{ Arr::get($LicenceExpiryList, $value) }}</td>
                                        @if ($renewedLicenceList != null)
                                            @if (in_array($value, $renewedLicenceList))
                                                <td>
                                                    <h6><b style="color: #ff2f00;">Renew</b></h6>
                                                </td>
                                            @else
                                                <td>
                                                    <h6><b style="color: #00a1ff;">Onboard</b></h6>
                                                </td>
                                            @endif
                                        @else
                                            <td>
                                                <h6><b style="color: #00a1ff;">Onboard</b></h6>
                                            </td>
                                        @endif
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
            <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.min.js" type="text/javascript"></script>

            <script type="text/javascript">
                $(document).ready(function() {
                    $("#bala1").addClass("active");

                });
            </script>
            </body>

            </html>
            @include('vdm.franchise.js_create') --}}
