@extends('includes.adminheader')
@section('mainContent')

    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-lg-9">
                    <h1>Franchises Management</h1>
                </div>
                <div class="col-lg-3">
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id='example1'>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Franchise Code</th>
                            <th>Franchise Name</th>
                            <th>Franchise Type</th>
                            <th>Actions</th>
    
                        </tr>
                    </thead>
                    <tbody id="myTable">
                        @foreach ($franArray as $value => $value1)
                            <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{ $value1['fcode'] }}</td>
                                @if (is_array($disable) && array_key_exists($value1['fcode'], $disable))
                                    <td style="color: red;">{{ $value1['fname'] }}</td>
                                @else
                                    <td>{{ $value1['fname'] }}</td>
                                @endif
                                <td>{{ $value1['prepaid'] }}</td>
    
                                <td>
    
                                    {{ Form::open(['url' => 'vdmFranchises/' . $value, 'class' => 'pull-right', 'id' => 'deleteForm' . $index]) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::button('Remove Franchise', ['class' => 'btn btn-danger btn-sm deleteBtn', 'id' => $index, 'formType' => 'Franchise']) }}
                                    {{ Form::close() }}
    
                                    <a class="btn btn-sm btn-success" href="{{ URL::to('vdmFranchises/' . $value) }}">Show
                                        Franchise</a>
    
                                    <a class="btn btn-sm btn-info text-white"
                                        href="{{ URL::to('vdmFranchises/' . $value . '/edit') }}">Amend
                                        Franchise</a>
    
                                    <a class="btn btn-sm btn-success"
                                        href="{{ URL::to('vdmFranchises/reports/' . $value) }}">Edit
                                        Reports</a>
                                    @if (is_array($disable) && array_key_exists($value1['fcode'], $disable))
                                        <a class="btn btn-sm btn-success"
                                            href="{{ URL::to('vdmFranchises/Enable/' . $value) }}">Enable</a>
                                    @else
                                        <a class="btn btn-sm btn-warning"
                                            href="{{ URL::to('vdmFranchises/disable/' . $value) }}">Disable</a>
                                    @endif
    
    
                                    @if ($value1['enableExpiry'] == 'yes')
                                        <a class="btn btn-sm btn-success"
                                            href="{{ URL::to('vdmFranchises/EnableVehicleExpiry/' . $value . '/yes') }}">Enable
                                            Vehicle
                                            Expiry</a>
                                    @else
                                        <a class="btn btn-sm btn-warning"
                                            href="{{ URL::to('vdmFranchises/EnableVehicleExpiry/' . $value . '/no') }}">Disable
                                            Vehicle
                                            Expiry</a>
                                    @endif
                                    {{-- @if ($value1['prepaid'] == 'Prepaid')
                                        <a class="btn btn-sm btn-success"
                                            href="{{ URL::to('licencelist/' . $value) }}">Licence
                                            List</a>
                                    @endif --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop