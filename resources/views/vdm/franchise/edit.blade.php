@extends('includes.adminheader')
@section('mainContent')
    <font color="red">{{ HTML::ul($errors->all()) }}</font>
    <div class="page-header d-flex">
        <div class="page-title">
            Edit Franchise
        </div>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="#">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="{{ route('vdmFranchises.index') }}">Franchise</a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="#">Edit </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="#">{{ $fcode }}</a>
            </li>
        </ul>
    </div>

    <!-- if there are creation errors, they will show here -->

    <div class="card">

        <div class="card-body">

            {{ Form::model($fcode, ['route' => ['vdmFranchises.update', $fcode], 'method' => 'PUT','id'=>'editForm']) }}
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('description', 'Franchise Name') }}
                        {{ Form::label('description', $fname, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('description', 'Franchise Code') }}
                        {{ Form::label('fcode', $fcode, ['class' => 'form-control']) }}
                    </div>


                    <div class="form-group">
                        {{ Form::label('description', 'Description') }}
                        {{ Form::text('description', $description, ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('fullAddress', 'Full Address') }}
                        {{ Form::text('fullAddress', $fullAddress, ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('landline', 'Landline Number') }}
                        {{ Form::text('landline', $landline, ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('mobileNo1', 'Mobile Number 1') }}
                        {{ Form::text('mobileNo1', $mobileNo1, ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('mobileNo2', 'Mobile Number 2') }}
                        {{ Form::text('mobileNo2', $mobileNo2, ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('email1', 'Email 1') }}
                        {{ Form::text('email1', $email1, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('email2', 'Email 2') }}
                        {{ Form::text('email2', $email2, ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('userId', 'User Id(login)') }}
                        <br />
                        {{ Form::label('userId', $userId, ['class' => 'form-control']) }}

                    </div>

                    <div class="form-group">
                        {{ Form::label('otherDetails', 'Other Details') }}
                        {{ Form::text('otherDetails', $otherDetails, ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('languageCode', 'Country Language') }}
                        {{ Form::select('languageCode', ['ta' => 'Tamil', 'hi' => 'Hindi', 'en' => 'English', 'es' => 'Spanish'], $languageCode, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('defaultMap', __('messages.DefaultMap')) }}
                        {{ Form::select('defaultMap',$defaultMapList, $defaultMap, ['class' => 'form-control']) }}
                    </div>   
                    <div class="form-group">
                        {{ Form::label('website', 'Website ') }}
                        {{ Form::text('website', $website, ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('apiKey', 'Map Key / Api Key') }}
                        {{ Form::text('apiKey', $apiKey, ['id' => 'mapkey', 'class' => 'form-control', 'placeholder' => 'Map Key']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('website2', 'Website 2') }}
                        {{ Form::text('website2', $website2, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('apiKey2', 'Map Key / Api Key 2') }}
                        {{ Form::text('apiKey2', $apiKey2, ['id' => 'mapkey', 'class' => 'form-control', 'placeholder' => 'Map Key']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('website3', 'Website 3') }}
                        {{ Form::text('website3', $website3, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('apiKey3', 'Map Key / Api Key 3') }}
                        {{ Form::text('apiKey3', $apiKey3, ['id' => 'mapkey', 'class' => 'form-control', 'placeholder' => 'Map Key']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('website4', 'Website 4') }}
                        {{ Form::text('website4', $website4, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('apiKey4', 'Map Key / Api Key 4') }}
                        {{ Form::text('apiKey4', $apiKey4, ['id' => 'mapkey', 'class' => 'form-control', 'placeholder' => 'Map Key']) }}
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('subDomain', 'SubDomain Name') }}
                        {{ Form::text('subDomain', $subDomain, ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('type', 'Track Page') }}<br>
                        <div class="custom-control custom-radio">
                            {{ Form::radio('trackPage', 'Normal View', $trackPage == 'Normal View', ['class' => 'custom-control-input', 'id' => 'NormalView']) }}
                            <label class="custom-control-label" for="NormalView"> Normal View</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {{ Form::radio('trackPage', 'Customized View', $trackPage == 'Customized View', ['class' => 'custom-control-input', 'id' => 'CustomizedView']) }}
                            <label class="custom-control-label" for="CustomizedView">Customized View</label>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('smsSender', 'SMS Sender ') }}
                        {{ Form::text('smsSender', $smsSender, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('smsProvider', 'SMS Provider') }}
                        {{ Form::select('smsProvider', $smsP, $smsProvider, ['class' => 'form-control selectpicker', 'id' => 'smsProvider']) }}
                    </div>
                    <div class=" form-group" id='textLocalSmsKeyFiled'>
                        {{ Form::label('textLocalSmsKey', __('messages.TextLocalSmsKey')) }}<span
                            class="rq"></span>
                        {{ Form::text('textLocalSmsKey', $textLocalSmsKey, ['class' => 'form-control', 'placeholder' => __('messages.TextLocalSmsKey'), 'id' => 'textLocalSmsKey']) }}
                    </div>
                    <div class="form-group" id="smsEntityNameFiled">
                        {{ Form::label('smsEntityName', __('messages.SMSEntityName')) }} <span
                            class="rq"></span>
                        {{ Form::text('smsEntity', $smsEntity, ['class' => 'form-control', 'placeholder' => 'SMS Entity Name', 'id' => 'smsEntityName']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('providerUserName', 'SMS Provider User Name') }}
                        {{ Form::text('providerUserName', $providerUserName, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('providerPassword', 'SMS Provider Password') }}
                        {{ Form::text('providerPassword', $providerPassword, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('ipadd', 'DB IP') }}
                        {{ Form::select('ipadd', $dbIpAr, $dbIp, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('backUpDays', 'DB Backup Days') }}
                        {{ Form::number('backUpDays', $backUpDays, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'numbers', 'min' => '1']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('dbType', 'DB Type') }}
                        {{ Form::select('dbType', $backType, $dbType, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('prepaid', 'Prepaid Franchise') }}
                        {{ Form::select('prepaid1', ['no' => 'No', 'yes' => 'Yes'], $prepaid, ['class' => 'form-control', 'id' => 'prepaid', 'disabled' => 'true']) }}
                        {{ Form::input('hidden', 'prepaid', $prepaid, ['class' => 'form-control', 'id' => 'prepaid']) }}
                    </div>
                    <div class="form-group" id='enableStarterdiv'>
                        {{ Form::label('prepaid', 'Enable Starter Licence') }}
                        {{ Form::select($enableStarter == 'yes' ? 'enableStarter1' : 'enableStarter', ['no' => 'No', 'yes' => 'Yes'], $enableStarter, ['class' => 'form-control', 'id' => 'enableStarter', $enableStarter == 'yes' ? 'disabled' : '' => '']) }}
                        {{ Form::input('hidden', $enableStarter == 'yes' ? 'enableStarter' : 'enableStarter1', $enableStarter, ['class' => 'form-control', 'id' => 'prepaid']) }}
                    </div>
                    <div class="form-group" id='authenticatedEnable'>
                        {{ Form::label('authenticated', 'Authenticated User') }}
                        {{ Form::select($authenticated == 'yes' ? 'authenticated1' : 'authenticated', ['no' => 'No', 'yes' => 'Approved'], $authenticated, ['class' => 'form-control', 'id' => 'authenticated', $authenticated == 'yes' ? 'disabled' : '' => '']) }}
                        {{ Form::input('hidden', $authenticated == 'yes' ? 'authenticated' : 'authenticated1', $authenticated, ['class' => 'form-control', 'id' => 'authenticated1']) }}
                    </div>
                    <div id="numberofLicence">
                        <div class="form-group">
                            {{ Form::label('numberofLicence', 'Number of  Licence') }}
                            {{ Form::text('numberofLicence', $numberofLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                            {{ Form::hidden('numberofLicenceO', $numberofLicence, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('availableLincence', ' Available licence ') }}
                            {{ Form::text('availableLincence', $availableLincence, ['class' => 'form-control', 'readonly' => 'true']) }}
                            {{ Form::hidden('availableLincenceO', $availableLincence, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('addLicence', 'Add licence ') }}
                            {{ Form::text('addLicence', old('addLicence'), ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row p-2" id="typeCount">
                        
                       
                        <div class="col-md-12">
                            <div class="row text-center">
                                <div class="col-md-4">
                                    {{ Form::label('numberofBasicLicence', 'Number of Licence') }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('availableBasicLicence', 'Available Licence') }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('addBasicLicence', 'Add  Licence') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" id="StarterPlus">
                            <div class="row">
                                <div class="col-md-4">
                                    {{ Form::label('numberofStarterLicence', 'Starter Plus') }}
                                    {{ Form::number('numberofStarterPlusLicence', $numberofStarterPlusLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('availableStarterLicence', ' Starter Plus ') }}
                                    {{ Form::number('availableStarterPlusLicence', $availableStarterPlusLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('addStarterLicence', ' Starter Plus ') }}
                                    {{ Form::number('addStarterPlusLicence', Input::old('addStarterPlusLicence'), ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <br>
                        </div>
                        <div class="col-md-12" id="Starter">
                            <div class="row">
                                <div class="col-md-4">
                                    {{ Form::label('numberofStarterLicence', ' Starter ') }}
                                    {{ Form::number('numberofStarterLicence', $numberofStarterLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('availableStarterLicence', ' Starter ') }}
                                    {{ Form::number('availableStarterLicence', $availableStarterLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('addStarterLicence', ' Starter ') }}
                                    {{ Form::number('addStarterLicence', old('addStarterLicence'), ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <br />
                        </div>
                        
                        <div class="col-md-12 mt-2" id="Basic">
                            <div class="row">
                                <div class="col-md-4">
                                    {{ Form::label('numberofBasicLicence', ' Basic') }}
                                    {{ Form::number('numberofBasicLicence', $numberofBasicLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('availableBasicLicence', ' Basic ') }}
                                    {{ Form::number('availableBasicLicence', $availableBasicLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('addBasicLicence', ' Basic ') }}
                                    {{ Form::number('addBasicLicence', old('addBasicLicence'), ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <br />
                        </div>
                        <div class="col-md-12" id="Advance">
                            <div class="row">
                                <div class="col-md-4">
                                    {{ Form::label('numberofAdvanceLicence', 'Advance ') }}
                                    {{ Form::number('numberofAdvanceLicence', $numberofAdvanceLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('availableAdvanceLicence', ' Advance ') }}
                                    {{ Form::number('availableAdvanceLicence', $availableAdvanceLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('addAdvanceLicence', ' Advance ') }}
                                    {{ Form::number('addAdvanceLicence', old('addAdvanceLicence'), ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <br />
                        </div>
                        <div class="col-md-12" id="Premium">
                            <div class="row">
                                <div class="col-md-4">
                                    {{ Form::label('numberofPremiumLicence', 'Premium ') }}
                                    {{ Form::number('numberofPremiumLicence', $numberofPremiumLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('availablePremiumLicence', ' Premium ') }}
                                    {{ Form::number('availablePremiumLicence', $availablePremiumLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('addPremiumLicence', ' Premium ') }}
                                    {{ Form::number('addPremiumLicence', old('addPremiumLicence'), ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <br />
                        </div>
                        <div class="col-md-12" id="PremiumPlus">
                            <div class="row">
                                <div class="col-md-4">
                                    {{ Form::label('numberofPremPlusLicence', 'Premium Plus ') }}
                                    {{ Form::number('numberofPremPlusLicence', $numberofPremPlusLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('availablePremPlusLicence', ' Premium Plus ') }}
                                    {{ Form::number('availablePremPlusLicence', $availablePremPlusLicence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::label('addPrePlusLicence', ' Premium Plus ') }}
                                    {{ Form::number('addPrePlusLicence', old('addPrePlusLicence'), ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <br />
                        </div>
                    </div><br />
                    <div class="form-group">
                        {{ Form::label('timeZone', 'Time Zone') }}
                        {{ Form::select('timeZone', $timeZoneC, $timeZone, ['class' => 'selectpicker form-control', 'data-live-search ' => 'true']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('gpsvtsAppKey', 'Mobile App Package Key') }}
                        {{ Form::text('gpsvtsAppKey', $gpsvtsAppKey, ['id' => 'gpsapp', 'class' => 'form-control', 'placeholder' => 'Mobile App Package Key']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('addressKey', 'Address Key') }}
                        {{ Form::text('addressKey', $addressKey, ['id' => 'addkey', 'class' => 'form-control', 'placeholder' => 'Address Key']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('notificationKey', 'Notification Key') }}
                        {{ Form::text('notificationKey', $notificationKey, ['id' => 'notify', 'class' => 'form-control', 'placeholder' => 'Notification Key']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('zoho', 'Zoho Organisation') }}
                        {{ Form::text('zoho', $zoho, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('auth', 'Zoho Authentication') }}
                        {{ Form::text('auth', $auth, ['class' => 'form-control']) }}
                    </div>


                </div>

            </div>
            <div class="row mt-5">
                {{ Form::submit('Update the Franchise!', ['id' => 'sub', 'class' => 'btn btn-primary mx-auto']) }}
            </div>
            {{ Form::close() }}

        </div>
    </div>
    <script>
        $(document).ready(function() {

            $('#prepaid').on('change', showPrepaid);
            $("#enableStarter").on('change', showStarter);
            $("#authenticated").on("change", showStarterPlus);
            $("#authenticated,#enableStarter,#prepaid").trigger("change");

            function showStarter() {
                $("#Starter").hide();
                if ($('#enableStarter').val() === 'yes') {
                    $("#Starter").show();
                }
            }

            function showStarterPlus() {
                $("#StarterPlus").hide();
                if ($("#authenticated").val() == "yes") {
                    $("#StarterPlus").show()
                }
            }

            function showPrepaid() {
                if ($('#prepaid').val() == 'yes') {
                    $('#typeCount,#enableStarterdiv,#authenticatedEnable').show();
                    $('#numberofLicence').hide();
                } else {
                    $('#typeCount,#enableStarterdiv,#authenticatedEnable').hide();
                    $('#numberofLicence').show();
                }
            }

            $('#editForm').submit(function() {
                var datas = {
                    'val': $('#notify').val(),
                    'val1': $('#gpsapp').val(),
                    'val2': $('#mapkey').val(),
                    'val3': $('#addkey').val()
                }

                if ((datas.val).indexOf(' ') !== -1) {
                    wornigAlert('Please Enter Valid Notification Key');
                    $("#notify").focus();
                    return false;

                } else if ((datas.val1).indexOf(' ') !== -1) {
                    wornigAlert('Please Enter Valid Mobile App Key');
                    $("#gpsapp").focus();
                    return false;

                } else if ((datas.val2).indexOf(' ') !== -1) {
                    wornigAlert('Please Enter Valid Map Key / API Key');
                    $("#mapkey").focus();
                    return false;

                } else if ((datas.val3).indexOf(' ') !== -1) {
                    wornigAlert('Please Enter Valid Address Key');
                    $("#addkey").focus();
                    return false;

                }
            });
        });
    </script>


@stop
