@extends('includes.adminheader')
@section('mainContent')
    @if (session()->has('message'))
    <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
        {{ session()->get('message') }}
    </p>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-lg-9">
                    <h1>Licence Conversion</h1>
                </div>
                <div class="col-lg-3">
                </div>
            </div>
        </div>
        <div class="card-body">
            <span class="text-danger error-message"> {{ HTML::ul($errors->all()) }}</span>
            {{ Form::open(['url' => route('updateLicConvert'), 'method' => 'post', 'id' => 'submit']) }}
            <div class="row p-2">
                <div class="col-md-6 form-group">
                    <label for="lic">
                        Choose Licence Type to Convert
                    </label>
                    {{Form::select('licType',$licenceList,'',['class'=>'form-control','id'=>'licType','required'=>'required'])}}
                </div>
            </div>
            <div class="row p-2">
                <div class="col-md-6 form-group">
                    <label for="imei">
                        IMEI No
                    </label>
                    <input type="number" name="imei" id="imei" class="form-control" required>
                </div>
                
                <div class="col-md-12 text-center mt-3">
                    {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
@stop