@extends('includes.adminheader')
@section('mainContent')


    <div class="card">
        <div class="card-header">
            <h2>
                <font color="blue">Address Control</font>
            </h2>
        </div>
        <div class="card-body">
            {{ HTML::ul($errors->all()) }}

            {{ Form::open(['url' => 'vdmFranchises/updateAddCount']) }}
            <br>
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="form-group">
                        <div class="form-group">
                            {{ Form::label('addressCount', 'Daily Address buying count') }}
                            {{ Form::text('addressCount', $addressCount, ['class' => 'form-control']) }}
                        </div>

                    </div>
                    
                </div>
            </div>
            <div class="row mt-5">
                {{ Form::submit('Submit', ['class' => 'btn btn-primary mx-auto']) }}
            </div>
            {{ Form::close() }}
        </div>


    @stop
