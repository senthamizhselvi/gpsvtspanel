@extends('includes.adminheader')
@section('mainContent')



    <div class="card">
        <div class="card-header">
            <h2>
                <font color="blue">Select the Franchise name</font>
            </h2>
        </div>
        <div class="card-body">
            <!-- if there are creation errors, they will show here -->
            {{ HTML::ul($errors->all()) }}
        
            {{ Form::open(['url' => 'vdmFranchises/findUsersList']) }}
            <br>
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="form-group">
        
                        {{ Form::label('dealerId', 'Franchise Id') }}
                        {{ Form::select('users', $userId, null, ['class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true', 'required' => 'required']) }}
                                
                    </div>
        
                    <div class="col-md-6 mx-auto">
                        <input class="btn btn-primary" type="submit" value="Submit" style="margin-top: 10%;">
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop
