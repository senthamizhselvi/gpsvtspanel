@extends('includes.adminheader')
@section('mainContent')

	<span class="text-danger error-message">
		{{ HTML::ul($errors->all()) }}
	</span>
	<div class="page-header d-flex">
        <div class="page-title">
            Add Franchise
        </div>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="#">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="{{ route('vdmFranchises.index') }}">Franchise List</a>
            </li>
        </ul>
    </div>
	<div class="card">
		<div class="card-body">
			<!-- if there are creation errors, they will show here -->

			{{ Form::open(['url' => 'vdmFranchises']) }}
			
			<div class="row mx-2">
					<div class="col-md-6 form-group">
						{{ Form::label('fname', 'Franchise Name') }}
						{{ Form::text('fname', old('fname'), ['class' => 'form-control','required' => 'required']) }}
					</div>
				
					<div class="col-md-6 form-group">
						{{ Form::label('fcode', 'Franchise Code') }}
						{{ Form::text('fcode', old('fcode'), ['class' => 'form-control caps', 'required' => 'required']) }}
					</div>
				
					<div class="col-md-6 form-group">
						{{ Form::label('description', 'Description') }}
						{{ Form::text('description', old('description'), ['class' => 'form-control','required' => 'required']) }}
					</div>
				
					<div class="col-md-6 form-group">
						{{ Form::label('fullAddress', 'Full Address') }}
						{{ Form::text('fullAddress', old('fullAddress'), ['class' => 'form-control','required' => 'required']) }}
					</div>
				
					<div class="col-md-6 form-group">
						{{ Form::label('landline', 'Landline Number') }}
						{{ Form::text('landline', old('landline'), ['class' => 'form-control','required' => 'required']) }}
					</div>
				
					<div class="col-md-6 form-group">
						{{ Form::label('mobileNo1', 'Mobile Number1') }}
						{{ Form::text('mobileNo1', old('mobileNo1'), ['class' => 'form-control','required' => 'required']) }}
					</div>
				
					<div class="col-md-6 form-group">
						{{ Form::label('mobileNo2', 'Mobile Number2') }}
						{{ Form::text('mobileNo2', old('mobileNo2'), ['class' => 'form-control']) }}
					</div>
				
					<div class="col-md-6 form-group">
						{{ Form::label('email1', 'Email 1') }}
						{{ Form::text('email1', old('email1'), ['class' => 'form-control','required' => 'required']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('email2', 'Email 2') }}
						{{ Form::text('email2', old('email2'), ['class' => 'form-control','required' => 'required']) }}
					</div>
				
					<div class="col-md-6 form-group">
						{{ Form::label('userId', 'User Id(login)') }}
						{{ Form::text('userId', old('userId'), ['class' => 'form-control','required' => 'required']) }}
					</div>
				
					<div class="col-md-6 form-group">
						{{ Form::label('otherDetails', 'Other Details') }}
						{{ Form::text('otherDetails', old('otherDetails'), ['class' => 'form-control']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('prepaid', 'Prepaid Franchise') }}
						{{ Form::select('prepaid', ['' => '--Select--', 'no' => 'No', 'yes' => 'Yes'], null, ['class' => 'form-control', 'id' => 'prepaid', 'required' => 'required']) }}
					</div>
					<div class="col-md-6 form-group" id="numberofLicence">
						{{ Form::label('numberofLicence', 'Number of Licence') }}
						{{ Form::text('numberofLicence', old('numberofLicence'), ['class' => 'form-control']) }}
					</div>
					<div class="col-md-12 form-group" id="typeCount">
						<div class="row">
							<div class="col-md-3" id="Basic">
								{{ Form::label('numberofBasicLicence', 'Number of Basic Licence') }}
								{{ Form::number('numberofBasicLicence', old('numberofBasicLicence'), ['class' => 'form-control', 'placeholder' => 'Quantity', 'min' => '0']) }}
							</div>
							<div class="col-md-3" id="Advance">
								{{ Form::label('numberofAdvanceLicence', 'Number of Advance Licence') }}
								{{ Form::number('numberofAdvanceLicence', old('numberofAdvanceLicence'), ['class' => 'form-control', 'placeholder' => 'Quantity', 'min' => '0']) }}
							</div>
							<div class="col-md-3" id="Premium">
								{{ Form::label('numberofPremiumLicence', 'Number of Premium Licence') }}
								{{ Form::number('numberofPremiumLicence', old('numberofPremiumLicence'), ['class' => 'form-control', 'placeholder' => 'Quantity', 'min' => '0']) }}
							</div>
							<div class="col-md-3" id="PremPlus">
								{{ Form::label('numberofPremPlusLicence', 'Number of Premium Plus Licence') }}
								{{ Form::number('numberofPremPlusLicence', old('numberofPremPlusLicence'), ['class' => 'form-control', 'placeholder' => 'Quantity', 'min' => '0']) }}
							</div>
						</div>
					</div>
				
					<div class="col-md-6 form-group">
						{{ Form::label('website', 'Website') }}
						{{ Form::text('website', old('website'), ['class' => 'form-control','required' => 'required']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('apiKey', 'Map Key / Api Key') }}
						{{ Form::text('apiKey', old('apiKey'), ['id' => 'mapkey', 'class' => 'form-control', 'placeholder' => 'Map Key']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('website2', 'Website 2') }}
						{{ Form::text('website2', old('website2'), ['class' => 'form-control']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('apiKey2', 'Map Key / Api Key 2') }}
						{{ Form::text('apiKey2', old('apiKey2'), ['id' => 'mapkey', 'class' => 'form-control', 'placeholder' => 'Map Key']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('website3', 'Website 3') }}
						{{ Form::text('website3', old('website3'), ['class' => 'form-control']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('apiKey3', 'Map Key / Api Key 3') }}
						{{ Form::text('apiKey3', old('apiKey3'), ['id' => 'mapkey', 'class' => 'form-control', 'placeholder' => 'Map Key']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('website4', 'Website 4') }}
						{{ Form::text('website4', old('website4'), ['class' => 'form-control']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('apiKey4', 'Map Key / Api Key 4') }}
						{{ Form::text('apiKey4', old('apiKey4'), ['id' => 'mapkey', 'class' => 'form-control', 'placeholder' => 'Map Key']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('subDomain', 'SubDomain Name') }}
						{{ Form::text('subDomain', old('subDomain'), ['class' => 'form-control']) }}
					</div>
					<div class="form-check col-md-6 ">
						{{ Form::label('type', 'Track Page') }}<br>
						<label class="form-radio-label">
							{{ Form::radio('trackPage', 'Normal View', true,['class'=>'form-radio-input']) }}
							<span class="form-radio-sign">Normal View</span>
						</label>
						<label class="form-radio-label ml-3">
							{{ Form::radio('trackPage', 'Customized View',false,['class'=>'form-radio-input']) }}
							<span class="form-radio-sign">Customized View</span>
						</label>
					</div>
					<br />
					<div class="col-md-6 form-group">
						{{ Form::label('smsSender', 'SMS Sender') }}
						{{ Form::text('smsSender', old('smsSender'), ['class' => 'form-control']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('smsProvider', 'SMS Provider') }}
						{{ Form::select('smsProvider', $smsP, old('smsProvider'), ['class' => 'form-control']) }}
					</div>
				
					<div class="col-md-6 form-group">
						{{ Form::label('providerUserName', 'SMS Provider User Name') }}
						{{ Form::text('providerUserName', old('providerUserName'), ['class' => 'form-control']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('providerPassword', 'SMS Provider Password') }}
						{{ Form::text('providerPassword', old('providerPassword'), ['class' => 'form-control']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('ipadd', 'DB IP') }}
						{{ Form::select('ipadd', $dbIp, old('ipadd'), ['class' => 'form-control']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('backUpDays', 'DB BackupDays') }}
						{{ Form::number('backUpDays', old('backUpDays'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'numbers', 'min' => '1']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('dbType', 'DB Type') }}
						{{ Form::select('dbType', $backType, 'mysql', ['class' => 'form-control']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('timeZone', 'Time Zone') }}
						{{ Form::select('timeZone', $timeZoneC, 'Asia/Kolkata', ['class' => 'selectpicker form-control', 'data-live-search ' => 'true']) }}
					</div>
					
					<div class="col-md-6 form-group">
						{{ Form::label('gpsvtsAppKey', 'Mobile App Key') }}
						{{ Form::text('gpsvtsAppKey', old('gpsvtsAppKey'), ['id' => 'gpsapp', 'class' => 'form-control', 'placeholder' => 'Mobile App Key']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('addressKey', 'Address Key') }}
						{{ Form::text('addressKey', old('addressKey'), ['id' => 'addkey', 'class' => 'form-control', 'placeholder' => 'Address Key']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('notificationKey', 'Notification Key') }}
						{{ Form::text('notificationKey', old('notificationKey'), ['id' => 'notify', 'class' => 'form-control', 'placeholder' => 'Notification Key']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('zoho', 'Zoho Organization') }}
						{{ Form::text('zoho', old('zoho'), ['class' => 'form-control']) }}
					</div>
					<div class="col-md-6 form-group">
						{{ Form::label('auth', 'Zoho Authentication') }}
						{{ Form::text('auth', old('auth'), ['class' => 'form-control']) }}
					</div>
			</div>
			<div class="row mt-3">
				{{ Form::submit('Add the Franchise!', ['class' => 'btn btn-primary mx-auto','id'=>'sub']) }}
			</div>
		
			{{ Form::close() }}
		</div>
	</div>


    <script>
        $(document).ready(function() {
            $('#prepaid').on('change', function() {
                var licence = {
                    'Type': $('#prepaid').val()
                }
                if (licence.Type == 'yes') {
                    $('#typeCount').show();
                    $('#numberofLicence').hide();
                } else {
                    $('#typeCount').hide();
                    $('#numberofLicence').show();
                }

            });


            $('#sub').on('click', function() {
                var datas = {
                    'val': $('#notify').val(),
                    'val1': $('#gpsapp').val(),
                    'val2': $('#mapkey').val(),
                    'val3': $('#addkey').val()
                }

                if ((datas.val).indexOf(' ') !== -1) {

                    alert('Please Enter Valid Notification Key');
                    document.getElementById("notify").focus();
                    return false;

                } else if ((datas.val1).indexOf(' ') !== -1) {
                    alert('Please Enter Valid Mobile App Key');
                    document.getElementById("gpsapp").focus();
                    return false;

                } else if ((datas.val2).indexOf(' ') !== -1) {
                    alert('Please Enter Valid Map Key / API Key');
                    document.getElementById("mapkey").focus();
                    return false;

                } else if ((datas.val3).indexOf(' ') !== -1) {
                    alert('Please Enter Valid Address Key');
                    document.getElementById("addkey").focus();
                    return false;

                }
            });
        });

        window.onload = function() {
            var licence = {
                'Type': $('#prepaid').val()
            }
            if (licence.Type == 'yes') {
                $('#typeCount').show();
                $('#numberofLicence').hide();
            } else {
                $('#typeCount').hide();
                $('#numberofLicence').show();
            }

        }
    </script>

@stop
