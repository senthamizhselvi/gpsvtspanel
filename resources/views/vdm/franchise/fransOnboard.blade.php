@extends('includes.adminheader')
@section('mainContent')

    <div class="page-body">
        <div class="card">
            <div class="card-header">
              <div class="h3 b">
                Onboard Franchise
              </div>
            </div>
            <div class="card-body">
                <span class="text-danger">
                    {{ HTML::ul($errors->all()) }}
                </span>
                {{ Form::open(['url' => 'fransOnUpdate']) }}
                <div class="form-row flex-column">
                    <div class="form-group w-50 mx-auto">
                        {{ Form::label('frans', 'Franchises') }}
                        {{ Form::select('fcodeList', $fcodeList, null, ['id' => 'fcodeList', 'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true', 'required' => 'required']) }}
                    </div>
                    <div class="form-group w-50 mx-auto">
                        {{ Form::label('date', 'Date') }}
                        {{ Form::text('frmDate', '', ['id' => 'txtFromDate', 'class' => 'form-control', 'placeholder' => 'Select Date']) }}
                    </div>
                    {{Form::submit('Submit',['class'=>'btn btn-primary mx-auto mt-3'])}}
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>



    {{-- <div class="row" style="margin-left: 2%;">
        <div class="form-group">
            <h2>
                <font color="blue">Select the Franchise name</font>
            </h2>
            <div class="row">
                <div class="col-md-3">
                    {{ Form::label('frans', 'Franchises') }}
                    {{ Form::select('fcodeList', $fcodeList, null, ['id' => 'fcodeList', 'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true', 'required' => 'required']) }}
                </div>
                <div class="col-md-2">Date: <input class="txtdate" id="txtFromDate" type="text" name="frmDate"
                        placeholder="Select Date"></div>
                <!--  <div class="col-md-2"> To Date: <input class="txtdate" id="txtToDate" name="toDate" type="text" placeholder="To Date"> </div> -->
                <div class="col-md-3">
                    <input class="btn btn-primary" type="submit" value="Submit">
                </div>
            </div>
        </div>
    </div> --}}
    <script language="javascript">
        $(document).ready(function() {
            $("#txtFromDate").datepicker({
                maxDate: '-1',
                minDate: new Date(2019, 02 - 1, 12),
                onSelect: function(selected) {
                    $("#txtToDate").datepicker("option", "minDate", selected)

                }
            });
            $("#txtToDate").datepicker({
                maxDate: '0',
                numberOfMonths: 1,
                onSelect: function(selected) {
                    $("#txtFromDate").datepicker("option", "maxDate", selected)
                }
            });
        });
    </script>
    {{ Form::close() }}
@stop
