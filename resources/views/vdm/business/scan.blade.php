@extends("layouts.app")
@section('content')

    <div id="wrapper">
        <div class="content page-inner">
            @if (session()->has('message'))
                <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                    {{ session()->get('message') }}
                </p>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="pull-right">
                            <a href="DeviceScan/sendExcel">
                                <img class="pull-right" width="10%" height="10%" method="get"
                                    src="{{ asset('assets/imgs/xls.svg') }}" alt="xls" />
                            </a>
                        </div>
                        <div class="page-header">
                            <h4 class="page-title">
                                @lang('messages.OnboardDevices')
                            </h4>
                        </div>
                        <div class="card" class="panel-body">
                            <div class="card-header">
                                {{ Form::open(['url' => 'DeviceScan', 'method' => 'post']) }}
                                <div class="row">
                                    <div class="col-md-3 col-sm-9 d-flex justify-content-center align-items-center">
                                        {{ Form::text('text_word', request()->old('text_word'), ['class' => 'form-control ', 'placeholder' => 'Search Devices']) }}
                                    </div>
                                    <div class="col-md-6 col-sm-3">
                                        {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary']) }}
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                            <div id="data" class="form-group overflow-auto">
                                <table id="example1" class="table table-bordered table-head-bg-info">
                                    <thead data-background-color="blue2">
                                        <tr>
                                        <tr>
                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                <th style="text-align: center;">
                                                    @lang('messages.LicenceID')
                                                </th>
                                            @endif
                                            <th style="text-align: center;">
                                                @lang('messages.DeviceID')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.VehicleId')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.DealerName')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.LicenceIssuedDate')
                                            </th>
                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                <th style="text-align: center;">
                                                    @lang('messages.Type')
                                                </th>
                                            @endif
                                            <th style="text-align: center;">
                                                @lang('messages.OnboardDate')
                                            </th>
                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                <th style="text-align: center;">
                                                    @lang('messages.LicenceExpiry')
                                                </th>
                                            @endif
                                            @if (session()->get('cur1') != 'prePaidAdmin')
                                                <th style="text-align: center;">
                                                    @lang('messages.VehicleExpiry')
                                                </th>
                                                <th style="text-align: center;">
                                                    @lang('messages.MoveVehicle')
                                                </th>
                                            @endif
                                            <th style="text-align: center;">
                                                @lang('messages.EditVehicle')
                                            </th>
                                        </tr>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($deviceMap))
                                            @foreach ($deviceMap as $key => $value)
                                                <tr style="text-align: center;">
                                                    @if (session()->get('cur1') == 'prePaidAdmin')
                                                        <td>{{ explode(',', $value)[8] }}</td>
                                                    @endif
                                                    <td>{{ explode(',', $value)[1] }}</td>
                                                    <td>{{ explode(',', $value)[0] }}</td>
                                                    <td>{{ explode(',', $value)[2] }}</td>
                                                    <td>{{ explode(',', $value)[4] }}</td>
                                                    @if (session()->get('cur1') == 'prePaidAdmin')
                                                        <td>{{ explode(',', $value)[7] }}</td>
                                                    @endif
                                                    <td>{{ explode(',', $value)[5] }}</td>
                                                    @if (session()->get('cur1') == 'prePaidAdmin')
                                                        <td>{{ explode(',', $value)[9] }}</td>
                                                    @endif
                                                    @if (session()->get('cur1') != 'prePaidAdmin')
                                                        <td>{{ explode(',', $value)[6] }}</td>
                                                        <td>
                                                            <a class="btn btn-sm btn-danger"
                                                                href="{{ URL::to('vdmVehicles/move_vehicle/' . explode(',', $value)[0]) }}">
                                                                @lang('messages.MoveVehicle')
                                                            </a>
                                                        </td>
                                                    @endif
                                                    <td>
                                                        <a class="btn btn-sm btn-warning"
                                                            href="{{ URL::to('vdmVehicles/edit/' . explode(',', $value)[0]) }}">
                                                            @lang('messages.EditVehicle')
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
@push('js')
    @include('includes.js_index')
@endpush
