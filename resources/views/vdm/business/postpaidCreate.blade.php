@extends("layouts.app")
@section('content')
<div id="wrapper">
    <style>
        .form-control {
            height: auto !important;
        }
    </style>
    <div class="page-inner">
        <div class="content ">
            <div class="hpanel">
                <div class="page-header">
                    <h4 class="page-title">@lang('messages.AddBusiness')</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('Device.index') }}">@lang('messages.Business')</a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('Device.index') }}">@lang('messages.AddDevice')</a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">@lang('messages.AddBusiness')</a>
                        </li>
                    </ul>
                </div>
                <div class="card animated fadeIn">

                    <div class="card-header">
                        <p class="h3">@lang('messages.Business')</p>
                    </div>
                    <div class="card-body">
                        <span class="text-danger error-message">
                            {{ HTML::ul($errors->all()) }}
                        </span>
                        <div class="panel-body" style="">
                            {{ Form::open(['id' => 'submit']) }}
                            <span id="error" style="color:red;font-weight:bold"></span>
                            <div class="col-md-12">
                                {{ Form::hidden('numberofdevice', $numberofdevice, ['class' => 'form-control']) }}
                                {{ Form::hidden('availableLincence', $availableLincence, ['class' => 'form-control']) }}
                            </div>
                            <div class="row text-center">
                                <div class="col-md-3  "></div>
                                <div class="col-4 col-md-2"
                                    style="color: #6a6c6f;font-size: 15px; font-weight: bold;">
                                    <div class="form-group">
                                        <label class="form-radio-label" id="hide">
                                            <input class="form-radio-input" type="radio" name="dealerOrUser"
                                                value="Move">
                                            <span class="form-radio-sign">&nbsp;&nbsp;
                                                @lang('messages.Dealers')</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-4 col-md-2"
                                    style="color: #6a6c6f;font-size: 15px; font-weight: bold;">
                                    <div class="form-group">
                                        <label class="form-radio-label" id="show">
                                            <input class="form-radio-input" type="radio" name="dealerOrUser"
                                                value="Sale">
                                            <span class="form-radio-sign">&nbsp;&nbsp;
                                                @lang('messages.Users')</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 col-4 text-center text-md-left">
                                        {{ Form::submit(__('messages.Submit'), ['id' => 'Subtest', 'class' => 'btn btn-primary']) }}
                                </div>
                            </div>
                            <div id="p1">
                                <hr>
                                <div class="row" >
                                    <div class="col-md-4 mx-auto mb-3 " >
                                        {{ Form::label('dealerList', __('messages.SelectDealer')) }}
                                        {{ Form::select('dealerId', $dealerIdList, old('dealerId'), ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true', 'data-toggle' => 'dropdown']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="" id="p">
                                <hr id="hr">
                                <div class="row text-center" >
                                    <div class="col-md-3"></div>
                                    <div class="col-6 col-md-2"
                                        style="color: #16564b;font-size: 15px; font-weight: bold;">
                                        <div class="form-group">
                                            <label class="form-radio-label" id="hide1">
                                                <input class="form-radio-input" type="radio" name="isExistingUser" @if (old('isExistingUser') == "new") checked  @endif
                                                    value="new">
                                                <span class="form-radio-sign">&nbsp;&nbsp;
                                                    @lang('messages.NewUser')</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-2"
                                        style="color: #16564b;font-size: 15px; font-weight: bold;">
                                        <div class="form-group">
                                            <label class="form-radio-label" id="show1">
                                                <input class="form-radio-input" type="radio" name="isExistingUser" @if (old('isExistingUser') == "existing") checked  @endif
                                                    value="existing">
                                                <span class="form-radio-sign">&nbsp;&nbsp;
                                                    @lang('messages.ExistingUser')</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <div class="row mx-md-5 mx-0 my-3 mb-4 animated fadeIn " id="t">
                                <div class=" col-md-4 col-12 text-center">
                                    {{ Form::label('ExistingUser', __('messages.ExistingUser')) }}
                                    {{ Form::select('userIdtemp', $userList, 'select', ['id' => 'ExistingUser', 'class' => 'selectpicker show-menu-arrow form-control ExistingUser', 'data-live-search ' => 'true']) }}
                                </div>
                                <div class="col-md-4 col-12 text-center">
                                    {{ Form::label('Group', __('messages.GroupName')) }}
                                    {{ Form::select('groupname', ['' => __('messages.select')], old('groupname'), ['id' => 'groupname', 'class' => 'form-control selectpicker show-menu-arrow ExistingUser', 'data-live-search ' => 'true']) }}
                                </div>
                                <div class="col-md-4 col-12 text-center">
                                    {{ Form::label('orgId', __('messages.OrganizationName')) }}
                                    {{ Form::select('orgId', $orgList, old('orgId'), ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true']) }}
                                </div>

                            </div>
                            <div class="row animated fadeIn my-3 mb-4 mx-0 mx-md-5" Id="t1">
                                <div class="col-md-3 col-12">
                                    {{Form::label(__('messages.UserName'))}}
                                    {{ Form::text('userId', old('userId'), ['id' => 'NewUserId', 'class' => 'form-control caps newUser', 'onkeyup' => 'caps(this)', 'placeholder' => __('messages.UserName')]) }}
                                </div>
                                <div class="col-md-3 col-12">
                                    {{Form::label(__('messages.MobileNumber'))}}
                                    {{ Form::text('mobileNoUser', old('mobileNoUser'), ['class' => 'form-control newUser', 'placeholder' => __('messages.MobileNumber')]) }}
                                </div>
                                <div class="col-md-3 col-12">
                                    {{Form::label(__('messages.Email'))}}
                                    {{ Form::Email('emailUser', old('emailUser'), ['class' => 'form-control newUser', 'placeholder' => __('messages.Email')]) }}
                                </div>
                                <div class="col-md-3 col-12">
                                    {{Form::label(__('messages.Password'))}}
                                    {{ Form::text('password', old('password'), ['class' => 'form-control newUser', 'placeholder' => __('messages.Password')]) }}
                                </div>

                            </div>
                            <hr id="h2" style="width: 100%;">
                            <div class="row animated fadeIn my-3 text-center" id="m">
                                <div class="col-md-2 mr-auto"></div>
                                <div class="col-6 col-md-3 px-0 px-md-2"
                                    style="color: #2e0b4c;font-size: 15px; font-weight: bold;">
                                    <label class="form-radio-label" id="excel1">
                                        <input class="form-radio-input" type="radio" name="type3" value="detail"
                                            id="excel">
                                        <span class="form-radio-sign">
                                            @lang('messages.UploadDevices')
                                        </span>
                                    </label>
                                </div>
                                <div class="col-6 col-md-3 px-0 px-md-2"
                                    style="color: #2e0b4c;font-size: 15px; font-weight: bold;">
                                    <label class="form-radio-label" id="de1">
                                        <input class="form-radio-input" type="radio" name="type3" value="detail"
                                            id="ondetails" checked>
                                        <span class="form-radio-sign">
                                            @lang('messages.DeviceDetails')
                                        </span>
                                    </label>
                                </div>
                                <div class="col-md-2 ml-auto"></div>
                            </div>

                            <div class="row animated fadeIn mb-3 mt-5" Id="n1">
                                <br>
                                <div class="col-md-2"></div>
                                <div class="col-md-2"><a href="{{ URL::to('downloadExcel/xls') }}"><button
                                            class="btn btn-sm btn-primary"
                                            id="dexcel">@lang('messages.DownloadTemplate')</button></a>
                                </div>

                                <div class="col-md-4">
                                    <div class="custom-file">
                                        <input type="file" name="import_file" class="custom-file-input" id="Upload"
                                            accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" >
                                        <label class="custom-file-label" for="customFile"
                                            id="fileLabel">@lang('messages.ChooseaFile')
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2" style="margin-left: 30px;">
                                    <a class="btn btn-sm"
                                        style="background-color:#2d86a7;border-color: #0e75a0; color:#ffffff;"
                                        id="import">@lang('messages.ImportExcel')</a>
                                </div>

                            </div>
                            <br>
                            <div class="row animated fadeIn" id="details">
                                <div class="col-12 px-md-5 px-0 table-responsive">
                                    <table class="table table-bordered table-head-bg-info ">
                                        <thead>
                                            <tr>
                                                <th
                                                    style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">
                                                    @lang('messages.No')</th>
                                                <th
                                                    style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">
                                                    @lang('messages.DeviceID')</th>
                                                <th
                                                    style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">
                                                    @lang('messages.DeviceType')</th>
                                                <th
                                                    style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">
                                                    @lang('messages.VehicleId')
                                                </th>
                                                <th
                                                    style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">
                                                    @lang('messages.Actions')
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @for ($i = 1; $i <= $numberofdevice; $i++)
                                               
                                                <tr style="text-align: center;">
                                                    <td>{{ $i }}</td>
                                                    <td class="batchSale">
                                                        <div class="form-group">
                                                            {{ Form::text('deviceid[]', old('deviceid[]'), ['id' => 'deviceid' . $i, 'class' => 'form-control batchSale caps deviceid', 'placeholder' => __('messages.DeviceId')]) }}
                                                        </div>
                                                    </td>
                                                    <td class="batchMove">
                                                        <div class="form-group">
                                                            {{ Form::text('deviceidSale[]', $deviceId[$i], ['id' => 'deviceidSale' . $i, 'class' => 'form-control batchMove deviceidSale']) }}
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div class="form-group">
                                                            {{ Form::select('deviceidtype[]', $protocol, old('deviceidtype[]'), ['class' => 'form-control selectpicker']) }}
                                                        </div>
                                                    </td>
                                                    <td class="batchSale">
                                                        <div class="form-group">
                                                            {{ Form::text('vehicleId[]', old('vehicleId[]'), ['id' => 'vehicleId' . $i, 'class' => 'form-control batchSale caps vehicleId', 'placeholder' => __('messages.VehicleId')]) }}
                                                        </div>
                                                    </td>
                                                    <td class="batchMove">
                                                        <div class="form-group">
                                                            {{ Form::text('vehicleIdSale[]', $vehicleId[$i], ['id' => 'vehicleIdSale' . $i, 'class' => 'form-control batchMove caps vehicleIdSale']) }}
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <a class="btn btn-sm btn-success showDetails" data-count="{{$i}}">@lang('messages.Details')</a>
                                                    </td>
                                                </tr>

                                                <tr id="td{{ $i }}" class="animated fadeIn showDetailsRow">
                                                    <td></td>
                                                    <td >
                                                        <div class="form-group">
                                                            {{ Form::text('shortName[]', old('shortName[]'), ['class' => 'form-control', 'placeholder' => __('messages.VehicleName')]) }}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group w-100">
                                                            {{ Form::select('vehicleType[]', $vehicleModalList, old('vehicleType[]'), ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true', 'style' => 'width:100%']) }}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            {{ Form::number('gpsSimNo[]', old('gpsSimNo[]'), ['class' => 'form-control', 'placeholder' => __('messages.GPSsimNo')]) }}
                                                        </div>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            @endfor
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    var alerttest;
    function isExistingUser(){
        value = $('[name="isExistingUser"]:checked').val()
        $('.newUser').attr('required',value == 'new')
        $('.ExistingUser').attr('required',value == 'existing')
    }
    isExistingUser()

    $(document).ready(function() {
        $(".showDetailsRow").hide();
        $('.deviceid').on('change', function() {
            removeAfterErr(this)
            let input = this;
            if($(input).val().trim() == "") return
            var data = {
                'id': $(input).val(),
                '_token': $('input[name="_token"]').val()
            };
            var splChars =
                /[ !@#$%^&*() +\=\[\]{};':"\\|,.<>\/?]/;
            if ((data.id).match(splChars)) {
                alerttest = true;
                wornigAlert('Please Enter valid Id');
                return false;
            }
            $.post('{{ route('ajax.checkDevice') }}',data,
                function(data, textStatus,xhr) {
                    if (data.error.trim() != '') {
                        $(input).aftererr(data.error);
                        wornigAlert(data.error);
                        alerttest = true;
                    }
                });
        });

        $('.deviceidSale').on('change', function() {
            removeAfterErr(this)
            let input = this;
            if($(input).val().trim() == "") return

            var data = {
                'id': $(this).val(),
                '_token': $('input[name="_token"]').val()
            };
            var splChars =
                /[ !@#$%^&*() +\=\[\]{};':"\\|,.<>\/?]/;
            if ((data.id).match(splChars)) {
                alerttest = true;
                wornigAlert('Please Enter valid Id');
                $(this).focus();
                return false;
            }
            $.post('{{ route('ajax.checkDevice') }}',
                data,
                function(data, textStatus,
                    xhr) {
                        if (data.error.trim() != '') {
                        $(input).aftererr(data.error);
                        wornigAlert(data.error);
                        alerttest = true;
                    }
                });
        });

        $('.vehicleId').on('change', function() {
            removeAfterErr(this)
            let input = this;
            if($(input).val().trim() == "") return

            var data = {
                'id': $(this).val(),
                '_token': $('input[name="_token"]').val()
            };
            var splChars =
                /[ !@#$%^&*() +\=\[\]{};':"\\|,.<>\/?]/;
            if ((data.id).match(splChars)) {
                alerttest = true;
                wornigAlert('Please Enter valid Id');
                return false;
            }
            $.post('{{ route('ajax.checkvehicle') }}',
                data,
                function(data, textStatus,
                    xhr) {
                    if (data.error != ' ') {
                        $(input).aftererr(data
                            .error);
                        wornigAlert(data.error);
                        alerttest = true;
                    }

                });
        });

        $('.vehicleIdSale').on('change', function() {
            removeAfterErr(this)
            let input = this;
            if($(input).val().trim() == "") return

            var data = {
                'id': $(this).val(),
                '_token': $('input[name="_token"]').val()

            };
            var splChars =
                /[ !@#$%^&*() +\=\[\]{};':"\\|,.<>\/?]/;
            if ((data.id).match(splChars)) {
                alerttest = true;
                wornigAlert('Please Enter valid Id');
                $(this).focus();
                return false;
            }
            $.post('{{ route('ajax.checkvehicle') }}',
                data,
                function(data, textStatus, xhr) {
                    
                    if (data.error != ' ') {
                        $(input).aftererr(data
                            .error);
                        wornigAlert(data.error);
                        alerttest = true;
                    }

                });
        });

        $('#ExistingUser').on('change', getGroupList);
        getGroupList()
        function getGroupList() {
            $userName = $("#ExistingUser").val();
            if($userName == "select") return
            var data = {
                'id': $userName,
                '_token': $('input[name="_token"]').val()
            };
            $.post('{{ route('ajax.getGroup') }}', data,
                function(data, textStatus, xhr) {
                    $('#groupname').empty();
                    if (data.error.trim() != '') {
                        wornigAlert(data.error);
                    }
                    $.each(data.groups, function(key,value) {
                        $('#groupname')
                            .append(
                                $("<option></option>")
                                .attr("value",key)
                                .text(value)
                            );
                    });
                    // $('#groupname').selectpicker('refresh');
                });
        }

        $('#NewUserId').on('change', function() {
            removeAfterErr(this)
            let input = this;
            if($(input).val().trim() == "") return

            $('#error').text('');
            var data = {
                'id': $(this).val(),
                '_token': $('input[name="_token"]').val()
            };
            console.log('ahan' + data);
            $.post('{{ route('ajax.checkUser') }}', data,
                function(data, textStatus, xhr) {
                    if (data.error.trim() != '') {
                        $(input).aftererr(data.error);
                        wornigAlert(data.error);
                    }
                });
        });

        $(".showDetails").click(function() {
            id = $(this).attr('data-count');
            $("#td"+id).toggle(100);
        });
    });

</script>
    <script>
        $("#Subtest").click(function() {
            var formAction = "{{route('Business/adddevice')}}";
            $('#submit').attr('action', formAction);
        });

        $("#import").click(function() {
            var formAction1 = "{{route('import/Excel')}}";
            var excel = $("#Upload").value;
            var enc = 'multipart/form-data';
            //enctype="multipart/form-data"
            $('#submit').attr('action', formAction1);
            $('#submit').attr('enctype', enc);
            $('#submit').submit();
        });


        $("#dexcel").click(function() {
            var formAction = "{{route('downloadExcel/xls')}}";
            $('#submit').attr('action', formAction);
            //submit form
            $('#submit').submit();
        });


        $("#hide").click(function() {
            $("#p,#t,#t1,.batchSale").hide();
            $("#p1,#m,.batchMove").show();
            $('#hide').attr('disabled', true);
            $(".batchMove").prop('required', true);
            $(".batchSale").prop('required', false);
            $('#de1').click();
            $("[name='isExistingUser']").prop('checked',false);
            var inputUser = $("input[name=isExistingUser]:checked").val();
            $("input[name=type1]:checked").attr('value', '');
            isExistingUser()
        });
        $("#show").click(function() {
            $('#m,#n1,#details,#h1,#p1,.batchMove').hide();
            $('#hr,#p,.batchSale').show();
            $('#show').attr('disabled', true);
            $(".batchSale").prop('required', true);
            $(".batchMove").prop('required', false);
            // var inputUser = $("input[name=isExistingUser]:checked").val();
            // var inputUser = $(":input[name=type1]:checked").val();
            //  $(":input[name=type1]:checked").attr('value', inputUser);
            //$("#Subtest").value="SUBMIT";
            isExistingUser()
        });

        $("#excel1").click(function() {
            $("#n1,#h2,#import").show();
            $("#details,#Subtest").hide();
            $("#Subtest").value = "IMPORT EXCEL";
        });

        $("#de1").click(function() {
            $('#de1').checked = true;
            $('#excel1').checked = false;
            $("#details,#Subtest").show();
            $("#n1,#import").hide();
            $("#Subtest").value = "SUBMIT";
            isExistingUser()

        });
        $("#hide1").click(function() {
            $("#t").hide();
            $("#t1,#h1,#h2,#m").show();
            $('#de1').click();
            //$('#NewUserId').required=true;
            isExistingUser()


        });


        $("#show1").click(function() {
            $("#t,#h1,#h2,#m").show();
            $("#t1").hide();
            $('#de1').click();
            $('#show1').attr('disabled', true);
            $('#NewUserId').required = "";
            isExistingUser()

        });

        
        
        // When the user clicks anywhere outside of the modal, close it
        $(function() {
            $("#p,#p1,#t,#t1,#m,#n1,#details,#h1,#h2,#import,#Subtest,#hr").hide();
            $('.batchMove').hide();
            $("#Subtest").value = "SUBMIT";
        })
    </script>
@endpush
