@extends("layouts.app")
@section('content')
    @if (session()->get('cur1') == 'prePaidAdmin')
        @include('vdm.business.parentCreate')
    @else
        @include('vdm.business.parentCreate1')
    @endif
@endsection
@push('js')
    @include('includes.js_create')
@endpush
