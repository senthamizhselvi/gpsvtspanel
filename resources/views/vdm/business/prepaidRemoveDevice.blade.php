
@extends("layouts.app")
@section('content')
<div id="wrapper">
    <div class="content page-inner">

        <div class="hpanel">
            <div class="page-header">
                <h4 class="page-title">
                    @lang('messages.RemoveDevice')
                </h4>
            </div>
            @if (session()->has('message'))
                <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                    {{ session()->get('message') }}</p>
            @endif
            <span class="text-denger">{{ HTML::ul($errors->all()) }}</span>
            <div class="page-body">
                <div class="card animated zoomIn">
                    <div class="card-body">
                        <div class="col-sm-12">
                            {{ Form::hidden('availableBasicLicence', $availableBasicLicence, ['id' => 'availableBasicLicence']) }}
                            {{ Form::hidden('availableAdvanceLicence', $availableAdvanceLicence, ['id' => 'availableAdvanceLicence']) }}
                            {{ Form::hidden('availablePremiumLicence', $availablePremiumLicence, ['id' => 'availablePremiumLicence']) }}
                            {{ Form::open(['url' => 'Remove']) }}

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-8" style="left: 8%;">
                                        <div class="row" id='type'>
                                            <div class="col-md-5">
                                                <label for="LicenceType"
                                                    style="padding-top: 5%;">@lang('messages.DeviceType')
                                                    :</label>
                                            </div>
                                            <div class="col-md-5">
                                                <select id="LicenceType" class="form-control" data-live-search="true"
                                                    required="required" name="LicenceType" style="min-width: 100%;">
                                                    <option value="Basic">Basic</option>
                                                    <option value="Advance">Advance</option>
                                                    <option value="Premium">Premium</option>
                                                </select>
                                            </div>
                                        </div><br />
                                        <div class="row" id='basic'>
                                            <div class="col-md-5">
                                                <label for="basicdevice" class="pt-0">
                                                    @lang('messages.NumberOfBasicDevicestoberemoved') :
                                                </label>
                                            </div>
                                            <div class="col-md-5">
                                                <input class="form-control" placeholder="Quantity" min="1"
                                                    name="onBasicdevice" type="number" id="basicdevice"
                                                    style=" min-width: 100%;">
                                            </div>
                                        </div>
                                        <div class="row" id='advance'>
                                            <div class="col-md-5">
                                                <label for="advancedevice" class="pt-0">
                                                    @lang('messages.>NumberOfAdvanceDevicestoberemoved') :
                                                </label>
                                            </div>
                                            <div class="col-md-5">
                                                <input class="form-control" placeholder="Quantity" min="1"
                                                    name="onAdvancedevice" type="number" id="advancedevice"
                                                    style=" min-width: 100%;">
                                            </div>
                                        </div>
                                        <div class="row" id='premium'>
                                            <div class="col-md-5">
                                                <label for="premiumdevice" class="pt-0">
                                                    @lang('messages.NumberOfPremiumDevicestoberemoved') :
                                                </label>
                                            </div>
                                            <div class="col-md-5">
                                                <input class="form-control" placeholder="Quantity" min="1"
                                                    name="onPremiumdevice" type="number" id="premiumdevice"
                                                    style=" min-width: 100%;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                    </div>
                                </div>
                            </div><br />
                            <div class="col-sm-4" style="margin-left: 20%;">
                                {{ Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => 'sub']) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
    function LicenceType(input) {
        let type = $(input).val().toLowerCase();
        $('#basic,#advance,#premium').hide();
        $('#basicdevice,#advancedevice,#premiumdevice').removeAttr('required');
        $('#' + type).show();
        $('#' + type + 'device').attr({
            'required': 'required'
        });
    }
    $('#LicenceType').on('change', function() {
        LicenceType(this);
    });
    $(function() {
        LicenceType('#LicenceType');
    })
</script>
@endpush
