@extends("layouts.app")
@section('content')
	@if(session()->get('cur1') =='prePaidAdmin')
		@include('vdm.business.prepaidRemoveDevice')
	@else
		@include('vdm.business.removeDevice')
	@endif
@endsection
