@extends("layouts.app")
@section('content')
<div id="wrapper">
    <div class="content animate-panel page-inner">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="page-header">
                        <h4 class="page-title">@lang('messages.AddDevice')</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="#">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('Device.index') }}">
                                    @lang('messages.Business')
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">
                                    @lang('messages.AddDevice')
                                </a>
                            </li>
                        </ul>
                    </div>
                    @if (session()->has('message'))
                        <p class="alert {{ session('alert-class', 'alert-success') }}">
                            {{ session('message') }}
                        </p>
                    @endif
                    <div class="page-body">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="text-info">
                                    @lang('messages.AvailableLicence') : {{ $availableLincence }}
                                </h4>
                            </div>
                            <div class="card-body">
                                <span class="text-danger  error-message"> {{ HTML::ul($errors->all()) }}</span>
                                <div class="col-sm-12 mt-3">
                                    {{ Form::open(['url' => 'Business']) }}
                                    {{ Form::hidden('availableStarterLicence', $availableStarterLicence, ['id' => 'availableStarterLicence']) }}
                                    {{ Form::hidden('availableBasicLicence', $availableBasicLicence, ['id' => 'availableBasicLicence']) }}
                                    {{ Form::hidden('availableAdvanceLicence', $availableAdvanceLicence, ['id' => 'availableAdvanceLicence']) }}
                                    {{ Form::hidden('availablePremiumLicence', $availablePremiumLicence, ['id' => 'availablePremiumLicence']) }}
                                    {{ Form::hidden('availablePremPlusLicence', $availablePremPlusLicence, ['id' => 'availablePremPlusLicence']) }}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="mx-auto">
                                                <div class="row" id='type'>
                                                    <div class="col-md-5">
                                                        <label for="LicenceType" style="padding-top: 5%;">@lang('messages.LicenceType')
                                                            :</label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <select class=" form-control" id="LicenceType"
                                                            required="required" data-live-search="true"
                                                            name="LicenceType">
                                                            @if (session('isAuthenticated') == 'yes')
                                                                <option value="StarterPlus">Starter Plus</option>
                                                            @endif
                                                            @if (session('enableStarter') == 'yes')
                                                                <option value="Starter">Starter</option>
                                                            @endif
                                                            <option value="Basic">Basic</option>
                                                            <option value="Advance">Advance</option>
                                                            <option value="Premium">Premium</option>
                                                            <option value="PremiumPlus">Premium Plus</option>
                                                        </select>
                                                    </div>
                                                </div><br />
                                                <div class="row" id='starter'>
                                                    @if (session('enableStarter') == 'yes')
                                                        <div class="col-md-5">
                                                            <label for="basicdevice" style="padding-top: 5%;">
                                                                @lang('messages.NumberOfStarterDevices') :
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <span class="float-right text-success mt--1 b"
                                                                style="font-size:13px">
                                                                @lang('messages.Available') : {{ $availableStarterLicence }}
                                                            </span>
                                                            @if ($numberofdevice != 0)
                                                                <input class="form-control" placeholder="Quantity"
                                                                    min="1" name="onStarterdevice" type="number"
                                                                    id="starterdevice" value="{{$numberofdevice}}">
                                                            @else
                                                                <input class="form-control" placeholder="Quantity"
                                                                    min="1" name="onStarterdevice" type="number"
                                                                    id="starterdevice">
                                                            @endif
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="row" id='starterplus'>
                                                    @if (session('isAuthenticated') == 'yes')
                                                        <div class="col-md-5">
                                                            <label for="starterPlusDevice" style="padding-top: 5%;">
                                                                @lang('messages.NumberOfStarterPlusDevices') :
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <span class="float-right text-success mt--1 b"
                                                                style="font-size:13px">
                                                                @lang('messages.Available') : {{ $availableStarterPlusLicence }}
                                                            </span>
                                                           <input class="form-control" placeholder="Quantity"
                                                                    min="1" name="onStarterPlusdevice" type="number"
                                                                    id="starterplusdevice">
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="row" id='basic'>
                                                    <div class="col-md-5">
                                                        <label for="basicdevice" style="padding-top: 5%;">
                                                            @lang('messages.NumberOfBasicDevices') :
                                                        </label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <span class="float-right text-success mt--1 b"
                                                            style="font-size:13px">
                                                            @lang('messages.Available') : {{ $availableBasicLicence }}
                                                        </span>
                                                        @if ($numberofdevice != 0)
                                                            <input class="form-control" placeholder="Quantity" min="1"
                                                                name="onBasicdevice" type="number" id="basicdevice"
                                                                value="{{$numberofdevice}}">
                                                        @else
                                                            <input class="form-control" placeholder="Quantity" min="1"
                                                                name="onBasicdevice" type="number" id="basicdevice">
                                                        @endif

                                                    </div>
                                                </div>
                                                <div class="row" id='advance'>
                                                    <div class="col-md-5">
                                                        <label for="advancedevice" style="padding-top: 5%;">
                                                            @lang('messages.NumberOfAdvanceDevices') :</label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <span class="float-right text-success mt--1 b"
                                                            style="font-size:13px">
                                                            @lang('messages.Available') : {{ $availableAdvanceLicence }}
                                                        </span>
                                                        <input class="form-control" placeholder="Quantity" min="1"
                                                            name="onAdvancedevice" type="number" id="advancedevice">
                                                    </div>
                                                </div>
                                                <div class="row" id='premium'>
                                                    <div class="col-md-5">
                                                        <label for="premiumdevice" style="padding-top: 5%;">
                                                            @lang('messages.NumberOfPremiumDevices') :</label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <span class="float-right text-success mt--1 b"
                                                            style="font-size:13px">
                                                            @lang('messages.Available') : {{ $availablePremiumLicence }}
                                                        </span>
                                                        <input class="form-control" placeholder="Quantity" min="1"
                                                            name="onPremiumdevice" type="number" id="premiumdevice">
                                                    </div>
                                                </div>
                                                <div class="row" id='premiumplus'>
                                                    <div class="col-md-5">
                                                        <label for="premiumPlusdevice" style="padding-top: 5%;">
                                                            @lang('messages.NumberOfPremiumPlusDevices') :</label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <span class="float-right text-success mt--1 b"
                                                            style="font-size:13px">
                                                            @lang('messages.Available') : {{ $availablePremPlusLicence }}
                                                        </span>
                                                        <input class="form-control" placeholder="Quantity" min="1"
                                                            name="onPremiumPlusdevice" type="number"
                                                            id="premiumplusdevice">
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div><br />
                                    <div class="row">
                                        <div class="mx-auto">
                                            {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary', 'id' => 'sub']) }}
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script type="text/javascript">
        function LicenceType(input) {
            let type = $(input).val().toLowerCase();
            $('#starter,#starterplus,#basic,#advance,#premium,#premiumplus').hide();
            $('#starterdevice,#starterplusdevice,#basicdevice,#advancedevice,#premiumdevice,#premiumplusdevice').removeAttr('required');
            $('#' + type).show();
            $('#' + type + 'device').attr({
                'required': 'required'
            });
        }
        $('#LicenceType').on('change', function() {
            LicenceType(this);
        });
        $(function() {
            LicenceType('#LicenceType')
        })
    </script>
@endpush
