@include('includes.header_index')
<!-- Main Wrapper -->
<div id="wrapper">
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <h4>
                            Tags Create
                            <h4>
                    </div>
                    <div class="panel-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div class="row">
                                <div class="col-sm-12">
                                    {{ HTML::ul($errors->all()) }}
                                    {{ Form::open(['url' => 'Business/adddevice']) }}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="row">

                                                    {{ Form::hidden('numberofdevice1', $numberofdevice, ['class' => 'form-control']) }}

                                                    <h5>
                                                        <font color="green">
                                                            {{ Form::label('Business', 'BUSINESS :') }}
                                                        </font>
                                                    </h5>
                                                    <font color="blue">

                                                        <table>
                                                            <tr>
                                                                <td id="hide">
                                                                    {{ Form::radio('type', 'Move', ['required']) }}
                                                                </td>
                                                                <td width=20></td>
                                                                <td>Batch Move</td>
                                                                <td width=20></td>
                                                                <td id="p1">
                                                                    {{ Form::select('dealerId', $dealerId, ['class' => 'form-control']) }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="show">
                                                                    {{ Form::radio('type', 'Sale', ['required']) }}
                                                                </td>
                                                                <td width=20></td>
                                                                <td>Batch Sale</td>

                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                        <br>
                                                        </p>
                                                        <table id="p">
                                                            <tr>
                                                                <td id="hide1">{{ Form::radio('type1', 'new') }}</td>
                                                                <td width=20></td>
                                                                <td>New</td>
                                                                <td width=20></td>
                                                                <td id="p1">
                                                            <tr>
                                                                <td id="show1">{{ Form::radio('type1', 'existing') }}
                                                                </td>
                                                                <td width=20></td>
                                                                <td>Existing</td>

                                                                <td></td>
                                                            </tr>
                                                        </table>


                                                        <table>
                                                            <tr>

                                                                <td id="t">
                                                                    {{ Form::select('userIdtemp', $userList, ['class' => 'form-control']) }}

                                                                </td>
                                                                <td id="t1">
                                                                    <p>
                                                                    <div class="row">

                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                {{ Form::label('userId', 'User ID') }}
                                                                            </div>
                                                                            <div class="col-md-9">
                                                                                {{ Form::text('userId', request()->old('userId'), ['class' => 'form-control']) }}
                                                                            </div>
                                                                        </div>
                                                                        </br>
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                {{ Form::label('mobileNo', 'Mobile Number') }}
                                                                            </div>
                                                                            <div class="col-md-9">
                                                                                {{ Form::text('mobileNo', request()->old('mobileNo'), ['class' => 'form-control']) }}
                                                                            </div>
                                                                        </div>
                                                                        </br>
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                {{ Form::label('email', 'Email') }}
                                                                            </div>
                                                                            <div class="col-md-9">
                                                                                {{ Form::text('email', request()->old('email'), ['class' => 'form-control']) }}
                                                                            </div>
                                                                        </div>
                                                                        </br>
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                {{ Form::label('password', 'Password') }}
                                                                            </div>
                                                                            <div class="col-md-9">
                                                                                {{ Form::text('password', request()->old('password'), ['class' => 'form-control']) }}
                                                                            </div>
                                                                        </div>



                                                                    </div>

                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </table>




                                                        <span id="error" style="color:red;font-weight:bold"></span>




                                                        <table class="table table-bordered dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th style="text-align: center;">No</th>
                                                                    <th style="text-align: center;">Device ID</th>
                                                                    <th style="text-align: center;">Device Type</th>
                                                                    <th style="text-align: center;">Vehicle Id</th>
                                                                    <th style="text-align: center;">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>


                                                                @for ($i = 1; $i <= $numberofdevice; $i++)
                                                                    <tr style="text-align: center;">
                                                                        <td>{{ $i }}

                                                                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js">
                                                                            </script>


                                                                            <script>
                                                                                $(document).ready(function() {
                                                                                        $("#td{{ $i }}")
                                                                                            .hide();
                                                                                        $('#deviceid{{ $i }}')
                                                                                            .on('change',
                                                                                                function() {
                                                                                                    console.log(
                                                                                                        'ahan1');
                                                                                                    var data = {
                                                                                                        'id': $(this).val(),
                                                                                                        '_token': $('input[name="_token"]').val()

                                                                                                    };
                                                                                                    console.log(
                                                                                                        'ahan' +
                                                                                                        data);
                                                                                                    $.post('{{ route('ajax.checkDevice') }}',
                                                                                                        data,
                                                                                                        function(
                                                                                                            data,
                                                                                                            textStatus,
                                                                                                            xhr
                                                                                                        ) {
                                                                                                            /*optional stuff to do after success */
                                                                                                            // console.log('ahan ram'+data.rfidlist);
                                                                                                            $('#error')
                                                                                                                .text(
                                                                                                                    data
                                                                                                                    .error
                                                                                                                );
                                                                                                        });
                                                                                                    //alert("The text has been changeded.");
                                                                                                });



                                                                                        $('#submitp').on('onclick',
                                                                                            function() {
                                                                                                console.log(
                                                                                                    'ahan1');
                                                                                                // var data = {
                                                                                                // 'id': $(this).val(),
                                                                                                // '_token':$('input[name="_token"]').val()
                                                                                                // };
                                                                                                // console.log('ahan'+data);
                                                                                                // $.post('{{ route('ajax.checkDevice') }}', data, function(data, textStatus, xhr) {
                                                                                                // /*optional stuff to do after success */
                                                                                                // // console.log('ahan ram'+data.rfidlist);
                                                                                                // $('#error').text(data.error);
                                                                                                // });
                                                                                                //alert("The text has been changeded.");
                                                                                            });



                                                                                        $('#vehicleId{{ $i }}')
                                                                                            .on('change',
                                                                                                function() {
                                                                                                    console.log(
                                                                                                        'ahan1');
                                                                                                    var data = {
                                                                                                        'id': $(this).val(),
                                                                                                        '_token': $('input[name="_token"]').val();
                                                                                                    };
                                                                                                    console.log(
                                                                                                        'ahan' +
                                                                                                        data);
                                                                                                    $.post('{{ route('ajax.checkvehicle') }}',
                                                                                                        data,
                                                                                                        function(
                                                                                                            data,
                                                                                                            textStatus,
                                                                                                            xhr
                                                                                                        ) {
                                                                                                            /*optional stuff to do after success */
                                                                                                            // console.log('ahan ram'+data.rfidlist);
                                                                                                            $('#error')
                                                                                                                .text(
                                                                                                                    data
                                                                                                                    .error
                                                                                                                );
                                                                                                        });
                                                                                                    //alert("The text has been changeded.");
                                                                                                });

                                                                                        $("#refData{{ $i }}")
                                                                                            .click(function() {
                                                                                                $("#td{{ $i }}")
                                                                                                    .toggle(
                                                                                                        500);
                                                                                            });


                                                                                        $('#submitp').click(
                                                                                            function() {
                                                                                                console.log(
                                                                                                    'ahan1');
                                                                                                // var data = {
                                                                                                // 'id': $(this).val(),
                                                                                                // '_token':$('input[name="_token"]').val()

                                                                                                // };
                                                                                                // console.log('ahan'+data);
                                                                                                // $.post('{{ route('ajax.checkDevice') }}', data, function(data, textStatus, xhr) {
                                                                                                // /*optional stuff to do after success */
                                                                                                // // console.log('ahan ram'+data.rfidlist);
                                                                                                // $('#error').text(data.error);
                                                                                                // });
                                                                                                //alert("The text has been changeded.");
                                                                                            });

                                                                                        $("#hide").click(
                                                                                            function() {
                                                                                                $("#p").hide();
                                                                                                $("#p1").show();
                                                                                                $("#t").hide();
                                                                                                $("#t1").hide();
                                                                                                $('#hide').attr(
                                                                                                    'disabled',
                                                                                                    true);
                                                                                            });
                                                                                        $("#show").click(
                                                                                            function() {
                                                                                                $("#p").show();
                                                                                                $("#p1").hide();
                                                                                                $('#show').attr(
                                                                                                    'disabled',
                                                                                                    true);
                                                                                            });
                                                                                        $("#hide1").click(
                                                                                            function() {
                                                                                                $("#t").hide();
                                                                                                $("#t1").show();
                                                                                                $('#hide1')
                                                                                                    .attr(
                                                                                                        'disabled',
                                                                                                        true);
                                                                                            });
                                                                                        $("#show1").click(
                                                                                            function() {
                                                                                                $("#t").show();
                                                                                                $("#t1").hide();
                                                                                                $('#show1')
                                                                                                    .attr(
                                                                                                        'disabled',
                                                                                                        true);
                                                                                            });

                                                                                        $("#p").hide();
                                                                                        $("#p1").hide();
                                                                                        $("#t").hide();
                                                                                        $("#t1").hide();

                                                                                    }


                                                                                );
                                                                            </script>





                                                                        </td>
                                                                        <td>{{ Form::text('deviceid' . $i, request()->old('deviceid'), ['id' => 'deviceid' . $i, 'required']) }}
                                                                        </td>
                                                                        <td>{{ Form::select('deviceidtype' . $i, ['GT06N' => 'GT06N (9964)', 'FM1202' => 'FM1202 (9975)', 'FM1120' => 'FM1120 (9975)', 'TR02' => 'TR02 (9965)', 'GT03A' => 'GT03A (9969)', 'VTRACK2' => 'VTRACK2 (9964)', 'ET01' => 'ET01 (9971)', 'ET02' => 'ET02 (9962)', 'ET03' => 'ET03 (9974)'], request()->old('deviceidtype'), ['class' => 'form-control']) }}
                                                                        </td>
                                                                        <td>{{ Form::text('vehicleId' . $i, request()->old('vehicleId'), ['id' => 'vehicleId' . $i]) }}
                                                                        </td>
                                                                        <td>
                                                                            <a id="refData{{ $i }}"
                                                                                class="btn btn-sm btn-success">Details</a>
                                                                        </td>
                                                                    </tr>


                                                                    <tr>
                                                                        <td id="td{{ $i }}" colspan="6">
                                                                            <div>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>{{ Form::label('shortName', 'Short Name') }}

                                                                                            {{ Form::text('shortName' . $i, request()->old('shortName'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                        <td>

                                                                                            {{ Form::label('regNo' . $i, 'Vehicle Registration Number') }}

                                                                                            {{ Form::text('regNo' . $i, request()->old('regNo'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                        <td>
                                                                                            {{ Form::label('vehicleType' . $i, 'Vehicle Type') }}

                                                                                            {{ Form::select('vehicleType' . $i, ['Car' => 'Car', 'Truck' => 'Truck', 'Bus' => 'Bus'], request()->old('vehicleType'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td>
                                                                                            {{ Form::label('overSpeedLimit' . $i, 'OverSpeed Limit') }}

                                                                                            {{ Form::select('overSpeedLimit' . $i, ['60' => '60', '70' => '70', '80' => '80', '90' => '90', '100' => '100', '110' => '110', '120' => '120', '130' => '130', '140' => '140', '150' => '150'], request()->old('overSpeedLimit'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                        <td>
                                                                                            {{ Form::label('morningTripStartTime' . $i, 'Morning Trip Start Time') }}

                                                                                            {{ Form::text('morningTripStartTime' . $i, request()->old('morningTripStartTime'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                        <td>
                                                                                            {{ Form::label('orgId' . $i, 'org/College Name') }}
                                                                                        </td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            {{ Form::label('eveningTripStartTime' . $i, 'Evening Trip Start Time') }}

                                                                                            {{ Form::text('eveningTripStartTime' . $i, request()->old('eveningTripStartTime'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                        <td>
                                                                                            {{ Form::label('oprName' . $i, 'Telecom Operator Name') }}

                                                                                            {{ Form::select('oprName' . $i, ['airtel' => 'airtel', 'reliance' => 'reliance', 'idea' => 'idea'], request()->old('oprName'), ['class' => 'form-control']) }}

                                                                                        </td>
                                                                                        <td>
                                                                                            {{ Form::label('mobileNo' . $i, 'Mobile Number for Alerts') }}

                                                                                            {{ Form::text('mobileNo' . $i, request()->old('mobileNo'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td>
                                                                                            {{ Form::label('odoDistance' . $i, 'Odometer Reading') }}

                                                                                            {{ Form::text('odoDistance' . $i, request()->old('odoDistance'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                        <td>
                                                                                            {{ Form::label('driverName' . $i, 'Driver Name') }}

                                                                                            {{ Form::text('driverName' . $i, request()->old('driverName'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                        <td>

                                                                                            {{ Form::label('gpsSimNo' . $i, 'GPS Sim Number') }}

                                                                                            {{ Form::text('gpsSimNo' . $i, request()->old('gpsSimNo'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            {{ Form::label('email' . $i, 'Email for Notification') }}

                                                                                            {{ Form::text('email' . $i, request()->old('email'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                        <td>{{ Form::label('fuel' . $i, 'Fuel') }}

                                                                                            {{ Form::select('fuel' . $i, ['no' => 'No', 'yes' => 'Yes'], request()->old('fuel'), ['class' => 'form-control']) }}
                                                                                        </td>

                                                                                        <td>{{ Form::label('isRfi' . $i, 'IsRfid') }}

                                                                                            {{ Form::select('isRfid' . $i, ['no' => 'No', 'yes' => 'Yes'], request()->old('isRfid'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>{{ Form::label('altShort' . $i, 'Alternate Short Name') }}

                                                                                            {{ Form::text('altShortName' . $i, request()->old('altShortName'), ['class' => 'form-control']) }}
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>


                                                                @endfor


                                                            </tbody>
                                                        </table>




                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    </div>
                                                    <div class="col-md-6">
                                                        {{ Form::submit('Submit', ['id' => 'submitp'], ['id' => 'submitp']) }}{{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        @include('includes.js_index')
        </body>

        </html>
