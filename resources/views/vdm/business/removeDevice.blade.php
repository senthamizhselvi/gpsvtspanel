@extends("layouts.app")
@section('content')
<div class="page-inner">
    <div id="wrapper">
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="page-header">
                            <h4 class="page-title">
                                @lang('messages.RemoveDevice')
                            </h4>
                            <ul class="breadcrumbs">
                                <li class="nav-home">
                                    <a href="#">
                                        <i class="flaticon-home"></i>
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="flaticon-right-arrow"></i>
                                </li>
                                <li class="nav-item">
                                    <a href="#">
                                        @lang('messages.RemoveDevice')
                                    </a>
                                </li>
                            </ul>
                        </div>
                        @if (session()->has('message'))
                            <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                                {{ session()->get('message') }}
                            </p>
                        @endif
                        <div class="card">
                            <span class="text-danger error-message"> {{ HTML::ul($errors->all()) }}</span>
                            <div class="panel-body py-5">
                                <br>
                                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap no-footer">
                                    {{ Form::open(['url' => 'Remove']) }}
                                    <div class="row">
                                        <div class="col-4  d-flex align-items-center justify-content-center">
                                            {{ Form::label('numberofdevice', __('messages.NumberOfDevicestoberemoved') . ' :', ['class' => 'font-weight-bold']) }}
                                        </div>
                                        <div class="col-4 d-flex align-items-center">
                                            {{ Form::number('numberofdevice', old('numberofdevice'), ['class' => 'form-control', 'placeholder' => 'Quantity', 'min' => '1', 'required']) }}
                                            {{ Form::hidden('availableLincence', $availableLincence, ['class' => 'form-control']) }}
                                        </div>
                                        <div class="col-2">
                                            {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
