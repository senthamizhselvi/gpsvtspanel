@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            @if (session()->has('message'))
                <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                    {{ session()->get('message') }}</p>
            @endif
            <div class="row">
                <div class="col-12">
                    <div class="hpanel">
                        <div class="page-header">
                            <h4 class="page-title">
                                @lang('messages.DeviceList')                      
                            </h4>
                            <ul class="breadcrumbs">
                                <li class="nav-home">
                                    <a href="#">
                                        <i class="flaticon-home"></i>
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="flaticon-right-arrow"></i>
                                </li>
                                <li class="nav-item">
                                    <a href="#">
                                        @lang('messages.DeviceList')
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="page-body">
                            <div class="card animated zoomIn ">
                                <div class="card-body">
                                    <div class="overflow-auto">
                                        <table id="example1" class="table table-bordered dataTable table-head-bg-info ">
                                            <thead>
                                                <tr>
                                                    @if (session()->get('cur1') == 'prePaidAdmin')
                                                        <th style="text-align: center;">
                                                            @lang('messages.LicenceID')
                                                        </th>
                                                    @endif
                                                    <th style="text-align: center;">
                                                        @lang('messages.DeviceID')
                                                    </th>
                                                    <th style="text-align: center;">
                                                        @lang('messages.VehicleId')
                                                    </th>
                                                    <th style="text-align: center;">
                                                        @lang('messages.DealerName')
                                                    </th>
                                                    <th style="text-align: center;">
                                                        @lang('messages.LicenceIssuedDate')
                                                    </th>
                                                    @if (session()->get('cur1') == 'prePaidAdmin')
                                                        <th style="text-align: center;">
                                                            @lang('messages.Type')
                                                        </th>
                                                    @endif
                                                    <th style="text-align: center;">
                                                        @lang('messages.OnboardDate')
                                                    </th>
                                                    @if (session()->get('cur1') == 'prePaidAdmin')
                                                        <th style="text-align: center;">
                                                            @lang('messages.LicenceExpiry')
                                                        </th>
                                                    @endif
                                                    @if (session()->get('cur1') != 'prePaidAdmin')
                                                        <th style="text-align: center;">
                                                            @lang('messages.VehicleExpiry')
                                                        </th>
                                                        <th style="text-align: center;">
                                                            @lang('messages.MoveVehicle')
                                                        </th>
                                                    @endif
                                                    <th style="text-align: center;">
                                                        @lang('messages.EditVehicle')
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (isset($devicesList))
                                                    @foreach ($devicesList as $key => $value)

                                                        @php
                                                            $refData = json_decode($refDataList[$key]);
                                                            // $licType = $refData->Licence ?? 'Advance';
                                                            $licType = $refData->Licence ?? '-';
                                                            $own = $refData->OWN ?? "-";
                                                            $date = $refData->date ?? "";
                                                            $date = trim($date);
                                                            if ($date !=="") {
                                                                $date = date("d-m-Y", strtotime($date));
                                                            }
                                                            $onDate = $refData->onboardDate ?? $date;
                                                            $onDate = trim($onDate);
                                                            $onboardDate = ($onDate == "null" || $onDate == "") ? $date : $onDate;
                                            				$licenceRef	= json_decode($licenceRefList[$key]);
                                                			$vehicleExpiry = $refData->vehicleExpiry ?? '';


                                                            if ($prepaid == 'yes') {
                                                                $licenceissuedDate = $licenceRef->LicenceissuedDate ?? '-';
				                                                $licenceExpiryDate = $licenceRef->LicenceExpiryDate ?? '-';
                                                            }else {
                                                                $licenceissuedDate = $refData->licenceissuedDate ?? "-";
                                                            }

                                                            $licenceId =  $licenceIdList[$key] ?? "-";
                                                            $vehicleId = $vehicleList[$key] ?? "-";
                                                        @endphp
                                                        <tr style="text-align: center;">
                                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                                <td>{{ $licenceId }}</td>
                                                            @endif
                                                            <td>{{ $value }}</td>
                                                            <td>{{ $vehicleId }}</td>
                                                            <td>{{ $own }}</td>
                                                            <td>{{ $licenceissuedDate }}</td>
                                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                                <td>{{ $licType }}</td>
                                                            @endif
                                                            <td>{{ $onboardDate }}</td>
                                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                                <td>{{ $licenceExpiryDate }}</td>
                                                            @endif
                                                            @if (session()->get('cur1') != 'prePaidAdmin')
                                                                <td>{{ $vehicleExpiry }}</td>
                                                                <td>
                                                                    <a class="btn btn-sm btn-danger"
                                                                        href="{{ URL::to('vdmVehicles/move_vehicle/' . $vehicleId) }}">
                                                                        @lang('messages.MoveVehicle')
                                                                    </a>
                                                                </td>
                                                            @endif
                                                            <td>
                                                                <a class="btn btn-sm btn-warning"
                                                                    href="{{ URL::to('vdmVehicles/edit/'.$vehicleId) }}">
                                                                    @lang('messages.EditVehicle')
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @push('js')
        <script>
            // $(function(){
            //     $('#example').DataTable( {
            //         "ajax": "data/arrays.txt"
            //     } );
            // })
           /* $(document).ready(function() {
                let val = document.getElementsByTagName('th');
                let data = [],
                    i = 0,
                    temp = [];
                for (item of val) {
                    temp[i] = item.innerHTML;
                    data[i] = {
                        data: item.innerHTML,
                        name: item.innerHTML
                    };
                    i++;
                }
                console.log(temp)
                $('#deviceTable').DataTable({
                    'oLanguage': {
                        'sProcessing': "<img src='{{asset("assets/imgs/ajax-load.gif")}}'>"
                    },
                    'processing': true,
                    'serverSide': true,
                    'serverMethod': 'post',
                    'ajax': {
                        'data': {
                            "_token": "{{ csrf_token() }}",
                        },
                        'url': '{{ route('ajax.deviceList') }}',
                    },
                    'columns': data
                });
            });*/
        </script>
    @endpush
