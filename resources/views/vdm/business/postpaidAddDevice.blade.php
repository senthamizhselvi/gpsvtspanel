@extends("layouts.app")
@section('content')
<div class="page-inner">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="page-header">
                    <h4 class="page-title">@lang('messages.AddDevice')</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('Device.index') }}">@lang('messages.Business')</a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">@lang('messages.AddDevice')</a>
                        </li>
                    </ul>
                </div>
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif
                <div class="page-body">
                    <div class="card animated zoomIn">
                        <span class="text-danger error-message"> {{ HTML::ul($errors->all()) }}</span>
                        <div class="card-body py-5">
                            <h4>
                                <span style="color:#196481">@lang('messages.AvailableLicence') :
                                    {{ $availableLincence }}
                                </span>
                            </h4>
                            <div class="row">
                                <div class="col-sm-12">
                                    {{ Form::open(['url' => 'Business', 'id' => 'form']) }}
                                    <div class="row">
                                        <div class="col-md-3 d-flex align-items-center">
                                            {{ Form::label('numberofdevice', __('messages.NumberOfDevicestobeadded'), ['class' => 'b']) }}
                                        </div>
                                        <div class="col-md-4 d-flex align-items-center">
                                            {{ Form::number('numberofdevice', old('numberofdevice'), ['class' => 'form-control', 'placeholder' => __('messages.Quantity'), 'min' => '1', 'required' => 'required']) }}
                                            {{ Form::hidden('availableLincence', $availableLincence, ['class' => 'form-control']) }}
                                        </div>
                                        <div class="col-md-4 mt-4 mt-md-0 text-center text-md-left">
                                            {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary px-3 py-2']) }}
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
