@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            @if (session()->has('message'))
                <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                    {{ session()->get('message') }}</p>
            @endif
            <div class="row">
                <div class="col-12">
                    <div class="hpanel">
                        <div class="page-header">
                            <h4 class="page-title">
                                @lang('messages.DeviceList')                      
                            </h4>
                            <ul class="breadcrumbs">
                                <li class="nav-home">
                                    <a href="#">
                                        <i class="flaticon-home"></i>
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="flaticon-right-arrow"></i>
                                </li>
                                <li class="nav-item">
                                    <a href="#">
                                        @lang('messages.DeviceList')
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="page-body">
                            <div class="card animated zoomIn ">
                                <div class="card-body">
                                    <div class="overflow-auto">
                                        <table id="example1" class="table table-bordered dataTable table-head-bg-info ">
                                            <thead>
                                                <tr>
                                                    @if (session()->get('cur1') == 'prePaidAdmin')
                                                        <th style="text-align: center;">
                                                            @lang('messages.LicenceID')
                                                        </th>
                                                    @endif
                                                    <th style="text-align: center;">
                                                        @lang('messages.DeviceID')
                                                    </th>
                                                    <th style="text-align: center;">
                                                        @lang('messages.VehicleId')
                                                    </th>
                                                    <th style="text-align: center;">
                                                        @lang('messages.DealerName')
                                                    </th>
                                                    <th style="text-align: center;">
                                                        @lang('messages.LicenceIssuedDate')
                                                    </th>
                                                    @if (session()->get('cur1') == 'prePaidAdmin')
                                                        <th style="text-align: center;">
                                                            @lang('messages.Type')
                                                        </th>
                                                    @endif
                                                    <th style="text-align: center;">
                                                        @lang('messages.OnboardDate')
                                                    </th>
                                                    @if (session()->get('cur1') == 'prePaidAdmin')
                                                        <th style="text-align: center;">
                                                            @lang('messages.LicenceExpiry')
                                                        </th>
                                                    @endif
                                                    @if (session()->get('cur1') != 'prePaidAdmin')
                                                        <th style="text-align: center;">
                                                            @lang('messages.VehicleExpiry')
                                                        </th>
                                                        <th style="text-align: center;">
                                                            @lang('messages.MoveVehicle')
                                                        </th>
                                                    @endif
                                                    <th style="text-align: center;">
                                                        @lang('messages.EditVehicle')
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (isset($deviceMap))
                                                    @foreach ($deviceMap as $key => $value)
                                                        <tr style="text-align: center;">
                                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                                <td>{{ explode(',', $value)[7] }}</td>
                                                            @endif
                                                            <td>{{ explode(',', $value)[1] }}</td>
                                                            <td>{{ explode(',', $value)[0] }}</td>
                                                            <td>{{ explode(',', $value)[2] }}</td>
                                                            <td>{{ explode(',', $value)[3] }}</td>
                                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                                <td>{{ explode(',', $value)[6] }}</td>
                                                            @endif
                                                            <td>{{ explode(',', $value)[4] }}</td>
                                                            @if (session()->get('cur1') == 'prePaidAdmin')
                                                                <td>{{ explode(',', $value)[8] }}</td>
                                                            @endif
                                                            @if (session()->get('cur1') != 'prePaidAdmin')
                                                                <td>{{ explode(',', $value)[5] }}</td>
                                                                <td>
                                                                    <a class="btn btn-sm btn-danger"
                                                                        href="{{ URL::to('vdmVehicles/move_vehicle/' . $value) }}">
                                                                        @lang('messages.MoveVehicle')
                                                                    </a>
                                                                </td>
                                                            @endif
                                                            <td>
                                                                <a class="btn btn-sm btn-warning"
                                                                    href="{{ URL::to('vdmVehicles/edit/' . explode(',', $value)[0]) }}">
                                                                    @lang('messages.EditVehicle')
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @push('js')
        <script>
           /* $(document).ready(function() {
                let val = document.getElementsByTagName('th');
                let data = [],
                    i = 0,
                    temp = [];
                for (item of val) {
                    temp[i] = item.innerHTML;
                    data[i] = {
                        data: item.innerHTML,
                        name: item.innerHTML
                    };
                    i++;
                }
                console.log(temp)
                $('#deviceTable').DataTable({
                    'oLanguage': {
                        'sProcessing': "<img src='{{asset("assets/imgs/ajax-load.gif")}}'>"
                    },
                    'processing': true,
                    'serverSide': true,
                    'serverMethod': 'post',
                    'ajax': {
                        'data': {
                            "_token": "{{ csrf_token() }}",
                        },
                        'url': '{{ route('ajax.deviceList') }}',
                    },
                    'columns': data
                });
            });*/
        </script>
    @endpush
