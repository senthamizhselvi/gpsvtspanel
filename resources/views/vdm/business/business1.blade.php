@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animate-panel page-inner">
            <style>
                .table-bordered th, .table-bordered td {
                    border: 1px solid #dee2e6;
                }
            </style>
            <div class="hpanel">
                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.DashBoard')
                    </h4>
                </div>
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif
               
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="b">
                                @lang('messages.BatchSale')
                            </h5>
                        </div>
                        <div class="card-body">
                            <span class="text-danger  error-message">
                                {{ HTML::ul($errors->all()) }}
                            </span>
                            {{ Form::open(['id' => 'fromSub']) }}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3 form-check">
                                        <label class="form-radio-label" id="hide1">
                                            <input class="form-radio-input" type="radio" name="userType" value="new">
                                            <span class="form-radio-sign">&nbsp;&nbsp;
                                                @lang('messages.NewUser')
                                            </span>
                                        </label>
                                    </div>
                                    <div class="col-md-3 form-check">
                                        <label class="form-radio-label" id="show1">
                                            <input class="form-radio-input" type="radio" name="userType" value="existing"
                                                id="onexist" checked>
                                            <span class="form-radio-sign">
                                                &nbsp;&nbsp;
                                                @lang('messages.ExistingUser')
                                            </span>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div>
                                            {{ Form::submit(__('messages.Submit'), ['class' => 'btn  btn-primary text-white', 'id' => 'subSmt']) }}
                                        </div>
                                    </div>

                                </div>
                                <div id="t" class="">
                                    {{-- <br> --}}
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-3">
                                            {{ Form::label('ExistingUser', __('messages.ExistingUser')) }}
                                            {{ Form::select('userIdtemp', $userList, 'select', ['id' => 'valSelected', 'class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true']) }}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::label('Group', __('messages.Groupname')) }}
                                            {{ Form::select('groupname', ['' => 'Please Select'], old('groupname'), ['id' => 'groupname', 'class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true']) }}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::label('orgId', __('messages.Org/CollegeName')) }}
                                            {{ Form::select('orgId', array_merge(['' => 'Please Select'], $orgList), old('orgId'), ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true']) }}
                                        </div>
                                    </div>
                                </div>
                                <div id="t1">
                                    {{-- <br> --}}
                                    <hr>
                                    <div class="row my-4 mx-5">
                                        <div class="col">
                                            {{Form::label(__('messages.UserName'))}}
                                            {{ Form::text('userId', old('userId'), ['id' => 'userIdtempNew', 'placeholder' => __('messages.UserName'), 'class' => 'form-control userIdtempNew', 'onkeyup' => 'caps(this)']) }}
                                        </div>
                                        <div class="col">
                                            {{Form::label(__('messages.MobileNumber'))}}
                                            {{ Form::number('mobileNo', old('mobileNo'), ['placeholder' => __('messages.MobileNumber'), 'class' => 'form-control']) }}
                                        </div>
                                        <div class="col">
                                            {{Form::label(__('messages.Email'))}}
                                            {{ Form::email('email', old('email'), ['placeholder' => __('messages.Email'), 'class' => 'form-control']) }}
                                        </div>
                                        <div class="col">
                                            {{Form::label(__('messages.Password'))}}
                                            {{ Form::text('password', old('password'), ['placeholder' => __('messages.Password'), 'class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>


                                <hr id="h2">
                                <div class="row " id="m">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-radio-label" id="excel1">
                                            <input class="form-radio-input" type="radio" name="type3" value="excel"
                                                checked="">
                                            <span class="form-radio-sign">&nbsp;&nbsp;
                                                @lang('messages.UploadDetails')
                                            </span>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-radio-label" id="de1">
                                            <input class="form-radio-input" type="radio" name="type3" value="detail"
                                                id="ondetails" checked>
                                            <span class="form-radio-sign">&nbsp;&nbsp;
                                                @lang('messages.DeviceDetails')
                                            </span>
                                        </label>
                                    </div>

                                </div>

                                <div Id="n1">
                                    <hr>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-2">
                                            <a href="{{ URL::to('downloadDealerExcel/xls') }}">
                                                <button class="btn btn-sm btn-primary" id="dexcel">
                                                    @lang('messages.DownloadTemplate')
                                                </button>
                                            </a>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="import_file" id="Upload">
                                                <label class="custom-file-label" for="import_file" id="fileLabel">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="margin-left: 30px;">
                                            <a class="btn btn-sm btn-info text-white" id="import">
                                                @lang('messages.IMPORTEXCEL')
                                            </a>
                                        </div>
                                    </div>
                                    <br>
                                </div>


                                <span id="error" style="color:red;font-weight:bold"></span>
                                <div class="row" id="tab">
                                    <div class="col-md-12">
                                        <hr>
                                        <br>
                                        <table id="business1" class="table table-bordered  table-head-bg-info">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center;">
                                                        @lang('messages.ID')
                                                    </th>
                                                    <th style="text-align: center;">
                                                        @lang('messages.Choose')
                                                    </th>
                                                    <th style="text-align: center;">
                                                        @lang('messages.DeviceID')
                                                    </th>
                                                    <th style="text-align: center;">
                                                        @lang('messages.DeviceType')
                                                    </th>
                                                    <th style="text-align: center;">
                                                        @lang('messages.Action')
                                                    </th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (isset($devices))
                                                    @foreach ($devices as $key => $value)
                                                        <tr class="tetxt-center">
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>
                                                                <div class="form-check">
                                                                    <label class="form-check-label">
                                                                        {{ Form::checkbox('vehicleList[]', $devices[$key] . ',' . $devicestypes[$key], null, ['class' => 'form-check-input field']) }}
                                                                        <span class="form-check-sign"></span>
                                                                    </label>
                                                                </div>
                                                            </td>

                                                            <td>{{ Arr::get($devices, $key) }}</td>
                                                            <td>{{ Arr::get($devicestypes, $key) }}</td>
                                                            <td>
                                                                <a class="btn btn-sm btn-success showDetails" data-count="{{$key}}">
                                                                    @lang('messages.Details')
                                                                </a>
                                                            </td>
                                                            {{-- <td>
                                                                <a class="btn btn-sm btn-success"
                                                                    href="{{ URL::to('vdmVehicles/create/' . $devices[$key]) }}">
                                                                    @lang('messages.Details')
                                                                </a>

                                                            </td> --}}

                                                        </tr>
                                                        <tr id="td{{ $key }}" class="showDetailsRow animated fadeIn">
                                                            <td>
                                                                <span class="d-none">{{$key+1+0.5}}</span>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    {{ Form::text('shortName[]', old('shortName[]'), ['class' => 'form-control', 'placeholder' => __('messages.VehicleName')]) }}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    {{ Form::select('vehicleType[]', $vehicleModalList, old('vehicleType[]'), ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true', 'style' => 'width:100%']) }}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    {{ Form::number('gpsSimNo[]', old('gpsSimNo[]'), ['class' => 'form-control', 'placeholder' => __('messages.GPSsimNo')]) }}
                                                                </div>
                                                            </td>
                                                            <td></td>
                                                           
                                                        </tr>
                                                    @endforeach
                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function caps(element) {
            element.value = element.value.toUpperCase();
        }
        function isExistingUser(){
        value = $('[name="isExistingUser"]:checked').val()
        $('.newUser').attr('required',value == 'new')
        $('.ExistingUser').attr('required',value == 'existing')
    }
    isExistingUser()
        $(document).ready(function() {
            $("#business1").DataTable();
            $('.showDetailsRow').hide()
            $(".showDetails").click(function() {
                id = $(this).attr('data-count');
                $("#td"+id).toggle(100);
            });

            $("#hide").click(function() {
                $("#p,#t,#t1").hide();
                $("#p1").show();
                $('#hide').attr('disabled', true);
                isExistingUser()
            });
            $("#show").click(function() {
                $("#p1").hide();
                $("#p").show();
                $('#show').attr('disabled', true);
                isExistingUser()
            });


            $("#hide1").click(function() {
                $("#t").hide();
                $("#h2,#t1,#m").show();
                $('#hide1').attr('disabled', true);

                isExistingUser()
            });
            $("#show1").click(function() {
                $("#t1").hide();
                $("#t,#h2,#m").show();
                $('#show1').attr('disabled', true);
                isExistingUser()
            });

            $("#excel1").click(function() {
                $("#tab").hide();
                $("#n1,n2").show();
                $("#subSmt").val("IMPORT EXCEL");
                document.getElementById('excel1').checked = true;
                document.getElementById('de1').checked = false;
                $('#subSmt').hide();
                $('#import').show();
                isExistingUser()
            });

            $("#de1").click(function() {
                $("#tab").show();
                $("#n1").hide();
                $("#subSmt").val('SUBMIT');
                document.getElementById('de1').checked = true;
                document.getElementById('excel1').checked = false;
                $('#import').hide();
                $('#subSmt').show();
                isExistingUser()
            });



            $("#t1").hide(); //newuser 
            $("#n1").hide();

            $('#valSelected').on('change', function() {
                var data = {
                    'id': $(this).val(),
                    '_token': $("input[name='_token']").val()
                };
                $.post('{{ route('ajax.getGroup') }}', data, function(data, textStatus, xhr) {

                    $('#groupname').empty();
                    if (data.error != ' ') {
                        wornigAlert(data.error);
                    }
                    $.each(data.groups, function(key, value) {
                        $('#groupname')
                            .append($("<option></option>")
                                .attr("value", key)
                                .text(value));
                    });
                    $('#groupname').selectpicker('refresh');
                });
            });


            $('#userIdtempNew').on('change', function() {
                removeAfterErr(this)
                let input = this;
                var data = {
                    'id': $(input).val(),
                    '_token': $("input[name='_token']").val()
                };

                $.post('{{ route('ajax.checkUser') }}', data, function(data, textStatus, xhr) {

                    if (data.error.trim() != '') {
                        $(input).aftererr(data.error)
                        wornigAlert(data.error);
                    }
                });
            });
        });

        $("#dexcel").click(function() {
            var formAction = 'downloadDealerExcel/xls';
            $('#fromSub').attr('action', formAction);
            //submit form
            $('#fromSub').submit();
        });

        $("#import").click(function() {
            var formAction1 = 'import/DealerExcel';
            var enc = 'multipart/form-data';
            $('#fromSub').attr('action', formAction1);
            $('#fromSub').attr('enctype', enc);
            $('#fromSub').submit();
            var formAction = 'Business/batchSale';
            $('#fromSub').attr('action', formAction);
        });
        $("#subSmt").click(function() {

            var formAction = 'Business/batchSale';
            $('#fromSub').attr('action', formAction);
            //submit form
            $('#fromSub').submit();
        });
    </script>
@endpush
