@extends("layouts.app")
@section('content')
<div id="wrapper">
    <style>
        .form-control {
            height: auto !important;
        }
    </style>
    <div class="content page-inner">
        <div class="hpanel">
            <div class="page-header">
                <h4 class="page-title">@lang('messages.AddBusiness')</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('Device.index') }}">@lang('messages.Business')</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('Device.index') }}">@lang('messages.AddDevice')</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">@lang('messages.AddBusiness')</a>
                    </li>
                </ul>
            </div>
            <div class="page-body">
                <div class="card animated fadeIn">
                    <div class="card-header">
                        <h6>
                            {{ Form::label('Business', __('messages.BUSINESS')) }}
                        </h6>
                    </div>
                    <div class="card-body">
                        <span class="text-danger error-message">
                            {{ HTML::ul($errors->all()) }}
                        </span>
                        {{ Form::open(['id' => 'submit']) }}
                        <span id="error" style="color:red;font-weight:bold"></span>
                        <div class="col-md-12">
                            {{ Form::hidden('numberofdevice', $numberofdevice) }}
                            {{ Form::hidden('licenceType', $licenceType) }}

                        </div>
                        <div style="display: none;">
                            {{ Form::radio('dealerOrUser', 'Sale', ['value' => 'true', 'id' => 'saleenable']) }}</div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-8" id="p">
                                <div class="row mb-2 text-sm-center mx-0">
                                    <div class="col-md-4 col form-check"
                                        style="color: #16564b;font-size: 15px; font-weight: bold;">
                                        <label class="form-radio-label" id="hide1">
                                            <input class="form-radio-input" type="radio" name="isExistingUser" value="new" @if (old('isExistingUser') == "new") checked  @endif>
                                            <span class="form-radio-sign">&nbsp;&nbsp; @lang('messages.NewUser')</span>
                                        </label>

                                    </div>
                                    <div class="col-md-4  col form-check"
                                        style="color: #16564b;font-size: 15px; font-weight: bold;">
                                        <label class="form-radio-label" id="show1">
                                            <input class="form-radio-input" type="radio" name="isExistingUser" value="existing" @if (old('isExistingUser') == "existing") checked  @endif>
                                            <span class="form-radio-sign">&nbsp;&nbsp;
                                                @lang('messages.ExistingUser')</span>
                                        </label>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="col-md-12 animated fadeIn" id="t">
                            <br>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3 form-group">
                                    {{ Form::label('ExistingUser', __('messages.ExistingUser')) }} <br>
                                    {{ Form::select('userIdtemp', $userList, 'select', ['id' => 'ExistingUser', 'class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true', 'style' => 'width:100%']) }}
                                </div>
                                <div class="col-md-3 form-group">
                                    {{ Form::label('Group', __('messages.GroupName')) }} <br>
                                    {{ Form::select('groupname', ['' => __('messages.select')], old('groupname'), ['id' => 'groupname', 'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true', 'style' => 'width:100%']) }}
                                </div>
                                <div class="col-md-3 form-group">
                                    {{ Form::label('orgId', __('messages.OrganizationName')) }} <br>
                                    {{ Form::select('orgId', $orgList, old('orgId'), ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true', 'style' => 'width:100%']) }}
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="col-md-12" id="t1">
                            <br>
                            <div class="row mb-3 animated fadeIn " >
                                <div class="col-md-3 form-group">
                                    {{Form::label(__('messages.UserName'))}}
                                    {{ Form::text('userId', old('userId'), ['id' => 'NewUserId', 'class' => 'form-control caps newUser', 'placeholder' => __('messages.UserName')]) }}
                                </div>
                                <div class="col-md-3 form-group">
                                    {{Form::label(__('messages.MobileNumber'))}}
                                    {{ Form::text('mobileNoUser', old('mobileNoUser'), ['class' => 'form-control newUser', 'placeholder' => __('messages.MobileNumber')]) }}
                                </div>
                                <div class="col-md-3 form-group">
                                    {{Form::label(__('messages.Email'))}}
                                    {{ Form::Email('emailUser', old('emailUser'), ['class' => 'form-control newUser', 'placeholder' => __('messages.Email')]) }}
                                </div>
                                <div class="col-md-3 form-group">
                                    {{Form::label(__('messages.Password'))}}
                                    {{ Form::text('password', old('password'), ['class' => 'form-control newUser', 'placeholder' => __('messages.Password')]) }}
                                </div>
                                <hr id="h2" style="width: 100%;">
                            </div>
                        </div>
                        <div class="col-md-12" id="m">
                            <div class="row mb-3 animated fadeIn" >
                                <div class="col-3"></div>
                                <div class="col-4 form-check" style="color: #2e0b4c;font-size: 15px; font-weight: bold;">
                                    <label class="form-radio-label" id="excel1">
                                        <input class="form-radio-input" type="radio" name="type3" value="excel">
                                        <span class="form-radio-sign">&nbsp;&nbsp;@lang('messages.UploadDevices')</span>
                                    </label>
                                </div>
                                <div class="col-4 form-check" style="color: #2e0b4c;font-size: 15px; font-weight: bold;">
                                    <label class="form-radio-label" id="de1">
                                        <input class="form-radio-input" type="radio" name="type3" value="detail"
                                            id="ondetails" checked>
                                        <span class="form-radio-sign">&nbsp;&nbsp;@lang('messages.DeviceDetails')</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row animated fadeIn mt-2" Id="n1">
                            {{-- <br> --}}
                            <div class="col-md-2"></div>
                            <div class="col-md-2 mt-sm-2">
                                <a href="{{ URL::to('downloadExcel/xls') }}">
                                    <button class="btn btn-sm btn-primary"   id="dexcel">
                                        @lang('messages.DownloadTemplate')
                                    </button>
                                </a>
                            </div>

                            <div class="col-md-4 mt-sm-2">
                                <div class="custom-file">
                                    <input type="file" name="import_file" class="custom-file-input" id="Upload"
                                    accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" >
                                    <label class="custom-file-label" for="customFile"
                                        id="fileLabel">@lang('messages.ChooseaFile')
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2 mt-sm-3 ml-md-2 text-center" >
                                <a class="btn btn-sm "
                                    style="background-color:#099ad7;border-color: #099ad7; color:#ffffff;"
                                    id="import">
                                    @lang('messages.ImportExcel')
                                </a>
                            </div>


                        </div>
                        <br>
                        <div class="row animated fadeIn" id="details">
                            <div class="col-md-1"></div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-head-bg-info" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th
                                                    style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">
                                                    @lang('messages.No')</th>
                                                <th
                                                    style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">
                                                    @lang('messages.LicenceId')(<?php echo $Licence; ?>)</th>
                                                <th
                                                    style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">
                                                    @lang('messages.DeviceId')</th>
                                                <th
                                                    style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">
                                                    @lang('messages.DeviceType')</th>
                                                <th
                                                    style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">
                                                    @lang('messages.VehicleId')</th>
                                                <th
                                                    style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">
                                                    @lang('messages.Action')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @for ($i = 1; $i <= $numberofdevice; $i++)
                                                
                                                <tr style="text-align: center;">
                                                    <td>{{ $i }}</td>
                                                    <td>{{ Form::text('LicenceId[]', $LicenceId[$i], ['id' => 'LicenceId' . $i, 'class' => 'form-control', 'readonly' => 'true']) }}
                                                    </td>
                                                    <td class="batchSale">
                                                        {{ Form::text('deviceid[]', old('deviceid[]'), ['id' => 'deviceid' . $i, 'class' => 'form-control batchSale caps deviceid' , 'placeholder' =>__('messages.deviceId')]) }}
                                                    </td>
                                                    <td class="batchMove">
                                                        {{ Form::text('deviceidSale[]', $devices[$i], ['id' => 'deviceidSale' . $i, 'class' => 'form-control batchMove deviceidSale']) }}
                                                    </td>
                                                    <td>{{ Form::select('deviceidtype[]', $protocol, old('deviceidtype'), ['class' => 'form-control selectpicker']) }}
                                                    </td>
                                                    <td class="batchSale">
                                                        {{ Form::text('vehicleId[]', old('vehicleId[]'), ['id' => 'vehicleId' . $i, 'class' => 'form-control batchSale caps vehicleId', 'placeholder' => __('messages.VehicleId')]) }}
                                                    </td>
                                                    <td class="batchMove">
                                                        {{ Form::text('vehicleIdSale[]', $vehicles[$i], ['id' => 'vehicleIdSale' . $i, 'class' => 'form-control batchMove caps vehicleIdSale']) }}
                                                    </td>
    
                                                    <td>
                                                        <a class="btn btn-sm btn-success showDetails" data-count="{{$i}}">
                                                            @lang('messages.Details')
                                                        </a>
                                                    </td>
                                                </tr>
    
                                                <tr id="td{{ $i }}" class="showDetailsRow animated fadeIn">
                                                    <td></td>
                                                    <td>
                                                        <div class="form-group">
                                                            {{ Form::text('shortName[]', old('shortName[]'), ['class' => 'form-control', 'placeholder' => __('messages.VehicleName')]) }}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            {{ Form::select('vehicleType[]', $vehicleModalList, old('vehicleType[]'), ['class' => 'selectpicker show-menu-arrow form-control', 'data-live-search ' => 'true', 'style' => 'width:100%']) }}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            {{ Form::number('gpsSimNo[]', old('gpsSimNo[]'), ['class' => 'form-control', 'placeholder' => __('messages.GPSsimNo')]) }}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        {{ Form::hidden('Licence', $Licence, ['class' => 'form-control', 'readonly' => 'true']) }}
                                                    </td>
                                                </tr>
                                            @endfor
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr id="h2" style="width: 100%;">
                        </div>
                        <div class="row">
                            <div class="col-md-2 mx-auto text-center">
                                {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary', 'id' => 'Subtest']) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    var alerttest;
    $(document).ready(function() {
            $(".showDetailsRow").hide();
            $('.deviceid').on('change', function() {
                removeAfterErr(this)
                let input = this;
                var data = {
                    'id': $(input).val(),
                    '_token': $('input[name="_token"]').val()
                };
                var splChars =
                    /[ !@#$%^&*() +\=\[\]{};':"\\|,.<>\/?]/;
                if ((data.id).match(splChars)) {
                    alerttest = true;
                    wornigAlert('Please Enter valid Id');
                    return false;
                }
                $.post('{{ route('ajax.checkDevice') }}',data,
                    function(data, textStatus,xhr) {
                        if (data.error.trim() != '') {
                            $(input).aftererr(data.error);
                            wornigAlert(data.error);
                            alerttest = true;
                        }
                    });
            });

            $('.deviceidSale').on('change', function() {
                removeAfterErr(this)
                let input = this;
                var data = {
                    'id': $(this).val(),
                    '_token': $('input[name="_token"]').val()
                };
                var splChars =
                    /[ !@#$%^&*() +\=\[\]{};':"\\|,.<>\/?]/;
                if ((data.id).match(splChars)) {
                    alerttest = true;
                    wornigAlert('Please Enter valid Id');
                    $(this).focus();
                    return false;
                }
                $.post('{{ route('ajax.checkDevice') }}',
                    data,
                    function(data, textStatus,
                        xhr) {
                            if (data.error.trim() != '') {
                            $(input).aftererr(data.error);
                            wornigAlert(data.error);
                            alerttest = true;
                        }
                    });
            });

            $('.vehicleId').on('change', function() {
                removeAfterErr(this)
                let input = this;
                var data = {
                    'id': $(this).val(),
                    '_token': $('input[name="_token"]').val()
                };
                var splChars =
                    /[ !@#$%^&*() +\=\[\]{};':"\\|,.<>\/?]/;
                if ((data.id).match(splChars)) {
                    alerttest = true;
                    wornigAlert('Please Enter valid Id');
                    return false;
                }
                $.post('{{ route('ajax.checkvehicle') }}',
                    data,
                    function(data, textStatus,
                        xhr) {
                        if (data.error != ' ') {
                            $(input).aftererr(data
                                .error);
                            wornigAlert(data.error);
                            alerttest = true;
                        }

                    });
            });

            $('.vehicleIdSale').on('change', function() {
                removeAfterErr(this)
                let input = this;
                var data = {
                    'id': $(this).val(),
                    '_token': $('input[name="_token"]').val()

                };
                var splChars =
                    /[ !@#$%^&*() +\=\[\]{};':"\\|,.<>\/?]/;
                if ((data.id).match(splChars)) {
                    alerttest = true;
                    wornigAlert('Please Enter valid Id');
                    $(this).focus();
                    return false;
                }
                $.post('{{ route('ajax.checkvehicle') }}',
                    data,
                    function(data, textStatus, xhr) {
                        
                        if (data.error != ' ') {
                            $(input).aftererr(data
                                .error);
                            wornigAlert(data.error);
                            alerttest = true;
                        }

                    });
            });
            $('#ExistingUser').on('change', getGroupList);
            getGroupList()
            function getGroupList() {
                $userName = $("#ExistingUser").val();
                if($userName == "select") return
                var data = {
                    'id': $userName,
                    '_token': $('input[name="_token"]').val()
                };
               
                $.post('{{ route('ajax.getGroup') }}', data,
                    function(data, textStatus, xhr) {
                        $('#groupname').empty();
                        if (data.error.trim() != '') {
                            wornigAlert(data.error);
                        }
                        $.each(data.groups, function(key,value) {
                            $('#groupname')
                                .append(
                                    $("<option></option>")
                                    .attr("value",key)
                                    .text(value)
                                );
                        });
                        // $('#groupname').selectpicker('refresh');
                    });
            }

            $('#NewUserId').on('change', function() {
                removeAfterErr(this)
                let input = this;
                $('#error').text('');
                var data = {
                    'id': $(this).val(),
                    '_token': $('input[name="_token"]').val()
                };
                console.log('ahan' + data);
                $.post('{{ route('ajax.checkUser') }}', data,
                    function(data, textStatus, xhr) {
                        if (data.error.trim() != '') {
                            $(input).aftererr(data.error);
                            wornigAlert(data.error);
                        }
                    });
            });

            $(".showDetails").click(function() {
                id = $(this).attr('data-count');
                $("#td"+id).toggle(100);
            });

        }
    );
</script>
<script>
    $("#Subtest").click(function() {
        var formAction = "{{route('Business/adddevice')}}";
        $('#submit').attr('action', formAction);
    });

    $("#import").click(function() {

        var formAction1 = "{{route('import/Excel')}}";
        // var excel = $("#Upload").value;
        var enc = 'multipart/form-data';
        //enctype="multipart/form-data"
        $('#submit').attr('action', formAction1);
        $('#submit').attr('enctype', enc);
        $('#submit').submit();
    });


    $("#dexcel").click(function() {
        var formAction = "{{route('downloadExcel/xls')}}";
        $('#submit').attr('action', formAction);
        //submit form
        $('#submit').submit();
    });


    $("#hide").click(function() {
        $("#p").hide();
        $("#p1").show();
        $('#m').show();
        $("#t").hide();
        $("#t1").hide();
        $('#hide').attr('disabled', true);
        $('.batchSale').hide();
        $('.batchMove').show();
        $(".batchMove").prop('required', true);
        $(".batchSale").prop('required', false);
        $("[name='isExistingUser']").prop('checked',false);
        $('#de1').click();
        var inputUser = $(":input[name=type1]:checked").val();
        $(":input[name=type1]:checked").attr('value', '');
        isExistingUser()

    });
    $("#show").click(function() {
        $('#hr').show();
        $('#m').hide();
        $('#n1').hide();
        $("#details").hide();
        $("#h1").hide();
        $("#p").show();
        $("#p1").hide();
        $('#show').attr('disabled', true);

        $('.batchSale').show();
        $('.batchMove').hide();
        $(".batchSale").prop('required', true);
        $(".batchMove").prop('required', false);
        // var inputUser = $(":input[name=type1]:checked").val();
        //  $(":input[name=type1]:checked").attr('value', inputUser);
        //document.getElementById("Subtest").value="SUBMIT";
        isExistingUser()

    });

    function isExistingUser(){
        value = $('[name="isExistingUser"]:checked').val()
        $('.newUser').attr('required',value == 'new')
        $('.ExistingUser').attr('required',value == 'existing')
    }
    isExistingUser()
    $(document).on('click',isExistingUser)

    $("#excel1").click(function() {
        $("#n1").show();
        $('#h2').show();
        $("#details").hide();
        $("#Subtest").value = "IMPORT EXCEL";
        $('#Subtest').hide();
        $('#import').show();
        isExistingUser()
    });

    $("#de1").click(function() {
        $('#de1').checked = true;
        $('#excel1').checked = false;
        $("#details").show();
        $("#n1").hide();
        // $("#Subtest").value = "{{__('Submit')}}";
        $('#import').hide();
        $('#Subtest').show();
        isExistingUser()

    });
    $("#hide1").click(function() {
        $("#t").hide();
        $("#t1,#h1,#h2,#m").show();
        $('#de1').click();
        //document.getElementById('NewUserId').required=true;
        isExistingUser()
    });


    $("#show1").click(function() {
        $("#t,#h1,#h2,#m").show();
        $("#t1").hide();
        $('#de1').click();
        $('#show1').attr('disabled', true);
        document.getElementById('NewUserId').required = "";
        isExistingUser()
    });

    
    
    // When the user clicks anywhere outside of the modal, close it
    $(function() {
        $("#p,#p1,#t,#t1,#m,#n1,#details,#h1,#h2,#import,#Subtest,#hr").hide();
        $('.batchMove').hide();
        $('#hr,#p').show();
        $('.batchSale').show();
        // $("#Subtest").val("{{__('Submit')}}");
    })

  
</script>
@endpush
