@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="page-header">
                            <h4 class="page-title">
                                @lang('messages.RemoveDevice')
                            </h4>
                            <ul class="breadcrumbs">
                                <li class="nav-home">
                                    <a href="#">
                                        <i class="flaticon-home"></i>
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="flaticon-right-arrow"></i>
                                </li>
                                <li class="nav-item">
                                    <a href="#">
                                        @lang('messages.RemoveDevice')
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="page-body">
                            @if (session()->has('message'))
                                <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                                    {{ session()->get('message') }}
                                </p>
                            @endif
                            <div class="card animated zoomIn pb-5">
                                {{ Form::open(['url' => 'Remove/removedevices', 'id' => 'removedevicesform']) }}
                                {{ HTML::ul($errors->all()) }}
                                {{ Form::hidden('LicenceType', $LicenceType, ['class' => 'form-control', 'id' => 'ltype']) }}
                                {{ Form::hidden('numberofdevice', $numberofdevice, ['class' => 'form-control']) }}
                                <div class="col-md-12">
                                </div>
                                <div class="card-header">
                                    <span  class="h3">
                                        @lang('messages.Business')
                                    </span>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-6 mx-auto">
                                        <div class="form-check d-flex justify-content-around">
                                            <label class="form-radio-label" id="hide">
                                                <input class="form-radio-input" type="radio" name="type" value="Device">
                                                <span class="form-radio-sign">
                                                    @lang('messages.DeviceID')
                                                </span>
                                            </label>
                                            <label class="form-radio-label" id="show">
                                                <input class="form-radio-input" type="radio" name="type" value="Vehicle"
                                                    checked>
                                                <span class="form-radio-sign">
                                                    @lang('messages.VehicleID')
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary']) }}
                                    </div>

                                </div>

                                {{-- <br>
                                <div class="row px-5" id="p1">
                                    <div class="col-md-12 ">
                                        <table class="table table-head-bg-info table-bordered-bd-info">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center;">No</th>
                                                    <th style="text-align: center;">@lang('messages.DeviceID')</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @for ($i = 1; $i <= $numberofdevice; $i++)
                                                    <tr>
                                                        <td>{{ $i }}</td>
                                                        <td>{{ Form::text('deviceid[]', old('deviceid'), ['id' => 'deviceid' . $i, 'class' => 'form-control deviceid', 'placeholder' => __('messages.DeviceID'), 'onchange' => 'deviceId(this)']) }}
                                                        </td>
                                                    </tr>
                                                @endfor
                                            </tbody>
                                        </table>
                                    </div>
                                </div> --}}
                                <br>
                                <div class="row px-5" id="p">
                                    <div class="col-md-12 ">
                                        <table class="table table-head-bg-info table-bordered-bd-info">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center;">No</th>
                                                    <th style="text-align: center;" class="header">@lang('messages.VehicleID')</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @for ($i = 1; $i <= $numberofdevice; $i++)
                                                    <tr>
                                                        <td>{{ $i }}</td>
                                                        <td class="text-left">{{ Form::text('deviceIds[]', old('deviceIds[]'), ['id' => 'vehicleId' . $i, 'class' => 'form-control vehicleId field', 'placeholder' => __('messages.VehicleID'), 'onchange' => 'deviceId(this)']) }}
                                                        </td>
                                                    </tr>

                                                @endfor
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>

        function deviceId(input) {
            $(input).removeClass('is-invalid');
            $('.aftererr').remove();
            if($(input).val() == "") return
            $(':submit').attr('disabled', false);
            var data = {
                'id': $(input).val(),
                'licene': $('#ltype').val(),
                '_token': $('input[name="_token"]').val()
            };
            type = $('[name="type"]:checked').val();
            route = {
                'Device' : '{{ route('ajax.checkDevice1') }}',
                'Vehicle' : '{{ route('ajax.checkvehicle1') }}'
            }
            $.post(route[type],
                data,
                function(data, textStatus, xhr) {

                    if (data.error != ' ') {
                        $(input).aftererr(data.error);
                        $(':submit').attr('disabled', true);
                    }
                });
        }

        // function vehicleId(input) {
        //     $(input).removeClass('is-invalid');
        //     $('.aftererr').remove();
        //     if($(input).val() == "") return
        //     $(':submit').attr('disabled', false);
        //     var data = {
        //         'id': $(input).val(),
        //         'licene': $('#ltype').val(),
        //         '_token': $('input[name="_token"]').val()
        //     };
        //     debugger
        //     $.post(,
        //         data,
        //         function(data, textStatus, xhr) {
        //             if (data.error != ' ') {
        //                 $(input).aftererr(data.error);
        //                 $(':submit').attr('disabled', true);
        //             }
        //         });
        // }
        $('#removedevicesform').submit(function() {
            let valid = true;
            // valid = formValidation("#removedevicesform",valid);
            type = $('[name="type"]:checked').val();
            $(':submit').attr('disabled', valid);
            return valid
        })
        $('[name="type"]').on('change',function(){
            type = $('[name="type"]:checked').val();
            deviceLabel = "{{__('messages.DeviceID')}}"
            vehicleLabel = "{{__('messages.VehicleID')}}"
            label={
                'Device' : deviceLabel,
                'Vehicle' : vehicleLabel
            }
            onchange={
                'Device' : 'deviceId(this)',
                'Vehicle' : 'vehicleId(this)'
            }

            $('.header').html(label[type])
            $('.field').attr({
                'placeholder':label[type],
                // 'onchange' : onchange[type]
            })
           
        })
        // $("#hide").click(function() {
        //     $("#p").hide();
        //     $("#p1").show();
        //     $('.deviceid').attr('required', true);
        //     $('.vehicleId').removeAttr('required')
        // });
        // $("#show").click(function() {
        //     $("#p").show();
        //     $("#p1").hide();
        //     $('.deviceid').removeAttr('required');
        //     $('.vehicleId').attr('required', true);
        // });

        $(function() {
            // $("#p1").hide();
            // $("#p").show();
            // $('.vehicleId').attr('required', true);
        })
    </script>
@endpush
