@extends("layouts.app")
@section('content')
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">
                @lang('messages.DashBoard')
            </h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">
                        @lang('messages.DashBoard')
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">{{ $fname }}</a>
                </li>
            </ul>
        </div>
        <div class="page-body">
            <div class="row row-card-no-pd mt--2">
                <div class="col-sm-6 col-md-3">
                    <div class="card card-stats card-round">
                        <div class="card-body ">
                            <div class="row mx-0">
                                <div class="col-5">
                                    <div class="icon-big text-center">
                                        <i class="fas fa-user-friends text-danger"></i>
                                    </div>
                                </div>
                                <div class="col-7 col-stats">
                                    <div class="numbers">
                                            <p class="
                                            card-category">
                                                @lang('messages.Users')
                                            </p>
                                        <h4 class="card-title text-center">{{ $users_count }}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="card card-stats card-round">
                        <div class="card-body ">
                            <div class="row mx-0">
                                <div class="col-5">
                                    <div class="icon-big text-center">
                                        <i class="fas fas fa-user text-danger"></i>
                                    </div>
                                </div>
                                <div class="col-7 col-stats">
                                    <div class="numbers">
                                        <p class="card-category">@lang('messages.Dealers')</p>
                                        <h4 class="card-title text-center">{{ $dealer_count }}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="card card-stats card-round">
                        <div class="card-body">
                            <div class="row mx-0">
                                <div class="col-5">
                                    <div class="icon-big text-center">
                                        <i class="fas fa-users text-danger"></i>
                                    </div>
                                </div>
                                <div class="col-7 col-stats">
                                    <div class="numbers">
                                        <p class="card-category">@lang('messages.Groups')</p>
                                        <h4 class="card-title text-center">{{ $Group_count }} </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="card card-stats card-round">
                        <div class="card-body">
                            <div class="row mx-0">
                                <div class="col-5">
                                    <div class="icon-big text-center">
                                        <i class="fas fa-truck text-danger"></i>
                                    </div>
                                </div>
                                <div class="col-7 col-stats">
                                    <div class="numbers">
                                        <p class="card-category">@lang('messages.Vehicles')</p>
                                        <h4 class="card-title text-center">{{ $vehicleCnt }} </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="d-flex flex-column">
                        <h1 class="text-primary">{{ $fname }}</h1>
                        <span class="b">@lang('messages.Website') : <a href="https://{{ $website }}" target="_blank"> {{ $website }} </a></span>
                        {{-- <span class="b">{{ $address }}</span> --}}
                    </div>
                </div>
                <div class="card-body">
                    <div class="row mx-0">

                        @if (session()->get('isAuthenticated') == 'yes')
                            <div class="col-sm-6 col px-2 mt-2 detail-card">
                                <div class="flip-card">
                                    <div class="flip-card-inner">
                                        <div class="flip-card-front">

                                            <h5 class="h4 my-1">
                                                @lang('messages.StarterPlus')
                                            </h5>

                                            <h2 class="text-center text-primary">
                                                {{ $numberofStarterPlusLicence }}</h2>
                                            <div class="row mx-0">
                                                <div class="col-6 px-1">
                                                    <h5>
                                                        @lang('messages.Used')
                                                    </h5>
                                                    <p class="text-primary">
                                                        {{ $numberofStarterPlusLicence - $availableStarterPlusLicence }}
                                                    </p>
                                                </div>
                                                <div class="col-6 px-1">
                                                    <h5>@lang('messages.Available')</h5>
                                                    <p class="text-primary">
                                                        {{ $availableStarterPlusLicence }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flip-card-back">

                                            <h5 class="h4 my-1">@lang('messages.UsedDetails')</h5>

                                            @if (session()->get('cur') == 'admin')
                                                <div class="row mx-0">
                                                    <div class="col-md-12 mx-auto">
                                                        <span class="h3 text-primary">
                                                            {{ $sp_dealer_tol }}
                                                        </span> <br>
                                                        <span class="h6">@lang('messages.Dealers')</span>
                                                    </div>
                                                </div>
                                            @else
                                                <h2 style="color: #0c25d6; text-align: center;">
                                                    {{ $numberofStarterPlusLicence - $availableStarterPlusLicence }}
                                                </h2>
                                            @endif
                                            <div class="row mx-0">
                                                <div class="col-6 px-1">
                                                    <h5>@lang('messages.Renew')</h5>
                                                    <p class="text-primary">{{ $sp_renew_count }}
                                                    </p>
                                                </div>
                                                <div class="col-6 px-1">
                                                    <h5>@lang('messages.Onboard')</h5>
                                                    <p class="text-primary">
                                                        {{ $sp_onboard_count }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if (session()->get('enableStarter') == 'yes')
                            <div class="col-sm-6 col px-2 mt-2 detail-card">
                                <div class="flip-card">
                                    <div class="flip-card-inner">
                                        <div class="flip-card-front">

                                            <h5 class="h4 my-1">
                                                @lang('messages.Starter')
                                            </h5>

                                            <h2 class="text-center text-primary">
                                                {{ $numberofStarterLicence }}</h2>
                                            <div class="row mx-0">
                                                <div class="col-6 px-1">
                                                    <h5>
                                                        @lang('messages.Used')
                                                    </h5>
                                                    <p class="text-primary">
                                                        {{ $numberofStarterLicence - $availableStarterLicence }}
                                                    </p>
                                                </div>
                                                <div class="col-6 px-1">
                                                    <h5>@lang('messages.Available')</h5>
                                                    <p class="text-primary">
                                                        {{ $availableStarterLicence }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flip-card-back">

                                            <h5 class="h4 my-1">@lang('messages.UsedDetails')</h5>

                                            @if (session()->get('cur') == 'admin')
                                                <div class="row mx-0">
                                                    <div class="col-md-12 mx-auto">
                                                        <span class="h3 text-primary">
                                                            {{ $st_dealer_tol }}
                                                        </span> <br>
                                                        <span class="h6">@lang('messages.Dealers')</span>
                                                    </div>
                                                </div>
                                            @else
                                                <h2 style="color: #0c25d6; text-align: center;">
                                                    {{ $numberofStarterLicence - $availableStarterLicence }}
                                                </h2>
                                            @endif
                                            <div class="row mx-0">
                                                <div class="col-6 px-1">
                                                    <h5>@lang('messages.Renew')</h5>
                                                    <p class="text-primary">{{ $st_renew_count }}
                                                    </p>
                                                </div>
                                                <div class="col-6 px-1">
                                                    <h5>@lang('messages.Onboard')</h5>
                                                    <p class="text-primary">
                                                        {{ $st_onboard_count }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="col-sm-6 col px-2 mt-2 detail-card">
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">
                                        <h5 class="h4 my-1">
                                            @lang('messages.Basic')
                                        </h5>
                                        <h2 class="text-center text-primary">
                                            {{ $numberofBasicLicence }}</h2>
                                        <div class="row mx-0">
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Used')</h5>
                                                <p class="text-primary">
                                                    {{ $numberofBasicLicence - $availableBasicLicence }}
                                                </p>
                                            </div>
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Available')</h5>
                                                <p class="text-primary">
                                                    {{ $availableBasicLicence }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flip-card-back">

                                        <h5 class="h4 my-1">@lang('messages.UsedDetails')</h5>

                                        @if (session()->get('cur') == 'admin')
                                            <div class="row mx-0">
                                                <div class="col-md-12  mx-auto">
                                                    <span class="h3 text-primary">
                                                        {{ $bc_dealer_tol }}
                                                    </span><br>
                                                    <span class="h6">@lang('messages.Dealers')</span>
                                                </div>
                                            </div>
                                        @else
                                            <h2 style="color: #0c25d6; text-align: center;margin-top: 25%">
                                                {{ $numberofBasicLicence - $availableBasicLicence }}
                                            </h2>
                                        @endif
                                        <div class="row mx-0">
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Renew')</h5>
                                                <p class="text-primary">{{ $bc_renew_count }}
                                                </p>
                                            </div>
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Onboard')</h5>
                                                <p class="text-primary">
                                                    {{ $bc_onboard_count }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col px-2 mt-2 detail-card">
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">

                                        <h5 class="h4 my-1">
                                            @lang('messages.Advance')
                                        </h5>

                                        <h2 class="text-center text-primary">
                                            {{ $numberofAdvanceLicence }}</h2>
                                        <div class="row mx-0">
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Used')</h5>
                                                <p class="text-primary">
                                                    {{ $numberofAdvanceLicence - $availableAdvanceLicence }}
                                                </p>
                                            </div>
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Available')</h5>
                                                <p class="text-primary">
                                                    {{ $availableAdvanceLicence }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flip-card-back">

                                        <h5 class="h4 my-1">@lang('messages.UsedDetails')</h5>

                                        @if (session()->get('cur') == 'admin')
                                            <div class="row mx-0">
                                                <div class="col-md-12 mx-auto">
                                                    <span class="h3 text-primary">
                                                        {{ $ad_dealer_tol }}
                                                    </span><br>
                                                    <span class="h6">@lang('messages.Dealers')</span>
                                                </div>
                                            </div>
                                        @else
                                            <h2 style="color: #0c25d6; text-align: center;margin-top: 25%">
                                                {{ $numberofAdvanceLicence - $availableAdvanceLicence }}
                                            </h2>
                                        @endif
                                        <div class="row mx-0">
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Renew')</h5>
                                                <p class="text-primary">{{ $ad_renew_count }}
                                                </p>
                                            </div>
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Onboard')</h5>
                                                <p class="text-primary">
                                                    {{ $ad_onboard_count }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col px-2 mt-2 detail-card">
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">

                                        <h5 class="h4 my-1">
                                            @lang('messages.Premium')
                                        </h5>

                                        <h2 class="text-center text-primary">
                                            {{ $numberofPremiumLicence }}</h2>
                                        <div class="row mx-0">
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Used')</h5>
                                                <p class="text-primary">
                                                    {{ $numberofPremiumLicence - $availablePremiumLicence }}
                                                </p>
                                            </div>
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Available')</h5>
                                                <p class="text-primary">
                                                    {{ $availablePremiumLicence }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flip-card-back">

                                        <h5 class="h4 my-1">@lang('messages.UsedDetails')</h5>

                                        @if (session()->get('cur') == 'admin')
                                            <div class="row mx-0">
                                                <div class="col-md-12 mx-auto">
                                                    <span class="h3 text-primary">
                                                        {{ $pr_dealer_tol }}
                                                    </span> <br>
                                                    <span class="h6">@lang('messages.Dealers')</span>
                                                </div>
                                            </div>
                                        @else
                                            <h2 style="color: #0c25d6; text-align: center;margin-top: 25%">
                                                {{ $numberofPremiumLicence - $availablePremiumLicence }}
                                            </h2>
                                        @endif
                                        <div class="row mx-0">
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Renew')</h5>
                                                <p class="text-primary">{{ $pr_renew_count }}
                                                </p>
                                            </div>
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Onboard')</h5>
                                                <p class="text-primary">
                                                    {{ $pr_onboard_count }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col px-2 mt-2 detail-card">
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">

                                        <h5 class="h4 my-1">
                                            @lang('messages.PremiumPlus')
                                        </h5>

                                        <h2 class="text-center text-primary">
                                            {{ $numberofPremPlusLicence }}</h2>
                                        <div class="row mx-0">
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Used')</h5>
                                                <p class="text-primary">
                                                    {{ $numberofPremPlusLicence - $availablePremPlusLicence }}
                                                </p>
                                            </div>
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Available')</h5>
                                                <p class="text-primary">
                                                    {{ $availablePremPlusLicence }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flip-card-back">

                                        <h5 class="h4 my-1">@lang('messages.UsedDetails')</h5>

                                        @if (session()->get('cur') == 'admin')
                                            <div class="row mx-0">
                                                <div class="col-md-12 mx-auto">
                                                    <span class="h3 text-primary">
                                                        {{ $prpl_dealer_tol }}
                                                    </span><br>
                                                    <span class="h6">@lang('messages.Dealers')</span>
                                                </div>
                                            </div>
                                        @else
                                            <h2 style="color: #0c25d6; text-align: center;margin-top: 25%">
                                                {{ $numberofPremPlusLicence - $availablePremPlusLicence }}
                                            </h2>
                                        @endif
                                        <div class="row mx-0">
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Renew')</h5>
                                                <p class="text-primary">
                                                    {{ $prpl_renew_count }}
                                                </p>
                                            </div>
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Onboard')</h5>
                                                <p class="text-primary">
                                                    {{ $prpl_onboard_count }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col px-2 mt-2 detail-card">
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">

                                        <h5 class="h4 my-1">
                                            @lang('messages.TotalLicences')
                                        </h5>

                                        <h2 class="text-center text-primary">
                                            {{ $numberofBasicLicence + $numberofAdvanceLicence + $numberofPremiumLicence + $numberofPremPlusLicence }}
                                        </h2>
                                        <div class="row mx-0">
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Used')</h5>
                                                <p class="text-primary">{{ $ttlused }}</p>
                                            </div>
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Available')</h5>
                                                <p class="text-primary">
                                                    {{ $availableBasicLicence + $availableAdvanceLicence + $availablePremiumLicence + $availablePremPlusLicence }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flip-card-back">

                                        <h5 class="h4 my-1">@lang('messages.UsedDetails')</h5>

                                        @if (session()->get('cur') == 'admin')
                                            <div class="row mx-0">
                                                <div class="col-md-12 mx-auto">
                                                    <span class="h3 text-primary">
                                                        {{ $bc_dealer_tol + $ad_dealer_tol + $pr_dealer_tol }}
                                                    </span><br>
                                                    <span class="h6">@lang('messages.Dealers')</span>
                                                </div>
                                            </div>
                                        @else
                                            <h2 style="color: #0c25d6; text-align: center;margin-top: 25%">
                                                {{ $ttlused }}</h2>
                                        @endif
                                        <div class="row mx-0">
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Renew')</h5>
                                                <p class="text-primary">
                                                    {{ $bc_renew_count + $ad_renew_count + $pr_renew_count + $prpl_renew_count }}
                                                </p>
                                            </div>
                                            <div class="col-6 px-1">
                                                <h5>@lang('messages.Onboard')</h5>
                                                <p class="text-primary">
                                                    {{ $bc_onboard_count + $ad_onboard_count + $pr_onboard_count + $prpl_onboard_count }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row p-2">
                        @if (session()->get('cur') == 'admin')
                            <b>
                                <h3>@lang('messages.DealersDetails')</h3>
                            </b>
                            <table id="columnspan" class="table table-hover table-striped dataTable table-head-bg-info">
                                <thead>
                                    <tr>
                                        {{-- <th class="text-center" rowspan="2" style="z-index: 999">
                                            @lang('messages.ID')
                                        </th> --}}
                                        <th class="text-center"  rowspan="2"  style="z-index: 999">
                                            @lang('messages.DealersID')
                                        </th>
                                        @if (session('isAuthenticated') == 'yes')
                                            <th class="text-center" colspan="4">
                                                @lang('messages.NUMBEROFSTARTERPLUSLICENCE')
                                            </th>
                                        @endif
                                        @if (session('enableStarter') == 'yes')
                                            <th class="text-center" colspan="4">
                                                @lang('messages.NUMBEROFSTARTERLICENCE')
                                            </th>
                                        @endif
                                        <th class="text-center" colspan="4">
                                            @lang('messages.NUMBEROFBASICLICENCE')
                                        </th>
                                        <th class="text-center" colspan="4">
                                            @lang('messages.NUMBEROFADVANCELICENCE')
                                        </th>
                                        <th class="text-center" colspan="4">
                                            @lang('messages.NUMBEROFPREMIUMLICENCE')
                                        </th>
                                        <th class="text-center" colspan="4">
                                            @lang('messages.NUMBEROFPREMIUM-PLUSLICENCE')
                                        </th>
                                        <th class="text-center" rowspan="2">
                                            @lang('messages.TOTALNUMBEROFLICENCE')
                                        </th>
                                    </tr>
                                    <tr>
                                        @php
                                            $headerCount = 3;
                                            if(session('isAuthenticated') == 'yes') $headerCount +=1;
                                            if(session('enableStarter') == 'yes') $headerCount +=1;
                                        @endphp
                                        
                                        @for ($i = 0; $i <= $headerCount; $i++)
                                            <th class="text-center">@lang('messages.Used')</th>
                                            <th class="text-center">@lang('messages.Available')</th>
                                            <th class="text-center">@lang('messages.Onboard')</th>
                                            <th class="text-center">@lang('messages.Total')</th>
                                        @endfor
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($dealer != null)
                                        @foreach ($dealer as $key => $value)
                                            <tr class="text-center">
                                                {{-- <td>{{ $key + 1 }}</td> --}}
                                                <td> 
                                                    {{ Form::label('li', $value) }}
                                                </td>
                                                @if (session()->get('isAuthenticated') == 'yes')
                                                    <td> {{ Arr::get($dealerNoSp, $value) - Arr::get($dealerAvlSp, $value) }}
                                                    </td>
                                                    <td> {{ Arr::get($dealerAvlSp, $value) }}</td>
                                                    <td> {{ Arr::get($starterPlusCount, $value) }}</td>
                                                    <td> {{ Arr::get($dealerNoSp, $value) }}</td>
                                                @endif
                                                @if (session()->get('enableStarter') == 'yes')
                                                    <td> {{ Arr::get($dealerNoSt, $value) - Arr::get($dealerAvlSt, $value) }}
                                                    </td>
                                                    <td> {{ Arr::get($dealerAvlSt, $value) }}</td>
                                                    <td> {{ Arr::get($starterCount, $value) }}</td>
                                                    <td> {{ Arr::get($dealerNoSt, $value) }}</td>
                                                @endif
                                                <td> {{ Arr::get($dealerNoBc, $value) - Arr::get($dealerAvlBc, $value) }}
                                                </td>
                                                <td> {{ Arr::get($dealerAvlBc, $value) }}</td>
                                                <td> {{ Arr::get($basicCount, $value) }}</td>
                                                <td> {{ Arr::get($dealerNoBc, $value) }}</td>

                                                <td> {{ Arr::get($dealerNoAd, $value) - Arr::get($dealerAvlAd, $value) }}
                                                </td>
                                                <td> {{ Arr::get($dealerAvlAd, $value) }}</td>
                                                <td> {{ Arr::get($advanceCount, $value) }}</td>
                                                <td> {{ Arr::get($dealerNoAd, $value) }}</td>

                                                <td> {{ Arr::get($dealerNoPr, $value) - Arr::get($dealerAvlPr, $value) }}
                                                </td>
                                                <td> {{ Arr::get($dealerAvlPr, $value) }}</td>
                                                <td> {{ Arr::get($premiumCount, $value) }}</td>
                                                <td> {{ Arr::get($dealerNoPr, $value) }}</td>

                                                <td> {{ Arr::get($dealerNoPrPl, $value) - Arr::get($dealerAvlPrPl, $value) }}
                                                </td>
                                                <td> {{ Arr::get($dealerAvlPrPl, $value) }}</td>
                                                <td> {{ Arr::get($premiumPlusCount, $value) }}</td>
                                                <td> {{ Arr::get($dealerNoPrPl, $value) }}</td>

                                                @if (session()->get('enableStarter') == 'yes')
                                                    <td> {{ Arr::get($dealerNoSt, $value) + Arr::get($dealerNoBc, $value) + Arr::get($dealerNoAd, $value) + Arr::get($dealerNoPr, $value) + Arr::get($dealerNoPrPl, $value) }}
                                                    @else
                                                    <td> {{ Arr::get($dealerNoBc, $value) + Arr::get($dealerNoAd, $value) + Arr::get($dealerNoPr, $value) + Arr::get($dealerNoPrPl, $value) }}
                                                    </td>
                                                @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            {{-- <div id="tabNew" style="overflow-y: auto;">
                            </div> --}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <head>
        @if (session()->get('cur') == 'admin')
            <style>
                .flip-card:hover .flip-card-inner {
                    transform: rotateY(180deg);
                }

            </style>
        @endif
        <style>
            tr.odd>.dtfc-fixed-left{
                background-color: #f0efef !important;
            }
            tr.even>.dtfc-fixed-left{
                background-color: #ffffff !important;
            }
            .flip-card {
                background-color: transparent;
                /* width: 180px; */
                height: 140px;
                perspective: 1000px;
                border-radius: 10px;
            }

            .flip-card-inner {
                position: relative;
                width: 100%;
                height: 100%;
                text-align: center;
                transition: transform 0.6s;
                transform-style: preserve-3d;
                border-radius: 10px;
                box-shadow: 0px 1px 10px 1px rgba(0, 0, 0, 0.2);
            }


            .flip-card-front,
            .flip-card-back {
                position: absolute;
                width: 100%;
                border-radius: 10px;
                height: 100%;
                backface-visibility: hidden;
            }

            .flip-card-front {
                color: black;
                z-index: 2;
            }

            .flip-card-back {
                color: black;
                transform: rotateY(180deg);
                z-index: 1;
            }

        </style>
    </head>

@endsection
@push('js')
    <script src="{{ asset('common/js/fixedCol.js') }}"></script>
    <script>
        $(document).ready(function() {		
            $('#columnspan').dataTable({
                scrollX: true,
                scrollCollapse: true,
                stateSave:true,
                fixedColumns: {
                    left: 1,
                },
                "language": {
				"url": $("#langJson").val()
			    },
            });
        });
    </script>
@endpush
