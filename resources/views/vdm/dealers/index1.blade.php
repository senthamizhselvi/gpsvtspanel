@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            <div class="hpanel">
                <div class="pull-right">
                    <a href="{{ route('dealer/sendExcel') }}"><img class="pull-right" width="40px" height="50px"
                            src="{{ asset('assets/imgs/xls.svg') }}" method="get" alt="xls" /></a>
                </div>
                @if (session()->has('message'))
                    <p n class="alert {{ session()->get('alert-class', 'alert-info') }}">
                        {{ session()->get('message') }}</p>
                @endif

                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.DealerSearch')
                    </h4>
                </div>
                <div class="page-body">
                    <div class="card">

                        <div class="card-header">
                            {{ Form::open(['url' => 'vdmDealersScan/Search', 'method' => 'post']) }}
                            <div class="row p-2">
                                <div class="col-md-6 form-group">
                                    {{ Form::label('Dealer Search') }}
                                    <i class="fa fa-info-circle fa-lg text-warning ml-auto" data-toggle="tooltip"
                                        data-placement="right" title=" Use * for getting all Dealers in the search"></i>
                                    <br>
                                    {{ Form::text('text_word', old('text_word'), ['class' => 'form-control', 'placeholder' => 'Search Dealer']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::submit('Search', ['class' => 'btn btn-primary mt-4']) }}
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                        <div class="card-body" id="tabNew">
                            <table id="example1"
                                class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th>
                                            @lang('messages.ID')
                                        </th>
                                        <th>
                                            @lang('messages.DealerName')
                                        </th>
                                        <th>
                                            @lang('messages.MobileNo')
                                        </th>
                                        <th>
                                            @lang('messages.Website')
                                        </th>
                                        <th>
                                            @lang('messages.Code')
                                        </th>
                                        <th>
                                            @lang('messages.Actions')
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dealerlist as $key => $value)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $value }}</td>
                                            <td>{{ Arr::get($userGroupsArr, $value) }}</td>
                                            <td>{{ Arr::get($dealerWeb, $value) }}</td>
                                            <td>{{ $fcode }}</td>
                                            <td>
                                                <div class="dropdown dropleft">
                                                    <button class="btn btn-info text-white btn-sm dropdown-toggle"
                                                        type="button" id="dropdownMenuButton{{ $key }}"
                                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        @lang('messages.Action')
                                                    </button>
                                                    <div class="dropdown-menu"
                                                        aria-labelledby="dropdownMenuButton{{ $key }}">
                                                        <a class="btn btn-sm btn-success text-white dropdown-item mb-1"
                                                            href="{{ URL::to('vdmDealers/' . $value) }}">
                                                            @lang('messages.ShowDealer')
                                                        </a>
                                                        <a class="btn btn-sm btn-info text-white dropdown-item mb-1"
                                                            href="{{ URL::to('vdmDealers/' . $value . '/edit') }}">
                                                            @lang('messages.EditDealer')
                                                        </a>
                                                        <a class="btn btn-sm btn-secondary text-white dropdown-item mb-1"
                                                            href="{{ URL::to('vdmUsers/reports/' . $value) }}">
                                                            @lang('messages.EditDealerReports')
                                                        </a>
                                                        @if ($enableArr[$value])
                                                            <a class="btn btn-sm btn-warning text-white dropdown-item mb-1"
                                                                href="{{ URL::to('vdmDealers/disable/' . $value) }}">
                                                                @lang('messages.Disable')
                                                            </a>
                                                        @else
                                                            <a class="btn btn-sm btn-success text-white dropdown-item mb-1"
                                                                href="{{ URL::to('vdmDealers/enable/' . $value) }}">
                                                                @lang('messages.Enable')
                                                            </a>
                                                        @endif
                                                        {{ Form::open(['url' => 'vdmDealers/' . $value, 'class' => '', 'id' => 'deleteForm' . $key]) }}
                                                        {{ Form::hidden('_method', 'DELETE') }}
                                                        {{ Form::button(__('messages.DeletethisDealer'), ['class' => 'btn btn-sm btn-danger text-white dropdown-item deleteBtn', 'id' => $key, 'formtype' => 'Dealer']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
