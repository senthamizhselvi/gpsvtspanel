@extends("layouts.app")
@section('content')
    <div>
        <div class="page-inner">
            <div class="page-header">
                <div class="page-title">
                    @lang('messages.AllocateLicence')
                </div>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                            @lang('messages.AllocateLicence')
                        </a>
                    </li>
                </ul>
            </div>
            <div class="page-body">
                @include('vdm.billing.licDetails')
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}</p>
                @endif
                <div class="card">
                    <div class="card-body">
                        <span class="text-danger error-message">
                            {{ HTML::ul($errors->all()) }}
                        </span>
                        {{ Form::open(['url' => 'dealers/licenceUpdate', 'id' => 'sub']) }}
                        {{ Form::hidden('availableStarterPlusLicence', $availableStarterPlusLicence, ['id' => 'avlsp']) }}
                        {{ Form::hidden('availableStarterLicence', $availableStarterLicence, ['id' => 'avlst']) }}
                        {{ Form::hidden('availableBasicLicence', $availableBasicLicence, ['id' => 'avlbc']) }}
                        {{ Form::hidden('availableAdvanceLicence', $availableAdvanceLicence, ['id' => 'avlad']) }}
                        {{ Form::hidden('availablePremiumLicence', $availablePremiumLicence, ['id' => 'avlpr']) }}
                        {{ Form::hidden('availablePremPlusLicence', $availablePremPlusLicence, ['id' => 'avlprpl']) }}
                        <div class="row p-2">
                            <div class="col-md-6 form-group">
                                <label for="dealersList">
                                    @lang('messages.SelectDealer')
                                </label>
                                {{ Form::select('dealersList', $dealersList, null, ['id' => 'dealersList', 'class' => 'form-control show-menu-arrow selectpicker', 'data-live-search' => 'true', 'data-dropup-auto ' => 'false', 'required' => 'required']) }}
                            </div>
                        </div>
                        <hr>
                        <div class="row p-2">
                            <div class="col-md-6 form-group" id="starterPlus" style="display: none">
                                <label for="addofBasic">
                                    @lang('messages.AddStarterPlusLicence')
                                </label>
                                <input class="form-control" placeholder="Quantity" id="addofStarterPlus" min="1"
                                    name="addofStarterPlus" type="number">
                            </div>
                            <div class="col-md-6 form-group" id="starter" style="display: none">
                                <label for="addofStarter">
                                    @lang('messages.AddStarterLicence')
                                </label>
                                <input class="form-control" placeholder="Quantity" id="addofStarter" min="1"
                                    name="addofStarter" type="number">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="addofBasic">
                                    @lang('messages.AddBasicLicence')
                                </label>
                                <input class="form-control" placeholder="Quantity" id="addofBasic" min="1"
                                    name="addofBasic" type="number">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="addofAdvance">
                                    @lang('messages.AddAdvanceLicence')
                                </label>
                                <input class="form-control" placeholder="Quantity" min="1" id="addofAdvance"
                                    name="addofAdvance" type="number">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="addofPremium">
                                    @lang('messages.AddPremiumLicence')
                                </label>
                                <input class="form-control" placeholder="Quantity" min="1" id="addofPremium"
                                    name="addofPremium" type="number">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="addofPremiumPlus">
                                    @lang('messages.AddPremiumPlusLicence')
                                </label>
                                <input class="form-control" placeholder="Quantity" min="1" id="addofPremiumPlus"
                                    name="addofPremiumPlus" type="number">
                            </div>
                            <div class="col-md-12 text-center mt-4">
                                <input class="btn btn-primary mx-auto" id="submit" type="submit" value="Submit">
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('js')
    <script type="text/javascript">
        if (!!window.performance && window.performance.navigation.type === 2) {
            window.location.reload();
        }
        $(document).ready(function() {
            $('#sub').submit(function(e) {
                $('#submit').attr('disabled', true);
            });
            $("#dealersList").on('change', function() {
                var data = {
                    'delaerId': $(this).val(),
                    '_token': $('input[name="_token"]').val()
                };
                $.post('{{ route('ajax.enabledStarterDealer') }}', data)
                    .done(function(data, textStatus, xhr) {
                        if (data['enableStarter'] === "yes") {
                            $('#starter').show();
                        } else {
                            $('#starter').hide();
                        }
                        if (data['authenticated'] === "yes") {
                            $('#starterPlus').show();
                        } else {
                            $('#starterPlus').hide();
                        }
                    });
            });

            $('#sub').on('submit', function(e) {
                var enter = {
                    'st': $('#addofStarter').val(),
                    'sp': $("#addofStarterPlus").val(),
                    'bc': $('#addofBasic').val(),
                    'ad': $('#addofAdvance').val(),
                    'pr': $('#addofPremium').val(),
                    'prpl': $('#addofPremiumPlus').val()
                }
                var avl = {
                    'avlst': $('#avlst').val(),
                    'avlsp': $("#avlsp").val(),
                    'avlbc': $('#avlbc').val(),
                    'avlad': $('#avlad').val(),
                    'avlpr': $('#avlpr').val(),
                    'avlprpl': $('#avlprpl').val()
                }
                if (parseInt(enter.st) > parseInt(avl.avlst)) {
                    e.preventDefault()
                    wornigAlert("Starter Licence type not available");
                    $('#submit').removeAttr('disabled');

                }
                if (parseInt(enter.bc) > parseInt(avl.avlbc)) {
                    e.preventDefault()
                    wornigAlert("Basic Licence type not available");
                    $('#submit').removeAttr('disabled');
                }
                if (parseInt(enter.ad) > parseInt(avl.avlad)) {
                    e.preventDefault()
                    wornigAlert("Advance Licence type not available");
                    $('#submit').removeAttr('disabled');
                }
                if (parseInt(enter.pr) > parseInt(avl.avlpr)) {
                    e.preventDefault()
                    wornigAlert("Premium Licence type not available");
                    $('#submit').removeAttr('disabled');
                }
                if (parseInt(enter.prpl) > parseInt(avl.avlprpl)) {
                    e.preventDefault()
                    wornigAlert("Premium Plus Licence type not available");
                    $('#submit').removeAttr('disabled');
                }
            });
        });
    </script>
@endpush
{{-- @include('includes.js_create') --}}
