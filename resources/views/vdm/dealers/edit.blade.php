@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animate-panel">
            <div class="page-inner">
                <div class="page-header">
                    <div class="page-title">
                        @if (\Request::is('*vdmDealers/editDealer*'))
                            @lang('messages.EditProfile')
                        @else
                            @lang('messages.EditDealer')
                        @endif
                    </div>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        @if (!Request::is('*vdmDealers/editDealer*'))
                            <li class="nav-item">
                                <a href="{{ route('vdmDealers.index') }}">
                                    @lang('messages.DealerList')
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a href="#">
                                @if (\Request::is('*vdmDealers/editDealer*'))
                                    @lang('messages.EditProfile')
                                @else
                                    @lang('messages.EditDealer')
                                @endif
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                {{$dealerid}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card">
                    <div class="hpanel">
                        <div class="card-body">
                            <span class="text-danger error-message">
                                {{ HTML::ul($errors->all()) }}
                            </span>
                            {{ Form::model($dealerid, ['route' => ['vdmDealers.update', $dealerid], 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'id' => 'editForm']) }}
                            {{ Form::hidden('show', $show, ['class' => 'form-control']) }}
                            <h4 class="text-primary">
                                @lang('messages.General')
                            </h4>
                            <hr>
                            <div class="row p-2">
                                <div class="col-md-6 form-group">
                                    {{ Form::label('dealerid', __('messages.DealerName')) }}
                                    {{ Form::label('dealerid', $dealerid, ['class' => 'form-control', 'required' => 'required']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('mobileNo', __('messages.MobileNo')) }} <span
                                        class="rq"></span>
                                    {{ Form::number('mobileNo', $mobileNo, ['class' => 'form-control', 'placeholder' => 'Mobile Number', 'required' => 'required','id'=>'mobileNo', 'minlength' => '8','maxlength'=>'15']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('city', __('messages.City')) }} <span
                                        class="rq"></span>
                                    {{ Form::text('city', $city, ['class' => 'form-control', 'placeholder' => 'City', 'required' => 'required']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('state', __('messages.State')) }} <span
                                        class="rq"></span>
                                    {{ Form::text('state', $state, ['class' => 'form-control', 'placeholder' => 'State', 'required' => 'required']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('address', __('messages.Address')) }} <span
                                        class="rq"></span>
                                    {{ Form::text('address', $address, ['class' => 'form-control', 'placeholder' => 'Enter Full Address', 'required' => 'required']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('email', __('messages.Email')) }} <span class="rq"></span>
                                    {{ Form::email('email', $email, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('subDomain', __('messages.Subdomain(AdminPanelUrl)')) }}
                                    {{ Form::text('subDomain', $subDomain, ['class' => 'form-control', 'placeholder' => 'Subdomain']) }}
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    {{ Form::label('zoho', __('messages.Zoho')) }} <span class="rq"></span>
                                    {{ Form::text('zoho', $zoho, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Zoho']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('website', __('messages.Website')) }} <span
                                        class="rq"></span>
                                    {{ Form::text('website', $website, ['class' => 'form-control', 'placeholder' => 'Website', 'required' => 'required']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('mapKey', __('messages.MapKey/APIKey')) }} <span
                                        class="rq"></span>
                                    {{ Form::text('mapKey', $mapKey, ['id' => 'mapkey', 'class' => 'form-control', 'placeholder' => 'Map Key']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('website1', __('messages.Website1')) }}
                                    {{ Form::text('website1', $website1, ['class' => 'form-control', 'placeholder' => 'Website 1']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('mapKey1', __('messages.MapKey1/APIKey1')) }}
                                    {{ Form::text('mapKey1', $mapKey1, ['class' => 'form-control', 'placeholder' => 'Map Key 1']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('defaultMap', __('messages.DefaultMap')) }}
                                    {{ Form::select('defaultMap',$defaultMapList, $defaultMap, ['class' => 'form-control']) }}
                                </div>                               
                                <div class="col-md-6 form-group">
                                    {{ Form::label('timeZone', __('messages.CountryTimezone')) }}
                                    {{ Form::select('timeZone', $countryTimezoneList, $timeZone, ['class' => 'selectpicker form-control', 'data-live-search ' => 'true']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('languageCode', __('messages.LanguageSelection')) }}
                                    {{ Form::select('languageCode', ['ta' => 'Tamil', 'hi' => 'Hindi', 'en' => 'English', 'es' => 'Spanish'], $languageCode, ['class' => 'selectpicker form-control', 'data-live-search ' => 'true']) }}
                                </div>
                            </div>
                            <br>
                            <h3 class="text-primary">
                                @lang('messages.SMSConfiguration')
                            </h3>
                            <hr>
                            <div class="row p-2">
                                <div class="col-md-6 form-group">
                                    {{ Form::label('smsSender', __('messages.SmsSender')) }}
                                    {{ Form::text('smsSender', $smsSender, ['class' => 'form-control', 'placeholder' => 'Sms Sender']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('smsProvider', __('messages.SMSProvider')) }}
                                    {{ Form::select('smsProvider', $smsP, $smsProvider, ['class' => 'form-control selectpicker', 'id' => 'smsProvider']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('providerUserName', __('messages.SmsProviderUserName')) }}
                                    {{ Form::text('providerUserName', $providerUserName, ['class' => 'form-control', 'placeholder' => 'Provider Name']) }}
                                </div>
                                <div class="col-md-6 form-group" id='textLocalSmsKeyFiled'>
                                    {{ Form::label('textLocalSmsKey', __('messages.TextLocalSmsKey')) }}<span
                                        class="rq"></span>
                                    {{ Form::text('textLocalSmsKey', $textLocalSmsKey, ['class' => 'form-control', 'id' => 'textLocalSmsKey']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('providerPassword', __('messages.SmsProviderPassword')) }}
                                    {{ Form::input('password', 'providerPassword', $providerPassword, ['class' => 'form-control passhover', 'placeholder' => 'Provider Password ', 'id' => 'dealerpass1']) }}
                                </div>
                                <div class="col-md-6 form-group" id="smsEntityNameFiled">
                                    {{ Form::label('smsEntityName', __('messages.SMSEntityName')) }} <span
                                        class="rq"></span>
                                    {{ Form::text('smsEntity', $smsEntity, ['class' => 'form-control', 'placeholder' => 'SMS Entity Name', 'id' => 'smsEntityName']) }}
                                </div>

                            </div>
                            <br>
                            <h4 class="text-primary">
                                @lang('messages.MobileAppConfiguration')
                            </h4>
                            <hr>
                            <div class="row p-2">
                                <div class="col-md-6 form-group">
                                    {{ Form::label('gpsvtsAppKey', __('messages.MobileAppName')) }} <span
                                        class="rq"></span>
                                    <i class="fa fa-info-circle fa-lg text-warning ml-auto" data-toggle="tooltip"
                                        data-placement="right" title="Mobile App Key should be starts with 'com.'"></i> <br>
                                    {{ Form::text('gpsvtsAppKey', $gpsvtsAppKey, ['id' => 'gpsapp', 'class' => 'form-control', 'placeholder' => 'Mobile App Key']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('notificationKey', __('messages.NotificationKey')) }}
                                    {{ Form::text('notificationKey', $notificationKey, ['id' => 'notify', 'class' => 'form-control', 'placeholder' => 'Notification Key']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('addressKey', __('messages.AddressKey')) }}
                                    {{ Form::text('addressKey', $addressKey, ['class' => 'form-control', 'placeholder' => 'Address Key']) }}
                                </div>
                            </div>
                            @if (session()->get('cur1') == 'prePaidAdmin')
                                <br />
                                <h3 class="text-primary">
                                    @lang('messages.LicenceConfiguration')
                                </h3>
                                <hr>
                                <div class="row p-2">
                                    <div class="col-md-6 form-group">
                                        {{ Form::label('enableStarter', __('messages.EnableStarter')) }}
                                        {{ Form::select($enableStarter == 'yes' ? 'enableStarter1' : 'enableStarter', ['no' => 'No', 'yes' => 'Yes'], $enableStarter, ['class' => 'form-control', 'id' => 'enableStarter', $enableStarter == 'yes' ? 'disabled' : '' => '']) }}
                                        {{ Form::input('hidden', $enableStarter == 'yes' ? 'enableStarter' : 'enableStarter1', $enableStarter, ['class' => 'form-control']) }}
                                    </div>
                                    @if ($authenticatedEnable == 'yes')
                                        <div class="col-md-6 form-group">
                                            {{ Form::label('authenticated', __('messages.AuthenticatedDomain')) }}
                                            {{ Form::select($authenticated == 'yes' ? 'authenticated1' : 'authenticated', ['no' => 'No', 'yes' => 'Yes'], $authenticated, ['class' => 'form-control', 'id' => 'authenticated', $authenticated == 'yes' ? 'disabled' : '' => '']) }}
                                            {{ Form::input('hidden', $authenticated == 'yes' ? 'authenticated' : 'authenticated1', $authenticated, ['class' => 'form-control']) }}
                                        </div>
                                    @endif
                                </div>
                                <div class="row p-2 text-center licLabel">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <label for="numofStarterPlus">
                                            @lang('messages.NumberofLicence')
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="avlofBasic">
                                            @lang('messages.AvailableLicence')
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        @if ($show != 'yes')
                                        <label for="addofBasic">
                                            @lang('messages.AddLicence')
                                        </label>
                                        @endif
                                    </div>
                                </div>
                                <div class="row p-2" id="StarterPlus">
                                    <div class="col-md-3 form-group d-flex justify-content-right align-items-center">
                                        <label for="basic" class="b">
                                            @lang('messages.StarterPlus')
                                        </label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" placeholder="Quantity" min="0" readonly
                                            value="{{ $numofStarterPlus }}" name="numofStarterPlus" type="number">
                                    </div>
                                    
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" placeholder="Quantity" min="0" readonly="1"
                                            name="avlofBasic" value="{{ $avlofStarterPlus }}" type="number">
                                    </div>
                                    @if ($show != 'yes')
                                        <div class="col-md-3 form-group">
                                            <input class="form-control lic" placeholder="Quantity" min="0" name="addofStarterPlus"
                                                type="number">
                                        </div>
                                    @endif
                                </div>
                                <div class="row p-2" id="Starter">
                                    @if (session()->get('enableStarter') == 'yes')
                                        <div class="col-md-3 form-group d-flex justify-content-right align-items-center">
                                            <label for="Starter" class="b">
                                                @lang('messages.Starter')
                                            </label>
                                        </div>
                                        <div class="col-md-3 form-group">
                                            
                                            <input class="form-control" placeholder="Quantity" min="0" readonly="1"
                                                value="{{$numofStarter}}" name="numofStarter" type="number">
                                        </div>
                                        <div class="col-md-3 form-group">
                                           
                                            <input class="form-control" placeholder="Quantity" min="0" readonly="1"
                                                value="{{$avlofStarter}}" name="avlofStarter" type="number">
                                        </div>
                                        @if ($show != 'yes')
                                            <div class="col-md-3 form-group">
                                               
                                                <input class="form-control lic" placeholder="Quantity" name="addofStarter"
                                                    type="number">
                                            </div>
                                        @endif
                                    @endif
                                </div>
                                <div class="row p-2">
                                    <div class="col-md-3 d-flex justify-content-right align-items-center">
                                        <label for="basic" class="b">
                                            @lang('messages.Basic')
                                        </label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        
                                        <input class="form-control" placeholder="Quantity" min="0" readonly="1"
                                            value="{{ $numofBasic }}" name="numofBasic" type="number">
                                    </div>
                                    <div class="col-md-3 form-group">
                                       
                                        <input class="form-control" placeholder="Quantity" min="0" readonly="1"
                                            name="avlofBasic" value="{{ $avlofBasic }}" type="number">
                                    </div>
                                    @if ($show != 'yes')
                                        <div class="col-md-3 form-group">
                                            
                                            <input class="form-control lic" placeholder="Quantity" min="0" name="addofBasic"
                                                type="number">
                                        </div>
                                    @endif
                                </div>
                                <div class="row p-2">
                                    <div class="col-md-3 form-group d-flex justify-content-right align-items-center">
                                        <label for="Advance" class="b">
                                            @lang('messages.Advance')
                                        </label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                       
                                        <input class="form-control" placeholder="Quantity" min="0" readonly="1"
                                            value="{{ $numofAdvance }}" name="numofAdvance" type="number">
                                    </div>
                                    <div class="col-md-3 form-group">
                                       
                                        <input class="form-control" placeholder="Quantity" min="0" readonly="1"
                                            value="{{ $avlofAdvance }}" name="avlofAdvance" type="number">
                                    </div>
                                    @if ($show != 'yes')
                                        <div class="col-md-3 form-group">
                                           
                                            <input class="form-control lic" placeholder="Quantity" min="0"
                                                name="addofAdvance" type="number">
                                        </div>
                                    @endif
                                </div>
                                <div class="row p-2">
                                    <div class="col-md-3 form-group d-flex justify-content-right align-items-center">
                                        <label for="Premium" class="b">
                                            @lang('messages.Premium')
                                        </label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        
                                        <input class="form-control" placeholder="Quantity" min="0" readonly="1"
                                            name="numofPremium" value="{{ $numofPremium }}" type="number">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        
                                        <input class="form-control" placeholder="Quantity" min="0" readonly="1"
                                            name="avlofPremium" value="{{ $avlofPremium }}" type="number">
                                    </div>
                                    @if ($show != 'yes')
                                        <div class="col-md-3 form-group">
                                           
                                            <input class="form-control lic" placeholder="Quantity" min="0"
                                                name="addofPremium" type="number">
                                        </div>
                                    @endif
                                </div>
                                <div class="row p-2">

                                    <div class="col-md-3 form-group d-flex justify-content-right align-items-center">
                                        <label for="PremiumPlus" class="b">
                                            @lang('messages.PremiumPlus') 
                                        </label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                       
                                        <input class="form-control" placeholder="Quantity" min="0" readonly="1"
                                            name="numofPremiumPlus" value="{{ $numofPremiumPlus }}" type="number">
                                    </div>
                                    <div class="col-md-3 form-group">
                                       
                                        <input class="form-control" placeholder="Quantity" min="0" readonly="1"
                                            name="avlofPremiumPlus" value="{{ $avlofPremiumPlus }}" type="number">
                                    </div>
                                    @if ($show != 'yes')
                                        <div class="col-md-3 form-group">
                                           
                                            <input class="form-control lic" placeholder="Quantity" min="0"
                                                name="addofPremiumPlus" type="number">
                                        </div>
                                    @endif
                                </div>
                            @endif

                            @if (session()->get('curdomin') != 'gps' && $uploads == 'yes')
                                <br>
                                <h3 class="text-primary">
                                    @lang('messages.UploadDealerLogo')
                                </h3>
                                <hr>
                                <div class="row p-2">
                                    <div class="col-md-4 pb-1">
                                        <div class="custom-file ">
                                            <input type="file" class="custom-file-input" id="validatedCustomFile1" data-img="img1"
                                                name="logo_smallEdit" accept="image/x-png,image/gif,image/jpeg" data-width="52" data-height="52">
                                            <label class="custom-file-label" for="validatedCustomFile1" data-def="@lang('messages.Image52*52(png)')">
                                                @lang('messages.Image52*52(png)')
                                            </label>
                                        </div>
                                        <div class="img-prev  d-none img1">
                                            <div class="card">
                                                <div class="card-header">
                                                    <i class="fas fa-window-close text-danger  close" data-img="img1"></i>
                                                </div>
                                                <div class="card-body text-center">
                                                    <img  class="img-thumbnail"width="auto" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 pb-1">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="validatedCustomFile2" data-img="img2"
                                                name="logo_mobEdit" accept="image/x-png,image/gif,image/jpeg" data-width="272" data-height="144">
                                            <label class="custom-file-label" for="validatedCustomFile2" data-def="@lang('messages.Image272*144(png)')">
                                                @lang('messages.Image272*144(png)')</label>
                                        </div>
                                        <div class="img-prev d-none  img2">
                                            <div class="card">
                                                <div class="card-header">
                                                    <i class="fas fa-window-close text-danger  close" data-img="img2"></i>
                                                </div>
                                                <div class="card-body text-center">
                                                    <img  class="img-thumbnail" width="auto" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 pb-1">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="validatedCustomFile3"  data-img="img3"
                                                name="logo_deskEdit" accept="image/x-png,image/gif,image/jpeg"  data-width="144" data-height="144">
                                            <label class="custom-file-label" for="validatedCustomFile3" data-def="@lang('messages.Image144*144(png)')">
                                                @lang('messages.Image144*144(png)')</label>
                                        </div>
                                        <div class="img-prev d-none  img3">
                                            <div class="card">
                                                <div class="card-header">
                                                    <i class="fas fa-window-close text-danger  close" data-img="img3"></i>
                                                </div>
                                                <div class="card-body text-center">
                                                    <img  class="img-thumbnail" width="auto"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                            @endif
                            <div class="row mt-4">
                                <div class="col-lg-12 text-center">
                                    {{ Form::submit(__('messages.UpdatetheDealer'), ['id' => 'sub', 'class' => 'btn btn-primary', 'onClick' => '']) }}
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('common/js/dealer.js') }}"></script>

    <script>
        if (!!window.performance && window.performance.navigation.type === 2) {
            window.location.reload();
        }
        $(document).ready(function() {
            $("#enableStarter").on('change',showStarter);
            $("#authenticated").on('change',showStarterPlus);
            $("#enableStarter,#authenticated").trigger('change');

           function showStarter(){
               $("#Starter").hide();
                if($("#enableStarter").val()==='yes'){
                    $("#Starter").show();
                }
            }
            function showStarterPlus(){
                $("#StarterPlus").hide();
                if($("#authenticated").val() == "yes"){
                    $("#StarterPlus").show();
                }
            }

            $('.lic').keyup(function() {
                let licInput = $(this);
                licInput.attr('min', parseInt(licInput.val()) === 0 ? '1' : '0')
            })

            smsProviderTextLocal();
            $('#smsProvider').on('change', function() {
                smsProviderTextLocal();
            });

            function smsProviderTextLocal() {
                val = $('#smsProvider').val() === "TextLocal";
                $('#textLocalSmsKeyFiled,#smsEntityNameFiled').toggle(val);
                $('#textLocalSmsKey,#smsEntityName').attr('required', val)
            }
            $('#editForm').submit(function() {
                $('.passerr').remove();
                $('#sub').attr('disabled', true);
                $('input').removeClass('is-invalid');
                var datas = {
                    'val': $('#notify').val(),
                    'val1': $('#gpsapp').val(),
                    'val2': $('#mapkey').val(),
                    'val3': $('#addkey').val(),
                }
                let validate = true

                let mobileNo = $('#mobileNo')
                if(mobileNo.val().length <= 7){
                    mobileNo.focus().aftererr('Please, Enter valid number');
                    return false
                }

                if (datas.val2 == "") {
                    $("#mapkey")
                        .focus()
                        .aftererr('Please Enter Valid Map Key / API Key');
                    validate = false;
                }

                if (datas.val1 !== "" && datas.val1 !== "null") {
                    var variable2 = (datas.val1).substring(0, 4);
                    if (variable2 != 'com.' && (datas.val1) != 'null') {
                        $("#gpsapp")
                            .focus()
                            .aftererr("Mobile App Key should be starts with 'com.' or 'null'");
                        validate = false;
                    }

                    if (variable2 !== 'com.' && datas.val == "") {
                        $("#notify").focus().aftererr('Please Enter Valid Notification Key');
                        validate = false;
                    }

                }
                validate = formValidation('#editForm',validate);
                // debugger
                $('#sub').attr('disabled', validate);
                return validate;
            });
            $('#gpsapp').on('change', function() {
                $(this).removeClass('is-invalid');
                var datas = {
                    'val': $('#notify').val(),
                    'val1': $('#gpsapp').val()
                }
                if ((datas.val1).substring(0, 4) == 'com.') {
                    $("#notify").attr({
                        'readOnly': 'true'
                    });
                } else {
                    $("#notify").removeAttr('readOnly');
                }
            });
            $('#gpsapp').on('keyup', function() {
                $(this).val($(this).val().trim())
            });
        });


        $(document).ready(function() {
            var datas = {
                'val': $('#notify').val(),
                'val1': $('#gpsapp').val()
            }

            if ((datas.val1).substring(0, 4) == 'com.') {
                $("#notify").attr({
                    'readOnly': 'true'
                });
            } else {
                $("#notify").removeAttr('readOnly');
            }

            if ($("#enableStarter").val() === 'yes') {
                $("#Starter").show();
            } else {
                $("#Starter").hide();
            }

        });
    </script>
@endpush
