@extends("layouts.app")
@section('content')
    <div id="wrapper">

        <div class="content animated zoomIn page-inner">

            <div class="hpanel">
                <div class="pull-right">
                    <a href="{{ route('dealer/sendExcel') }}"><img class="pull-right" width="40px" height="50px"
                            src="{{ asset('assets/imgs/xls.svg') }}" method="get" alt="xls" /></a>
                </div>

                <div class="page-header">
                    <h4 class="page-title"><b>
                            @lang('messages.DealerList')
                        </b>
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.DealerList')
                            </a>
                        </li>
                    </ul>
                </div>
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif
                <div class="page-body card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-hover table-striped dataTable table-head-bg-info">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">
                                            @lang('messages.DealerName')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.MobileNumber')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.Website')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.Code')
                                        </th>
                                        <th style="text-align: center;">
                                            @lang('messages.Action')
                                        </th>
                                    </tr>
                                </thead>
    
    
                                <tbody>
                                    @foreach ($dealerlist as $key => $value)
                                        <tr>
                                            <td>{{ $value }}</td>
                                            <td>{{ Arr::get($userMobileArr, $value) }}</td>
                                            <td>{{ Arr::get($dealerWeb, $value) }}</td>
                                            <td>{{ $fcode }}</td>
                                            <td>
                                                <div class="dropdown dropleft">
                                                    <button class="btn btn-info text-white btn-sm dropdown-toggle" type="button"
                                                        id="dropdownMenuButton{{ $key }}" data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                        @lang('messages.Action')
                                                    </button>
                                                    <div class="dropdown-menu"
                                                        aria-labelledby="dropdownMenuButton{{ $key }}">
                                                        <a class="btn btn-sm btn-info text-white dropdown-item mb-1"
                                                            href="{{ URL::to('vdmDealers/' . $value) }}">
                                                            @lang('messages.ViewDealer')
                                                        </a>
                                                        <a class="btn btn-sm btn-success text-white dropdown-item mb-1"
                                                            href="{{ URL::to('vdmDealers/' . $value . '/edit') }}">
                                                            @lang('messages.EditDealer')
                                                        </a>
                                                        <a class="btn btn-sm btn-secondary text-white dropdown-item mb-1"
                                                            href="{{ URL::to('vdmDealers/reports/' . $value) }}">
                                                            @lang('messages.DealerReports')
                                                        </a>
                                                        <a class="btn btn-sm btn-info text-white dropdown-item mb-1"
                                                            href="{{route('audit',['type'=>'dealer','name'=>$value ])}}" 
                                                            onClick="setGroup('{{ $value }}',this);">
                                                            @lang('messages.AuditReport')
                                                        </a>
                                                        {{-- {{ route('audit',['type'=>'dealer','name'=>$value ]) }} --}}
                                                        @if ($enableArr[$value])
                                                            <a class="btn btn-sm btn-warning text-white dropdown-item mb-1"
                                                                href="{{ URL::to('vdmDealers/disable/' . $value) }}">
                                                                @lang('messages.Disable')
                                                            </a>
                                                        @else
                                                            <a class="btn btn-sm btn-success text-white dropdown-item mb-1"
                                                                href="{{ URL::to('vdmDealers/enable/' . $value) }}">
                                                                @lang('messages.Enable')
                                                            </a>
                                                        @endif
                                                        {{ Form::open(['url' => 'vdmDealers/' . $value, 'class' => '', 'id' => 'deleteForm' . $key]) }}
                                                        {{ Form::hidden('_method', 'DELETE') }}
                                                        {{ Form::button(__('messages.DeletethisDealer'), ['class' => 'btn btn-sm btn-danger text-white dropdown-item deleteBtn', 'id' => $key, 'formtype' => 'Dealer']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
