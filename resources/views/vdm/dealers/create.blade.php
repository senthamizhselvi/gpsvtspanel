    @extends("layouts.app")
    @section('content')
        <div id="wrapper">
            <div class="content animated zoomIn page-inner">

                <div class="page-header">
                    <h4 class="page-title"><b>
                            @lang('messages.AddDealer')
                        </b>
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('vdmDealers.index') }}">
                            @lang('messages.DealerList')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.AddDealer')
                            </a>
                        </li>
                    </ul>
                </div>
                @if (session()->get('cur1') == 'prePaidAdmin')
                    @include('vdm.billing.licDetails')
                @endif
                <div class="page-body">
                    <div class="card">

                        <div class="card-body">
                            <span class="text-danger error-message">
                                {{ HTML::ul($errors->all()) }}
                            </span>

                            {{ Form::open(['url' => 'vdmDealers', 'files' => true, 'enctype' => 'multipart/form-data', 'id' => 'form']) }}
                            <h4 class="text-primary">
                                @lang('messages.General')
                            </h4>
                            <hr>
                            <div class="row p-2">
                                <div class="col-md-6 form-group">
                                    {{ Form::label('dealerId', __('messages.DealerName')) }} <span class="rq"></span>
                                    <i class="fa fa-info-circle fa-lg text-warning ml-auto" data-toggle="tooltip"
                                        data-placement="right"
                                        title="Dealer name is case sensitive and space is not allowed"></i> <br>
                                    {{ Form::text('dealerId', $dealerName, ['class' => 'form-control caps', 'required' => 'required', 'placeholder' => 'Dealer Name', 'id' => 'dealerName']) }}
                                    <span id="validation" class=" text-danger"></span>
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('mobileNo', __('messages.MobileNumber')) }} <span class="rq"></span>
                                    {{ Form::number('mobileNo', old('mobileNo'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Mobile Number', 'id' => 'mobileNo', 'minlength' => '8','maxlength'=>'15']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('email', __('messages.Email')) }} <span class="rq"></span>
                                    {{ Form::email('email', old('email'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'E-mail Id']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('password', __('messages.Password')) }} <span class="rq"></span>
                                    {{ Form::text('password', old('password'), ['class' => 'form-control passhover', 'required' => 'required', 'placeholder' => 'Password', 'id' => 'dealerpass', 'minlength' => '6']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('address', __('messages.Address')) }} <span class="rq"></span>
                                    {{ Form::text('address', old('address'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Full Address']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('zoho', __('messages.Zoho')) }} <span class="rq"></span>
                                    {{ Form::text('zoho', old('zoho'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Zoho']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('website', __('messages.Website')) }} <span class="rq"></span>
                                    {{ Form::text('website', old('website'), ['class' => 'form-control', 'placeholder' => 'Website', 'required' => 'required']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('mapKey', __('messages.MapKey/APIKey')) }} <span class="rq"></span>
                                    {{ Form::text('mapKey', old('mapKey'), ['id' => 'mapkey', 'class' => 'form-control', 'placeholder' => 'Map Key', 'required' => 'required']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('website1', __('messages.Website1')) }}
                                    {{ Form::text('website1', old('website1'), ['class' => 'form-control', 'placeholder' => 'Website 1']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('mapKey1', __('messages.MapKey1/APIKey1')) }}
                                    {{ Form::text('mapKey1', old('mapKey1'), ['class' => 'form-control', 'placeholder' => 'Map Key 1']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('defaultMap', __('messages.DefaultMap')) }}
                                    {{ Form::select('defaultMap',$defaultMapList, old('defaultMap'), ['class' => 'form-control']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('subDomain', __('messages.Subdomain(AdminPanelUrl)')) }}
                                    {{ Form::text('subDomain', old('subDomain'), ['class' => 'form-control', 'placeholder' => 'Subdomain']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('timeZone', __('messages.CountryTimeZone')) }}
                                    {{ Form::select('timeZone', $timezoneList, $timeZone, ['class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('languageCode', __('messages.LanguageSelection')) }}
                                    {{ Form::select('languageCode', ['ta' => 'Tamil', 'hi' => 'Hindi', 'en' => 'English', 'es' => 'Spanish'], 'en', ['class' => ' form-control', 'data-live-search ' => 'true']) }}
                                </div>
                            </div>
                            @if (session()->get('cur1') == 'prePaidAdmin' || true)
                                <br>
                                <h4 class="text-primary ">
                                    @lang('messages.LicenceConfiguration')
                                </h4>
                                <hr>
                                <div class="row p-2">
                                    <div class="col-md-6 form-group">
                                        {{ Form::label('numofbasic', __('messages.NumberofBasicLicence')) }}
                                        {{ Form::number('numofbasic', old('numofbasic'), ['class' => 'form-control numberOnly', 'placeholder' => 'Quantity', 'min' => '0']) }}
                                    </div>
                                    <div class="col-md-6 form-group">
                                        {{ Form::label('numofadvance', __('messages.NumberofAdvanceLicence')) }}
                                        {{ Form::number('numofadvance', old('numofadvance'), ['class' => 'form-control numberOnly', 'placeholder' => 'Quantity', 'min' => '0']) }}
                                    </div>
                                    <div class="col-md-6 form-group">
                                        {{ Form::label('numofpremium', __('messages.NumberofPremiumLicence')) }}
                                        {{ Form::number('numofpremium', old('numofpremium'), ['class' => 'form-control numberOnly', 'placeholder' => 'Quantity', 'min' => '0']) }}
                                    </div>
                                    <div class="col-md-6 form-group">
                                        {{ Form::label('numofpremiumplus', __('messages.NumberofPremiumPlusLicence')) }}
                                        {{ Form::number('numofpremiumplus', old('numofpremiumplus'), ['class' => 'form-control numberOnly', 'placeholder' => 'Quantity', 'min' => '0']) }}
                                    </div>
                                </div>
                            @endif
                            <br>
                            <h4 class="text-primary">
                                @lang('messages.SMSConfiguration')
                            </h4>
                            <hr>
                            <div class="row p-2">
                                <div class="col-md-6 form-group">
                                    {{ Form::label('smsSender', __('messages.SMSSender')) }}
                                    {{ Form::text('smsSender', old('smsSender'), ['class' => 'form-control', 'placeholder' => 'SMS Sender']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('smsProvider', __('messages.SMSProvider')) }}
                                    {{ Form::select('smsProvider', $smsP, old('smsProvider'), ['class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('providerUserName', __('messages.SMSProviderUserName')) }}
                                    {{ Form::text('providerUserName', old('providerUserName'), ['class' => 'form-control', 'placeholder' => 'SMS Provider Name']) }}
                                </div>
                                <div class="col-md-6 form-group" id='textLocalSmsKeyFiled'>
                                    {{ Form::label('textLocalSmsKey', __('messages.TextLocalSmsKey')) }} <span class="rq"></span>
                                    {{ Form::text('textLocalSmsKey', old('textLocalSmsKey'), ['class' => 'form-control', 'id' => 'textLocalSmsKey']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('providerPassword', __('messages.SMSProviderPassword')) }}
                                    {{ Form::text('providerPassword', old('providerPassword'), ['class' => 'form-control passhover', 'placeholder' => 'SMS Provider Password', 'id' => 'dealerpass1']) }}
                                </div>
                                <div class="col-md-6 form-group" id="smsEntityNameFiled">
                                    {{ Form::label('smsEntityName', __('messages.SMSEntityName')) }} <span class="rq"></span>
                                    {{ Form::text('smsEntityName', old('smsEntityName'), ['class' => 'form-control', 'placeholder' => 'SMS Entity Name', 'id' => 'smsEntityName']) }}
                                </div>
                            </div>
                            <br>
                            <h4 class="text-primary">
                                @lang('messages.MobileAppConfiguration')
                            </h4>
                            <hr>
                            <div class="row p-2">
                                <div class="col-md-6 form-group">
                                    {{ Form::label('gpsvtsAppKey', __('messages.MobileAppPackageName')) }} <span class="rq"></span>
                                    <i class="fa fa-info-circle fa-lg text-warning ml-auto" data-toggle="tooltip"
                                        data-placement="right"
                                        title="Mobile App Name is case sensitive and space is not allowed"></i> <br>
                                    {{ Form::text('gpsvtsAppKey', old('gpsvtsAppKey'), ['id' => 'gpsapp', 'class' => 'form-control', 'placeholder' => 'Mobile App Name']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('notificationKey', __('messages.NotificationKey')) }}
                                    {{ Form::text('notificationKey', old('notificationKey'), ['id' => 'notify', 'class' => 'form-control', 'placeholder' => 'Notification Key']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('addressKey', __('messages.AddressKey')) }}
                                    {{ Form::text('addressKey', old('addressKey'), ['id' => 'addkey', 'class' => 'form-control', 'placeholder' => 'Address Key']) }}
                                </div>
                            </div>
                            <br>
                            <h4 class="text-primary">
                                @lang('messages.UploadDealerLogo')
                            </h4>
                            <hr>
                            <div class="row p-2">
                                <div class="col-md-4 pb-1">
                                    <div class="custom-file ">
                                        <input type="file" class="custom-file-input" id="validatedCustomFile1" data-img="img1"
                                            name="logo_small" accept="image/x-png,image/gif,image/jpeg" data-width="52" data-height="52">
                                        <label class="custom-file-label" for="validatedCustomFile1" data-def="@lang('messages.Image52*52(png)')">
                                            @lang('messages.Image52*52(png)')
                                        </label>
                                    </div>
                                    <div class="img-prev  d-none img1">
                                        <div class="card">
                                            <div class="card-header">
                                                <i class="fas fa-window-close text-danger  close" data-img="img1"></i>
                                            </div>
                                            <div class="card-body text-center">
                                                <img  class="img-thumbnail"width="auto" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 pb-1">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="validatedCustomFile2" data-img="img2"
                                            name="logo_mob" accept="image/x-png,image/gif,image/jpeg" data-width="272" data-height="144">
                                        <label class="custom-file-label" for="validatedCustomFile2" data-def="@lang('messages.Image272*144(png)')">
                                            @lang('messages.Image272*144(png)')</label>
                                    </div>
                                    <div class="img-prev d-none  img2">
                                        <div class="card">
                                            <div class="card-header">
                                                <i class="fas fa-window-close text-danger  close" data-img="img2"></i>
                                            </div>
                                            <div class="card-body text-center">
                                                <img  class="img-thumbnail" width="auto" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 pb-1">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="validatedCustomFile3"  data-img="img3"
                                            name="logo_desk" accept="image/x-png,image/gif,image/jpeg"  data-width="144" data-height="144">
                                        <label class="custom-file-label" for="validatedCustomFile3" data-def="@lang('messages.Image144*144(png)')">
                                            @lang('messages.Image144*144(png)')</label>
                                    </div>
                                    <div class="img-prev d-none  img3">
                                        <div class="card">
                                            <div class="card-header">
                                                <i class="fas fa-window-close text-danger  close" data-img="img3"></i>
                                            </div>
                                            <div class="card-body text-center">
                                                <img  class="img-thumbnail" width="auto"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row mt-4">
                                <div class="col-md-12 text-center ">
                                    {{ Form::submit('Submit', ['id' => 'sub', 'class' => 'btn btn-primary']) }}{{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endsection
    @push('js')
        <script src="{{ asset('common/js/dealer.js') }}"></script>

        <script type="text/javascript">

            var smsProvider = $('#smsProvider').val() === "TextLocal";
            smsProviderTextLocal(smsProvider);
            $('#smsProvider').on('change', function() {
                smsProviderTextLocal($(this).val() === "TextLocal");
            });

            function smsProviderTextLocal(val) {
                $('#textLocalSmsKeyFiled,#smsEntityNameFiled').toggle(val);
                $('#textLocalSmsKey,#smsEntityNameFiled').attr('required', val)
            }

            $('#dealerName').on('change', function() {
                dealercheck();
            });
            $('#mobileNo').on('change', function() {
                dealercheck();
            });

            function dealercheck() {

                $('#validation').text('');
                $('#dealerName').removeClass('is-invalid');
                var postValue = {
                    'id': $("#dealerName").val(),
                    '_token': $('input[name="_token"]').val()
                };
                // alert($('#groupName').val());
                $.post('{{ route('ajax.dealerCheck') }}', postValue)
                    .done(function(data) {
                        if (data != '') {
                            $('#validation').text(data);
                            $("#dealerName").focus().addClass('is-invalid').addClass('is-invalid');
                            return false;
                        }

                    }).fail(function() {
                        console.log("fail");
                    });
            }

            $(document).ready(function() {

                $('#form').submit(function() {
                    $('.passerr').remove();
                    $('#sub').attr('disabled', true);
                    $('input').removeClass('is-invalid');

                    var datas = {
                        'val': $('#notify').val(),
                        'val1': $('#gpsapp').val(),
                        'val2': $('#mapkey').val(),
                        'val3': $('#addkey').val(),
                    }

                    let validate = true
                    let mobileNo = $('#mobileNo')
                    if(mobileNo.val().length <= 7){
                        mobileNo.focus().aftererr('Please, Enter valid number');
                        return false
                    }

                    if (datas.val2 == "") {
                        $("#mapkey")
                            .focus()
                            .aftererr('Please Enter Valid Map Key / API Key');
                        $('#sub').attr('disabled', false);
                        validate = false;
                    }

                    if (datas.val1 !== "" && datas.val1 !== "null") {
                        var variable2 = (datas.val1).substring(0, 4);
                        if (variable2 != 'com.' && (datas.val1) != 'null') {
                            $("#gpsapp")
                                .focus()
                                .aftererr("Mobile App Key should be starts with 'com.' or 'null'");
                            validate = false;
                        }

                        if (variable2 !== 'com.' && datas.val == "") {
                            $("#notify").focus().aftererr('Please Enter Valid Notification Key');
                            validate =  false;
                        }
                        
                        // if (datas.val3 == "") {
                        //     $("#addkey")
                        //         .focus()
                        //         .aftererr('Please Enter Valid Address Key');
                        //     validate =  false;

                        // }
                    }
                    validate  = formValidation('#form',validate);
                    $('#sub').attr('disabled', validate);
                    return validate;

                });
                $('#gpsapp').on('change', function() {
                    $(this).removeClass('is-invalid');
                    var datas = {
                        'val': $('#notify').val(),
                        'val1': $('#gpsapp').val()
                    }
                    if ((datas.val1).substring(0, 4) == 'com.') {
                        $("#notify").attr({
                            'readOnly': 'true'
                        });
                    } else {
                        $("#notify").removeAttr('readOnly');
                    }
                });
                $('#gpsapp').on('keyup',function(){
                    $(this).val($(this).val().trim())
                });
            });

        </script>
        @include('includes.js_create')
    @endpush
