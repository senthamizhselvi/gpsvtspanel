@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            <div class="page-header">
                <div class="page-title">
                    @lang('messages.ViewDealer')
                </div>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('vdmDealers.index') }}">
                            @lang('messages.DealerList')
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                            @lang('messages.ViewDealer')
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">{{ $userId }}</a>
                    </li>
                </ul>
            </div>
            @if (session()->has('message'))
                <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                    {{ session()->get('message') }}
                </p>
            @endif

            <div class="card">

                <div class="card-body">
                    <h3 class="text-primary">
                        @lang('messages.General')
                    </h3>
                    <hr>
                    <div class="row p-2">
                        <div class="col-md-6 form-group">
                            {{ Form::label('adminName', __('messages.DealerName')) }}
                            <label class="form-control">{{ $userId }}</label>
                        </div>
                        <div class="col-md-6 form-group">
                            {{ Form::label('mobileNo', __('messages.MobileNo')) }}
                            <label class="form-control">{{ $mobileNo }}</label>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            {{ Form::label('email', __('messages.Email')) }}
                            <label class="form-control">{{ $email }}</label>
                        </div>
                        @if ($subDomain !=="")
                            <div class="col-md-6 form-group">
                                {{ Form::label('subDomain', __('messages.Subdomain(AdminPanelUrl)')) }}
                                <label class="form-control">{{ $subDomain }}</label>
                            </div>
                        @endif
                        <div class="col-md-6 form-group">
                            {{ Form::label('zoho', __('messages.Website')) }}  
                            <label class="form-control">{{ $website }}</label>
                        </div>
                        <div class="col-md-6 form-group">
                            {{ Form::label('address', __('messages.Address')) }}
                            {{Form::textarea('address',$address,['class'=>'form-control border readonly','rows'=>4,'cols'=>'3'])}}
                        </div>
                        
                    </div>
                  
                    @if (session()->get('cur1') == 'prePaidAdmin')
                        <br>
                        <h3 class="text-primary">
                            @lang('messages.LicenceConfiguration')
                        </h3>
                        <hr>
                        <div class="row p-2">
                            @if ($authenticated == 'yes')
                                <div class="col-md-6 form-group">
                                    {{ Form::label('numofStarterPlus', __('messages.NumberofStarterPlusLicence')) }}
                                    <label class="form-control">{{ $numofStarterPlus }}</label>
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('avlofStarterPlus', __('messages.AvailableStarterPlusLicence')) }}
                                    <label class="form-control">{{ $avlofStarterPlus }}</label>
                                </div>
                            @endif
                            @if ($enableStarter == 'yes')
                                <div class="col-md-6 form-group">
                                    {{ Form::label('numofStarter', __('messages.NumberofStarterLicence')) }}
                                    <label class="form-control">{{ $numofStarter }}</label>
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::label('avlofStarter', __('messages.StarterAvailableLicence')) }}
                                    <label class="form-control">{{ $avlofStarter }}</label>
                                </div>
                            @endif
                           
                            <div class="col-md-6 form-group">
                                {{ Form::label('numofBasic', __('messages.NumberofBasicLicence')) }}
                                <label class="form-control">{{ $numofBasic }}</label>
                            </div>
                            <div class="col-md-6 form-group">
                                {{ Form::label('avlofBasic', __('messages.BasicAvailableLicence')) }}
                                <label class="form-control">{{ $avlofBasic }}</label>
                            </div>
                            <div class="col-md-6 form-group">
                                {{ Form::label('numofAdvance', __('messages.NumberofAdvanceLicence')) }}
                                <label class="form-control">{{ $numofAdvance }}</label>
                            </div>
                            <div class="col-md-6 form-group">
                                {{ Form::label('avlofAdvance', __('messages.AdvanceAvailableLicence')) }}
                                <label class="form-control">{{ $avlofAdvance }}</label>
                            </div>
                            <div class="col-md-6 form-group">
                                {{ Form::label('numofPremium', __('messages.NumberofPremiumLicence')) }}
                                <label class="form-control">{{ $numofPremium }}</label>
                            </div>
                            <div class="col-md-6 form-group">
                                {{ Form::label('avlofPremium', __('messages.PremiumAvailableLicence')) }}
                                <label class="form-control">{{ $avlofPremium }}</label>
                            </div>
                            <div class="col-md-6 form-group">
                                {{ Form::label('numofPremiumPlus', __('messages.NumberofPremiumPlusLicence')) }}
                                <label class="form-control">{{ $numofPremiumPlus }}</label>
                            </div>
                            <div class="col-md-6 form-group">
                                {{ Form::label('avlofPremiumPlus', __('messages.PremiumPlusAvailableLicence')) }}
                                <label class="form-control">{{ $avlofPremiumPlus }}</label>
                            </div>
                        </div>

                    @endif
                    <br>
                    @if (session()->get('curdomin') != 'gps' && $uploads == 'yes')
                    <h3 class="text-primary">
                        @lang('messages.DealerLogo')
                    </h3>
                    <hr>
                    <div class="row p-2 text-center">
                        <div class="col-md-4 col-12 card m-1 m-md-0">
                            <div class="card-header">
                                <label for="sd">
                                    @lang('messages.Image52*52(png)')
                                </label>
                            </div>
                            <div class="card-body">
                                <img src='{{ $imgSmall }}' class="img-thumbnail" />
                            </div>

                        </div>
                        <div class="col-md-4 col-12 card m-1 m-md-0">
                            <div class="card-header">
                                <label for="logo">
                                    @lang('messages.Image272*144(png)')
                                </label>
                            </div>
                            <div class="card-body">
                                <img src='{{ $imgMob }}' class="img-thumbnail" />
                            </div>
                        </div>
                        <div class="col-md-4 col-12 card m-1 m-md-0">
                            <div class="card-header">
                                <label for="sd">
                                    @lang('messages.Image144*144(png)')
                                </label>
                            </div>
                            <div class="card-body">
                                <img src='{{ $imgLogo }}' class="img-thumbnail" />
                            </div>

                        </div>
                        
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
