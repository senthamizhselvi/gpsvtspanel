@extends("layouts.app")
@section('content')
    <div id="wrapper">
        @include('vdm.billing.licenceList_create')
        <div class="content animate-panel">
            {{ Form::open(['url' => 'convert/update', 'id' => 'convertFrom']) }}
            <div class="page-inner">
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif

                @include('vdm.billing.licDetails')
                <div class="card">
                    <div class="card-body">
                        <span class="text-danger error-message"> {{ HTML::ul($errors->all()) }}</span>
                        <span class="text-right">
                            <h6 class="text-warning">*VALUES STARTER = 0.5,BASIC = 1,
                                ADVANCE = 1.5, PREMIUM = 2, PREMIUM-PLUS = 3</h6>
                        </span>
                        <hr>
                        {{ Form::hidden('availableStarterLicence', $availableStarterLicence, ['class' => 'form-control', 'id' => 'avlST']) }}
                        {{ Form::hidden('availableBasicLicence', $availableBasicLicence, ['class' => 'form-control', 'id' => 'avlBC']) }}
                        {{ Form::hidden('availableAdvanceLicence', $availableAdvanceLicence, ['class' => 'form-control', 'id' => 'avlAD']) }}
                        {{ Form::hidden('availablePremiumLicence', $availablePremiumLicence, ['class' => 'form-control', 'id' => 'avlPR']) }}
                        {{ Form::hidden('availablePremPlusLicence', $availablePremPlusLicence, ['class' => 'form-control', 'id' => 'avlPPl']) }}
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <label for="LicenceTypes">@lang('messages.LicenceType')</label>
                                <select id="LicenceTypes" class="form-control" data-live-search="true" required="required"
                                    name="LicenceTypes" value="Basic">
                                    <option value="">--Select--</option>
                                    <option value="Starter">Starter</option>
                                    <option value="Basic">Basic</option>
                                    <option value="Advance">Advance</option>
                                    <option value="Premium">Premium</option>
                                    <option value="PremiumPlus">Premium Plus</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                {{ Form::hidden('reminingLic', '0', ['id' => 'reminingLic']) }}
                                <label for="vehicleList">@lang('messages.NumberofLicence')</label>
                                {{ Form::number('numOfLic', request()->old('numOfLic'), ['id' => 'numOfLic', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Number of Licence', 'min' => 1]) }}
                                <span id="span" style="color: #c35151;"></span>
                            </div>
                            <div class="col-md-1 align-self-center">
                                <h2 style="margin-left:45%;">&#8658;</h2>
                            </div>
                            <div class="col-md-2">
                                <label for="cnvLicenceTypes">@lang('messages.LicenceType')</label>
                                <select id="cnvLicenceTypes" class="form-control" data-live-search="true"
                                    required="required" name="cnvLicenceTypes" value="Basic">
                                    <option value="">--Select--</option>
                                    <option value="Starter">Starter</option>
                                    <option value="Basic">Basic</option>
                                    <option value="Advance">Advance</option>
                                    <option value="Premium">Premium</option>
                                    <option value="PremiumPlus">Premium Plus</option>
                                </select>
                            </div>
                        </div>
                        <div class="mt-3 text-center">
                            <input class="btn btn-primary" id="sub" type="button" value="{{__('messages.Submit')}}">
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        if (!!window.performance && window.performance.navigation.type === 2) {
            window.location.reload();
        }
        $(document).ready(function() {
            $('select').on('change', function(event) {
                var prevValue = $(this).data('previous');
                $('select').not(this).find('option[value="' + prevValue + '"]').show();
                var value = $(this).val();
                $(this).data('previous', value);
                $('select').not(this).find('option[value="' + value + '"]').hide();
            });
        });

        $('#sub').on('click', function(e) {
            let licenceValue = {
                'Starter': 0.5,
                'Basic': 1,
                'Advance': 1.5,
                'Premium': 2,
                'PremiumPlus': 3
            }
            let licenceTypes = {
                'Starter': 'Starter',
                'Basic': 'Basic',
                'Advance': 'Advance',
                'Premium': 'Premium',
                'PremiumPlus': 'PremiumPlus'
            }
            var st = 0.5;
            var bc = 1;
            var ad = 1.5;
            var pr = 2;
            var prpl = 3;
            var datas = {
                'avlStarter': $('#avlST').val(),
                'avlBasic': $('#avlBC').val(),
                'avlAdvance': $('#avlAD').val(),
                'avlPremium': $('#avlPR').val(),
                'avlPremiumPlus': $('#avlPPl').val(),
                'types': $('#LicenceTypes').val(),
                'cnvTypes': $('#cnvLicenceTypes').val(),
                'numOfLic': $('#numOfLic').val()
            }
            var cnl = 0;
            var initialData = datas.numOfLic;
            if (datas.types == datas.cnvTypes) {
                wornigAlert('{{__("messages.LicenceTypeshouldnotbesame")}}');
                return false;
            } else {
                if (parseFloat(datas['avl' + datas.types]) < parseFloat(datas.numOfLic)) {
                    wornigAlert("Entered Insufficient " + licenceTypes[datas.types] + " Licences !");
                    return false;
                } else {
                    shuffleLicence(datas.types, licenceValue[datas.types], licenceValue[datas.cnvTypes], datas
                        .cnvTypes, initialData, e);
                }
            }

        });

        async function shuffleLicence(types, typesRatio, cnvTypesRatio, cnvTypes, initialData, e) {
            var numOfLic = initialData;
            while (parseInt(numOfLic) > 0) {
                var val = numOfLic * typesRatio;
                var rem = modulo(val, cnvTypesRatio);
                if (rem == 0) {
                    var redPRPL = (numOfLic * typesRatio) / cnvTypesRatio;
                    var remining = initialData - numOfLic;
                    if (remining != 0) {
                        let sure = await confirmAlert('Are you sure to convert ' + numOfLic + ' ' + types +
                            ' Licences into ' + redPRPL + ' ' + cnvTypes + ' Licences ?  Remaining ' + remining +
                            ' will be added into ' + types + ' Licences Type !')
                        $('#reminingLic').val(remining);
                        if (!sure) {
                            return false
                        }
                    } else {
                        let sure = await confirmAlert('Are you sure to convert ' + numOfLic + ' ' + types +
                            ' Licences into ' + redPRPL + ' ' + cnvTypes + ' Licences ?')
                        if (!sure) {
                            return false
                        }
                    }
                    break;
                } else {
                    numOfLic = numOfLic - 1;
                }
            }
            if (numOfLic <= 0) {
                wornigAlert('Enter valid quantity to shuffle from ' + types + ' to ' + cnvTypes + ' Conversion');
                return false;
            }

            $('#convertFrom').submit()

        }

        function modulo(num1, num2) {
            if (isNaN(num1) || isNaN(num2)) {
                return NaN;
            }
            if (num1 === 0) {
                return 0;
            }
            if (num2 === 0) {
                return NaN;
            }
            var newNum1 = Math.abs(num1);
            var newNum2 = Math.abs(num2);
            var quot = newNum1 - Math.floor(newNum1 / newNum2) * newNum2;
            if (num1 < 0) {
                return -(quot);
            } else {
                return quot;
            }
        }


        $(document).ready(function() {
            $("#bala5").addClass("active");
        });
    </script>
    {{-- @include('includes.js_create') --}}
@endpush

</body>

</html>
