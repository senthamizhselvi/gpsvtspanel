@extends("layouts.app")
@section('content')
    <div id="wrapper">
        @include('vdm.billing.licenceList_index')
        <div class="content animate-panel">
            <div class="page-inner">
                <div class="hpanel">
                    <div class="panel-body">
                        @if (session()->has('message'))
                            <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                                {{ session()->get('message') }}</p>
                        @endif
                        <span class="text-danger error-message"> {{ HTML::ul($errors->all()) }}</span>
                        @include('vdm.billing.licDetails')
                        <div class="card">

                            <div class="card-body ">
                                <div class="overflow-auto">
                                    <table id="billingTable"
                                        class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th>
                                                    @lang('messages.LicenceID')
                                                </th>
                                                <th>
                                                    @lang('messages.VehicleID')
                                                </th>
                                                <th>
                                                    @lang('messages.VehicleName')
                                                </th>
                                                <th>
                                                    @lang('messages.Type')
                                                </th>
                                                <th>
                                                    @lang('messages.DeviceID')
                                                </th>
                                                <th>
                                                    @lang('messages.Organisation')
                                                </th>
                                                <th>
                                                    @lang('messages.DeviceModel')
                                                </th>
                                                <th>
                                                    @lang('messages.DealerName')
                                                </th>
                                                <th>
                                                    @lang('messages.GpsSimNo')
                                                </th>
                                                <th>
                                                    @lang('messages.LicenceIssuedDate')
                                                </th>
                                                <th>
                                                    @lang('messages.LicenceOnboardDate')
                                                </th>
                                                <th>
                                                    @lang('messages.LicenceExpireDate')
                                                </th>
                                                <th>
                                                    @lang('messages.Status')
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="myTable">
                                            @foreach ($LicenceList as $key => $value)
                                                <tr @if (in_array($value, $ExpiredLicenceList)) class="text-danger" @endif>
                                                    <td>{{ $value }}</td>
                                                    <td>{{ Arr::get($vehicleList, $value) }}</td>
                                                    <td>{{ Arr::get($VehicleNameList, $value) }}</td>
                                                    <td>{{ Arr::get($LtypeList, $value) }}</td>
                                                    <td>{{ Arr::get($deviceList, $value) }}</td>
                                                    <td>{{ Arr::get($orgIdList, $value) }}</td>
                                                    <td>{{ Arr::get($deviceModelList, $value) }}</td>
                                                    <td>{{ Arr::get($dealerList, $value) }}</td>
                                                    <td>{{ Arr::get($gpsSimNoList, $value) }}</td>
                                                    <td>{{ Arr::get($licenceissuedList, $value) }}</td>
                                                    <td>{{ Arr::get($LicenceOnboardList, $value) }}</td>
                                                    <td>{{ Arr::get($LicenceExpiryList, $value) }}</td>
                                                    <td>
                                                        @if (in_array($value, $ExpiredLicenceList))
                                                            <span class="h6 b text-danger">
                                                                @lang('messages.Expired')
                                                            </span>
                                                        @else
                                                            <span class="h6 b text-success">
                                                                @lang('messages.Active')
                                                            </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        if (!!window.performance && window.performance.navigation.type === 2) {
            window.location.reload();
        }
        $(document).ready(function() {
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-uk-pre": function (a) {
                    var ukDatea = a.split('-');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                },

                "date-uk-asc": function (a, b) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-uk-desc": function (a, b) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });

            $("#bala1").addClass("active");
            $('#billingTable').dataTable( {
                'stateSave': true,
                "aoColumns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    { "sType": "date-uk" },
                    { "sType": "date-uk" },
                    { "sType": "date-uk" },
                    null
                ]
            });
        });
    </script>
@endpush
