<h3 class="text-primary b">
    @lang('messages.AvailableLicence')
</h3>
<div class="row row-card-no-pd mt--1 mb-2 justify-content-center">
    @if (session()->get('isAuthenticated') == 'yes')
        <div class="col-sm-6 col-md-2">
            <div class="card card-stats card-round">
                <div class="card-body text-center">

                    <div class="numbers">
                        <label class="card-category">
                            @lang('messages.StarterPlus')
                        </label>
                        <h4 class="card-title b text-primary">{{ $availableStarterPlusLicence }}</h4>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if (session()->get('enableStarter') == 'yes')
        <div class="col-sm-6 col-md-2">
            <div class="card card-stats card-round">
                <div class="card-body text-center">
                    <div class="numbers">
                        <label class="card-category">
                            @lang('messages.Starter')
                        </label>
                        <h4 class="card-title b text-primary">{{ $availableStarterLicence }}</h4>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="col-sm-6 col-md-2">
        <div class="card card-stats card-round">
            <div class="card-body text-center">

                <div class="numbers">
                    <label class="card-category">
                        @lang('messages.Basic')
                    </label>
                    <h4 class="card-title b text-primary">{{ $availableBasicLicence }}</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-2">
        <div class="card card-stats card-round">
            <div class="card-body text-center">
                <div class="numbers">
                    <label class="card-category">
                        @lang('messages.Advance')
                    </label>
                    <h4 class="card-title b text-primary">{{ $availableAdvanceLicence }}</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-2">
        <div class="card card-stats card-round">
            <div class="card-body text-center">

                <div class="numbers">
                    <label class="card-category">
                        @lang('messages.Premium')
                    </label>
                    <h4 class="card-title b text-primary">{{ $availablePremiumLicence }} </h4>
                </div>

            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-2">
        <div class="card card-stats card-round">
            <div class="card-body text-center">
                <div class="numbers">
                    <label class="card-category">
                        @lang('messages.PremiumPlus')
                    </label>
                    <h4 class="card-title b text-primary">{{ $availablePremPlusLicence }} </h4>
                </div>
            </div>
        </div>
    </div>
</div>
