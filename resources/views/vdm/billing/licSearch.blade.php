@extends("layouts.app")
@section('content')
    <div id="wrapper">
        @include('vdm.billing.licenceList_index')
        <div class="content animate-panel">
            <div class="page-inner">
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif
                @include('vdm.billing.licDetails')
                <div class="hpanel animated zoomIn">
                    <div class="card">
                        <div class="card-body">
                            <table id="billingTable"
                                class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th>
                                            @lang('messages.LicenceID')
                                        </th>
                                        <th>
                                            @lang('messages.VehicleID')
                                        </th>
                                        <th>
                                            @lang('messages.Type')
                                        </th>
                                        <th>
                                            @lang('messages.LicenceIssuedDate')
                                        </th>
                                        <th>
                                            @lang('messages.LicenceOnboardDate')
                                        </th>
                                        <th>
                                            @lang('messages.LicenceExpireDate')
                                        </th>
                                        <th>
                                            @lang('messages.Status')
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($licenceList != null)
                                        @foreach ($licenceList as $key => $value)
                                            <tr>
                                                <td>{{ $value }}</td>
                                                <td>{{ Arr::get($vehicleList, $value) }}</td>
                                                <td>{{ Arr::get($licenType, $value) }}</td>
                                                <td>{{ Arr::get($licenceissuedList, $value) }}</td>
                                                <td>{{ Arr::get($licenceOnboardList, $value) }}</td>
                                                <td>{{ Arr::get($licenceExpList, $value) }}</td>
                                                <td>{{ Arr::get($statusList, $value) }}</td>
                                            </tr>
                                        @endforeach
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $(document).ready(function() {
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-uk-pre": function (a) {
                    var ukDatea = a.split('-');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                },

                "date-uk-asc": function (a, b) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-uk-desc": function (a, b) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });
            $('#billingTable').dataTable( {
                'stateSave': true,
                "aoColumns": [
                    null,
                    null,
                    null,
                    { "sType": "date-uk" },
                    { "sType": "date-uk" },
                    { "sType": "date-uk" },
                    null
                ]
            });
        });
    </script>
@endpush
