@extends("layouts.app")
@section('content')
    <div id="wrapper">
        @include('vdm.billing.licenceList_create')
        {{ Form::open(['url' => 'Billing', 'id' => 'testForm']) }}
        <div class="content animate-panel">
            <div class="page-inner">
                <div class="hpanel">
                    @if (session()->has('message'))
                        <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                            {{ session()->get('message') }}
                        </p>
                    @endif
                    {{ Form::hidden('list', $list, ['id' => 'list']) }}
                    <span class="text-denger"> {{ HTML::ul($errors->all()) }} </span>
                    @include('vdm.billing.licDetails')
                    <div class="card">
                        @if ($list == 'no')
                        <span class="pt-3 px-5">
                            <h5 class="text-danger float-right m-0">
                                *Expiring in 30 days</h5>
                        </span>
                        @endif
                        <div class="card-body ">
                            <div class="overflow-auto">
                                <table id="billingTable"
                                    class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                    <thead>
                                        <tr class="text-center">
                                            <th>
                                                @lang('messages.LicenceID')
                                            </th>
                                            <th>
                                                @lang('messages.VehicleID')
                                            </th>
                                            <th>
                                                @lang('messages.Type')
                                            </th>
                                            <th>
                                                @lang('messages.DeviceID')
                                            </th>
                                            <th>
                                                @lang('messages.DeviceModel')
                                            </th>
                                            <th>
                                                @lang('messages.DealerName')
                                            </th>
                                            <th>
                                                @lang('messages.LicenceIssuedDate')
                                            </th>
                                            <th>
                                                @lang('messages.LicenceOnboardDate')
                                            </th>
                                            <th>
                                                @lang('messages.LicenceExpireDate')
                                            </th>
                                            @if ($list == 'no')
                                                <th>
                                                    @lang('messages.Lefttorenewal')
                                                </th>
                                            @endif
                                            <th>
                                                @lang('messages.Action')
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="myTable">

                                        @if ($preLicenceList != null)
                                            @foreach ($preLicenceList as $key => $value)
                                                <tr>

                                                    <td>{{ $value }}</td>
                                                    <td>{{ Arr::get($vehicleList, $value) }}</td>
                                                    <td>{{ Arr::get($LtypeList, $value) }}</td>
                                                    <td>{{ Arr::get($deviceList, $value) }}</td>
                                                    <td>{{ Arr::get($deviceModelList, $value) }}</td>
                                                    <td>{{ Arr::get($dealerList, $value) }}</td>
                                                    <td>{{ Arr::get($licenceissuedList, $value) }}</td>
                                                    <td>{{ Arr::get($LicenceOnboardList, $value) }}</td>
                                                    <td>{{ Arr::get($LicenceExpiryList, $value) }}</td>
                                                    @if ($list == 'no')
                                                        <td>{{ Arr::get($LeftofLicence, $value) }}-Days</td>
                                                    @endif
                                                    @if ($list == 'no')
                                                        <td><a class="btn btn-success confirmation btn-sm"
                                                                href="{{ URL::to('billing/Renewal/' . $value) }}">
                                                            @lang('messages.Renew')
                                                            </a>
                                                        </td>
                                                    @else
                                                        <td><a class="btn btn-danger confirmation btn-sm"
                                                                href="{{ URL::to('billing/Cancel/' . Arr::get($vehicleList, $value)) }}">
                                                            @lang('messages.CANCEL')
                                                            </a>
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection
@push('js')
    <script>
        var elems = document.getElementsByClassName('confirmation');
        var confirmIt = async function(e) {
            e.preventDefault();

            let list = $('#list').val();
            let txt = list == 'no' ? 'renewal':'Cancel';
            if (await confirmAlert(`Are you sure to ${txt} ?`)){
                location =  e.target.href;
            }

        };
        for (var i = 0, l = elems.length; i < l; i++) {
            elems[i].addEventListener('click', confirmIt, false);
        }
    </script>
    <script type="text/javascript">
        
        if (!!window.performance && window.performance.navigation.type === 2) {
            window.location.reload();
        }
        $("#checkAll").click(function() {
            $(".check").prop('checked', $(this).prop('checked'));
        });

        $('#testForm').on('submit', function(e) {

            var chkArray = [];

            $(".check:checked").each(function() {
                chkArray.push($(this).val());
            });
            var selected;
            selected = chkArray.join(', ');
            if (selected.length> 0) {
                if (!confirm('Are you sure to renewal ? ' + selected)) e.preventDefault();
            } else {
                e.preventDefault()
                alert("Please at least check one of the Licence");
            }

        });
        $(document).ready(function() {
            var list = $('#list').val();
            if (list == 'no') {
                $("#bala4").addClass("active");
            } else {
                $("#bala3").addClass("active");
            }
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-uk-pre": function (a) {
                    var ukDatea = a.split('-');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                },

                "date-uk-asc": function (a, b) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-uk-desc": function (a, b) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });
            $('#billingTable').dataTable( {
                'stateSave': true,
                "aoColumns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    { "sType": "date-uk" },
                    { "sType": "date-uk" },
                    { "sType": "date-uk" },
                    null
                ]
            });
        });
    </script>
    {{-- @include('includes.js_create') --}}
@endpush
