<nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-3">
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02"
        aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor02">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ \Request::is('*Billing') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('Billing.index') }}">
                    @lang('messages.Licences')
                </a>
            </li>
            <li class="nav-item {{ \Request::is('*billing/prerenewal') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('billing/prerenewal') }}">
                    @lang('messages.Renew')
                </a>
            </li>
            <li class="nav-item {{ \Request::is('*billing/preRenewalList') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('billing/preRenewalList') }}">
                    @lang('messages.Pre-Renewed')
                </a>
            </li>
            <li class="nav-item {{ \Request::is('*billing/expired') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('billing/expired') }}">
                    @lang('messages.Expired')
                </a>
            </li>
            <li class="nav-item {{ \Request::is('*billing/convert') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('billing/convert') }}">
                    @lang('messages.Shuffle-Licences')
                </a>
            </li>
        </ul>
        {{ Form::open(['url' => 'licence/search', 'class' => 'form-inline m-0']) }}
        <div class="input-group">
            <input class="form-control" type="search" placeholder="{{ __('messages.LicenceId') }}"
                aria-label="Search" name="licSearch">
            <div class="input-group-append">
                <button class="btn btn-dark my-2 my-sm-0 btn-sm" type="submit"><i
                        class="fas fa-search fa-lg"></i></button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</nav>
