@extends("layouts.app")
@section('content')
    <div id="wrapper">
        @include('vdm.billing.licenceList_create')
        {{ Form::open(['url' => 'Billing', 'id' => 'BillingForm']) }}
        <div class="content animate-panel">
            <div class="page-inner">
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif
                <span class="text-red"> {{ HTML::ul($errors->all()) }}</span>
                @include('vdm.billing.licDetails')
                <div class="hpanel">
                    <div class="card">

                        <div class="card-body">
                            <div class="overflow-auto">
                                <table id="billingTable" class="table table-bordered dataTable table-head-bg-info">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value=""
                                                            id="checkAll">
                                                        <span class="form-check-sign"></span>
                                                    </label>
                                                </div>
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.LicenceID')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.VehicleID')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.Type')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.DeviceID')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.DeviceModel')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.LicenceIssuedDate')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.LicenceOnboardDate')
                                            </th>
                                            <th style="text-align: center;">
                                                @lang('messages.LicenceExpireDate')
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="myTable" style="color: red;">
                                        @if ($licenceExpList != null)
                                            @foreach ($licenceExpList as $key => $value)
                                                <tr style="text-align: center;">
                                                    @if (in_array($value, $ExpiredLicenceList))
                                                        <td>
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    <input name="renewalLicence[]"
                                                                        class="form-check-input check" type="checkbox"
                                                                        value="{{ htmlspecialchars($value) }}">
                                                                    <span class="form-check-sign"></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                    @else
                                                        <td>
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    <input class="form-check-input" type="checkbox"
                                                                        onclick="return false;">
                                                                    <span class="form-check-sign"></span>
                                                                </label>
                                                            </div>
                                                            <input type="checkbox" onclick="return false;" />
                                                        </td>
                                                    @endif
                                                    <td>{{ $value }}</td>
                                                    <td>{{ Arr::get($vehicleList, $value) }}</td>
                                                    <td>{{ Arr::get($LtypeList, $value) }}</td>
                                                    <td>{{ Arr::get($deviceList, $value) }}</td>
                                                    <td>{{ Arr::get($deviceModelList, $value) }}</td>
                                                    <td>{{ Arr::get($licenceissuedList, $value) }}</td>
                                                    <td>{{ Arr::get($LicenceOnboardList, $value) }}</td>
                                                    <td>{{ Arr::get($LicenceExpiryList, $value) }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        <script>
                                            var elems = document.getElementsByClassName('confirmation');
                                            var confirmIt = function(e) {
                                                if (!confirm('Are you sure to renewal ?')) e.preventDefault();
                                            };
                                            for (var i = 0, l = elems.length; i < l; i++) {
                                                elems[i].addEventListener('click', confirmIt, false);
                                            }
                                        </script>
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center">
                                <input class="btn btn-primary" id="sub" type="button" value="{{ __('messages.RENEW') }}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#bala2").addClass("active");
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-uk-pre": function(a) {
                    var ukDatea = a.split('-');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                },

                "date-uk-asc": function(a, b) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-uk-desc": function(a, b) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });
            $('#billingTable').dataTable({
                'stateSave': true,
                "aoColumns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    {
                        "sType": "date-uk"
                    },
                    {
                        "sType": "date-uk"
                    },
                    {
                        "sType": "date-uk"
                    },
                    null
                ]
            });
        });
        if (!!window.performance && window.performance.navigation.type === 2) {
            window.location.reload();
        }
        $("#checkAll").click(function() {
            $(".check").prop('checked', $(this).prop('checked'));
        });

        async function submitForm(e) {
            $('#sub').attr('value', 'Loading..');
            var chkArray = [];

            $(".check:checked").each(function() {
                chkArray.push($(this).val());
            });
            var selected;
            selected = chkArray.join(', ');
            if (selected.length > 0) {
                let sure = await confirmAlert('{{ __('messages.Areyousuretorenewal') }} ? ' + selected);
                if (!sure) {
                    $('#sub').attr('value', 'RENEW');
                    return false;
                }
            } else {
                wornigAlert("{{ __('messages.PleaseatleastcheckoneoftheLicence') }}");
                $('#sub').attr('value', 'RENEW');
                return false;
            }
            $('#BillingForm').submit();

        }

        $('#sub').on('click', submitForm);
    </script>
@endpush
