@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animate-panel">
            @if (session()->has('message'))
                <p class="alert {!! session()->get('alert-class', 'alert-success') !!}">
                    {!! session()->get('message') !!}
                </p>
            @endif
            <div class="page-inner">
                <div class="page-header">
                    <div class="page-title">
                        @lang('messages.'.ucwords($type).'Audit')
                    </div>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.Audits')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.'.ucwords($type).'Audit')
                                {{-- @lang('messages.VehicleAudit') --}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card">
                    <div class="card-body">
                        @if(!empty($header))
                            <div class="table-responsive">
                                <table id="auditTable"
                                    class="table table-striped table-hover w-100 table-head-bg-primary table-bordered">
                                    <thead>
                                        <tr>
                                            @foreach ($header as $key => $value)
                                                @if ($value == 'oldData' || $value == 'newData')
                                                    @if ($value == 'oldData')
                                                        <th class="text-uppercase">
                                                            Old Data to New data
                                                        </th>
                                                    @endif
                                                @else
                                                    <th class="text-uppercase">
                                                        {{ $value }}
                                                    </th>
                                                @endif
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($auditDatas as $auditData)
                                            <tr>
                                                @foreach ($auditData->toArray() as $key => $data)
                                                    @if ($key == 'oldData' || $key == 'newData')
                                                        @if ($auditData->toArray()['type'] == 'User')
                                                            @if ($key == 'oldData')
                                                                <td class="text-left">
                                                                    @php
                                                                        $oldData = json_decode($data, true);
                                                                        // $newData = json_decode($auditData['newData'], true);
                                                                    @endphp
                                                                    @if ($oldData !== null)
                                                                        @foreach ($oldData as $key => $item)
                                                                            <span
                                                                                class="b">{{ $key }}</span>
                                                                            : {{ $item }} <br>
                                                                        @endforeach
                                                                    @endif
                                                                </td>
                                                            @elseif($key == 'newData')
                                                                <td class="text-left">
                                                                    @php
                                                                        $newData = json_decode($data, true);
                                                                    @endphp
                                                                    @if ($newData !== null)
                                                                        @foreach ($oldData as $key => $item)
                                                                            <span class="b">{{ $key }}</span>
                                                                            : {{ $item }} <br>
                                                                        @endforeach
                                                                    @endif
                                                                </td>
                                                            @else
                                                                {{-- {{$data}} --}}
                                                            @endif
                                                        @else
                                                            @if ($key == 'oldData')
                                                                <td class="text-left">
                                                                    @php
                                                                        $oldData = json_decode($data, true);
                                                                        $newData = json_decode($auditData['newData'], true);
                                                                    @endphp
                                                                    @if ($oldData !== null || $newData !== null)
                                                                        @foreach ($newData as $key => $item)
                                                                            @php
                                                                                $oldIdem = $oldData[$key] ?? 'nill';
                                                                                if ($oldIdem == '') {
                                                                                    $oldIdem = 'nill';
                                                                                }
                                                                                $newIdem = $newData[$key] ?? 'nill';
                                                                                if ($newIdem == '') {
                                                                                    $newIdem = 'nill';
                                                                                }
                                                                            @endphp
                                                                            <span class="pl-3">
                                                                                <strong>
                                                                                    {{ $key }} :
                                                                                </strong>
                                                                                {{ "$oldIdem => $newIdem" }}
                                                                            </span><br>
                                                                        @endforeach
                                                                    @endif
                                                                </td>
                                                            @endif
                                                        @endif
                                                    @else
                                                        <td>
                                                            {{ $data }}
                                                        </td>
                                                    @endif
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="pull-right">
                                    @if ($auditDatas !== [])
                                        {{ $auditDatas->links() }}
                                    @endif
                                </div>
                            </div>
                        @else
                            <div class="text-center mh-100">
                                <span class="h1 text-primary">
                                    <i class="fas fa-frown"></i>
                                    No Data
                                </span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('js')
    <script>
        $(function() {
            $('.splash').hide();
            $('#auditTable').dataTable({
                "bPaginate": false,
                "bInfo": false,
                // "aaSorting": [
                //     [7, "desc"]
                // ]
            });
        })
    </script>
@endpush
