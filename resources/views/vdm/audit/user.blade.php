@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animate-panel">
            @if (session()->has('message'))
                <p class="alert {!! session()->get('alert-class', 'alert-success') !!}">
                    {!! session()->get('message') !!}
                </p>
            @endif
            <div class="page-inner">
                <div class="page-header">
                    <div class="page-title">
                        @lang('messages.'.ucwords($type).'Audit')
                    </div>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.Audits')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.'.ucwords($type).'Audit')
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="auditTable"
                                class="table table-striped table-hover w-100 table-head-bg-primary table-bordered">
                                <thead>
                                    <tr>
                                        @foreach ($header as $key => $value)
                                            @if ($value == 'oldData' || $value == 'newData')
                                                @if ($value == 'oldData')
                                                    <th class="text-uppercase">
                                                        Old Data
                                                    </th>
                                                @else
                                                    <th class="text-uppercase">
                                                        new Data
                                                    </th>
                                                @endif
                                            @else
                                                <th class="text-uppercase">
                                                    {{ $value }}
                                                </th>
                                            @endif
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($auditDatas as $auditData)
                                        <tr>
                                            @foreach ($auditData->toArray() as $key => $data)
                                                @if ($key == 'oldData' || $key == 'newData')
                                                    <td class="text-left">
                                                        @php
                                                            $data = json_decode($data, true);
                                                            $spList = ['addedGroups','removedGroups','addedNotification','removedNotification','addedReports','removedReports','reports'];
                                                        @endphp
                                                        @if ($data !== null)
                                                            @foreach ($data as $key => $item)
                                                                <span class="b">
                                                                    {{ ucwords($key) }} : 
                                                                </span>
                                                                @if (in_array($key,$spList))
                                                                    @if ($item !== '')
                                                                        @foreach (explode(',', $item) as $value)
                                                                            {{ $value }} , <br>
                                                                        @endforeach
                                                                    @else
                                                                        - , <br>
                                                                    @endif
                                                                @else
                                                                    {!! $item !!} ,<br>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                @else
                                                    <td>
                                                        {{ $data }}
                                                    </td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pull-right">
                                {{ $auditDatas->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('js')
    <script>
        $(function() {
            let dataTableLang = $("#langJson").val()
            $('.splash').hide();
            $('#auditTable').dataTable({
                "bPaginate": false,
                "bInfo": false,
                'stateSave': true,
                "aaSorting": [
                    [7, "desc"]
                ],
                "language": {
				"url": dataTableLang
			    },
            });
        })
    </script>
@endpush
