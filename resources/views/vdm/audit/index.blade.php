@extends("layouts.app")
@section('content')

    <div id="wrapper">
        <div class="content animate-panel">
            @if (session()->has('message'))
                <p class="alert {!! session()->get('alert-class', 'alert-success') !!}">{!! session()->get('message') !!}</p>
            @endif
            <div class="page-inner">
                <div class="page-header">
                    <div class="page-title">
                        @if ($model == 'AuditDealer')
                            @lang('messages.AuditDealers')
                        @elseif($model=='AuditVehicle')
                            @lang('messages.AuditVehicles')
                        @elseif($model=='AuditUser')
                            @lang('messages.AuditUsers')
                        @elseif($model=='RenewalDetails')
                            @lang('messages.AuditRenewal')
                        @elseif($model=='AuditFrans')
                            @lang('messages.AuditAdmin')
                        @elseif($model=='AuditOrg')
                            @lang('messages.AuditOrganization')
                        @elseif($model=='AuditGroup')
                            @lang('messages.AuditGroups')
                        @elseif($model=='loginCustomisation')
                            @lang('messages.LoginCustomisation')
                        @elseif($model=='CustomizeLandingPage')
                            @lang('messages.CustomizeLandingPage')
                        @elseif($model=='AuditManualCalibrate')
                            @lang('messages.AuditManualCalibrate')
                        @elseif($model=='AuditAutoCalibrate')
                            @lang('messages.AuditAutoCalibrate')
                        @elseif($model=='AuditMoveVehicle')
                            @lang('messages.MoveVehiclesAudit')
                        @else
                            @lang('messages.Audit')
                        @endif
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            @if ($model != 'RenewalDetails')
                                <table id="auditTable"
                                    class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                            @endif
                            @if ($model == 'RenewalDetails')
                                <table id="auditTable"
                                    class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                            @endif
                            <thead>
                                <tr>
                                    @foreach ($columns as $key => $value)
                                        @if ($value == 'country')
                                            <th style="text-transform: capitalize;">CountryTimeZone</th>
                                        @else
                                            <th id="<?php echo $value; ?>1"
                                                style="text-transform: capitalize;  {{ $value == 'reports' ? 'min-width:350px !important;' : '' }}">
                                                {!! $value !!}</th>
                                        @endif
                                    @endforeach

                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @foreach ($rows as $rowId => $row)
                                    @if ($row['status'] == config()->get('constant.created') && $model == 'AuditUser')
                                        <tr>
                                            @foreach ($columns as $key => $value)
                                                @if ($value == 'reports')
                                                    <td>
                                                        <div>
                                                            @foreach ($AllReport[$rowId] as $reportTitle => $Reports)

                                                                @if (count($Reports) > 0)
                                                                    <input class="btn btn-info btn-sm text-white"
                                                                        type="button"  value="{!! $reportTitle !!}"
                                                                        data-target="#{{ $reportTitle . $rowId }}"
                                                                        onclick="report(this)"
                                                                        style="margin-top: 2%;">
                                                                    

                                                                    <div id="{{ $reportTitle . $rowId }}" class="d-none"
                                                                        >
                                                                        <div>
                                                                            @foreach ($Reports as $report)
                                                                               <span>
                                                                                    {!! $report !!}
                                                                                </span><br>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>


                                                                @endif

                                                            @endforeach
                                                        </div>
                                                    </td>
                                                @elseif($value=='addedReports')
                                                    <td>
                                                        @foreach ($AddedReportArr[$rowId] as $reportTitle => $Reports)

                                                            @if (count($Reports) > 0)
                                                                <div>

                                                                    <input class="btn btn-success btn-sm text-white"
                                                                        type="button" value="{{ $reportTitle }}"
                                                                        style="margin-top: 5%;" 
                                                                        data-target="#{{ $reportTitle . $rowId }}add"
                                                                        onclick="report(this)">

                                                                    <div id="{{ $reportTitle . $rowId }}add" class="d-none"
                                                                        >
                                                                        <div>
                                                                            @foreach ($Reports as $report)
                                                                               <span>
                                                                                    {!! $report !!}
                                                                                </span><br>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif

                                                        @endforeach
                                                    </td>
                                                @elseif($value=='removedReports')
                                                    <td>
                                                        @foreach ($RemovedReportArr[$rowId] as $reportTitle => $Reports)

                                                            @if (count($Reports) > 0)
                                                                <div>

                                                                    <input class="btn btn-danger btn-sm text-white"
                                                                        type="button" value="{{ $reportTitle }}"
                                                                        style="margin-top: 5%;" 
                                                                        data-target="#{{ $reportTitle . $rowId }}rem"
                                                                        onclick="report(this)">

                                                                    <div id="{{ $reportTitle . $rowId }}rem" class="d-none"
                                                                        >
                                                                        <div>
                                                                            @foreach ($Reports as $report)
                                                                               <span>
                                                                                    {!! $report !!}
                                                                               </span><br>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            @endif

                                                        @endforeach
                                                    </td>

                                                @else
                                                   
                                                        <td class="text-break ">{!! $row[$value] !!}</td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @elseif($row['status']==config()->get('constant.created'))
                                        <tr>
                                            @foreach ($columns as $key => $value)                                               
                                                    <td class="text-break ">{!! $row[$value] !!}</td>
                                            @endforeach
                                        </tr>
                                    @elseif($row['status']==config()->get('constant.updated'))
                                        <tr>
                                            @foreach ($columns as $key => $value)
                                                    <td class="text-break ">{!! $row[$value] !!}</td>
                                            @endforeach
                                        </tr>
                                    @elseif($row['status']==config()->get('constant.deleted'))
                                        <tr>
                                            @foreach ($columns as $key => $value)
                                                    <td class="text-break ">{!! $row[$value] !!}</td>
                                            @endforeach
                                        </tr>
                                    @elseif($row['Status']=='OnBoard')
                                        <tr>
                                            @foreach ($columns as $key => $value)
                                                    <td class="text-break ">{!! $row[$value] !!}</td>
                                            @endforeach
                                        </tr>
                                    @elseif($row['Status']=='Migration')
                                        <tr>
                                            @foreach ($columns as $key => $value)
                                                    <td class="text-break ">{!! $row[$value] !!}</td>
                                            @endforeach
                                        </tr>
                                    @elseif($row['Status']=='Renew')
                                        <tr>
                                            @foreach ($columns as $key => $value)
                                                    <td class="text-break ">{!! $row[$value] !!}</td>
                                            @endforeach
                                        </tr>
                                    @elseif($row['status']==config()->get('constant.disabled'))
                                        <tr>
                                            @foreach ($columns as $key => $value)
                                                    <td class="text-break ">{!! $row[$value] !!}</td>
                                            @endforeach
                                        </tr>
                                    @elseif($row['status']==config()->get('constant.enabled'))
                                        <tr>
                                            @foreach ($columns as $key => $value)
                                                    <td class="text-break ">{!! $row[$value] !!}</td>
                                            @endforeach
                                        </tr>
                                    @elseif($row['status']==config()->get('constant.reportsEdited'))
                                        <tr>
                                            @foreach ($columns as $key => $value)
                                                @if ($value == 'reports')
                                                    <td>
                                                        <div>
                                                            @foreach ($AllReport[$rowId] as $reportTitle => $Reports)

                                                                @if (count($Reports) > 0)
                                                                    <input class="btn btn-info btn-sm text-white"
                                                                        type="button" value="{!! $reportTitle !!}"
                                                                        data-target="#{{ $reportTitle . $rowId }}"
                                                                        style="margin-top: 2%;"
                                                                        onclick="report(this)">
                                                                   

                                                                    <div id="{{ $reportTitle . $rowId }}" class="d-none"
                                                                        >
                                                                        <div>
                                                                            @foreach ($Reports as $report)

                                                                               <span>
                                                                                    {!! $report !!}
                                                                                </span><br>

                                                                            @endforeach
                                                                        </div>
                                                                    </div>


                                                                @endif

                                                            @endforeach
                                                        </div>
                                                    </td>
                                                @elseif($value=='addedReports')
                                                    <td>
                                                        @foreach ($AddedReportArr[$rowId] as $reportTitle => $Reports)

                                                            @if (count($Reports) > 0)
                                                                <div>
                                                                    <input class="btn btn-success btn-sm text-white"
                                                                        type="button" value="{{ $reportTitle }}"
                                                                        style="margin-top: 5%;" 
                                                                        data-target="#{{ $reportTitle . $rowId }}add"
                                                                        onclick="report(this)">

                                                                    <div id="{{ $reportTitle . $rowId }}add" class="d-none">
                                                                        <div>
                                                                            @foreach ($Reports as $report)
                                                                               <span>
                                                                                    {!! $report !!}
                                                                                </span><br>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif

                                                        @endforeach
                                                    </td>
                                                @elseif($value=='removedReports')
                                                    <td>
                                                        @foreach ($RemovedReportArr[$rowId] as $reportTitle => $Reports)

                                                            @if (count($Reports) > 0)
                                                                <div>

                                                                    <input class="btn btn-danger btn-sm text-white"
                                                                        type="button" value="{{$reportTitle}}"
                                                                        style="margin-top: 5%;" 
                                                                        data-target="#{{ $reportTitle . $rowId }}rem"
                                                                        onclick="report(this)">

                                                                    <div id="{{ $reportTitle . $rowId }}rem" class="d-none"
                                                                        >
                                                                        <div>
                                                                            @foreach ($Reports as $report)
                                                                               <span>
                                                                                    {!! $report !!}
                                                                                </span><br>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </td>

                                                @else
                                                     <td class="text-break ">{!! $row[$value] !!}</td>
                                                @endif

                                            @endforeach
                                        </tr>
                                    @else
                                        <tr>
                                            @foreach ($columns as $key => $value)
                                                    <td class="text-break ">{!! $row[$value] !!}</td>
                                            @endforeach
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

    {{-- @include('includes.js_create') --}}

    <script type="text/javascript">
        window.onload = function() {
            var tabIn = document.getElementsByTagName('th').length - 1;
            var id = 0;
            $('#auditTable').dataTable({
                "aaSorting": [
                    [tabIn, "desc"]
                ]
            });
        }

        function getReort(id){
            return document.querySelector(id)
        }
        function report(val) {
            id = $(val).attr("data-target");
            header =$(val).attr("value");
            temp = $('<div></div>').attr({'id':'temp','class':'temp'}).html($(id).html())
            $('body').append(temp);
            Swal.fire({
                title: header,
                html: getReort('#temp'),
            }).then(function(){
                $('#temp').remove();
            });
            // $(id).html(list);
        }
    </script>
@endpush
