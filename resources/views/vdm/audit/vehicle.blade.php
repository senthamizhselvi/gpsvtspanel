@extends("layouts.app")
@section('content')
<div id="wrapper">
    <div class="content animate-panel">
        @if (session()->has('message'))
            <p class="alert {!! session()->get('alert-class', 'alert-success') !!}">
                {!! session()->get('message') !!}
            </p>
        @endif
        <div class="page-inner">
            <div class="page-header">
                <div class="page-title">
                    @lang('messages.VehicleAudit')
                </div>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">
                            @lang('messages.VehicleAudit')
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="auditTable"  class="table table-striped table-hover w-100 table-head-bg-primary table-bordered">
                            <thead>
                                <tr>
                                    @foreach ($header as $key => $value)
                                            @if($value == "oldData" || $value == "newData")
                                                @if($value == "oldData")
                                                    <th  class="text-uppercase">
                                                        Old Data to New data  
                                                    </th>                                            
                                                @endif
                                            @else
                                                <th  class="text-uppercase">
                                                    {{$value}}
                                                </th>
                                            @endif
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($auditDatas as $auditData)
                                    <tr>
                                        @foreach ($auditData->toArray() as $key =>$data)
                                                @if($key == "oldData" || $key == "newData")
                                                    @if ($key == "oldData")
                                                        <td @if($key == "oldData" || $key == "newData") class="text-left "@endif>
                                                            @php
                                                                $oldData = json_decode($data,true);
                                                                $newData = json_decode($auditData['newData'],true);
                                                            @endphp
                                                            @if($oldData !== null || $newData !== null)
                                                                @foreach ($oldData as $key=>$item)
                                                                    @php
                                                                        $oldIdem = $item ?? "nill";
                                                                        if($oldIdem == "") $oldIdem = "nill";
                                                                        $newIdem = $newData[$key]??"nill";
                                                                        if($newIdem == "") $newIdem = "nill";
                                                                    @endphp
                                                                    <span class="pl-3"><strong>{{$key}} : </strong>{{"$oldIdem => $newIdem"}}</span><br>
                                                                @endforeach
                                                            @endif
                                                            </td>
                                                    @endif
                                                @else
                                                    <td>
                                                        {{$data}}
                                                    </td>
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pull-right">
                            {{$auditDatas->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('js')
    <script>
        $(function(){
            $('.splash').hide();
            $('#auditTable').dataTable({
                "bPaginate": false,
                "bInfo":false,
                "aaSorting": [
                    [7, "desc"]
                ]
            });
        })
    </script>
@endpush