<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login V17</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href={{ URL::asset('imgs/icons/favicon.ico') }} />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{ URL::asset('vendor/bootstrap2/css/bootstrap.min.css') }}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{ URL::asset('fonts1/font-awesome-4.7.3/css/font-awesome.min.css') }}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{ URL::asset('fonts1/Linearicons-Free-v1.0.2/icon-font.min.css') }}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{ URL::asset('vendor/animate2/animate.css') }}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{ URL::asset('vendor/css-hamburgers3/hamburgers.min.css') }}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{ URL::asset('vendor/animsition2/css/animsition.min.css') }}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{ URL::asset('vendor/select23/select2.min.css') }}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{ URL::asset('vendor/daterangepicker3/daterangepicker.css') }}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{ URL::asset('css/util4.css') }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset('css/main4.css') }}>
    <!--===============================================================================================-->
</head>
<style type="text/css">
    *::-webkit-scrollbar {
            width: 8px;
            height: 8px;
        }

        *::-webkit-scrollbar-track {
            background-color: #e4e4e4;
            border-radius: 100px;
        }

        *::-webkit-scrollbar-thumb {
            border-radius: 100px;
            background-image: linear-gradient(180deg, #a19f9f 0%, #a19f9f 99%);
            box-shadow: inset 2px 2px 5px 0 rgba(#fff, 0.5);
        }
        
    input[type="color"] {
        -webkit-appearance: none;
        width: 60%;
        height: 30px;
        border: 0;
        border-radius: .5rem;
        padding: 0;
        overflow: hidden;
        box-shadow: 2px 2px 5px rgba(0, 0, 0, .1);
    }

    input[type="color"]::-webkit-color-swatch-wrapper {
        padding: 0;
    }

    input[type="color"]::-webkit-color-swatch {
        border: none;
    }

    h3 {
        font-family: Poppins-Bold;
        line-height: 1.2;
    }

    #wrapper {
        float: left;
        background: #fff;
        box-shadow: #9990;
        border-radius: 25px;
        color: white;
    }

    label {
        margin-bottom: .5rem;
        font-family: Poppins-Bold;
        line-height: 1.2;
    }

</style>

<body>
    <div id="styleDiv">
        <style>
        </style>
    </div>
    {{ Form::open(['url' => '/Upload', 'enctype' => 'multipart/form-data', 'id' => 'frmSub']) }}
    {{ Form::hidden('dealerName', $dealerName, ['id' => 'dealerName']) }}
    <div class="container" id="wrapper"
        style="width: 35%; float: left; background: -webkit-linear-gradient(top, #7579ff, #b224ef); border: 1px solid #212229;margin: 1% 3%;color: white;">
        <h2 style="text-align: center;font-weight: bold;color: #f8f9fa;background-color: #9990;">Custom Design</h2>
        <div class="row">
            <div class='col-sm-5'>Background Color :</div>
            <div class='col-sm-6'><input type="color" id="color" style="border-radius: 5%;width: 32%;"></div>
        </div> <br />
        <div class="row">
            <div class='col-sm-5'>Font Color :</div>
            <div class='col-sm-6'><input type="color" id="font_color" style="border-radius: 5%;width: 32%;"></div>
        </div> <br />
        <input type="hidden" name="bgcolor" id="bg_color">
        <input type="hidden" name="fontcolor" id="ft_color">
        <input type="hidden" value="{{ csrf_token() }}" name="_token">
        <!--<input type="hidden" name="web_address" value="live.vamosys.com1">-->
        <input type="hidden" name="template_name" value="3">
        <div class="row">
            <div class='col-sm-5'>Background Image :</div>
            <div class='col-sm-6'><input type="file" id="myFile" name="background" class="btn1"
                    onchange="myFunction(this)" accept="image/x-png,image/gif,image/jpeg"></div>
        </div> <br />
        @if ($errors->has('background'))
            <span class="help-block">
                <strong>{{ $errors->first('background') }}</strong>
            </span>
        @endif

        <div class="row">
            <div class='col-sm-5'>Logo:</div>
            <div class='col-sm-6'><input type="file" name="logo" id="myFile1" class="btn1" name="logo"
                    onchange="readURL(this)" accept="image/x-png,image/gif,image/jpeg"></div>
        </div>
        @if ($errors->has('logo'))
            <span class="help-block">
                <strong>{{ $errors->first('logo') }}</strong>
            </span>
        @endif
        <div class="row" style="padding-top: 35px;margin-left: 40%;"><input type="submit" class="btn green"
                value="Upload" name="submit"></div>
        {{ Form::close() }}
    </div>
    <div class="row">
        <div class="limiter">
            <div class="bgimg" style="width: 100%">
                <div class="container-login100" id="grid">
                    <div class="wrap-login100" id="artiststhumbnail">
                        <div class="login100-more"
                            style="background-image: url('{{ URL::asset('assets/imgs/bg-04.png') }}');">
                        </div>
                        <form class="login100-form validate-form">
                            <img id="blah" class="login-logo login-6" src="{{ URL::asset('assets/imgs/logo1.png') }}"
                                style="margin-top: -164px;">
                            <span class="login100-form-title p-b-34">
                                Login
                            </span>

                            <div class="wrap-input100 validate-input" data-validate="Type user name">
                                <input id="first-name" class="input100" type="text" name="username"
                                    placeholder="User name">

                            </div>
                            <div class="wrap-input100 validate-input" data-validate="Type password" style="margin-top: 13px;
    margin-bottom: 13px;">
                                <input class="input100" type="password" name="pass" placeholder="Password">

                            </div>

                            <div class="container-login100-form-btn">
                                <button class="login100-form-btn">
                                    Sign in
                                </button>
                            </div>

                            <div class="w-full text-center p-t-27 p-b-239">
                                <span class="txt1">
                                    Forgot
                                </span>

                                <a href="#" class="txt2">
                                    User name / password?
                                </a>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script type="text/javascript">
        $('#frmSub').on('submit', function(e) {
            var bgIMG = $('#myFile').val();
            var bgLogo = $('#myFile1').val();
            if (bgIMG == '') {
                alert("Select the background Image");
                return false;
            } else if (bgLogo == '') {
                alert("Select the Logo Image");
                e.preventDefault();
                return false;
            } else {
                var extensionLogo = bgLogo.split('.').pop().toUpperCase();
                var extension = bgIMG.split('.').pop().toUpperCase();
                if (extension != "BMP" && extension != "PNG" && extension != "JPEG") {
                    e.preventDefault();
                    alert("Background image invalid extension " + extension);
                    return false;
                }
                if (extensionLogo != "BMP" && extensionLogo != "PNG" && extensionLogo != "JPEG") {
                    e.preventDefault();
                    alert("Logo invalid extension " + extensionLogo);
                    return false;
                }
            }

        });
        var grid = document.getElementById("grid");
        color_input = document.getElementById("color");
        color_input.addEventListener("change", function() {
            var newdiv = document.createElement("div");
            grid.appendChild(newdiv);
            newdiv.style.backgroundColor = color_input.value;
            $(".login100-form").css('background', color_input.value)
            $("#bg_color").val(color_input.value);
        });
        font_color = document.getElementById("font_color");
        font_color.addEventListener("change", function() {
            var newdiv = document.createElement("div");
            grid.appendChild(newdiv);
            newdiv.style.backgroundColor = font_color.value;
            $(".bgimg").css('color', font_color.value)
            $("input::-webkit-input-placeholder").css('color', font_color.value);
            $("#ft_color").val(font_color.value);
            change_placeholder_color(font_color.value);
        });
        function change_placeholder_color(color_choice) {
            $("#styleDiv>style").remove();
            $("#styleDiv").append("<style>" + "" + "::placeholder{color:" +  color_choice + "}</style>")
            $("#styleDiv").append("<style>" + "" + ".focus-input100::after{color:" +  color_choice + "}</style>")
            $("#styleDiv").append("<style>" + "" + ".focus-input100::before{background:" +  color_choice + "}</style>")
        }

        function myFunction(input) {
            var file = document.getElementById("myFile").files[0];
            url = URL.createObjectURL(file);
            $(".login100-more").css('background', "url(" + url + ")");
        }

        function readURL(input) {
            console.log("test");
            if (input.files && input.files[0]) {
                url = URL.createObjectURL(input.files[0]);
                $('#blah')
                    .attr('src', url)
                    .width(150)
                    .height(150);
            }
        }
    </script>
    <style type="text/css">
        h5 {
            color: #999999;
            font-family: arial, sans-serif;
            font-size: 16px;
            font-weight: bold;
            margin-top: 0px;
            margin-bottom: 1px;
        }

        #wrapper {

            width: 30%;
            float: left;
            border: 1px solid #999;
            margin: 0 4px;
            padding: 5px;
            height: auto;
            background-color: #CCC;

        }

        #artiststhumbnail img {
            display: block;
            margin: auto;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        * {
            box-sizing: border-box;
        }

        input[type=text],
        select,
        textarea {
            width: 100%;

            border-radius: 4px;
            box-sizing: border-box;
            margin-top: 6px;
            margin-bottom: 16px;
            resize: vertical;
        }

        input[type=submit] {
            background-color: #4CAF50;
            color: white;
            padding: 12px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        .container {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
        }

        #wrapper h2 {
            text-align: center;
            font-weight: bold;
            color: #000;
            background-color: #999;
        }

    </style>


    <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
    <script src={{ URL::asset('vendor/jquery3/jquery-3.2.1.min.js') }}></script>
    <!--===============================================================================================-->
    <script src={{ URL::asset('vendor/animsition2/js/animsition.min.js') }}></script>
    <!--===============================================================================================-->
    <script src={{ URL::asset('vendor/bootstrap2/js/popper.js') }}></script>
    <script src={{ URL::asset('vendor/bootstrap2/js/bootstrap.min.js') }}></script>
    <!--===============================================================================================-->
    <script src={{ URL::asset('vendor/select23/select2.min.js') }}></script>

    <!--===============================================================================================-->
    <script src={{ URL::asset('vendor/daterangepicker3/moment.min.js') }}></script>
    <script src={{ URL::asset('vendor/daterangepicker3/daterangepicker.js') }}></script>
    <!--===============================================================================================-->
    <script src={{ URL::asset('vendor/countdowntime3/countdowntime.js') }}></script>
    <!--===============================================================================================-->
    <script src={{ URL::asset('js/main3.js') }}></script>

</body>

</html>
