<!DOCTYPE html>
<html lang="en">
<head>
	<title>GPS</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstraplogin1/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0login1/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animatelogin1/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgerslogin1/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2login1/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/utillogin1.css">
	<link rel="stylesheet" type="text/css" href="css/mainlogin1.css">
<!--===============================================================================================-->
<style type="text/css">
/*  request()->placeholder {
  color: {{$fcolor}};
}*/


.modal-content {
    background: linear-gradient(-135deg, {{$bcolor}}, {{$fcolor}});
    margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    border-radius: 12px;
    width: 330px;
    min-height: 45vh;
    max-height: 100vh;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
.cff img {
  position:absolute;
  /*left:20px;*/
  -webkit-transition: opacity 1s ease-in-out;
  -moz-transition: opacity 1s ease-in-out;
  -o-transition: opacity 1s ease-in-out;
  transition: opacity 1s ease-in-out;
}

.cff img.top:hover {
  opacity:0;
}
</style>
</head>
<body  onload="callMe()">
	<div class="limiter">
		<div class="container-login100" style="background: linear-gradient(-135deg, {{$bcolor}}, {{$fcolor}});">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="{{$logo}}" alt="IMG" style="width:316px;height: 289px;">
				</div>

					{{ Form::open(array('url' => route('dologin'),'class' => 'login100-form validate-form')) }}
					<span class="login100-form-title">
						Member Login
					</span>
					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="username" placeholder="Username" id="userIds">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit" id="clickme">
							Login
						</button>
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							Forgot
						</span>
						<a class="txt2"  href="password/resetingWin" style="cursor: pointer;">
							 Password?
						</a>@if($url=="yes")<a href="/gps/public/apiAcess" class="txt1" target="blank" > | API Access |</a>&nbsp;<a href="/gps/public/faq" class="txt1" target="blank">FAQ</a>@endif
					</div>
        @if($url=="yes")
     <div class="row" style="margin-top: 2%; margin-left: 2%">
       <div class="col-sm-2 cff">
          <a target="_blank" href="https://play.google.com/store/apps/details?id=com.gpsvts"><img class="bottom" src="/gps/public/assets/imgs/andG.png" style="width: 25px; height: 25px"/></a>
          <a target="_blank" href="https://play.google.com/store/apps/details?id=com.gpsvts"><img class="top" src="/gps/public/assets/imgs/android1.png" style="width: 25px; height: 25px"/></a>
       </div>
       <div class="col-sm-2 cff">
          <a target="_blank" href="https://itunes.apple.com/in/app/gpsvts/id1038796257?mt=8"><img class="bottom" src="/gps/public/assets/imgs/appGy.png" style="width: 25px; height: 25px"/></a>
            <a target="_blank" href="https://itunes.apple.com/in/app/gpsvts/id1038796257?mt=8"><img class="top" src="/gps/public/assets/imgs/appG.png" style="width: 25px; height: 25px"/></a> 
       </div>
       <div class="col-sm-2 cff">
           <a target="_blank" href="https://www.facebook.com/vamosystems"><img class="bottom" src="/gps/public/assets/imgs/facebook1.png" style="width: 25px; height: 25px;"/></a>
            <a target="_blank" href="https://www.facebook.com/vamosystems"><img class="top" src="/gps/public/assets/imgs/facebook.png" style="width: 25px; height: 25px;"/></a>
       </div>
       <div class="col-sm-2 cff">
          <a target="_blank" href="https://twitter.com/gps_vamosystems"><img class="bottom" src="/gps/public/assets/imgs/twitter.png" style="width: 25px; height: 25px;"/></a>
            <a target="_blank" href="https://twitter.com/gps_vamosystems"><img class="top" src="/gps/public/assets/imgs/twitter1.png" style="width: 25px; height: 25px;"/></a>
       </div>
       <div class="col-sm-2 cff"> 
          <a target="_blank" href="https://www.linkedin.com/company/vamo-systems"><img class="bottom" src="/gps/public/assets/imgs/linked1.png" style="width: 25px; height: 25px;"/></a>
            <a target="_blank" href="https://www.linkedin.com/company/vamo-systems"><img class="top" src="/gps/public/assets/imgs/linked3.png" style="width: 25px; height: 25px;"/></a>
       </div>
       <div class="col-sm-2 cff">
         <a target="_blank" href="https://www.youtube.com/channel/UC1LNXPejDkOT7TOgsMltNKg"><img class="bottom" src="/gps/public/assets/imgs/youtube.png" style="width: 25px; height: 25px;"/></a>
            <a target="_blank" href="https://www.youtube.com/channel/UC1LNXPejDkOT7TOgsMltNKg"><img class="top" src="/gps/public/assets/imgs/youtube1.png" style="width: 25px; height: 25px;"/></a>
       </div>
     </div>
     @endif
                    <div class="text-center p-t-12">
						<a class="txt2 text-center" style="color: red;">
							  <?php if(session()->has('flash_notice')): ?>
							<?php echo session()->get('flash_notice') ?>
							 <?php endif; ?>{{ HTML::ul($errors->all()) }}
						</a>{{ HTML::ul($errors->all()) }}
                </div>
					<div class="text-center p-t-136">
						<a class="txt2">
<!--							Copyright © VAMOSYS-->
						</a>
					</div>
				   {{ Form::close() }}
			</div>
		</div>
	</div>
       


 <div id="id01" class="modal">
  <form class="modal-content animate" action="password/resetting">

     <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'"  style="cursor: pointer;" class="close" title="Close">&times;</span>
       <img src="{{$logo}}" alt="IMG" style="width:10vh;">
    </div>
          <div class="wrap-input100" style="margin-left: 5%;width: 90%;">
						<input class="input100" id="usern" type="text" name="uname" placeholder="Enter Username" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>


       <div class="container-login100-form-btn">
            <button class="login100-form-btn" type="submit" style="width: 90%">
             Send Password Reset Link
            </button>
          </div>
   </form>
</div>


<!--===============================================================================================-->	
	<script src="vendor/jquerylogin1/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstraplogin1/js/popper.js"></script>
	<script src="vendor/bootstraplogin1/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2login1/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tiltlogin1/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
   localStorage.clear();
   var globalIP = document.location.host;
 var contextMenu = '/'+document.location.pathname.split('/')[1];
 
  function callMe() {

    var isChrome = !!window.chrome && !!window.chrome.webstore;

        if(isChrome == false && (navigator.userAgent.indexOf('Chrome') == -1)) {
                
          for(i=0;i <= 15; i++) {
            alert("Please Open Site on Google Chrome");
          }
        }
  }

  $('#clickme').click(function() {
      
      var userId  = $('#userIds').val().toUpperCase();
      //var postVal = {'id':userId};
      var userIP=localStorage.getItem('myIP');
      var postVal = {'id':userId,'userIP':userIP};
      localStorage.setItem('lang','en');

    //$.get('http://128.199.159.130:9000/isAssetUser?userId=MSS', function(response) {
      $.get('//'+globalIP+contextMenu+'/public/isAssetUser', function(response) {
          //alert(response);
          localStorage.setItem('isAssetUser', response);
      }).error(function(){
          console.log('error in isAssetUser');
      });

       $.post('{{ route("ajax.fcKeyAcess") }}',postVal)
         .done(function(data) {

          localStorage.setItem('fCode',data);
        //alert(data);
        
        }).fail(function() {
            console.log("fcode fail..");
      });

      localStorage.setItem('userIdName', JSON.stringify('username'+","+userId));
      //var language=$('#lang').val();
      
      var usersID = JSON.stringify(userId);

      if(usersID == '\"BSMOTORS\"' || usersID == '\"TVS\"') {

        window.localStorage.setItem('refreshTime',120000);

      } else {

        window.localStorage.setItem('refreshTime',60000);
      }

  });

  $('#userIds').on('change', function() {
    
    var postValue = {
      'id': $(this).val().toUpperCase()

      };
    // alert($('#groupName').val());
    $.post('{{ route("ajax.apiKeyAcess") }}',postValue)
      .done(function(data) {
        
        // $('#validation').text(data);
            localStorage.setItem('apiKey', JSON.stringify(data));
            
          }).fail(function() {
            console.log("fail");
      });

    $.post('{{ route("ajax.dealerAcess") }}',postValue)
      .done(function(data) {
        
        //alert(data);
          localStorage.setItem('dealerName', data);
            
      }).fail(function() {
          console.log("fail");
    });    

    
  });

// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

</script>
</html>