<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login V3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('fonts1/font-awesome-4.7.1/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('fonts1/iconic/css/material-design-iconic-font.min.css') }}">
    <!--===============================================================================================-->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('vendor/css-hamburgers4/hamburgers.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('vendor/animsition/css/animsition.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/util1.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/main1.css') }}">
    <!--===============================================================================================-->
    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('common/css/atlantis.css') }}">

    <style type="text/css">
        input[type="color"] {
            -webkit-appearance: none;
            width: 60%;
            height: 30px;
            border: 0;
            border-radius: .5rem;
            padding: 0;
            overflow: hidden;
            box-shadow: 2px 2px 5px rgba(0, 0, 0, .1);
        }

        input[type="color"]::-webkit-color-swatch-wrapper {
            padding: 0;
        }

        input[type="color"]::-webkit-color-swatch {
            border: none;
        }

        h3{
            font-family: Poppins-Bold;
            line-height: 1.2;
        }

        #wrapper {
            /* float: left; */
            background: #fff;
            box-shadow: #9990;
            border-radius: 25px;
            color: white;
        }

        label {
            margin-bottom: .5rem;
            font-family: Poppins-Bold;
            line-height: 1.2;
        }
        .page{
            background: #f1f1f1;
        }
        *::-webkit-scrollbar {
            width: 8px;
            height: 8px;
        }

        *::-webkit-scrollbar-track {
            background-color: #e4e4e4;
            border-radius: 100px;
        }

        *::-webkit-scrollbar-thumb {
            border-radius: 100px;
            background-image: linear-gradient(180deg, #a19f9f 0%, #a19f9f 99%);
            box-shadow: inset 2px 2px 5px 0 rgba(#fff, 0.5);
        }

    </style>
    
</head>

<body data-spy="scroll">
    <div id="styleDiv">
        <style>
        </style>
    </div>
    <div class="row page">
        <div class="col-md-4 my-auto px-md-5 px-0 px-sm-2 col-12 col-sm-12">
            <div>
                <div class="container shadow" id="wrapper">
                    {{ Form::open(['url' => '/Upload', 'enctype' => 'multipart/form-data', 'id' => 'frmSub']) }}
                    {{ Form::hidden('dealerName', $dealerName, ['id' => 'dealerName']) }}

                    <h3 class="text-center mt-3 p-2 text-dark m-0"> Design 1</h3>
                    <div class="row  p-4">
                        <input type="hidden" name="template_name" value="1">
                        <div class="col-md-12 form-group ">
                            <label for="bg1">
                                Background Color 1
                            </label><br>
                            <div class="d-flex">
                                <input type="text" name="bg_color" id="bg_color" class="form-control" placeholder="Hex color code" value="#7579ff">
                                <input type="color" id="color" class="w-25 form-control rounded-left" value="#7579ff">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="bg2">
                                Background Color 2
                            </label><br>
                            <div class="d-flex">
                                <input type="text" name="bg_color1" id="bg_color1" class="form-control" placeholder="Hex color code" value="#b224ef">
                                <input type="color" id="color1" class="w-25 form-control rounded-left" value="#b224ef">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="fontcoler">Font Color </label><br>
                            <div class="d-flex">
                                <input type="text" name="fontcolor" id="ft_color" class="form-control" placeholder="Hex color code" value="#e5e8eb">
                                <input type="color" id="font_color" class="w-25 form-control rounded-left" value="#e5e8eb">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="fontcoler">Web Theme Color </label><br>
                            <div class="d-flex">
                            <input type="text" name="themecolor" id="themecolor" class="form-control" placeholder="Hex color code">
                            <input type="color" id="theme_color" class="w-25 form-control rounded-left">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="bgi">
                                Background Image
                            </label><br>
                            <div class="custom-file w-100">
                                <input type="file" class="custom-file-input" id="myFile" name="background"
                                    onchange="myFunction(this)" accept="image/x-png,image/gif,image/jpeg">
                                <label class="custom-file-label text-dark" for="myFile"></label>
                            </div>
                            @if ($errors->has('background'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('background') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="logo">
                                Logo
                            </label><br>
                            <div class="custom-file w-100">
                                <input type="file" class="custom-file-input" id="myFile1" name="logo"
                                    onchange="readURL(this)" accept="image/x-png,image/gif,image/jpeg">
                                <label class="custom-file-label text-dark" for="myFile"></label>
                            </div>
                            @if ($errors->has('logo'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('logo') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-12 text-center mt-2">
                            <input type="submit" class="btn btn-primary" value="Upload" name="submit">
                        </div>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-md-8 mh-100 col-12">
            <div>
                <div class="limiter">
                    <div class="container-login100"
                        style="background-image: url('{{ URL::asset('assets/imgs/background3.png') }}');">
                        <div class="wrap-login100" id="grid">
                            <form class="login100-form validate-form">
                                <span class="login100-form-logo">
                                    <img id="blah" style="width: 100%;height: 100%;" class="avatar-img rounded-circle"
                                        src="{{ URL::asset('assets/imgs/logo3.png') }}" />
                                </span>

                                <span class="login100-form-title p-b-34 p-t-27">
                                    Log in
                                </span>

                                <div class="wrap-input100 validate-input" data-validate="Enter username">
                                    <input class="input100" type="text" name="username" placeholder="Username">
                                    <span class="focus-input100" data-placeholder="&#xf207;"></span>
                                </div>

                                <div class="wrap-input100 validate-input" data-validate="Enter password">
                                    <input class="input100" type="password" name="pass" placeholder="Password">
                                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
                                </div>

                                <div class="contact100-form-checkbox">
                                    <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                                    <label class="label-checkbox100" for="ckb1">
                                        Remember me
                                    </label>
                                </div>

                                <div class="container-login100-form-btn">
                                    <button class="login100-form-btn">
                                        Login
                                    </button>
                                </div>

                                <div class="text-center p-t-34">
                                    <a class="txt1" href="#">
                                        Forgot Password?
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="{{ asset('common/js/core/jquery.3.2.1.min.js') }}"></script>
    <script src="{{ asset('common/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('common/js/core/bootstrap.min.js') }}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('common/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <!-- Sweet Alert -->
    <script src="{{ asset('common/js/plugin/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Atlantis JS -->
    <script src="{{ asset('common/js/atlantis.min.js') }}"></script>

    <script src="{{ asset('common/js/script.js') }}"></script>
    <script src="{{ URL::asset('js/main1.js') }}"></script>

    <script type="text/javascript">

        
        $('#frmSub').on('submit', function(e) {
            var bgIMG = $('#myFile').val();
            var bgLogo = $('#myFile1').val();
            if (bgIMG == '') {
                wornigAlert("Select the background Image");
                return false;
            } else if (bgLogo == '') {
                wornigAlert("Select the Logo Image");
                e.preventDefault();
                return false;
            } else {
                var extensionLogo = bgLogo.split('.').pop().toUpperCase();
                var extension = bgIMG.split('.').pop().toUpperCase();
                if (extension != "BMP" && extension != "PNG" && extension != "JPEG" && extension != "JPG") {
                    e.preventDefault();
                    wornigAlert("You can upload Background Image that are .JPEG, .JPG, or .PNG.");
                    return false;
                }
                if (extensionLogo != "BMP" && extensionLogo != "PNG" && extensionLogo != "JPEG" &&
                    extensionLogo != "JPG") {
                    e.preventDefault();
                    wornigAlert("You can upload Logo that are .JPEG, .JPG, or .PNG.");
                    return false;
                }
            }

        });

        $(function(){
            fontColorChange("#e5e8eb")
        })
        $('#bg_color').on('change',function(){
            $('#color').val($(this).val())
            backgroundColorChange()
        })
        $('#bg_color1').on('change',function(){
            $('#color1').val($(this).val())
            backgroundColorChange()
        })
        $('#ft_color').on('change',function(){
            $('#font_color').val($(this).val())
            fontColorChange($(this).val())
        })
        $('#themecolor').on('change',function(){
            $('#theme_color').val($(this).val())
            $(".login100-form-btn").css('background', $(this).val());
        });
        $('#color').on("change", function() {
            $("#bg_color").val($(this).val());
            backgroundColorChange()
        });

        $('#color1').on("change", function() {
            $("#bg_color1").val( $(this).val());
            backgroundColorChange()
        });

        $('#font_color').on("change", function() {
            $("#ft_color").val($(this).val());
            fontColorChange($(this).val())
        });

        function backgroundColorChange(){
            $(".wrap-login100").css('background', "linear-gradient(-160deg," + $('#color').val() + "," +
            $('#color1').val() + ")");
        }
        function fontColorChange(colorcode) {
            $(".login100-form-title").css('color', colorcode);
            $(".label-checkbox100").attr({'style': `color:${colorcode} !important;`});
            $(".txt1").css('color', colorcode);
            $("input::-webkit-input-placeholder").css('color', colorcode);
            change_placeholder_color(colorcode);
        }

        $('#theme_color').on('change',function(){
            $("#themecolor").val($(this).val());
            $(".login100-form-btn").css('background', $(this).val());
        });
        function change_placeholder_color(color_choice) {
            $("#styleDiv>style").remove();
            $("#styleDiv").append("<style>" + "" + ".input100::placeholder{color:" +  color_choice + "}</style>")
            $("#styleDiv").append("<style>" + "" + ".focus-input100::after{color:" +  color_choice + "}</style>")
            $("#styleDiv").append("<style>" + "" + ".focus-input100::before{background:" +  color_choice + "}</style>")
        }
        const imageFormat = ['NONE','PNG','JPG','APNG','JPEG','WEBP','SVG','GIF'];
        function myFunction(input) {
            var file = input.files[0];
            var fileName = file.name.split('.');
            var fileType = fileName[fileName.length-1].toUpperCase();
            if(imageFormat.includes(fileType)){
                url = URL.createObjectURL(file);
                $(".container-login100").css('background', "url(" + url+ ")");
            }else{
                wornigAlert(fileType+' Format not support.')
            }
        }

        function readURL(input) {
            var file = input.files[0];
            var fileName = file.name.split('.');
            var fileType = fileName[fileName.length-1].toUpperCase();
            if(imageFormat.includes(fileType)){
                url = URL.createObjectURL(file);
                $('#blah')
                    .attr('src',url)
                    .width(150)
                    .height(150);
            }else{
                wornigAlert(fileType+' Format not support.')
            }
        }
    </script>

</body>

</html>
