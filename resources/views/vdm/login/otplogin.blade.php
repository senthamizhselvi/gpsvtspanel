<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        /* Full-width input fields */

        /* Set a style for all buttons */


        /* Extra styles for the cancel button */
        .cancelbtn {
            width: 100%;
            padding: 10px 18px;
            background-color: #128282;
            color: #ffffff;
            margin-top: 6%;
            border-radius: 10px;
        }

        .cancelbtn1 {

            width: 100%;
            padding: 10px 18px;
            background-color: #FF3366;
            color: #ffffff;
            margin-top: 6%;
            border-radius: 10px;
        }

        /* Center the image and position the close button */
        .imgcontainer {
            text-align: center;
            margin: 24px 0 12px 0;
            position: relative;
        }

        .imgcontainer1 {
            text-align: center;
            margin: 24px 0 12px 0;
            position: relative;
        }

        img.avatar {
            border-radius: 8px;
            max-width: 75%;
            height: auto;
        }

        .container {
            padding: 21px;
            padding-top: 33px;
        }

        .container {
            padding: 21px;
            padding-top: 0px;
        }


        span.psw {
            float: right;
            padding-top: 16px;
        }

        /* The Modal (background) */
        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 1;
            /* Sit on top */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4);
            /* Black w/ opacity */
            padding-top: 60px;
        }

        /* Modal Content/Box */
        .modal-content {
            /*background-image: url(../public/uploads/pucture6.jpg);*/
            background-color: #fefefe;
            margin: 5% auto 15% auto;
            /* 5% from the top, 15% from the bottom and centered */
            border: 1px solid #888;
            border-radius: 12px;
            width: 330px;
            max-height: 72%;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            /* Could be more or less, depending on screen size */
        }

        /* The Close Button (x) */
        .close {
            position: absolute;
            right: 19px;
            top: -20px;
            color: #ab9999;
            font-size: 30px;
            /*font-weight: bold;*/
        }


        .close:hover,
        .close:focus {
            color: red;
            cursor: pointer;
        }

        .textbox:hover {
            /* background-color: red;
    cursor: pointer; */
            box-shadow: none !important;
            outline: none !important;
            border: 1px solid #1da7da !important;
        }

        .btn1 {
            width: 100%;
            padding: 10px 18px;
            background-color: #128282;
            color: #ffffff;
            margin-top: 6%;
            border-radius: 10px;

        }


        /* Add Zoom Animation */
        .animate {
            -webkit-animation: animatezoom 0.6s;
            animation: animatezoom 0.6s
        }

        @-webkit-keyframes animatezoom {
            from {
                -webkit-transform: scale(0)
            }

            to {
                -webkit-transform: scale(1)
            }
        }

        @keyframes animatezoom {
            from {
                transform: scale(0)
            }

            to {
                transform: scale(1)
            }
        }

        ::-webkit-input-placeholder {
            /* Chrome/Opera/Safari */
            color: #b8b8ba;
        }

    </style>
    <title>GPS</title>
    @if ($byPass == 'yes')
        <link rel="stylesheet" type="text/css" href="../../../assets/css/login.css">
    @else
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
    @endif
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

</head>
<link rel="shortcut icon" href="assets/imgs/tab.ico">

<body class="cont" onload="callMe()">
    <div class="demo">
        <div class="login" align="center">
            <div class="login__check"> </div>
            <img id="imagesrc" style="border-radius: 8px;max-width: 75%;height: auto;" />

            <br>
            <br>
            <p class="login__signup" style="font-size: 14px"><a>GPS Tracking System</a></p>
            <h5>
                <?php if(session()->has('flash_notice')): ?>
                <div class="flashMessage" id="flash_notice"><?php echo session()->get('flash_notice'); ?></div>
                <?php endif; ?>

                <span id="error"
                    style="color:#ff6666;font-weight:bold;font-size:15px;">{{ HTML::ul($errors->all()) }}</span>
            </h5><br /><br />
            <p class="login__signup" style="font-size: 14px"><a>Please,Confirm your mobile number. We will send you a
                    One time Email Message !</a></p>

            <div class="login__form">

                <div class="login__row">
                    <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
                        <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
                    </svg>
                    {{ Form::hidden('username', $username, ['id' => 'usr']) }}
                    {{ Form::number('mobile', null, ['placeholder' => 'Mobile Number', 'class' => 'login__input name', 'id' => 'mobile', 'required' => 'required']) }}

                </div>
                <br />


                <button class="cancelbtn" id="clickme"><a>SEND</a></button>
                <button class="cancelbtn1" id="cancel"><a>CANCEL</a></button>
            </div>
        </div>

    </div>

    <div id="id01" class="modal">

        <form class="modal-content animate" action="password/resetting">
            <div class="imgcontainer">
                <span onclick="document.getElementById('id01').style.display='none'" class="close"
                    title="Close">&times;</span>
                <img id="avatarsrc" class="avatar" />
            </div>

            <div class="container">
                <input type="number" id="otp"
                    style="width: 100%; padding: 12px 20px; margin: -5px 0; display: inline-block; border-bottom:1px solid #ccc; box-sizing: border-box; border-radius: 10px;"
                    class="textbox" placeholder="Enter OTP" name="otp" required>
                <h2 id="bala" style="margin-top: 3%;margin-left: 59%;">Expires in <span id="countdowntimer">300 </span>
                    Seconds</h2>
                <a href="#" id="resentbtn"
                    style="width: 30%; padding: 4px; margin-top: 62%; margin-left: 80%; font-size: 2.5em; color: color: sienna;display: none;">Resend</a>
                <button class="cancelbtn" id="verify"><a>Verify</a></button>

            </div>
        </form>
    </div>


</body>

<script>
    sessionStorage.clear();
    var logo = document.location.host;
    var substring = "www";
    if (logo.indexOf(substring) !== -1) {
        var logocsk = logo.split(".");
        logo = logocsk[1];
    }

    function ValidateIPaddress(ipaddress) {
        var ipformat =
            /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        if (ipaddress.match(ipformat)) {
            return (true)
        }
        // alert("You have entered an invalid IP address!")  
        return (false)
    }



    if (ValidateIPaddress(logo)) {
        var parser = document.createElement('a');
        parser.href = document.location.ancestorOrigins[0];
        logo = parser.host;
    }

    var path = document.location.pathname;
    var splitpath = path.split("/");


    var imgName = '/' + splitpath[1] + '/public/uploads/' + logo + '.png';
    var wwwSplit = logo.split(".")
    if (wwwSplit[0] == "www") {
        wwwSplit.shift();
        imgName = '/' + splitpath[1] + '/public/uploads/' + wwwSplit[0] + '.' + wwwSplit[1] + '.png';
    }

    $('#avatarsrc').attr('src', imgName);
    $('#imagesrc').attr('src', imgName);
</script>

<script type="text/javascript">
    var globalIP = document.location.host;
    var contextMenu = '/gps';

    function callMe() {

        var isChrome = !!window.chrome && !!window.chrome.webstore;

        if (isChrome == false && (navigator.userAgent.indexOf('Chrome') == -1)) {

            for (i = 0; i <= 15; i++) {
                alert("Please Open Site on Google Chrome");
            }
        }
    }
    $('#resentbtn').click(function(e) {
        var datas = {
            'valu': $('#mobile').val(),
            'usr': $('#usr').val(),
            '_token': "{{ csrf_token() }}"
        }
        if (!confirm('OTP has been send to your Register mobile number ! ')) {
            e.preventDefault();
            $("#resentbtn").show();
            $("#bala").hide();
        } else {
            $("#resentbtn").hide();
            $("#bala").show();
            $.post('{{ route('ajax.resent') }}', datas).done(function(data) {
                if (data.trim() == 'success') {
                    var timeleft = 300;
                    var downloadTimer = setInterval(function() {
                        timeleft--;
                        document.getElementById("countdowntimer").textContent = timeleft;
                        if (timeleft <= 0) {
                            clearInterval(downloadTimer);
                            $("#bala").hide();
                            $("#resentbtn").show();
                        }
                    }, 1000);
                    alert('OTP successfully send to your Register mobile number!');
                } else {
                    e.preventDefault();
                    alert('This mobile number was not Register !');
                }



            }).fail(function() {
                console.log("Login Failed1");
            });


        }
    });

    $('#clickme').click(function(e) {
        var datas = {
            'val': $('#mobile').val(),
            'usr': $('#usr').val(),
            '_token': "{{ csrf_token() }}"

        }
        if (datas.val == null || datas.val == '') {
            alert('Please enter the mobile number!');
        } else {
            $.post('{{ route('ajax.mobleverisy') }}', datas).done(function(data) {
                if (data == 'success') {
                    document.getElementById('id01').style.display = 'block';
                    var timeleft = 300;
                    var downloadTimer = setInterval(function() {
                        timeleft--;
                        document.getElementById("countdowntimer").textContent = timeleft;
                        if (timeleft <= 0) {
                            clearInterval(downloadTimer);
                            $("#bala").hide();
                            $("#resentbtn").show();
                        }
                    }, 1000);

                } else {
                    e.preventDefault();

                    alert('This mobile number was not registered !');
                    //location.reload();
                }
            }).fail(function() {});
            e.preventDefault(); //document.getElementById('clickme').disabled = 'disabled';
            return false;


        }
    });
    $('#cancel').click(function() {

        $.post('{{ route('ajax.cancel') }}').done(function(data) {
            if (data.trim() == 'success') {
                window.location.href = '{{ url('login') }}';
            }

        }).fail(function() {
            console.log("Login Failed1");
        });

    });
    $('#verify').click(function(e) {
        var datas = {
            'val': $('#otp').val(),
            'valu': $('#mobile').val(),
            'usr': $('#usr').val(),
            '_token': "{{ csrf_token() }}"
        }
        if (datas.val == null || datas.val == '') {
            alert('Please enter six digit OTP!');
        } else {

            $.post('{{ route('ajax.otpverify') }}', datas).done(function(data) {
                if (data == 'success') {
                    if (datas.usr == 'smpadmin' || datas.usr == 'vamoadmin') {
                        window.location.href = '{{ url('Business') }}';
                    } else if (datas.usr = 'vamos') {
                        window.location.href = '{{ url('track') }}';
                    }
                } else {
                    e.preventDefault();
                    alert('Incorrect OTP!!!');
                    //  location.reload();
                }
            }).fail(function() {});
            e.preventDefault();
            return false;
            //  document.getElementById('verify').disabled = 'disabled';  


        }

    });

    // Get the modal
</script>
