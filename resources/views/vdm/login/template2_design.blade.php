<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login V13</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href={{ URL::asset('images/icons/favicon.ico') }} />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href={{ URL::asset('fonts/font-awesome-4.7.13.013/css/font-awesome.min.css') }}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href={{ URL::asset('fonts/Linearicons-Free-v1.0.13.013/icon-font.min.css') }}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href={{ URL::asset('fonts/iconic13/css/material-design-iconic-font.min.css') }}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{ URL::asset('vendor/animate13/animate.css') }}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{ URL::asset('css/util13.css') }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset('css/main13.css') }}>
    <!--===============================================================================================-->
    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('common/css/atlantis.css') }}">
    <style type="text/css">
        .img-circular {
            display: table-cell;
            height: 300px;
            text-align: center;
            width: 300px;
            vertical-align: middle;
            margin-left: 25%;
        }

        input[type="color"] {
            -webkit-appearance: none;
            width: 60%;
            height: 30px;
            border: 0;
            border-radius: .5rem;
            padding: 0;
            overflow: hidden;
            box-shadow: 2px 2px 5px rgba(0, 0, 0, .1);
        }

        input[type="color"]::-webkit-color-swatch-wrapper {
            padding: 0;
        }

        input[type="color"]::-webkit-color-swatch {
            border: none;
        }

        h3 {
            font-family: Poppins-Bold;
            line-height: 1.2;
        }

        #wrapper {
            /* float: left; */
            background: #fff;
            box-shadow: #9990;
            border-radius: 25px;
            color: white;
        }

        label {
            margin-bottom: .5rem;
            font-family: Poppins-Bold;
            line-height: 1.2;
        }

        .page {
            background: #f1f1f1;
        }
        *::-webkit-scrollbar {
            width: 8px;
            height: 8px;
        }

        *::-webkit-scrollbar-track {
            background-color: #e4e4e4;
            border-radius: 100px;
        }

        *::-webkit-scrollbar-thumb {
            border-radius: 100px;
            background-image: linear-gradient(180deg, #a19f9f 0%, #a19f9f 99%);
            box-shadow: inset 2px 2px 5px 0 rgba(#fff, 0.5);
        }
    </style>
</head>

<body data-spy="scroll">
    <div id="styleDiv">
        <style>
        </style>
    </div>
    <div class="row page">
        <div class="col-md-4 my-auto px-md-5 px-0 px-sm-2  col-12  col-sm-12">
            <div>
                <div class="container shadow" id="wrapper">
                    {{ Form::open(['url' => '/Upload', 'enctype' => 'multipart/form-data', 'id' => 'frmSub']) }}
                    {{ Form::hidden('dealerName', $dealerName, ['id' => 'dealerName']) }}

                    <h3 class="text-center mt-3 text-dark"> Design 2</h3>
                    <div class="row p-4">
                        {{-- hidden input --}}
                        <input type="hidden" name="template_name" value="2">

                        <div class="col-md-12 form-group ">
                            <label for="bg1">
                                Background Color 1
                            </label><br>
                            <div class="d-flex">
                                <input type="text" name="bg_color" id="bg_color" class="form-control"
                                    placeholder="Hex color code" value="">
                                <input type="color" id="color" class="w-25 form-control rounded-left">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="bg2">
                                Background Color 2
                            </label><br>
                            <div class="d-flex">
                                <input type="text" name="bg_color1" id="bg_color1" class="form-control"
                                    placeholder="Hex color code" value="">
                                <input type="color" id="color1" class="w-25 form-control rounded-left">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="fontcoler">Font Color </label><br>
                            <div class="d-flex">
                                <input type="text" name="fontcolor" id="ft_color" class="form-control"
                                    placeholder="Hex color code" value="">
                                <input type="color" id="font_color" class="w-25 form-control rounded-left">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="fontcoler">Web Theme Color </label><br>
                            <div class="d-flex">
                                <input type="text" name="themecolor" id="themecolor" class="form-control"
                                    placeholder="Hex color code">
                                <input type="color" id="theme_color" class="w-25 form-control rounded-left">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="bgi">
                                Background Image
                            </label><br>
                            <div class="custom-file w-100">
                                <input type="file" class="custom-file-input" id="myFile" name="background"
                                    onchange="myFunction(this)" accept="image/x-png,image/gif,image/jpeg">
                                <label class="custom-file-label text-dark" for="myFile"></label>
                            </div>
                            @if ($errors->has('background'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('background') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="logo">
                                Logo
                            </label><br>
                            <div class="custom-file w-100">
                                <input type="file" class="custom-file-input" id="myFile1" name="logo"
                                    onchange="readURL(this)" accept="image/x-png,image/gif,image/jpeg">
                                <label class="custom-file-label text-dark" for="myFile"></label>
                            </div>
                            @if ($errors->has('logo'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('logo') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-12 text-center mt-2">
                            <input type="submit" class="btn btn-primary" value="Upload" name="submit">
                        </div>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-md-8  col-12">
            <div>
                <div class="limiter">
                    <div class="container-login100" id="grid">
                        <div class="login100-more"
                            style="background-image: url('{{ URL::asset('assets/imgs/background1.png') }}');"></div>

                        <div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
                            <form class="login100-form validate-form">

                                <span class="span mx-auto" style="">
                                    <img class="mb-4" alt="centered image" id="blah"
                                        src="{{ URL::asset('assets/imgs/logo3.png') }}" width="100px"
                                        height="auto" />
                                </span>

                                <span class="login100-form-title p-b-25">
                                    LOG IN
                                </span>

                                <div class="wrap-input100 validate-input" data-validate="Name is required">
                                    <span class="label-input100">User Name</span>
                                    <input class="input100" type="text" name="name">
                                    <span class="focus-input100"></span>
                                </div>


                                <div class="wrap-input100 validate-input" data-validate="Password is required">
                                    <span class="label-input100">Password</span>
                                    <input class="input100" type="password" name="pass">
                                    <span class="focus-input100"></span>
                                </div>

                                <div class="flex-m w-full p-b-33">
                                    <div class="contact100-form-checkbox">
                                        <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                                        <label class="label-checkbox100" for="ckb1">
                                            <span class="txt1">
                                                Remember me
                                            </span>
                                        </label>
                                    </div>


                                </div>

                                <div class="container-login100-form-btn">
                                    <div class="wrap-login100-form-btn">
                                        <div class="login100-form-bgbtn"></div>
                                        <button class="login100-form-btn">
                                            Sign In
                                        </button>
                                    </div>
                                    <a href="#" class="dis-block txt3 mt-5" style="font-size:12px">
                                        Forgot Password?
                                        <i class="fa fa-long-arrow-right m-l-5"></i>
                                    </a>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('common/js/core/jquery.3.2.1.min.js') }}"></script>
    <script src="{{ asset('common/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('common/js/core/bootstrap.min.js') }}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('common/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <!-- Sweet Alert -->
    <script src="{{ asset('common/js/plugin/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Atlantis JS -->
    <script src="{{ asset('common/js/atlantis.min.js') }}"></script>

    <script src="{{ asset('common/js/script.js') }}"></script>
    <script type="text/javascript">
        $('#frmSub').on('submit', function(e) {
            var bgIMG = $('#myFile').val();
            var bgLogo = $('#myFile1').val();
            if (bgIMG == '') {
                wornigAlert("Select the background Image");
                return false;
            } else if (bgLogo == '') {
                wornigAlert("Select the Logo Image");
                e.preventDefault();
                return false;
            } else {
                var extensionLogo = bgLogo.split('.').pop().toUpperCase();
                var extension = bgIMG.split('.').pop().toUpperCase();
                if (extension != "BMP" && extension != "PNG" && extension != "JPEG" && extension != "JPG") {
                    e.preventDefault();
                    wornigAlert("You can upload Background Image that are .JPEG, .JPG, or .PNG.");
                    return false;
                }
                if (extensionLogo != "BMP" && extensionLogo != "PNG" && extensionLogo != "JPEG" &&
                    extensionLogo != "JPG") {
                    e.preventDefault();
                    wornigAlert("You can upload Logo that are .JPEG, .JPG, or .PNG.");
                    return false;
                }
            }

        });

        $('#bg_color').on('change', function() {
            $('#color').val($(this).val())
            backgroundColorChange()
        })
        $('#bg_color1').on('change', function() {
            $('#color1').val($(this).val())
            backgroundColorChange()
        })
        $('#ft_color').on('change', function() {
            $('#font_color').val($(this).val())
            fontColorChange($(this).val())
        })
        $('#themecolor').on('change', function() {
            $('#theme_color').val($(this).val())
            $(".login100-form-btn").css('background', $(this).val());
        });
        $('#color').on("change", function() {
            $("#bg_color").val($(this).val());
            backgroundColorChange()
        });

        $('#color1').on("change", function() {
            $("#bg_color1").val($(this).val());
            backgroundColorChange()
        });

        $('#font_color').on("change", function() {
            $("#ft_color").val($(this).val());
            fontColorChange($(this).val())
        });
        $('#theme_color').on('change', function() {
            $("#themecolor").val($(this).val());
            $(".login100-form-btn").css('background', $(this).val());
        });

        function backgroundColorChange() {
            $(".wrap-login100").css('background', "linear-gradient(-160deg," + $('#color').val() + "," +
                $('#color1').val() + ")");
        }

        function fontColorChange(colorcode) {
            $(".login100-form-title,.label-input100,.input100,.txt1").css('color', colorcode);
            $(".label-checkbox100").attr({
                'style': `color:${colorcode} !important;`
            });
            change_placeholder_color(colorcode);
        }


        function change_placeholder_color(color_choice) {
            $("#styleDiv>style").remove();
            $("#styleDiv").append("<style>" + "" + ".input100::placeholder{color:" + color_choice + "}</style>")
            $("#styleDiv").append("<style>" + "" + ".focus-input100::after{color:" + color_choice + "}</style>")
            $("#styleDiv").append("<style>" + "" + ".focus-input100::before{background:" + color_choice + "}</style>")
        }
        const imageFormat = ['NONE', 'PNG', 'JPG', 'APNG', 'JPEG', 'WEBP', 'SVG', 'GIF'];

        function myFunction(input) {
            var file = input.files[0];
            var fileName = file.name.split('.');
            var fileType = fileName[fileName.length - 1].toUpperCase();
            if (imageFormat.includes(fileType)) {
                url = URL.createObjectURL(file);
                $(".login100-more").css('background', "url(" + url+ ")");
            } else {
                wornigAlert(fileType + ' Format not support.')
            }
        }

        function readURL(input) {
            var file = input.files[0];
            var fileName = file.name.split('.');
            var fileType = fileName[fileName.length - 1].toUpperCase();
            if (imageFormat.includes(fileType)) {
                url = URL.createObjectURL(file);
                $('#blah')
                    .attr('src', url)
                    .width(150)
                    .height(150);
            } else {
                wornigAlert(fileType + ' Format not support.')
            }
        }
    </script>
    <script src={{ URL::asset('js/main13.js') }}></script>

</body>

</html>
