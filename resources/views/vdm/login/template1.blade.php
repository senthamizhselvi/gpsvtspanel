<!DOCTYPE html>
<html lang="en">

<head>
    <title>GPS</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap3/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts1/font-awesome-4.7.1/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts1/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate3/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util1.css">
    <link rel="stylesheet" type="text/css" href="css/main1.css">
    <!--===============================================================================================-->
</head>
<style type="text/css">
    .input100::-webkit-input-placeholder {
        color:  {{$fcolor}} !important;
    }

    .input100:-ms-input-placeholder {
        /* Internet Explorer 10-11 */
        color: {{$fcolor}} !important;
    }

    .input100,
    .input100::placeholder {
        color: {{$fcolor}} !important;
    }

    .focus-input100::after {
        color: {{$fcolor}} !important;
    }

    .focus-input100::before {
        background: {{$fcolor}} !important;
    }

    <?php if($themecolor !=""): ?> 
    .login100-form-btn {
        background: {{$themecolor}} !important;
    }
    <?php endif; ?> 
    .modal-content {
        background: linear-gradient(-135deg, {{ $bcolor }}, {{ $bcolor1 }});
        margin: 5% auto 15% auto;
        border: 1px solid #888;
        border-radius: 12px;
        width: 330px;
        min-height: 45vh;
        max-height: 100vh;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    .cf {
        position: relative;
        margin: 0 auto;
    }

    .cf img {
        position: absolute;
        left: 30px;
        -webkit-transition: opacity 1s ease-in-out;
        -moz-transition: opacity 1s ease-in-out;
        -o-transition: opacity 1s ease-in-out;
        transition: opacity 1s ease-in-out;
    }

    .cf img.top:hover {
        opacity: 0;
    }

    .cf {
        position: relative;
        margin: 0 auto;
    }

    .cff img {
        position: absolute;
        -webkit-transition: opacity 1s ease-in-out;
        -moz-transition: opacity 1s ease-in-out;
        -o-transition: opacity 1s ease-in-out;
        transition: opacity 1s ease-in-out;
    }

    .cff img.top:hover {
        opacity: 0;
    }

    .field-icon {
        float: right;
        margin-left: -25px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
        color: white;
    }

    .field-icon {
        color: {{$fcolor}} !important;
    }

</style>

<body onload="callMe()">

    <div class="limiter">
        <div class="container-login100" style="background-image:url({{ $backgrounds }});">
            <div class="wrap-login100"
                style="background: linear-gradient(-135deg, {{ $bcolor }}, {{ $bcolor1 }});">
                {{ Form::open(['url' => route('dologin'), 'class' => 'login100-form validate-form']) }}
                <span class="login100-form-logo">
                    <img id="blah" height="120" width="150" src="{{ $logo }}" />
                </span>

                <span class="login100-form-title p-b-34 p-t-27" style="color: {{ $fcolor }};">
                    Log in
                </span>

                <div class="wrap-input100 validate-input" data-validate="Enter username">
                    <input class="input100" type="text" name="username" placeholder="Username" id="userIds">
                    <span class="focus-input100" data-placeholder="&#xf207;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Enter password">
                    <input class="input100 password" type="password" name="password" placeholder="Password">
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    <i class="fa fa-eye field-icon toggle-password" toggle="#password-field"></i>
                </div>

                <div class="contact100-form-checkbox">
                    <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember_me">
                    <label class="label-checkbox100" for="ckb1" style="color: {{ $fcolor }};">
                        Remember me
                    </label>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn" type="submit" id="clickme">
                        Login
                    </button>
                </div>

                <h5 style="color: white;    margin-left: 9%;-size: 17px;">
                    @if (session()->has('flash_notice'))
                        <div class="text-denger text-uppercase">
                            {{ session()->get('flash_notice') }}
                        </div>
                    @endif
                    <span class="text-danger">{{ HTML::ul($errors->all()) }}</span>
                </h5>

                <div class="text-center p-t-34">
                    <a class="txt1" href="password/resetingWin" style="color: {{ $fcolor }};cursor: pointer;">
                        Forgot Password?
                    </a>
                    @if ($url == 'yes')<a href="/gps/public/apiAcess" class="txt1"
                            target="blank"> | API Access |</a>&nbsp;<a href="/gps/public/faq" class="txt1"
                            target="blank">FAQ</a>@endif
                </div>
                {{ Form::close() }}
                @if ($url == '')
                    <div class="row" style="margin-top: 2%; margin-left: 2%">
                        <div class="col-sm-2 cff">
                            <a target="_blank" href="https://play.google.com/store/apps/details?id=com.gpsvts"><img
                                    class="bottom" src="/gps/public/assets/imgs/andG.png"
                                    style="width: 25px; height: 25px" /></a>
                            <a target="_blank" href="https://play.google.com/store/apps/details?id=com.gpsvts"><img
                                    class="top" src="/gps/public/assets/imgs/android1.png"
                                    style="width: 25px; height: 25px" /></a>
                        </div>
                        <div class="col-sm-2 cff">
                            <a target="_blank" href="https://itunes.apple.com/in/app/gpsvts/id1038796257?mt=8"><img
                                    class="bottom" src="/gps/public/assets/imgs/appGy.png"
                                    style="width: 25px; height: 25px" /></a>
                            <a target="_blank" href="https://itunes.apple.com/in/app/gpsvts/id1038796257?mt=8"><img
                                    class="top" src="/gps/public/assets/imgs/appG.png"
                                    style="width: 25px; height: 25px" /></a>
                        </div>
                        <div class="col-sm-2 cff">
                            <a target="_blank" href="https://www.facebook.com/vamosystems"><img class="bottom"
                                    src="/gps/public/assets/imgs/facebook1.png"
                                    style="width: 25px; height: 25px;" /></a>
                            <a target="_blank" href="https://www.facebook.com/vamosystems"><img class="top"
                                    src="/gps/public/assets/imgs/facebook.png" style="width: 25px; height: 25px;" /></a>
                        </div>
                        <div class="col-sm-2 cff">
                            <a target="_blank" href="https://twitter.com/gps_vamosystems"><img class="bottom"
                                    src="/gps/public/assets/imgs/twitter.png" style="width: 25px; height: 25px;" /></a>
                            <a target="_blank" href="https://twitter.com/gps_vamosystems"><img class="top"
                                    src="/gps/public/assets/imgs/twitter1.png" style="width: 25px; height: 25px;" /></a>
                        </div>
                        <div class="col-sm-2 cff">
                            <a target="_blank" href="https://www.linkedin.com/company/vamo-systems"><img class="bottom"
                                    src="/gps/public/assets/imgs/linked1.png" style="width: 25px; height: 25px;" /></a>
                            <a target="_blank" href="https://www.linkedin.com/company/vamo-systems"><img class="top"
                                    src="/gps/public/assets/imgs/linked3.png" style="width: 25px; height: 25px;" /></a>
                        </div>
                        <div class="col-sm-2 cff">
                            <a target="_blank" href="https://www.youtube.com/channel/UC1LNXPejDkOT7TOgsMltNKg"><img
                                    class="bottom" src="/gps/public/assets/imgs/youtube.png"
                                    style="width: 25px; height: 25px;" /></a>
                            <a target="_blank" href="https://www.youtube.com/channel/UC1LNXPejDkOT7TOgsMltNKg"><img
                                    class="top" src="/gps/public/assets/imgs/youtube1.png"
                                    style="width: 25px; height: 25px;" /></a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div id="dropDownSelect1"></div>

    <div id="id01" class="modal">

        <form class="modal-content animate" action="password/resetting">
            <div class="imgcontainer">
                <span onclick="document.getElementById('id01').style.display='none'" class="close"
                    title="Close">&times;</span>
                <img class="avatar" id="avatarsrc" height="120" width="150" src="{{ $logo }}" />
            </div>


            <div class="wrap-input100 validate-input1" data-validate="Enter username"
                style="margin-left: 5%;width: 90%;">
                <input class="input100" type="text" id="usern" name="uname" placeholder="Enter Username" required>
                <span class="focus-input100" data-placeholder="&#xf207;"></span>
            </div>
            <div class="container-login100-form-btn">
                <button class="login100-form-btn" type="submit">
                    Send Password Reset Link
                </button>
            </div>
        </form>
    </div>

    <script src="vendor/jquery4/jquery-3.2.1.min.js"></script>
    <script src="vendor/animsition/js/animsition.min.js"></script>
    <script src="vendor/bootstrap3/js/popper.js"></script>
    <script src="vendor/bootstrap3/js/bootstrap.min.js"></script>
    <script src="js/main1.js"></script>
</body>
<script type="text/javascript">
    localStorage.clear();
    var globalIP = document.location.host;
    var contextMenu = '/gps';

    function callMe() {

        var isChrome = !!window.chrome && !!window.chrome.webstore;

        if (isChrome == false && (navigator.userAgent.indexOf('Chrome') == -1)) {

            for (i = 0; i <= 15; i++) {
                alert("Please Open Site on Google Chrome");
            }
        }
    }

    $('#clickme').click(function() {

        var userId = $('#userIds').val().toUpperCase();
        var userIP = localStorage.getItem('myIP');
        var postVal = {
            'id': userId,
            'userIP': userIP,
            '_token': $('input[name="_token"]').val()
        };
        localStorage.setItem('lang', 'en');

        //$.get('http://128.199.159.130:9000/isAssetUser?userId=MSS', function(response) {
        $.get('//' + globalIP + contextMenu + '/public/isAssetUser', function(response) {
            //alert(response);
            localStorage.setItem('isAssetUser', response);
        }).error(function() {
            console.log('error in isAssetUser');
        });

        $.post('{{ route('ajax.fcKeyAcess') }}', postVal)
            .done(function(data) {

                localStorage.setItem('fCode', data);
                //alert(data);

            }).fail(function() {
                console.log("fcode fail..");
            });

        localStorage.setItem('userIdName', JSON.stringify('username' + "," + userId));
        var usersID = JSON.stringify(userId);

        if (usersID == '\"BSMOTORS\"' || usersID == '\"TVS\"') {

            window.localStorage.setItem('refreshTime', 120000);

        } else {

            window.localStorage.setItem('refreshTime', 60000);
        }

    });

    $('#userIds').on('change', function() {

        var postValue = {
            'id': $(this).val().toUpperCase(),
            '_token': $('input[name="_token"]').val()

        };
        // alert($('#groupName').val());
        $.post('{{ route('ajax.apiKeyAcess') }}', postValue)
            .done(function(data) {

                // $('#validation').text(data);
                localStorage.setItem('apiKey', JSON.stringify(data));

            }).fail(function() {
                console.log("fail");
            });

        $.post('{{ route('ajax.dealerAcess') }}', postValue)
            .done(function(data) {

                //alert(data);
                localStorage.setItem('dealerName', data);

            }).fail(function() {
                console.log("fail");
            });


    });
    var modal = document.getElementById('id01');

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $(".password");
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>

</html>
