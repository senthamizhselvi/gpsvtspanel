<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login V1</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />

    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('fonts/font-awesome-4.7.0login1/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('vendor/css-hamburgerslogin1/hamburgers.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{ URL::asset('css/utillogin1.css') }}>
    <link rel="stylesheet" type="text/css" href={{ URL::asset('css/mainlogin1.css') }}>
    <!--===============================================================================================-->
    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('common/css/atlantis.css') }}">
    <style type="text/css">
        input[type="color"] {
            -webkit-appearance: none;
            width: 60%;
            height: 30px;
            border: 0;
            border-radius: .5rem;
            padding: 0;
            overflow: hidden;
            box-shadow: 2px 2px 5px rgba(0, 0, 0, .1);
        }

        input[type="color"]::-webkit-color-swatch-wrapper {
            padding: 0;
        }

        input[type="color"]::-webkit-color-swatch {
            border: none;
        }

        h3 {
            font-family: Poppins-Bold;
            line-height: 1.2;
        }

        #wrapper {
            /* float: left; */
            background: #fff;
            box-shadow: #9990;
            border-radius: 25px;
            color: white;
        }

        label {
            margin-bottom: .5rem;
            font-family: Poppins-Bold;
            line-height: 1.2;
        }

        .wrap-login100{
            padding: 100px !important;
        }

        *::-webkit-scrollbar {
            width: 8px;
            height: 8px;
        }

        *::-webkit-scrollbar-track {
            background-color: #e4e4e4;
            border-radius: 100px;
        }

        *::-webkit-scrollbar-thumb {
            border-radius: 100px;
            background-image: linear-gradient(180deg, #a19f9f 0%, #a19f9f 99%);
            box-shadow: inset 2px 2px 5px 0 rgba(#fff, 0.5);
        }

    </style>
</head>

<body>
    <div id="styleDiv">
        <style></style>
    </div>
    <div class="row ">
        <div class="col-md-4 my-auto px-md-5 px-0 px-sm-2 col-12  col-sm-12">
            <div>
                <div class="container shadow" id="wrapper">
                    {{ Form::open(['url' => '/Upload', 'enctype' => 'multipart/form-data', 'id' => 'frmSub']) }}
                    {{ Form::hidden('dealerName', $dealerName, ['id' => 'dealerName']) }}

                    <h3 class="text-center m-3 text-dark"> Design 3</h3>
                    <div class="row my-4 p-4">
                        {{-- hidden input --}}
                        <input type="hidden" name="fontcolor" id="ft_color">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token">
                        <input type="hidden" name="template_name" value="3">

                        <div class="col-md-12 form-group ">
                            <label for="bg1">
                                Background Color 1
                            </label><br>
                            <div class="d-flex">
                                <input type="text" name="bg_color" id="bg_color" class="form-control"
                                    placeholder="Hex color code" value="">
                                <input type="color" id="color" class="w-25 form-control rounded-left">
                            </div>
                            {{-- <input type="color" id="color" class="w-100"> --}}
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="bg2">
                                Background Color 2
                            </label><br>
                            <div class="d-flex">
                                <input type="text" name="bg_color1" id="bg_color1" class="form-control"
                                    placeholder="Hex color code" value="">
                                <input type="color" id="color1" class="w-25 form-control rounded-left">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="fontcoler">Web Theme Color </label><br>
                            <div class="d-flex">
                                <input type="text" name="themecolor" id="themecolor" class="form-control"
                                    placeholder="Hex color code">
                                <input type="color" id="theme_color" class="w-25 form-control rounded-left">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="logo">
                                Logo
                            </label><br>
                            <div class="custom-file w-100">
                                <input type="file" class="custom-file-input" id="myFile1" name="logo"
                                    onchange="readURL(this)" accept="image/x-png,image/gif,image/jpeg">
                                <label class="custom-file-label text-dark" for="myFile"></label>
                            </div>
                            @if ($errors->has('logo'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('logo') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-12 text-center mt-3">
                            <input type="submit" class="btn btn-primary" value="Upload" name="submit">
                        </div>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-md-8  col-12">
            <div class="limiter">
                <div class="container-login100" id="grid">
                    <div class="wrap-login100">
                        <div class="login100-pic js-tilt" data-tilt>
                            <img id="blah" src="{{ URL::asset('assets/imgs/logo2.png') }}" alt="IMG">
                        </div>
                        <form class="login100-form validate-form">
                            <span class="login100-form-title">
                                Member Login
                            </span>

                            <div class="wrap-input100 validate-input"
                                data-validate="Valid email is required: ex@abc.xyz">
                                <input class="input100" type="text" name="email" placeholder="Email">
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                </span>
                            </div>

                            <div class="wrap-input100 validate-input" data-validate="Password is required">
                                <input class="input100" type="password" name="pass" placeholder="Password">
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                </span>
                            </div>

                            <div class="container-login100-form-btn">
                                <button class="login100-form-btn">
                                    Login
                                </button>
                            </div>

                            <div class="text-center p-t-12">
                                <a class="txt2" href="#">
                                    Forgot Password?
                                </a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('common/js/core/jquery.3.2.1.min.js') }}"></script>
    <script src="{{ asset('common/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('common/js/core/bootstrap.min.js') }}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('common/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <!-- Sweet Alert -->
    <script src="{{ asset('common/js/plugin/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Atlantis JS -->
    <script src="{{ asset('common/js/atlantis.min.js') }}"></script>

    <script src="{{ asset('common/js/script.js') }}"></script>
    <script type="text/javascript">
        $('#frmSub').on('submit', function(e) {
            var bgLogo = $('#myFile1').val();
            if (bgLogo == '') {
                wornigAlert("Select the Logo Image");
            } else {
                var extensionLogo = bgLogo.split('.').pop().toUpperCase();
                if (extensionLogo != "BMP" && extensionLogo != "PNG" && extensionLogo != "JPEG" &&
                    extensionLogo != "JPG") {
                    e.preventDefault();
                    wornigAlert("You can upload Logo that are .JPEG, .JPG, or .PNG.");
                    return false;
                }
            }

        });

        $('#bg_color').on('change', function() {
            $('#color').val($(this).val())
            backgroundColorChange()
        })
        $('#bg_color1').on('change', function() {
            $('#color1').val($(this).val())
            backgroundColorChange()
        })
        $('#themecolor').on('change', function() {
            $('#theme_color').val($(this).val())
            $(".login100-form-btn").css('background', $(this).val());
        });
        $('#color').on("change", function() {
            $("#bg_color").val($(this).val());
            backgroundColorChange()
        });

        $('#color1').on("change", function() {
            $("#bg_color1").val($(this).val());
            backgroundColorChange()
        });
        $('#theme_color').on('change', function() {
            $("#themecolor").val($(this).val());
            $(".login100-form-btn").css('background', $(this).val());
            change_placeholder_color($(this).val())
        });

        function change_placeholder_color(color_choice) {
            $("#styleDiv>style").remove();
            $("#styleDiv").append("<style>.fa::before{color:" +  color_choice + " !important;}</style>");
            $("#styleDiv").append("<style>.focus-input100{color:" +  color_choice + " !important;}</style>");
            $("#styleDiv").append("<style>.txt2:hover{color:" +  color_choice + " !important;}</style>");
        }

        function backgroundColorChange() {
            $(".wrap-login100").css('background', "linear-gradient(-160deg," + $('#color').val() + "," +
                $('#color1').val() + ")");
        }
        const imageFormat = ['NONE', 'PNG', 'JPG', 'APNG', 'JPEG', 'WEBP', 'SVG', 'GIF'];
        function readURL(input) {
            var file = input.files[0];
            var fileName = file.name.split('.');
            var fileType = fileName[fileName.length - 1].toUpperCase();
            if (imageFormat.includes(fileType)) {
                url = URL.createObjectURL(file);
                $('#blah')
                    .attr('src', url)
                    .width(150)
                    .height(150);
               
            } else {
                wornigAlert(fileType + ' Format not support.')
            }
        }
    </script>
    <script src={{ URL::asset('vendor/tiltlogin1/tilt.jquery.min.js') }}></script>
    <script>
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>
    <!--===============================================================================================-->
    <script src="{{ URL::asset('js/mainlogin1.js') }}"></script>
</body>

</html>
