<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Admin Panel</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <meta name="description" content="Admin panel of GPSVTS" />
    <link rel="icon" href="{{ asset('assets/imgs/favicon.ico') }}" type="image/x-icon" />
    <!-- Fonts and icons -->
    <script src="{{ asset('common/js/plugin/webfont/webfont.min.js') }}"></script>
    <script src="{{ asset('common/js/core/jquery.3.2.1.min.js') }}"></script>
    <script src="{{ asset('scripts/homer.js') }}"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">

    <script>
        WebFont.load({
            google: {
                "families": ["Lato:300,400,700,900"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular",
                    "Font Awesome 5 Brands", "simple-line-icons"
                ],
                urls: ['{{ asset('common/css/fonts.min.css') }}']
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('common/css/atlantis.css') }}">

    <style>
        input[type="color"] {
            -webkit-appearance: none;
            width: 200px;
            height: 30px;
            border: 0;
            border-radius: .5rem;
            padding: 0;
            overflow: hidden;
            box-shadow: 2px 2px 5px rgba(0, 0, 0, .1);
        }

        input[type="color"]::-webkit-color-swatch-wrapper {
            padding: 0;
        }

        input[type="color"]::-webkit-color-swatch {
            border: none;
        }

        .fa-one::before {
            content: ' 1 ';
        }

        .fa-two::before {
            content: ' 2 ';
        }

        .fa-three::before {
            content: ' 3 ';
        }

        .fa-four::before {
            content: ' 4 ';
        }

        .wrapper {
            height: 0;
            min-height: 0
        }

        .main-panel {
            width: clamp(100%,calc(100% - 100px),100%);
        }

        /* .sidebar_minimize_hover .fas {
            display: none;
        } */

        .sidebar_minimize_hover .strong>.fa-sign-out-alt {
            display: block;
        }

        *::-webkit-scrollbar {
            width: 8px;
        }

        *::-webkit-scrollbar-track {
            background-color: #e4e4e4;
            border-radius: 100px;
        }

        *::-webkit-scrollbar-thumb {
            border-radius: 100px;
            background-image: linear-gradient(180deg, #a19f9f 0%, #a19f9f 99%);
            box-shadow: inset 2px 2px 5px 0 rgba(#fff, 0.5);
        }

    </style>
</head>

<body class="m-0 p-0" data-background-color="bg3">
    <div class="wrapper sidebar_minimize ">
        @include('layouts.top_nav_bar')
        <div class="sidebar sidebar-style-2 ">
            <div class="sidebar-wrapper scrollbar scrollbar-inner">
                <div class="sidebar-content">
                    <ul class="nav nav-primary">
                        <li class="nav-item " id="design1">
                            <a href="#" class="justify-content-center">
                                <i class="fas fa-one"></i>
                                <p class="strong">@lang('messages.Design') </p>
                            </a>
                        </li>
                        <li class="nav-item " id="design2">
                            <a href="#" class="justify-content-center">
                                <i class="fas fa-two"></i>
                                <p class="strong">@lang('messages.Design') </p>
                            </a>
                        </li>
                        <li class="nav-item " id="design3">
                            <a href="#" class="justify-content-center">
                                <i class="fas fa-three"></i>
                                <p class="strong">@lang('messages.Design') </p>
                            </a>
                        </li>
                        <li class="nav-item " id="design4">
                            <a href="#" class="justify-content-center">
                                <i class="fas fa-four"></i>
                                <p class="strong">@lang('messages.Design') </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ URL::to('Business') }}" class="justify-content-center">
                                <i class="fas fa-sign-out-alt"></i>
                                <p class="strong">@lang('messages.Back') </p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="main-panel">
            <div class="content ">
                <div class="page-inner p-0">
                    <iframe class="p-0 m-0 border-0 overflow-none" scrolling="yes" id="frame" src="" width="100%"
                        height="100%" style="height: 100vh">
                    </iframe>
                </div>
            </div>
        </div>
    </div>

    {{ Form::hidden('dealerName', $dealerName, ['id' => 'dealerName']) }}



    <script src="{{ asset('common/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('common/js/core/bootstrap.min.js') }}"></script>
    <!-- jQuery UI -->
    <script src="{{ asset('common/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <!-- jQuery Scrollbar -->
    <script src="{{ asset('common/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
    <!-- Atlantis JS -->
    <script src="{{ asset('common/js/atlantis.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            var datas = $('#dealerName').val();
            $("#frame").attr("src", "temp1/" + datas);
            $('#design1').addClass('active')

            function listActive(id) {
                $('li').removeClass('active');
                $(id).addClass('active');
            }

            $("#design1").click(function() {
                listActive(this);
                var datas = $('#dealerName').val();
                $("#frame").attr("src", "temp1/" + datas);
            });

            $("#design2").click(function() {
                listActive(this);
                var datas = $('#dealerName').val();
                $("#frame").attr("src", "temp2/" + datas);
            });
            $("#design3").click(function() {
                listActive(this);
                var datas = $('#dealerName').val();
                $("#frame").attr("src", "temp3/" + datas);
            });
            $("#design4").click(function() {
                listActive(this);
                var datas = $('#dealerName').val();
                $("#frame").attr("src", "temp4/" + datas);
            });

        })
    </script>
</body>

</html>
