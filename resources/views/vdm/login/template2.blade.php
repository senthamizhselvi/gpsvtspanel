<!DOCTYPE html>
<html lang="en">

<head>
    <title>GPS</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap13/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic13/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate13/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition13/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util13.css">
    <link rel="stylesheet" type="text/css" href="css/main13.css">
    <!--===============================================================================================-->
    <style type="text/css">
        .img-circular {
            width: 130px;
        }

        .modal-content {
            background: linear-gradient(-135deg, {{ $bcolor }}, {{ $bcolor1 }});
            margin: 5% auto 23% auto;
            border: 1px solid #888;
            border-radius: 12px;
            width: 330px;
            min-height: 60vh;
            max-height: 100vh;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        .input100::-webkit-input-placeholder {
            color: {{ $fcolor }} !important;
        }

        .input100:-ms-input-placeholder {
            color: {{ $fcolor }} !important;
        }

        .input100,
        .input100::placeholder {
            color: {{ $fcolor }} !important;
        }

        .focus-input100::after {
            color: {{ $fcolor }} !important;
        }

        .focus-input100::before {
            background: {{ $fcolor }} !important;
        }

        <?php if($themecolor !=""): ?> .login100-form-btn {
            background: {{ $themecolor }} !important;
        }

        <?php endif; ?> .field-icon {
            float: right;
            position: relative;
            z-index: 2;
            margin-top: -55px;
            color: white;
        }

        .field-icon {
            color: {{ $fcolor }} !important;
        }

        .img-circular {
            width: 130px;
        }

    </style>
</head>

<body style="background-color: #999999;" onload="callMe()">
    <div class="limiter">
        <div class="container-login100">
            <div class="login100-more" style="background-image:url({{ $backgrounds }});"></div>

            <div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50"
                style="background: linear-gradient(-135deg, {{ $bcolor }}, {{ $bcolor1 }});">
                {{ Form::open(['url' => route('dologin'), 'class' => 'login100-form validate-form']) }}
                <span
                    style="font-size: 60px; color: #333333;display: -webkit-box;display: -webkit-flex;display: -moz-box;display: -ms-flexbox;display: flex;justify-content: center;align-items: center;width: 120px; margin: 0 auto;">
                    <img class="img-circular" id="blah" src="{{ $logo }}" />
                </span>
                <span class="login100-form-title p-b-25" style="color: {{ $fcolor }};">
                    LOG IN
                </span>


                <div class="wrap-input100 validate-input" data-validate="Username is required">
                    <span class="label-input100" style="color: {{ $fcolor }};">Username</span>
                    <input class="input100" type="text" name="userName" id="userIds">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Password is required">
                    <span class="label-input100" style="color: {{ $fcolor }};">Password</span>
                    <input class="input100 password" type="password" name="password">
                    <span class="focus-input100"></span>
                </div>
                <div>
                    <i class="fa fa-eye field-icon toggle-password" toggle="#password-field"></i>
                </div>


                <div class="flex-m w-full p-b-33">
                    <div class="contact100-form-checkbox">
                        <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember_me">
                        <label class="label-checkbox100" for="ckb1">
                            <span class="txt1" style="color: {{ $fcolor }};">
                                Remember me
                            </span>
                        </label>
                    </div>


                </div>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn" type="submit" style="color: {{ $fcolor }};"
                            id="clickme">
                            Sign in
                        </button>
                    </div>

                    <a class="dis-block txt3 mt-4" style="font-size: 76%;color: {{ $fcolor }};"
                        href="password/resetingWin">
                        Forgot Password?
                    </a>
                </div>
                <div class="text-center p-t-25">
                    <div class="txt1 text-danger text-uppercase">
                        @if (session()->has('flash_notice'))
                            {{ session()->get('flash_notice') }}
                        @endif
                        <div>
                            {{ HTML::ul($errors->all()) }}
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>

    <div id="id01" class="modal">

        <form class="modal-content animate" action="password/resetting">
            <div class="imgcontainer">
                <span onclick="document.getElementById('id01').style.display='none'" class="close"
                    title="Close">&times;</span>
                <img class="img-circular" id="blah" src="{{ $logo }}" />
            </div>


            <div class="wrap-input100 validate-input1" data-validate="Enter username"
                style="margin-left: 5%;width: 90%;">
                <input class="input100" type="text" name="uname" placeholder="Enter Username" required>
                <span class="focus-input100" data-placeholder="&#xf207;"></span>
            </div>

            <div class="container-login100-form-btn">
                <div class="wrap-login100-form-btn">
                    <div class="login100-form-bgbtn"></div>
                    <button class="login100-form-btn" type="submit" style="color: {{ $fcolor }};min-width: 0;">
                        Send Password Reset Link
                    </button>
                </div>
        </form>
    </div>

    <!--===============================================================================================-->
    <script src="vendor/jquery13/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/animsition13/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap13/js/popper.js"></script>
    <script src="vendor/bootstrap13/js/bootstrap.min.js"></script>
    <script src="js/main13.js"></script>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
    localStorage.clear();
    var globalIP = document.location.host;
    var contextMenu = '/' + document.location.pathname.split('/')[1];

    function callMe() {

        var isChrome = !!window.chrome && !!window.chrome.webstore;

        if (isChrome == false && (navigator.userAgent.indexOf('Chrome') == -1)) {

            for (i = 0; i <= 15; i++) {
                alert("Please Open Site on Google Chrome");
            }
        }
    }

    $('#clickme').click(function() {

        var userId = $('#userIds').val().toUpperCase();
        var postVal = {
            'id': userId,
            '_token': $('input[name="_token"]').val()
        };

        //$.get('http://128.199.159.130:9000/isAssetUser?userId=MSS', function(response) {
        $.get('//' + globalIP + contextMenu + '/public/isAssetUser', function(response) {
            //alert(response);
            localStorage.setItem('isAssetUser', response);
        }).error(function() {
            console.log('error in isAssetUser');
        });

        $.post('{{ route('ajax.fcKeyAcess') }}', postVal)
            .done(function(data) {

                localStorage.setItem('fCode', data);
                //alert(data);

            }).fail(function() {
                console.log("fcode fail..");
            });

        localStorage.setItem('userIdName', JSON.stringify('username' + "," + userId));
        var language = $('#lang').val();
        localStorage.setItem('lang', language);
        var usersID = JSON.stringify(userId);

        if (usersID == '\"BSMOTORS\"' || usersID == '\"TVS\"') {

            window.localStorage.setItem('refreshTime', 120000);

        } else {

            window.localStorage.setItem('refreshTime', 60000);
        }

    });

    $('#userIds').on('change', function() {

        var postValue = {
            'id': $(this).val().toUpperCase(),
            '_token': $('input[name="_token"]').val()
        };
        // alert($('#groupName').val());
        $.post('{{ route('ajax.apiKeyAcess') }}', postValue)
            .done(function(data) {

                // $('#validation').text(data);
                localStorage.setItem('apiKey', JSON.stringify(data));

            }).fail(function() {
                console.log("fail");
            });

        $.post('{{ route('ajax.dealerAcess') }}', postValue)
            .done(function(data) {

                //alert(data);
                localStorage.setItem('dealerName', data);

            }).fail(function() {
                console.log("fail");
            });

    });

    // Get the modal
    var modal = document.getElementById('id01');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $(".password");
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>

</html>
