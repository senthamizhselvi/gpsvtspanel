<!DOCTYPE html>
<html lang="en">

<head>
    <title>GPS</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400' rel='stylesheet' type='text/css'>
    <style>
        :root {
            --font: #000;
            --theme: #f26f21;
            --white: #ffffff;
            --size: 20px;
            --height: 70px;
        }

        :root {
            <?php if($fcolor !== ""):  ?>
                --font: <?php echo $fcolor; ?> !important;
            <?php endif; ?>
            <?php if($themecolor !== ""):  ?>
                --theme: <?php echo $themecolor; ?> !important;
            <?php endif; ?>
        }

        body {
            width: 100%;
            min-height: 100vh;
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            padding: 15px;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;

            position: relative;
            z-index: 1;
        }

        h1,
        input::-webkit-input-placeholder,
        button {
            font-family: "roboto", sans-serif;
            -webkit-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }

        h1 {
            height: var(--height);
            width: 100%;
            font-size: 18px;
            background: var(--theme);
            color: var(--white);
            line-height: 150%;
            border-radius: 5px 5px 0 0;
            box-shadow: 0 2px 5px 1px rgba(0, 0, 0, 0.2);
        }

        img {
            height: var(--height);
            width: auto;
        }

        form {
            box-sizing: border-box;
            width: 300px;
            box-shadow: 2px 2px 5px 1px rgba(0, 0, 0, 0.2);
            background-color: var(--white);
            padding-bottom: 20px;
            border-radius: 10px;
        }

        .input-content-wrap {
            margin-top: var(--size);
        }

        .input::before {
            content: "";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 300px;
            background: var(--font);
        }

        .input-content {
            position: relative;
            background: var(--white);
            background: linear-gradient(-135deg, <?php echo $bcolor; ?>, <?php echo $bcolor1; ?>);
            z-index: 10;
        }

        .input-content .inputbox {
            overflow: hidden;
            position: relative;
            padding: 0 var(--size);
            padding-top: 15px;
        }

        .input-content .inputbox-title {
            position: absolute;
            top: 15px;
            left: 0;
            width: 200px;
            height: 30px;
            color: #666;
            font-weight: bold;
            line-height: 30px;
        }

        .input-content .inputbox-content {
            position: relative;
            width: 100%;
        }

        .input-content .inputbox-content input {
            width: 100%;
            height: 30px;
            box-sizing: border-box;
            line-height: 30px;
            font-size: 14px;
            border: 0;
            background: none;
            border-bottom: 1px solid var(--font);
            color: var(--font);
            outline: none;
            border-radius: 0;
            -webkit-appearance: none;
        }

        .input-content .inputbox-content input:focus~label,
        .input-content .inputbox-content input:valid~label {
            color: var(--font);
            transform: translateY(-20px);
            font-size: 0.825em;
            cursor: default;
        }

        .input-content .inputbox-content input:focus~.underline {
            width: 100%;
        }

        .input-content .inputbox-content label {
            position: absolute;
            top: 0;
            left: 0;
            height: 30px;
            line-height: 30px;
            color: #ccc;
            cursor: text;
            transition: all 200ms ease-out;
            z-index: 10;
        }

        .input-content .inputbox-content .underline {
            content: "";
            display: block;
            position: absolute;
            bottom: -1px;
            left: 0;
            width: 0;
            height: 2px;
            background: var(--font);
            transition: all 200ms ease-out;
        }

        /* btn */
        .input-content .btns {
            display: flex;
        }

        .input-content .btns .btn {
            display: inline-block;
            margin-right: 2px;
            background: none;
            border: 1px solid #c0c0c0;
            border-radius: 2px;
            color: #666;
            font-size: 12px;
            outline: none;
            transition: all 100ms ease-out;
        }

        .input-content .btns .btn:hover,
        .input-content .btns .btn:focus {
            transform: translateY(-3px);
        }

        .input-content .btns .btn-confirm {
            border: 1px solid var(--theme);
            background: var(--white);
            color: var(--theme);
            margin: auto;
            border-radius: 10px;
        }

        .input-content .btns .btn-confirm:hover {
            background: var(--theme);
            color: var(--white);
        }

        .custom-control-input+label {
            color: #c0c0c0 !important;

        }

        .custom-control-input:checked+label {
            color: var(--font) !important;
            font-weight: 500;
        }

        .remember-me {
            margin: 0;
            margin: 0 var(--size);
        }

        select {
            border: 1px solid var(--theme);
            padding: 2px;
            border-radius: 10px;
        }

        select:-moz-focusring {
            color: transparent;
            text-shadow: 0 0 0 #000;
        }

        a,
        label {
            font-size: 14px;
        }

        a:hover {
            color: var(--font);
        }
        .field-icon {
            float: right;
            margin-left: -25px;
            margin-top: 10px;
            position: relative;
            z-index: 2;
            color: <?php echo $fcolor; ?> !important;
        }
    </style>
</head>

<body style="background-image:url({{ $backgrounds }});background-size: cover;" onload="callMe()">
    {{ Form::open(['url' => 'login', 'class' => 'input-content']) }}
    <form class="input-content wrap-login100" style="linear-gradient(-160deg,{{ $bcolor }}, {{ $bcolor1 }})">
        <h1 class="d-flex justify-content-around">
            <img src="{{ $logo }}" alt="logo">
        </h1>
        <div class="input-content-wrap">
            <dl class="inputbox">
                <dd class="inputbox-content">
                    <input id="input0" type="text" name="userName" required id="userIds" />
                    <label for="input0"><i class="fa fa-user"></i> &nbsp;User Name</label>
                    <span class="underline"></span>
                </dd>
            </dl>
            <dl class="inputbox">
                <dd class="inputbox-content">
                    <input id="input1" type="password" class="password" name="password" required />
                    <label for="input1"><i class="fa fa-lock"></i> &nbsp;Password</label>
                    <i class="fa fa-eye field-icon toggle-password" toggle="#password-field"></i>
                        <span class="underline"></span>
                </dd>
            </dl>
            <div class="custom-control custom-checkbox my-1 mr-sm-2 remember-me">
                <input type="checkbox" class="custom-control-input" id="customControlInline" name="remember_me">
                <label class="custom-control-label" for="customControlInline">Remember Me</label>
            </div>
            <div class="btns mt-3 ">
                <button class="btn btn-confirm" id="clickme">LOGIN</button>
            </div>
            <h5 style="color: white;    margin-left: 9%;-size: 17px;">
                <?php if(Session::has('flash_notice')): ?>
                <div style="font-family: monospace;font-size: 15px;color: red;line-height: 1.2;text-align: center;text-transform: uppercase;display: block;"
                    id="flash_notice">
                    <?php echo Session::get('flash_notice'); ?>
                </div>
                <?php endif; ?>
                <span id="error"
                    style="font-family: monospace;font-size: 15px;color: red;line-height: 1.2;text-align: center;text-transform: uppercase;display: block;margin-top: 2vh; margin-left: -4vh;">{{ HTML::ul($errors->all()) }}</span>
            </h5>
            <div class="d-flex justify-content-around align-items-center mt-3">
                <select name="lang" class="">
                    <option value="en">English</option>
                    <option value="hi">Hindi</option>
                    <option value="es">Spanish</option>
                </select>
                <a href="password/resetingWin"> Forgot Password? </a>
            </div>
        </div>
    </form>
    {{ Form::close() }}
    <div id="id01" class="modal">

        <form class="modal-content animate" action="password/resetting">
            <div class="imgcontainer">
                <span onclick="document.getElementById('id01').style.display='none'" class="close"
                    title="Close">&times;</span>
                <img class="avatar" id="avatarsrc" height="120" width="150" src="{{ $logo }}" />
            </div>


            <div class="wrap-input100 validate-input1" data-validate="Enter username"
                style="margin-left: 5%;width: 90%;">
                <input class="input100" type="text" id="usern" name="uname" placeholder="Enter Username" required>
                <span class="focus-input100" data-placeholder="&#xf207;"></span>
            </div>
            <div class="container-login100-form-btn">
                <button class="login100-form-btn" type="submit">
                    Send Password Reset Link
                </button>
            </div>
        </form>
    </div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
    localStorage.clear();
    var globalIP = document.location.host;
    var contextMenu = '/gps';

    function callMe() {

        var isChrome = !!window.chrome && !!window.chrome.webstore;

        if (isChrome == false && (navigator.userAgent.indexOf('Chrome') == -1)) {

            for (i = 0; i <= 15; i++) {
                alert("Please Open Site on Google Chrome");
            }
        }
    }

    $('#clickme').click(function() {

        var userId = $('#userIds').val().toUpperCase();
        var userIP = localStorage.getItem('myIP');
        var postVal = {
            'id': userId,
            'userIP': userIP
        };
        localStorage.setItem('lang', 'en');

        //$.get('http://128.199.159.130:9000/isAssetUser?userId=MSS', function(response) {
        $.get('//' + globalIP + contextMenu + '/public/isAssetUser', function(response) {
            //alert(response);
            localStorage.setItem('isAssetUser', response);
        }).error(function() {
            console.log('error in isAssetUser');
        });

        $.post('{{ route('ajax.fcKeyAcess') }}', postVal)
            .done(function(data) {

                localStorage.setItem('fCode', data);
                //alert(data);

            }).fail(function() {
                console.log("fcode fail..");
            });

        localStorage.setItem('userIdName', JSON.stringify('username' + "," + userId));
        var usersID = JSON.stringify(userId);

        if (usersID == '\"BSMOTORS\"' || usersID == '\"TVS\"') {

            window.localStorage.setItem('refreshTime', 120000);

        } else {

            window.localStorage.setItem('refreshTime', 60000);
        }

    });

    $('#userIds').on('change', function() {

        var postValue = {
            'id': $(this).val().toUpperCase()

        };
        // alert($('#groupName').val());
        $.post('{{ route('ajax.apiKeyAcess') }}', postValue)
            .done(function(data) {

                // $('#validation').text(data);
                localStorage.setItem('apiKey', JSON.stringify(data));

            }).fail(function() {
                console.log("fail");
            });

        $.post('{{ route('ajax.dealerAcess') }}', postValue)
            .done(function(data) {

                //alert(data);
                localStorage.setItem('dealerName', data);

            }).fail(function() {
                console.log("fail");
            });


    });
    var modal = document.getElementById('id01');

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    $(function() {
        if ("{{ $themecolor }}" !== "") {
            window.localStorage.setItem('themeColor', "{{ $themecolor }}");
        } else {
            window.localStorage.setItem('themeColor', "#196481");
        }
    });
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $(".password");
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>

</html>
