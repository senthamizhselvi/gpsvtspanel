<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login V3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
        integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('common/css/atlantis.css') }}">
    <style type="text/css">
        *::-webkit-scrollbar {
            width: 8px;
            height: 8px;
        }

        *::-webkit-scrollbar-track {
            background-color: #e4e4e4;
            border-radius: 100px;
        }

        *::-webkit-scrollbar-thumb {
            border-radius: 100px;
            background-image: linear-gradient(180deg, #a19f9f 0%, #a19f9f 99%);
            box-shadow: inset 2px 2px 5px 0 rgba(#fff, 0.5);
        }
        input[type="color"] {
            -webkit-appearance: none;
            width: 60%;
            height: 30px;
            border: 0;
            border-radius: .5rem;
            padding: 0;
            overflow: hidden;
            box-shadow: 2px 2px 5px rgba(0, 0, 0, .1);
        }

        input[type="color"]::-webkit-color-swatch-wrapper {
            padding: 0;
        }

        #wrapper {
            /* float: left; */
            background: #fff;
            box-shadow: #9990;
            border-radius: 25px;
            /* color: white; */
        }

        input[type="color"]::-webkit-color-swatch {
            border: none;
        }

        :root {
            --theme: #f26f21;
            --font: #f26f21;
            --white: #ffffff;
            --size: 20px;
            --height: 70px;
        }

        .container-login100 {
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        /* h1, */
        input::-webkit-input-placeholder,
        button {
            font-family: "roboto", sans-serif;
            -webkit-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }

        h1 {
            height: var(--height);
            width: 100%;
            font-size: 18px;
            background: var(--theme);
            color: var(--white);
            line-height: 150%;
            border-radius: 5px 5px 0 0;
            box-shadow: 0 2px 5px 1px rgba(0, 0, 0, 0.2);
        }

        img {
            height: var(--height);
            width: auto;
        }

        .input-content {
            box-sizing: border-box;
            width: 300px;
            box-shadow: 2px 2px 5px 1px rgba(0, 0, 0, 0.2);
            background-color: var(--white);
            padding-bottom: 20px;
            border-radius: 10px;
        }

        .input-content-wrap {
            margin-top: var(--size);
        }

        .input::before {
            content: "";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 300px;
            background: var(--font);
        }

        .input-content {
            position: relative;
            background: var(--white);
            z-index: 10;
        }

        .input-content .inputbox {
            overflow: hidden;
            position: relative;
            padding: 0 var(--size);
            padding-top: 15px;
        }

        .input-content .inputbox-title {
            position: absolute;
            top: 15px;
            left: 0;
            width: 200px;
            height: 30px;
            color: #666;
            font-weight: bold;
            line-height: 30px;
        }

        .input-content .inputbox-content {
            position: relative;
            width: 100%;
        }

        .input-content .inputbox-content input {
            width: 100%;
            height: 30px;
            box-sizing: border-box;
            line-height: 30px;
            font-size: 14px;
            border: 0;
            background: none;
            border-bottom: 1px solid var(--font) !important;
            color: var(--font) !important;
            outline: none;
            border-radius: 0;
            -webkit-appearance: none;
        }

        .input-content .inputbox-content input:focus~label,
        .input-content .inputbox-content input:valid~label {
            color: var(--font) !important;
            transform: translateY(-20px);
            font-size: 0.825em;
            cursor: default;
        }

        .input-content .inputbox-content input:focus~.underline {
            width: 100%;
        }

        .input-content .inputbox-content label {
            position: absolute;
            top: 0;
            left: 0;
            height: 30px;
            line-height: 30px;
            color: var(--font);
            cursor: text;
            transition: all 200ms ease-out;
            z-index: 10;
        }

        .input-content .inputbox-content .underline {
            content: "";
            display: block;
            position: absolute;
            bottom: -1px;
            left: 0;
            width: 0;
            height: 2px;
            background: var(--font);
            transition: all 200ms ease-out;
        }

        /* btn */
        .input-content .btns {
            display: flex;
        }

        .input-content .btns .btn {
            display: inline-block;
            margin-right: 2px;
            background: none;
            border: 1px solid #c0c0c0;
            border-radius: 2px;
            color: #666;
            font-size: 12px;
            outline: none;
            transition: all 100ms ease-out;
        }

        .input-content .btns .btn:hover,
        .input-content .btns .btn:focus {
            transform: translateY(-3px);
        }

        .input-content .btns .btn-confirm {
            border: 1px solid var(--theme);
            background: var(--white);
            color: var(--theme);
            margin: auto;
            border-radius: 10px;
        }

        .input-content .btns .btn-confirm:hover {
            background: var(--theme);
            color: var(--white);
        }

        .col-md-8 label {
            color: #c0c0c0 !important;

        }

        .custom-control-input:checked+label {
            color: var(--font) !important;
            font-weight: 500;
        }

        .remember-me {
            margin: 0;
            margin: 0 var(--size);
        }

        select {
            border: 1px solid var(--theme);
            padding: 2px;
            border-radius: 10px;
        }

        select:-moz-focusring {
            color: transparent;
            text-shadow: 0 0 0 #000;
        }

        a,
        label {
            font-size: 14px;
        }

        a:hover {
            color: var(--font);
        }

        .page {
            background: #f1f1f1;
        }

    </style>
</head>

<body>
    <div id="stylefont">
        <style>
        </style>
    </div>
    <div id="styletheme">
        <style>
        </style>
    </div>
    <div class="row page">
        <div class="col-md-4 my-auto px-md-5 px-0 px-sm-2 col-12   col-sm-12">
            <div>
                <div class="container shadow" id="wrapper">
                    {{ Form::open(['url' => '/Upload', 'enctype' => 'multipart/form-data', 'id' => 'frmSub']) }}
                    {{ Form::hidden('dealerName', $dealerName, ['id' => 'dealerName']) }}

                    <h3 class="text-center m-3 text-dark">Design 4</h3>
                    <div class="row my-4 p-4">
                        <input type="hidden" name="template_name" value="4">

                        <div class="col-md-12 form-group ">
                            <label for="bg1">
                                Background Color 1
                            </label><br>
                            <div class="d-flex">
                                <input type="text" name="bg_color" id="bg_color" class="form-control" placeholder="Hex color code" value="#7579ff">
                                <input type="color" id="color" class="w-25 form-control rounded-left" value="#7579ff">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="bg2">
                                Background Color 2
                            </label><br>
                            <div class="d-flex">
                                <input type="text" name="bg_color1" id="bg_color1" class="form-control" placeholder="Hex color code" value="#b224ef">
                                <input type="color" id="color1" class="w-25 form-control rounded-left" value="#b224ef">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="fontcoler">Font Color </label><br>
                            <div class="d-flex">
                                <input type="text" name="fontcolor" id="ft_color" class="form-control" placeholder="Hex color code" value="#e5e8eb">
                                <input type="color" id="font_color" class="w-25 form-control rounded-left" value="#e5e8eb">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="fontcoler">Web Theme Color </label><br>
                            <div class="d-flex">
                            <input type="text" name="themecolor" id="themecolor" class="form-control" placeholder="Hex color code">
                            <input type="color" id="theme_color" class="w-25 form-control rounded-left">
                            </div>
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="bgi">
                                Background Image
                            </label><br>
                            <div class="custom-file w-100">
                                <input type="file" class="custom-file-input" id="myFile" name="background"
                                    onchange="myFunction(this)" accept="image/x-png,image/gif,image/jpeg">
                                <label class="custom-file-label text-dark" for="myFile"></label>
                            </div>
                            @if ($errors->has('background'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('background') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-12 form-group ">
                            <label for="logo">
                                Logo
                            </label><br>
                            <div class="custom-file w-100">
                                <input type="file" class="custom-file-input" id="myFile1" name="logo"
                                    onchange="readURL(this)" accept="image/x-png,image/gif,image/jpeg">
                                <label class="custom-file-label text-dark" for="myFile"></label>
                            </div>
                            @if ($errors->has('logo'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('logo') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-12 text-center mt-3">
                            <input type="submit" class="btn btn-primary" value="Upload" name="submit">
                        </div>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-md-8 mh-100 col-12">
            <div>
                <div class="container-login100"
                    style="background-image: url('{{ URL::asset('assets/imgs/background3.png') }}');">
                    <form class="input-content wrap-login100">

                        <h1 class="d-flex justify-content-around">
                            <img src="{{ URL::asset('assets/imgs/logo3.png') }}" id="blah" alt="logo">
                        </h1>
                        <div class="input-content-wrap">
                            <dl class="inputbox">
                                <dd class="inputbox-content">
                                    <input id="input0" type="text" required />
                                    <label for="input0" ><i class="fa fa-user"></i> &nbsp;User Name</label>
                                    <span class="underline"></span>
                                </dd>
                            </dl>
                            <dl class="inputbox">
                                <dd class="inputbox-content">
                                    <input id="input1" type="password" required />
                                    <label for="input1" ><i class="fa fa-lock"></i> &nbsp;Password</label>
                                    <span class="underline"></span>
                                </dd>
                            </dl>
                            <div class="custom-control custom-checkbox my-1 mr-sm-2 remember-me">
                                <input type="checkbox" class="custom-control-input" id="customControlInline">
                                <label class="custom-control-label" for="customControlInline">Remember Me</label>
                            </div>
                            <div class="btns mt-3 ">
                                <button class="btn btn-confirm">LOGIN</button>
                            </div>

                            <div class="d-flex justify-content-around align-items-center mt-3">
                                <select name="lang" class="">
                                    <option value="en">English</option>
                                    <option value="hi">Hindi</option>
                                    <option value="es">Spanish</option>
                                </select>
                                <a href="#"> Forgot Password? </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div id="dropDownSelect1"></div>
    <script src="{{ asset('common/js/core/jquery.3.2.1.min.js') }}"></script>
    <script src="{{ asset('common/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('common/js/core/bootstrap.min.js') }}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('common/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <!-- Sweet Alert -->
    <script src="{{ asset('common/js/plugin/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Atlantis JS -->
    <script src="{{ asset('common/js/atlantis.min.js') }}"></script>

    <script src="{{ asset('common/js/script.js') }}"></script>
    <script src="{{ URL::asset('js/main1.js') }}"></script>

    <script type="text/javascript">
        $('#frmSub').on('submit', function(e) {
            var bgIMG = $('#myFile').val();
            var bgLogo = $('#myFile1').val();
            if (bgIMG == '') {
                alert("Select the background Image");
                return false;
            } else if (bgLogo == '') {
                alert("Select the Logo Image");
                e.preventDefault();
                return false;
            } else {
                var extensionLogo = bgLogo.split('.').pop().toUpperCase();
                var extension = bgIMG.split('.').pop().toUpperCase();
                if (extension != "BMP" && extension != "PNG" && extension != "JPEG" && extension != "JPG") {
                    e.preventDefault();
                    alert("You can upload Background Image that are .JPEG, .JPG, or .PNG.");
                    return false;
                }
                if (extensionLogo != "BMP" && extensionLogo != "PNG" && extensionLogo != "JPEG" && extensionLogo !=
                    "JPG") {
                    e.preventDefault();
                    alert("You can upload Logo that are .JPEG, .JPG, or .PNG.");
                    return false;
                }
            }

        });

        $('#bg_color').on('change',function(){
            $('#color').val($(this).val())
            backgroundColorChange()
        })
        $('#bg_color1').on('change',function(){
            $('#color1').val($(this).val())
            backgroundColorChange()
        })
        $('#ft_color').on('change',function(){
            $('#font_color').val($(this).val())
            change_placeholder_color('font', $(this).val());
        })
        $('#themecolor').on('change',function(){
            $('#theme_color').val($(this).val())
            change_placeholder_color('theme', $(this).val());
        });
        $('#color').on("change", function() {
            $("#bg_color").val($(this).val());
            backgroundColorChange()
        });

        $('#color1').on("change", function() {
            $("#bg_color1").val( $(this).val());
            backgroundColorChange()
        });

        $('#font_color').on("change", function() {
            $("#ft_color").val($(this).val());
            change_placeholder_color('font', $(this).val());
        });

        function backgroundColorChange(){
            $(".wrap-login100").css('background', "linear-gradient(-160deg," + $('#color').val() + "," +
            $('#color1').val() + ")");
        }
        
        $('#theme_color').on('change', function() {
            $("#themecolor").val($(this).val());
            change_placeholder_color('theme', $(this).val());
        });

        function change_placeholder_color(type, color_choice) {
            $("#style" + type + ">style").remove();
            $("#style" + type).append("<style>" + ':root{ --' + type + ':' + color_choice +
                " !important;}</style>")
        }

        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
        }

        function myFunction(input) {
            var file = document.getElementById("myFile").files[0];
            var reader = new FileReader();
            reader.onloadend = function() {
                $(".container-login100").css('background', "url(" + reader.result + ")");
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                console.log('Failed');
            }
        }

        function readURL(input) {
            console.log("test");
            if (input.files && input.files[0]) {
                url = URL.createObjectURL(input.files[0]);
                $('#blah')
                    .attr('src', url);
               
            }
        }
    </script>

</body>

</html>
