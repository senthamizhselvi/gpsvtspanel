@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content">
            <div class="page-inner">
                <div class="hpanel">
                    @if (session()->has('message'))
                        <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                            {{ session()->get('message') }}
                        </p>
                    @endif
					<div class="page-body animated zoomIn">
						<div class="card">
							<div class="card-header">
								@lang('messages.SMSREPORT')
							</div>

							<div class="card-body">
								<div class="row">
									<div class="col-md-6 mx-auto">
										{{ HTML::ul($errors->all()) }}
										{{ Form::open(['url' => 'vdmSmsReport']) }}
										<div class="form-group">
											{{ Form::label('orgId', 'Organization List :') }}
											{{ Form::select('orgId', $orgsArr, request()->old('orgId'), ['class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true', 'required' => 'required']) }}
										</div>
			
										<div class="form-group">
											{{ Form::label('vehicleId', 'Vehicle Id :') }}
											{{ Form::text('vehicleId', request()->old('vehicleId'), ['class' => 'form-control', 'placeholder' => 'Vehicle Id', 'required' => 'required']) }}
										</div>
			
										<div class="form-group">
											{{ Form::label('Date', 'Date :') }}
											<input type="date" name="date" class="form-control" required>
											<!-- <form action="action_page.php"> -->
										</div>
			
										<div class="form-group">
											{{ Form::label('tripType', 'Trip :') }}
											{{ Form::select('tripType', ['pickUp' => 'PickUp', 'drop' => 'Drop'], request()->old('tripType'), ['class' => 'form-control', 'required' => 'required']) }}
										</div>
			
										<div class="form-group text-center">
											{{ Form::submit('Submit', ['class' => 'btn btn-md btn-primary']) }}
											<br>
										</div>
										{{ Form::close() }}
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
@endsection
