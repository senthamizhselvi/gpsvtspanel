@include('includes.header_index')
<div id="wrapper">
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-14">
                <div class="hpanel">
                    <div class="panel-heading">
                        @if (session()->has('message'))
                            <p class="alert {{ session()->get('alert-class', 'alert-info') }}">
                                {{ session()->get('message') }}</p>
                        @endif
                        <h4> <b>Devices List</b></h4>
                    </div>
                    <div class="panel-body">

                        <div class="form-group">
                            <table id="example1" class="table table-bordered dataTable" style="background: #ccffff;">
                                <thead>
                                    <tr style="background:  #00b3b3;color: #ffffff">
                                        <th style="text-align: center;">ID</th>
                                        <th style="text-align: center;">Device ID</th>
                                        <th style="text-align: center;">Device Model</th>
                                        <th style="text-align: center;">Device Type</th>
                                        <th style="text-align: center;">Mobile Number</th>
                                        <th style="text-align: center;">PreOnboard Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($deviceMap))
                                        @foreach ($deviceMap as $key => $value)
                                            <tr style="text-align: center; color: #000000">
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ explode(',', $value)[0] }}</td>
                                                <td>{{ explode(',', $value)[1] }}</td>
                                                <td>{{ explode(',', $value)[2] }}</td>
                                                <td>{{ explode(',', $value)[3] }}</td>
                                                <td>{{ explode(',', $value)[4] }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('includes.js_index')
</body>

</html>
