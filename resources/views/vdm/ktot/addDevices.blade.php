@extends('includes.vdmEditHeader')
@section('mainContent')
    <style>
        .countbtn {
            width: 50%;
            margin-left: 17%;
        }

    </style>
    <div id="wrapper">
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div style="background-color: #3ca9a9; border-radius: 11px; font-family: initial; margin-top: 9px; height: 6%;"
                            class="panel-heading" align="center">
                            <h4 style="margin-top: -5px; !impotant">
                                <font size="6px" color="white">Add Devices</font>
                            </h4>
                        </div>
                        <div class="panel-body">
                            {{ Form::open(['url' => 'ktot']) }}
                            <font style="color:red;font-weight:bold">{{ HTML::ul($errors->all()) }}</font>
                            <span id="error" style="color:red;font-weight:bold"></span>
                            <hr>
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-3">{{ Form::label('app', 'Application') }}</div>
                                <div class="col-md-3">
                                    {{ Form::text('app1', 'KEEP TRACK OF IT', ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-3">{{ Form::label('mobileNo', 'Mobile Number :') }}</div>
                                <div class="col-md-3">
                                    {{ Form::number('mobileNo', request()->old('mobileNo'), ['class' => 'form-control', 'placeholder' => 'Mobile Number', 'required' => 'required']) }}
                                </div>
                                {{ Form::hidden('numberofdevice', $numberofdevice, ['class' => 'form-control']) }}
                            </div>
                            <hr>

                            <div class="row">
                                <!-- <div class="col-md-2">as</div> -->
                                <div class="col-md-2"></div>
                                <div class="col-md-3"
                                    style="color: #ffffff; text-align: center; background: #3ca9a9; border-radius: 25px; margin-left: 86px; width: 13%;">
                                    {{ Form::label('imei', 'IMEI', ['disabled' => 'disabled']) }}</div>
                                <div class="col-md-3"
                                    style="color: #ffffff; text-align: center; background: #3ca9a9; border-radius: 25px; margin-left: 163px; width: 13%;">
                                    {{ Form::label('deviceModel', 'Device Model', ['disabled' => 'disabled']) }}
                                </div>
                                <div class="col-md-3"
                                    style="color: #ffffff; text-align: center; background: #3ca9a9; border-radius: 25px; margin-left: 163px; width: 13%;">
                                    {{ Form::label('deviceType', 'Device Type', ['disabled' => 'disabled']) }}</div>
                            </div>

                            @for ($i = 1; $i <= $numberofdevice; $i++)
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
                                <script>
                                    $(document).ready(function() {
                                        console.log('ahan');
                                        $('#deviceid{{ $i }}').on('change', function() {
                                            console.log('ahan1');
                                            var data = {
                                                'id': $(this).val()
                                            };
                                            $.post('{{ route('ajax.checkDevice') }}', data, function(data, textStatus, xhr) {
                                                $('#error').text(data.error);
                                                if (data.error != ' ') {
                                                    alert(data.error);
                                                }
                                            });
                                            //alert("The text has been changeded.");
                                        });
                                    });
                                </script>
                                <br>
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-3">
                                        {{ Form::text('deviceId' . $i, request()->old('deviceId'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'IMEI / Device Id', 'id' => 'deviceid' . $i]) }}
                                    </div>
                                    <div class="col-md-3">
                                        {{ Form::select('deviceModel' . $i, $protocol, 'select', ['class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true', 'required' => 'required']) }}
                                    </div>
                                    <div class="col-md-3">
                                        {{ Form::select('deviceType' . $i, ['' => '-- Select --', 'Bag' => 'Bag', 'Bike' => 'Bike', 'Car' => 'Car', 'PersonalTracker' => 'Personal Tracker', 'PetTracker' => 'Pet Tracker', 'Watch' => 'Watch'], 'select', ['class' => 'form-control selectpicker show-menu-arrow', 'data-live-search ' => 'true', 'required' => 'required']) }}
                                    </div>


                                </div>
                            @endfor
                            <br>
                            <div class="row">
                                <div class='col-md-6'></div>
                                <div class='col-md-4' align="center" style=" margin-left: -8%; width: 0%;">
                                    {{ Form::submit(' Submit !', ['class' => 'btn btn-success']) }}</div>
                                <div class='col-md-2'><a class="btn btn-sm "
                                        style="height: 33px; color: #ffffff; background-color: #b3294b;"
                                        href="Business">Cancel</a></div>
                            </div>

                            {{ Form::close() }}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div align="center">@stop</div>
