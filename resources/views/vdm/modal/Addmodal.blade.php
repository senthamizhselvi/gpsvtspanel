<div class="modal fade" id="addTagModal" tabindex="-1" role="dialog" aria-labelledby="addTagLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-md" role="document" id="addTagBody">
        <div class="modal-content">
            {{ Form::open(['url' => route('rfid.store')]) }}
            <div class="modal-header">
                <h4 class="b">
                    @lang('messages.AddTags')
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row py-2">
                    <div class="col-md-10  mx-auto">
                        <div class="form-group">
                            {{ Form::label('tags', __('messages.NumberOfRFIDTags'), ['class' => 'b']) }}
                            {{ Form::Number('tags', request()->old('tags'), ['class' => 'form-control', 'placeholder' => __('messages.Quantity')]) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>



<div class="modal fade" id="SwitchLoginModal" tabindex="-1" role="dialog" aria-labelledby="SwitchLoginLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-md" role="document" id="SwitchLoginBody">
        <div class="modal-content">
            {{ Form::open(['url' => route('vdmVehicles/findDealerList')]) }}
            <div class="modal-header">
                <h3 class="modal-title b">
                    @lang('messages.SelecttheDealer')
                </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               
            </div>
            <div class="modal-footer">
                {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-md btn-primary mx-auto']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

@push('js')
<script>
        @if (session()->get('SwitchLoginTrigger') == "true")
                $(document).ready(function(){
                    $('#SwitchLogin').trigger('click');
                });
        @endif
        @if(session()->get('addtagPopUp')=='true')
            $(document).ready(function(){
                $('#addTagPopUp').trigger('click');
            });
        @endif
        $('#SwitchLogin').one('click', function() {
            $('#SwitchLoginBody .modal-body').addClass('is-loading is-loading-lg my-4');
            $.get('{{ route('vdmVehicles/dealerSearchModel') }}').done(function(data, textStatus, xhr) {
                $('#SwitchLoginBody .modal-body').removeClass('is-loading is-loading-lg my-4');
                $('#SwitchLoginBody .modal-body').html(data);
                $('.selectpicker').css('width', '100%');
                $('.selectpicker').select2({
                    dropdownParent: $('#SwitchLoginModal .modal-content')
                });
                $('span.selection>.select2-selection.select2-selection--single').addClass(
                    'form-control py-select-1');
                $('.select2-selection.select2-selection--single>.select2-selection__arrow').attr('style',
                    "top:10px !important;right:10px !important");
                    @if (session()->has('switchErr'))
                        $(".switchErr").html('{{session()->get('switchErr')}}')
                    @endif
            });
        });
        $(document).ready(function() {
            $('.selectpicker').select2();
        });
    </script>

@endpush
