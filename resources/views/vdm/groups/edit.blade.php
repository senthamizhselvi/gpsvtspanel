@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content page-inner">
            <div class="hpanel">
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}</p>
                @endif
                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.EditGroup')
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('vdmGroups.index') }}">
                                @lang('messages.Groups')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.EditGroup')
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="page-body">
                    <div class="card">
                        {{ Form::model($groupId, ['route' => ['vdmGroups.update', $groupId], 'method' => 'PUT', 'id' => 'formid']) }}
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-3 form-group ml-0 ml-md-3">
                                    {{ Form::label('groupId', __('messages.GroupName'), ['class' => 'b  mt-2']) }} :  
                                    {{ Form::label('groupId', Arr::first(explode(":",$groupId)),['class'=>'text-primary']) }}
                                </div>
                                <div class="col-md-2 ml-auto">
                                    {{ Form::submit(__('messages.UpdateGroup'), ['class' => 'btn btn-primary  mr-3']) }}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <span class="text-danger error-message">
                                {{ HTML::ul($errors->all()) }}
                            </span>
                            <div class="row p-2">
                                <div class="col-12 ">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    {{ Form::checkbox('group', 'value', false, ['class' => 'form-check-input field check']) }}
                                                    <span class="form-check-sign">
                                                        @lang('messages.SelectallVehicles')
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <input type="text" placeholder="Search..." id="search" name="searchtext"
                                                    class="form-control searchkey">
                                                <div class="input-group-prepend">
                                                    <div class="btn btn-search btn-primary rounded-right" aria-label="search">
                                                        <i class="fa fa-search search-icon"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3 p-2">
                                <div class="col-md-12">
                                    <h3 class="SelectedVehicles">
                                        {{ Form::label('vehicleList', __('messages.SelectedVehicles') . ' :', ['class' => 'text-primary']) }}
                                    </h3>
                                </div>
                                <div class="col-md-12">
                                    <div class="row border-bottom mb-2" id="selectedItems"></div>
                                </div>
                            </div>
                            <div class="row p-2">
                                <div class="col-md-12 mb-3">
                                    <h3 id="SelecttheVehicles">
                                        {{ Form::label('vehicleList', __('messages.SelecttheVehicles') . ' :', ['class' => 'text-primary']) }}
                                    </h3>
                                </div>
                                <div class="col-md-12" id="unSelectedItems">
                                    <div class="row SelecttheVehicles">
                                        @if (isset($vehicleList))
                                            @foreach ($vehicleList as $key => $value)
                                                <div class="col-md-3 form-check vehiclelist">
                                                    <label class="form-check-label d-inline-block text-truncate w-100">
                                                        {{ Form::checkbox('vehicleList[]', $key . ' || ' . Arr::get($shortNameList, $value), in_array($value, $selectedVehicles), ['class' => 'form-check-input field', 'id' => 'questionCheckBox']) }}
                                                        <span class="form-check-sign "
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="{{ Arr::get($shortNameList, $value) }}">
                                                            {{ Arr::get($shortNameList, $value) }}
                                                        </span>
                                                    </label>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::hidden('value', json_encode($vehicleList, true)) }}
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        list = [];
        var value = $('input[name="value"]').val();
        $(window).bind("load", function() {
            if ($('.SelecttheVehicles>.vehiclelist').length == 0) {
                $('#SelecttheVehicles').hide();
            }
        })
    </script>
    @include('includes.js_footer')
@endpush
