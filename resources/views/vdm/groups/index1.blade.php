@extends("layouts.app")
@section('content')

    <div id="wrapper">
        <div class="content page-inner">
            <div class="hpanel">
                <div class="pull-right">
                    <a href="{{route("vdmGroup/sendExcel")}}"><img class="pull-right" width="40px" height="50px"
                            src="{{asset('assets/imgs/xls.svg')}}" method="get" alt="xls"/></a>
                </div>
                <div class="page-header">

                    <h4 class="page-title">
                        Group Search
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('vdmGroups.index') }}">Group List</a>
                        </li>
                    </ul>
                </div>
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}
                    </p>
                @endif
                <div class="page-body">
                    <div class="card">
                        <div class="card-header p-3">
                            {{ Form::open(['url' => 'vdmGroupsScan/Search', 'method' => 'post']) }}
                            <div class="row p-2">
                                <div class="col-md-6 form-group">
                                    {{ Form::label('Group Search') }}
                                    <i class="fa fa-info-circle fa-lg text-warning ml-auto" data-toggle="tooltip"
                                        data-placement="right" title=" Use * for getting all Groups in the search"></i> <br>
                                    {{ Form::text('text_word', request()->old('text_word'), ['class' => 'form-control', 'placeholder' => 'Search Group']) }}
                                </div>
                                <div class="col-md-6 form-group">
                                    {{ Form::submit('Search', ['class' => 'btn btn-primary mt-4']) }}
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                        <div class="card-body">
                            <div class="row overflow-auto">
                                <div class="col-sm-12">
                                    <div id="tabNew">
                                        <table id="example1"
                                            class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Group Name</th>
                                                    <th>Vehicle ID</th>
                                                    <th>Vehicle Name</th>
                                                    <th>Vehicles Count</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($groupList as $key => $value)
                                                    <tr>
                                                        <td>{{ ++$key }}</td>
                                                        <td>{{ $value }}</td>
                                                        <td>
                                                            @if (Arr::get($vehicleListArr, $value))
                                                                @foreach (Arr::get($vehicleListArr, $value) as $vehicleList)
                                                                    <span>{{ $vehicleList }}</span><br>
                                                                @endforeach
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (Arr::get($shortNameListArr, $value))
                                                                @foreach (Arr::get($shortNameListArr, $value) as $shortNameList)
                                                                    <span>{{ $shortNameList }}</span><br>
                                                                @endforeach
                                                            @endif
                                                        </td>
                                                        <td>{{ Arr::get($countArr, $value) }}</td>
                                                        <td>
                                                            <div class="dropdown dropleft">
                                                                <button
                                                                    class="btn btn-info text-white btn-sm dropdown-toggle"
                                                                    type="button"
                                                                    id="dropdownMenuButton{{ $key }}"
                                                                    data-toggle="dropdown" aria-haspopup="true"
                                                                    aria-expanded="false">
                                                                    Action
                                                                </button>
                                                                <div class="dropdown-menu"
                                                                    aria-labelledby="dropdownMenuButton{{ $key }}">
                                                                    <a class="btn  btn-info btn-sm text-white dropdown-item mb-1"
                                                                        href="{{ URL::to('vdmGroups/' . $value . '/edit') }}">Edit
                                                                        Group</a>
                                                                    <a class="btn  btn-warning btn-sm text-white dropdown-item mb-1"
                                                                        href="" id="auditReport"
                                                                        onClick="setGroup('{{ $value }}');">Audit
                                                                        Report</a>
                                                                    {{ Form::open(['url' => 'vdmGroups/' . $value, 'class' => '', 'if' => 'deleteForm' . $key]) }}
                                                                    {{ Form::hidden('_method', 'DELETE') }}
                                                                    {{ Form::button('Delete Group', ['id' => $key, 'class' => 'btn btn-danger text-white btn-sm text-white dropdown-item deleteBtn', 'formtype' => 'Group']) }}
                                                                    {{ Form::close() }}
                                                                </div>
                                                            </div>


                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
@push('js')
    <script>
        // $("#example1").dataTable();
        var encodedString = (Intl.DateTimeFormat().resolvedOptions().timeZone);
        var time1 = window.btoa(encodedString);

        function setGroup(group) {
            $('#auditReport').attr('href', '../auditReport/AuditGroup&' + time1 + '&' + group);
        }
    </script>
@endpush
