@extends("layouts.app")
@section('content')
    <div id="wrapper">
        <div class="content animate-panel page-inner">
            <div class="hpanel">
                <div class="pull-right">
                    <a href="{{ route('vdmGroup/sendExcel') }}"><img class="pull-right" width="40px" height="50px"
                            src="{{asset('assets/imgs/xls.svg')}}" method="get" alt="xls" /></a>
                </div>
                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.GroupList')
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.GroupList')
                            </a>
                        </li>
                    </ul>
                </div>
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}</p>
                @endif
                <div class="card">
                    <div class="card-body p-3 overflow-auto">
                        <table id="example1"
                            class="display table table-striped table-hover w-100 table-head-bg-primary dataTable no-footer">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">
                                    @lang('messages.ID')
                                    </th>
                                    <th style="text-align: center;">
                                    @lang('messages.GroupName')
                                    </th>
                                    <th style="text-align: center;">
                                    @lang('messages.VehicleId')
                                    </th>
                                    <th style="text-align: center;">
                                    @lang('messages.VehicleName')
                                    </th>
                                    <th style="text-align: center;">
                                    @lang('messages.VehicleCount')
                                    </th>
                                    <th style="text-align: center;">
                                    @lang('messages.Action')
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (isset($groupList))
                                    @foreach ($groupList as $key => $value)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $value }}</td>
                                            <td>
                                                @if (Arr::get($vehicleListArr, $value))
                                                    @foreach (Arr::get($vehicleListArr, $value) as $vehicleList)
                                                        <span>{{ $vehicleList }}</span><br>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>
                                                @if (Arr::get($shortNameListArr, $value))
                                                    @foreach (Arr::get($shortNameListArr, $value) as $shortNameList)
                                                        <span>{{ $shortNameList }}</span><br>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>{{ Arr::get($countArr, $value) }}</td>
                                            <td>
                                                <div class="dropdown dropleft">
                                                    <button class="btn btn-info btn-sm text-white dropdown-toggle"
                                                        type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                        @lang('messages.Action')
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        <a class="btn btn-sm btn-info dropdown-item text-white mb-1"
                                                            href="{{ URL::to('vdmGroups/' . $value . '/edit') }}">
                                                        @lang('messages.EditGroup')
                                                        </a>
                                                        <a class="btn btn-sm btn-success dropdown-item text-white mb-1"
                                                        href="{{route('audit',['type'=>'group','name'=>$value ])}}">
                                                        @lang('messages.AuditReport')
                                                        </a>
                                                        <button type="button" class="btn btn-sm btn-warning  dropdown-item text-white mb-1" onclick="uploadVehicle(this)" data-groupId="{{$value}}" data-groupName="{{Arr::first(explode(":",$value))}}">
                                                            @lang('messages.UploadVehicle')
                                                        </button>
                                                        {{ Form::open(['url' => 'vdmGroups/' . $value, 'class' => '', 'id' => "deleteForm$key"]) }}
                                                        {{ Form::hidden('_method', 'DELETE') }}
                                                        {{ Form::button(__('messages.DeleteGroup'), ['class' => 'btn btn-danger text-white btn-sm dropdown-item mb-1 deleteBtn', 'id' => $key, 'formtype' => 'Group']) }}
                                                        {{ Form::close() }}


                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('modal')
    <div class="modal fade" id="uploadVehiclesModal" tabindex="-1" role="dialog" aria-labelledby="uploadVehiceLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document" id="uploadVehiclesBody">
            <div class="modal-content">
                {{ Form::open(['url' => route('uploadVehicles'),'files' => true,'id'=>'updateVehicleForm']) }}
                    <div class="modal-header">
                        <h4 class="b">
                            @lang('messages.uploadVehicles')
                        </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <span class="text-danger error-message">
                            {{ HTML::ul($errors->all()) }}
                        </span>
                        <div class="row px-3">
                            <div class="form-group col-md-12">
                                <label for="groupId">
                                    @lang('messages.GroupId')
                                </label>
                                <input type="text" readonly class="form-control" id="modalGroupName" name="">
                                <input type="hidden" readonly class="form-control" name="groupId" id="groupId">
                            </div>
                            <div class="form-check col-md-12">
                                <label>
                                @lang('messages.ChooseMode')    
                                </label><br>
                                <label class="form-radio-label">
                                    <input class="form-radio-input" type="radio" name="vehicleData" value="vehicleId" @if(old('vehicleData') == "vehicleid") checked @elseif(old('vehicleData') != "deviceid") checked @endif>
                                    <span class="form-radio-sign">
                                        @lang('messages.VehicleId')
                                    </span>
                                </label>
                                <label class="form-radio-label ml-3">
                                    <input class="form-radio-input" type="radio" name="vehicleData" value="deviceId" @if(old('vehicleData') == "vehicleid") checked @endif>
                                    <span class="form-radio-sign">
                                        @lang('messages.DeviceId')
                                    </span>
                                </label>
                            </div>
                            <div class="col-md-12 p-3">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="Upload" name="import_file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" >
                                            <label class="custom-file-label" for="customFile" id="fileName1">
                                                @lang('messages.Selectafile')
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mt-2 mt-md-0">
                                        <a href="#" id="downloadUploadVehicle" class="btn btn-sm btn-warning"><i class="fa fa-download" aria-hidden="true"></i> Excel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-center">
                        {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary']) }}
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endpush
@push('js')

<script>
        @if(session()->has('updateVehicleModalGroupId'))
            $(function(){
                $('.splash').hide();
                $('#uploadVehiclesModal').modal('show')
                $("#modalGroupName").val("{{ Arr::first(explode(':',session('updateVehicleModalGroupId'))) }}")
                $('#groupId').val("{{session('updateVehicleModalGroupId')}}")
                setTimeout(function(){
                    // $('.error-message').fadeOut('slow')
                },10000)
            })
        @endif
        
        $('#Upload').on('change',function(){
            log = $(this).val();
            split = log.split('.');
			format = split[split.length - 1];
			type = ['xls','xlsx','csv']
			if(!type.includes(format)){
				alert('Please upload valid extension(.xls,.xlsx,.csv) file');
				$(this).val('');
			}else{
				if( input.length ) {
					input.val(log);
				} else {
					if( log ) alert(log);
				}
			}
        });
        function uploadVehicle(input){
            let groupId = $(input).attr('data-groupId');
            let groupName = $(input).attr('data-groupName');
            $('#uploadVehiclesModal').modal('show')
            $("#modalGroupName").val(groupName)
            $('#groupId').val(groupId)
        }
        $('#downloadUploadVehicle').on('click',function(){
            url ="{{route('downloadUploadVehicle')}}";
            $("#updateVehicleForm").attr('action',url).submit();
            url = "{{route('uploadVehicles')}}";
            $("#updateVehicleForm").attr('action',url)
        })
        function setGroup(group,aTag) {
            var encodedString = (Intl.DateTimeFormat().resolvedOptions().timeZone);
            var time1 = window.btoa(encodedString);
            $(aTag).attr('href', 'auditReport/AuditGroup&' + time1 + '&' + group);
        }
    </script>
@endpush
