@extends("layouts.app")
@section('content')

    <div id="wrapper">
        <div class="content page-inner">
            <div class="hpanel">
                @if (session()->has('message'))
                    <p class="alert {{ session()->get('alert-class', 'alert-success') }}">
                        {{ session()->get('message') }}</p>
                @endif
                <div class="page-header">
                    <h4 class="page-title">
                        @lang('messages.AddGroup')
                    </h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('vdmGroups.index') }}">
                                @lang('messages.Groups')
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                @lang('messages.AddGroup')
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="page-body">
                    <div class="card">
                        <div class="card-body">
                            <span class="text-danger error-message">
                                {{ HTML::ul($errors->all()) }}
                            </span>
                            {{ Form::open(['url' => 'vdmGroups', 'id' => 'formid']) }}
                            <div class="row p-2">
                                <div class="col-md-4 form-group">
                                    {{ Form::label('groupId', __('messages.GroupName')) }}
                                    <i class="fa fa-info-circle fa-lg text-warning ml-auto" data-toggle="tooltip"
                                        data-placement="right"
                                        title="Dealer name is case sensitive and space is not allowed"></i> <br>
                                    {{ Form::text('groupId', $groupName1, ['class' => 'form-control', 'placeholder' => 'Group Name', 'required' => 'required', 'id' => 'groupName', 'onkeyup' => 'caps(this)']) }}
                                    <span id="validation" class="text-danger b"></span>
                                </div>
                                <div class="col-md-3 form-group">
                                    {{ Form::submit(__('messages.Submit'), ['class' => 'btn btn-primary mt-0 mt-md-4', 'id' => 'checkGroup']) }}
                                </div>
                            </div>
                            <hr>
                            <div class="row p-2">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    {{ Form::checkbox('group', 'value', false, ['class' => 'form-check-input field check']) }}
                                                    <span class="form-check-sign">
                                                        @lang('messages.SelectallVehicles')
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <input type="text" placeholder="Search..." id="search" name="searchtext"
                                                    class="form-control searchkey">
                                                <div class="input-group-prepend">
                                                    <div class="btn btn-search btn-primary rounded-right" aria-label="search">
                                                        <i class="fa fa-search search-icon"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row p-2">
                                <div class="col-md-12">
                                    <div class="row border-bottom mb-2 form-check" id="selectedItems"></div>
                                </div>
                                <div class="col-md-12">
                                    {{ Form::label('vehicleList',  __('messages.SelecttheVehicles').':', ['class' => 'text-primary mb-3']) }}
                                </div>
                                <br>
                                <div class="col-md-12" id="unSelectedItems">
                                    <div class="row ">
                                        @if (isset($userVehicles))
                                            @foreach ($userVehicles as $key => $value)
                                                <div class="col-md-2 form-check vehiclelist">
                                                    <label class="form-check-label d-inline-block text-truncate w-100">
                                                        {{ Form::checkbox('vehicleList[]', $key . ' || ' . Arr::get($shortNameList, $value), null, ['class' => 'form-check-input field', 'id' => 'questionCheckBox']) }}
                                                        <span class="form-check-sign "
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="{{ Arr::get($shortNameList, $value) }}">
                                                            {{ Arr::get($shortNameList, $value) }}
                                                        </span>
                                                    </label>
                                                </div>

                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{ Form::close() }}

                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::hidden('value', json_encode($userVehicles, true), ['id' => 'value']) }}
    </div>

@endsection
@push('js')

    <script type="text/javascript">
        list = [];
        // var list ='{{ json_encode($userVehicles) }}'
        var value = $('#value').val();

        $('#groupName').keyup(function() {
            $('#validation').text('');
            $('#groupName').removeClass('is-invalid');
            var postValue = {
                'id': $(this).val(),
                "_token": '{{ csrf_token() }}'
            };
            $.post('{{ route('ajax.groupIdCheck') }}', postValue)
                .done(function(data) {
                    if (data == 'fail') {
                        $('#validation').text('Group Id already exist. Please use different Id');
                        $('#groupName').addClass('is-invalid');
                        return false
                    }
                    $('#groupName').removeClass('is-invalid');
                    console.log("Sucess-------->" + data);

                }).fail(function() {
                    console.log("fail");
                });


        });
    </script>
    @include('includes.js_footer')
    @include('includes.js_create')
@endpush
