<head>
</head>
<table>
  <tr>
    <th>No</th>
	<th>
		@lang('messages.UserName')
	</th>
    <th>
		@lang('messages.MobileNumber')
	</th>
	<th>
		@lang('messages.EmailId')
	</th>
	<th>
		@lang('messages.CCMails')
	</th>
	<th>
		@lang('messages.Zoho')
	</th>
    <th>
		@lang('messages.GroupsName')
	</th>
</tr>
<tbody>
@foreach($userList as $key => $value)
<tr>
    <td >{{++$key }}</td>
    <td >{{ $value }}</td> 
	<td>{{ Arr::get($userMobileArr, $value) }}</td>
	<td>{{ Arr::get($userEmailArr, $value) }}</td> 
	<td>{{ Arr::get($userCCMailsArr, $value) }}</td>
	<td>{{ Arr::get($userZohoArr, $value) }}</td> 
	<td>{{ Arr::get($userGroupsArr, $value) }}</td>
    
</tr>

@endforeach
</tbody>
</table>
