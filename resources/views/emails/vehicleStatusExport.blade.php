<head>
</head>
<table>
    <thead>
        <tr>
            @foreach ($header as $item)
                <th style="background-color:#087ae4;color:white;width:25px;">{{$item}}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($vehicleList as $key=>$vehicle)
        <tr>
            <td>{{$vehicle}}</td>
            <td>{{$shortNameList[$vehicle]}}</td>
            <td>{{$deviceList[$vehicle]}}</td>
            <td>{{$orgIdList[$vehicle]}}</td>
            <td>{{$regNoList[$vehicle]}}</td>
            <td>{{$groupList[$vehicle]}}</td>
            <td>{{$statusList[$vehicle]}}</td>
            <td>{{$speedList[$vehicle]}}</td>
            <td>{{$ignList[$vehicle]}}</td>
            <td>{{$commList[$vehicle]}}</td>
            <td>{{$locList[$vehicle]}}</td>
            <td>{{$addressList[$vehicle]}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
