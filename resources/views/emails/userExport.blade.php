<head>
</head>
<table>
    <tr>
        <th style="background-color:#1269db;color:white;width:5px;">No</th>
        <th style="background-color:#1269db;color:white;width:25px;">
            @lang('messages.UserName')
        </th>
        <th style="background-color:#1269db;color:white;width:25px;">
            @lang('messages.MobileNumber')
        </th>
        <th style="background-color:#1269db;color:white;width:35px;">
            @lang('messages.EmailId')
        </th>
        <th style="background-color:#1269db;color:white;width:25px;">
            @lang('messages.CCMails')
        </th>
        <th style="background-color:#1269db;color:white;width:25px;">
            @lang('messages.Zoho')
        </th>
        <th style="background-color:#1269db;color:white;width:25px;">
            @lang('messages.GroupsName')
        </th>
    </tr>
    <tbody>
        @foreach ($userList as $key => $value)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $value }}</td>
                <td>{{ Arr::get($userMobileArr, $value) }}</td>
                <td>{{ Arr::get($userEmailArr, $value) }}</td>
                <td>{{ Arr::get($userCCMailsArr, $value) }}</td>
                <td>{{ Arr::get($userZohoArr, $value) }}</td>
                <td>{{ Arr::get($userGroupsArr, $value) }}</td>

            </tr>

        @endforeach
    </tbody>
</table>
