<head>
</head>
<table>
    <thead>
        <tr>
            <th style="background-color:#880a99;color:white;width:25px;">
                @lang('messages.TagId')
            </th>
            <th style="background-color:#880a99;color:white;width:25px;">
                @lang('messages.TagName/EmployeeName')
            </th>
            <th style="background-color:#880a99;color:white;width:25px;">
                @lang('messages.Designation')
            </th>
            <th style="background-color:#880a99;color:white;width:25px;">
                @lang('messages.EmployeeId')
            </th>
            <th style="background-color:#880a99;color:white;width:25px;">
                @lang('messages.Mobilenumber')
            </th>
            <th style="background-color:#880a99;color:white;width:25px;">
                @lang('messages.Department')
            </th>
        </tr>
    </thead>
    <tbody>
        @for ($i = 1; $i <= $tags; $i++)
            <tr style="text-align: center;">
                <td>{{ Form::text('tagid' . $i, request()->old('tagid'), ['class' => 'form-control']) }}
                </td>
                <td>{{ Form::text('tagname' . $i, request()->old('tagname'), ['class' => 'form-control']) }}
                </td>
                <td>
                </td>
                <td>{{ Form::text('empid' . $i, request()->old('tagname'), ['class' => 'form-control']) }}
                </td>
                <td>{{ Form::number('mobile' . $i, request()->old('nobile'), ['class' => 'form-control']) }}
                </td>
                <td>{{ Form::text('department' . $i, request()->old('tagname'), ['class' => 'form-control']) }}
                </td>
            </tr>
        @endfor
    </tbody>
</table>
