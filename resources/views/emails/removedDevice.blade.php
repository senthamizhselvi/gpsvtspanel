<head>

    <style>
        table,
        td,
        th {
            border: 1px solid black;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            height: 30px;
        }

        .caption {
            background: #f2f2f2;
            padding: 5px;
            font-weight: bolder;
        }

    </style>
</head>
<h4>Hi, {{ $fcode }}!</h4>

<p>Your Removed devices details !!! </p>

<p>Franchise Name - {{ $fcode }}</p>
<p>Number of Devices Removed - {{ $nod }}</p>
<table>
    <tr>
        <td colspan="4" class="caption" align="center">Number of Licences Available - {{ $avail }}</td>
    </tr>
    <tr>
        <td colspan="2" class="caption" align="center">Vehicle Details</td>
        <td colspan="2" class="caption" align="center">Device Details</td>


    </tr>
    <tbody>

        @if (isset($Devices))
            @foreach ($Devices as $key => $value)
                <tr>
                    <td colspan="2" align="center">{{ $key }}</td>
                    <td colspan="2" align="center">{{ $value }}</td>
                </tr>
            @endforeach
        @endif


    </tbody>
</table>

<p>
    Thanks,
    <br>
    GPS Platform.
</p>
