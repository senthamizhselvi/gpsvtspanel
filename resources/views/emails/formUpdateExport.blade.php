<head>
</head>
<table>
    <tr>
        @foreach ($titles as $item)
            <th style="background-color:#1269db;color:white;width:25px;">{{ $item }}</th>
        @endforeach
    </tr>
    <tbody>
        @foreach ($data as $value)
            <tr>
                @foreach ($value as $item)
                    <td>{{ $item }}</td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
