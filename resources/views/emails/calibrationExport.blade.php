<head>
</head>
<table>
  <tr>
    <th>
      @lang('messages.Litre')
    </th>
    <th>
      @lang('messages.Volt')
    </th>
    <th>
      @lang('messages.Frequency')
    </th>
  </tr>
<tbody>
@foreach($litres as $key => $value)
<tr>
    <td >{{ $value }}</td>    
    <td>{{ Arr::get($volts, $key) }}</td>
    <td>{{Arr::get($frequencyData,$key)}}
@endforeach
</tbody>
</table>