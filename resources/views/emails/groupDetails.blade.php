<head>
</head>
<table>
  <tr>
    <th style="background-color:#1269db;color:white;width:5px;">No</th>
    <th style="background-color:#1269db;color:white;width:25px;">
    @lang('messages.GroupName')
    </th>
    <th style="background-color:#1269db;color:white;width:55px;">
    @lang('messages.VehicleId')
    </th>
    <th style="background-color:#1269db;color:white;width:55px;">
    @lang('messages.VehicleName')
    </th>
</tr>
<tbody>
@foreach($groupList as $key => $value)
<tr>
    <td >{{++$key }}</td>
    <td >{{ $value }}</td>    
    <td>{{ Arr::get($vehicleListArr, $value) }}</td>
    <td>{{ Arr::get($shortNameListArr, $value) }}</td>
    
</tr>

@endforeach
</tbody>
</table>
