<head>
</head>
<table>
    <tr>
        @if ($prepaid == 'yes')
            <th style="background-color:powderblue;width:25px;">
                @lang('messages.LicenceId')
            </th>
            <th style="background-color:powderblue;width:25px;">
                @lang('messages.Type')
            </th>
        @endif
        <th style="background-color:powderblue;width:25px;">
            @lang('messages.AssetId')
        </th>
        <th style="background-color:powderblue;width:25px;">
            @lang('messages.VehicleName')
        </th>
        <th style="background-color:powderblue;width:25px;">
            @lang('messages.DeviceId')
        </th>
        <th style="background-color:powderblue;width:25px;">
            @lang('messages.OrganisationId')
        </th>
        <th style="background-color:powderblue;width:25px;">
            @lang('messages.GPSSimNo')
        </th>
        <th style="background-color:powderblue;width:25px;">
            @lang('messages.Status')
        </th>
        <th style="background-color:powderblue;width:25px;">
            @lang('messages.DeviceModel')
        </th>
        <th style="background-color:powderblue;width:25px;">
            @lang('messages.LicenceIssuedDate')
        </th>
        <th style="background-color:powderblue;width:25px;">
            @lang('messages.OnboardedDate')
        </th>
        <th style="background-color:powderblue;width:25px;">
            @lang('messages.LicenceExpiryDate')
        </th>
        <th style="background-color:powderblue;width:25px;">
            @lang('messages.LastlocationTime')
        </th>
        <th style="background-color:powderblue;width:25px;">
            @lang('messages.LastComunicatoinTime')
        </th>

    </tr>
    <tbody>
        @foreach ($vehicleList as $key => $value)
            <tr>
                @if ($prepaid == 'yes')
                    <td>{{ Arr::get($licenceIdList, $value) }}</td>
                    <td>{{ Arr::get($typeList, $value) }}</td>
                @endif
                <td>{{ $value }}</td>
                <td>{{ Arr::get($shortNameList, $value) }}</td>
                <td>{{ Arr::get($deviceList, $value) }}</td>
                <td>{{ Arr::get($orgIdList, $value) }}</td>
                <td>{{ Arr::get($mobileNoList, $value) }}</td>
                @if (Arr::get($statusList, $value) == 'P')
                    <td style="color: #8e8e7b">
                        @lang('messages.Parking')
                    <td>
                    @elseif (Arr::get($statusList, $value) == 'M')
                    <td style="color: #00b374">
                        @lang('messages.Moving')
                    </td>
                @elseif(Arr::get($statusList, $value) == 'S')
                    <td style="color: #ff6500">
                        @lang('messages.Idle')
                    </td>
                @elseif(Arr::get($statusList, $value) == 'U')
                    <td style="color: #fe068d">
                        @lang('messages.NoData')
                    </td>
                @elseif(Arr::get($statusList, $value) == 'N')
                    <td style="color: #0a85ff">
                        @lang('messages.NewDevice')
                    </td>
                @endif
                <td>{{ Arr::get($deviceModelList, $value) }}</td>
                <td>{{ Arr::get($licenceList, $value) }}</td>
                <td>{{ Arr::get($onboardDateList, $value) }}</td>
                @if ($prepaid == 'yes')
                    <td>{{ Arr::get($LicexpiredPeriodList, $value) }}</td>
                @else
                    <td>{{ Arr::get($expiredList, $value) }}</td>
                @endif
                <td>{{ Arr::get($locList, $value) }}</td>
                <td>{{ Arr::get($commList, $value) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
