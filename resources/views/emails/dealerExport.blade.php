<head>
</head>
<table>
    <tr>
        <th style="background-color:#1269db;color:white;width:25px;">
            @lang('messages.DealerName')
        </th>
        <th style="background-color:#1269db;color:white;width:25px;">
            @lang('messages.MobileNumber')
        </th>
        <th style="background-color:#1269db;color:white;width:35px;">
            @lang('messages.WebSite')
        </th>
        <th style="background-color:#1269db;color:white;width:50px;">
            @lang('messages.Address/Location')
        </th>
        <th style="background-color:#1269db;color:white;width:35px;">
            @lang('messages.E-MailId')
        </th>
        <th style="background-color:#1269db;color:white;width:55px;">
            @lang('messages.MobileAppKey')
        </th>
        <th style="background-color:#1269db;color:white;width:55px;">
            @lang('messages.MapKey/APIKey')
        </th>
        <th style="background-color:#1269db;color:white;width:55px;">
            @lang('messages.AddressKey')
        </th>
        <th style="background-color:#1269db;color:white;width:55px;">
            @lang('messages.NotificationKey')
        </th>
    </tr>
    <tbody>
        @foreach ($dealerlist as $key => $value)
            <tr>
                <td>{{ $value }}</td>
                <td>{{ Arr::get($userGroupsArr, $value) }}</td>
                <td>{{ Arr::get($dealerWeb, $value) }}</td>
                <td>{{ Arr::get($address, $value) }}</td>
                <td>{{ Arr::get($mailid, $value) }}</td>
                <td>{{ Arr::get($mobilekey, $value) }}</td>
                <td>{{ Arr::get($mapkey, $value) }}</td>
                <td>{{ Arr::get($addressKey, $value) }}</td>
                <td>{{ Arr::get($notikey, $value) }}</td>
            </tr>

        @endforeach
    </tbody>
</table>
