<head>
</head>
<table>
    <tr>
        <th>
            @lang('messages.DealerName')
        </th>
        <th>
            @lang('messages.MobileNumber')
        </th>
        <th>
            @lang('messages.WebSite')
        </th>
        <th>
            @lang('messages.E-MailId')
        </th>
        <th>
            @lang('messages.MobileAppKey')
        </th>
        <th>
            @lang('messages.MapKey/APIKey')
        </th>
        <th>
            @lang('messages.AddressKey')
        </th>
        <th>
            @lang('messages.NotificationKey')
        </th>
    </tr>
    <tbody>
        @foreach ($dealerlist as $key => $value)
            <tr>
                <td>{{ $value }}</td>
                <td>{{ Arr::get($userGroupsArr, $value) }}</td>
                <td>{{ Arr::get($dealerWeb, $value) }}</td>
                <td>{{ Arr::get($mailid, $value) }}</td>
                <td>{{ Arr::get($mobilekey, $value) }}</td>
                <td>{{ Arr::get($mapkey, $value) }}</td>
                <td>{{ Arr::get($addressKey, $value) }}</td>
                <td>{{ Arr::get($notikey, $value) }}</td>
            </tr>

        @endforeach
    </tbody>
</table>
