<head>
</head>
<table>
    <tr>
        @foreach ($titles as $title)        
            <th style="background-color:#1179f0;color:white;width:20px;">{{$title}}</th>
        @endforeach
    </tr>
    <tbody>
        @foreach ($licenceList as $key => $list)
            <tr>
                @foreach ($list as $item)
                    <td>{{ $item }}</td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
