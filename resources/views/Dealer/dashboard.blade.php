@extends("layouts.app")
@section('content')
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">{{ $title }}</h4>
            <ul class="breadcrumbs">
			<li class="nav-home">
				<a href="#">
					<i class="flaticon-home"></i>
				</a>
			</li>
			<li class="separator">
				<i class="flaticon-right-arrow"></i>
			</li>
			<li class="nav-item">
				<a href="#">{{ $title }}</a>
			</li>
			<li class="separator">
				<i class="flaticon-right-arrow"></i>
			</li>
			<li class="nav-item">
				<a href="#">{{strtoupper($username)}}</a>
			</li>
		</ul>
        </div>
        <div class="row animated zoomIn">
            <div class="col-md-12">
                <div class="card">
                    <div class=" card-body ">
                        <div class="row">
                            <div class="col-12 animated zoomIn" style="animation-delay: 0.5s;">
                                {{-- <div style="margin-left: 2%;">
                                    <h1>{{ }}</h1>
                                    <p></p>
                                    <p>{{ $username }}</p>
                                </div> --}}
                                {{-- <hr style="border: 1px solid#3490dc; width: 95%; margin-left: 2%;"> --}}
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="card col mx-2 animated zoomIn" style="animation-delay: 0.5s;">
                                            <div class=" card-body text-center">
                                                <h5 class="card-title">
                                                    <label for="tnovo" class="text-primary">Total Vehicles OnBoard :</label>
                                                </h5>
                                                <label for="count">{{ $count }} </label>
                                            </div>
                                        </div>
                                        <div class="card col mx-2 animated zoomIn" style="animation-delay: 0.6s;">
                                            <div class=" card-body text-center">
                                                <h5 class="card-title">
                                                    <label for="tnovo" class="text-primary">Previous Month :</label>
                                                </h5>
                                                <label for="">{{ count($vechile) }} </label>
                                            </div>
                                        </div>
                                        <div class="card col mx-2 animated zoomIn" style="animation-delay: 0.7s;">
                                            <div class=" card-body text-center">
                                                <h5 class="card-title">
                                                    <label for="tnovo" class="text-primary">Present Month :</label>
                                                </h5>
                                                <label for="">{{ '0' }} </label>
                                            </div>
                                        </div>
                                        <div class="card col mx-2 animated zoomIn" style="animation-delay: 0.8s;">
                                            <div class=" card-body text-center">
                                                <h5 class="card-title">
                                                    <label for="tnovo" class="text-primary">Next Month :</label>
                                                </h5>
                                                <label for="">{{ '0' }} </label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-12 animated zoomIn" style="animation-delay: 1s;">
                                    <hr style="border: 1px solid#3490dc; width: 95%; margin-left: 2%;">
                                    <div class="row">
                                        <div class="col-6 pb-2"><label for="vehicles"
                                                class="text-primary font-weight-bold">Vehicles OnBoard with each Dealers :
                                            </label></div>
                                        <div class="col-12">
                                            <table id="example1" class="table table-striped">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th style="text-align: center;">DEALER ID</th>
                                                        <th style="text-align: center;">NUMBER OF VEHICLES</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (isset($dealerId))
                                                        @foreach ($dealerId as $key => $value)
                                                            <tr style="text-align: center; font-size: 11px;">
                                                                <td> {{ Form::label('li', $key) }}</td>
                                                                <td> {{ Form::label('l', $value . ' ' . ' ') }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $(document).ready(function() {
            $('#example1').DataTable();

            
        });

    </script>
@endpush
