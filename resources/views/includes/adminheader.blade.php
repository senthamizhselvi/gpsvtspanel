<!-- app/views/nerds/index.blade.php -->

<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Admin Panel</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <meta name="description" content="Admin panel of GPSVTS" />
    <link rel="icon" href="{{ asset('assets/imgs/SMP.png') }}" type="image/x-icon" />
    <!-- Fonts and icons -->
    <script src="{{ asset('common/js/plugin/webfont/webfont.min.js') }}"></script>
    <script src="{{ asset('common/js/core/jquery.3.2.1.min.js') }}"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">


    <script>
        WebFont.load({
            google: {
                "families": ["Lato:300,400,700,900"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular",
                    "Font Awesome 5 Brands", "simple-line-icons"
                ],
                urls: ['{{ asset('common/css/fonts.min.css') }}']
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('common/css/atlantis.min.css') }}">
    <link rel="stylesheet" href="{{ asset('common/css/demo.css') }}">




</head>

<body>

    <div class="container mw-100 p-0">
        <div>
            <nav class="navbar navbar-expand-lg bg-primary">
                <a class="navbar-brand" href="{{ URL::to('vdmFranchises') }}">
                    SUPER ADMIN PANEL 
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item {{ \Request::is('*vdmFranchises/fransearch*') ? 'active' : '' }}">
                            <a class="nav-link" herf="" data-toggle="modal" data-target="#myModel" id="switchLogin">Switch Login</a>
                        </li>
                        <li class="nav-item {{ \Request::is('*vdmFranchises') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ URL::to('vdmFranchises') }}">View All Franchises</a>
                        </li>
                        <li class="nav-item {{ \Request::is('*vdmFranchises/create*') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ URL::to('vdmFranchises/create') }}">Add Franchise</a>
                        </li>

                        <li class="nav-item {{ \Request::is('*audit/AuditFrans-*') ? 'active' : '' }}">
                            <a class="nav-link" href="{{route('Adminaudit',['type'=>'admin'])}}" id="auditfrans">Audit Trial - Franchises</a>
                        </li>
                        <li class="nav-item {{ \Request::is('*vdmFranchises/updatetimezone*') ? 'active' : '' }}">
                            <!--Timezone and apn updates -->
                            @php
                                $redis = app('redis')->connection();
                                $apnKey = $redis->exists('Update:Apn');
                                $timezoneKey = $redis->exists('Update:Timezone');
                            @endphp
                            @if ($timezoneKey != 'True')
                                <a class="nav-link" href="{{ URL::to('vdmFranchise/updatetimezone') }}"> Update
                                    Timezone </a> &nbsp;
                            @endif
                            @if ($apnKey != 'True')
                                <a class="nav-link" href="{{ URL::to('vdmFranchise/updateapn') }}"> Update APN
                                </a>
                            @endif
                        </li>
                        <li class="nav-item {{ \Request::is('*vdmFranchises/licConvert*') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('vdmFranchises/licConvert') }}">Licence Conversion </a>
                        </li>

                    </ul>
                    <a class="nav-link btn btn-sm ml-auto btn-danger text-white" href="{{ URL::to('logout/') }}">
                        <i class="fas fa-sign-out-alt"></i>
                        Logout
                    </a>

                </div>
            </nav>
        </div>
        <div class="page-inner p-4 pt-3">
            @if (session()->has('message'))
                <div class="alert alert-info mt-1">{{ session()->get('message') }}</div>
            @endif
            @if (session()->has('errormessage'))
                <div class="alert alert-danger mt-1">{{ session()->get('errormessage') }}</div>
            @endif


            @yield('mainContent')

            <footer class="row">
                @include('includes.footer')
            </footer>
        </div>
    </div>
    @include('vdm.franchise.frans')
    <!--   Core JS Files   -->
    <script src="{{ asset('common/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('common/js/core/bootstrap.min.js') }}"></script>
    <!-- Datatables -->
    <script src="{{ asset('common/js/plugin/datatables/datatables.min.js') }}"></script>
    <!-- Sweet Alert -->
    <script src="{{ asset('common/js/plugin/sweetalert/sweetalert.min.js') }}"></script>
    <!-- selecct2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}" />
    <script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
    <!-- datetimepicker JS -->
    {{-- <script src="{{ asset('common/js/bootstrap-datetimepicker.min.js') }}"></script> --}}
    <script src="{{ asset('common/js/DateTimePicker/datepicker.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('common/js/DateTimePicker/datepicker.min.css') }}">
    <script src="{{ asset('common/js/DateTimePicker/timepicker.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('common/js/DateTimePicker/timepicker.min.css') }}">
    <script src="{{ asset('common/js/script.js') }}"></script>
</body>
<script type='text/javascript'>
    var encodedString = (Intl.DateTimeFormat().resolvedOptions().timeZone);
    var time1 = window.btoa(encodedString);
    $('#auditfrans').on('click', function() {
        // $('#auditfrans').attr('href', 'audit/AuditFrans-' + time1);
    });
</script>

</html>
