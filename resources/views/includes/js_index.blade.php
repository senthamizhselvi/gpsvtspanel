<!-- Vendor scripts -->
{{-- <script src="vendor/jquery/dist/jquery.min.js"></script> --}}
{{-- <script src="vendor/jquery-ui/jquery-ui.min.js"></script> --}}
{{-- <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script> --}}
{{-- <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script> --}}
<script src="vendor/jquery-flot/jquery.flot.js"></script>
<script src="vendor/jquery-flot/jquery.flot.resize.js"></script>
<script src="vendor/jquery-flot/jquery.flot.pie.js"></script>
<script src="vendor/flot.curvedlines/curvedLines.js"></script>
<script src="vendor/jquery.flot.spline/index.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="assets/dropDown/bootstrap-select.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/peity/jquery.peity.min.js"></script>
<script src="vendor/sparkline/index.js"></script> 


{{-- <script src="plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script> --}}

<script src="scripts/homer.js"></script>
<script src="scripts/charts.js"></script>
