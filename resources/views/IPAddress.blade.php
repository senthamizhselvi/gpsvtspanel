@extends('includes.adminheader')
@section('mainContent')


    <div class="card">
        <div class="card-header">
            <h1>Manage IPAddress</h1>
        </div>
        <div class="card-body">

            <!-- if there are creation errors, they will show here -->
            {{ HTML::ul($errors->all()) }}

            {{ Form::open(['url' => 'ipAddressManager']) }}



            <div class="form-group">
                {{ Form::label('IPAddress', 'IPAddress') }}
                {{ Form::text('ipAddress', $ipAddress, ['class' => 'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('gt06nCount', 'GT06N Count') }}
                {{ Form::text('gt06nCount', $gt06nCount, ['class' => 'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('tr02Count', 'TR02 Count') }}
                {{ Form::text('tr02Count', $tr02Count, ['class' => 'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('gt03aCount', 'GT03A Count') }}
                {{ Form::text('gt03aCount', $gt03aCount, ['class' => 'form-control']) }}
            </div>
            <div class="row">
                {{ Form::submit('Update IPAddress!', ['class' => 'btn btn-primary mx-auto']) }}
            </div>

            {{ Form::close() }}
        </div>
    </div>



@stop
