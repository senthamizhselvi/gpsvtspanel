<?php

return [

	"private" => [
		"login"							=> "Login",
		"user_list"						=> "User List",
		"user_profile"					=> "User Profile",
		"user_post"						=> "User Post",
		"my_profile"					=> "My Profile",
		"adminuser_profile"				=> "Admin User Profile",
		"adminuser_list"				=> "Admin User List",
		"dashboard"						=> "Dashboard",
		"admin_type_list"				=> "Admin Type",
		"user_type_list"				=> "User Type",
		"hospital_list"					=> "Hospital List",
		"hospital_profile"				=> "Hospital Profile",
		"department_list"				=> "Department List",
		"doctor_list"					=> "Doctors List",
		"appointment_date"				=> "Appointment Date",
		"appointment_booking"			=> "Appointment Booking List",
		"review"						=> "Hospital Reviews",
		"hospital_slider"				=> "Hospital Slider"
	]

];
