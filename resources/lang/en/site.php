<?php

return [

	"invalid_login" 	=> "Invalid Username / Password",
	"invalid_password" 	=> "Current password is invalid",
	"blocked"			=> "Admin is Blocked or Unactive for user",
	"otp_emailed" 		=> "We have e-mailed OTP to your registered email address",
	"otp_send" 			=> "We have  send OTP to your registered phone",
	"check_email" 		=> "Check email address",
	"server_error" 		=> "Internal server error. Please try again later",
	"logout_success" 	=> "User logged out sucessfully",
	"no_data" 			=> "No :attr available",
	"clubRegister" 		=> "Club Register create sucessfully",

	"eventTitle" 		=> "New Event Created",
	"eventCancelled" 	=> " Event Cancelled",
	"eventActivated" 	=> " Event Activated",
	"eventMessage" 		=> "New Event Created",

];