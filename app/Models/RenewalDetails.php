<?php

namespace App\Models;
use Eloquent;
use Illuminate\Database\Eloquent\Model;

class RenewalDetails extends Model {
   

    /**
     * The database table used by the model.
     *
     * @var string
     */
    //protected $table = 'Audit_Frans';

     protected $table = 'Renewal_Details';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     *
    
    /**
     * Indicates primary key of the table
     *
     * @var bool
     */
    //public $primaryKey = 'id';
    
    /**
     * Indicates if the model should be timestamped
     * 
     * default value is 'true'
     * 
     * If set 'true' then created_at and updated_at columns 
     * will be automatically managed by Eloquent
     * 
     * If created_at and updated_at columns are not in your table
     * then set the value to 'false'
     *
     * @var bool
     */
    //public $timestamps = false;
    
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = array('ID','User_Name','Licence_Id','Vehicle_Id','Device_Id','Type','Processed_Date','Status');
    
    /**
     * The attributes that aren't mass assignable
     *
     * @var array
     */
    protected $guarded = array();

}
