<?php

namespace App\Models;
use Eloquent;
use Illuminate\Database\Eloquent\Model;

class AuditOrg extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'Audit_Org';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 *
    
    /**
     * Indicates primary key of the table
     *
     * @var bool
     */
    public $primaryKey = 'id';
    
    /**
     * Indicates if the model should be timestamped
     * 
     * default value is 'true'
     * 
     * If set 'true' then created_at and updated_at columns 
     * will be automatically managed by Eloquent
     * 
     * If created_at and updated_at columns are not in your table
     * then set the value to 'false'
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = array('fcode','organizationName','userName', 'status','description','email','address','mobile','startTime','endTime','atc','etc','mtc','parkingAlert','idleAlert','parkDuration','idleDuration','overspeedalert','sendGeoFenceSMS','radius','smsSender','sosAlert','live','smsProvider','providerUserName','providerPassword','geofense','safemove','deleteHistoryEod','harshBreak','dailySummary','dailyDieselSummary','fuelLevelBelow','fuelLevelBelowValue','noDataDuration','SchoolGeoFence','morningEntryStartTime','morningEntryEndTime','eveningEntryStartTime','eveningEntryEndTime','morningExitStartTime','morningExitEndTime','eveningExitStartTime','eveningExitEndTime','isRfid','PickupStartTime','PickupEndTime','DropStartTime','DropEndTime','escalationsms','emailAlerts','smsAlerts','created_at','userIpAddress');
    
    /**
     * The attributes that aren't mass assignable
     *
     * @var array
     */
    protected $guarded = array();

}
