<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App;
// use Route;

class adminauth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!auth()->check()) {
            return redirect()->to('login');
        }
        $username = auth()->id();
        if ($username == 'vamos') {
            return redirect()->to('honda9964');
        }

        app()->setlocale(session('phplang'));
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        session([
            'user'=> $username,
            'userIP' => request()->server('REMOTE_ADDR'),
            'fcode'=>$fcode
        ]);
        
       
        if (strpos($username, 'admin') !== false) {
            
            $franDetails_json = $redis->hget('H_Franchise', $fcode);
            $franchiseDetails = json_decode($franDetails_json, true);
            $prepaid = $franchiseDetails['prepaid'] ?? 'no';
            $isTimeZone = "false";
		    $timeZone = $franchiseDetails['timeZone'] ?? 'Asia/Kolkata';
			if($timeZone == 'Asia/Kolkata' &&($fcode === "SMP" || $fcode === "VAM")) {
                $isTimeZone = "true";
            }
            $enableStarter = 'no';
            $isAuthenticated = 'no';
            $cur1 = 'prePaidAdmin';
            $cur2 = "notKtrackAdmin";
            if ($prepaid == 'yes') {
                $enableStarter = $franchiseDetails['enableStarter'] ?? 'no';
                $isAuthenticated = $franchiseDetails['authenticated'] ?? 'no';
            } else {
                $cur1 = 'norPaidAdmin';
                if ($fcode == 'KTOT') {
                    $cur2 = 'ktrackAdmin';
                }
            }

            session([
                'cur'=> 'admin',
                'cur1'=> $cur1,
                'cur2'=>$cur2,
                'timeZone'=> $isTimeZone,
                'enableStarter'=> $enableStarter,
                'isAuthenticated'=> $isAuthenticated
            ]);

        } else {
            $val1 = $redis->sismember('S_Dealers_' . $fcode, $username);
            $val = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            if ($val1 !== 1 && !isset($val)) {
                return redirect()->to('logout'); //TODO should be replaced with aunthorized page - error
            }

            $franDetails_json = $redis->hget('H_Franchise', $fcode);
            $franchiseDetails = json_decode($franDetails_json, true);
            $dealDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
            $dealerDetails = json_decode($dealDetails_json, true);
            $timeZone = $dealerDetails['timeZone'] ?? 'Asia/Kolkata';
            $isTimeZone = "false";
            if($timeZone == 'Asia/Kolkata'  &&($fcode === "SMP" || $fcode === "VAM")) {
                $isTimeZone = "true";
            }
            
            $prepaid = $franchiseDetails['prepaid'] ?? 'no';
            $enableStarter = 'no';
            $isAuthenticated = 'no';
            $cur1 = 'prePaidAdmin';        
            if ($prepaid == 'yes') {
                $enableStarter = $dealerDetails['enableStarter'] ?? 'no';
                $isAuthenticated = $dealerDetails['authenticated'] ?? 'no';
            } else {
                $cur1 = "norPaidAdmin";
            }
            session([
                'cur'=> 'dealer',
                'cur1'=> $cur1,
                'timeZone'=> $isTimeZone,
                'enableStarter'=> $enableStarter,
                'isAuthenticated'=> $isAuthenticated
            ]);
            
        }
        return $next($request);
    }
}
