<?php

namespace App\Http\Controllers;

use App\Models\AuditDetails;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Schema;

class AuditNewController extends Controller
{
    public static function changeDataBase($db = null)
    {
        if (is_null($db)) $db = config()->get('constant.VAMOSdb');
        AuditTables::ChangeDB($db);
    }

    public static function updateAudit($mysqlDetails = null)
    {
        DB::listen(function ($query) {
            logger($query->sql);
            logger($query->time . " ms");
            logger("\n--------------------------------------------------------------------");
        });
        if ($mysqlDetails == null) return null;
        logger($mysqlDetails);
        try {
            $username = auth()->id();
            $redis = Redis::connection();
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $db = $fcode;
            $table = "Audit_Details";
            if ($username == "vamos") {
                $db = null;
            }
            self::changeDataBase($db);
            if (!Schema::hasTable($table)) {
                AuditTables::CreateAuditDetails();
            }
            AuditDetails::create($mysqlDetails);
            self::changeDataBase();
            return true;
        } catch (Exception $e) {
            self::changeDataBase();
            logger("-------------- updateAudit -------------------");
            logger($e->getMessage());
            return null;
        }
    }

    public function dropTable($type = null, $name = null)
    {
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        if ($fcode == 'vamos') $fcode = 'VAMOSYS';
        self::changeDataBase($fcode);
        $res = [];
        if ($type == null) {
            $table = "Audit_Details";
            $res =  Schema::dropIfExists($table);
        } else {
            $wheres = ['type' => $type];
            if ($name !== null) {
                $wheres['name'] = $name;
            }
            $res = AuditDetails::where($wheres)->delete();
        }
        self::changeDataBase();
        return $res;
    }

    public static function index($type, $name = null)
    {
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        try {
            // DB::listen(function ($query) {
            //     logger($query->sql);
            //     logger($query->time . " ms");
            //     logger("\n--------------------------------------------------------------------");
            // });
            if ($type == 'admin') {
                $fcode = null;
            }
            self::changeDataBase($fcode);
            // 
            $table = "Audit_Details";
            // Schema::dropIfExists($table);
            if (!Schema::hasTable($table)) {
                $data = [
                    'header' => [],
                    'auditDatas' => [],
                    'type' => $type
                ];
                return view('vdm.audit.indexNew', $data);
            }
            $header = Schema::getColumnListing($table);
            unset($header[0]);

            $wheres = ['type' => $type];
            if ($name !== null) {
                $wheres['name'] = $name;
            }
            if ($type == 'admin') {
                $wheres = [
                    'name' => $username,
                    'type' => 'admin',
                    'username' => $username
                ];
            } else {
                $wheres['username'] = $username;
            }
            if ($username == 'vamos') {
                $wheres = [
                    'type' => $type
                ];
            }
            // logger($wheres);
            // return $auditDatas = AuditDetails::where($wheres)->orderBy('created_at', 'DESC')->get($header)->toArray();
            $auditDatas = AuditDetails::where($wheres)->orderBy('created_at', 'DESC')->paginate(10, $header, 'pagination');
            self::changeDataBase();
            $data = [
                'header' => $header,
                'auditDatas' => $auditDatas,
                'type' => $type
            ];
            // $blade = "indexNew";
            if ($type == "user" || $type == "admin" || true) $blade = "user";
            if ($username == 'vamos') {
                return view('vdm.franchise.audit', $data);
            }
            return view('vdm.audit.' . $blade, $data);
        } catch (Exception $e) {
            info($e);
            return redirect()->route('Business.create')->with('message', 'No Audit Data(Error)');
        }
    }

    public function vehicleAuditAjax()
    {
        // do here
    }
}
