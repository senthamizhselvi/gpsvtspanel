<?php

namespace App\Http\Controllers;


use Exception;
use App\Models\User;
use App\Models\AuditDealer;
use Illuminate\Support\Arr;
use App\Exports\DealerExport;
use App\Models\AuditDetails;
use Illuminate\Support\Facades\Redis;
use Kreait\Firebase\Exception\FirebaseException;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Facades\Excel;
use Swift_TransportException;

class VdmDealersController extends Controller
{


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$dealerListKey = 'S_Dealers_' . $fcode;

		$dealerlist = $redis->smembers($dealerListKey);

		$userMobileArr = null;
		$dealerWeb = null;
		$enableArr = null;
		// sort($dealerlist);
		if (request()->server('HTTP_HOST') === "localhost") {
			$dealerlist = array_slice($dealerlist, 0, 30);
		}
		sort($dealerlist);
		foreach ($dealerlist as $key => $value) {
			$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $value);
			$detail = json_decode($detailJson, true);
			$userMobileArr = Arr::add($userMobileArr, $value, $detail['mobileNo']);
			$dealerWeb = Arr::add($dealerWeb, $value, $detail['website']);
			$disable = $redis->hget('H_UserId_Cust_Map', $value . ':lock');
			$enableArr = Arr::add($enableArr, $value, !isset($disable));
		}
		$indexData = [
			'fcode' => $fcode,
			'userMobileArr' => $userMobileArr,
			'dealerlist' => $dealerlist,
			'dealerWeb' => $dealerWeb,
			'enableArr' => $enableArr
		];

		return view('vdm.dealers.index', $indexData);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */



	public function create()
	{

		$vehicleGroups = null;

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		// $dealer = 'S_dealer_' . $fcode;
		// $user = $this->checkuser();
		$vehicleGroups = null;
		$availableDeatils = [];
		$timeZone = 'Asia/Kolkata';
		if (session()->get('cur1') == 'prePaidAdmin') {
			$franDetails_json = $redis->hget('H_Franchise', $fcode);
			$franchiseDetails = json_decode($franDetails_json, true);
			$availableStarterLicence = isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : '0';
			$availableStarterPlusLicence = isset($franchiseDetails['availableStarterPlusLicence']) ? $franchiseDetails['availableStarterPlusLicence'] : '0';
			$availableBasicLicence = isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';
			$availableAdvanceLicence = isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';
			$availablePremiumLicence = isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';
			$availablePremPlusLicence = isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0';

			$availableDeatils = [
				'availableStarterLicence' => $availableStarterLicence,
				'availableStarterPlusLicence' => $availableStarterPlusLicence,
				'availableBasicLicence' => $availableBasicLicence,
				'availableAdvanceLicence' => $availableAdvanceLicence,
				'availablePremiumLicence' => $availablePremiumLicence,
				'availablePremPlusLicence' => $availablePremPlusLicence
			];
			$timeZone =  $franchiseDetails['timeZone'] ?? 'Asia/Kolkata';
		}

		$timeList1 = VdmFranchiseController::timeZoneC();
		$timezoneList =  Arr::sort(Arr::add($timeList1, "", "-- Select Timezone --"));

		$defaultMapList = ['osm' => 'OSM', 'googleMap' => "Google"];
		$createDealerData = [
			'dealerName' => "",
			'vehicleGroups' => $vehicleGroups,
			'smsP' => VdmFranchiseController::smsP(),
			'timezoneList' => $timezoneList,
			'timeZone' => $timeZone,
			'defaultMapList' => $defaultMapList
		];
		$createDealerData = array_merge($createDealerData, $availableDeatils);

		return view('vdm.dealers.create', $createDealerData);
	}

	public function createNew($id)
	{


		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$vehicleGroups = null;
		$availableDeatils = [];
		$timeZone =  'Asia/Kolkata';
		if (session()->get('cur1') == 'prePaidAdmin') {
			$franDetails_json = $redis->hget('H_Franchise', $fcode);
			$franchiseDetails = json_decode($franDetails_json, true);
			$availableStarterLicence = isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : '0';
			$availableBasicLicence = isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';
			$availableAdvanceLicence = isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';
			$availablePremiumLicence = isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';
			$availablePremPlusLicence = isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0';

			$availableDeatils = [
				'availableStarterLicence' => $availableStarterLicence,
				'availableBasicLicence' => $availableBasicLicence,
				'availableAdvanceLicence' => $availableAdvanceLicence,
				'availablePremiumLicence' => $availablePremiumLicence,
				'availablePremPlusLicence' => $availablePremPlusLicence
			];
			$timeZone =  $franchiseDetails['timeZone'] ?? 'Asia/Kolkata';
		}
		$timeList1 = VdmFranchiseController::timeZoneC();
		$timezoneList =  Arr::sort(Arr::add($timeList1, "", '-- Select Timezone --'));

		$createDealerData = [
			'dealerName' => $id,
			'vehicleGroups' => $vehicleGroups,
			'smsP' => VdmFranchiseController::smsP(),
			'timezoneList' => $timezoneList,
			'timeZone' => $timeZone
		];
		$createDealerData = array_merge($createDealerData, $availableDeatils);

		return view('vdm.dealers.create', $createDealerData);
	}

	public function checkuser()
	{

		$username = auth()->id();
		$uri = Route::current()->getName();
		if (strpos($username, 'admin') !== false || $uri == 'vdmVehicles.edit') {
			//do nothing
			return 'admin';
		} else {
			$redis = Redis::connection();
			$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
			$val1 = $redis->sismember('S_Dealers_' . $fcode, $username);
			if ($val1 == 1 || isset($fcode)) {
				return 'dealer';
			} else {
				return null; //TODO should be replaced with aunthorized page - error
			}
		}
	}
	/**
	 * Store a newly created resource in storage.
	 * TODO validations should be improved to prevent any attacks
	 * 
	 * @return Response
	 */


	/**
	 * Display the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function show($id)
	{

		$username = auth()->id();
		$redis = Redis::connection();

		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $id);
		$detail = json_decode($detailJson, true);

		$userId = $id;
		if (session()->get('cur1') == 'prePaidAdmin') {
			$detail['numofStarter'] = isset($detail['numofStarter']) ? $detail['numofStarter'] : '0';
			$detail['numofStarterPlus'] = isset($detail['numofStarterPlus']) ? $detail['numofStarterPlus'] : '0';
			$detail['numofBasic'] = isset($detail['numofBasic']) ? $detail['numofBasic'] : '0';
			$detail['numofAdvance'] = isset($detail['numofAdvance']) ? $detail['numofAdvance'] : '0';
			$detail['numofPremium'] = isset($detail['numofPremium']) ? $detail['numofPremium'] : '0';
			$detail['numofPremiumPlus'] = isset($detail['numofPremiumPlus']) ? $detail['numofPremiumPlus'] : '0';
			$detail['avlofStarter'] = isset($detail['avlofStarter']) ? $detail['avlofStarter'] : '0';
			$detail['avlofStarterPlus'] = isset($detail['avlofStarterPlus']) ? $detail['avlofStarterPlus'] : '0';
			$detail['avlofBasic'] = isset($detail['avlofBasic']) ? $detail['avlofBasic'] : '0';
			$detail['avlofAdvance'] = isset($detail['avlofAdvance']) ? $detail['avlofAdvance'] : '0';
			$detail['avlofPremium'] = isset($detail['avlofPremium']) ? $detail['avlofPremium'] : '0';
			$detail['avlofPremiumPlus'] = isset($detail['avlofPremiumPlus']) ? $detail['avlofPremiumPlus'] : '0';
		}
		$detail['enableStarter'] = isset($detail['enableStarter']) ? $detail['enableStarter'] : 'no';
		$detail['authenticated'] = isset($detail['authenticated']) ? $detail['authenticated'] : 'no';

		$detail['timeZone'] = isset($detail['timeZone']) ? $detail['timeZone'] : '';

		$fransRef = $redis->hget('H_Franchise', $fcode);
		$franRefDatajson = json_decode($fransRef, true);
		if ($detail['timeZone'] == null || $detail['timeZone'] == '' || empty($detail['timeZone'])) {
			$detail['timeZone'] = isset($franRefDatajson['timeZone']) ? $franRefDatajson['timeZone'] : 'Asia/Kolkata';
		}

		$detail = InputController::nullCheckArr($detail);
		$detail['website'] = isset($detail['website']) ? $detail['website'] : "";
		$detail['website1'] = isset($detail['website1']) ? $detail['website1'] : "";
		$detail['smsSender'] = isset($detail['smsSender']) ? $detail['smsSender'] : "";
		$detail['smsProvider'] = isset($detail['smsProvider']) ? $detail['smsProvider'] : "";
		$detail['providerUserName'] = isset($detail['providerUserName']) ? $detail['providerUserName'] : "";
		$detail['providerPassword'] = isset($detail['providerPassword']) ? $detail['providerPassword'] : "";
		$detail['address'] = isset($detail['address']) ? $detail['address'] : "";
		$detail['subDomain'] = isset($detail['subDomain']) ? $detail['subDomain'] : "";
		$detail['timeZone'] = isset($detail['timeZone']) ? $detail['timeZone'] : "";
		$detail['textLocalSmsKey'] = isset($detail['textLocalSmsKey']) ? $detail['textLocalSmsKey'] : "";


		$path = $_SERVER['REQUEST_URI'];
		$pathValue =  explode("/", $path);
		$imgLoc = '/' . $pathValue[1] . '/public/uploads/';

		$imageName = $this->utilMethod($detail['website']);
		$website = $detail['website'];

		$imgSmall = config()->get('constant.endpoint') . '/' . config()->get('constant.bucket') . '/picture/' . $imageName . '.small.png';
		$imgMob = config()->get('constant.endpoint') . '/' . config()->get('constant.bucket') . '/picture/' . $imageName . '.png';
		$imgLogo = config()->get('constant.endpoint') . '/' . config()->get('constant.bucket') . '/picture/' . $userId . '.png';

		$fransRef = $redis->hget('H_Franchise', $fcode);
		$franRefDatajson = json_decode($fransRef, true);
		$adminwebsiteArry = array($franRefDatajson['website'], $franRefDatajson['website2'], $franRefDatajson['website3'], $franRefDatajson['website4']);
		$adminwebsiteArry1 = array('www.' . $franRefDatajson['website'], 'www.' . $franRefDatajson['website2'], 'www.' . $franRefDatajson['website3'], 'www.s' . $franRefDatajson['website4']);
		$uploads = 'yes';
		if (in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower('www' . ($website)), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry1))) {
			$uploads = 'no';
		}
		$showDealerData = [
			'userId' => $userId,
			'imgSmall' => $imgSmall,
			'imgMob' => $imgMob,
			'imgLogo' => $imgLogo,
			'uploads' => $uploads
		];

		return view('vdm.dealers.show', $showDealerData, $detail);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */

	public function editDealer($id)
	{
		$userId = session()->get('user');
		return self::edit($userId);
	}

	public static function edit($id)
	{
		$userId = $id;
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $id);
		$detail = json_decode($detailJson, true);

		$dealerid = $id;
		$numofStarter = null;
		$numofBasic = null;
		$numofStarterPlus = null;
		$avlofStarterPlus = null;
		$numofAdvance = null;
		$numofPremium = null;
		$numofPremiumPlus = null;
		$avlofStarter = null;
		$avlofBasic = null;
		$avlofAdvance = null;
		$avlofPremium = null;
		$avlofPremiumPlus = null;
		if (session()->get('cur1') == 'prePaidAdmin') {
			$numofStarter = isset($detail['numofStarter']) ? $detail['numofStarter'] : '0';
			$numofStarterPlus = isset($detail['numofStarterPlus']) ? $detail['numofStarterPlus'] : '0';
			$numofBasic = isset($detail['numofBasic']) ? $detail['numofBasic'] : '0';
			$numofAdvance = isset($detail['numofAdvance']) ? $detail['numofAdvance'] : '0';
			$numofPremium = isset($detail['numofPremium']) ? $detail['numofPremium'] : '0';
			$numofPremiumPlus = isset($detail['numofPremiumPlus']) ? $detail['numofPremiumPlus'] : '0';
			$avlofStarter = isset($detail['avlofStarter']) ? $detail['avlofStarter'] : '0';
			$avlofStarterPlus = isset($detail['avlofStarterPlus']) ? $detail['avlofStarterPlus'] : '0';
			$avlofBasic = isset($detail['avlofBasic']) ? $detail['avlofBasic'] : '0';
			$avlofAdvance = isset($detail['avlofAdvance']) ? $detail['avlofAdvance'] : '0';
			$avlofPremium = isset($detail['avlofPremium']) ? $detail['avlofPremium'] : '0';
			$avlofPremiumPlus = isset($detail['avlofPremiumPlus']) ? $detail['avlofPremiumPlus'] : '0';
		}

		$mobileNo = isset($detail['mobileNo']) ? $detail['mobileNo'] : '';
		$email = isset($detail['email']) ? $detail['email'] : '';
		$website = isset($detail['website']) ? $detail['website'] : '';
		$website1 = isset($detail['website1']) ? $detail['website1'] : '';
		$smsSender = isset($detail['smsSender']) ? $detail['smsSender'] : '';
		$zoho = isset($detail['zoho']) ? $detail['zoho'] : '';
		$mapKey = isset($detail['mapKey']) ? $detail['mapKey'] : '';
		$mapKey1 = isset($detail['mapKey1']) ? $detail['mapKey1'] : '';
		$addressKey = isset($detail['addressKey']) ? $detail['addressKey'] : '';

		$notificationKey = $redis->hget('H_Google_Android_ServerKey', $fcode . ':' . $dealerid);
		$notificationKey = isset($detail['notificationKey']) ? $detail['notificationKey'] : $notificationKey;

		$smsProvider = isset($detail['smsProvider']) ? $detail['smsProvider'] : 'nill';
		$providerUserName = isset($detail['providerUserName']) ? $detail['providerUserName'] : '';
		$providerPassword = isset($detail['providerPassword']) ? $detail['providerPassword'] : '';
		$gpsvtsAppKey = isset($detail['gpsvtsApp']) ? $detail['gpsvtsApp'] : '';
		$subDomain = isset($detail['subDomain']) ? $detail['subDomain'] : '';
		$address = isset($detail['address']) ? $detail['address'] : '';
		$timeZone = isset($detail['timeZone']) ? $detail['timeZone'] : '';
		$languageCode = isset($detail['languageCode']) ? $detail['languageCode'] : 'en';
		$textLocalSmsKey = isset($detail['textLocalSmsKey']) ? $detail['textLocalSmsKey'] : '';
		$smsEntity = isset($detail['smsEntity']) ? $detail['smsEntity'] : '';
		$enableStarter = isset($detail['enableStarter']) ? $detail['enableStarter'] : 'no';
		$authenticated = isset($detail['authenticated']) ? $detail['authenticated'] : 'no';
		$city = isset($detail['city']) ? $detail['city'] : '';
		$state = isset($detail['state']) ? $detail['state'] : '';
		$defaultMap = isset($detail['defaultMap']) ? $detail['defaultMap'] : 'osm';

		$show = 'no';
		$fransRef = $redis->hget('H_Franchise', $fcode);
		$franRefDatajson = json_decode($fransRef, true);
		$franDefultMap = isset($franRefDatajson['defaultMap']) ? $franRefDatajson['defaultMap'] : 'osm';
		$defaultMap = isset($detail['defaultMap']) ? $detail['defaultMap'] : $franDefultMap;

		$authenticatedEnable = isset($franRefDatajson['authenticated']) ? $franRefDatajson['authenticated'] : 'no';
		$adminwebsiteArry = array($franRefDatajson['website'], $franRefDatajson['website2'], $franRefDatajson['website3'], $franRefDatajson['website4']);
		$adminwebsiteArry1 = array('www.' . $franRefDatajson['website'], 'www.' . $franRefDatajson['website2'], 'www.' . $franRefDatajson['website3'], 'www.s' . $franRefDatajson['website4']);
		if (in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower('www' . ($website)), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry1))) {
			$uploads = 'no';
		} else {
			$uploads = 'yes';
		}
		$timeList1 = VdmFranchiseController::timeZoneC();
		$timezoneList =  Arr::sort(Arr::add($timeList1, "", '-- Select Timezone --'));

		if ($timeZone == null || $timeZone == '' || empty($timeZone)) {
			$timeZone = isset($franRefDatajson['timeZone']) ? $franRefDatajson['timeZone'] : 'Asia/Kolkata';
		}
		$defaultMapList = ['osm' => 'OSM', 'googleMap' => "Google"];
		$editData = [
			'dealerid' => $dealerid,
			'textLocalSmsKey' => $textLocalSmsKey,
			'mobileNo' => $mobileNo,
			'email' => $email,
			'zoho' => $zoho,
			'gpsvtsAppKey' => $gpsvtsAppKey,
			'languageCode' => $languageCode,
			'mapKey' => $mapKey,
			'addressKey' => $addressKey,
			'notificationKey' => $notificationKey,
			'website' => $website,
			'smsSender' => $smsSender,
			'smsProvider' => $smsProvider,
			'providerUserName' => $providerUserName,
			'providerPassword' => $providerPassword,
			'smsP' => VdmFranchiseController::smsP(),
			'numofStarter' => $numofStarter,
			'numofStarterPlus' => $numofStarterPlus,
			'numofBasic' => $numofBasic,
			'numofAdvance' => $numofAdvance,
			'numofPremium' => $numofPremium,
			'avlofStarter' => $avlofStarter,
			'avlofStarterPlus' => $avlofStarterPlus,
			'avlofBasic' => $avlofBasic,
			'avlofAdvance' => $avlofAdvance,
			'avlofPremium' => $avlofPremium,
			'avlofPremiumPlus' => $avlofPremiumPlus,
			'numofPremiumPlus' => $numofPremiumPlus,
			'show' => $show,
			'subDomain' => $subDomain,
			'uploads' => $uploads,
			'website1' => $website1,
			'mapKey1' => $mapKey1,
			'address' => $address,
			'countryTimezoneList' => $timezoneList,
			'timeZone' => $timeZone,
			'smsEntity' => $smsEntity,
			'enableStarter' => $enableStarter,
			'authenticated' => $authenticated,
			'authenticatedEnable' => $authenticatedEnable,
			'city' => $city,
			'state' => $state,
			'defaultMap' => $defaultMap,
			'defaultMapList' => $defaultMapList
		];
		return view('vdm.dealers.edit', $editData);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function update($id)
	{

		$dealerid = $id;
		$username = auth()->id();
		$redis = Redis::connection();
		$s3 = app('aws')->createClient('s3', [
			'endpoint' => config()->get('constant.endpoint'),
		]);
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$rules = array(
			'mobileNo' => 'required',
			'email' => 'required',
			'website' => 'required',
			'city' => 'required',
			'state' => 'required',
			'address' => 'required',
			'zoho' => 'required',
		);
		$validator = validator()->make(request()->all(), $rules);
		if ($validator->fails()) {
			session()->flash('message', 'Update failed. Please check logs for more details !');
			return redirect()->back()->withInput()->withErrors($validator);
		} else {
			// store
			$path = $_SERVER['REQUEST_URI'];
			$pathValue =  explode("/", $path);

			$oldDealerList  = $redis->hget('H_DealerDetails_' . $fcode, $dealerid);
			$detail = json_decode($oldDealerList, true);
			$oldDetail = $detail;

			$numofStarter = 0;
			$numofBasic = 0;
			$numofAdvance = 0;
			$numofPremium = 0;
			$numofPremiumPlus = 0;
			$avlofStarter = 0;
			$avlofBasic = 0;
			$avlofAdvance = 0;
			$avlofPremium = 0;
			$avlofPremiumPlus = 0;
			$addofStarter = 0;
			$addofBasic = 0;
			$addofAdvance = 0;
			$addofPremium = 0;
			$addofPremiumPlus = 0;
			// get a data from form
			$mobileNo = request()->get('mobileNo');
			$email = request()->get('email');
			$zoho  = request()->get('zoho');
			$mapKey = request()->get('mapKey');
			$mapKey1 = request()->get('mapKey1');
			$addressKey = request()->get('addressKey');
			$notificationKey = request()->get('notificationKey');
			$gpsvtsAppKey = request()->get('gpsvtsAppKey');
			$website = request()->get('website');
			$website1 = request()->get('website1');
			$smsSender = request()->get('smsSender');
			$smsProvider = request()->get('smsProvider');
			$providerUserName = request()->get('providerUserName');
			$providerPassword = request()->get('providerPassword');
			$subDomain = request()->get('subDomain');
			$address = request()->get('address');
			$timeZone = request()->get('timeZone');
			$languageCode = request()->get('languageCode');
			$city = request()->get('city');
			$state = request()->get('state');

			$textLocalSmsKey = !is_null(request()->get('textLocalSmsKey')) ? request()->get('textLocalSmsKey') : '';
			$smsEntity = !is_null(request()->get('smsEntity')) ? request()->get('smsEntity') : '';
			$enableStarter = request()->get('enableStarter');
			$authenticated = request()->get('authenticated');

			$show = request()->get('show');
			if ($website == $website1) {
				return redirect()->back()->withInput()->withErrors('Websites name should not be same.');
			}

			$fransRef = $redis->hget('H_Franchise', $fcode);
			$franRefDatajson = json_decode($fransRef, true);
			$adminwebsiteArry = array($franRefDatajson['website'], $franRefDatajson['website2'], $franRefDatajson['website3'], $franRefDatajson['website4']);
			$adminwebsiteArry1 = array('www.' . $franRefDatajson['website'], 'www.' . $franRefDatajson['website2'], 'www.' . $franRefDatajson['website3'], 'www.s' . $franRefDatajson['website4']);
			if (in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower('www' . ($website)), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry1))) {
				$uploads = 'no';
			} else {
				$uploads = 'yes';
			}
			//prepaid franchise
			if (session()->get('cur1') == 'prePaidAdmin') {
				$numofStarter1 = isset($detail['numofStarter']) ? $detail['numofStarter'] : '0';
				$numofStarterPlus1 = isset($detail['numofStarterPlus']) ? $detail['numofStarterPlus'] : '0';
				$numofBasic1 = isset($detail['numofBasic']) ? $detail['numofBasic'] : '0';
				$numofAdvance1 = isset($detail['numofAdvance']) ? $detail['numofAdvance'] : '0';
				$numofPremium1 = isset($detail['numofPremium']) ? $detail['numofPremium'] : '0';
				$numofPremiumPlus1 = isset($detail['numofPremiumPlus']) ? $detail['numofPremiumPlus'] : '0';
				$avlofStarter1 = isset($detail['avlofStarter']) ? $detail['avlofStarter'] : '0';
				$avlofStarterPlus1 = isset($detail['avlofStarterPlus']) ? $detail['avlofStarterPlus'] : '0';
				$avlofBasic1 = isset($detail['avlofBasic']) ? $detail['avlofBasic'] : '0';
				$avlofAdvance1 = isset($detail['avlofAdvance']) ? $detail['avlofAdvance'] : '0';
				$avlofPremium1 = isset($detail['avlofPremium']) ? $detail['avlofPremium'] : '0';
				$avlofPremiumPlus1 = isset($detail['avlofPremiumPlus']) ? $detail['avlofPremiumPlus'] : '0';
				//input from franchise
				$addofStarter = request()->get('addofStarter', '0');
				$addofStarterPlus = request()->get('addofStarterPlus', '0');
				$addofBasic = request()->get('addofBasic', '0');
				$addofAdvance = request()->get('addofAdvance', '0');
				$addofPremium = request()->get('addofPremium', '0');
				$addofPremiumPlus = request()->get('addofPremiumPlus', '0');

				if ($addofStarterPlus < 0 || $addofStarter < 0 || $addofBasic < 0 || $addofAdvance < 0 || $addofPremium < 0 || $addofPremiumPlus < 0) {
					if (($avlofStarterPlus1 + $addofStarterPlus) < 0  || ($avlofStarter1 + $addofStarter) < 0 || ($avlofBasic1 + $addofBasic) < 0 || ($avlofAdvance1 + $addofAdvance) < 0 || ($avlofPremium1 + $addofPremium) < 0 || ($avlofPremiumPlus1 + $addofPremiumPlus) < 0) {
						return redirect()->back()->withErrors('Number of entered licences is exceeded the available licences.')->withInput();
					}
				}

				$franDetails_json = $redis->hget('H_Franchise', $fcode);
				$franchiseDetails = json_decode($franDetails_json, true);
				$availableStarterLicence	= isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : '0';
				$availableStarterPlusLicence = isset($franchiseDetails['availableStarterPlusLicence']) ? $franchiseDetails['availableStarterPlusLicence'] : '0';
				$availableBasicLicence		= isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';
				$availableAdvanceLicence	= isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';
				$availablePremiumLicence	= isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';
				$availablePremPlusLicence	= isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0';

				if ($availableStarterPlusLicence < $addofStarterPlus || $availableStarterLicence < $addofStarter || $availableBasicLicence < $addofBasic || $availableAdvanceLicence < $addofAdvance || $availablePremiumLicence < $addofPremium || $availablePremPlusLicence < $addofPremiumPlus) {
					return redirect()->back()->withErrors('Number of entered licences is exceeded the available licences.');
				}

				//count add
				$numofStarter = $numofStarter1 + $addofStarter;
				$numofStarterPlus = $numofStarterPlus1 + $addofStarterPlus;
				$numofBasic = $numofBasic1 + $addofBasic;
				$numofAdvance = $numofAdvance1 + $addofAdvance;
				$numofPremium = $numofPremium1 + $addofPremium;
				$numofPremiumPlus = $numofPremiumPlus1 + $addofPremiumPlus;
				$avlofStarter = $avlofStarter1 + $addofStarter;
				$avlofStarterPlus = $avlofStarterPlus1 + $addofStarterPlus;
				$avlofBasic = $avlofBasic1 + $addofBasic;
				$avlofAdvance = $avlofAdvance1 + $addofAdvance;
				$avlofPremium = $avlofPremium1 + $addofPremium;
				$avlofPremiumPlus = $avlofPremiumPlus1 + $addofPremiumPlus;
				$LicenceissuedDate = date('d-m-Y');
				$detail = array(
					'email'	=> $email,
					'mobileNo' => $mobileNo,
					'zoho'   => $zoho,
					'address' => $address,
					'city' => $city,
					'state' => $state,
					'mapKey' => $mapKey,
					'mapKey1' => $mapKey1,
					'addressKey' => $addressKey,
					'notificationKey' => $notificationKey,
					'gpsvtsApp' => $gpsvtsAppKey,
					'website' => $website,
					'website1' => $website1,
					'subDomain' => $subDomain,
					'smsSender' => $smsSender,
					'smsProvider' => $smsProvider,
					'providerUserName' => $providerUserName,
					'providerPassword' => $providerPassword,
					'numofStarter'	=> $numofStarter,
					'numofStarterPlus'	=> $numofStarterPlus,
					'numofBasic'	=> $numofBasic,
					'numofAdvance'	=> $numofAdvance,
					'numofPremium'	=> $numofPremium,
					'numofPremiumPlus'	=> $numofPremiumPlus,
					'avlofStarter'	=> $avlofStarter,
					'avlofStarterPlus'	=> $avlofStarterPlus,
					'avlofBasic'	=> $avlofBasic,
					'avlofAdvance'	=> $avlofAdvance,
					'avlofPremium'	=> $avlofPremium,
					'avlofPremiumPlus'	=> $avlofPremiumPlus,
					'LicenceissuedDate' => $LicenceissuedDate,
					'timeZone' => $timeZone,
					'languageCode' => $languageCode,
					'textLocalSmsKey' => $textLocalSmsKey,
					'smsEntity' => $smsEntity,
					'enableStarter' => $enableStarter,
					'authenticated' => $authenticated,
				);
				$franchiseDetails['availableStarterLicence'] = $franchiseDetails['availableStarterLicence'] - $addofStarter;
				$franchiseDetails['availableStarterPlusLicence'] = (isset($franchiseDetails['availableStarterPlusLicence']) ? $franchiseDetails['availableStarterPlusLicence'] : 0) - $addofStarterPlus;
				$franchiseDetails['availableBasicLicence'] = $franchiseDetails['availableBasicLicence'] - $addofBasic;
				$franchiseDetails['availableAdvanceLicence'] = $franchiseDetails['availableAdvanceLicence'] - $addofAdvance;
				$franchiseDetails['availablePremiumLicence'] = $franchiseDetails['availablePremiumLicence'] - $addofPremium;
				if (isset($franchiseDetails['availablePremPlusLicence']) == 1) {
					$franchiseDetails['availablePremPlusLicence'] = $franchiseDetails['availablePremPlusLicence'] - $addofPremiumPlus;
				}

				$detailsJson = json_encode($franchiseDetails);
				$redis->hmset('H_Franchise', $fcode, $detailsJson);
			} else {
				$detail = array(
					'email'	=> $email,
					'mobileNo' => $mobileNo,
					'zoho'   => $zoho,
					'address' => $address,
					'city' => $city,
					'state' => $state,
					'mapKey' => $mapKey,
					'mapKey1' => $mapKey1,
					'addressKey' => $addressKey,
					'notificationKey' => $notificationKey,
					'gpsvtsApp' => $gpsvtsAppKey,
					'website' => $website,
					'website1' => $website1,
					'subDomain' => $subDomain,
					'smsSender' => $smsSender,
					'smsProvider' => $smsProvider,
					'providerUserName' => $providerUserName,
					'providerPassword' => $providerPassword,
					'timeZone' => $timeZone,
					'languageCode' => $languageCode,
					'textLocalSmsKey' => $textLocalSmsKey,
					'smsEntity' => $smsEntity,
				);
			}

			$detail = InputController::nullCheckArr($detail);
			$detailJson = json_encode($detail);
			$redis->hset('H_DealerDetails_' . $fcode, $dealerid, $detailJson);
			// getlatlong..with address
			$geolocation = VdmFranchiseController::geocode($address);
			if ($geolocation != null) {
				$LatLong = implode(":", $geolocation);
			} else {
				$LatLong = '13.0231406:80.2087368';
			}
			$redis->hset('H_Franchise_LatLong', $fcode . ":" . $dealerid, $LatLong);
			try {
				
				$oldData = json_encode(array_diff($oldDetail, $detail));
				$newData = json_encode(array_diff($detail, $oldDetail));
				$mysqlDetails = [
					'userIpAddress' => session()->get('userIP'),
					'userName' => $username,
					'type' => config()->get('constant.dealer'),
					'name' => $id,
					'status' => config()->get('constant.updated'),
					'oldData' => $oldData,
					'newData' => $newData,
				];
				AuditNewController::updateAudit($mysqlDetails);
			} catch (Exception $e) {
				logger('ExceptionDealer_' . $dealerid . ' error -->' . $e->getMessage());
				logger('Error inside AuditDealer Update' . $e->getMessage());
			}


			$redis->hmset('H_UserId_Cust_Map', $dealerid . ':fcode', $fcode, $dealerid . ':mobileNo', $mobileNo, $dealerid . ':email', $email, $dealerid . ':zoho', $zoho, $dealerid . ':mapKey', $mapKey, $dealerid . ':addressKey', $addressKey, $dealerid . ':notificationKey', $notificationKey);
			if (strpos($gpsvtsAppKey, 'com.') !== false) {
				$redis->hset('H_Google_Android_ServerKey', $fcode . ':' . $dealerid, $notificationKey);
			} else {
				$redis->hdel('H_Google_Android_ServerKey', $fcode . ':' . $dealerid);
			}
			$oldDealerList = json_decode($oldDealerList, true);

			$imageName = $this->utilMethod($website);


			$upload_folder = '/var/www/' . $pathValue[1] . '/public/temp/'; ///var/www/gitsrc/vamos/public/assets/imgs/
			if ($uploads == 'yes') {
				if (request()->hasFile('logo_smallEdit')) {
					$logoSmall =  request()->file('logo_smallEdit');
					list($width, $height, $type, $attr) = getimagesize($logoSmall);
					if (!($height == 52 && $width == 52)) {
						return redirect()->back()->withInput()->withErrors('Please check with image 52*52 dimension or Png image format is mismatched');
					}
					$file_name_small = $imageName . '.' . 'small' . '.' . $logoSmall->getClientOriginalExtension();
					if (!file_exists($upload_folder)) {
						$result = File::makeDirectory($upload_folder, 0777, true);
					}
					$logoSmall->move($upload_folder, $file_name_small);
					$s3->putObject(array(
						'Bucket' => config()->get('constant.bucket'),
						'Key' => "picture/" . $file_name_small,
						'SourceFile' => $upload_folder . $file_name_small,
						'ACL' => 'public-read'
					));
					File::deleteDirectory($upload_folder);
				}
				if (request()->hasFile('logo_mobEdit')) {
					$logoMob =  request()->file('logo_mobEdit');
					list($width, $height, $type, $attr) = getimagesize($logoMob);
					if (!($height == 144 && $width == 272)) {
						return redirect()->back()->withInput()->withErrors('Please check with image 272*144 dimension or Png image format is mismatched');
					}
					$file_name_Mob = $imageName . '.' . $logoMob->getClientOriginalExtension();
					if (!file_exists($upload_folder)) {
						$result = File::makeDirectory($upload_folder, 0777, true);
					}
					$logoMob->move($upload_folder, $file_name_Mob);
					$s3->putObject(array(
						'Bucket' => config()->get('constant.bucket'),
						'Key' => "picture/" . $file_name_Mob,
						'SourceFile' => $upload_folder . $file_name_Mob,
						'ACL' => 'public-read'
					));
					File::deleteDirectory($upload_folder);
				}
				if (request()->hasFile('logo_deskEdit')) {
					$logoDesk =  request()->file('logo_deskEdit');
					list($width, $height, $type, $attr) = getimagesize($logoDesk);
					if (!($height == 144 && $width == 144)) {
						return redirect()->back()->withInput()->withErrors('Please check with image 144*144 dimension or Png image format is mismatched');
					}
					if (!file_exists($upload_folder)) {
						$result = File::makeDirectory($upload_folder, 0777, true);
					}
					$file_name_Desk = $dealerid . '.' . $logoDesk->getClientOriginalExtension();
					$logoDesk->move($upload_folder, $file_name_Desk);
					$s3->putObject(array(
						'Bucket' => config()->get('constant.bucket'),
						'Key' => "picture/" . $file_name_Desk,
						'SourceFile' => $upload_folder . $file_name_Desk,
						'ACL' => 'public-read'
					));
					File::deleteDirectory($file_name_Desk);
				}
			}
			// redirect
			return redirect()->to('vdmDealers')->with([
				'alert-class' => 'alert-success',
				'message' => "Successfully updated $dealerid !"
			]);
		}
	}



	public function utilMethod($imageName)
	{
		if (substr($imageName, 0, 3) == 'www')
			$imageName = substr($imageName, 4);

		return $imageName;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function store()
	{
		$username = auth()->id();
		$redis = Redis::connection();
		$s3 = app('aws')->createClient('s3', [
			'endpoint' => config()->get('constant.endpoint'),
		]);
		$auth = app('firebase.auth');
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$rules = array(
			'dealerId' => 'required|alpha_dash',
			'email' => 'required|email',
			'mobileNo' => 'required',
			'zoho'   => 'required'
		);

		$validator = validator()->make(request()->all(), $rules);

		if ($validator->fails()) {
			return redirect()->back()->withInput()->withErrors($validator);
		} else {
			$dealerId = request()->get('dealerId');
			$val = $redis->hget('H_UserId_Cust_Map', $dealerId . ':fcode');
			$val1 = $redis->sismember('S_Dealers_' . $fcode, $dealerId);
		}
		if (strpos($dealerId, 'admin') !== false || strpos($dealerId, 'ADMIN') !== false || strpos($dealerId, 'Admin') !== false) {
			return redirect()->back()->withInput()->withErrors('Name with admin not acceptable');
		}
		if ($val1 == 1 || isset($val)) {
			return redirect()->back()->withInput()->withErrors($dealerId . ' already exist. Please use different id ' . '!');
		} else {
			// store
			$path = $_SERVER['REQUEST_URI'];
			$pathValue =  explode("/", $path);

			$dealerId = request()->get('dealerId');
			$email = request()->get('email');
			$mobileNo = request()->get('mobileNo');
			$website = request()->get('website');
			$website1 = request()->get('website1');
			$subDomain = request()->get('subDomain');
			$smsSender = request()->get('smsSender');
			$smsProvider = request()->get('smsProvider');
			$providerUserName = request()->get('providerUserName');
			$providerPassword = request()->get('providerPassword');
			$zoho = request()->get('zoho');
			$mapKey = request()->get('mapKey');
			$mapKey1 = request()->get('mapKey1');
			$addressKey = request()->get('addressKey');
			$notificationKey = request()->get('notificationKey');
			$gpsvtsAppKey = request()->get('gpsvtsAppKey');
			$address = request()->get('address');
			$timeZone = request()->get('timeZone');
			$languageCode = request()->get("languageCode");
			$textLocalSmsKey = request()->get('textLocalSmsKey', '');
			$smsEntity = request()->get('smsEntity', '');

			if ($website == $website1) {
				return redirect()->back()->withErrors('Website names should not be same.');
			}

			$fransRef = $redis->hget('H_Franchise', $fcode);
			$franRefDatajson = json_decode($fransRef, true);
			$adminwebsiteArry = array($franRefDatajson['website'], $franRefDatajson['website2'], $franRefDatajson['website3'], $franRefDatajson['website4']);
			$adminwebsiteArry1 = array('www.' . $franRefDatajson['website'], 'www.' . $franRefDatajson['website2'], 'www.' . $franRefDatajson['website3'], 'www.s' . $franRefDatajson['website4']);
			if (in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower('www' . ($website)), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry1))) {
				$uploads = 'no';
			} else {
				$uploads = 'yes';
			}

			// /var/www/gitsrc/vamos/public/assets/imgs/ for production path
			$upload_folder = '/var/www/' . $pathValue[1] . '/public/temp/';

			$imageName = $this->utilMethod($website);

			$totalReports = $redis->smembers('S_Users_Reports_Admin_' . $fcode);
			if ($totalReports != null) {
				foreach ($totalReports as $key => $value) {
					$redis->sadd('S_Users_Reports_Dealer_' . $dealerId . '_' . $fcode, $value);
				}
			}
			if ($uploads == 'yes') {
				if (request()->hasFile('logo_small')) {
					$logoSmall =  request()->file('logo_small');
					list($width, $height, $type, $attr) = getimagesize($logoSmall);
					if ($height == 52 && $width == 52 && $type == 3) {
						$file_name_small = $imageName . '.' . 'small' . '.' . $logoSmall->getClientOriginalExtension();
						if (!file_exists($upload_folder)) {
							$result = File::makeDirectory($upload_folder, 0777, true);
						}
						$logoSmall->move($upload_folder, $file_name_small);
						$res = $s3->putObject(array(
							'Bucket' => config()->get('constant.bucket'),
							'Key' => "picture/" . $file_name_small,
							'SourceFile' => $upload_folder . $file_name_small,
							'ACL' => 'public-read'
						));
						File::deleteDirectory($upload_folder);
					} else {
						return redirect()->back()->withInput()->withErrors('Please check with image 52*52 dimension or PNG image format is mismatched.');
					}
				}
				if (request()->hasFile('logo_mob')) {
					$logoMob =  request()->file('logo_mob');
					list($width, $height, $type, $attr) = getimagesize($logoMob);
					if ($height == 144 && $width == 272 && $type == 3) {
						$file_name_Mob = $imageName . '.' . $logoMob->getClientOriginalExtension();
						if (!file_exists($upload_folder)) {
							$result = File::makeDirectory($upload_folder, 0777, true);
						}
						$logoMob->move($upload_folder, $file_name_Mob);
						$s3->putObject(array(
							'Bucket' => config()->get('constant.bucket'),
							'Key' => "picture/" . $file_name_Mob,
							'SourceFile' => $upload_folder . $file_name_Mob,
							'ACL' => 'public-read'
						));
						File::deleteDirectory($upload_folder);
					} else {
						return redirect()->back()->withInput()->withErrors('Please check with image 272*144 dimension or PNG image format is mismatched.');
					}
				}
				if (request()->hasFile('logo_desk')) {
					$logoDesk =  request()->file('logo_desk');
					list($width, $height, $type, $attr) = getimagesize($logoDesk);
					if ($height == 144 && $width == 144 && $type == 3) {
						$file_name_Desk = $dealerId . '.' . $logoDesk->getClientOriginalExtension();
						if (!file_exists($upload_folder)) {
							$result = File::makeDirectory($upload_folder, 0777, true);
						}
						$logoDesk->move($upload_folder, $file_name_Desk);
						$s3->putObject(array(
							'Bucket' => config()->get('constant.bucket'),
							'Key' => "picture/" . $file_name_Desk,
							'SourceFile' => $upload_folder . $file_name_Desk,
							'ACL' => 'public-read'
						));
						File::deleteDirectory($file_name_Desk);
					} else {
						return redirect()->back()->withInput()->withErrors('Please check with image 144*144 dimension or PNG image format is mismatched.');
					}
				}
			}
			if (session()->get('cur1') == 'prePaidAdmin') {
				$LicenceissuedDate = date('d-m-Y');
				$franDetails_json = $redis->hget('H_Franchise', $fcode);
				$franchiseDetails = json_decode($franDetails_json, true);
				$availableStarterLicence		= isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : '0';
				$availableBasicLicence		= isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';
				$availableAdvanceLicence	= isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';
				$availablePremiumLicence	= isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';
				$availablePremPlusLicence = isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0';

				$numofStarter = request()->get('numofStarter', '0');
				$numofbasic = request()->get('numofbasic', '0');
				$numofadvance = request()->get('numofadvance', '0');
				$numofpremium = request()->get('numofpremium', '0');
				$numofPremiumPlus = request()->get('numofpremiumplus', '0');


				if ($numofStarter == null) {
					$numofStarter = 0;
				}
				if ($numofbasic == null) {
					$numofbasic = 0;
				}
				if ($numofadvance == null) {
					$numofadvance = 0;
				}
				if ($numofpremium == null) {
					$numofpremium = 0;
				}
				if ($numofPremiumPlus == null) {
					$numofPremiumPlus = 0;
				}


				if ($availableStarterLicence < $numofStarter || $availableBasicLicence < $numofbasic || $availableAdvanceLicence < $numofadvance || $availablePremiumLicence < $numofpremium || $availablePremPlusLicence < $numofPremiumPlus) {
					return redirect()->back()->withInput()->withErrors('Number of entered licences is exceeded the available licences.');
				}

				$detail = array(
					'email'	=> $email,
					'mobileNo' => $mobileNo,
					'address' => $address,
					'website' => $website,
					'website1' => $website1,
					'subDomain' => $subDomain,
					'smsSender' => $smsSender,
					'smsProvider' => $smsProvider,
					'providerUserName' => $providerUserName,
					'providerPassword' => $providerPassword,
					'numofStarter'	=> $numofStarter,
					'numofBasic'	=> $numofbasic,
					'numofAdvance'	=> $numofadvance,
					'numofPremium'	=> $numofpremium,
					'numofPremiumPlus' => $numofPremiumPlus,
					'avlofStarter'	=> $numofStarter,
					'avlofBasic'	=> $numofbasic,
					'avlofAdvance'	=> $numofadvance,
					'avlofPremium'	=> $numofpremium,
					'avlofPremiumPlus' => $numofPremiumPlus,
					'zoho' => $zoho,
					'mapKey' => $mapKey,
					'mapKey1' => $mapKey1,
					'addressKey' => $addressKey,
					'notificationKey' => $notificationKey,
					'gpsvtsApp' => $gpsvtsAppKey,
					'LicenceissuedDate' => $LicenceissuedDate,
					'timeZone' => $timeZone,
					'languageCode' => $languageCode,
					'textLocalSmsKey' => $textLocalSmsKey,
					'smsEntity' => $smsEntity,
					'enableStarter' => 'no'
				);
				$detail = InputController::nullCheckArr($detail);
				$franchiseDetails['availableStarterLicence'] = $franchiseDetails['availableStarterLicence'] - $numofStarter;
				$franchiseDetails['availableBasicLicence'] = $franchiseDetails['availableBasicLicence'] - $numofbasic;
				$franchiseDetails['availableAdvanceLicence'] = $franchiseDetails['availableAdvanceLicence'] - $numofadvance;
				$franchiseDetails['availablePremiumLicence'] = $franchiseDetails['availablePremiumLicence'] - $numofpremium;
				if (isset($franchiseDetails['availablePremPlusLicence']) == 1) {
					$franchiseDetails['availablePremPlusLicence'] = $franchiseDetails['availablePremPlusLicence'] - $numofPremiumPlus;
				}


				$detailsJson = json_encode($franchiseDetails);
				$redis->hmset('H_Franchise', $fcode, $detailsJson);
			} else {
				// ram
				$detail = array(
					'email'	=> $email,
					'mobileNo' => $mobileNo,
					'address' => $address,
					'website' => $website,
					'website1' => $website1,
					'subDomain' => $subDomain,
					'smsSender' => $smsSender,
					'smsProvider' => $smsProvider,
					'providerUserName' => $providerUserName,
					'providerPassword' => $providerPassword,
					'zoho' => $zoho,
					'mapKey' => $mapKey,
					'mapKey1' => $mapKey1,
					'addressKey' => $addressKey,
					'notificationKey' => $notificationKey,
					'gpsvtsApp' => $gpsvtsAppKey,
					'timeZone' => $timeZone,
					'languageCode' => 'en',
					'textLocalSmsKey' => $textLocalSmsKey,
					'smsEntity' => $smsEntity
				);
			}
			$detail = InputController::nullCheckArr($detail);
			$detailJson = json_encode($detail);

			$redis->sadd('S_Dealers_' . $fcode, $dealerId);
			$redis->hset('H_DealerDetails_' . $fcode, $dealerId, $detailJson);
			// firebase user add here
			//GeoLoction
			$geolocation = VdmFranchiseController::geocode($address);
			if ($geolocation != null) {
				$LatLong = implode(":", $geolocation);
			} else {
				$LatLong = '13.0231406:80.2087368';
			}
			$redis->hset('H_Franchise_LatLong', $fcode . ":" . $dealerId, $LatLong);

			try {
				$status = array(
					'userIpAddress' => session()->get('userIP'),
					'userName'=> $username,
					'type'=>config()->get('constant.dealer'),
					'name'=>$dealerId,
					'status'=>config()->get('constant.created'),
					'oldData'=>"",
					'newData'=>$detailJson,
				);
				AuditNewController::updateAudit($status);

			} catch (Exception $e) {
				AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
				logger('ExceptionDealer_' . $dealerId . ' error -->' . $e->getMessage());
				logger('Error inside AuditDealer Store' . $e->getMessage());
			}

			if (strpos($gpsvtsAppKey, 'com.') !== false) {
				$redis->hset('H_Google_Android_ServerKey', $fcode . ':' . $dealerId, $notificationKey);
			} else {
				$redis->hdel('H_Google_Android_ServerKey', $fcode . ':' . $dealerId);
			}
			$password = request()->get('password');
			if ($password == null) {
				$password = 'awesome';
			}
			info('dealer craete');
			$redis->hmset('H_UserId_Cust_Map', $dealerId . ':fcode', $fcode, $dealerId . ':mobileNo', $mobileNo, $dealerId . ':email', $email, $dealerId . ':password', $password);
			try {
				$userProperties = [
					'email' => $dealerId . '@vamosys.com',
					'emailVerified' => false,
					'password' => $password,
					'displayName' => $dealerId,
					// 'phoneNumber' => "+918248092270",
					'disabled' => false,
					'uid' => $dealerId,
				];
				$let = $auth->createUser($userProperties);
			} catch (FirebaseException $e) {
				logger('FirebaseException --->' . $e->getMessage());
				if ('DUPLICATE_LOCAL_ID' == $e->getMessage()) {
					return redirect()->back()->withInput()->withErrors('Dealer name already exist');
				}
				return redirect()->back()->withInput()->withErrors($e->getMessage());
			}
			info('dealer craete after');
			session()->flash('message', 'Successfully created ' . $dealerId . '!');
			return redirect()->to('vdmDealers');
		}
	}
	public function destroy($id)
	{

		$username = auth()->id();
		$userId = $id;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$delDetail = $redis->hget('H_DealerDetails_' . $fcode, $userId);
		$redis->srem('S_Dealers_' . $fcode, $userId);
		$redis->hdel('H_DealerDetails_' . $fcode, $userId);

		try {
			$status = array(
				'userIpAddress' => session()->get('userIP'),
				'userName'=> $username,
				'type'=>config()->get('constant.dealer'),
				'name'=>$id,
				'status'=>config()->get('constant.deleted'),
				'oldData'=>"",
				'newData'=>$delDetail,
			);
			AuditNewController::updateAudit($status);
		} catch (Exception $e) {
			AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
			logger('ExceptionDealer_' . $userId . ' error -->' . $e->getMessage());
			logger('Error inside AuditDealer destroyed' . $e->getMessage());
		}



		$email = $redis->hget('H_UserId_Cust_Map', $userId . ':email');
		$mobileNo = $redis->hget('H_UserId_Cust_Map', $userId . ':mobileNo');
		$redis->hdel('H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo, $userId . ':email', $email, $userId . ':password');

		$vehicles = $redis->smembers('S_Vehicles_Dealer_' . $userId . '_' . $fcode);
		foreach ($vehicles as $key => $value) {
			$refData = $redis->hget('H_RefData_' . $fcode, $value);
			$refDataJson1 = json_decode($refData, true);
			
			$redis->del('L_Suggest_' . $refDataJson1['shortName'] . '_' . $refDataJson1['orgId'] . '_' . $fcode);
			$redis->hdel('H_Stopseq_' . $refDataJson1['orgId'] . '_' . $fcode, $refDataJson1['shortName'] . ':' . 'morning');
			$redis->hdel('H_Stopseq_' . $refDataJson1['orgId'] . '_' . $fcode, $refDataJson1['shortName'] . ':' . 'evening');
			$refDataArr =   array(
				'onboardDate' => '',
				'orgId' => 'DEFAULT',
				'expiredPeriod' => '',
				'OWN' => 'OWN',
				'licenceissuedDate' => '',
			);
			$refDataArr = array_merge($refDataJson1,$refDataArr);
			$refDataJson = json_encode($refDataArr);
			$redis->hset('H_RefData_' . $fcode, $value, $refDataJson);

			$redis->del('S_' . $value . '_' . $fcode);
			$redis->srem('S_Vehicles_Dealer_' . $userId . '_' . $fcode, $value);
			$redis->sadd('S_Vehicles_Admin_' . $fcode, $value);
			$prod = '12.9995857,80.2027206,0,N,1480153001488,0.0,N,N,ON,0,S,N';
			$redis->hset('H_ProData_' . $fcode, $value, $prod);
			$value11 = strtoupper($value);
			$devId = strtoupper($refDataJson1['deviceId']);
			$shortName1 = strtoupper($refDataJson1['shortName']);
			$shortName = str_replace(' ', '', $shortName1);
			$orgId1 = strtoupper($refDataJson1['orgId']);
			$searchKey = $value11 . ':' . $devId . ':' . $shortName . ':' . $orgId1 . ':' . $refDataJson1['gpsSimNo'];
			$redis->hset('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $searchKey, $value);

			//$redis->sadd ( 'S_Vehicles_' . $fcode, $vehicle );
		}

		$groups = $redis->smembers('S_Groups_Dealer_' . $userId . '_' . $fcode);
		foreach ($groups as $key => $value1) {
			$redis->del($value1);
			$redis->del('S_' . $value1);
			$redis->srem('S_Groups_Dealer_' . $userId . '_' . $fcode, $value1);
			//$redis->sadd('S_Groups_Admin_'.$fcode,$value1);
		}

		$orgDel = $redis->smembers('S_Organisations_Dealer_' . $userId . '_' . $fcode);
		foreach ($orgDel as $key => $value2) {
			$redis->hdel('H_Organisations_' . $fcode, $value2);
			$redis->srem('S_Organisations_Dealer_' . $userId . '_' . $fcode, $value2);
			//$redis->sadd('S_Organisations_Admin'.$fcode,$value2);
			$redis->del('H_Site_' . $value2 . '_' . $fcode);
			$redis->del('S_' . $value2);
			$redis->hdel('H_Scheduler_' . $fcode, 'getReadyForMorningSchoolTrips_' . $value2);
			$redis->hdel('H_Scheduler_' . $fcode, 'getReadyForAfternoonSchoolTrips_' . $value2);
			$redis->hdel('H_Scheduler_' . $fcode, 'getReadyForEveningSchoolTrips_' . $value2);
			$redis->del('H_Poi_' . $value2 . '_' . $fcode);
			$redis->del('S_Poi_' . $value2 . '_' . $fcode);
			$redis->del('S_RouteList_' . $value2 . '_' . $fcode);
		}

		$users = $redis->smembers('S_Users_Dealer_' . $userId . '_' . $fcode);
		foreach ($users as $key => $value3) {
			$redis->del($value3);
			$redis->srem('S_Users_Dealer_' . $userId . '_' . $fcode, $value3);
			$redis->hdel('H_UserSmsInfo_' . $fcode, $value3);
			$redis->srem('S_Users_Virtual_' . $fcode, $value3);
			$redis->srem('S_Users_AssetTracker_' . $fcode, $value3);
			$redis->del('S_Orgs_' . $value3 . '_' . $fcode);
			$redis->hdel('H_Notification_Map_User', $value3);
			//$redis->sadd('S_Users_Admin_'.$fcode,$value3);
			$redis->hdel('H_UserId_Cust_Map', $value3 . ':fcode', $fcode, $value3 . ':mobileNo', $mobileNo, $value3 . ':email', $email, $value3 . ':password');
			DB::table('users')->where('username', $value3)->delete();
		}
		//'H_VehicleName_Mobile_Dealer_'.$userId.'_Org_'.$fcode
		$redis->del('S_Pre_Onboard_Dealer_' . $userId . '_' . $fcode);
		$redis->del('H_Pre_Onboard_Dealer_' . $userId . '_' . $fcode);
		$redis->del('H_VehicleName_Mobile_Dealer_' . $userId . '_Org_' . $fcode);
		$redis->hdel('H_Google_Android_ServerKey', $fcode . ':' . $userId);

		DB::table('users')->where('username', $userId)->delete();
		try {
			// FirebaseController::deleteUser($userId);
		} catch (FirebaseException $e) {
			logger('FirebaseException destroy --->' . $e->getMessage());
			return redirect()->back()->withErrors($e->getMessage());
		}


		session()->put('email', $email);
		return redirect()->to('vdmDealers')->with([
			'message'=> "Successfully deleted  $userId !"
		]);
	}

	public function dealerCheck()
	{

		$username =	auth()->id();
		$redis = Redis::connection();
		$newGroupId = request()->get('id');
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$dealerName = $redis->sismember('S_Dealers_' . $fcode, $newGroupId);
		$dealerVal = $redis->hget('H_UserId_Cust_Map', $newGroupId . ':fcode');



		if (strpos($newGroupId, 'admin') !== false || strpos($newGroupId, 'ADMIN') !== false || strpos($newGroupId, 'Admin') !== false) {
			return  'Name with admin not acceptable';
		}
		if ($dealerName == 1 || isset($dealerVal)) {
			return 'Dealer Name already Exist !';
		}
		return false;
	}

	public function sendExcel()
	{

		$excel = Excel::download(new DealerExport(), 'Dealer_Details.xlsx');
		return $excel;
	}

	public function licenceAlloc()
	{

		$username =	auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$dealersList1 = $redis->smembers('S_Dealers_' . $fcode);
		$dealersList = array('' => '-- Select --');
		sort($dealersList1);
		foreach ($dealersList1 as $key => $dealerId) {
			$dealersList = Arr::add($dealersList, $dealerId, $dealerId);
		}
		$allocateLicenceData = [
			'dealersList' => $dealersList,
		];
		$allocateLicenceData = array_merge($allocateLicenceData, VdmBillingController::getLicenceDetails());
		// return $allocateLicenceData;
		return view('vdm.dealers.dealLicenceAlc', $allocateLicenceData);
	}

	public function enabledStarterDealer()
	{
		$username =	auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$delaerId = request()->get('delaerId');
		$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $delaerId);
		$detail = json_decode($detailJson, true);
		$enableStarter = $detail['enableStarter'] ?? 'no';
		$authenticated = $detail['authenticated'] ?? 'no';
		return  [
			'enableStarter' => $enableStarter,
			'authenticated' => $authenticated
		];
	}

	public function licenceUpdate()
	{

		$username =	auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$rules = array(
			'dealersList' => 'required',
		);

		$validator = validator()->make(request()->all(), $rules);
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator);
		} else {
			$addofStarterPlus = request()->get('addofStarterPlus', '0');
			$addofStarter = request()->get('addofStarter', '0');
			$addofBasic = request()->get('addofBasic', '0');
			$addofAdvance = request()->get('addofAdvance', '0');
			$addofPremium = request()->get('addofPremium', '0');
			$addofPremiumPlus = request()->get('addofPremiumPlus', '0');
			$dealerId = request()->get('dealersList');
			if ($addofStarterPlus == null && $addofPremium == null && $addofAdvance == null && $addofStarter == null && $addofBasic == null && $addofPremiumPlus == null) {
				return redirect()->back()->withInput()->withErrors('Please enter the Licence Quantity!');
			} else {
				//Dealer Licence Increase
				if ($addofStarterPlus == null) {
					$addofStarterPlus = 0;
				}
				if ($addofStarter == null) {
					$addofStarter = 0;
				}
				if ($addofBasic == null) {
					$addofBasic = 0;
				}
				if ($addofAdvance == null) {
					$addofAdvance = 0;
				}
				if ($addofPremium == null) {
					$addofPremium = 0;
				}
				if ($addofPremiumPlus == null) {
					$addofPremiumPlus = 0;
				}
				$dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $dealerId);
				$dealersDetails = json_decode($dealersDetails_json, true);
				$oldDealerDetails=$dealersDetails;

				$dealersDetails['avlofStarterPlus'] = (isset($dealersDetails['avlofStarterPlus']) ? $dealersDetails['avlofStarterPlus'] : 0) + $addofStarterPlus;
				$dealersDetails['avlofStarter'] = isset($dealersDetails['avlofStarter']) ? $dealersDetails['avlofStarter'] : 0;
				$dealersDetails['avlofStarter'] = $dealersDetails['avlofStarter'] + $addofStarter;
				$dealersDetails['avlofBasic'] = $dealersDetails['avlofBasic'] + $addofBasic;
				$dealersDetails['avlofAdvance'] = $dealersDetails['avlofAdvance'] + $addofAdvance;
				$dealersDetails['avlofPremium'] = $dealersDetails['avlofPremium'] + $addofPremium;
				if (isset($dealersDetails['avlofPremiumPlus']) == 1) {
					$dealersDetails['avlofPremiumPlus'] = $dealersDetails['avlofPremiumPlus'] + $addofPremiumPlus;
				}
				//number of Licence
				$dealersDetails['numofStarterPlus'] = (isset($dealersDetails['numofStarterPlus']) ? $dealersDetails['numofStarterPlus'] : 0) + $addofStarterPlus;
				$dealersDetails['numofStarter'] = isset($dealersDetails['numofStarter']) ? $dealersDetails['numofStarter'] : 0;
				$dealersDetails['numofStarter'] = $dealersDetails['numofStarter'] + $addofStarter;
				$dealersDetails['numofBasic'] = $dealersDetails['numofBasic'] + $addofBasic;
				$dealersDetails['numofAdvance'] = $dealersDetails['numofAdvance'] + $addofAdvance;
				$dealersDetails['numofPremium'] = $dealersDetails['numofPremium'] + $addofPremium;
				if (isset($dealersDetails['numofPremiumPlus']) == 1) {
					$dealersDetails['numofPremiumPlus'] = $dealersDetails['numofPremiumPlus'] + $addofPremiumPlus;
				}
				$detailsJson = json_encode($dealersDetails);
				$redis->hmset('H_DealerDetails_' . $fcode, $dealerId, $detailsJson);
				//Franchise Licence Reduce
				$franDetails_json = $redis->hget('H_Franchise', $fcode);
				$franchiseDetails = json_decode($franDetails_json, true);
				$franchiseDetails['availableStarterPlusLicence'] = (isset($franchiseDetails['availableStarterPlusLicence']) ? $franchiseDetails['availableStarterPlusLicence'] : 0) - $addofStarterPlus;
				$franchiseDetails['availableStarterLicence'] = (isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : 0) - $addofStarter;
				$franchiseDetails['availableBasicLicence'] = $franchiseDetails['availableBasicLicence'] - $addofBasic;
				$franchiseDetails['availableAdvanceLicence'] = $franchiseDetails['availableAdvanceLicence'] - $addofAdvance;
				$franchiseDetails['availablePremiumLicence'] = $franchiseDetails['availablePremiumLicence'] - $addofPremium;
				if (isset($franchiseDetails['availablePremPlusLicence']) == 1) {
					$franchiseDetails['availablePremPlusLicence'] = $franchiseDetails['availablePremPlusLicence'] - $addofPremiumPlus;
				}
				$detailsJson = json_encode($franchiseDetails);
				$redis->hmset('H_Franchise', $fcode, $detailsJson);

				$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $dealerId);
				$detail = json_decode($detailJson, true);
				$detailofMysql = array(
					'email'	=> isset($detail['email']) ? $detail['email'] : '-',
					'mobileNo' => isset($detail['mobileNo']) ? $detail['mobileNo'] : '-',
					'website' => isset($detail['website']) ? $detail['website'] : '-',
					'website1' => isset($detail['website1']) ? $detail['website1'] : '-',
					'smsSender' => isset($detail['smsSender']) ? $detail['smsSender'] : '-',
					'smsProvider' => isset($detail['smsProvider']) ? $detail['smsProvider'] : '-',
					'providerUserName' => isset($detail['providerUserName']) ? $detail['providerUserName'] : '-',
					'providerPassword' => isset($detail['providerPassword']) ? $detail['providerPassword'] : '-',
					'numofStarter'	=> isset($detail['numofStarter']) ? $detail['numofStarter'] : '-',
					'numofBasic'	=> isset($detail['numofBasic']) ? $detail['numofBasic'] : '-',
					'numofAdvance'	=> isset($detail['numofAdvance']) ? $detail['numofAdvance'] : '-',
					'numofPremium'	=> isset($detail['numofPremium']) ? $detail['numofPremium'] : '-',
					'numofPremiumPlus' => isset($detail['numofPremiumPlus']) ? $detail['numofPremiumPlus'] : '-',
					'avlofStarter'	=> isset($detail['avlofStarter']) ? $detail['avlofStarter'] : '-',
					'avlofBasic'	=> isset($detail['avlofBasic']) ? $detail['avlofBasic'] : '-',
					'avlofAdvance'	=> isset($detail['avlofAdvance']) ? $detail['avlofAdvance'] : '-',
					'avlofPremium'	=> isset($detail['avlofPremium']) ? $detail['avlofPremium'] : '-',
					'avlofPremiumPlus' => isset($detail['avlofPremiumPlus']) ? $detail['avlofPremiumPlus'] : '-',
					'zoho' => isset($detail['zoho']) ? $detail['zoho'] : '-',
					'mapKey' => isset($detail['mapKey']) ? $detail['mapKey'] : '-',
					'mapKey1' => isset($detail['mapKey1']) ? $detail['mapKey1'] : '-',
					'addressKey' => isset($detail['addressKey']) ? $detail['addressKey'] : '-',
					'notificationKey' => isset($detail['notificationKey']) ? $detail['notificationKey'] : '-',
					'gpsvtsApp' => isset($detail['gpsvtsApp']) ? $detail['gpsvtsApp'] : '-',
					'LicenceissuedDate' => isset($detail['LicenceissuedDate']) ? $detail['LicenceissuedDate'] : '-',
				);
				try {
					$mysqlDetails = array();
					$oldData = array_diff($oldDealerDetails,$dealersDetails);
					$newData = array_diff($dealersDetails,$oldDealerDetails);
					$status = array(
						'userIpAddress' => session()->get('userIP'),
						'userName'=> $username,
						'type'=>config()->get('constant.dealer'),
						'name'=>$dealerId,
						'status'=>config()->get('constant.licenceALC'),
						'oldData'=>json_encode($oldData),
						'newData'=>json_encode($newData),
					);
					AuditNewController::updateAudit($status);

				} catch (Exception $e) {
					AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
					logger('ExceptionDealer_' . $dealerId . ' error -->' . $e->getMessage());
					logger('Error inside AuditDealer licence update' . $e->getMessage());
				}

				$massage = ' Successfully Allocated Licence for ' . $dealerId . '!';
				session()->flash('message', $massage);
				session()->flash('alert-class', 'alert-success');
				return redirect()->back();
			}
		}
	}
	public function disable($dealerId)
	{
		$username =	auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $dealerId . ':fcode');
		$lock = $redis->hget('H_UserId_Cust_Map', $dealerId . ':lock');
		if ($lock == 'Disabled') {
			session()->flash('message', $dealerId . 'Already Disabled !');
		} else {
			$redis->hmset('H_UserId_Cust_Map', $dealerId . ':lock', 'Disabled');

			try {

				$status = array(
					'userIpAddress' => session()->get('userIP'),
					'userName'=> $username,
					'type'=>config()->get('constant.dealer'),
					'name'=>$dealerId,
					'status'=>config()->get('constant.disabled'),
					'oldData'=>"",
					'newData'=>"",
				);
				AuditNewController::updateAudit($status);

			} catch (Exception $e) {
				AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
				logger('ExceptionDealer_' . $dealerId . ' error -->' . $e->getMessage());
				logger('Error inside AuditDealer  for Disable' . $e->getMessage());
			}
			session()->flash('message', $dealerId . ' Successfully Disabled !');
		}

		return redirect()->to('vdmDealers');
	}
	public function enable($dealerId)
	{

		$username =	auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $dealerId . ':fcode');
		$lock = $redis->hget('H_UserId_Cust_Map', $dealerId . ':lock');
		if ($lock == 'Disabled') {
			$redis->hdel('H_UserId_Cust_Map', $dealerId . ':lock');

			try {
				$status = array(
					'userIpAddress' => session()->get('userIP'),
					'userName'=> $username,
					'type'=>config()->get('constant.dealer'),
					'name'=>$dealerId,
					'status'=>config()->get('constant.enabled'),
					'oldData'=>"",
					'newData'=>"",
				);
				AuditNewController::updateAudit($status);
			} catch (Exception $e) {
				AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
				logger('ExceptionDealer_' . $dealerId . ' error -->' . $e->getMessage());
				logger('Error inside AuditDealer  for Enable' . $e->getMessage());
			}
			session()->flash('message', $dealerId . ' Successfully Enabled !');
		} else {
			session()->flash('message', $dealerId . 'Already Enabled !');
		}
		return redirect()->to('vdmDealers');
	}
}
