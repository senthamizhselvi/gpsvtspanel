<?php

namespace App\Http\Controllers;

use App\Models\AuditOrg;
use App\Exports\FormUpdateExport;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Facades\Excel;

class VdmOrganizationController extends Controller
{
	public function create()
	{
		$id = '';
		$createData = [
			'orgName1' => $id,
			'smsP' => VdmFranchiseController::smsP()
		];
		return view('vdm.organization.create', $createData);
	}

	public function createNew($id)
	{
		$createData = [
			'orgName1' => $id,
			'smsP' => VdmFranchiseController::smsP()
		];
		return view('vdm.organization.create', $createData);
	}

	public function placeOfInterest()
	{
		
		$username = auth()->id();
		$redis = Redis::connection();

		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$Places = $redis->zrange('S_Places_India', 0, -1);

		$userplace = array();
		$orgList = [];
		try {
			if ($Places != null) {
				$t = 0;
				foreach ($Places as $key => $value) {

					$userplace[$t] = $value;
					$t++;
				}
			}
			$tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
			if (session()->get('cur') == 'dealer') {
				$tmpOrgList = $redis->smembers('S_Organisations_Dealer_' . $username . '_' . $fcode);
			} else if (session()->get('cur') == 'admin') {
				$tmpOrgList = $redis->smembers('S_Organisations_Admin_' . $fcode);
			}
			foreach ($tmpOrgList as $org) {
				$orgList = Arr::add($orgList, $org, $org);
			}
			$placeOfInterestData = [
				'userplace' => $userplace,
				'orgList' => $orgList,
			];
			return view('vdm.organization.placeOfInterest', $placeOfInterestData);
		} catch (\Exception $e) {
			return redirect()->back()->withErrors($e->getMessage());
		}
	}

	public function addpoi()
	{
		$username = auth()->id();
		$rules = array(
			'orgId'       => 'required',
			'radiusrange' => 'required',
			'poi' => 'required'
		);
		$validator = validator()->make(request()->all(), $rules);
		if ($validator->fails()) {
			return redirect()->back()->withInput()->withErrors($validator);
		} else {
			// store
			$orgId  = request()->get('orgId');
			$poi = request()->get('poi');
			$radiusrange = request()->get('radiusrange');
			$redis = Redis::connection();
			$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

			$jsonData =  $redis->hget('H_Organisations_' . $fcode, $orgId);

			$orgDataArr = json_decode($jsonData, true);

			$email = isset($orgDataArr['email']) ? $orgDataArr['email'] : ' ';
			$address = isset($orgDataArr['address']) ? $orgDataArr['address'] : ' ';
			$description = isset($orgDataArr['description']) ? $orgDataArr['description'] : ' ';
			$mobile = isset($orgDataArr['mobile']) ? $orgDataArr['mobile'] : ' ';
			$time1 = isset($orgDataArr['startTime']) ? $orgDataArr['startTime'] : ' ';
			$time2 = isset($orgDataArr['endTime']) ? $orgDataArr['endTime'] : ' ';
			$etc = isset($orgDataArr['etc']) ? $orgDataArr['etc'] : ' ';
			$mtc = isset($orgDataArr['mtc']) ? $orgDataArr['mtc'] : ' ';
			$atc = isset($orgDataArr['atc']) ? $orgDataArr['atc'] : ' ';
			$parkDuration = isset($orgDataArr['parkDuration']) ? $orgDataArr['parkDuration'] : '';
			$idleDuration = isset($orgDataArr['idleDuration']) ? $orgDataArr['idleDuration'] : '';
			$parkingAlert = isset($orgDataArr['parkingAlert']) ? $orgDataArr['parkingAlert'] : '';
			$idleAlert = isset($orgDataArr['idleAlert']) ? $orgDataArr['idleAlert'] : '';
			$overspeedalert = isset($orgDataArr['overspeedalert']) ? $orgDataArr['overspeedalert'] : '';
			$sendGeoFenceSMS = isset($orgDataArr['sendGeoFenceSMS']) ? $orgDataArr['sendGeoFenceSMS'] : '';
			$smsSender = isset($orgDataArr['smsSender']) ? $orgDataArr['smsSender'] : '';
			$sosAlert = isset($orgDataArr['sosAlert']) ? $orgDataArr['sosAlert'] : '';
			$live = isset($orgDataArr['live']) ? $orgDataArr['live'] : '';
			$smsProvider = isset($orgDataArr['smsProvider']) ? $orgDataArr['smsProvider'] : '';
			$providerUserName = isset($orgDataArr['providerUserName']) ? $orgDataArr['providerUserName'] : '';
			$providerPassword = isset($orgDataArr['providerPassword']) ? $orgDataArr['providerPassword'] : '';
			$deleteHistoryEod = isset($orgDataArr['deleteHistoryEod']) ? $orgDataArr['deleteHistoryEod'] : 'no';
			$harshBreak = isset($orgDataArr['harshBreak']) ? $orgDataArr['harshBreak'] : 'no';
			$dailySummary = isset($orgDataArr['dailySummary']) ? $orgDataArr['dailySummary'] : 'no';
			$dailyDieselSummary = isset($orgDataArr['dailyDieselSummary']) ? $orgDataArr['dailyDieselSummary'] : 'no';
			$fuelLevelBelow = isset($orgDataArr['fuelLevelBelow']) ? $orgDataArr['fuelLevelBelow'] : 'no';
			$fuelLevelBelowValue = isset($orgDataArr['fuelLevelBelowValue']) ? $orgDataArr['fuelLevelBelowValue'] : '';
			$fuelApproximate = isset($orgDataArr['fuelApproximate']) ? $orgDataArr['fuelApproximate'] : '';
			$tripPlannedTime = isset($orgDataArr['tripPlannedTime']) ? $orgDataArr['tripPlannedTime'] : 'no';
			$instantFuel = isset($orgDataArr['instantFuel']) ? $orgDataArr['instantFuel'] : 'no';
			if ($fuelApproximate == 'no') {
				$fuelApproximate = '';
			}
			$radius = $radiusrange;
			$orgDataArr = array(
				'mobile' => $mobile,
				'email' => $email,
				'address' => $address,
				'description' => $description,
				'startTime' => $time1,
				'endTime'  => $time2,
				'atc' => $atc,
				'etc' => $etc,
				'mtc' => $mtc,
				'parkingAlert' => $parkingAlert,
				'idleAlert' => $idleAlert,
				'parkDuration' => $parkDuration,
				'idleDuration' => $idleDuration,
				'overspeedalert' => $overspeedalert,
				'sendGeoFenceSMS' => $sendGeoFenceSMS,
				'radius' => $radiusrange,
				'smsSender' => $smsSender,
				'sosAlert' => $sosAlert,
				'live' => $live,
				'smsProvider' => $smsProvider,
				'providerUserName' => $providerUserName,
				'providerPassword' => $providerPassword,
				'deleteHistoryEod' => $deleteHistoryEod,
				'harshBreak' => $harshBreak,
				'dailySummary' => $dailySummary,
				'dailyDieselSummary' => $dailyDieselSummary,
				'fuelLevelBelow' => $fuelLevelBelow,
				'fuelLevelBelowValue' => $fuelLevelBelowValue,
				'fuelApproximate' => $fuelApproximate,
				'tripPlannedTime' => $tripPlannedTime,
				'instantFuel'	  => $instantFuel,
			);

			$orgDataJson = json_encode($orgDataArr);
			$redis->hset('H_Organisations_' . $fcode, $orgId, $orgDataJson);


			$temp = 'S_Poi_' . $orgId . '_' . $fcode . ':temp';
			$redis->del($temp);
			foreach ($poi as $place) {
				$redis->sadd($temp, $place);
			}
			$ipaddress = $redis->get('ipaddress');
			$url = 'http://' . $ipaddress . ':9000/getPoiPlace?key=' . $temp;
			$url = htmlspecialchars_decode($url);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			$response = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
		}

		return redirect()->to('vdmOrganization')->with([
			'message'=> 'Successfully created !'
		]);
	}

	public function updateNotification()
	{
		
		$username = auth()->id();
		$redis = Redis::connection();

		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$orgId = request()->get('orgId');
		$emailList = request()->get('emailList');
		$smsList = request()->get('smsList');
		$oldMailData = $redis->hget("H_EMAIL_" . $fcode, $orgId);
		$oldSMSData = $redis->hget("H_SMS_" . $fcode, $orgId);

		if($oldMailData==null) $oldMailData="";
		$oldMailDataArr = explode(",", $oldMailData);
		if($oldSMSData==null) $oldSMSData="";
		$oldSMSDataArr = explode(",", $oldSMSData);
		
		if ($emailList && count($emailList) > 0) {
			$email = implode(",", $emailList);
			$emailArr = implode(", ", $emailList);
			$redis->hset("H_EMAIL_" . $fcode, $orgId, $email);
		} else {
			$emailArr = "";
			$redis->hdel("H_EMAIL_" . $fcode, $orgId);
		}
		if ($smsList && count($smsList) > 0) {
			$sms = implode(",", $smsList);
			$smsArr = implode(", ", $smsList);
			$redis->hset("H_SMS_" . $fcode, $orgId, $sms);
		} else {
			$smsArr = "";
			$redis->hdel("H_SMS_" . $fcode, $orgId);
		}
		
		$newArrMail=array_diff($emailList,$oldMailDataArr);
		$oldArrMail=array_diff($oldMailDataArr,$emailList);
		$newArrSms=array_diff($smsList,$oldSMSDataArr);
		$oldArrSms=array_diff($oldSMSDataArr,$smsList);
		
		try {
			
			$mysqlDetails = [
				'userIpAddress' => session()->get('userIP'),
				'userName' => $username,
				'type' => config()->get('constant.organization'),
				'name' => $orgId,
				'status' => config()->get('constant.editAlerts'),
				'oldData' => json_encode([
					'sms'=>implode(",",$oldArrSms),
					'mail'=> implode(",",$oldArrMail) ]),
				'newData' => json_encode([
					'sms'=>implode(",",$newArrSms),
					'mail'=> implode(",",$newArrMail) ])
				
			];
			AuditNewController::updateAudit($mysqlDetails);
		} catch (Exception $e) {
			AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
			logger('Exception_' . $orgId . ' DeleteAuditOrg error -->' . $e->getMessage() . ' line -->' . $e->getLine());
			logger('Error inside AuditOrg Delete' . $e->getMessage());
		}

		return redirect()->to('vdmOrganization')->with([
			'alert-class'=> 'alert-success',
			'message', "Notification alert Successfully updated $orgId !"
		]);
	}

	public function siteUpdate()
	{
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$orgId = request()->get('orgId');
		$emailList = request()->get('emailList');
		$smsList = request()->get('smsList');
		$timeList = request()->get('time');
		$enableList = request()->get('enable');
		$alertList = request()->get('key');
		$siteName = request()->get('value');
		$alertList = $redis->hgetall('H_Site_' . $orgId . '_' . $fcode);
		$index  = 0;
		foreach ($alertList as $keyS => $value) {
			if ($index % 2 != 0)
				continue; //The key is odd, skip
			$siteList[] = $keyS;
			$index++;
		}
		$dd = [];
		$siteList1 = isset($siteList) ? $siteList : $dd;
		$rr = count($siteList1);
		for ($i = 0; $i < $rr; $i++) {
			$siteName = $siteList[$i];
			$timeS = $timeList[$i];
			if ($timeS == "") {
				$timeS = 0;
			}
			$enableS = $enableList[$i];
			$refDataJson1 = $redis->hget('H_Site_' . $orgId . '_' . $fcode, $siteName);
			$refDataJson1 = json_decode($refDataJson1, true);

			$refDataArr = array(
				'siteName' => isset($refDataJson1['siteName']) ? $refDataJson1['siteName'] : $siteName,
				'siteType' => isset($refDataJson1['siteType']) ? $refDataJson1['siteType'] : ' ',
				'userId' => isset($refDataJson1['userId']) ? $refDataJson1['userId'] : 'XXXXX',
				'latLng' => isset($refDataJson1['latLng']) ? $refDataJson1['latLng'] : ' ',
				'OrgId' =>  isset($refDataJson1['OrgId']) ? $refDataJson1['OrgId'] : '',
				'sitePlanTime' =>  isset($refDataJson1['sitePlanTime']) ? $refDataJson1['sitePlanTime'] : '0',
				'time' => $timeS,
				'enable' => $enableS,
			);

			$refDataJson = json_encode($refDataArr);
			$redis->hdel('H_Site_' . $orgId . '_' . $fcode, $siteName);
			$redis->hset('H_Site_' . $orgId . '_' . $fcode, $siteName, $refDataJson);
		}
		return redirect()->to('vdmOrganization')->with([
			'message'=> 'Successfully created !'
		]);
	}


	public function poiEdit($id)
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$orgId = $id;
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$places = $redis->zrange('S_Places_India', 0, -1);

		$selectedVehicles =  $redis->zrange('S_Poi_' . $id . '_' . $fcode, 0, -1);

		$shortNameList = null;
		$placeList = array();
		$t = 0;
		foreach ($places as $key => $value) {
			$placeList[$t] = $value;
			$t++;
		}

		$vehicleRefData = $redis->hget('H_Organisations_' . $fcode, $id);
		$vehicleRefData = json_decode($vehicleRefData, true);
		$radiusrange = isset($vehicleRefData['radius']) ? $vehicleRefData['radius'] : '';

		$poiEditDate = [
			'orgId' => $orgId,
			'userplace' => $placeList,
			'selectedVehicles' => $selectedVehicles,
			'radiusrange' => $radiusrange
		];

		return view('vdm.organization.poiEdit', $poiEditDate);
	}

	public function poiDelete($id)
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$orgId = $id;
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$redis->del('S_Poi_' . $id . '_' . $fcode);
		$redis->del('S_Poi_' . $id . '_' . $fcode . ':temp');

		return redirect()->to('vdmOrganization')->with([
			'message'=> 'POI removed !'
		]);
	}

	public function getSmsReport($id)
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$ipaddress = $redis->get('ipaddress');
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$url = 'http://' . $ipaddress . ':9000/getSmsAuditOrg?fcode=' . $fcode . '&orgId=' . $id;
		$url = htmlspecialchars_decode($url);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$report = json_decode($response, true);
		if ($report == null) {
			return redirect()->to('vdmOrganization')->with([
				'message'=> 'No repoert found !'
			]);
		}
		$address = array();
		$details = array();
		$address = $report;
		foreach ($address as $org => $rowId) {
			$temp = array();
			foreach ($rowId as $org1 => $rowId2) {
				if ($org1 != 'year' && $org1 != 'month') {
					$temp = Arr::add($temp, $org1, $rowId2);
				}
			}
			$details = Arr::add($details, $org, $temp);
		}

		return view('vdm.organization.smsReport', ['details' => $details]);
	}


	public function pView($id)
	{
		$username = auth()->id();
		$redis = Redis::connection();

		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$poiViewValue = $redis->zrange('S_Poi_' . $id . '_' . $fcode, 0, -1);

		$userplace = array();
		$radius = null;
		$shortNameList = null;
		try {

			if ($poiViewValue != null) {
				$t = 0;
				foreach ($poiViewValue as $key => $value) {
					$userplace[$t] = $value;
					$t++;
				}

				$vehicleRefData = $redis->hget('H_Organisations_' . $fcode, $id);
				$vehicleRefData = json_decode($vehicleRefData, true);
				$radius = $vehicleRefData['radius'];
			}
		} catch (\Exception $e) {
			logger('----------Exception ---- radius----------->' . $e->getMessage());
		}
		$poiViewData = [
			'orgId' => $id,
			'userplace' => $userplace,
			'radius' => $radius,
		];
		return view('vdm.organization.poiView', $poiViewData);
	}

	public function editAlerts($id)
	{

		$username = auth()->id();
		$redis = Redis::connection();

		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$alertList = array();
		$smsArray = array();
		$emailArray = array();

		try {
			$alertList = $redis->smembers("S_VAMOS_SMS_" . $fcode);
			if ($alertList == null) {
				$alertList = $redis->smembers("S_VAMOS_SMS");
			}
			$email = $redis->hget("H_EMAIL_" . $fcode, $id);
			$sms = $redis->hget("H_SMS_" . $fcode, $id);
			if ($email !== null) {
				$emailArray = explode(",", $email);
			}

			if ($sms !== null) {
				$smsArray = explode(",", $sms);
			}
		} catch (\Exception $e) {
			logger('-------Exception ------- radius----------->' . $e->getMessage());
		}
		sort($alertList);

		$List = [
			"Air Conditioner" => [
				"AC ON/OFF" => "Air Conditioner"
			],
			"Ignition" => [
				"ACC ON/OFF" => "Ignition"
			],
			"Temperature" => [
				"Temperature"=>"Temperature"
			],
			"Route Deviation" => [
				"Route Deviation"=>"Route Deviation"
			],
			"FMS" => [
				"FMS"=>"FMS"
			],
			"Track Alert" => [
				"Track Alert"=>"Track Alert"
			],
			"Location Alerts" => [
				"AREA ENTRY/EXIT"=>"AREA ENTRY/EXIT",
				"AREA ENTRY/EXIT LEVEL 1"=>"AREA ENTRY/EXIT LEVEL 1",
				"AREA ENTRY/EXIT LEVEL 2"=>"AREA ENTRY/EXIT LEVEL 2",
				"AREA ENTRY/EXIT LEVEL 3"=>"AREA ENTRY/EXIT LEVEL 3"
			],
			"Device Alerts" => [
				"BACK TO ONLINE" => "BACK TO ONLINE",
				"VEHICLE STATUS" => "VEHICLE STATUS",
				"OFFLINE" => "VEHICLE OFFLINE"
			],
			"Geofence" => [
				"Site Entry or Site Alert"=>"Site Entry or Site Alert",
				"Site Exit"=>"Site Exit",
				"Site Stoppage"=>"Site Stoppage",
				"Alarm"=>"Alarm",
				"SOS"=>"SOS",
				"Low Battery"=>"Low Battery",
				"Harsh Braking"=>"Harsh Braking",
				"Shock"=>"Shock",
				"Power Cut OFF"=>"Power Cut OFF"
			],
			"Moving" => [
				"Start Moving"=>"Start Moving",
				"Safety Movement"=>"Safety Movement"
			],
			"Overspeed" => [
				"Overspeed"=>"Overspeed",
				"Max Overspeed"=>"Max Overspeed"
			],
			"Parking" => [
				"Parking"=>"Parking",
				"Safety Parking"=>"Safety Parking",
				"Idle"=>"Idle"
			],
			"Fuel" => [
				"Below Fuel"=>"Below Fuel",
				"Fuel Fill"=>"Fuel Fill",
				"Fuel Theft"=>"Fuel Theft",
				"Ongoing Fuel Fill"=>"Ongoing Fuel Fill",
				"Ongoing Fuel Theft"=>"Ongoing Fuel Theft",
				"Daily Diesal Summary"=>"Daily Diesal Summary",
				"Daily Summary"=>"Daily Summary",
				"Fuel Alarm"=>"Fuel Alarm"
			]

		];
		$editAlertsData = [
			'orgId' => $id,
			'alertList' => $alertList,
			'tmp' => 0,
			'List' => $List,
			'smsArray' => $smsArray,
			'emailArray' => $emailArray
		];
		return view('vdm.organization.editAlerts', $editAlertsData);
	}

	public function siteNotification($id)
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$time = array('' => '', '5' => '5 minutes', '10' => '10 minutes', '20' => '20 minutes', '30' => '30 minutes', '40' => '40 minutes', '45' => '45 minutes', '50' => '50 minutes', '60' => '60 minutes', '70' => '70 minutes', '80' => '80 minutes', '90' => '90 minutes', '100' => '100 minutes');
		$enable = array('N' => 'NO', 'Y' => 'YES');
		$alertList = array();
		$smsArray = array();
		$emailArray = array();
		$alertList = $redis->hgetall('H_Site_' . $id . '_' . $fcode);
		$index = 0;
		foreach ($alertList as $key => $value) {
			if ($index % 2 != 0)
				continue; //The key is odd, skip
			//do your stuff
			$siteList[] = $key;
			foreach ($siteList as $keys => $values)
				$site = $redis->hget('H_Site_' . $id . '_' . $fcode, $values);
			$refDataJson1 = json_decode($site, true);
			$time1 = isset($refDataJson1['time']) ? $refDataJson1['time'] : '';
			$enable1 = isset($refDataJson1['enable']) ? $refDataJson1['enable'] : '';
			$times[] = $time1;
			$enables[] = $enable1;
			$index++;
		}
		$ee = [];
		$siteList = isset($siteList) ? $siteList : $ee;
		$counts = count($siteList);
		$times = isset($times) ? $times : $ee;
		$enables = isset($enables) ? $enables : $ee;

		$siteNotificationData = [
			'orgId' => $id,
			'times' => $times,
			'enables' => $enables,
			'counts' => $counts,
			'alertList' => $siteList,
			'tmp' => 0,
			'smsArray' => $smsArray,
			'emailArray' => $emailArray,
			'time' => $time,
			'enable' => $enable
		];
		return view('vdm.organization.siteNotification', $siteNotificationData);
	}

	public function store()
	{
		$username = auth()->id();
		$rules = [
			'organizationId' => 'required',
			'mobile' => 'required'
		];
		$time1 = request()->get('time1');
		$time2 = request()->get('time2');
		$redis = Redis::connection();

		if (!$time1 == null || !$time2 == null) {
			$rules2 = [
				'time2' => 'required',
				'time1' => 'required'
			];
			$rules = array_merge($rules, $rules2);
		}
		$validator = validator()->make(request()->all(), $rules);
		if ($validator->fails()) {
			return redirect()->back()->withInput()->withErrors($validator);
		} else {
			// store
			$time1 = request()->get('time1');
			$time2 = request()->get('time2');

			$organizationId = request()->get('organizationId');
			$organizationId = str_replace(' ', '_', $organizationId);
			$organizationId = str_replace('.', '_', $organizationId);
			$description = request()->get('description');
			$email = request()->get('email');
			$description = request()->get('description');
			$address = request()->get('address');
			$live = request()->get('live');
			$mobile = request()->get('mobile');
			$parkingAlert = request()->get('parkingAlert');
			$idleAlert = request()->get('idleAlert');
			$parkDuration = request()->get('parkDuration');
			$idleDuration = request()->get('idleDuration');
			$overspeedalert = request()->get('overspeedalert');
			$startTime = $time1;
			$endTime = $time2;
			$sendGeoFenceSMS = request()->get('sendGeoFenceSMS');
			$sosAlert = request()->get('sosAlert');
			$radius = 0;
			$smsSender = request()->get('smsSender');
			$smsProvider = request()->get('smsProvider');
			$providerUserName = request()->get('providerUserName');
			$providerPassword = request()->get('providerPassword');
			$geofense = request()->get('geofence');
			$safemove = request()->get('safemove');
			$deleteHistoryEod = request()->get('deleteHistoryEod');
			$harshBreak = request()->get('harshBreak');
			$dailySummary = request()->get('dailySummary');
			$dailyDieselSummary = request()->get('dailyDieselSummary');
			$fuelLevelBelow = request()->get('fuelLevelBelow');
			$fuelLevelBelowValue = request()->get('fuelLevelBelowValue');
			$noDataDuration = request()->get('noDataDuration');
			$SchoolGeoFence = request()->get('SchoolGeoFence');
			$escalatesms = request()->get('escalatesms');
			$fuelAlarm = request()->get('fuelAlarm');
			$tripPlannedTime = request()->get('tripPlannedTime');
			$instantFuel = request()->get('instantFuel');
			$excessConsumptionFilter = request()->get('excessConsumptionFilter');
			$smsEntityName = request()->get('smsEntityName');
			$textLocalSmsKey = request()->get('textLocalSmsKey');
			$enableFuelNotification = request()->get('enableFuelNotification');
			$morningEntryStartTime	= request()->get('morningEntryStartTime');
			$morningEntryEndTime	= request()->get('morningEntryEndTime');
			$eveningEntryStartTime	= request()->get('eveningEntryStartTime');
			$eveningEntryEndTime	= request()->get('eveningEntryEndTime');
			$morningExitStartTime	= request()->get('morningExitStartTime');
			$morningExitEndTime		= request()->get('morningExitEndTime');
			$eveningExitStartTime	= request()->get('eveningExitStartTime');
			$eveningExitEndTime		= request()->get('eveningExitEndTime');
			$overSpeedDuration		= request()->get('overSpeedDuration');
			$fuelAlertGeoFence = request()->get('fuelAlertGeoFence');
			$routeDeviation = request()->get('routeDeviation');

			$companymap = $redis->hget('H_Org_Company_Map', $organizationId);
			if ($companymap != null) {
				return redirect()->back()->withInput()->withErrors('Organiztaion is already exist. Please choose another one');
			}
			$isRfid = request()->get('isRfid');
			if ($isRfid == 'yes') {
				$PickupStartTime = request()->get('PickupStartTime');
				$PickupEndTime	= request()->get('PickupEndTime');
				$DropStartTime	= request()->get('DropStartTime');
				$DropEndTime	= request()->get('DropEndTime');
				if (strtotime($PickupStartTime) <= strtotime($PickupEndTime) && strtotime($DropStartTime) <= strtotime($DropEndTime)) {
				} else {
					return redirect()->back()->withErrors('Invalid Pick Up and Drop Time !')->withInput();
				}
			} else {
				$PickupStartTime = '';
				$PickupEndTime	= '';
				$DropStartTime	= '';
				$DropEndTime	= '';
			}
			// check null in yes or no
			$yesOrNoFields = InputController::nullCheckArr([
				'live' => $live,
				'parkingAlert' => $parkingAlert,
				'idleAlert' => $idleAlert,
				'sosAlert' => $sosAlert,
				'overspeedalert' => $overspeedalert,
				'harshBreak' => $harshBreak,
				'dailySummary' => $dailySummary,
				'safemove' => $safemove,
				'escalatesms' => $escalatesms,
				'tripPlannedTime' => $tripPlannedTime,
				'dailyDieselSummary' => $dailyDieselSummary,
				'fuelLevelBelow' => $fuelLevelBelow,
				'fuelAlarm' => $fuelAlarm,
				'instantFuel' => $instantFuel,
				'excessConsumptionFilter' => $excessConsumptionFilter,
				'isRfid' => $isRfid,
				'SchoolGeoFence' => $SchoolGeoFence,
				'deleteHistoryEod' => $deleteHistoryEod,
			], 'no');

			$orgDataArr = InputController::nullCheckArr([
				'description' => $description,
				'email' => $email,
				'address' => $address,
				'mobile' => $mobile,
				'startTime' => $startTime,
				'endTime'  => $endTime,
				'parkDuration' => $parkDuration,
				'idleDuration' => $idleDuration,
				'sendGeoFenceSMS' => $sendGeoFenceSMS,
				'radius' => $radius,
				'smsSender' => $smsSender,
				'smsProvider' => $smsProvider,
				'providerUserName' => $providerUserName,
				'providerPassword' => $providerPassword,
				'geofense' => $geofense,
				'fuelLevelBelowValue' => $fuelLevelBelowValue,
				'noDataDuration' => $noDataDuration,
				'morningEntryStartTime'	=> $morningEntryStartTime,
				'morningEntryEndTime'	=> $morningEntryEndTime,
				'eveningEntryStartTime'	=> $eveningEntryStartTime,
				'eveningEntryEndTime'	=> $eveningEntryEndTime,
				'morningExitStartTime'	=> $morningExitStartTime,
				'morningExitEndTime'	=> $morningExitEndTime,
				'eveningExitStartTime'	=> $eveningExitStartTime,
				'eveningExitEndTime'	=> $eveningExitEndTime,
				'PickupStartTime'		=>	$PickupStartTime,
				'PickupEndTime'			=>	$PickupEndTime,
				'DropStartTime'			=>	$DropStartTime,
				'DropEndTime'			=>	$DropEndTime,
				'enableFuelNotification'=> $enableFuelNotification,
				'smsEntityName' 		=> $smsEntityName,
				'textLocalSmsKey' 		=> $textLocalSmsKey,
				'overSpeedDuration'		=>$overSpeedDuration,
				'fuelAlertGeoFence'=>$fuelAlertGeoFence,
				'routeDeviation'=>$routeDeviation
			]);
			$orgDataArr = array_merge($orgDataArr, $yesOrNoFields);
			$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
			$orgDataJson = json_encode($orgDataArr);
			
			try {
                $mysqlDetails = [
                    'userIpAddress' => session()->get('userIP'),
                    'userName' => $username,
                    'name' => "$organizationId",
                    'type' => config()->get('constant.organization'),
                    'status' => config()->get('constant.created'),
                    'oldData' => "",
                    'newData' => $orgDataJson,
                ];
                AuditNewController::updateAudit($mysqlDetails);
            } catch (Exception $e) {
                logger('Exception_' . $organizationId . ' audit org error --->' . $e->getMessage() . ' line--->' . $e->getLine());
            }

			$redis->sadd('S_Organisations_' . $fcode, $organizationId);

			if (session()->get('cur') == 'dealer') {
				$redis->sadd('S_Organisations_Dealer_' . $username . '_' . $fcode, $organizationId);
			} else if (session()->get('cur') == 'admin') {
				$redis->sadd('S_Organisations_Admin_' . $fcode, $organizationId);
			}

			$redis->hset('H_Organisations_' . $fcode, $organizationId, $orgDataJson);
			$redis->hset('H_Org_Company_Map', $organizationId, $fcode);
			
			return redirect()->to('vdmOrganization')->with([
				'alert-class'=> 'alert-success',
				'message'=> "Successfully created $organizationId !"
			]);
		}
	}

	public function index()
	{

		$username = auth()->id();

		$redis = Redis::connection();

		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$orgListId = 'S_Organisations_' . $fcode;

		if (session()->get('cur') == 'dealer') {
			$orgListId = 'S_Organisations_Dealer_' . $username . '_' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$orgListId = 'S_Organisations_Admin_' . $fcode;
		}

		$orgList = $redis->smembers($orgListId);

		$orgArray = array();
		$i = 1;
		sort($orgList);
		foreach ($orgList as $org) {
			$org1 = strtoupper($org);
			if ($org == $org1) {
				$orgArray = Arr::add($orgArray, $i, $org);
			}
			$i = $i + 1;
		}
		return view('vdm.organization.index', [
			'orgList' => $orgArray
		]);
	}

	public function Search()
	{
		$orgLis = [];
		return view('vdm.organization.orgScan', ['orgList' => $orgLis]);
	}

	//advance search organization
	public function Scan()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$orgListId = 'S_Organisations_' . $fcode;
		$text_word = strtoupper(request()->get('text_word'));

		if (session()->get('cur') == 'dealer') {
			$orgListId = 'S_Organisations_Dealer_' . $username . '_' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$orgListId = 'S_Organisations_Admin_' . $fcode;
		}

		$cou = $redis->SCARD($orgListId);
		$orgLi = $redis->sScan($orgListId, 0,  'count', $cou, 'match', '*' . $text_word . '*');

		return view('vdm.organization.orgScan', ['orgList' => $orgLi[1]]);
	}

	public function destroy($id)
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$organizationId = $id;
		$redis->srem('S_Organisations_' . $fcode, $id);
		$redis->srem('S_Organisations_Dealer_' . $username . '_' . $fcode, $id);
		$redis->srem('S_Organisations_Admin_' . $fcode, $id);
		$redis->hdel("H_EMAIL_" . $fcode, $id);
		$redis->hdel("H_SMS_" . $fcode, $id);
		$orgData =  $redis->hget('H_Organisations_' . $fcode, $id);
		$orgDataArr = json_decode($orgData, TRUE);
		try {
			
			$mysqlDetails = [
				'userIpAddress' => session()->get('userIP'),
				'userName' => $username,
				'type' => config()->get('constant.organization'),
				'name' => $organizationId,
				'status' => config()->get('constant.deleted'),
				'oldData' => json_encode($orgDataArr),
				'newData' => ""
			];
			AuditNewController::updateAudit($mysqlDetails);
		} catch (Exception $e) {
			AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
			logger('Exception_' . $organizationId . ' DeleteAuditOrg error -->' . $e->getMessage() . ' line -->' . $e->getLine());
			logger('Error inside AuditOrg Delete' . $e->getMessage());
		}
		$redis->hdel('H_Organisations_' . $fcode, $id); //Orgnizations
		$redis->hdel('H_Org_Company_Map', $id);
		$userList = $redis->smembers('S_Users_' . $fcode);

		$vehicleSet = $redis->smembers('S_Vehicles_' . $id . '_' . $fcode);
		foreach ($vehicleSet as $list) {
			$refData = $redis->hget('H_RefData_' . $fcode, $list);

			$refData = json_decode($refData, true);

			$redis->del('L_Suggest_' . $refData['shortName'] . '_' . $refData['orgId'] . '_' . $fcode);

			$redis->hdel('H_Stopseq_' . $refData['orgId'] . '_' . $fcode, $refData['shortName'] . ':' . 'morning');
			$redis->hdel('H_Stopseq_' . $refData['orgId'] . '_' . $fcode, $refData['shortName'] . ':' . 'evening');

			try {

				$redis->del('L_Suggest_' . $refData['altShortName'] . '_' . $refData['orgId'] . '_' . $fcode);

				$redis->hdel('H_Stopseq_' . $refData['orgId'] . '_' . $fcode, $refData['altShortName'] . ':' . 'morning');
				$redis->hdel('H_Stopseq_' . $refData['orgId'] . '_' . $fcode, $refData['altShortName'] . ':' . 'evening');
			} catch (\Exception $e) {
			}
			try {
				$vehiclemake = $refData['vehicleMake'];
			} catch (\Exception $e) {
				$vehiclemake = ' ';
			}

			$refDataArr = array(
				'orgId' => 'Default',
				'altShortName' => 'Default',
				'vehicleMake' => $vehiclemake,
			);

			$refDataArr = array_merge($refData,$refDataArr);
			$refDataJson = json_encode($refDataArr);
			$redis->hset('H_RefData_' . $fcode, $list, $refDataJson);

			$deviceId1 = $refData['deviceId'];
			$pub = $redis->PUBLISH('sms:topicNetty', $deviceId1);
			$deviceId = strtoupper($deviceId1);
			$shortNameOld1 = $refData['shortName'];
			$shortNameOld = strtoupper($shortNameOld1);
			$orgIdOld1 = $refData['orgId'];
			$orgIdOld = strtoupper($orgIdOld1);
			$gpsSimNoOld = $refData['gpsSimNo'];
			$orgIdNew = 'DEFAULT';
			$OWN1 = $refData['OWN'];
			$OWN = strtoupper($OWN1);
			$redis->hdel('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $list . ':' . $deviceId . ':' . $shortNameOld . ':' . $orgIdOld . ':' . $gpsSimNoOld . ':' . $OWN);
			$redis->hset('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $list . ':' . $deviceId . ':' . $shortNameOld . ':' . $orgIdNew . ':' . $gpsSimNoOld . ':' . $OWN, $list);
		}
		$redis->del('H_Poi_' . $organizationId . '_' . $fcode);
		$redis->del('H_Bus_Stops_' . $organizationId . '_' . $fcode);

		return redirect()->to('vdmOrganization')->with([
			'message'=> "Successfully deleted  $id !"
		]);
	}

	public function getOrgSite()
	{
		//  HKEYS H_Site_MSS_SMP
		$username = auth()->id();;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$orgId = request()->get('orgId');
		$key = 'H_Site_' . $orgId . '_' . $fcode;
		$getSite = $redis->hkeys($key);

		return response()->json($getSite);
	}

	public function edit($id)
	{

		$username = auth()->id();;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$jsonData =  $redis->hget('H_Organisations_' . $fcode, $id);
		$orgDataArr = json_decode($jsonData, true);

		$email = isset($orgDataArr['email']) ? $orgDataArr['email'] : ' ';
		$address = isset($orgDataArr['address']) ? $orgDataArr['address'] : ' ';
		$description = isset($orgDataArr['description']) ? $orgDataArr['description'] : ' ';
		$mobile = isset($orgDataArr['mobile']) ? $orgDataArr['mobile'] : ' ';
		$time1 =  isset($orgDataArr['startTime']) ? $orgDataArr['startTime'] : ' ';
		$time2 = isset($orgDataArr['endTime']) ? $orgDataArr['endTime'] : ' ';
		$etc = isset($orgDataArr['etc']) ? $orgDataArr['etc'] : ' ';
		$mtc = isset($orgDataArr['mtc']) ? $orgDataArr['mtc'] : ' ';
		$atc = isset($orgDataArr['atc']) ? $orgDataArr['atc'] : ' ';
		$parkDuration = isset($orgDataArr['parkDuration']) ? $orgDataArr['parkDuration'] : '';
		$idleDuration = isset($orgDataArr['idleDuration']) ? $orgDataArr['idleDuration'] : '';
		$parkingAlert = isset($orgDataArr['parkingAlert']) ? $orgDataArr['parkingAlert'] : 'no';
		$idleAlert = isset($orgDataArr['idleAlert']) ? $orgDataArr['idleAlert'] : '';
		$overspeedalert = isset($orgDataArr['overspeedalert']) ? $orgDataArr['overspeedalert'] : '';
		$SchoolGeoFence = isset($orgDataArr['SchoolGeoFence']) ? $orgDataArr['SchoolGeoFence'] : 'no';
		$sendGeoFenceSMS = isset($orgDataArr['sendGeoFenceSMS']) ? $orgDataArr['sendGeoFenceSMS'] : '';
		$radius = isset($orgDataArr['radius']) ? $orgDataArr['radius'] : '';
		$smsSender = isset($orgDataArr['smsSender']) ? $orgDataArr['smsSender'] : '';
		$sosAlert = isset($orgDataArr['sosAlert']) ? $orgDataArr['sosAlert'] : '';
		$live = isset($orgDataArr['live']) ? $orgDataArr['live'] : 'no';
		$smsProvider = isset($orgDataArr['smsProvider']) ? $orgDataArr['smsProvider'] : 'nill';
		$providerUserName = isset($orgDataArr['providerUserName']) ? $orgDataArr['providerUserName'] : '';
		$providerPassword = isset($orgDataArr['providerPassword']) ? $orgDataArr['providerPassword'] : '';
		$geofence = isset($orgDataArr['geofence']) ? $orgDataArr['geofence'] : 'nill';
		$safemove = isset($orgDataArr['safemove']) ? $orgDataArr['safemove'] : 'no';
		$smsPattern = isset($orgDataArr['schoolPattern']) ? $orgDataArr['schoolPattern'] : 'nill';
		$deleteHistoryEod = isset($orgDataArr['deleteHistoryEod']) ? $orgDataArr['deleteHistoryEod'] : 'no';
		$harshBreak = isset($orgDataArr['harshBreak']) ? $orgDataArr['harshBreak'] : 'no';
		$dailySummary = isset($orgDataArr['dailySummary']) ? $orgDataArr['dailySummary'] : 'no';
		$dailyDieselSummary = isset($orgDataArr['dailyDieselSummary']) ? $orgDataArr['dailyDieselSummary'] : 'no';
		$fuelLevelBelow = isset($orgDataArr['fuelLevelBelow']) ? $orgDataArr['fuelLevelBelow'] : 'no';
		$fuelLevelBelowValue = isset($orgDataArr['fuelLevelBelowValue']) ? $orgDataArr['fuelLevelBelowValue'] : '';
		$noDataDuration = isset($orgDataArr['noDataDuration']) ? $orgDataArr['noDataDuration'] : '';
		$morningEntryStartTime = isset($orgDataArr['morningEntryStartTime']) ? $orgDataArr['morningEntryStartTime'] : '';
		$morningEntryEndTime = isset($orgDataArr['morningEntryEndTime']) ? $orgDataArr['morningEntryEndTime'] : '';
		$eveningEntryStartTime = isset($orgDataArr['eveningEntryStartTime']) ? $orgDataArr['eveningEntryStartTime'] : '';
		$eveningEntryEndTime = isset($orgDataArr['eveningEntryEndTime']) ? $orgDataArr['eveningEntryEndTime'] : '';
		$morningExitStartTime = isset($orgDataArr['morningExitStartTime']) ? $orgDataArr['morningExitStartTime'] : '';
		$morningExitEndTime = isset($orgDataArr['morningExitEndTime']) ? $orgDataArr['morningExitEndTime'] : '';
		$eveningExitStartTime = isset($orgDataArr['eveningExitStartTime']) ? $orgDataArr['eveningExitStartTime'] : '';
		$eveningExitEndTime = isset($orgDataArr['eveningExitEndTime']) ? $orgDataArr['eveningExitEndTime'] : '';
		$isRfid = isset($orgDataArr['isRfid']) ? $orgDataArr['isRfid'] : 'no';
		$PickupStartTime = isset($orgDataArr['PickupStartTime']) ? $orgDataArr['PickupStartTime'] : '';
		$PickupEndTime = isset($orgDataArr['PickupEndTime']) ? $orgDataArr['PickupEndTime'] : '';
		$DropStartTime = isset($orgDataArr['DropStartTime']) ? $orgDataArr['DropStartTime'] : '';
		$DropEndTime =  isset($orgDataArr['DropEndTime']) ? $orgDataArr['DropEndTime'] : '';
		$escalatesms  = isset($orgDataArr['escalationsms']) ? $orgDataArr['escalationsms'] : 'no';
		$fuelAlarm = isset($orgDataArr['fuelAlarm']) ? $orgDataArr['fuelAlarm'] : 'no';
		$fuelApproximate = isset($orgDataArr['fuelApproximate']) ? $orgDataArr['fuelApproximate'] : '';
		$tripPlannedTime = isset($orgDataArr['tripPlannedTime']) ? $orgDataArr['tripPlannedTime'] : 'no';
		$instantFuel = isset($orgDataArr['instantFuel']) ? $orgDataArr['instantFuel'] : 'no';
		$delayTimeAlert = isset($orgDataArr['delayTimeAlert']) ? $orgDataArr['delayTimeAlert'] : 'no';
		$enableFuelNotification = isset($orgDataArr['enableFuelNotification']) ? $orgDataArr['enableFuelNotification'] : 'always';
		$sendDataToGovtip = isset($orgDataArr['sendDataToGovtip']) ? $orgDataArr['sendDataToGovtip'] : 'no';
		$ipAddress = isset($orgDataArr['ipAddress']) ? $orgDataArr['ipAddress'] : '';
		$ipPort = isset($orgDataArr['ipPort']) ? $orgDataArr['ipPort'] : '';
		$excessConsumptionFilter = isset($orgDataArr['excessConsumptionFilter']) ? $orgDataArr['excessConsumptionFilter'] : 'no';
		$smsEntityName = isset($orgDataArr['smsEntityName']) ? $orgDataArr['smsEntityName'] : '';
		// textLocalSmsKey is based on SMS Provider
		$textLocalSmsKey = isset($orgDataArr['textLocalSmsKey']) ? $orgDataArr['textLocalSmsKey'] : '';
		$overSpeedAlertDuration = isset($orgDataArr['overSpeedAlertDuration']) ? $orgDataArr['overSpeedAlertDuration'] : '0';
		$seperateDetails = isset($orgDataArr['seperateDetails']) ? $orgDataArr['seperateDetails'] : 'no';
		$overSpeedDuration = isset($orgDataArr['overSpeedDuration']) ? $orgDataArr['overSpeedDuration'] : '';
		$fuelAlertGeoFence = isset($orgDataArr['fuelAlertGeoFence']) ? $orgDataArr['fuelAlertGeoFence'] : 'default';
		$powerCutOffAlarmDuration = isset($orgDataArr['powerCutOffAlarmDuration']) ? $orgDataArr['powerCutOffAlarmDuration'] : '0';
		$routeDeviation = isset($orgDataArr['routeDeviation']) ? $orgDataArr['routeDeviation'] : '';
		$EndofDayDetails = $orgDataArr['EndofDayDetails'] ?? 'no';

		$reportsDataJson = $redis->hget('H_Org_ShiftTime_'.$fcode,$id);
		$reportsData = [];
		if(isset($reportsDataJson)){
			$reportsData = json_decode($reportsDataJson,true);
		}


		$shiftDataJson = $redis->hget('H_ShiftTime_' . $fcode, $id);
		$shiftData = [];
		if (isset($shiftDataJson)) {
			$shiftData = json_decode($shiftDataJson, true);
		}
		$key = 'H_Site_' . $id . '_' . $fcode;
		$getSites = $redis->hkeys($key);



		if ($fuelApproximate == 'no') {
			$fuelApproximate = '';
		}
		$address1 = array();
		$place = array();
		$address1 = $redis->hgetall('H_Poi_' . $id . '_' . $fcode);
		foreach ($address1 as $org => $rowId) {
			$place = Arr::add($address1, $org, $org);
		}
		$i = 0;
		$editData = [
			'organizationId' => $id,
			'mobile' => $mobile,
			'smsEntityName' => $smsEntityName,
			'description' => $description,
			'address' => $address,
			'textLocalSmsKey' => $textLocalSmsKey,
			'seperateDetails' => $seperateDetails,
			'overSpeedAlertDuration' => $overSpeedAlertDuration,
			'excessConsumptionFilter' => $excessConsumptionFilter,
			'enableFuelNotification' => $enableFuelNotification,
			'email' => $email,
			'place' => $place,
			'i' => $i,
			'time1' => $time1,
			'time2' => $time2,
			'atc' => $atc,
			'etc' => $etc,
			'mtc' => $mtc,
			'idleAlert' => $idleAlert,
			'parkingAlert' => $parkingAlert,
			'idleDuration' => $idleDuration,
			'parkDuration' => $parkDuration,
			'overspeedalert' => $overspeedalert,
			'SchoolGeoFence' => $SchoolGeoFence,
			'sendGeoFenceSMS' => $sendGeoFenceSMS,
			'radius' => $radius,
			'smsSender' => $smsSender,
			'sosAlert' => $sosAlert,
			'live' => $live,
			'smsProvider' => $smsProvider,
			'providerUserName' => $providerUserName,
			'providerPassword' => $providerPassword,
			'smsP' => VdmFranchiseController::smsP(),
			'smsPattern' => $smsPattern,
			'geofence' => $geofence,
			'safemove' => $safemove,
			'deleteHistoryEod' => $deleteHistoryEod,
			'harshBreak' => $harshBreak,
			'dailySummary' => $dailySummary,
			'dailyDieselSummary' => $dailyDieselSummary,
			'fuelLevelBelow' => $fuelLevelBelow,
			'fuelLevelBelowValue' => $fuelLevelBelowValue,
			'noDataDuration' => $noDataDuration,
			'morningEntryStartTime' => $morningEntryStartTime,
			'morningEntryEndTime' => $morningEntryEndTime,
			'eveningEntryStartTime' => $eveningEntryStartTime,
			'eveningEntryEndTime' => $eveningEntryEndTime,
			'morningExitStartTime' => $morningExitStartTime,
			'morningExitEndTime' => $morningExitEndTime,
			'eveningExitStartTime' => $eveningExitStartTime,
			'eveningExitEndTime' => $eveningExitEndTime,
			'isRfid' => $isRfid,
			'PickupStartTime' => $PickupStartTime,
			'PickupEndTime' => $PickupEndTime,
			'DropStartTime' => $DropStartTime,
			'DropEndTime' => $DropEndTime,
			'escalatesms' => $escalatesms,
			'fuelAlarm' => $fuelAlarm,
			'fuelApproximate' => $fuelApproximate,
			'tripPlannedTime' => $tripPlannedTime,
			'instantFuel' => $instantFuel,
			'delayTimeAlert' => $delayTimeAlert,
			'sendDataToGovtip' => $sendDataToGovtip,
			'ipAddress' => $ipAddress,
			'ipPort' => $ipPort,
			'shiftData' => $shiftData,
			'getSites' => $getSites,
			'fuelAlertGeoFence'=>$fuelAlertGeoFence,
			'powerCutOffAlarmDuration'=>$powerCutOffAlarmDuration,
			'overSpeedDuration'=>$overSpeedDuration,
			'routeDeviation'=>$routeDeviation,
			'EndofDayDetails'=>$EndofDayDetails,
			'reportsData'=>$reportsData
		];
		// sms config
		$durationList = $redis->hkeys('H_EscalationConfig_' . $id . '_' . $fcode);
		$smsmobile = null;
		foreach ($durationList as $key => $dur) {
			$mobilenumber = $redis->hget('H_EscalationConfig_' . $id . '_' . $fcode, $dur);
			$smsmobile = Arr::add($smsmobile, $dur, $mobilenumber);
		}
		$smsconfigData = [
			'orgId' => $id,
			'smsmobile' => $smsmobile,
			'duration' => $durationList
		];
		return view('vdm.organization.edit', $editData,$smsconfigData);
	}

	public function update($id)
	{

		$username = auth()->id();
		//no rules as of now
		$rules = array();
		$time1 = request()->get('time1');
		$time2 = request()->get('time2');
		if (!$time1 == null || !$time2 == null) {
			$rules2 = [
				'time2' => 'required',
				'time1' => 'required'
			];
			$rules = array_merge($rules, $rules2);
		}
		$validator = validator()->make(request()->all(), $rules);
		if ($validator->fails()) {
			return redirect()->back()->withInput()->withErrors($validator);
		} else {
			// store
			$organizationId = $id;
			$redis = Redis::connection();
			$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
			$redis->sadd('S_Organisations_' . $fcode, $organizationId);
			$orgDataJson1 = $redis->hget('H_Organisations_' . $fcode, $organizationId);
			$oldOrgData = $orgDataJson1;
			$orgDataJson1 = json_decode($orgDataJson1, true);

			$radius = isset($orgDataJson1['radius']) ? $orgDataJson1['radius'] : '';

			$mobileOld = isset($orgDataJson1['mobile']) ? $orgDataJson1['mobile'] : ' ';
			$emailOld = isset($orgDataJson1['email']) ? $orgDataJson1['email'] : ' ';
			$addressOld = isset($orgDataJson1['address']) ? $orgDataJson1['address'] : ' ';
			$descriptionOld = isset($orgDataJson1['description']) ? $orgDataJson1['description'] : ' ';
			$startTimeOld = isset($orgDataJson1['startTime']) ? $orgDataJson1['startTime'] : ' ';
			$endTimeOld = isset($orgDataJson1['endTime']) ? $orgDataJson1['endTime'] : ' ';
			$parkDurationOld = isset($orgDataJson1['parkDuration']) ? $orgDataJson1['parkDuration'] : '';
			$idleDurationOld = isset($orgDataJson1['idleDuration']) ? $orgDataJson1['idleDuration'] : '';
			$parkingAlertOld = isset($orgDataJson1['parkingAlert']) ? $orgDataJson1['parkingAlert'] : 'no';
			$idleAlertOld = isset($orgDataJson1['idleAlert']) ? $orgDataJson1['idleAlert'] : '';
			$overspeedalertOld = isset($orgDataJson1['overspeedalert']) ? $orgDataJson1['overspeedalert'] : '';
			$SchoolGeoFence = isset($orgDataJson1['SchoolGeoFence']) ? $orgDataJson1['SchoolGeoFence'] : 'no';
			$sendGeoFenceSMSOld = isset($orgDataJson1['sendGeoFenceSMS']) ? $orgDataJson1['sendGeoFenceSMS'] : '';
			$radiusOld = isset($orgDataJson1['radius']) ? $orgDataJson1['radius'] : '';
			$smsSenderOld = isset($orgDataJson1['smsSender']) ? $orgDataJson1['smsSender'] : '';
			$sosAlertOld = isset($orgDataJson1['sosAlert']) ? $orgDataJson1['sosAlert'] : '';
			$liveOld = isset($orgDataJson1['live']) ? $orgDataJson1['live'] : 'no';
			$smsProviderOld = isset($orgDataJson1['smsProvider']) ? $orgDataJson1['smsProvider'] : 'nill';
			$providerUserNameOld = isset($orgDataJson1['providerUserName']) ? $orgDataJson1['providerUserName'] : '';
			$providerPasswordOld = isset($orgDataJson1['providerPassword']) ? $orgDataJson1['providerPassword'] : '';
			$schoolPatternOld = isset($orgDataJson1['schoolPattern']) ? $orgDataJson1['schoolPattern'] : 'nill';
			$geofenceOld = isset($orgDataJson1['geofence']) ? $orgDataJson1['geofence'] : 'no';
			$deleteHistoryEodOld = isset($orgDataJson1['deleteHistoryEod']) ? $orgDataJson1['deleteHistoryEod'] : 'no';
			$harshBreakOld = isset($orgDataJson1['harshBreak']) ? $orgDataJson1['harshBreak'] : 'no';
			$dailySummaryOld = isset($orgDataJson1['dailySummary']) ? $orgDataJson1['dailySummary'] : 'no';
			$dailyDieselSummaryOld = isset($orgDataJson1['dailyDieselSummary']) ? $orgDataJson1['dailyDieselSummary'] : 'no';
			$fuelLevelBelowOld = isset($orgDataJson1['fuelLevelBelow']) ? $orgDataJson1['fuelLevelBelow'] : 'no';
			$fuelLevelBelowValueOld = isset($orgDataJson1['fuelLevelBelowValue']) ? $orgDataJson1['fuelLevelBelowValue'] : '';
			$noDataDurationOld = isset($orgDataJson1['noDataDuration']) ? $orgDataJson1['noDataDuration'] : '';
			$morningEntryStartTime = isset($orgDataJson1['morningEntryStartTime']) ? $orgDataJson1['morningEntryStartTime'] : '';
			$morningEntryEndTime = isset($orgDataJson1['morningEntryEndTime']) ? $orgDataJson1['morningEntryEndTime'] : '';
			$eveningEntryStartTime = isset($orgDataJson1['eveningEntryStartTime']) ? $orgDataJson1['eveningEntryStartTime'] : '';
			$eveningEntryEndTime = isset($orgDataJson1['eveningEntryEndTime']) ? $orgDataJson1['eveningEntryEndTime'] : '';
			$morningExitStartTime = isset($orgDataJson1['morningExitStartTime']) ? $orgDataJson1['morningExitStartTime'] : '';
			$morningExitEndTime = isset($oorgDataJson1['morningExitEndTime']) ? $orgDataJson1['morningExitEndTime'] : '';
			$eveningExitStartTime = isset($orgDataJson1['eveningExitStartTime']) ? $orgDataJson1['eveningExitStartTime'] : '';
			$eveningExitEndTime = isset($orgDataJson1['eveningExitEndTime']) ? $orgDataJson1['eveningExitEndTime'] : '';

			$isRfidOld = isset($orgDataArr['isRfid']) ? $orgDataArr['isRfid'] : 'No';
			$PickupStartTimeOld = isset($orgDataArr['PickupStartTime']) ? $orgDataArr['PickupStartTime'] : '';
			$PickupEndTimeOld = isset($orgDataArr['PickupEndTime']) ? $orgDataArr['PickupEndTime'] : '';
			$DropStartTimeOld = isset($orgDataArr['DropStartTime']) ? $orgDataArr['DropStartTime'] : '';
			$DropEndTimeOld = isset($orgDataArr['DropEndTime']) ? $orgDataArr['DropEndTime'] : '';
			$escalatesmsold = isset($orgDataArr['escalationsms']) ? $orgDataArr['escalationsms'] : 'no';
			$fuelAlarmold = isset($orgDataArr['fuelAlarm']) ? $orgDataArr['fuelAlarm'] : 'no';
			$fuelApproximateOld = isset($orgDataArr['fuelApproximate']) ? $orgDataArr['fuelApproximate'] : 'no';
			$tripPlannedTimeOld = isset($orgDataArr['tripPlannedTime']) ? $orgDataArr['tripPlannedTime'] : 'no';
			$instantFuelOld = isset($orgDataArr['instantFuel']) ? $orgDataArr['instantFuel'] : 'no';
			$delayTimeAlertOld = isset($orgDataArr['delayTimeAlert']) ? $orgDataArr['delayTimeAlert'] : 'no';
			$enableFuelNotificationOld = isset($orgDataArr['enableFuelNotification']) ? $orgDataArr['enableFuelNotification'] : 'always';
			$sendDataToGovtipOld = isset($orgDataArr['sendDataToGovtip']) ? $orgDataArr['sendDataToGovtip'] : 'no';
			$ipAddressOld = isset($orgDataArr['ipAddress']) ? $orgDataArr['ipAddress'] : '';
			$ipPortOld = isset($orgDataArr['ipPort']) ? $orgDataArr['ipPort'] : '';
			$seperateDetailsOld = isset($orgDataArr['seperateDetails']) ? $orgDataArr['seperateDetails'] : 'no';
			$EndofDayDetailsOld = isset($orgDataArr['EndofDayDetails']) ? $orgDataArr['EndofDayDetails'] :"no";
			$excessConsumptionFilter = isset($orgDataArr['excessConsumptionFilter']) ? $orgDataArr['excessConsumptionFilter'] : 'no';
			$smsEntityName = isset($orgDataArr['smsEntityName']) ? $orgDataArr['smsEntityName'] : '';
			$textLocalSmsKey = isset($orgDataArr['textLocalSmsKey']) ? $orgDataArr['textLocalSmsKey'] : '';


			$mobile      = request()->get('mobile');
			$email      = request()->get('email');
			$description      = request()->get('description');
			$address      = request()->get('address');
			$live = request()->get('live');
			$parkingAlert = request()->get('parkingAlert')??"no";
			$idleAlert = request()->get('idleAlert')??"no";
			$sosAlert = request()->get('sosAlert')??'no';
			$parkDuration = request()->get('parkDuration');
			$idleDuration = request()->get('idleDuration');
			$overspeedalert = request()->get('overspeedalert')??"no";
			$SchoolGeoFence = request()->get('SchoolGeoFence')?? "no";
			$sendGeoFenceSMS = request()->get('sendGeoFenceSMS');

			$smsSender = request()->get('smsSender');
			$smsProvider = request()->get('smsProvider');
			$providerUserName = request()->get('providerUserName');
			$providerPassword = request()->get('providerPassword');
			$smsPattern = request()->get('smsPattern');
			$geofence = request()->get('geofence');
			$safemove = request()->get('safemove') ?? "no";
			$deleteHistoryEod = request()->get('deleteHistoryEod') ?? "no";
			$harshBreak = request()->get('harshBreak') ?? "no";
			$dailySummary = request()->get('dailySummary') ?? "no";
			$dailyDieselSummary = request()->get('dailyDieselSummary') ?? "no";
			$fuelLevelBelow = request()->get('fuelLevelBelow');
			$fuelLevelBelowValue = request()->get('fuelLevelBelowValue');
			$noDataDuration = request()->get('noDataDuration');
			$escalatesms = request()->get('escalatesms') ?? "no";
			$fuelAlarm = request()->get('fuelAlarm') ?? "no";
			$fuelApproximate = request()->get('fuelApproximate') ?? "no";
			$tripPlannedTime = request()->get('tripPlannedTime') ?? "no";
			$instantFuel = request()->get('instantFuel') ?? "no";
			$sendDataToGovtip = request()->get('sendDataToGovtip')??"no";
			$ipAddress = request()->get('ipAddress');
			$ipPort = request()->get('ipPort');
			$excessConsumptionFilter = request()->get('excessConsumptionFilter') ?? "no";
			$smsEntityName = request()->get('smsEntityName');
			$textLocalSmsKey = request()->get('textLocalSmsKey');
			$seperateDetails = request()->get('seperateDetails')??"no";
			$shiftDataCount = request()->get("shiftDataCount");
			$routeDeviation = request()->get("routeDeviation");
			$fuelAlertGeoFence=request()->get('fuelAlertGeoFence');
			$EndofDayDetails = request()->get('EndofDayDetails') ?? 'no';
			$EndofDayDetailsCount = request()->get('EndofDayDetailsCount');
			$shiftData = [];
			// $oldShiftData1 = $redis->hget('H_ShiftTime_' . $fcode, $id);
			// if($oldShiftData1 ==  null  || $oldShiftData1 == ""){
			// 	$oldShiftData1=[];
			// }else{
			// 	$oldShiftData1 = json_decode($oldShiftData1,true);
			// }
			$redis->hdel('H_ShiftTime_' . $fcode, $id);
			if ($seperateDetails == 'yes') {
				for ($i = 0; $i <= $shiftDataCount; $i++) {
					$startTime = request()->get('startTime' . $i);
					if (is_null($startTime)) continue;
					$startTime = isset($startTime) ? $startTime : '';

					$shiftName = request()->get('shiftname' . $i);
					$shiftName = isset($shiftName) ? $shiftName : '';

					$endTime = request()->get('endTime' . $i);
					if (is_null($endTime)) continue;
					$endTime = isset($endTime) ? $endTime : '';

					$sitenameArr = [];
					$sitenameArr = request()->get('sitename' . $i);
					$sitenameArr = isset($sitenameArr) ? $sitenameArr : [];

					// dd($sitenameArr);

					$sitenames = join(',', $sitenameArr);
					$shiftData[] = [
						'shiftName' => $shiftName,
						'startTime' => $startTime,
						'endTime' => $endTime,
						'siteName' => $sitenames
					];
				}

				$shiftDataJson = json_encode($shiftData);

				$redis->hset('H_ShiftTime_' . $fcode, $id, $shiftDataJson);
				$redis->sadd('S_ShiftTime_' . $fcode, $id);
			} else {
				$redis->hdel('H_ShiftTime_' . $fcode, $id);
				$redis->srem('S_ShiftTime_' . $fcode, $id);
			}
			// logger([$oldShiftData1,$shiftData]);
			// $oldShiftData = array_diff($oldShiftData1,$shiftData);
			// $newShiftData = array_diff($shiftData,$oldShiftData1);

			// 
			// $oldEndTime1 = $redis->hget('H_Org_ShiftTime_' . $fcode,$id);
			// if($oldEndTime1 == null || $oldEndTime1 ==""){
			// 	$oldEndTime1=[];
			// }else{
			// 	$oldEndTime1 = json_decode($oldEndTime1,true);
			// }

			$redis->hdel('H_Org_ShiftTime_'.$fcode,$id);
			$reportData=[];
			if($EndofDayDetails == 'yes'){
				$fromTimes = request()->get("fromTime");
				$toTimes = request()->get("toTime");
				if($fromTimes == null) $fromTimes = [];
				$i = 0;
				foreach($fromTimes as $key => $value){
					$fromTime = $value;
					$toTime = $toTimes[$key];
					if(is_null($toTime)) continue;
					$reportData[$key] =[
						'format' => ++$i,
						'fromTime' => $fromTime,
						'toTime' => $toTime,
					];
				}
				$reportDataJson = json_encode($reportData);
				$redis->hset('H_Org_ShiftTime_'.$fcode,$id,$reportDataJson);
			}
			// logger([$oldEndTime1,$reportData]);

			// $oldEndTime = array_diff($oldEndTime1,$reportData);
			// $newEndTime = array_diff($reportData,$oldEndTime1);

			if ($sendDataToGovtip === "yes") {
				$validator = validator()->make(request()->all(), [
					'ipAddress' => 'ip'
				]);
				if ($validator->fails()) {
					return redirect()->back()->withErrors('Please enter valid IP address!')->withInput();
				}
			}

			if ($fuelApproximate == null) {
				$fuelApproximate = 'no';
			}
			if ($escalatesms != 'yes') {
				$redis->del('H_EscalationConfig_' . $organizationId . '_' . $fcode);
			}

			$morningEntryStartTime	= request()->get('morningEntryStartTime');
			$morningEntryEndTime	= request()->get('morningEntryEndTime');
			$eveningEntryStartTime	= request()->get('eveningEntryStartTime');
			$eveningEntryEndTime	= request()->get('eveningEntryEndTime');
			$morningExitStartTime	= request()->get('morningExitStartTime');
			$morningExitEndTime		= request()->get('morningExitEndTime');
			$eveningExitStartTime	= request()->get('eveningExitStartTime');
			$eveningExitEndTime		= request()->get('eveningExitEndTime');
			$delayTimeAlert = request()->get('delayTimeAlert');
			$enableFuelNotification = request()->get('enableFuelNotification');

			$isRfid = request()->get('isRfid') ?? "no";
			if ($isRfid == 'yes') {
				$PickupStartTime = request()->get('PickupStartTime');
				$PickupEndTime	= request()->get('PickupEndTime');
				$DropStartTime	= request()->get('DropStartTime');
				$DropEndTime	= request()->get('DropEndTime');
				if (strtotime($PickupStartTime) <= strtotime($PickupEndTime) && strtotime($DropStartTime) <= strtotime($DropEndTime)) {
				} else {
					return redirect()->back()->withErrors('Invalid Pick Up and Drop Time !')->withInput();
				}
			} else {
				$PickupStartTime = '';
				$PickupEndTime	= '';
				$DropStartTime	= '';
				$DropEndTime	= '';
			}

			$live = request()->get('live') ?? "no";
			$startTime = $time1;
			$endTime = $time2;

			$orgDataArr = InputController::nullCheckArr(array(
				'mobile' => $mobile,
				'email' => $email,
				'address' => $address,
				'description' => $description,
				'startTime' => $startTime,
				'endTime'  => $endTime,
				'parkingAlert' => $parkingAlert,
				'idleAlert' => $idleAlert,
				'parkDuration' => $parkDuration,
				'idleDuration' => $idleDuration,
				'overspeedalert' => $overspeedalert,
				'SchoolGeoFence' => $SchoolGeoFence,
				'sendGeoFenceSMS' => $sendGeoFenceSMS,
				'radius' => $radius,
				'smsSender' => $smsSender,
				'sosAlert' => $sosAlert,
				'live' => $live,
				'smsProvider' => $smsProvider,
				'providerUserName' => $providerUserName,
				'providerPassword' => $providerPassword,
				'schoolPattern' => $smsPattern,
				'geofence' => $geofence,
				'safemove' => $safemove,
				'deleteHistoryEod' => $deleteHistoryEod,
				'harshBreak' => $harshBreak,
				'dailySummary' => $dailySummary,
				'dailyDieselSummary' => $dailyDieselSummary,
				'fuelLevelBelow' => $fuelLevelBelow,
				'fuelLevelBelowValue' => $fuelLevelBelowValue,
				'noDataDuration' => $noDataDuration,
				'morningEntryStartTime'	=> $morningEntryStartTime,
				'morningEntryEndTime'	=> $morningEntryEndTime,
				'eveningEntryStartTime'	=> $eveningEntryStartTime,
				'eveningEntryEndTime'	=> $eveningEntryEndTime,
				'morningExitStartTime'	=> $morningExitStartTime,
				'morningExitEndTime'	=> $morningExitEndTime,
				'eveningExitStartTime'	=> $eveningExitStartTime,
				'eveningExitEndTime'	=> $eveningExitEndTime,
				'isRfid'				=>	$isRfid,
				'PickupStartTime'		=>	$PickupStartTime,
				'PickupEndTime'			=>	$PickupEndTime,
				'DropStartTime'			=>	$DropStartTime,
				'DropEndTime'			=>	$DropEndTime,
				'escalationsms'			=> $escalatesms,
				'fuelAlarm'  			=> $fuelAlarm,
				'fuelApproximate'	 	=>	$fuelApproximate,
				'tripPlannedTime' => $tripPlannedTime,
				'instantFuel' => $instantFuel,
				'delayTimeAlert' => $delayTimeAlert,
				'enableFuelNotification' => $enableFuelNotification,
				'sendDataToGovtip' => $sendDataToGovtip,
				'ipAddress' => $ipAddress,
				'ipPort' => $ipPort,
				'excessConsumptionFilter' => $excessConsumptionFilter,
				'smsEntityName' => $smsEntityName,
				'textLocalSmsKey' => $textLocalSmsKey,
				'seperateDetails' => $seperateDetails,
				'routeDeviation'=>$routeDeviation,
				'fuelAlertGeoFence'=>$fuelAlertGeoFence,
				'EndofDayDetails'=>$EndofDayDetails
			));


			$mapping_Array = array(
				'mobile' => 'Mobile',
				'email' => 'Email',
				'address' => 'Address',
				'description' => 'Description',
				'startTime' => 'Start Time',
				'endTime'  => 'End Time',
				'parkingAlert' => 'Parking Alert',
				'idleAlert' => 'Idle Alert',
				'parkDuration' => 'Park Duration',
				'idleDuration' => 'Idle Duration',
				'overspeedalert' => 'Over Speed Alert',
				'SchoolGeoFence' => 'School GeoFence',
				'sendGeoFenceSMS' => 'Send GeoFence SMS',
				'radius' => 'Radius',
				'smsSender' => 'SMS Sender',
				'sosAlert' => 'SOS Alert',
				'live' => 'Show Live Site',
				'smsProvider' => 'SMS Provider',
				'providerUserName' => 'Provider UserName',
				'providerPassword' => 'Provider Password',
				'schoolPattern' => 'SMS Pattern',
				'geofence' => 'GeoFence',
				'safemove' => 'Safety Movement Alert',
				'deleteHistoryEod' => 'Delete History Eod',
				'harshBreak' => 'Harsh Break',
				'dailySummary' => 'Daily Summary',
				'dailyDieselSummary' => 'Daily Diesel Summary',
				'fuelLevelBelow' => 'Fuel Level Below',
				'fuelLevelBelowValue' => 'Fuel Level Below Value',
				'noDataDuration' => 'No Data Duration',

				'morningEntryStartTime'	=> 'morningEntryStartTime',
				'morningEntryEndTime'	=> 'morningEntryEndTime',
				'eveningEntryStartTime'	=> 'eveningEntryStartTime',
				'eveningEntryEndTime'	=> 'eveningEntryEndTime',
				'morningExitStartTime'	=> 'morningExitStartTime',
				'morningExitEndTime'	=> 'morningExitEndTime',
				'eveningExitStartTime'	=> 'eveningExitStartTime',
				'eveningExitEndTime'	=> 'eveningExitEndTime',
				'isRfid'				=>	'isRfid',
				'PickupStartTime'		=>	'Pickup Start Time',
				'PickupEndTime'			=>	'Pickup End Time',
				'DropStartTime'			=>	'Drop Start Time',
				'DropEndTime'			=>	'Drop End Time',
				'escalationsms'			=>  'Escalatesms',
				'fuelAlarm' 			=> 'Fuel Alarm',
				'fuelApproximate' 		=> 'OnGoing SMS Template(fuel)',
				'tripPlannedTime'		=>	'Trip Planned Time',
				'instantFuel' 			=>	'Instant Fuel',
				'delayTimeAlert' => 'Delay Time Alert',
				'enableFuelNotification' => 'Enable Fuel Notification',
				'sendDataToGovtip' 			=>	'Send Data To Government IP',
				'ipAddress' => 'IP Address',
				'ipPort' => 'IP Port',
				'excessConsumptionFilter' => 'Excess Consumption Filter',
				'smsEntityName' => 'SMS Entity Name',
				'textLocalSmsKey' => 'Text Local Sms Key',
				'seperateDetails' => 'Seperate Details',
				'EndofDayDetails'=>'Report Timing Details'

			);
			$orgDataJson = json_encode($orgDataArr);
			$redis->hset('H_Organisations_' . $fcode, $organizationId, $orgDataJson);
			try {
				
				$oldDiffRefData = array_diff_assoc($orgDataJson1, $orgDataArr);
				// $oldDiffRefData['shiftTime'] = $oldShiftData;
				// $oldDiffRefData['endTime'] = $oldEndTime;
				$newDiffRefDate = array_diff_assoc($orgDataArr, $orgDataJson1);
				// $newDiffRefDate['shiftTime'] = $newShiftData;
				// $newDiffRefDate['endTime'] = $newEndTime;
				
				$mysqlDetails = [
					'userIpAddress' => session()->get('userIP'),
					'userName' => $username,
					'type' => config()->get('constant.organization'),
					'name' => $organizationId,
					'status' => config()->get('constant.updated'),
					'oldData' => json_encode($oldDiffRefData),
					'newData' => json_encode($newDiffRefDate)
				];
				AuditNewController::updateAudit($mysqlDetails);
			} catch (Exception $e) {
				logger('Exception_' . $organizationId . ' error --->' . $e->getMessage() . ' line--->' . $e);
			}
			$routesArr = explode(",", $mobile);

			$oldOrg = array();
			$NewOrg = array();
			$mailId = array();
			$franDetails_json = $redis->hget('H_Franchise', $fcode);
			$franchiseDetails = json_decode($franDetails_json, true);
			if (isset($franchiseDetails['email1']) == 1) {
				$mailId[] = $franchiseDetails['email2'];
			}
			$own = 'OWN';
			if (session()->get('cur') == 'dealer') {
				$own = $username;
				$mailId[] =   $redis->hget('H_UserId_Cust_Map', $username . ':email');
			}

			if ($oldOrgData != $orgDataJson) {

				$updateJson 	= json_decode($orgDataJson, true);
				$updateJson1 = array(
					'mobile' => $mobile,
					'email' => $email,
					'address' => $address,
					'description' => $description,
					'startTime' => $startTime,
					'endTime'  => $endTime,
					'parkingAlert' => $parkingAlert,
					'idleAlert' => $idleAlert,
					'parkDuration' => $parkDuration,
					'idleDuration' => $idleDuration,
					'overspeedalert' => $overspeedalert,
					'SchoolGeoFence' => $SchoolGeoFence,
					'sendGeoFenceSMS' => $sendGeoFenceSMS,
					'radius' => $radius,
					'smsSender' => $smsSender,
					'sosAlert' => $sosAlert,
					'live' => $live,
					'smsProvider' => $smsProvider,
					'providerUserName' => $providerUserName,
					'providerPassword' => '********',
					'schoolPattern' => $smsPattern,
					'harshBreak' => $harshBreak,
					'dailySummary' => $dailySummary,
					'dailyDieselSummary' => $dailyDieselSummary,
					'fuelLevelBelow' => $fuelLevelBelow,
					'fuelLevelBelowValue' => $fuelLevelBelowValue,
					'noDataDuration' => $noDataDuration,
					'morningEntryStartTime'	=> $morningEntryStartTime,
					'morningEntryEndTime'	=> $morningEntryEndTime,
					'eveningEntryStartTime'	=> $eveningEntryStartTime,
					'eveningEntryEndTime'	=> $eveningEntryEndTime,
					'morningExitStartTime'	=> $morningExitStartTime,
					'morningExitEndTime'	=> $morningExitEndTime,
					'eveningExitStartTime'	=> $eveningExitStartTime,
					'eveningExitEndTime'	=> $eveningExitEndTime,
					'isRfid'				=>	$isRfid,
					'PickupStartTime'		=>	$PickupStartTime,
					'PickupEndTime'			=>	$PickupEndTime,
					'DropStartTime'			=>	$DropStartTime,
					'DropEndTime'			=>	$DropEndTime,
					'escalationsms'			=>  $escalatesms,
					'fuelAlarm' 			=>  $fuelAlarm,
					'fuelApproximate' 		=> 	$fuelApproximate,
					'tripPlannedTime'		=>	$tripPlannedTime,
					'instantFuel'			=>	$instantFuel,
					'delayTimeAlert' => $delayTimeAlert,
					'enableFuelNotification' => $enableFuelNotification,
					'sendDataToGovtip' => $sendDataToGovtip,
					'ipAddress' => $ipAddress,
					'ipPort' => $ipPort,
					'seperateDetails' => $seperateDetails,
					'EndofDayDetails'=>$EndofDayDetails

				);
				$mapping_Array1 = array(

					'mobile' => 'Mobile',
					'email' => 'Email',
					'address' => 'Address',
					'description' => 'Description',
					'startTime' => 'Start Time',
					'endTime'  => 'End Time',
					'parkingAlert' => 'Parking Alert',
					'idleAlert' => 'Idle Alert',
					'parkDuration' => 'Park Duration',
					'idleDuration' => 'Idle Duration',
					'overspeedalert' => 'Over Speed Alert',
					'SchoolGeoFence' => 'School GeoFence',
					'sendGeoFenceSMS' => 'Send GeoFence SMS',
					'radius' => 'Radius',
					'smsSender' => 'SMS Sender',
					'sosAlert' => 'SOS Alert',
					'live' => 'Show Live Site',
					'smsProvider' => 'SMS Provider',
					'providerUserName' => 'Provider UserName',
					'providerPassword' => 'Provider Password',
					'schoolPattern' => 'SMS Pattern',
					'harshBreak' => 'Harsh Break',
					'dailySummary' => 'Daily Summary',
					'dailyDieselSummary' => 'Daily Diesel Summary',
					'fuelLevelBelow' => 'Fuel Level Below',
					'fuelLevelBelowValue' => 'Fuel Level Below Value',
					'noDataDuration' => 'No Data Duration',
					'morningEntryStartTime'	=> 'morningEntryStartTime',
					'morningEntryEndTime'	=> 'morningEntryEndTime',
					'eveningEntryStartTime'	=> 'eveningEntryStartTime',
					'eveningEntryEndTime'	=> 'eveningEntryEndTime',
					'morningExitStartTime'	=> 'morningExitStartTime',
					'morningExitEndTime'	=> 'morningExitEndTime',
					'eveningExitStartTime'	=> 'eveningExitStartTime',
					'eveningExitEndTime'	=> 'eveningExitEndTime',
					'isRfid'				=>	'isRfid',
					'PickupStartTime'		=>	'Pickup Start Time',
					'PickupEndTime'			=>	'Pickup End Time',
					'DropStartTime'			=>	'Drop Start Time',
					'DropEndTime'			=>	'Drop End Time',
					'escalationsms'			=>  'Escalatesms',
					'fuelAlarm'  			=>  'Fuel Alarm',
					'fuelApproximate' 		=> 	'OnGoing SMS Template(fuel)',
					'tripPlannedTime'		=>	'Trip Planned Time',
					'instantFuel'			=>	'Instant Fuel',
					'delayTimeAlert'  => 'Delay Time Alert',
					'enableFuelNotification' > 'Enable Fuel Notification',
					'sendDataToGovtip' 			=>	'Send Data To Government IP',
					'ipAddress' => 'IP Address',
					'ipPort' => 'IP Port',
					'excessConsumptionFilter' => 'Excess Consumption Filter',
					'smsEntityName' => 'SMS Entity Name',
					'textLocalSmsKey' => 'Text Local Sms Key',
					'seperateDetails' => 'Seperate Details',
					'EndofDayDetails'=>'End of Day Details'
				);
				$orgDataJsonOld = array(
					'mobile' => $mobileOld,
					'email' => $emailOld,
					'address' => $addressOld,
					'description' => $descriptionOld,
					'startTime' => $startTimeOld,
					'endTime'  => $endTimeOld,
					'parkingAlert' => $parkingAlertOld,
					'idleAlert' => $idleAlertOld,
					'parkDuration' => $parkDurationOld,
					'idleDuration' => $idleDurationOld,
					'overspeedalert' => $overspeedalertOld,
					'SchoolGeoFence' => $SchoolGeoFence,
					'sendGeoFenceSMS' => $sendGeoFenceSMSOld,
					'radius' => $radiusOld,
					'smsSender' => $smsSenderOld,
					'sosAlert' => $sosAlertOld,
					'live' => $liveOld,
					'smsProvider' => $smsProviderOld,
					'providerUserName' => $providerUserNameOld,
					'providerPassword' => "*******",
					'schoolPattern' => $schoolPatternOld,
					'geofence' => $geofenceOld,
					'deleteHistoryEod' => $deleteHistoryEodOld,
					'harshBreak' => $harshBreakOld,
					'dailySummary' => $dailySummaryOld,
					'dailyDieselSummary' => $dailyDieselSummaryOld,
					'fuelLevelBelow' => $fuelLevelBelowOld,
					'fuelLevelBelowValue' => $fuelLevelBelowValueOld,
					'noDataDuration' => $noDataDurationOld,
					'morningEntryStartTime'	=> $morningEntryStartTime,
					'morningEntryEndTime'	=> $morningEntryEndTime,
					'eveningEntryStartTime'	=> $eveningEntryStartTime,
					'eveningEntryEndTime'	=> $eveningEntryEndTime,
					'morningExitStartTime'	=> $morningExitStartTime,
					'morningExitEndTime'	=> $morningExitEndTime,
					'eveningExitStartTime'	=> $eveningExitStartTime,
					'eveningExitEndTime'	=> $eveningExitEndTime,
					'isRfid'				=> $isRfidOld,
					'PickupStartTime'		=> $PickupStartTimeOld,
					'PickupEndTime'			=> $PickupEndTimeOld,
					'DropStartTime'			=> $DropStartTimeOld,
					'DropEndTime'			=> $DropEndTimeOld,
					'escalationsms'			=>  $escalatesmsold,
					'fuelAlarm'   			=>  $fuelAlarmold,
					'fuelApproximate' 		=> $fuelApproximateOld,
					'tripPlannedTime'		=>	$tripPlannedTimeOld,
					'instantFuel'			=>	$instantFuelOld,
					'delayTimeAlert'  =>  $delayTimeAlertOld,
					'enableFuelNotification' => $enableFuelNotificationOld,
					'sendDataToGovtip' => $sendDataToGovtipOld,
					'ipAddress' => $ipAddressOld,
					'ipPort' => $ipPortOld,
					'seperateDetails' => $seperateDetailsOld,
					'EndofDayDetails'=>$EndofDayDetailsOld
				);
				$orgDataJson1Old = json_encode($orgDataJsonOld);
				$orgDataJsonOld1	= json_decode($orgDataJson1Old, true);

				$orgDataJson2 = json_encode($updateJson1);
				$updateJson3	= json_decode($orgDataJson2, true);
				foreach ($updateJson3 as $key => $value) {
					if (isset($orgDataJsonOld1[$key]) == 1) {
						if ($orgDataJsonOld1[$key] != $updateJson3[$key]) {
							$oldOrg 	= Arr::add($oldOrg, $mapping_Array[$key], $orgDataJsonOld1[$key]);
							$NewOrg 	= Arr::add($NewOrg, $mapping_Array[$key], $updateJson3[$key]);
						}
					} else {
						$oldOrg 	= Arr::add($oldOrg, $mapping_Array[$key], '');
						$NewOrg 	= Arr::add($NewOrg, $mapping_Array[$key], $updateJson3[$key]);
					}
				}


				try {
					$caption = "Organization Id";
					$titles = ['Field Name', 'Old Value', 'New Value'];
					$updatefile =  Excel::download(new FormUpdateExport($titles, $oldOrg, $NewOrg), $organizationId . '.xlsx');
					$s3 = app('aws')->createClient('s3', [
						'endpoint' => config()->get('constant.endpoint'),
					]);

					$s3->putObject(array(
						'Bucket' => config()->get('constant.bucket'),
						'Key' => "mail/" . $organizationId . ".xls",
						'SourceFile' => $updatefile,
						'ACL' => 'public-read'
					));
					// DocumentNotifier:Organistaion:organistaionname:fcode:OWN:email
					$redis->set("DocumentNotifier:Organistaion:" . $organizationId . ":" . $fcode . ":" . $own . ":" . $mailId[0], "yes");
					$redis->expire("DocumentNotifier:Organistaion:" . $organizationId . ":" . $fcode . ":" . $own . ":" . $mailId[0], "30");
				} catch (\Exception $e) {
					logger("excel Exception error------------>" . $e->getMessage() . $e->getFile());
				}
			}
			// sms config upadate
			$mobile = request()->get('smsmobile');
			$duration1 = request()->get('duration');
			$mob = null;
			$smsconfigmessage=null;
			$orgId = $organizationId;
			if ($duration1 != null) {
				foreach ($duration1 as $key => $value) {
					$mob = Arr::add($mob, $value, $mobile[$key]);
				}
				$duration = array_unique($duration1);
				$duplicate = array_unique(array_diff_assoc($duration1, array_unique($duration1)));
				$redis->del('H_EscalationConfig_' . $orgId . '_' . $fcode);
				foreach ($duration1 as $key => $val) {
					$mobilenumber = Arr::get($mob, $val);
					$redis->hset('H_EscalationConfig_' . $orgId . '_' . $fcode, $val, $mobilenumber);
				}
				$duplicate = implode(',', $duplicate);
				$unique = implode(',', $duration);
				if ($duplicate == null & $unique != null) {
					$smsconfigmessage = $unique . ' Duration Successfully Updated in SMS configuration !';
				} else if ($duplicate != null & $unique == null) {
					$smsconfigmessage = $duplicate . ' Duration Not Updated in SMS configuration !';
				} else if ($duplicate != null & $unique != null) {
					$smsconfigmessage = $unique . ' Duration Successfully Updated in SMS configuration & ' . $duplicate . 'Not updated !';
				} else if ($duplicate == null & $unique == null) {
					$smsconfigmessage = 'Duration all are Empty';
				}
			} else if ($duration1 == null) {
				$redis->del('H_EscalationConfig_' . $orgId . '_' . $fcode);
				$smsconfigmessage = 'Duration all are Removed or Empty';
			}
			// redirect
			return redirect()->to('vdmOrganization')->with([
				'alert-class'=>'alert-success',
				'message'=>"Successfully updated $organizationId !",
				// 'smsConfigMessage'=>$smsconfigmessage
			]);
		}
	}

	public function ordIdCheck()
	{
		$organizationId = request()->get('id');
		$redis = Redis::connection();
		$organizationId = $redis->hget('H_Org_Company_Map', $organizationId);

		if ($organizationId != null) {
			return 'Organistaion is already exist. Please choose another one ';
		}
	}


	public function storedOrg()
	{

		$username 	= auth()->id();
		$redis 		= Redis::connection();
		$fcode 		= $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$orgid 		= request()->get('orgId');
		$orgArr = array();
		foreach (request()->get('orgId') as $key => $value) {
			$routeName 	= $redis->smembers('S_RouteList_' . $value . '_' . $fcode);
			foreach ($routeName as $k => $v) {
				$orgArr[] = $v;
			}
		}
		return $orgArr;
	}

	public function _editRoutes()
	{

		$_new_V 	= request()->get('newValue', '');
		$_old_V 	= request()->get('oldValue');
		$_ord_L		= request()->get('orgIds');
		$redis 		= Redis::connection();
		$username 	= auth()->id();
		$fcode 		= $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		if ($_new_V == '') {
			return 'false';
		}
		foreach ($_ord_L as $key => $value) {

			$routeName 	= $redis->smembers('S_RouteList_' . $value . '_' . $fcode);
			foreach ($routeName as $k => $val) {
				if ($_old_V == $val) {
					$redis->srem('S_RouteList_' . $value . '_' . $fcode, $_old_V);
					$redis->sadd('S_RouteList_' . $value . '_' . $fcode, $_new_V);
					$redis->rename('Z_' . $_old_V . '_' . $value . '_' . $fcode, 'Z_' . $_new_V . '_' . $value . '_' . $fcode);
					$redis->rename('L_' . $_old_V . '_' . $value . '_' . $fcode, 'L_' . $_new_V . '_' . $value . '_' . $fcode);
				}
			}
		}
		return 'true';
	}

	public function _mapHistory()
	{

		$_mapr_V   =   request()->get('maproute');
		$_orgid_L  =   request()->get('organId');
		$redis     =   Redis::connection();
		$username  =   auth()->id();
		$fcode     =   $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		if ($_mapr_V != '') {

			foreach ($_orgid_L as $key => $value) {

				$routeName = $redis->smembers('S_RouteList_' . $value . '_' . $fcode);

				foreach ($routeName as $k => $val) {

					if ($_mapr_V == $val) {
						$route_list  = $redis->lrange('L_' . $_mapr_V . '_' . $value . '_' . $fcode, 0, -1);
					}
				}
			}
		}

		return $route_list;
	}

	public function _deleteRoutes()
	{
		$redis 		= Redis::connection();
		$username 	= auth()->id();
		$fcode 		= $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$_del_V 	= request()->get('delete');
		$_org_L 	= request()->get('orgIds');
		if ($_del_V != '')
			foreach ($_org_L as $key => $value) {

				$routeName 	= $redis->smembers('S_RouteList_' . $value . '_' . $fcode);
				foreach ($routeName as $k => $val) {
					if ($_del_V == $val)
						$redis->srem('S_RouteList_' . $value . '_' . $fcode, $_del_V);
					$redis->del('Z_' . $_del_V . '_' . $value . '_' . $fcode);
					$redis->del('L_' . $_del_V . '_' . $value . '_' . $fcode);
				}
			}
		// return 'true';
	}

	public function getStopName()
	{

		$redis 		= Redis::connection();
		$username 	= auth()->id();
		$fcode 		= $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$orgIds 	= request()->get('orgId');
		$stopName 	= [];
		if (count($orgIds) > 0)
			foreach ($orgIds as $k => $val) {
				$stopName =  Arr::add($stopName, $val, $redis->smembers('S_Organisation_Route_' . $val . '_' . $fcode));
			}
		return $stopName;
	}

	public function ScanNew($id)
	{

		$text_word = strtoupper($id);
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$orgListId = 'S_Organisations_' . $fcode;

		if (session()->get('cur') == 'dealer') {
			$orgListId = 'S_Organisations_Dealer_' . $username . '_' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$orgListId = 'S_Organisations_Admin_' . $fcode;
		}

		$cou = $redis->SCARD($orgListId);
		$orgLi = $redis->sScan($orgListId, 0,  'count', $cou, 'match', '*' . $text_word . '*');
		return view('vdm.organization.orgScan', array('orgList' => $orgLi[1]));
	}

	public function orgTrack($id)
	{

		$username = auth()->id();

		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$organizationId = $id;
		$vehicleIdList = [];

		if (session()->get('cur') == 'dealer') {
			$vehicleNameMob = 'H_VehicleName_Mobile_Dealer_' . $username . '_Org_' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$vehicleNameMob = 'H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode;
		} else {
			$vehicleNameMob = 'H_VehicleName_Mobile_Org_' . $fcode;
		}
		$cou = $redis->hlen($vehicleNameMob);
		$orgLi = $redis->HScan($vehicleNameMob, 0,  'count', $cou, 'match', '*' . $organizationId . '*');
		$vehicleListId = $orgLi[1];

		foreach ($vehicleListId as  $veh) {
			$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $veh);
			$vehicleRefData = json_decode($vehicleRefData, true);
			$orgId = isset($vehicleRefData['orgId']) ? $vehicleRefData['orgId'] : 'DEFAULT';
			if ($orgId == $organizationId) {
				$vehicleIdList = Arr::add($vehicleIdList, $veh, $veh);
			}
		}
		if (!empty($vehicleIdList)) {
			return view('vdm.organization.orgTrack', ['organizationId' => $organizationId, 'vehicleIdList' => $vehicleIdList]);
		} 

		return redirect()->to('vdmOrganization')->with([
			'message'=> 'There is no vehicleId under this Organization !'
		]);
		
	}

	public function orgUpdate()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget("H_UserId_Cust_Map", "$username:fcode");
		$organizationId = request()->get('organizationId');
		$vehicleIdList = request()->get('vehicleId');
		
		$trackName = request()->get('trackName');
		$trackName = preg_replace('/\s+/', '', $trackName);
		$trackNameExist = $redis->exists("Public_Tracking:$trackName");
		if ($trackNameExist == 1) {
			return redirect()->back()->withErrors("Track Name $trackName Already Exists");
		}

		$vehicleIdref = implode(":$fcode,", $vehicleIdList);
		foreach ($vehicleIdList as $key => $vehicleId) {
			$added = $redis->hset("H_PublicTracking_Vehicles_$fcode", $vehicleId, $trackName);
		}
		$added = $redis->SET("Public_Tracking:$trackName", "$vehicleIdref:$fcode");


		return redirect()->to('vdmOrganization')->with([
			'message'=> 'Successfully Updated Track Name !'
		]);
	}

	public function orgTrackList($id)
	{

		$username = auth()->id();

		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$organizationId = $id;
		$trackName = '';
		$vehicleIdList = array();
		$trackNameList = null;

		if (session()->get('cur') == 'dealer') {
			$vehicleNameMob = 'H_VehicleName_Mobile_Dealer_' . $username . '_Org_' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$vehicleNameMob = 'H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode;
		} else {
			$vehicleNameMob = 'H_VehicleName_Mobile_Org_' . $fcode;
		}
		$cou = $redis->hlen($vehicleNameMob);
		$orgLi = $redis->HScan($vehicleNameMob, 0,  'count', $cou, 'match', '*' . $organizationId . '*');
		$vehicleListId = $orgLi[1];
		foreach ($vehicleListId as  $veh) {
			$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $veh);
			$vehicleRefData = json_decode($vehicleRefData, true);
			$orgId = isset($vehicleRefData['orgId']) ? $vehicleRefData['orgId'] : 'DEFAULT';
			if ($orgId == $organizationId) {
				$vehicleIdList = Arr::add($vehicleIdList, $veh, $veh);
			}
		}
		if (!empty($vehicleIdList)) {
			foreach ($vehicleIdList as $vehid) {
				$trackName = $redis->hget('H_PublicTracking_Vehicles_' . $fcode, $vehid);
				if ($trackName != null) {
					$trackNameList = Arr::add($trackNameList, $trackName, $trackName);
				}
			}
		}
		$trkvehicleList = null;
		if ($trackNameList != null) {
			$trkNameList = array_unique($trackNameList);
			foreach ($trkNameList as $key => $TrackName) {
				$vehicleIdList2 = $redis->get('Public_Tracking:' . $TrackName);
				$vehicle_id = str_replace(":" . $fcode, "", $vehicleIdList2);
				$trkvehicleList = Arr::add($trkvehicleList, $TrackName, $vehicle_id);
			}
		} else {
			$trkNameList = null;
		}

		if (!empty($trkvehicleList)) {
			$orgTrackListData = [
				'trkNameList' => $trkNameList,
				'organizationId' => $organizationId,
				'trkvehicleList' => $trkvehicleList
			];
			return view('vdm.organization.orgTrackList', $orgTrackListData);
		} else {
			return redirect()->back()->withErrors('No Data Found');
		}
	}

	public function orgTrackEdit($id, $value)
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$organizationId = $id;
		$trackName = $value;
		$vehicleIdList = array();

		if (session()->get('cur') == 'dealer') {
			$vehicleNameMob = 'H_VehicleName_Mobile_Dealer_' . $username . '_Org_' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$vehicleNameMob = 'H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode;
		} else {
			$vehicleNameMob = 'H_VehicleName_Mobile_Org_' . $fcode;
		}

		$cou = $redis->hlen($vehicleNameMob);
		$orgLi = $redis->HScan($vehicleNameMob, 0,  'count', $cou, 'match', '*' . $organizationId . '*');
		$vehicleListId = $orgLi[1];

		foreach ($vehicleListId as  $veh) {
			$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $veh);
			$vehicleRefData = json_decode($vehicleRefData, true);
			$orgId = isset($vehicleRefData['orgId']) ? $vehicleRefData['orgId'] : 'DEFAULT';
			if ($orgId == $organizationId) {
				$vehicleIdList = Arr::add($vehicleIdList, $veh, $veh);
			}
		}
		$vehicleIdList2 = $redis->get('Public_Tracking:' . $trackName);
		$vehicleIdexpo = str_replace(':' . $fcode, '', $vehicleIdList2);
		$vehicleId = explode(',', $vehicleIdexpo);

		$orgTrackEditData = [
			'organizationId' => $organizationId,
			'vehicleId' => $vehicleId,
			'trackName' => $trackName,
			'vehicleIdList' => $vehicleIdList
		];

		return view('vdm.organization.orgTrackEdit', $orgTrackEditData);
	}

	public function orgTrackEditUpdate()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$organizationId = request()->get('organizationId');
		$trackNameOld = request()->get('trackNameOld');
		$vehicleIdOld = request()->get('vehicleIdOld');
		$trackName1 = request()->get('trackName');
		$vehicleId = request()->get('vehicleId');

		$trackName = preg_replace('/\s+/', '', $trackName1);
		foreach ($vehicleIdOld as $key => $vehicleIdOld) {
			$redis->hdel('H_PublicTracking_Vehicles_' . $fcode, $vehicleIdOld);
		}

		$del = $redis->del('Public_Tracking:' . $trackNameOld);
		$vehicleIdref = implode(':' . $fcode . ',', $vehicleId);
		foreach ($vehicleId as $key => $vehicle) {
			$added = $redis->hset('H_PublicTracking_Vehicles_' . $fcode, $vehicle, $trackName1);
		}
		$added = $redis->SET('Public_Tracking:' . $trackName, $vehicleIdref . ':' . $fcode);

		return redirect()->to('vdmOrganization')->with([
			'message'=> 'Successfully Updated Track Name !'
		]);
	}

	public function orgTrackDelete($id, $value)
	{
		$username = auth()->id();

		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$organizationId = $id;
		$trackName = $value;
		$vehicleIdList2 = $redis->get('Public_Tracking:' . $trackName);
		$vehicleIdexpo = str_replace(':' . $fcode, '', $vehicleIdList2);
		$vehicleId = explode(',', $vehicleIdexpo);
		$del = $redis->del('Public_Tracking:' . $trackName);
		foreach ($vehicleId as $key => $veh) {
			$oldVehicle = $redis->hdel('H_PublicTracking_Vehicles_' . $fcode, $veh);
		}

		return redirect()->to('vdmOrganization')->with([
			'message'=> 'Successfully Deleted Track Name !'
		]);
	}

	public function smsconfig($id)
	{

		$orgId = $id;
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$orgDataJson1 = $redis->hget('H_Organisations_' . $fcode, $orgId);
		$orgDataJson1 = json_decode($orgDataJson1, true);
		$orgDataJson1['escalationsms'] = 'yes';
		$detailsJson = json_encode($orgDataJson1);
		$redis->hmset('H_Organisations_' . $fcode, $orgId, $detailsJson);
		$count = $redis->hlen('H_EscalationConfig_' . $orgId . '_' . $fcode);
		$durationList = $redis->hkeys('H_EscalationConfig_' . $orgId . '_' . $fcode);
		$mobile = null;
		foreach ($durationList as $key => $dur) {
			$mobilenumber = $redis->hget('H_EscalationConfig_' . $orgId . '_' . $fcode, $dur);
			$mobile = Arr::add($mobile, $dur, $mobilenumber);
		}
		$smsconfigData = [
			'orgId' => $orgId,
			'mobile' => $mobile,
			'duration' => $durationList
		];
		return view('vdm.organization.smsconfig', $smsconfigData);
	}

	public function smsconfigUpdate()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$orgId = request()->get('orgId');
		$mobile = request()->get('mobile');
		$duration1 = request()->get('duration');
		$mob = null;
		$message=null;

		if ($duration1 != null) {
			foreach ($duration1 as $key => $value) {
				$mob = Arr::add($mob, $value, $mobile[$key]);
			}
			$duration = array_unique($duration1);
			$duplicate = array_unique(array_diff_assoc($duration1, array_unique($duration1)));
			$redis->del('H_EscalationConfig_' . $orgId . '_' . $fcode);
			foreach ($duration1 as $key => $val) {
				$mobilenumber = Arr::get($mob, $val);
				$redis->hset('H_EscalationConfig_' . $orgId . '_' . $fcode, $val, $mobilenumber);
			}
			$duplicate = implode(',', $duplicate);
			$unique = implode(',', $duration);
			if ($duplicate == null & $unique != null) {
				$message = $unique . ' Duration Successfully Updated in SMS configuration !';
			} else if ($duplicate != null & $unique == null) {
				$message = $duplicate . ' Duration Not Updated in SMS configuration !';
			} else if ($duplicate != null & $unique != null) {
				$message = $unique . ' Duration Successfully Updated in SMS configuration & ' . $duplicate . 'Not updated !';
			} else if ($duplicate == null & $unique == null) {
				$message = 'Duration all are Empty';
			}
		} else if ($duration1 == null) {
			$redis->del('H_EscalationConfig_' . $orgId . '_' . $fcode);
			$message = 'Duration all are Removed or Empty';
		}
		session()->flash('message',$message);
		return redirect()->to('vdmOrganization/' . $orgId . '/edit');
	}

	public function getOrgveh()
	{
		$organizationId = request()->get('orgId');
		$vehicleListId = array();
		$vehicleIdList = array();
		try {
			foreach ($organizationId as $key => $org) {

				$vehicleListId  = DB::table('Vehicle_details')
					->select('vehicle_id')
					->where('orgId', $org)
					->get();
				$vehicleIdList = array_merge($vehicleIdList, $vehicleListId);
			}
		} catch (Exception $e) {
			logger($e->getMessage());
		}

		return $vehicleIdList;
	}
}
