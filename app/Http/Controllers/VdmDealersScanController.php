<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redis;

class VdmDealersScanController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function dealerSearch()
    {
        $orgLis = [];
        return view('vdm.dealers.index1')->with('dealerlist', $orgLis);
    }
    public function dealerScan()
    {
        
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $redisDealerCacheID = 'S_Dealers_' . $fcode;
        $dealerlist = $redis->smembers($redisDealerCacheID);
        $text_word = request()->get('text_word');
        $cou = $redis->SCARD($redisDealerCacheID); 
        $orgLi = $redis->sScan($redisDealerCacheID, 0, 'count', $cou, 'match', '*' . $text_word . '*'); 
        $orgL = $orgLi[1];
        $userGroups = null;
        $userGroupsArr = null;
        $dealerWeb = null;
        $enableArr =null;
        foreach ($orgL as $key => $value) {
            $userGroups = $redis->smembers($value);
            $userGroups = implode('<br/>', $userGroups);
            $detailJson = $redis->hget('H_DealerDetails_' . $fcode, $value);
            $detail = json_decode($detailJson, true);
            $userGroupsArr = Arr::add($userGroupsArr, $value, $detail['mobileNo']);
            $dealerWeb = Arr::add($dealerWeb, $value, $detail['website']);
            $disable = $redis->hget('H_UserId_Cust_Map', $value . ':lock');
            $enableArr = Arr::add($enableArr,$value,!isset($disable));
        }
        $indexData =[
            'fcode'=>$fcode,
            'userGroupsArr'=>$userGroupsArr,
            'dealerlist'=>$orgL,
            'dealerWeb'=>$dealerWeb,
            'enableArr'=>$enableArr
        ];
        return view('vdm.dealers.index1',$indexData);
        // return view('vdm.dealers.index1')->with('fcode', $fcode)->with('userGroupsArr', $userGroupsArr)->with('dealerlist', $orgL)->with('dealerWeb', $dealerWeb);
    }
    public function dealerScanNew($id)
    {
        
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $redisDealerCacheID = 'S_Dealers_' . $fcode;
        $dealerlist = $redis->smembers($redisDealerCacheID);
        $text_word = $id;
        $cou = $redis->SCARD($redisDealerCacheID);
        $orgLi = $redis->sScan($redisDealerCacheID, 0, 'count', $cou, 'match', '*' . $text_word . '*'); 
        $orgL = $orgLi[1];
        $userGroups = null;
        $userGroupsArr = null;
        $dealerWeb = null;
        $enableArr =null;
        foreach ($orgL as $key => $value) {
            $userGroups = $redis->smembers($value);
            $userGroups = implode('<br/>', $userGroups);
            $detailJson = $redis->hget('H_DealerDetails_' . $fcode, $value);
            $detail = json_decode($detailJson, true);
            $userGroupsArr = Arr::add($userGroupsArr, $value, $detail['mobileNo']);
            $dealerWeb = Arr::add($dealerWeb, $value, $detail['website']);
            $disable = $redis->hget('H_UserId_Cust_Map', $value . ':lock');
            $enableArr = Arr::add($enableArr,$value,!isset($disable));
        }
        $indexData =[
            'fcode'=>$fcode,
            'userGroupsArr'=>$userGroupsArr,
            'dealerlist'=>$orgL,
            'dealerWeb'=>$dealerWeb,
            'enableArr'=>$enableArr
        ];
        return view('vdm.dealers.index1',$indexData);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
}
