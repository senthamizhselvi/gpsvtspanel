<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use App\Models\AuditGroup;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\GroupsExport;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class VdmGroupController extends Controller
{

    public function index()
    {

        $username = auth()->id();
        $redis = Redis::connection();
        $vehicleListArr = null;
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $shortName = null;
        $shortNameList = null;
        $shortNameListArr = null;
        $countArr = null;
        if (session()->get('cur') == 'dealer') {
            $redisGrpId = 'S_Groups_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $redisGrpId = 'S_Groups_Admin_' . $fcode;
        } else {
            $redisGrpId = 'S_Groups_' . $fcode;
        }

        $groupList1 = array();
        $groupList = $redis->smembers($redisGrpId);
        sort($groupList);
        $i = 0;
        if (request()->server('HTTP_HOST') === "localhost") {
            $groupList  = array_slice($groupList, 0, 10);
        }
        foreach ($groupList as $key => $group) {
            $group1 = strtoupper($group);
            if ($group == $group1) {
                $groupList1 = Arr::add($groupList1, $i, $group);
                $vehicleList = $redis->smembers($group);
                $count = $redis->scard($group);
                $shortNameList = null;
                foreach ($vehicleList as $vehicle) {
                    $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);
                    $vehicleRefData = json_decode($vehicleRefData, true);
                    $shortName = isset($vehicleRefData['shortName']) ? $vehicleRefData['shortName'] : '-';
                    $shortNameList[] = $shortName;
                }
                $countArr = Arr::add($countArr, $group, $count);
                $vehicleListArr = Arr::add($vehicleListArr, $group, $vehicleList);
                $shortNameListArr = Arr::add($shortNameListArr, $group, $shortNameList);
                $i = $i + 1;
            }
        }
        $indexData = [
            'groupList' => $groupList1,
            'vehicleListArr' => $vehicleListArr,
            'shortNameListArr' => $shortNameListArr,
            'countArr' => $countArr
        ];
        return view('vdm.groups.index', $indexData);
    }


    public function groupSearch()
    {

        $orgLis = [];
        return view('vdm.groups.index1', ['groupList' => $orgLis]);
    }

    public function groupScan()
    {
        $text_word = request()->get('text_word');
        return self::groupScanNew($text_word);
    }

    public function create()
    {

        $username = auth()->id();
        $redis = Redis::connection();

        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        if (session()->get('cur') == 'dealer') {
            $vehicleList = $redis->smembers('S_Vehicles_Dealer_' . $username . '_' . $fcode);
        } else if (session()->get('cur') == 'admin') {
            $vehicleList = $redis->smembers('S_Vehicles_Admin_' . $fcode);
        } else {
            $vehicleList = $redis->smembers('S_Vehicles_' . $fcode);
        }

        $userVehicles = null;
        $shortName = null;
        $shortNameList = null;
        try {
            if ($vehicleList != null) {
                sort($vehicleList);
                if (request()->server('HTTP_HOST') === "localhost") {
                    $vehicleList  = array_slice($vehicleList, 0, 100);
                }
                foreach ($vehicleList as $key => $value) {
                    $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $value);
                    if (is_null($vehicleRefData)) continue;
                    $vehicleRefData = json_decode($vehicleRefData, true);
                    if (!isset($vehicleRefData['deviceId'])) continue;
                    $deviceId = $vehicleRefData['deviceId'];
                    if ((session()->get('cur') == 'dealer' &&  $redis->sismember('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId) == 0) || session()->get('cur') == 'admin') {
                        $shortName = isset($vehicleRefData['shortName']) ? $vehicleRefData['shortName'] : $value;
                        $userVehicles = Arr::add($userVehicles, $value, $value);
                        $shortNameList = Arr::add($shortNameList, $value, $shortName);
                    }
                }
            }
        } catch (Exception $e) {
            logger($e->getMessage());
            return redirect()->to('vdmGroups')->withErrors('Try again late!');
        }

        $id = '';
        $createData = [
            'groupName1' => $id,
            'userVehicles' => $userVehicles,
            'shortNameList' => $shortNameList,
        ];
        return view('vdm.groups.create', $createData);
    }

    public function createNew($id)
    {

        $username = auth()->id();
        $redis = Redis::connection();

        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $vehicleList = $redis->smembers('S_Vehicles_' . $fcode);
        if (session()->get('cur') == 'dealer') {

            $vehicleList = $redis->smembers('S_Vehicles_Dealer_' . $username . '_' . $fcode);
        } else if (session()->get('cur') == 'admin') {
            $vehicleList = $redis->smembers('S_Vehicles_Admin_' . $fcode);
        }


        $userVehicles = null;
        $shortName = null;
        $shortNameList = null;
        try {
            if ($vehicleList != null) {
                sort($vehicleList);
                foreach ($vehicleList as $key => $value) {
                    $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $value);
                    $vehicleRefData = json_decode($vehicleRefData, true);
                    $deviceId = $vehicleRefData['deviceId'];
                    if ((session()->get('cur') == 'dealer' &&  $redis->sismember('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId) == 0) || session()->get('cur') == 'admin') {
                        $shortName = $vehicleRefData['shortName'];
                        $userVehicles = Arr::add($userVehicles, $value, $value);
                        $shortNameList = Arr::add($shortNameList, $value, $shortName);
                    }
                }
            }
        } catch (\Exception $e) {
            logger($e->getMessage());
        }

        $createData = [
            'groupName1' => $id,
            'userVehicles' => $userVehicles,
            'shortNameList' => $shortNameList,
        ];
        return view('vdm.groups.create', $createData);
    }

    public function store()
    {


        $username = auth()->id();
        $rules = array(
            'groupId'       => 'required|alpha_dash',
            'vehicleList' => 'required'
        );
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } else {
            // store

            $groupId       = request()->get('groupId');
            $groupId        = strtoupper($groupId);
            $vehicleList    = request()->get('vehicleList');

            $redis = Redis::connection();
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $fcode1 = strtoupper($fcode);

            $value1 = $redis->SISMEMBER('S_Groups_' . $fcode, $groupId . ':' . $fcode1);
            if ($value1 == 1) {
                return redirect()->back()->withInput()->with([
                    'message'=>'This group name already present along the Admin level !'
                ]);
            }
            $redis->sadd('S_Groups_' . $fcode, $groupId . ':' . $fcode1);
            $vehicleId = '';
            $vehicleName = '';

            foreach ($vehicleList as $vehi) {
                $vehicle    = explode(" || ", $vehi)[0];
                $redis->sadd($groupId . ':' . $fcode1, $vehicle);

                $vehicleId = explode('||', $vehi)[0] . ', ' . $vehicleId;
                $vehicleName = explode('||', $vehi)[1] . ', ' . $vehicleName;

                //senthamizh lic $ onboard date
                $exist = $redis->smembers('S_' . $vehicle . '_' . $fcode);

                if ($exist == null) {
                    $current = Carbon::now();
                    $onboardDate = $current->format('d-m-Y');
                    $refData = $redis->hget('H_RefData_' . $fcode, $vehicle);

                    $refDataJson1 = json_decode($refData, true);
                    $refDataArr =   array(
                        'onboardDate' => $onboardDate,
                        'licenceissuedDate' => $onboardDate,
                    );
                    $refDataJson = json_encode(array_merge($refDataJson1, $refDataArr));
                    $redis->hset('H_RefData_' . $fcode, $vehicle, $refDataJson);
                    $dev = $redis->hget('H_Vehicle_Device_Map' . $fcode, $vehicle);
                    $pub = $redis->PUBLISH('sms:topicNetty', $dev);
                }
                ///ram noti
                $redis->sadd('S_' . $vehicle . '_' . $fcode, 'S_' . $groupId . ':' . $fcode1);
            }

            if (session()->get('cur') == 'dealer') {
                $redis->sadd('S_Groups_Dealer_' . $username . '_' . $fcode, $groupId . ':' . $fcode1);
            } else if (session()->get('cur') == 'admin') {
                $redis->sadd('S_Groups_Admin_' . $fcode, $groupId . ':' . $fcode1);
            }
           
            try {
                $mysqlDetails = [
                    'userIpAddress' => session()->get('userIP'),
                    'userName' => $username,
                    'name' => "$groupId:$fcode1",
                    'type' => config()->get('constant.group'),
                    'status' => config()->get('constant.created'),
                    'oldData' => "",
                    'newData' => json_encode([
                        'vehicleId'=>$vehicleId,
                        'vehicleName'=>$vehicleName
                    ]),
                ];
                AuditNewController::updateAudit($mysqlDetails);
            } catch (Exception $e) {
                logger('Exception_' . $groupId . ' error --->' . $e->getMessage() . ' line--->' . $e->getLine());
            }
            // redirect
            return redirect()->to('vdmGroups')->with([
                'message'=>"Successfully created $groupId !"
            ]);
        }
    }


    public function show($id)
    {
        $orgLis = [];
        return view('vdm.groups.index1', ['groupList' => $orgLis]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {


        $username = auth()->id();
        $redis = Redis::connection();
        $groupId = $id;
        if (strtoupper($groupId) == $groupId) {
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

            $vehicles = $redis->smembers('S_Vehicles_' . $fcode);
            if (session()->get('cur') == 'dealer') {

                $vehicles = $redis->smembers('S_Vehicles_Dealer_' . $username . '_' . $fcode);
            } else if (session()->get('cur') == 'admin') {
                $vehicles = $redis->smembers('S_Vehicles_Admin_' . $fcode);
            }


            $selectedVehicles =  $redis->smembers($groupId);
            $shortName = null;
            $shortNameList = null;
            $vehicleList = null;
            sort($vehicles);
            if (request()->server('HTTP_HOST') === "localhost") {
                $vehicles  = array_slice($vehicles, 0, 100);
            }

            foreach ($vehicles as $key => $value) {
                $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $value);
                $vehicleRefData = json_decode($vehicleRefData);

                $deviceId = isset($vehicleRefData->deviceId) ? $vehicleRefData->deviceId : "nill";

                if ((session()->get('cur') == 'dealer' &&  $redis->sismember('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId) == 0) || session()->get('cur') == 'admin') {

                    $shortName = isset($vehicleRefData->shortName) ? $vehicleRefData->shortName : $value;
                    $shortNameList = Arr::add($shortNameList, $value, $shortName);
                    $vehicleList = Arr::add($vehicleList, $value, $value);
                }
            }
            $editData = [
                'groupId' => $groupId,
                'vehicleList' => $vehicleList,
                'selectedVehicles' => $selectedVehicles,
                'shortNameList' => $shortNameList,
            ];
            return view('vdm.groups.edit', $editData);
        } else {
            return redirect()->back()->with(['message'=>'Please use Groups in UpperCase!']);
        }
    }

    public function update($id)
    {

        $username       = auth()->id();
        $groupId0       = request()->get('groupId');
        $groupId        = strtoupper($groupId0);
        $vehicleList    = request()->get('vehicleList');
        $redis          = Redis::connection();
        $oldVehi        = $redis->smembers($id);
        $getOldVehicle = [];
        $getOldVehicle = $redis->smembers($id);
        $redis->del($id);
        $updateVehi     = array();
        $own = 'OWN';


        if ($vehicleList != NULL || sizeof($vehicleList) > 0) {
            foreach ($vehicleList as $vehi) {
                $vehicle  = explode(" || ", $vehi)[0];
                $redis->sadd($id, $vehicle);
                $updateVehi[]   = $vehicle;
            }

            $mailId = array();
            $fcode      =       $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $franDetails_json = $redis->hget('H_Franchise', $fcode);
            $franchiseDetails = json_decode($franDetails_json, true);
            if (isset($franchiseDetails['email2']) == 1) {
                // $mailId[]    =       $franchiseDetails['email1'];
                $mailId[]               = $franchiseDetails['email2'];
            }

            if (session()->get('cur') == 'dealer') {
                $mailId[] =   $redis->hget('H_UserId_Cust_Map', $username . ':email');
            }

            /// ram noti
            $result = array_diff($oldVehi, $updateVehi);
            foreach ($result as $key => $oldV) {
                $Ulist = $redis->sismember('S_' . $oldV . '_' . $fcode, 'S_' . $id);
                $vehiGrpDel = $redis->srem('S_' . $oldV . '_' . $fcode, 'S_' . $id);
            }
            $result1 = array_diff($updateVehi, $oldVehi);
            foreach ($result1 as $key => $newV) {
                $exist = $redis->smembers('S_' . $newV . '_' . $fcode);

                if ($exist == null) {
                    $current = Carbon::now();
                    $onboardDate = $current->format('d-m-Y');
                    $refData = $redis->hget('H_RefData_' . $fcode, $newV);
                    $refDataJson1 = json_decode($refData, true);
                    $own = isset($refDataJson1['OWN']) ? $refDataJson1['OWN'] : 'OWN';
                    $refDataArr =   array(
                        'onboardDate' => $onboardDate,
                        'licenceissuedDate' => $onboardDate,
                    );
                    $refDataJson = json_encode(array_merge($refDataJson1, $refDataArr));
                    $redis->hset('H_RefData_' . $fcode, $newV, $refDataJson);
                    $dev = $redis->hget('H_Vehicle_Device_Map' . $fcode, $newV);
                    $pub = $redis->PUBLISH('sms:topicNetty', $dev);
                }
                $Ulist1 = $redis->sismember('S_' . $newV . '_' . $fcode, 'S_' . $id);
                $vehiGrpDel1 = $redis->sadd('S_' . $newV . '_' . $fcode, 'S_' . $id);
            }
            ///user notification
            $remVehi = array_diff($oldVehi, $updateVehi);
            foreach ($remVehi as $keyV => $vehiList) {
                $checkVehiOld = $redis->hget('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList);
                $redis->hdel('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList);
                $getGroup = $redis->smembers('S_' . $vehiList . '_' . $fcode);
                foreach ($getGroup as $keyG => $groupList) {
                    $getUser = $redis->smembers($groupList);
                    foreach ($getUser as $keyU => $userList) {
                        $checkVehi = $redis->hget('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList);
                        if (empty($checkVehi)) {
                            //$redis->hdel('H_Vamo_Notification_Vehicle_map_'.$fcode, $vehiList);
                            $success = $redis->hset('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList, ',' . $userList . ',');
                        } else {
                            $success = $redis->hset('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList, $checkVehi . $userList . ',');
                        }
                    }
                }
            }
            $VehicleID = '';
            $vehicleName = '';
            try {
                $VehicleIDList = [];
                $removedVehicleId = [];
                $addedVehicleId = [];

                foreach ($vehicleList as $value) {
                    $VehicleID = explode('||', $value)[0] . ', ' . $VehicleID;
                    $vehicleName = explode('||', $value)[1] . ', ' . $vehicleName;
                    $VehicleIDList[] = trim(explode('||', $value)[0]);
                }
                $addedVehicleId = array_diff($VehicleIDList, $getOldVehicle);
                $removedVehicleId = array_diff($getOldVehicle, $VehicleIDList);

                if (count($removedVehicleId) != 0) $removedVehicles = implode(", ", $removedVehicleId);
                else $removedVehicles = '';
                if (count($addedVehicleId) != 0) $addedVehicles = implode(", ", $addedVehicleId);
                else $addedVehicles = '';
                $mysqlDetails = [
                    'userIpAddress' => session()->get('userIP'),
                    'userName' => $username,
                    'name' => $id,
                    'type' => config()->get('constant.group'),
                    'status' => config()->get('constant.updated'),
                    'oldData' => json_encode([
                        'vehicleId'=>$removedVehicles
                    ]),
                    'newData' => json_encode([
                        'vehicleId'=>$addedVehicles,
                    ]),
                ];
                AuditNewController::updateAudit($mysqlDetails);
            } catch (Exception $e) {
                logger('Exception_' . $groupId . ' error --->' . $e->getMessage() . ' line--->' . $e->getLine());
            }
            return redirect()->to('vdmGroups')->with([
                'alert-class'=> 'alert-success',
                'message', "Successfully updated $id !"
            ]);
        } else {
            $redis->sadd($id, $oldVehi);
            $fcode      =       $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $result2 = array_diff($oldVehi, $updateVehi);
            foreach ($result2 as $key => $oldV) {
                $Ulist = $redis->sismember('S_' . $oldV . '_' . $fcode, 'S_' . $id);
                $vehiGrpDel = $redis->srem('S_' . $oldV . '_' . $fcode, 'S_' . $id);
            }
            ///
            return redirect()->back()->withErrors('Please select any one vehicle . ')->withInput();
        }
    }


    public function destroy($id)
    {

        $username = auth()->id();
        $redis = Redis::connection();

        $groupId = $id;
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        ///ram noti
        $getVehicle = $redis->smembers($id);
        foreach ($getVehicle as $key => $getV) {
            $redis->srem('S_' . $getV . '_' . $fcode, 'S_' . $id);
        }
        ///

        $redis->srem('S_Groups_' . $fcode, $groupId);
        $redis->srem('S_Groups_Dealer_' . $username . '_' . $fcode, $groupId);
        $redis->srem('S_Groups_Admin_' . $fcode, $groupId);

        $redis->del($groupId);

        $userList = $redis->smembers('S_Users_' . $fcode);
        foreach ($userList as $user) {
            $redis->srem($user, $groupId);
        }
        try {
            $mysqlDetails = [
                'userIpAddress' => session()->get('userIP'),
                'userName' => $username,
                'name' => $groupId,
                'type' => config()->get('constant.group'),
                'status' => config()->get('constant.deleted'),
                'oldData' => "",
                'newData' => json_encode([
                    'vehicleId'=>join(',',$getVehicle)
                ]),
            ];
            AuditNewController::updateAudit($mysqlDetails);

        } catch (Exception $e) {
            logger('Exception_' . $groupId . ' error --->' . $e->getMessage() . ' line--->' . $e->getLine());
        }
        return redirect()->to('vdmGroups')->with([
            'message'=> "Successfully deleted $groupId !"
        ]);
    }

    //check the group name
    public function groupIdCheck()
    {
        $username =    auth()->id();
        $redis = Redis::connection();
        $newGroupId1 = request()->get('id');
        $newGroupId = strtoupper($newGroupId1);
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $fcode1 = strtoupper($fcode);
        $groupValue = $redis->SISMEMBER('S_Groups_' . $fcode, $newGroupId . ':' . $fcode1);
        if ($groupValue == 1) {
            return 'fail';
        } else {
            return $newGroupId;
        }
    }

    // remove group from user
    public function removeGroup()
    {
        $username     =    auth()->id();
        $redis         = Redis::connection();
        $fcode         = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $ownerShip    =    $redis->hget('H_UserId_Cust_Map', $username . ':OWN');

        $grpNameId = request()->get('grpName');
        $getVehicle = $redis->smembers($grpNameId);
        $redis->srem('S_Groups_' . $fcode, $grpNameId);
        if ($ownerShip == 'admin')
            $redis->srem('S_Groups_Admin_' . $fcode, $grpNameId);
        else
            $redis->srem('S_Groups_Dealer_' . $ownerShip . '_' . $fcode, $grpNameId);
        $redis->del($grpNameId);
        $userList = $redis->smembers('S_Users_' . $fcode);

        foreach ($userList as $user) {
            $redis->srem($user, $grpNameId);
        }
        ///ram noti
        foreach ($getVehicle as $key => $getV) {
            $redis->srem('S_' . $getV . '_' . $fcode, 'S_' . $grpNameId);
        }
        $userDel = $redis->del('S_' . $grpNameId);
        ///


        return 'sucess';
    }

    public function _getRef_data($vId, $fcode)
    {
        $redis         = Redis::connection();
        $refData     = $redis->hget('H_RefData_' . $fcode, $vId);
        $refData    = json_decode($refData, true);
        return isset($refData['shortName']) ? $refData['shortName'] : ' ';
    }

    // show vehicles from user
    public function _showGroup()
    {
        $username =     auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $grpName0 = request()->get('grpName');
        $grpName  = strtoupper($grpName0);
        $g_List         = $redis->smembers($username);
        $vehiList = [];
        if ($grpName !== '') {
            if ($g_List && $g_List !== '' && sizeof($g_List) > 0) {

                foreach ($g_List as $g_Name) {
                    $g_level_vehi   =       $redis->smembers($g_Name);
                    foreach ($g_level_vehi as $key => $vehicles) {
                        $check  = ($grpName == $g_Name) ? true : false;
                        $vehiList[] =  array('vehicles' => $vehicles, 'check' => $check);
                    }
                }

                $_data = array();
                foreach ($vehiList as $v) {
                    if (isset($_data['vehicles'])) {
                        continue;
                    }
                    $_data[] = $v['vehicles'];
                }

                $_data = array_unique($_data);

                $g_vehi         =       $redis->smembers($grpName);
                $vehiList = [];
                foreach ($_data as $key => $value) {
                    $check = false;
                    foreach ($g_vehi as $ke => $val) {
                        if ($val == $value)
                            $check = true;
                    }
                    // $redis->hget ( 'H_RefData_' . $fcode, $value );
                    $vehiList[] = array('vehicles' => $value, 'check' => $check, 'shortName' => $this->_getRef_data($value, $fcode));
                }
            }
        } else {

            /*
                for new group
            */
            if ($g_List && $g_List !== '' && sizeof($g_List) > 0) {

                $_data = array();
                foreach ($g_List as $g_Name) {
                    $g_level_vehi   =       $redis->smembers($g_Name);
                    foreach ($g_level_vehi as $key => $vehicles) {
                        // $check       = ($grpName == $g_Name)? true : false;
                        array_push($_data,  $vehicles);
                    }
                }

                $_data = array_unique($_data);
                $vehiList = [];
                foreach ($_data as $key => $value) {
                    $vehiList[] =  array('vehicles' => $value, 'check' => false, 'shortName' => $this->_getRef_data($value, $fcode));
                }
            }
        }

        return $vehiList;
    }

    //save group for user 
    public function _saveGroup()
    {
        $username     =    auth()->id();
        $redis         =     Redis::connection();
        $fcode         =     $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $fcode1     =   strtoupper($fcode);
        $GrpName2    =     request()->get('grpName');
        $GrpName1   =   str_replace(' ', '', $GrpName2);
        $grpName    =   strtoupper($GrpName1);
        $newValue     =     request()->get('newValu');
        $vehList     =     request()->get('grplist');
        $mailId     =     array();
        $ownerShip    =    $redis->hget('H_UserId_Cust_Map', $username . ':OWN');
        $oldVehi    =     $redis->smembers($grpName);

        if (strpos($grpName, $fcode) === false) {
            $grpName = $grpName . ':' . $fcode1;
        }


        $franDetails_json = $redis->hget('H_Franchise', $fcode);
        $franchiseDetails = json_decode($franDetails_json, true);

        if (isset($franchiseDetails['email2']) == 1)
            $mailId[]               = $franchiseDetails['email2'];

        if (session()->get('cur') == 'dealer')
            $mailId[] =   $redis->hget('H_UserId_Cust_Map', $username . ':email');

        if ($newValue !== '') {
            $value1 = $redis->SISMEMBER('S_Groups_' . $fcode, $grpName);
            if ($value1 == 1) {
                return '';
            }
            $redis->sadd('S_Groups_' . $fcode, $grpName);
            $redis->sadd($username, $grpName);
            $redis->sadd('S_' . $grpName, $username);
            if ($ownerShip == 'admin')
                $redis->sadd('S_Groups_Admin_' . $fcode, $grpName);
            else
                $redis->sadd('S_Groups_Dealer_' . $ownerShip . '_' . $fcode, $grpName);
        }

        $redis->del($grpName);
        foreach ($vehList as $vehi) {
            $redis->sadd($grpName, $vehi);
        }
        /// ram noti
        $result = array_diff($oldVehi, $vehList);
        foreach ($result as $key => $oldV) {
            $Ulist = $redis->sismember('S_' . $oldV . '_' . $fcode, 'S_' . $grpName);
            $vehiGrpDel = $redis->srem('S_' . $oldV . '_' . $fcode, 'S_' . $grpName);
        }
        $result1 = array_diff($vehList, $oldVehi);
        foreach ($result1 as $key => $newV) {
            $Ulist1 = $redis->sismember('S_' . $newV . '_' . $fcode, 'S_' . $grpName);
            $vehiGrpDel1 = $redis->sadd('S_' . $newV . '_' . $fcode, 'S_' . $grpName);
        }
        ///user notification
        $remVehi = array_diff($oldVehi, $vehList);
        foreach ($remVehi as $keyV => $vehiList) {
            $checkVehiOld = $redis->hget('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList);
            $redis->hdel('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList);
            $getGroup = $redis->smembers('S_' . $vehiList . '_' . $fcode);
            foreach ($getGroup as $keyG => $groupList) {
                $getUser = $redis->smembers($groupList);
                foreach ($getUser as $keyU => $userList) {
                    $checkVehi = $redis->hget('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList);
                    if (empty($checkVehi)) {
                        //$redis->hdel('H_Vamo_Notification_Vehicle_map_'.$fcode, $vehiList);
                        $success = $redis->hset('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList, ',' . $userList . ',');
                    } else {
                        $success = $redis->hset('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList, $checkVehi . $userList . ',');
                    }
                }
            }
        }
        ///
        if (sizeof($mailId) > 0) {
            Mail::queue('emails.group', array('username' => $username, 'groupName' => $grpName, 'oldVehi' => $oldVehi, 'newVehi' => $vehList), function ($message) use ($mailId, $grpName) {
                $message->to($mailId)->subject('Group Updated -' . $grpName);
            });
        }

        return 'sucess';
    }

    public function groupScanNew($id)
    {
        $username = auth()->id();
        $redis = Redis::connection();
        $vehicleListArr = null;
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $redisGrpId = 'S_Groups_' . $fcode;
        $shortName = null;
        $shortNameList = null;
        $shortNameListArr = null;
        if (session()->get('cur') == 'dealer') {
            $redisGrpId = 'S_Groups_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $redisGrpId = 'S_Groups_Admin_' . $fcode;
        } else {
            $redisGrpId = 'S_Groups_' . $fcode;
        }
        $text_word1 = $id;
        $text_word = strtoupper($text_word1);
        $cou = $redis->SCARD($redisGrpId);
        $orgLi = $redis->sScan($redisGrpId, 0, 'count', $cou, 'match', '*' . $text_word . '*');
        $orgL = $orgLi[1];

        $countArr = array();
        foreach ($orgL as $key => $group) {
            $vehicleList = $redis->smembers($group);
            $shortNameList = null;
            foreach ($vehicleList as $vehicle) {
                $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);
                $vehicleRefData = json_decode($vehicleRefData, true);
                $shortName = $vehicleRefData['shortName'];
                $shortNameList[] = $shortName;
            }
            $vehicleListArr = Arr::add($vehicleListArr, $group, $vehicleList);
            $shortNameListArr = Arr::add($shortNameListArr, $group, $shortNameList);
            $count = $redis->scard($group);
            $countArr = Arr::add($countArr, $group, $count);
        }
        $index1Data = [
            'groupList' => $orgL,
            'vehicleListArr' => $vehicleListArr,
            'shortNameListArr' => $shortNameListArr,
            'countArr' => $countArr
        ];
        return view('vdm.groups.index1', $index1Data);
    }

    public function sendExcel()
    {
        return Excel::download(new GroupsExport(), 'GroupList.xlsx');
    }

    public function downloadVehicleTemplate()
    {
        $id = request()->get('groupId');
        $isVehicleId = request()->get('vehicleData') == "vehicleId";
        $title = "DeviceId";
        if ($isVehicleId) {
            $title = "VehicleId";
        }
        session()->flash('updateVehicleModalGroupId', $id);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getProtection()->setSheet(true);
        $sheet->getStyle('A2:A200')->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
        $sheet
            ->SetCellValue('A1', $title);
        $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(50);
        $sheet->getStyle('A1:A1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
        $sheet->getStyle('A1:A1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

        $sheet->getStyle('A1:A1')->getAlignment()->applyFromArray(
            array('horizontal' => 'center', 'text-transform' => 'uppercase')
        );
        $sheet->getStyle('A2:A2')->getAlignment()->setWrapText(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Update_' . $title . '.xlsx"');
        $writer->save("php://output");
    }
    
    public function uploadVehicles()
    {
        $username = auth()->id();
        $redis = Redis::connection();
        $groupId = request()->get('groupId');
        $isVehicleId = request()->get('vehicleData') == "vehicleId";
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        if (!request()->hasFile('import_file')) {
            return redirect()->back()->withInput()->withErrors('Please Upload Excel File of Extension(.xls,.csv)')->with('updateVehicleModalGroupId', $groupId);
        }
        $filename = request()->file('import_file')->getClientOriginalName();
        if ((strpos($filename, '.xls') == false) && (strpos($filename, '.csv') == false) && (strpos($filename, '.xlsx') == false)) {
            return redirect()->back()->withErrors('Please Upload a Valid Excel File')->with('updateVehicleModalGroupId', $groupId);
        }
        $path = request()->file('import_file')->getRealPath();
        try {
            // $reader = IOFactory::createReader('Xlsx');
            // $spreadsheet = $reader->load($path);
            $spreadsheet = IOFactory::load($path);
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->with('updateVehicleModalGroupId', $groupId)->withInput();
        }
        $datas = $spreadsheet->getActiveSheet()->toArray();
        $datas = array_slice($datas, 0, 200);
        // return count($datas);
        $title = head(head($datas));
        unset($datas[0]);
        $invalid = null;
        $insert = [];
        $vehicleData = request()->get('vehicleData');
        $errMode = "Device Id";
        $changeMode = "Vehicle Id";
        if ($isVehicleId) {
            $errMode = "Vehicle Id";
            $changeMode = "Device Id";
        }
        if (strtolower($title) !== $vehicleData) {
            return redirect()->back()->withInput()->withErrors("Please Give $title Excel")->with('updateVehicleModalGroupId', $groupId);
        }
        if (!empty($datas) && count($datas)) {
            foreach ($datas as $key => $value) {
                if ($value[0] == null) continue;
                $value = trim(head($value));
                if ($value == "") continue;
                if ($isVehicleId) {
                    $deviceId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $value);
                    $vehicleId = $value;
                    $refData = $redis->hget("H_RefData_$fcode", $vehicleId);
                    if ($refData == null || $deviceId == null) {
                        $invalid[] = $vehicleId;
                        continue;
                    }
                } else {
                    $vehicleId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $value);
                    $deviceId = $value;
                    $refData = $redis->hget("H_RefData_$fcode", $vehicleId);
                    if ($refData == null || $vehicleId == null) {
                        $invalid[] = $deviceId;
                        continue;
                    }
                }
                $insert[] = ['vehicleId' => $vehicleId, 'deviceId' => $deviceId];
            }
        }
        // return [$insert,$invalid];
        if ($invalid == null && count($insert) <= 0) {
            return redirect()->back()->withErrors("Please, Enter $errMode in Excel")->with('updateVehicleModalGroupId', $groupId)->withInput();
        }
        if ($invalid != null && count($insert) <= 0) {
            $type = "Device Id";
            if ($isVehicleId)  $type = "Vehicle Id";
            $invalidIds = join(', ', $invalid);
            return redirect()->back()->withErrors($type . ' Not Exist ' . $invalidIds)->with('updateVehicleModalGroupId', $groupId)->withInput();
        }

        $oldVehi = $redis->smembers($groupId);
        // $redis->del($groupId);
        $updateVehi = [];
        foreach ($insert as $value) {
            $vehicle  = $value['vehicleId'];
            $redis->sadd($groupId, $vehicle);
            $updateVehi[]   = $vehicle;
        }

        $result = array_diff($oldVehi, $updateVehi);
        foreach ($result as $key => $oldV) {
            $Ulist = $redis->sismember('S_' . $oldV . '_' . $fcode, 'S_' . $groupId);
            $vehiGrpDel = $redis->srem('S_' . $oldV . '_' . $fcode, 'S_' . $groupId);
        }
        $result1 = array_diff($updateVehi, $oldVehi);
        foreach ($result1 as $key => $newV) {
            $exist = $redis->smembers('S_' . $newV . '_' . $fcode);

            if ($exist == null) {
                $current = Carbon::now();
                $onboardDate = $current->format('d-m-Y');
                $refData = $redis->hget('H_RefData_' . $fcode, $newV);
                $refDataJson1 = json_decode($refData, true);
                $own = isset($refDataJson1['OWN']) ? $refDataJson1['OWN'] : 'OWN';
                $refDataArr =   array(
                    'onboardDate' => $onboardDate,
                    'licenceissuedDate' => $onboardDate,
                );
                $refDataJson = json_encode(array_merge($refDataJson1, $refDataArr));
                $redis->hset('H_RefData_' . $fcode, $newV, $refDataJson);
                $dev = $redis->hget('H_Vehicle_Device_Map' . $fcode, $newV);
                $pub = $redis->PUBLISH('sms:topicNetty', $dev);
            }
            $Ulist1 = $redis->sismember('S_' . $newV . '_' . $fcode, 'S_' . $groupId);
            $vehiGrpDel1 = $redis->sadd('S_' . $newV . '_' . $fcode, 'S_' . $groupId);
        }
        $remVehi = array_diff($oldVehi, $updateVehi);
        foreach ($remVehi as $keyV => $vehiList) {
            // $checkVehiOld = $redis->hget('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList);
            $redis->hdel('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList);
            $getGroup = $redis->smembers('S_' . $vehiList . '_' . $fcode);
            foreach ($getGroup as $keyG => $groupList) {
                $getUser = $redis->smembers($groupList);
                foreach ($getUser as $keyU => $userList) {
                    $checkVehi = $redis->hget('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList);
                    if (empty($checkVehi)) {
                        $redis->hset('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList, ',' . $userList . ',');
                    } else {
                        $redis->hset('H_Vamo_Notification_Vehicle_map_' . $fcode, $vehiList, $checkVehi . $userList . ',');
                    }
                }
            }
        }
        $VehicleID = '';
        $vehicleName = '';

        try {
            $VehicleIDList = [];
            $removedVehicleId = [];
            $addedVehicleId = [];

            foreach ($updateVehi as $vehicel) {
                $VehicleID = $vehicel . ',' . $VehicleID;
                $refData = $redis->hget('H_RefData_' . $fcode, $vehicle);
                $refDataJson1 = json_decode($refData, true);
                $vehiName = isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : '';
                $vehicleName = $vehiName . ',' . $vehicleName;
            }
            $addedVehicleId = array_diff($updateVehi, $oldVehi);
            $removedVehicleId = array_diff($oldVehi, $updateVehi);

            if (count($removedVehicleId) != 0) $removedVehicles = implode(", ", $removedVehicleId);
            else $removedVehicles = '';
            if (count($addedVehicleId) != 0) $addedVehicles = implode(", ", $addedVehicleId);
            else $addedVehicles = '';

            $mysqlDetails = [
                'userIpAddress' => session()->get('userIP'),
                'userName' => $username,
                'name' => $groupId,
                'type' => config()->get('constant.group'),
                'status' => config()->get('constant.updated'),
                'oldData' => json_encode([
                    'vehicleId'=>$removedVehicles
                ]),
                'newData' => json_encode([
                    'vehicleId'=>$addedVehicles,
                ]),
            ];
            AuditNewController::updateAudit($mysqlDetails);
        } catch (Exception $e) {
            logger('Exception_' . $groupId . ' error --->' . $e->getMessage() . ' line--->' . $e->getLine());
        }
        if ($invalid !== null) {
            $type = "Device Id";
            if ($isVehicleId)  $type = "Vehicle Id";
            $invalidIds = join(', ', $invalid);
            return redirect()->back()->withErrors($type . ' Not Exist ' . $invalidIds)->with('updateVehicleModalGroupId', $groupId);
        }
        session()->flash('alert-class', 'alert-success');
        session()->flash('message', 'Successfully updated ' . $groupId . '!');
        return redirect()->to('vdmGroups');
    }
}
