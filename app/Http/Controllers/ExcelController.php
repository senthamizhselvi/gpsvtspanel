<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\AuditVehicle;
use App\Models\AuditUser;
use App\Models\AuditGroup;
use App\Models\AuditOrg;
use App\Models\User;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;


class ExcelController extends Controller
{

  /**
   * Display the password reminder view.
   *
   * @return Response
   */
  public function index()
  {
    return view('vdm.excel.import');
  }

  public function downloadExcel()
  {
    logger("download excel ");
    $i = 0;
    $redis = Redis::connection();
    $numberofdevice = request()->get('numberofdevice');
    //$numberofdevice = 2;
    if (session()->get('cur1') == 'prePaidAdmin') {
      $hidLicence = request()->get('licenceType');
    }
    logger("prepaid");
    $nod = $numberofdevice + 1;
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->getProtection()->setSheet(true);
    $sheet->getStyle('A2:F' . $nod)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
    if (session()->get('cur1') == 'prePaidAdmin') {
      $sheet->getStyle('F2:F' . $nod)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
    }
    // $header =["Vehicle Id","Vehicle Name","Device Id","GPS SimNo","Device Type","Vehicle Type"];
    $sheet
      ->SetCellValue('A1', 'Vehicle Id')
      ->SetCellValue('B1', 'Vehicle Name')
      ->SetCellValue('C1', 'Device Id')
      ->SetCellValue('D1', 'GPS SimNo')
      ->SetCellValue('E1', 'Device Type')
      ->SetCellValue('F1', 'Vehicle Type');

    //$spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(12); 
    //$sheet->getDefaultColumnDimension()->setWidth(['A' => 30, 'B' => 30, 'C' => 25, 'D' => 15, 'E' => 20, 'F' => 20, 'G' => 15, 'H' => 15, 'I' => 10, 'J' => 25, 'K' => 20, 'L' => 15, 'M' => 15, 'N' => 35, 'O' => 15, 'P' => 10, 'Q' => 15, 'R' => 20, 'S' => 40]);

    $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(25);
    $sheet->getStyle('A1:F1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
    $sheet->getStyle('A1:F1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

    $sheet->getStyle('A1:F1')->getAlignment()->applyFromArray(
      array('horizontal' => 'center', 'text-transform' => 'uppercase')
    );

    $sheet->getStyle('A2:F2')->getAlignment()->setWrapText(true);

    $devicetype1 = "BSTPL,GV06N,FM1120,TK10SDC,GV05,GV04,GT800,GT02E,JT701,ET300,TS101,TRACKMATECAMRA,AGV,L100,LKGPS,AQUILATS101,GT02U,GK309,L1001,G100,ET03,ET02,ET01,GT02U1,TRAKMATE,VTRACK2,GT03A,TR02,TK99,TK103,GT300,GT06N GT3001,ET3001,TR06,JV200,TK303f,CR2003,GT02D";
    $vehicletype = "Ambulance,Bike,Bus,Car,heavyVehicle,Truck";
    // $vehicleModal = $redis->smembers('S_VehicleModels');
    if (session()->get('cur1') == 'prePaidAdmin') {
      $licence = $hidLicence;
      $values = ['E' => 'devicetype1', 'F' => 'vehicletype'];
    } else {
      $values = ['E' => 'devicetype1', 'F' => 'vehicletype'];
    }

    foreach ($values as $key => $value) {
      for ($i = 2; $i <= $nod; $i++) {

        $objValidation = $sheet->getCell($key . $i)->getDataValidation();
        $objValidation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
        $objValidation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
        $objValidation->setAllowBlank(false);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setShowDropDown(true);
        $objValidation->setErrorTitle('Input error');
        $objValidation->setError('Value is not in list.');
        $objValidation->setPromptTitle('Pick from list');
        $objValidation->setPrompt('Please pick a value from the drop-down list.');
        $objValidation->setFormula1('"' . $$value . '"'); //note this!

      }
    }

    $writer = new Xlsx($spreadsheet);
    //$writer->save('hello world.xlsx');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="VehiclesListUpload.xlsx"');
    $writer->save("php://output");
  }

  public function downloadDealerExcel()
  {
    $type = 'xls';
    //$data = ['Vehicle_Id','Device_Id'];
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $redis = Redis::connection();
    $username = auth()->id();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    if (session()->get('cur') == 'dealer') {
      $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
    }
    $key = 'H_Pre_Onboard_Dealer_' . $username . '_' . $fcode;
    $details = $redis->hgetall($key);
    $devices = null;
    $vehicles = null;
    $deviceModel = null;
    $shortName = null;
    $gpsSimNo = null;
    $regNo = null;
    $vehicleType = null;
    $oprName = null;
    $altShortName = null;
    $eveningTripStartTime = null;
    $overSpeedLimit = null;
    $mobileNo = null;
    $driverName = null;
    $email = null;
    $descriptionStatus = null;
    $odoDistance = null;
    $isRfid = null;
    $Licence = null;
    $Payment_Mode = null;
    $i = 2;
    foreach ($details as $key => $value) {
      $valueData = json_decode($value, true);
      $devices = Arr::add($devices, $i, $valueData['deviceid']);
      $vehicle = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $valueData['deviceid']);

      $details1 = $redis->hget('H_RefData_' . $fcode, $vehicle);
      $valueData1 = json_decode($details1, true);
      $vehicles = Arr::add($vehicles, $i, $vehicle);
      $deviceModel1 = isset($valueData1['deviceModel']) ? $valueData1['deviceModel'] : 'GV06';
      $deviceModel = Arr::add($deviceModel, $i, $deviceModel1);
      $shortName1 = isset($valueData1['shortName']) ? $valueData1['shortName'] : '';
      $shortName = Arr::add($shortName, $i, $shortName1);
      $gpsSimNo1 = isset($valueData1['gpsSimNo']) ? $valueData1['gpsSimNo'] : '';
      $gpsSimNo = Arr::add($gpsSimNo, $i, $gpsSimNo1);
      $regNo1 = isset($valueData1['regNo']) ? $valueData1['regNo'] : '';
      $regNo = Arr::add($regNo, $i, $regNo1);
      $vehicleType1 = isset($valueData1['vehicleType']) ? $valueData1['vehicleType'] : '';
      $vehicleType = Arr::add($vehicleType, $i, $vehicleType1);
      $oprName1 = isset($valueData1['oprName']) ? $valueData1['oprName'] : 'airtel';
      $oprName = Arr::add($oprName, $i, $oprName1);
      $altShortName1 = isset($valueData1['altShortName']) ? $valueData1['altShortName'] : '';
      $altShortName = Arr::add($altShortName, $i, $altShortName1);
      //$eveningTripStartTime1=isset($valueData1['eveningTripStartTime'])?$valueData1['eveningTripStartTime']:'';
      //$eveningTripStartTime = Arr::add($eveningTripStartTime, $i, $eveningTripStartTime1);
      $overSpeedLimit1 = isset($valueData1['overSpeedLimit']) ? $valueData1['overSpeedLimit'] : '60';
      $overSpeedLimit = Arr::add($overSpeedLimit, $i, $overSpeedLimit1);
      $mobileNo1 = isset($valueData1['mobileNo']) ? $valueData1['mobileNo'] : '0123456789';
      $mobileNo = Arr::add($mobileNo, $i, $mobileNo1);
      $driverName1 = isset($valueData1['driverName']) ? $valueData1['driverName'] : '';
      $driverName = Arr::add($driverName, $i, $driverName1);
      $email1 = isset($valueData1['email']) ? $valueData1['email'] : '';
      $email = Arr::add($email, $i, $email1);
      $odoDistance1 = isset($valueData1['odoDistance']) ? $valueData1['odoDistance'] : '';
      $odoDistance = Arr::add($odoDistance, $i, $odoDistance1);
      $isRfid1 = isset($valueData1['isRfid']) ? $valueData1['isRfid'] : 'no';
      $isRfid = Arr::add($isRfid, $i, $isRfid1);
      $Licence1 = isset($valueData1['Licence']) ? $valueData1['Licence'] : 'Advance';
      $Licence = Arr::add($Licence, $i, $Licence1);
      $Payment_Mode1 = isset($valueData1['Payment_Mode']) ? $valueData1['Payment_Mode'] : 'Monthly';
      $Payment_Mode = Arr::add($Payment_Mode, $i, $Payment_Mode1);
      $descriptionStatus1 = isset($valueData1['descriptionStatus']) ? $valueData1['descriptionStatus'] : '';
      $descriptionStatus = Arr::add($descriptionStatus, $i, $descriptionStatus1);
      $i++;
    }


    $sheet
      ->SetCellValue('A1', 'Licence_Id')
      ->SetCellValue('B1', 'Vehicle_Id')
      ->SetCellValue('C1', 'Device_Id')
      ->SetCellValue('D1', 'Vehicle Name')
      ->SetCellValue('E1', 'GPS SimNo')
      ->SetCellValue('F1', 'Device Type')
      ->SetCellValue('G1', 'Register No')
      ->SetCellValue('H1', 'Vehicle Type')
      ->SetCellValue('I1', 'Operator Name')
      ->SetCellValue('J1', 'Alt Short Name')
      ->SetCellValue('K1', 'OverSpeedLimit')
      ->SetCellValue('L1', 'Mobile No')
      ->SetCellValue('M1', 'Driver Name')
      ->SetCellValue('N1', 'Email-Id')
      ->SetCellValue('O1', 'Odometer')
      ->SetCellValue('P1', 'Rfid')
      ->SetCellValue('Q1', 'Vehicle Description');

    $sheet->setWidth(['A' => 30, 'B' => 30, 'C' => 25, 'D' => 30, 'E' => 15, 'F' => 10, 'G' => 15, 'H' => 15, 'I' => 15, 'J' => 25, 'K' => 20, 'L' => 15, 'M' => 25, 'N' => 35, 'O' => 15, 'P' => 15, 'Q' => 15, 'R' => 15, 'S' => 40]);
    $devicetype1 = "BSTPL,GV06N,FM1120,TK10SDC,GV05,GV04,GT800,GT02E,JT701,ET300,TS101,TRACKMATECAMRA,AGV,L100,LKGPS,AQUILATS101,GT02U,GK309,L1001,G100,ET03,ET02,ET01,GT02U1,TRAKMATE,VTRACK2,GT03A,TR02,TK99,TK103,GT300,GT06N GT3001,ET3001,TR06,JV200,TK303f,CR2003,GT02D";
    $vehicletype = "Ambulance,Bike,Bull,Bus,Car,heavyVehicle,JCB,Tanker,Tipper,Truck,Van,Generator";
    $opName = "airtel,reliance,idea";
    $fuel = "no,yes";
    $speed = "10,20,30,40,50,60,70,80,90,100,110,120,130,140,150";
    $rfid = "no,yes";
    $licence = "Basic,Advance";
    $payment = "Monthly,Quarterly,Half yearly,Yearly,2 Years,3 Years,4 Years,5 Years";


    $values = ['F' => 'devicetype1', 'H' => 'vehicletype', 'I' => 'opName', 'K' => 'speed', 'P' => 'rfid', 'Q' => 'licence', 'R' => 'payment'];
    $count1 = (count($devices) + 1);
    foreach ($values as $key => $value) {
      for ($i = 2; $i <= $count1; $i++) {

        $objValidation = $sheet->getCell($key . $i)->getDataValidation();
        $objValidation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
        $objValidation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
        $objValidation->setAllowBlank(false);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setShowDropDown(true);
        $objValidation->setErrorTitle('Input error');
        $objValidation->setError('Value is not in list.');
        $objValidation->setPromptTitle('Pick from list');
        $objValidation->setPrompt('Please pick a value from the drop-down list.');
        $objValidation->setFormula1('"' . $$value . '"');
      }
    }


    $sheet->getProtection()->setSheet(true);
    $sheet->getStyle('B2:S' . $count1)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
    $sheet->getStyle('A1:S1')->getAlignment()->applyFromArray(
      array('horizontal' => 'center', 'text-transform' => 'uppercase')
    );
    $sheet->getStyle('A2:A' . $count1)->getAlignment()->applyFromArray(
      array('horizontal' => 'center', 'text-transform' => 'uppercase')
    );

    $sheet->getStyle('A2:S2')->getAlignment()->setWrapText(true);
    $sheet->cells('A1:S1', function ($cells) {
      $cells->setBackground(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
      $cells->setFontColor('#ffffff');
    });
    $sheet->cells('A2:A' . $count1, function ($cells) {
      $cells->setBackground('#eff1f4');
      $cells->setBorder('#101111');
      $cells->setFontColor('#101111');
    });
    $sheet->freezeFirstRowAndColumn();
    //$sheet->fromArray($data);

    $cou = count($devices) + 2;
    for ($i = 2; $i < $cou; $i++) {
      $sheet->row($i, [$devices[$i], $vehicles[$i], $devices[$i], $shortName[$i], $gpsSimNo[$i], $deviceModel[$i], $regNo[$i], $vehicleType[$i], $oprName[$i], $altShortName[$i], $overSpeedLimit[$i], $mobileNo[$i], $driverName[$i], $email[$i], $odoDistance[$i], $isRfid[$i], $Licence[$i], $Payment_Mode[$i], $descriptionStatus[$i]]);
    }

    $writer = new Xlsx($spreadsheet);
    //$writer->save('hello world.xlsx');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="VehiclesListUpload.xlsx"');
    $writer->save("php://output");

    return Excel::create('VehiclesListUpload', function ($excel) {
      $excel->sheet('Vehicle List', function ($sheet) use ($excel) {
        $redis = Redis::connection();
        $username = auth()->id();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        if (session()->get('cur') == 'dealer') {
          $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
        }
        $key = 'H_Pre_Onboard_Dealer_' . $username . '_' . $fcode;
        $details = $redis->hgetall($key);
        $devices = null;
        $vehicles = null;
        $deviceModel = null;
        $shortName = null;
        $gpsSimNo = null;
        $regNo = null;
        $vehicleType = null;
        $oprName = null;
        $altShortName = null;
        $eveningTripStartTime = null;
        $overSpeedLimit = null;
        $mobileNo = null;
        $driverName = null;
        $email = null;
        $descriptionStatus = null;
        $odoDistance = null;
        $isRfid = null;
        $Licence = null;
        $Payment_Mode = null;
        $i = 2;
        foreach ($details as $key => $value) {
          $valueData = json_decode($value, true);
          $devices = Arr::add($devices, $i, $valueData['deviceid']);
          $vehicle = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $valueData['deviceid']);

          $details1 = $redis->hget('H_RefData_' . $fcode, $vehicle);
          $valueData1 = json_decode($details1, true);


          $vehicles = Arr::add($vehicles, $i, $vehicle);
          $deviceModel1 = isset($valueData1['deviceModel']) ? $valueData1['deviceModel'] : 'GV06';
          $deviceModel = Arr::add($deviceModel, $i, $deviceModel1);
          $shortName1 = isset($valueData1['shortName']) ? $valueData1['shortName'] : '';
          $shortName = Arr::add($shortName, $i, $shortName1);
          $gpsSimNo1 = isset($valueData1['gpsSimNo']) ? $valueData1['gpsSimNo'] : '';
          $gpsSimNo = Arr::add($gpsSimNo, $i, $gpsSimNo1);
          $regNo1 = isset($valueData1['regNo']) ? $valueData1['regNo'] : '';
          $regNo = Arr::add($regNo, $i, $regNo1);
          $vehicleType1 = isset($valueData1['vehicleType']) ? $valueData1['vehicleType'] : '';
          $vehicleType = Arr::add($vehicleType, $i, $vehicleType1);
          $oprName1 = isset($valueData1['oprName']) ? $valueData1['oprName'] : 'airtel';
          $oprName = Arr::add($oprName, $i, $oprName1);
          $altShortName1 = isset($valueData1['altShortName']) ? $valueData1['altShortName'] : '';
          $altShortName = Arr::add($altShortName, $i, $altShortName1);
          //$eveningTripStartTime1=isset($valueData1['eveningTripStartTime'])?$valueData1['eveningTripStartTime']:'';
          //$eveningTripStartTime = Arr::add($eveningTripStartTime, $i, $eveningTripStartTime1);
          $overSpeedLimit1 = isset($valueData1['overSpeedLimit']) ? $valueData1['overSpeedLimit'] : '60';
          $overSpeedLimit = Arr::add($overSpeedLimit, $i, $overSpeedLimit1);
          $mobileNo1 = isset($valueData1['mobileNo']) ? $valueData1['mobileNo'] : '0123456789';
          $mobileNo = Arr::add($mobileNo, $i, $mobileNo1);
          $driverName1 = isset($valueData1['driverName']) ? $valueData1['driverName'] : '';
          $driverName = Arr::add($driverName, $i, $driverName1);
          $email1 = isset($valueData1['email']) ? $valueData1['email'] : '';
          $email = Arr::add($email, $i, $email1);
          $odoDistance1 = isset($valueData1['odoDistance']) ? $valueData1['odoDistance'] : '';
          $odoDistance = Arr::add($odoDistance, $i, $odoDistance1);
          $isRfid1 = isset($valueData1['isRfid']) ? $valueData1['isRfid'] : 'no';
          $isRfid = Arr::add($isRfid, $i, $isRfid1);
          $Licence1 = isset($valueData1['Licence']) ? $valueData1['Licence'] : 'Advance';
          $Licence = Arr::add($Licence, $i, $Licence1);
          $Payment_Mode1 = isset($valueData1['Payment_Mode']) ? $valueData1['Payment_Mode'] : 'Monthly';
          $Payment_Mode = Arr::add($Payment_Mode, $i, $Payment_Mode1);
          $descriptionStatus1 = isset($valueData1['descriptionStatus']) ? $valueData1['descriptionStatus'] : '';
          $descriptionStatus = Arr::add($descriptionStatus, $i, $descriptionStatus1);
          $i++;
        }


        $sheet = $excel->getExcel()->getActiveSheet()
          ->SetCellValue('A1', 'Licence_Id')
          ->SetCellValue('B1', 'Vehicle_Id')
          ->SetCellValue('C1', 'Device_Id')
          ->SetCellValue('D1', 'Vehicle Name')
          ->SetCellValue('E1', 'GPS SimNo')
          ->SetCellValue('F1', 'Device Type')
          ->SetCellValue('G1', 'Register No')
          ->SetCellValue('H1', 'Vehicle Type')
          ->SetCellValue('I1', 'Operator Name')
          ->SetCellValue('J1', 'Alt Short Name')
          ->SetCellValue('K1', 'OverSpeedLimit')
          ->SetCellValue('L1', 'Mobile No')
          ->SetCellValue('M1', 'Driver Name')
          ->SetCellValue('N1', 'Email-Id')
          ->SetCellValue('O1', 'Odometer')
          ->SetCellValue('P1', 'Rfid')
          ->SetCellValue('Q1', 'Vehicle Description');

        $sheet->setWidth(['A' => 30, 'B' => 30, 'C' => 25, 'D' => 30, 'E' => 15, 'F' => 10, 'G' => 15, 'H' => 15, 'I' => 15, 'J' => 25, 'K' => 20, 'L' => 15, 'M' => 25, 'N' => 35, 'O' => 15, 'P' => 15, 'Q' => 15, 'R' => 15, 'S' => 40]);
        $devicetype1 = "BSTPL,GV06N,FM1120,TK10SDC,GV05,GV04,GT800,GT02E,JT701,ET300,TS101,TRACKMATECAMRA,AGV,L100,LKGPS,AQUILATS101,GT02U,GK309,L1001,G100,ET03,ET02,ET01,GT02U1,TRAKMATE,VTRACK2,GT03A,TR02,TK99,TK103,GT300,GT06N GT3001,ET3001,TR06,JV200,TK303f,CR2003,GT02D";
        $vehicletype = "Ambulance,Bike,Bull,Bus,Car,heavyVehicle,JCB,Tanker,Tipper,Truck,Van,Generator";
        $opName = "airtel,reliance,idea";
        $fuel = "no,yes";
        $speed = "10,20,30,40,50,60,70,80,90,100,110,120,130,140,150";
        $rfid = "no,yes";
        $licence = "Basic,Advance";
        $payment = "Monthly,Quarterly,Half yearly,Yearly,2 Years,3 Years,4 Years,5 Years";


        $values = ['F' => 'devicetype1', 'H' => 'vehicletype', 'I' => 'opName', 'K' => 'speed', 'P' => 'rfid', 'Q' => 'licence', 'R' => 'payment'];
        $count1 = (count($devices) + 1);
        foreach ($values as $key => $value) {
          for ($i = 2; $i <= $count1; $i++) {

            $objValidation = $sheet->getCell($key . $i)->getDataValidation();
            $objValidation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
            $objValidation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('Value is not in list.');
            $objValidation->setPromptTitle('Pick from list');
            $objValidation->setPrompt('Please pick a value from the drop-down list.');
            $objValidation->setFormula1('"' . $$value . '"');
          }
        }


        $sheet->getProtection()->setSheet(true);
        $sheet->getStyle('B2:S' . $count1)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
        $sheet->getStyle('A1:S1')->getAlignment()->applyFromArray(
          array('horizontal' => 'center', 'text-transform' => 'uppercase')
        );
        $sheet->getStyle('A2:A' . $count1)->getAlignment()->applyFromArray(
          array('horizontal' => 'center', 'text-transform' => 'uppercase')
        );

        $sheet->getStyle('A2:S2')->getAlignment()->setWrapText(true);
        $sheet->cells('A1:S1', function ($cells) {
          $cells->setBackground(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
          $cells->setFontColor('#ffffff');
        });
        $sheet->cells('A2:A' . $count1, function ($cells) {
          $cells->setBackground('#eff1f4');
          $cells->setBorder('#101111');
          $cells->setFontColor('#101111');
        });
        $sheet->freezeFirstRowAndColumn();
        //$sheet->fromArray($data);

        $cou = count($devices) + 2;
        for ($i = 2; $i < $cou; $i++) {
          $sheet->row($i, [$devices[$i], $vehicles[$i], $devices[$i], $shortName[$i], $gpsSimNo[$i], $deviceModel[$i], $regNo[$i], $vehicleType[$i], $oprName[$i], $altShortName[$i], $overSpeedLimit[$i], $mobileNo[$i], $driverName[$i], $email[$i], $odoDistance[$i], $isRfid[$i], $Licence[$i], $Payment_Mode[$i], $descriptionStatus[$i]]);
        }
      });
      logger('inside the download');
    })->download($type);
  }

  public function importExcel()
  {

    // $username = auth()->id();
    // $redis = Redis::connection();
    // $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    
    if (!request()->hasFile('import_file')) {
      return redirect()->back()->withInput()->withErrors('Please upload valid extension(.xls,.csv) file');
    }
    $filename = request()->file('import_file')->getClientOriginalName();
    if ((strpos($filename, '.xls') == false) && (strpos($filename, '.csv') == false) && (strpos($filename, '.xlsx') == false)) {
      return redirect()->back()->withErrors('Please upload valid extension(.xls,.csv) file');
    }
    
    $path = request()->file('import_file')->getRealPath();
    try {
      $spreadsheet = IOFactory::load($path);
    } catch (Exception $e) {
      dd($e);
      return redirect()->back()->withErrors($e->getMessage())->withInput();
    }
    $datas = $spreadsheet->getActiveSheet()->toArray();
    unset($datas[0]);
    $deviceIdArr = [];
    $vehicleIdArr = [];
    $deviceidtypeArr = [];
    $shortNameArr = [];
    $vehicleTypeArr = [];
    $gpsSimNoArr = [];
    $LicenceIdArr = [];
    $LicenceIdArr = [];
    $licenceType = request()->get('licenceType');
    $lic=[
      "StarterPlus"=>"SP",
      "Starter"=>"ST",
      "Basic"=>"BC",
      "Advance"=>"AV",
      "Premium"=>"PM",
      "PremiumPlus"=>"PL"
    ];
    $isPrepaid = session()->get('cur1') == "prePaidAdmin";
    $insert=[];
    $invalid=[];
    foreach ($datas as $key => $value) {
      
      if(is_null($value[0]) ||  is_null($value[2])){

        continue;
      } 
      $insert[] = [
        'vehicle' => $value[0],
        'vehicleName' => $value[1],
        'device' => $value[2],
        'gpsSimNo' => $value[3],
        'deviceType' => $value[4],
        'vehicleType' => $value[5]
      ];
      $deviceIdArr[] = $value[2];
      $vehicleIdArr[] = $value[0];
      $deviceidtypeArr[] = $value[4];
      $shortNameArr[] = $value[1];
      $vehicleTypeArr[] = $value[5];
      $gpsSimNoArr[] = $value[3];
      if($isPrepaid) {
        $unique_id = strtoupper($lic[$licenceType].uniqid());
        $LicenceIdArr[] = $unique_id;
      }

    }
    if (count($insert) == 0) {
      return redirect()->back()->withErrors('Excel has No Data')->withInput();
    }
    $numberofdevice = request()->get('numberofdevice');
    if (count($insert) < $numberofdevice) {
      return back()->withErrors('Please upload number of devices that you entered')->withInput();
    }

    // 
    $detailList =  [
      'deviceIdArr' => $deviceIdArr,
      'vehicleIdArr' => $vehicleIdArr,
      'deviceidtypeArr' => $deviceidtypeArr,
      'shortNameArr' => $shortNameArr,
      'vehicleTypeArr' => $vehicleTypeArr,
      'gpsSimNoArr' => $gpsSimNoArr,
      'LicenceIdArr'=>$LicenceIdArr
    ];
    return BusinessController::addDeviceV1($detailList);
  }

  /*
    public function importDealerExcel()
    { {

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $fcode1 = strtoupper($fcode);
        logger(sprintf("%s - line %d " . 'USERLOGIN ImportDealerExcel =>' . $username . '__fcode=>' . $fcode, __FILE__, __LINE__));
        if (request()->hasFile('import_file')) {
          $filename = request()->file('import_file')->getClientOriginalName();
          logger($filename);
          if (strpos($filename, '.xls') == false) {
            if (strpos($filename, '.csv') == false) {
              return back()->withErrors('Please upload valid extension(.xls,.csv) file');
            }
          } else {
            $path = request()->file('import_file')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count()) {
              //logger($data);
              try {
                foreach ($data as $key => $value) {
                  logger(sprintf("%s - line %d " . 'USERLOGIN ' . $username . ' vehicleId ' . $value->vehicle_id . ' licence_id ' . $value->licence_id . ' device_id ' . $value->device_id, __FILE__, __LINE__));
                  $insert[] = ['licenceid' => $value->licence_id, 'vehicle' => $value->vehicle_id, 'vehicleName' => $value->vehicle_name, 'device' => $value->device_id, 'gpsSimNo' => $value->gps_simno, 'deviceType' => $value->device_type, 'RegNo' => $value->register_no, 'vehicleType' => $value->vehicle_type, 'operatorName' => $value->operator_name, 'altShortName' => $value->alt_short_name, 'overspeedlimit' => $value->overspeedlimit, 'mobile_no' => $value->mobile_no, 'driver_name' => $value->driver_name, 'sim_no' => $value->sim_no, 'email_id' => $value->email_id, 'odometer' => $value->odometer, 'rfid' => $value->rfid, 'payment_mode' => $value->payment_mode, 'licence' => $value->licence, 'description' => $value->description];
                }
              } catch (Exception $e) {
                return back()->withErrors('Please upload valid extension(.xls,.csv) file');
              }
              $checkLic = $insert[0];
              //logger($checkLic);
              if ($checkLic['licenceid'] == null) {
                $error = "No Licence to upload";
                return back()->withErrors($error);
              } else {
                $vempty = [];
                $notexist = [];
                $vspecial = [];
                $notExistId = [];
                $vehicleIdarray = [];
                $licEx = [];
                $sameId = [];
                // $deviceList      = request()->get('vehicleList');
                // $deviceType      = request()->get('deviceType');
                $ownerShip      = request()->get('dealerId');
                $userId      = request()->get('userId');
                $userId1    = strtoupper($userId);
                $mobileNo      = request()->get('mobileNo');
                $email      = request()->get('email');
                $password      = request()->get('password');
                $type      = request()->get('type');
                $type1      = request()->get('type1');
                $current = Carbon::now();
                $onboardDate = $current->format('d-m-Y');
                $devicearray = [];
                if ($type1 == 'existing') {
                  $userId      = request()->get('userIdtemp');
                  if ($userId == null || $userId == 'select') {
                    return redirect()->to('Business')->withErrors('Invalid user Id');
                  }
                }


                if ($type1 == null) {
                  return redirect()->to('Business')->withErrors('select the sale');
                }
                $type = 'Sale';
                $ownerShip = $username;
                $mobArr = explode(',', $mobileNo);

                if ($type1 == null) {
                  return redirect()->to('Business')->withErrors('Select the user');
                }
                if ($type1 == 'new') {
                  if (session()->get('cur') == 'dealer') {
                    $totalReports = $redis->smembers('S_Users_Reports_Dealer_' . $username . '_' . $fcode);
                  }
                  if ($totalReports != null) {
                    foreach ($totalReports as $key => $value) {
                      $redis->sadd('S_Users_Reports_' . $userId . '_' . $fcode, $value);
                    }
                  }
                  logger($ownerShip . '3----a------->' . session()->get('cur'));
                  $rules =  [
                    'userId' => 'required|alpha_dash',
                    'email' => 'required|email',
                    'mobileNo' => 'required|numeric',
                    'password' => 'required'

                  ];

                  $validator = validator()->make(request()->all(), $rules);
                  if ($validator->fails()) {

                    return redirect()->to('Business')->withErrors($validator);
                  } else {
                    $val = $redis->hget('H_UserId_Cust_Map', $userId . ':fcode');
                    $val1 = $redis->sismember('S_Users_' . $fcode, $userId);
                    $valOrg = $redis->sismember('S_Organisations_' . $fcode, $userId);
                    $valOrg1 = $redis->sismember('S_Organisations_Admin_' . $fcode, $userId);
                    $valGroup = $redis->sismember('S_Groups_' . $fcode, $userId1 . ':' . $fcode1);
                    $valGroup1 = $redis->sismember('S_Groups_Admin_' . $fcode, $userId1 . ':' . $fcode1);
                  }
                  if ($valGroup == 1 || $valGroup1 == 1) {
                    logger('id group exist ' . $userId);
                    return redirect()->to('Business')->withErrors('Name already exist');
                  }
                  if ($valOrg == 1 || $valOrg1 == 1) {
                    logger('id org exist ' . $userId);
                    return redirect()->to('Business')->withErrors('Name already exist');
                  }
                  if ($val1 == 1 || isset($val)) {
                    logger('id already exist ' . $userId);
                    return redirect()->to('Business')->withErrors('User Id already exist');
                  }
                  if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) {
                    return redirect()->to('Business')->withErrors('Name with admin not acceptable');
                  }

                  $mobArr = explode(',', $mobileNo);
                  foreach ($mobArr as $mob) {
                    $val1 = $redis->sismember('S_Users_' . $fcode, $userId);
                    if ($val1 == 1) {
                      return redirect()->to('Business')->withErrors($mob . ' User Id already exist');
                    }
                  }
                }
                $organizationId = $userId;
                $orgId = $organizationId;
                $groupId0 = $orgId;
                $groupId = strtoupper($groupId0);
                if ($ownerShip == 'OWN' && $type != 'Sale') {
                  $orgId = 'DEFAULT';
                }
                if ($userId == null) {
                  $orgId = 'DEFAULT';
                  $organizationId = 'DEFAULT';
                }
                if ($type == 'Sale' && $type1 !== 'new') {
                  $orgId = request()->get('orgId');
                  if ($orgId == null && $redis->sismember('S_Organisations_' . $fcode, $userId) && $userId !== null) {
                    $orgId = $userId;
                  }
                  $orgId = !empty($orgId) ? $orgId : 'DEFAULT';
                  //$organizationId=$orgId;
                }

                $temp = 0;
                $dbarray = [];
                $addedCount = 0;

                for ($i = 0; $i < count($insert); $i++) {
                  $dbtemp = 0;
                  $dataval = $insert[$i];
                  $licenceid = $dataval['licenceid'];
                  $vehicle1 = $dataval['vehicle'];
                  $vehicleId = strtoupper($vehicle1);
                  $device1 = $dataval['device'];
                  $deviceId = strtoupper($device1);

                  $devOldexist = $redis->hget('H_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $licenceid);
                  $vehOld = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $licenceid);
                  $devOld = $licenceid;


                  if ($devOldexist == null && $licenceid != null) {
                    $licEx = Arr::add($licEx, $licenceid, $licenceid);
                    //return back()->withErrors($error);
                  } else if ($deviceId == null && $vehicleId != null) {

                    $vempty = Arr::add($vempty, $vehicleId, $vehicleId);
                  } else if ($vehicleId == null && $deviceId != null) {
                    $vempty = Arr::add($vempty, $deviceId, $deviceId);
                  } else if ($vehicleId == null && $deviceId == null) {
                  } else if ($vehicleId != null && $deviceId != null) {

                    $dev = $redis->hget('H_Device_Cpy_Map', $deviceId);
                    $dev1 = $redis->hget('H_Device_Cpy_Map', $vehicleId);
                    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
                    $back = $redis->hget($vehicleDeviceMapId, $deviceId);
                    $back12 = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
                    $devBack = $redis->sismember('S_Vehicles_' . $fcode, $deviceId);
                    $devBack1 = $redis->hget($vehicleDeviceMapId, $vehicleId);
                    $devBack2 = $redis->sismember('S_Devcie_' . $fcode, $deviceId);
                    $devexist = $redis->hget('H_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId);
                    $vehexist = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $deviceId);

                    $vehicleId = preg_replace('/[ ]{2,}|[\t]/', '', trim($vehicleId));
                    $deviceId = preg_replace('/[ ]{2,}|[\t]/', '', trim($deviceId));
                    logger('After trim vehicleId ' . $vehicleId);
                    $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/';
                    if ($vehicleId == $deviceId) {
                      $sameId = Arr::add($sameId, $vehicleId, $vehicleId);
                    } else if (preg_match($pattern, $vehicleId)) {
                      $vehicleId = str_replace('.', '^', $vehicleId);
                      $vspecial = Arr::add($vspecial, $vehicleId, $vehicleId);
                      //return back()->withErrors('Vehicle ID should be in alphanumeric with _ or - Characters '.$vehicleId1);
                    } else if (preg_match($pattern, $deviceId)) {
                      $deviceId = str_replace('.', '^', $deviceId);
                      $vspecial = Arr::add($vspecial, $deviceId, $deviceId);
                      //return back()->withErrors('Device ID should be in alphanumeric with _ or - Characters '.$deviceid);
                    } else if (($dev1 == 1 || $back12 != null || $devBack1 != null) && ($vehexist == null)) {
                      $vehicleIdarray = Arr::add($vehicleIdarray, $vehicleId, $vehicleId);
                    } else if (($dev == 1 || $devBack != null || $back != null || $devBack2 != null) && ($devexist == null)) {
                      $vehicleIdarray = Arr::add($vehicleIdarray, $deviceId, $deviceId);
                    } else {
                      $addedCount = $addedCount + 1;
                      $devicearray = Arr::add($devicearray, $vehicleId, $deviceId);
                      $deviceDataArr =  [
                        'deviceid' => $deviceId,
                        'deviceidtype' => isset($dataval['deviceType']) ? $dataval['deviceType'] : 'GT06N',
                      ];
                      $deviceDataJson = json_encode($deviceDataArr);

                      $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
                      $deviceMapId = 'H_Device_Cpy_Map';
                      $redis->hdel($vehicleDeviceMapId, $vehOld);
                      $redis->hdel($vehicleDeviceMapId, $devOld);
                      $redis->hdel($deviceMapId, $devOld);
                      $redis->hset($deviceMapId, $deviceId, $fcode);
                      $redis->hset($vehicleDeviceMapId, $vehicleId, $deviceId);
                      $redis->hset($vehicleDeviceMapId, $deviceId, $vehicleId);

                      $back = $redis->hget($vehicleDeviceMapId, $deviceId);
                      if ($back !== null) {
                        // $vehicleId=$back;


                        $refDataJson1 = $redis->hget('H_RefData_' . $fcode, $vehOld);
                        $refDataJson1 = json_decode($refDataJson1, true);
                        $temOrg = isset($refDataJson1['orgId']) ? $refDataJson1['orgId'] : 'DEFAULT';

                        AuditTables::ChangeDB("VAMOSYS");
                        $licence_id = DB::select('select licence_id from Licence where type = :type', ['type' => isset($dataval['licence']) ? $dataval['licence'] : 'Advance']);
                        $payment_mode_id = DB::select('select payment_mode_id from Payment_Mode where type = :type', ['type' => isset($dataval['payment_mode']) ? $dataval['payment_mode'] : 'Monthly']);
                        //logger($licence_id[0]->licence_id.'-------- av  in  ::----------'.$payment_mode_id[0]->payment_mode_id);

                        $licence_id = $licence_id[0]->licence_id;
                        $payment_mode_id = $payment_mode_id[0]->payment_mode_id;
                        try {

                          $dbarray[$dbtemp++] = [
                            'vehicle_id' => $vehicleId,
                            'fcode' => $fcode,
                            'sold_date' => Carbon::now(),
                            'renewal_date' => Carbon::now(),
                            'sold_time_stamp' => round(microtime(true) * 1000),
                            'month' => date('m'),
                            'year' => date('Y'),
                            'payment_mode_id' => $payment_mode_id,
                            'licence_id' => $licence_id,
                            'belongs_to' => $username,
                            'device_id' => $deviceId,
                            'orgId' => $orgId,
                            'status' => isset($refDataJson1['descriptionStatus']) ? $refDataJson1['descriptionStatus'] : ''
                          ];

                          // if($temOrg=='Default' || $temOrg=='default')
                        } catch (\Exception $e) {
                        }           // {// ram testing


                        $refDataArr =  [
                          'deviceId' => $deviceId,
                          'shortName' => isset($dataval['vehicleName']) ? $dataval['vehicleName'] : $vehicleId,
                          'deviceModel' => isset($dataval['deviceType']) ? $dataval['deviceType'] : 'GT06N',
                          'regNo' => isset($dataval['RegNo']) ? $dataval['RegNo'] : 'XXXXX',
                          'vehicleMake' => isset($refDataJson1['vehicleMake']) ? $refDataJson1['vehicleMake'] : '',
                          'vehicleType' =>  isset($dataval['vehicleType']) ? $dataval['vehicleType'] : 'Bus',
                          'oprName' => isset($dataval['operatorName']) ? $dataval['operatorName'] : 'airtel',
                          'mobileNo' => isset($dataval['mobile_no']) ? $dataval['mobile_no'] : '0123456789',
                          'overSpeedLimit' => isset($dataval['overspeedlimit']) ? $dataval['overspeedlimit'] : '60',
                          'odoDistance' => isset($dataval['odometer']) ? $dataval['odometer'] : '0',
                          'driverName' => isset($dataval['driver_name']) ? $dataval['driver_name'] : '',
                          'gpsSimNo' => isset($dataval['gpsSimNo']) ? $dataval['gpsSimNo'] : '0123456789',
                          'email' => isset($dataval['email_id']) ? $dataval['email_id'] : '',
                          'orgId' => $orgId,
                          'sendGeoFenceSMS' => isset($refDataJson1['sendGeoFenceSMS']) ? $refDataJson1['sendGeoFenceSMS'] : 'no',
                          'morningTripStartTime' => isset($refDataJson1['morningTripStartTime']) ? $refDataJson1['morningTripStartTime'] : ' ',
                          'eveningTripStartTime' => isset($refDataJson1['eveningTripStartTime']) ? $refDataJson1['eveningTripStartTime'] : ' ',
                          'parkingAlert' => isset($refDataJson1['parkingAlert']) ? $refDataJson1['parkingAlert'] : 'no',
                          'altShortName' => isset($dataval['altShortName']) ? $dataval['altShortName'] : '',
                          'paymentType' => isset($refDataJson1['paymentType']) ? $refDataJson1['paymentType'] : ' ',
                          'expiredPeriod' => isset($refDataJson1['expiredPeriod']) ? $refDataJson1['expiredPeriod'] : ' ',
                          'fuelType' => isset($refDataJson1['fuelType']) ? $refDataJson1['fuelType'] : ' ',
                          'isRfid' => isset($dataval['rfid']) ? $dataval['rfid'] : 'no',
                          'OWN' => $ownerShip,

                          'Licence' => isset($dataval['licence']) ? $dataval['licence'] : '',
                          'Payment_Mode' => isset($dataval['payment_mode']) ? $dataval['payment_mode'] : '',
                          'descriptionStatus' => isset($dataval['description']) ? $dataval['description'] : '',
                          'vehicleExpiry' => isset($refDataJson1['vehicleExpiry']) ? $refDataJson1['vehicleExpiry'] : '',

                          'onboardDate' => $onboardDate,
                          'tankSize' => isset($refDataJson1['tankSize']) ? $refDataJson1['tankSize'] : '0',
                          'licenceissuedDate' => isset($refDataJson1['licenceissuedDate']) ? $refDataJson1['licenceissuedDate'] : '',
                          'communicatingPortNo' => isset($refDataJson1['communicatingPortNo']) ? $refDataJson1['communicatingPortNo'] : '',
                        ];

                        $refDataJson = json_encode($refDataArr);
                        $redis->hdel('H_RefData_' . $fcode, $vehOld);
                        $redis->hset('H_RefData_' . $fcode, $vehicleId, $refDataJson);
                        $ve = $redis->hget('H_ProData_' . $fcode, $vehOld);
                        $redis->hdel('H_ProData_' . $fcode, $vehOld);
                        $redis->hset('H_ProData_' . $fcode, $vehicleId, $ve);

                        try {
                          $mysqlDetails = array();
                          $status = array();
                          $status = array(
                            'fcode' => $fcode,
                            'vehicleId' => $vehicleId,
                            'userName' => $username,
                            'status' => config()->get('constant.created'),
                            'userIpAddress' => session()->get('userIP'),
                          );
                          $db = $fcode;
                          AuditTables::ChangeDB($db);
                          $mysqlDetails   = array_merge($status, $refDataArr);
                          $modelname = new AuditVehicle();
                          $table = $modelname->getTable();
                          $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
                          if (count($tableExist) > 0) {
                            AuditVehicle::create($mysqlDetails);
                          } else {
                            AuditTables::CreateAuditVehicle();
                            AuditVehicle::create($mysqlDetails);
                          }
                          AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                        } catch (Exception $e) {
                          AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                        }
                      }
                      $redis->srem('S_Vehicles_Dealer_' . $ownerShip . '_' . $fcode, $vehOld);
                      $redis->sadd('S_Vehicles_Dealer_' . $ownerShip . '_' . $fcode, $vehicleId);
                      $redis->srem('S_Vehicles_Admin_' . $fcode, $vehOld);
                      $refDataJson = $redis->hget('H_RefData_' . $fcode, $vehicleId);
                      $refDataJson = json_decode($refDataJson, true);
                      $shortName1 = isset($refDataJson['shortName']) ? $refDataJson['shortName'] : '';
                      $shortName1 = strtoupper($shortName1);
                      $shortName1Old = isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : '';
                      $shortName1Old = strtoupper($shortName1Old);
                      $mobileNo1 = isset($refDataJson['mobileNo']) ? $refDataJson['mobileNo'] : '0123456789';
                      $gpsSimNo1 = isset($refDataJson['gpsSimNo1']) ? $refDataJson['gpsSimNo1'] : '0123456789';
                      $gpsSimNo1 = strtoupper($gpsSimNo1);
                      $gpsSimNo1Old = isset($refDataJson1['gpsSimNo1']) ? $refDataJson1['gpsSimNo1'] : '0123456789';
                      $gpsSimNo1Old = strtoupper($gpsSimNo1Old);
                      $orgIdOld1 = isset($refDataJson['orgId']) ? $refDataJson['orgId'] : 'DEFAULT';
                      $orgIdOld = strtoupper($orgIdOld1);
                      $orgId1 = strtoupper($orgId);

                      $redis->hdel('H_VehicleName_Mobile_Dealer_' . $ownerShip . '_Org_' . $fcode, $vehOld . ':' . $devOld . ':' . $shortName1Old . ':DEFAULT:' . $gpsSimNo1Old);
                      $redis->hset('H_VehicleName_Mobile_Dealer_' . $ownerShip . '_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortName1 . ':' . $orgId1 . ':' . $gpsSimNo1, $vehicleId);




                      /// $redis->hdel('H_Vehicle_Map_Uname_' . $fcode, $vehicleId.'/:'.$fcode);
                      /// $redis->hset('H_Vehicle_Map_Uname_' . $fcode, $vehicleId.'/'.$groupId . ':' . $fcode, $userId);
                      ///ram noti
                      $redis->srem('S_' . $vehOld . '_' . $fcode, 'S_:' . $fcode);
                      $redis->sadd('S_' . $vehicleId . '_' . $fcode, 'S_' . $groupId . ':' . $fcode);
                      $redis->del('S_:' . $fcode);
                      $newOneUser = $redis->sadd('S_' . $groupId . ':' . $fcode, $userId);
                      ///ram noti
                      $details = $redis->hget('H_Organisations_' . $fcode, $organizationId);
                      logger($details . 'before ' . $ownerShip);
                      if ($type == 'Sale' && $type1 == 'new' && $organizationId !== 'DEFAULT' && $organizationId !== 'DEFAULT') {
                        if ($details == null) {
                          $redis->sadd('S_Organisations_' . $fcode, $organizationId);
                          $redis->sadd('S_Organisations_Dealer_' . $ownerShip . '_' . $fcode, $organizationId);

                          $orgDataArr =  [
                            'mobile' => '1234567890',
                            'description' => '',
                            'email' => '',
                            'address' => '',
                            'mobile' => '',
                            'startTime' => '',
                            'endTime'  => '',
                            'atc' => '',
                            'etc' => '',
                            'mtc' => '',
                            'parkingAlert' => '',
                            'idleAlert' => '',
                            'parkDuration' => '',
                            'idleDuration' => '',
                            'overspeedalert' => '',
                            'sendGeoFenceSMS' => '',
                            'radius' => ''
                          ];
                          $orgDataJson = json_encode($orgDataArr);
                          $redis->hset('H_Organisations_' . $fcode, $organizationId, $orgDataJson);
                          $redis->hset('H_Org_Company_Map', $organizationId, $fcode);

                          try {
                            $mysqlDetails = array();
                            $status = array();
                            $status = array(
                              'fcode' => $fcode,
                              'organizationName' => $organizationId,
                              'userName' => $username,
                              'status' => config()->get('constant.created'),
                              'userIpAddress' => session()->get('userIP'),
                            );
                            $mysqlDetails   = array_merge($status, $orgDataArr);
                            $modelname = new AuditOrg();
                            $table = $modelname->getTable();
                            $db = $fcode;
                            AuditTables::ChangeDB($db);
                            $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
                            if (count($tableExist) > 0) {
                              AuditOrg::create($mysqlDetails);
                            } else {
                              AuditTables::CreateAuditOrg();
                              AuditOrg::create($mysqlDetails);
                            }
                            AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                          } catch (Exception $e) {
                            AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                          }
                        }
                      }
                      if ($type == 'Sale') {
                        $groupname1      = request()->get('groupname');
                        $groupname       = strtoupper($groupname1);
                        if ($type1 == 'existing' && $groupname !== null && $groupname !== '') {
                          $groupId1 = explode(":", $groupname)[0];
                          $groupId = strtoupper($groupId1);
                        }
                        $redis->sadd($groupId . ':' . $fcode1, $vehicleId);
                        if ($type1 == 'existing' && $groupname !== null && $groupname !== '') {
                          $groupId = explode(":", $groupname)[0];
                          try {
                            $vehicleIDList = $redis->smembers($groupId . ':' . $fcode1);
                            // logger($vehicleID);
                            $mysqlDetails = array(
                              'userName' => $username,
                              'status' => config()->get('constant.new_vehicle_added'),
                              'fcode' => $fcode,
                              'groupName' => $groupname,
                              'vehicleID' => implode(',', $vehicleIDList),
                              'userIpAddress' => session()->get('userIP'),
                              // 'vehicleName'=>$vehicleName
                            );
                            //$table=new AuditGroup->getTable();
                            $modelname = new AuditGroup();
                            $table = $modelname->getTable();
                            //return $table;
                            //return $mysqlDetails;
                            $db = $fcode;
                            AuditTables::ChangeDB($db);
                            $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
                            //return $tableExist;
                            if (count($tableExist) > 0) {
                              AuditGroup::create($mysqlDetails);
                            } else {
                              AuditTables::CreateAuditGroup();
                              AuditGroup::create($mysqlDetails);
                            }
                            AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                            //AuditGroup::create($details);
                          } catch (Exception $e) {
                            AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                            logger('Error inside AuditGroup Create' . $e->getMessage());
                            logger($mysqlDetails);
                          }
                        }
                      }

                      $redis->srem('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $devOld);
                      $redis->hdel('H_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $devOld);


                      $temp++;
                    }
                  }
                }
              }
              AuditTables::ChangeDB("VAMOSYS");
              if (count($dbarray) !== 0) {
                DB::table('Vehicle_details')->insert(
                  $dbarray
                );
              }

              if ($type == 'Sale') {

                $redis->sadd('S_Groups_' . $fcode, $groupId . ':' . $fcode1);
                $redis->sadd('S_Groups_Dealer_' . $ownerShip . '_' . $fcode, $groupId . ':' . $fcode1);
                $redis->sadd('S_Users_Dealer_' . $ownerShip . '_' . $fcode, $userId);

                $redis->sadd($userId, $groupId . ':' . $fcode1);
                $redis->sadd('S_Users_' . $fcode, $userId);
                $OWN = $username;

                if ($type1 == 'new') {
                  $password = request()->get('password');
                  if ($password == null) {
                    $password = 'awesome';
                  }
                  $redis->hmset('H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo, $userId . ':email', $email, $userId . ':password', $password, $userId . ':OWN', $OWN);

                  $user = new User;

                  $user->name = $userId;
                  $user->username = $userId;
                  $user->email = $email;
                  $user->mobileNo = $mobileNo;
                  $user->password = Hash::make($password);
                  $user->save();

                  $groupNameNew = $groupId . ':' . $fcode1;
                  // logger($groupId1. ':' .$fcode1);
                  $vehicleID = $redis->smembers($groupId . ':' . $fcode1);
                  // logger($vehicleID);
                  try {
                    $mysqlDetails = array(
                      'userName' => $username,
                      'status' => config()->get('constant.created'),
                      'fcode' => $fcode,
                      'groupName' => $groupNameNew,
                      'vehicleID' => implode(",", $vehicleID),
                      'vehicleName' => $shortName,
                      'userIpAddress' => session()->get('userIP'),
                    );
                    //$table=new AuditGroup->getTable();
                    $modelname = new AuditGroup();
                    $table = $modelname->getTable();
                    //return $table;
                    //return $mysqlDetails;
                    $db = $fcode;
                    AuditTables::ChangeDB($db);
                    $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
                    //return $tableExist;
                    if (count($tableExist) > 0) {
                      AuditGroup::create($mysqlDetails);
                    } else {
                      AuditTables::CreateAuditGroup();
                      AuditGroup::create($mysqlDetails);
                    }
                    AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                    //AuditGroup::create($details);
                  } catch (Exception $e) {
                    AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                  }
                  try {
                    $details = array(
                      'fcode' => $fcode,
                      'userId' => $userId,
                      'userName' => $username,
                      'status' => config()->get('constant.created'),
                      'email' => $emailUser,
                      'mobileNo' => $mobileNoUser,
                      'password' => Hash::make($password),
                      'zoho' => '-',
                      'companyName' => '-',
                      'cc_email' => '-',
                      'groups' => $groupId1 . ':' . $fcode1,
                      'vehicles' => implode(",", $vehicleID),
                      'userIpAddress' => session()->get('userIP'),
                    );
                    $modelname = new AuditUser();
                    $table = $modelname->getTable();
                    $db = $fcode;
                    AuditTables::ChangeDB($db);
                    $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
                    if (count($tableExist) > 0) {
                      AuditUser::create($details);
                    } else {
                      AuditTables::CreateAuditUser();
                      AuditUser::create($details);
                    }
                    AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                  } catch (Exception $e) {
                    AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                  }
                }
              }
            }



            $err = '';
            $err1 = '';
            $err2 = '';
            $err3 = '';
            $err4 = '';
            // checking error conditions
            if ($vspecial != null && $vehicleIdarray == null && $vempty == null && $sameId == null && $licEx == null) {
              $err = implode(",", $vspecial);
              $err = str_replace('^', '.', $err);
              if ($addedCount > 0) {
                return back()->withErrors(array('Special Characters are used : ' . $err, $addedCount . ' Devices has been successfully Added'));
              } else {
                return back()->withErrors('Special Characters are used : ' . $err);
              }
            } else if ($vehicleIdarray != null && $vspecial == null && $vempty == null && $sameId == null && $licEx == null) {
              $err = implode(",", $vehicleIdarray);
              if ($addedCount > 0) {
                return back()->withErrors(array('These are already exists : ' . $err, $addedCount . ' Devices has been successfully Added'));
              }
              return back()->withErrors('These are already exists : ' . $err);
            } else if ($vempty != null && $vspecial == null && $vehicleIdarray == null && $sameId == null && $licEx == null) {
              $err = implode(",", $vempty);
              if ($addedCount > 0) {
                return back()->withErrors(array('Empty Device or Vehicle ID Found : ' . $err, $addedCount . ' Devices has been successfully Added'));
              }
              return back()->withErrors('Empty Device or Vehicle ID Found : ' . $err && $licEx == null);
            } else if ($sameId != null && $vempty == null && $vspecial == null && $vehicleIdarray == null && $licEx == null) {
              $err = implode(",", $sameId);
              if ($addedCount > 0) {
                return back()->withErrors(array('Vehicle Id and Device Id should not be same : ' . $err, $addedCount . ' Devices has been successfully Added'));
              }
              return back()->withErrors('Vehicle Id and Device Id should not be same  : ' . $err);
            } else if ($vempty != null && $vspecial != null && $vehicleIdarray == null && $sameId == null && $licEx == null) {
              $err = implode(",", $vempty);
              $err1 = implode(",", $vspecial);
              $err1 = str_replace('^', '.', $err1);
              if ($addedCount > 0) {
                return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'Special Characters are used : ' . $err1, $addedCount . ' Devices has been successfully Added'));
              }
              return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'Special Characters are used : ' . $err1));
            } else if ($vempty != null && $vspecial == null && $vehicleIdarray != null && $sameId == null && $licEx == null) {

              $err = implode(",", $vempty);
              $err1 = implode(",", $vehicleIdarray);
              if ($addedCount > 0) {
                return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'These are already exists : ' . $err1, $addedCount . ' Devices has been successfully Added'));
              }

              return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'These are already exists : ' . $err1));
            } else if ($vempty == null && $vspecial != null && $vehicleIdarray != null && $sameId == null && $licEx == null) {

              $err = implode(",", $vspecial);
              $err = str_replace('^', '.', $err);
              $err1 = implode(",", $vehicleIdarray);
              if ($addedCount > 0) {
                return back()->withErrors(array('Special Characters are used : ' . $err, 'These are already exists : ' . $err1, $addedCount . ' Devices has been successfully Added'));
              }

              return back()->withErrors(array('Special Characters are used : ' . $err, 'These are already exists : ' . $err1));
            } else if ($vempty == null && $vspecial != null && $vehicleIdarray == null && $sameId != null && $licEx == null) {

              $err = implode(",", $vspecial);
              $err = str_replace('^', '.', $err);
              $err1 = implode(",", $sameId);
              if ($addedCount > 0) {
                return back()->withErrors(array('Special Characters are used : ' . $err, 'Vehicle Id and Device Id should not be same : ' . $err1, $addedCount . ' Devices has been successfully Added'));
              }

              return back()->withErrors(array('Special Characters are used : ' . $err, 'Vehicle Id and Device Id should not be same : ' . $err1));
            } else if ($vempty != null && $vspecial == null && $vehicleIdarray == null && $sameId != null && $licEx == null) {

              $err = implode(",", $vempty);
              $err1 = implode(",", $sameId);
              if ($addedCount > 0) {
                return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'Vehicle Id and Device Id should not be same : ' . $err1, $addedCount . ' Devices has been successfully Added'));
              }

              return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'Vehicle Id and Device Id should not be same : ' . $err1));
            } else if ($vempty == null && $vspecial == null && $vehicleIdarray != null && $sameId != null && $licEx == null) {

              $err = implode(",", $vehicleIdarray);
              $err1 = implode(",", $sameId);
              if ($addedCount > 0) {
                return back()->withErrors(array('These are already exists : ' . $err, 'Vehicle Id and Device Id should not be same : ' . $err1, $addedCount . ' Devices has been successfully Added'));
              }

              return back()->withErrors(array('These are already exists : ' . $err, 'Vehicle Id and Device Id should not be same : ' . $err1));
            } else if ($vempty != null && $vspecial != null && $vehicleIdarray != null && $sameId != null && $licEx == null) {
              $err = implode(",", $vempty);
              $err1 = implode(",", $vspecial);
              $err1 = str_replace('^', '.', $err1);
              $err2 = implode(",", $vehicleIdarray);
              $err3 = implode(",", $sameId);
              if ($addedCount > 0) {
                return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'Special Characters are used : ' . $err1, 'These are already exists : ' . $err2, 'Vehicle Id and Device Id should not be same : ' . $err3, $addedCount . ' Devices has been successfully Added'));
              }
              return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'Special Characters are used : ' . $err1, 'These are already exists : ' . $err2, 'Vehicle Id and Device Id should not be same : ' . $err3));
            } else if ($vempty != null && $vspecial != null && $vehicleIdarray != null && $sameId == null && $licEx == null) {
              $err = implode(",", $vempty);
              $err1 = implode(",", $vspecial);
              $err1 = str_replace('^', '.', $err1);
              $err2 = implode(",", $vehicleIdarray);

              if ($addedCount > 0) {
                return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'Special Characters are used : ' . $err1, 'These are already exists : ' . $err2, $addedCount . ' Devices has been successfully Added'));
              }
              return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'Special Characters are used : ' . $err1, 'These are already exists : ' . $err2));
            } else if ($vempty != null && $vspecial != null && $vehicleIdarray == null && $sameId != null && $licEx == null) {
              $err = implode(",", $vempty);
              $err1 = implode(",", $vspecial);
              $err1 = str_replace('^', '.', $err1);
              $err3 = implode(",", $sameId);
              if ($addedCount > 0) {
                return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'Special Characters are used : ' . $err1, 'Vehicle Id and Device Id should not be same : ' . $err3, $addedCount . ' Devices has been successfully Added'));
              }
              return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'Special Characters are used : ' . $err1, 'Vehicle Id and Device Id should not be same : ' . $err3));
            } else if ($vempty != null && $vspecial == null && $vehicleIdarray != null && $sameId != null && $licEx == null) {
              $err = implode(",", $vempty);
              $err2 = implode(",", $vehicleIdarray);
              $err3 = implode(",", $sameId);
              if ($addedCount > 0) {
                return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'These are already exists : ' . $err2, 'Vehicle Id and Device Id should not be same : ' . $err3, $addedCount . ' Devices has been successfully Added'));
              }
              return back()->withErrors(array('Empty Device or Vehicle ID Found: ' . $err, 'These are already exists : ' . $err2, 'Vehicle Id and Device Id should not be same : ' . $err3));
            } else if ($vempty == null && $vspecial != null && $vehicleIdarray != null && $sameId != null && $licEx == null) {
              $err1 = implode(",", $vspecial);
              $err1 = str_replace('^', '.', $err1);
              $err2 = implode(",", $vehicleIdarray);
              $err3 = implode(",", $sameId);
              if ($addedCount > 0) {
                return back()->withErrors(array('Special Characters are used : ' . $err1, 'These are already exists : ' . $err2, 'Vehicle Id and Device Id should not be same : ' . $err3, $addedCount . ' Devices has been successfully Added'));
              }
              return back()->withErrors(array('Special Characters are used : ' . $err1, 'These are already exists : ' . $err2, 'Vehicle Id and Device Id should not be same : ' . $err3));
            } else if ($licEx != null) {
              $err1 = implode(",", $licEx);
              if ($devicearray != null) {
                $err = implode(",", $devicearray);
                if ($vempty != null || $vspecial != null || $vehicleIdarray != null || $sameId != null) {
                  return back()->withErrors(array($addedCount . ' Devices has been successfully Added : ' . $err, 'Licences are not matching : ' . $err1, 'Other Licence details are not valid'));
                } else {
                  return back()->withErrors(array($addedCount . ' Devices has been successfully Added : ' . $err, 'Licences are not matching : ' . $err1));
                }
              } else {
                if ($vempty != null || $vspecial != null || $vehicleIdarray != null || $sameId != null) {
                  return back()->withErrors(array('Licences are not matching : ' . $err1, 'Other Licence details are not valid'));
                } else {
                  return back()->withErrors(array('Licences are not matching : ' . $err1));
                }
              }
            } else {
              $error = 'successfully Added';
            }
            return back()->withErrors($error);
          }
        } else {
          return back()->withErrors('Please upload valid extension(.xls,.csv) file');
        }
      }
    }
  */
}
