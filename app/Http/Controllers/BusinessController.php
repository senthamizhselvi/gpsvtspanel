<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use App\Models\User;
use App\Models\AuditFrans;
use App\Models\AuditOrg;
use App\Models\AuditDealer;
use App\Models\AuditVehicle;
use App\Models\AuditUser;
use App\Models\AuditGroup;
use Exception;
use Facade\FlareClient\Http\Response;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Kreait\Firebase\Exception\FirebaseException;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class BusinessController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $username = auth()->id();

    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

    if (session()->get('cur') == 'admin') {

      $franDetails_json = $redis->hget('H_Franchise', $fcode);
      $franchiseDetails = json_decode($franDetails_json, true);
      $prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
      if ($prepaid == 'yes') {
        $availableStarterLicence = isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : '0';
        $availableStarterPlusLicence = isset($franchiseDetails['availableStarterPlusLicence']) ? $franchiseDetails['availableStarterPlusLicence'] : '0';
        $availableBasicLicence = isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';
        $availableAdvanceLicence = isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';
        $availablePremiumLicence = isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';
        $availablePremPlusLicence = isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0';

        $availableLincence = $availableStarterPlusLicence + $availableStarterLicence + $availableBasicLicence + $availableAdvanceLicence + $availablePremiumLicence + $availablePremPlusLicence;
        $viewData = [
          'availableStarterLicence' => $availableStarterLicence,
          'availableStarterPlusLicence' => $availableStarterPlusLicence,
          'availableBasicLicence' => $availableBasicLicence,
          'availableAdvanceLicence' => $availableAdvanceLicence,
          'availablePremiumLicence' => $availablePremiumLicence,
          'availablePremPlusLicence' => $availablePremPlusLicence,
          'availableLincence' => $availableLincence,
          'numberofdevice' => 0
        ];
        return view('vdm.business.prepaidAddDevice', $viewData);
      } else {
        $availableLincence = isset($franchiseDetails['availableLincence']) ? $franchiseDetails['availableLincence'] : '0';
        $viewData = [
          'availableLincence' => $availableLincence,
        ];
        return view('vdm.business.postpaidAddDevice', $viewData);
      }
    } else if (session()->get('cur') == 'dealer') {   //prepaid dealer
      if (session()->get('cur1') == 'prePaidAdmin') {
        $dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
        $dealersDetails = json_decode($dealersDetails_json, true);
        $availableStarterLicence = isset($dealersDetails['avlofStarter']) ? $dealersDetails['avlofStarter'] : '0';
        $availableStarterPlusLicence = isset($dealersDetails['avlofStarterPlus']) ? $dealersDetails['avlofStarterPlus'] : '0';
        $availableBasicLicence = isset($dealersDetails['avlofBasic']) ? $dealersDetails['avlofBasic'] : '0';
        $availableAdvanceLicence = isset($dealersDetails['avlofAdvance']) ? $dealersDetails['avlofAdvance'] : '0';
        $availablePremiumLicence = isset($dealersDetails['avlofPremium']) ? $dealersDetails['avlofPremium'] : '0';
        $availablePremPlusLicence = isset($dealersDetails['avlofPremiumPlus']) ? $dealersDetails['avlofPremiumPlus'] : '0';

        $availableLincence = $availableStarterLicence + $availableStarterPlusLicence + $availableBasicLicence + $availableAdvanceLicence + $availablePremiumLicence + $availablePremPlusLicence;

        $numberofdevice = 0;
        $viewData = [
          'availableStarterLicence' => $availableStarterLicence,
          'availableStarterPlusLicence' => $availableStarterPlusLicence,
          'availableBasicLicence' => $availableBasicLicence,
          'availableAdvanceLicence' => $availableAdvanceLicence,
          'availablePremiumLicence' => $availablePremiumLicence,
          'availablePremPlusLicence' => $availablePremPlusLicence,
          'availableLincence' => $availableLincence,
          'numberofdevice' => $numberofdevice
        ];
        return view('vdm.business.prepaidAddDevice', $viewData);
      }

      $key = 'H_Pre_Onboard_Dealer_' . $username . '_' . $fcode;
      $details = $redis->hgetall($key);
      $devices = null;
      $devicestypes = null;
      $i = 0;
      foreach ($details as $key => $value) {
        $valueData = json_decode($value, true);
        $devices = Arr::add($devices, $i, $valueData['deviceid']);
        $devicestypes = Arr::add($devicestypes, $i, $valueData['deviceidtype']);
        $i++;
      }

      $dealerId = $redis->smembers('S_Dealers_' . $fcode);

      $orgArr = array();
      if ($dealerId != null) {
        foreach ($dealerId as $org) {
          $orgArr = Arr::add($orgArr, $org, $org);
        }
        $dealerId = $orgArr;
      } else {
        $dealerId = null;
        $dealerId = $orgArr;
      }

      $redisUserCacheId = 'S_Users_' . $fcode; //dummy installation
      $redisGrpCacheId = 'S_Users_';

      if (session()->get('cur') == 'dealer') {
        $redisUserCacheId = 'S_Users_Dealer_' . $username . '_' . $fcode;
      } else if (session()->get('cur') == 'admin') {
        $redisUserCacheId = 'S_Users_Admin_' . $fcode;
      }

      $userList = $redis->smembers($redisUserCacheId);
      $orgArra = array();
      $orgArra = Arr::add($orgArra, 'select', trans('messages.select'));
      foreach ($userList as $org) {
        if (!$redis->sismember('S_Users_Virtual_' . $fcode, $org) == 1) {
          $orgArra = Arr::add($orgArra, $org, $org);
        }
      }

      $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
      if (session()->get('cur') == 'dealer') {
        $tmpOrgList = $redis->smembers('S_Organisations_Dealer_' . $username . '_' . $fcode);
      } else if (session()->get('cur') == 'admin') {
        $tmpOrgList = $redis->smembers('S_Organisations_Admin_' . $fcode);
      }
      $orgList = null;
      $orgList = Arr::add($orgList, '',  trans('messages.select'));
      $orgList = Arr::add($orgList, 'Default', 'Default');
      foreach ($tmpOrgList as $org) {
        $orgList = Arr::add($orgList, $org, $org);
      }
      $userList = $orgArra;
      $vehicleModal = ['Ambulance' => 'Ambulance', 'Bike' => 'Bike', 'Bull' => 'Bull', 'Bus' => 'Bus', 'Car' => 'Car', 'heavyVehicle' => 'Heavy Vehicle', 'Tanker' => 'Tanker', 'Truck' => 'Truck', 'Van' => 'Van', 'JCB' => 'JCB', 'Generator' => 'Generator'];
      $vehicleModalList = Arr::set($vehicleModal, '', trans('messages.VehicleType'));
      $vehicleModalList = Arr::sort($vehicleModalList);
      $viewData = [
        'devices' => $devices,
        'devicestypes' => $devicestypes,
        'dealerId' => $dealerId,
        'userList' => $userList,
        'orgList' => $orgList,
        'vehicleModalList'=>$vehicleModalList
      ];
      return view('vdm.business.business1', $viewData);
    }
  }


  public function create()
  {
    DatabaseConfig::checkDb();
    return self::index();
  }

  public function checkUser()
  {
    $error = '';
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $fcode1 = strtoupper($fcode);
    $userId = request()->get('id');
    $userId1 = strtoupper($userId);
    $val = $redis->hget('H_UserId_Cust_Map', $userId . ':fcode');
    $valFirst = $redis->hget('H_UserId_Cust_Map',  ucfirst(strtolower($userId)) . ':fcode');
    $valUpper = $redis->hget('H_UserId_Cust_Map', strtolower($userId) . ':fcode');
    $val1 = $redis->sismember('S_Users_' . $fcode, $userId);
    $valOrg = $redis->sismember('S_Organisations_' . $fcode, $userId);
    $valOrg1 = $redis->sismember('S_Organisations_Admin_' . $fcode, $userId);
    $valGroup = $redis->sismember('S_Groups_' . $fcode, $userId1 . ':' . $fcode1);
    $valGroup1 = $redis->sismember('S_Groups_Admin_' . $fcode, $userId1 . ':' . $fcode1);

    if ($valGroup == 1 || $valGroup1 == 1) {
      $error = 'Name already exist';
    }
    if ($valOrg == 1 || $valOrg1 == 1) {
      $error = 'Name already exist';
    }
    // $userExist = FirebaseController::getUser($userId1) !== null;
    $userExist = false;
    if ($val1 == 1 || isset($val) || isset($valFirst) || isset($valUpper) || $userExist) {
      $error = 'User Id already exist';
    }
    if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) {
      $error = 'Name with admin not acceptable';
    }

    $refDataArr = [
      'error' => $error
    ];

    return response()->json($refDataArr);
  }
  public function checkDevice()
  {

    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

    $deviceid = request()->get('id');

    $vehicleU = strtoupper($deviceid);
    $dev = $redis->hget('H_Device_Cpy_Map', $deviceid);
    $vehicleIdCheck = $redis->sismember('S_Vehicles_' . $fcode, $deviceid);
    $vehicleIdCheck2 = $redis->sismember('S_KeyVehicles', $vehicleU);
    $vehicleIdCheck3 = $redis->HEXISTS('H_Vehicle_Device_Map_' . $fcode, $deviceid);
    $error = ' ';
    if ($dev !== null || $vehicleIdCheck == 1 || $vehicleIdCheck2 == 1 || $vehicleIdCheck3 == 1) {
      $error = 'Device Id already present ' . $deviceid;
    }
    $refDataArr = array(

      'error' => $error

    );
    $refDataJson = json_encode($refDataArr);

    logger('changes value ' . $error);
    return response()->json($refDataArr);
  }

  public function checkvehicle()
  {

    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

    $vehicleId = request()->get('id');
    $vehicleU = strtoupper($vehicleId);
    $vehicleIdCheck = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
    $vehicleIdCheck2 = $redis->sismember('S_KeyVehicles', $vehicleU);
    $dev = $redis->hget('H_Device_Cpy_Map', $vehicleId);
    $error = ' ';
    if ($vehicleIdCheck == 1 || $vehicleIdCheck2 == 1 || $dev != null) {
      $error = trans('messages.VehicleIdalreadypresent') . $vehicleId;
    }
    $refDataArr = array(
      'error' => $error
    );
    $refDataJson = json_encode($refDataArr);
    return response()->json($refDataArr);
  }

  public function getGroup()
  {

    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

    $userId = request()->get('id');
    $userId = str_replace('$', '.', $userId);
    $vehRfidYesList = array();
    $vehicleGroups = $redis->smembers($userId);
    $vehicleGroups = str_replace('.', '$', $vehicleGroups);

    foreach ($vehicleGroups as $vehicle) {
      $vehRfidYesList = Arr::add($vehRfidYesList, $vehicle, $vehicle);
    }
    $vehRfidYesList = str_replace('$', '.', $vehRfidYesList);
    $error = ' ';
    if (count($vehicleGroups) == 0) {
      $error = trans('messages.Nogroupsavailable,Pleaseselectanotheruser');
    }

    $refDataArr = array(
      'groups' => $vehRfidYesList,
      'error' => $error,
    );

    $refDataJson = json_encode($refDataArr);

    return response()->json($refDataArr);
  }

  public function store()
  {
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $prepaid = 'no';
    if (session()->get('cur1') == 'prePaidAdmin') {
      $rules = array(
        'LicenceType' => 'required',
      );
      $validator = validator()->make(request()->all(), $rules);
      if ($validator->fails()) {
        return redirect()->back()->withInput()->withErrors($validator);
      }
      $prepaid = 'yes';
      $onStarterdevice = request()->get('onStarterdevice');
      $onStarterPlusdevice = request()->get('onStarterPlusdevice');
      $onBasicdevice = request()->get('onBasicdevice');
      $onAdvancedevice = request()->get('onAdvancedevice');
      $onPremiumdevice = request()->get('onPremiumdevice');
      $onPremiumPlusdevice = request()->get('onPremiumPlusdevice');

      if (session()->get('cur') == 'admin') {
        $franDetails_json = $redis->hget('H_Franchise', $fcode);
        $franchiseDetails = json_decode($franDetails_json, true);
        $availableStarterPlusLicence = isset($franchiseDetails['availableStarterPlusLicence']) ? $franchiseDetails['availableStarterPlusLicence'] : '0';
        $availableStarterLicence = isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : '0';
        $availableBasicLicence = isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';
        $availableAdvanceLicence = isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';
        $availablePremiumLicence = isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';
        $availablePremPlusLicence = isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0';
      } else if (session()->get('cur') == 'dealer') {
        $dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
        $dealersDetails = json_decode($dealersDetails_json, true);
        $availableStarterPlusLicence = isset($dealersDetails['avlofStarterPlus']) ? $dealersDetails['avlofStarterPlus'] : '0';
        $availableStarterLicence = isset($dealersDetails['avlofStarter']) ? $dealersDetails['avlofStarter'] : '0';
        $availableBasicLicence = isset($dealersDetails['avlofBasic']) ? $dealersDetails['avlofBasic'] : '0';
        $availableAdvanceLicence = isset($dealersDetails['avlofAdvance']) ? $dealersDetails['avlofAdvance'] : '0';
        $availablePremiumLicence = isset($dealersDetails['avlofPremium']) ? $dealersDetails['avlofPremium'] : '0';
        $availablePremPlusLicence = isset($dealersDetails['avlofPremiumPlus']) ? $dealersDetails['avlofPremiumPlus'] : '0';
      }
      if ($onStarterPlusdevice == null && $onStarterdevice == null && $onBasicdevice == null && $onAdvancedevice == null && $onPremiumdevice == null && $onPremiumPlusdevice == null) {
        return redirect()->back()->withInput()->withErrors(trans('messages.PleaseEntervalidLicenceQuantity'));
      }  else if ($availableStarterPlusLicence < $onStarterPlusdevice) {
        return redirect()->back()->withInput()->withErrors('Starter Plus'.trans('messages.LicenceTypeisnotavailable'));
      } else if ($availableStarterLicence < $onStarterdevice) {
        return redirect()->back()->withInput()->withErrors('Starter'.trans('messages.LicenceTypeisnotavailable'));
      } else if ($availableBasicLicence < $onBasicdevice) {
        return redirect()->back()->withInput()->withErrors('Basic'.trans('messages.LicenceTypeisnotavailable'));
      } else if ($availableAdvanceLicence < $onAdvancedevice) {
        return redirect()->back()->withInput()->withErrors('Advance'.trans('messages.LicenceTypeisnotavailable'));
      } else if ($availablePremiumLicence < $onPremiumdevice) {
        return redirect()->back()->withInput()->withErrors('Premium'.trans('messages.LicenceTypeisnotavailable'));
      } else if ($availablePremPlusLicence < $onPremiumPlusdevice) {
        return redirect()->back()->withInput()->withErrors('Premium Plus'.trans('messages.LicenceTypeisnotavailable'));
      } else {

        $LicenceType = request()->get('LicenceType');
        $deviceType = 'on' . $LicenceType . 'device';
        $numberofdevice = $$deviceType;
        $protocol = VdmFranchiseController::getProtocal($LicenceType);
        session([
          'numberofdevice' => $numberofdevice,
          'protocol' => $protocol,
          'LicenceType' => $LicenceType,
          'prepaid' => $prepaid
        ]);
      }
    } else {
      $rules = array(
        'numberofdevice' => 'required|numeric|min:1'
      );
      $prepaid = 'no';
      $validator = validator()->make(request()->all(), $rules);
      $availableLincence = request()->get('availableLincence');
      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }
      $numberofdevice = request()->get('numberofdevice');
      if ($numberofdevice > $availableLincence) {
        return back()->withErrors("Your license count is less")->withInput();
      }
      session([
        'numberofdevice' => $numberofdevice,
        'availableLincence' => $availableLincence,
        'prepaid' => $prepaid
      ]);
    }
    return redirect()->to('onBoardDevice');
  }

  public function onBoardDevice()
  {

    $numberofdevice = session()->get('numberofdevice');
    if (is_null($numberofdevice)) {
      return back()->withErrors("Your license count is less")->withInput();
      // return redirect('Business');
    }
    $availableLincence = session()->get('availableLincence');
    $prepaid = session()->get('prepaid');
    $protocol = session()->get('protocol');
    $LicenceType = session()->get('LicenceType');

    if (session()->get('cur1') == 'prePaidAdmin') {
      return self::prepaid($numberofdevice, $protocol, $LicenceType, $prepaid);
    } else {
      return self::notprepaid($numberofdevice, $availableLincence, $prepaid);
    }
  }

  public static function prepaid($numberofdevice, $protocol, $LicenceType, $prepaid)
  {
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $userList = BusinessController::getUser();
    $orgList = BusinessController::getOrg();

    $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
    $servername = $franchiesJson;
    $servername = "209.97.163.4";
    if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
      return 'Ipaddress Failed !!!';
    }
    $usernamedb = "root";
    $password = "#vamo123";
    $dbname = $fcode;
    $maxcount = 0;
    if (request()->server("HTTP_HOST") !== "localhost") {
      $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
      if (!$conn) {
        die('Could not connect: ' . mysqli_connect_error());
        return 'Please Update One more time Connection failed';
      } else {
        $max = "select id from BatchMove order by id desc limit 1";
        $results = mysqli_query($conn, $max);
        while ($row = mysqli_fetch_array($results)) {
          $maxcount = $row[0];
        }
        $conn->close();
      }
    }
    $rowcount = $maxcount + 1;
    $deviceId = array();
    $vehicleId = array();
    $LicenceId = array();
    $n = 2;
    for ($i = 1; $i <= $numberofdevice; $i++) {
      $j = $rowcount + 400000;
      $fid = strtoupper($fcode);
      $devices = "GPSNEW_" . $j . "_" . $fid;
      $vehicles = $devices . "1";
      $dev = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $devices);
      $veh = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicles);

      for ($m = 1; $m <= $n; $m++) {
        if ($dev == null && $veh == null) {
          $deviceCheck = $devices;
          $vehicles = $deviceCheck . "1";
          $deviceId = Arr::add($deviceId, $i, $deviceCheck);
          $vehicleId = Arr::add($vehicleId, $i, $vehicles);
          if ($LicenceType == 'Starter') {
            $unique_id = strtoupper('ST' . uniqid());
          } else if ($LicenceType == 'StarterPlus') {
            $unique_id = strtoupper('SP' . uniqid());
          } else if ($LicenceType == 'Basic') {
            $unique_id = strtoupper('BC' . uniqid());
          } else if ($LicenceType == 'Advance') {
            $unique_id = strtoupper('AV' . uniqid());
          } else if ($LicenceType == 'Premium') {
            $unique_id = strtoupper('PM' . uniqid());
          } else if ($LicenceType == 'PremiumPlus') {
            $unique_id = strtoupper('PL' . uniqid());
          }
          $LicenceId = Arr::add($LicenceId, $i, $unique_id);
          $rowcount = $rowcount + 1;
          break;
        } else {
          $j = $rowcount + 400000 + 1;
          $devices = "GPSNEW_" . $j . "_" . $fid;
          $vehicles = $devices . "1";
          $dev = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $devices);
          $veh = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicles);
          $n++;
          $rowcount = $rowcount + 1;
        }
      }
    }

    // $hidLicence = $LicenceType;
    $Licence1 = $LicenceType;
    $vehicleModal = ['Ambulance' => 'Ambulance', 'Bike' => 'Bike', 'Bull' => 'Bull', 'Bus' => 'Bus', 'Car' => 'Car', 'heavyVehicle' => 'Heavy Vehicle', 'Tanker' => 'Tanker', 'Truck' => 'Truck', 'Van' => 'Van', 'JCB' => 'JCB', 'Generator' => 'Generator'];
    $vehicleModalList = Arr::set($vehicleModal, '', trans('messages.VehicleType'));
    $vehicleModalList = Arr::sort($vehicleModalList);

    $createCopyData = [
      'orgList' => $orgList,
      'numberofdevice' => $numberofdevice,
      'userList' => $userList,
      'Licence' => $Licence1,
      'protocol' => $protocol,
      'devices' => $deviceId,
      'vehicles' => $vehicleId,
      'LicenceId' => $LicenceId,
      'vehicleModalList' => $vehicleModalList,
      'licenceType' => $LicenceType
      // 'availableLincence' => $availableLincence,
      // 'dealerId' => $dealerIdList,
      // 'Payment_Mode' => $Payment_Mode1,
      // 'hidLicence' => $hidLicence,
    ];
    return view('vdm.business.prepaidCreate', $createCopyData);
  }

  public static function notprepaid($numberofdevice, $availableLincence, $prepaid)
  {
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

    $dealerIdList = $redis->smembers('S_Dealers_' . $fcode);
    // $orgArr = array();
    if ($dealerIdList == null) $dealerIdList = [];
    $dealerIdList = array_combine($dealerIdList, $dealerIdList);

    $userList = BusinessController::getUser();
    $orgList = BusinessController::getOrg();
    $protocol = BusinessController::getProtocal();

    $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
    $servername = $franchiesJson;
    $servername = "209.97.163.4";
    if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
      return 'Ipaddress Failed !!!';
    }
    $usernamedb = "root";
    $password = "#vamo123";
    $dbname = $fcode;
    $maxcount = 0;
    if (request()->server("HTTP_HOST") !== "localhost") {
      $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
      if (!$conn) {
        die('Could not connect: ' . mysqli_connect_error());
        return 'Please Update One more time Connection failed';
      } else {
        $max = "select id from BatchMove order by id desc limit 1";
        $results = mysqli_query($conn, $max);
        while ($row = mysqli_fetch_array($results)) {
          $maxcount = $row[0];
        }
        $conn->close();
      }
    }
    $rowcount = $maxcount + 1;
    $deviceId = array();
    $vehicleId = array();
    $n = 2;
    for ($i = 1; $i <= $numberofdevice; $i++) {
      $j = $rowcount + 400000;
      $fid = strtoupper($fcode);
      $devices = "GPSNEW_" . $j . "_" . $fid;
      $vehicles = $devices . "1";
      $dev = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $devices);
      $veh = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicles);
      for ($m = 1; $m <= $n; $m++) {
        if ($dev == null && $veh == null) {
          $deviceCheck = $devices;
          $vehicles = $deviceCheck . "1";
          $deviceId = Arr::add($deviceId, $i, $deviceCheck);
          $vehicleId = Arr::add($vehicleId, $i, $vehicles);
          $rowcount = $rowcount + 1;
          break;
        } else {
          $j = $rowcount + 300000 + 1;
          $devices = "GPSNEW_" . $j . "_" . $fid;
          $vehicles = $devices . "1";
          $dev = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $devices);
          $veh = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicles);
          $n++;
          $rowcount = $rowcount + 1;
        }
      }
    }
    $vehicleModalList = ['Ambulance' => 'Ambulance', 'Bike' => 'Bike', 'Bull' => 'Bull', 'Bus' => 'Bus', 'Car' => 'Car', 'heavyVehicle' => 'Heavy Vehicle', 'Tanker' => 'Tanker', 'Truck' => 'Truck', 'Van' => 'Van', 'JCB' => 'JCB', 'Generator' => 'Generator'];
    $vehicleModalList = Arr::set($vehicleModalList, '', '-- Vehicle Type --');
    $vehicleModalList = Arr::sort($vehicleModalList);
    $createCopyData = [
      'orgList' => $orgList,
      'availableLincence' => $availableLincence,
      'numberofdevice' => $numberofdevice,
      'dealerIdList' => $dealerIdList,
      'userList' => $userList,
      'protocol' => $protocol,
      'deviceId' => $deviceId,
      'vehicleId' => $vehicleId,
      'vehicleModalList' => $vehicleModalList
    ];
    return view('vdm.business.postpaidCreate', $createCopyData);
  }

  public static function getProtocal()
  {

    $redis = Redis::connection();
    $protocal = $redis->lrange('L_Protocal', 0, -1);
    sort($protocal);
    $getProtocal = array();
    foreach ($protocal as $pro) {
      $value  = explode(":", $pro);
      $check = isset($getProtocal[$value[0]]);
      $len = strlen($check);
      if ($len == 0) {
        $getProtocal = Arr::add($getProtocal, $value[0], $value[0] . ' (' . $value[1] . ') ');
      } else {
        $getProtocal = Arr::add($getProtocal, $value[0] . '1', $value[0] . ' (' . $value[1] . ') ');
      }
    }
    return $getProtocal;
  }

  public static function getUser()
  {
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $redisUserCacheId = 'S_Users_' . $fcode; //dummy installation
    $redisGrpCacheId = 'S_Users_';

    if (session()->get('cur') == 'dealer') {
      $redisUserCacheId = 'S_Users_Dealer_' . $username . '_' . $fcode;
    } else if (session()->get('cur') == 'admin') {
      $redisUserCacheId = 'S_Users_Admin_' . $fcode;
    }
    $userList = $redis->smembers($redisUserCacheId);
    // $userList = str_replace('.', '$', $userList);
    $virtualUsers = $redis->smembers("S_Users_Virtual_$fcode");

    if ($userList == null) $userList = [];
    if ($virtualUsers == null) $virtualUsers = [];

    $orgArra = array_diff($userList, $virtualUsers);

    $orgArra = array_combine($orgArra, $orgArra);
    $orgArra = Arr::add($orgArra, 'select',  trans('messages.select'));
    // logger($orgArra);

    // $userList = str_replace('$', '.', $userList);
    // logger($userList);
    return $orgArra;
  }

  public static function getdealer()
  {
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $dealerIdList = $redis->smembers('S_Dealers_' . $fcode);

    if ($dealerIdList == null) $dealerIdList = [];
    $dealerIdList = array_combine($dealerIdList, $dealerIdList);
    return $dealerIdList;
  }

  public static function getOrg()
  {
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $orgList = null;
    $orgListId = 'S_Organisations_' . $fcode;
    if (session()->get('cur') == 'dealer') {
      $orgListId = 'S_Organisations_Dealer_' . $username . '_' . $fcode;
    } else if (session()->get('cur') == 'admin') {
      $orgListId = 'S_Organisations_Admin_' . $fcode;
    }
    $orgList = $redis->smembers($orgListId);
    if ($orgList == null) $orgList = [];
    // $orgList = str_replace('.', '$', $orgList);

    $orgArray = array_combine($orgList, $orgList);
    $orgArray = Arr::add($orgArray, '',  trans('messages.select'));

    // $orgList = str_replace('$', '.', $orgArray);
    return $orgArray;
  }

  /***
  public function adddeviceOld()
  {
    try {

      $username = auth()->id();
      $redis = Redis::connection();
      $auth = app('firebase.auth');
      $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
      $fcode1 = strtoupper($fcode);
      $ownerShip      = request()->get('dealerId');
      $userId      = request()->get('userId');
      $userId = str_replace('$', '.', $userId);
      $userId1 = strtoupper($userId);
      $mobileNoUser      = request()->get('mobileNoUser');
      $emailUser      = request()->get('emailUser');
      $password      = request()->get('password');
      $type      = request()->get('type');
      $type1      = request()->get('type1');

      $numberofdevice = request()->get('numberofdevice1');
      $hidLicence = request()->get('hidLicence');

      $groupnameId = array();
      $groupnameId[0] = $userId;
      $userList = array();
      $userList = BusinessController::getUser();
      $orgList = array();
      $orgList = BusinessController::getOrg();
      $dealerId = array();
      $dealerId = BusinessController::getdealer();

      $franDetails_json = $redis->hget('H_Franchise', $fcode);
      $franchiseDetails = json_decode($franDetails_json, true);
      $prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
      $availableLincence = isset($franchiseDetails['availableLincence']) ? $franchiseDetails['availableLincence'] : '0';
      $timeZone = isset($franchiseDetails['timeZone']) ? $franchiseDetails['timeZone'] : 'Asia/Kolkata';

      if ($prepaid == 'yes') {
        $protocol = VdmFranchiseController::getProtocal($hidLicence);
      } else {
        $protocol = BusinessController::getProtocal();
      }

      $Payment_Mode1 = array();
      $Payment_Mode = DB::select('select type from Payment_Mode');
      foreach ($Payment_Mode as  $org1) {
        $Payment_Mode1 = Arr::add($Payment_Mode1, $org1->type, $org1->type);
      }
      $Licence1 = array();
      $Licence = DB::select('select type from Licence');
      foreach ($Licence as  $org) {
        $Licence1 = Arr::add($Licence1, $org->type, $org->type);
      }
      //thiru
      if ($type1 != null && $type1 == 'new') {
        if (session()->get('cur') == 'dealer') {
          logger('------login 1---------- ' . session()->get('cur'));

          $totalReports = $redis->smembers('S_Users_Reports_Dealer_' . $username . '_' . $fcode);
        } else if (session()->get('cur') == 'admin') {
          $totalReports = $redis->smembers('S_Users_Reports_Admin_' . $fcode);
        }
        if ($totalReports != null) {
          foreach ($totalReports as $key => $value) {
            $redis->sadd('S_Users_Reports_' . $userId . '_' . $fcode, $value);
          }
        }
        // addReportsForNewUser($userId);
      }
      if ($type1 == 'existing') {
        $userId      = request()->get('userIdtemp');
        if ($userId == null || $userId == 'select') {
          session()->flash('message', 'Please select user Id !');
          return redirect()->to('Business')->withErrors('Please select user Id !');
        }
      }
      if ($type == null && session()->get('cur') == 'admin') {
        logger($ownerShip . 'valuse ----------->' . request()->get('userIdtemp'));
        session()->flash('message', 'select the sale!');
        //return view ( 'vdm.business.store');
        return redirect()->back()->withInput()->withErrors('select the sale!');
        //return view ( 'vdm.business.create' )->withErrors ( "select the sale!" )->with ( 'orgList', null )->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', $numberofdevice )->with ( 'dealerId', $dealerId )->with ( 'userList', $userList )->with('orgList',$orgList);

      }
      if ($type == 'Sale' && session()->get('cur') != 'dealer') {
        logger($ownerShip . '1 ----------->' . $type);

        $ownerShip      = 'OWN';
      }
      if (session()->get('cur') == 'dealer') {
        logger($ownerShip . '2 --a--------->' . session()->get('cur'));
        if ($type1 == null) {
          //return redirect()->to ( 'Business' )->withErrors ( 'select the sale' );
          return redirect()->back()->withErrors('select the sale!');
        }
        $type = 'Sale';
        $ownerShip = $username;
        $mobArr = explode(',', $mobileNoUser);
      }
      if ($type == 'Sale' && $type1 == null) {
        //return redirect()->to ( 'Business' )->withErrors ( 'Select the user' );
        return redirect()->back()->withErrors('Select the user !');
      }
      if ($type == 'Sale' && $type1 == 'new') {
        logger($ownerShip . '3----a------->' . session()->get('cur'));
        $rules = array(
          'userId' => 'required|alpha_dash',
          'emailUser' => 'required|email',
          'mobileNoUser' => 'required|numeric',
          'password' => 'required',

        );

        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator);
          //return redirect()->back()->withErrors('Select the user !');
        } else {
          $val = $redis->hget('H_UserId_Cust_Map', $userId . ':fcode');
          $val1 = $redis->sismember('S_Users_' . $fcode, $userId);
          $valOrg = $redis->sismember('S_Organisations_' . $fcode, $userId);
          $valOrg1 = $redis->sismember('S_Organisations_Admin_' . $fcode, $userId);
          $valGroup = $redis->sismember('S_Groups_' . $fcode, $userId1 . ':' . $fcode1);
          $valGroup1 = $redis->sismember('S_Groups_Admin_' . $fcode, $userId1 . ':' . $fcode1);
        }
        $Licence1 = $hidLicence;
        if ($valGroup == 1 || $valGroup1 == 1 || $valOrg == 1 || $valOrg1 == 1) {
          if (session()->get('cur1') == 'prePaidAdmin') {
            return redirect()->back()->withErrors('User Name already exist');
          } else {

            return view('vdm.business.create')->withErrors("User Name already exist")
              ->with('orgList', null)->with('availableLincence', $availableLincence)
              ->with('numberofdevice', $numberofdevice)->with('dealerId', $dealerId)
              ->with('userList', $userList)->with('Licence', $Licence1)->with('Payment_Mode', $Payment_Mode1)
              ->with('protocol', $protocol)->with('devices', null)->with('vehicles', null)
              ->with('LicenceId', null)->with('hidLicence', $hidLicence);
          }
        }
        if ($val1 == 1 || isset($val)) {
          if (session()->get('cur1') == 'prePaidAdmin') {
            return redirect()->back()->withErrors('User Id already exist');
          } else {
            return view('vdm.business.create')->withErrors("User Id already exist")
              ->with('orgList', null)->with('availableLincence', $availableLincence)
              ->with('numberofdevice', $numberofdevice)->with('dealerId', $dealerId)
              ->with('userList', $userList)->with('Licence', $Licence1)->with('Payment_Mode', $Payment_Mode1)
              ->with('protocol', $protocol)->with('devices', null)->with('vehicles', null)
              ->with('LicenceId', null)->with('hidLicence', $hidLicence);
          }
        }
        if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) {
          //return redirect()->back()->withErrors ( 'Name with admin not acceptable' );
          if (session()->get('cur1') == 'prePaidAdmin') {
            return redirect()->back()->withErrors('User Name with admin not acceptable');
          } else {
            return view('vdm.business.create')->withErrors("User Name with admin not acceptable")
              ->with('orgList', null)->with('availableLincence', $availableLincence)
              ->with('numberofdevice', $numberofdevice)->with('dealerId', $dealerId)
              ->with('userList', $userList)->with('Licence', $Licence1)->with('Payment_Mode', $Payment_Mode1)
              ->with('protocol', $protocol)->with('devices', null)->with('vehicles', null)
              ->with('LicenceId', null)->with('hidLicence', $hidLicence);
          }
        }


        $mobArr = explode(',', $mobileNoUser);
        foreach ($mobArr as $mob) {
          $val1 = $redis->sismember('S_Users_' . $fcode, $userId);
          if ($val1 == 1) {
            logger('id alreasy exist ' . $mob);
            //return redirect()->back()->withErrors ( );
            return view('vdm.business.create')->withErrors($mob . ' User Id already exist')
              ->with('orgList', null)->with('availableLincence', $availableLincence)
              ->with('numberofdevice', $numberofdevice)->with('dealerId', $dealerId)
              ->with('userList', $userList)->with('Licence', $Licence1)->with('Payment_Mode', $Payment_Mode1)
              ->with('protocol', $protocol)->with('devices', $deviceId)->with('vehicles', null)
              ->with('LicenceId', null)->with('hidLicence', $hidLicence);
          }
        }
      }
      if ($type == 'Sale') {

        $groupname1      = request()->get('groupname');
        $groupname1 = str_replace('$', '.', $groupname1);
        $groupname       = strtoupper($groupname1);
        $groupnameId     = explode(':', $groupname);
        logger($userId . '-------------- groupname- 1-------------' . $groupname);
        if ($type1 == 'existing' && ($groupname == null || $groupname == '' || $groupname == '0')) {
          return view('vdm.business.create')->withErrors($groupname . ' Invalid GroupName ')
            ->with('orgList', null)->with('availableLincence', $availableLincence)
            ->with('numberofdevice', $numberofdevice)->with('dealerId', $dealerId)
            ->with('userList', $userList)->with('Licence', $Licence1)->with('Payment_Mode', $Payment_Mode1)
            ->with('protocol', $protocol)->with('devices', $deviceId)->with('vehicles', null)
            ->with('LicenceId', null)->with('hidLicence', $hidLicence);
        }
      }
      logger('value type---->' . $type);
      $organizationId = $userId;
      $orgId = $organizationId;
      if ($groupnameId[0] === 0 || $groupnameId[0] === "0" || empty($groupnameId[0])) {
        $groupId = $userId;
      } else {
        $groupId = $groupnameId[0];
      }
      $groupId1 = strtoupper($groupId);
      if ($ownerShip == 'OWN' && $type != 'Sale') {
        $orgId = 'Default';
      }
      if ($userId == null) {
        $orgId = 'Default';
        $organizationId = 'Default';
      }




      $franDetails_json = $redis->hget('H_Franchise', $fcode);
      $franchiseDetails = json_decode($franDetails_json, true);
      if (isset($franchiseDetails['availableLincence']) == 1)
        $availableLincence = $franchiseDetails['availableLincence'];
      else
        $availableLincence = '0';
      $count = 0;
      $numberofdevice = request()->get('numberofdevice1');
      logger('--------number of  device::----------' . $numberofdevice);
      $vehicleIdarray = array();
      $deviceidarray = array();
      $KeyVehicles = array();
      $SpecialdChar = array();
      $emptyChar = array();
      $vehDevSame = array();
      $current = Carbon::now();
      $trkpt = $current->addYears(1)->format('Y-m-d');
      logger($trkpt);
      if ($type == 'Sale' && $type1 !== 'new') {
        $orgId = request()->get('orgId');
        $orgId = str_replace('$', '.', $orgId);
        if ($orgId == null && $redis->sismember('S_Organisations_' . $fcode, $userId) && $userId !== null) {
          $orgId = $userId;
        }
      }
      //ram vehicleExpiry default date

      $vehicleExpiry = request()->get('vehicleExpiry');
      logger('vehicle expired date--------------->');
      logger($vehicleExpiry);
      if ($fcode == 'TRKPT') {
        $vehicleExpiry1 = !empty($vehicleExpiry) ? $vehicleExpiry : $trkpt;
        $vehicleExpiry = $vehicleExpiry1;
      } else if ($prepaid == 'yes') {
        $vehicleExpiry = date('Y-m-d', strtotime(date("Y-m-d", time()) . " + 1 year"));
      } else {
        $vehicleExpiry = !empty($vehicleExpiry) ? $vehicleExpiry : $trkpt;
      }
      $current = Carbon::now();

      if ($type == 'Sale') {
        $mailSubject = 'Licences Onboarded to User - ' . $userId;
        $dealerOrUser = 'User Name';
        $onboardDate = $current->format('d-m-Y');
        if ($prepaid == 'yes') {
          $LicenceOnboardDate = $current->format('d-m-Y');
          $LicenceExpiryDate = date('d-m-Y', strtotime(date("d-m-Y", time()) . "+ 1 year"));
        } else {
          $LicenceExpiryDate = '';
          $LicenceOnboardDate = '';
        }
      } else {
        $mailSubject = 'Licences Added to Dealer - ' . $ownerShip;
        $dealerOrUser = 'Dealer Name';
        $onboardDate = '';
        $vehicleExpiry = '';
        $LicenceExpiryDate = '';
        $LicenceOnboardDate = '';
      }
      $licissueddate = $current->format('d-m-Y');

      //
      $devicearray = array();
      $dbarray = array();
      $dbtemp = 0;
      logger($numberofdevice);
      $typeship = $type;
      for ($i = 1; $i <= $numberofdevice; $i++) {
        //$vehi=request()->get ( 'vehicleIdSale43');
        if ($typeship == 'Sale') {

          if ($i >= 44) {
            $typeship = 'Move';
          } else {
            $deviceid = request()->get('deviceid' . $i);
            $vehicleId1 = request()->get('vehicleId' . $i);
          }
        } else {
          if ($i >= 44) {
            $j = $i - 1;
            $vehicleId43 = $vehicleId1;
            $fid = strtoupper($fcode);
            $sep = $fid . "1";
            $mobArr = explode($sep, $vehicleId43);
            $ram1 = explode('GPSNEW_', $mobArr[0]);
            $addLic = $numberofdevice - $j;
            $totalLic = $ram1[1] + $addLic;
            $autoGeNumber = $ram1[1];
            $autoGeNumber1 = $autoGeNumber + 1;

            $deviceid = 'GPSNEW_' . $autoGeNumber1 . '_' . $fid;
            $vehicleId1 = 'GPSNEW_' . $autoGeNumber1 . '_' . $sep;
          } else {
            $deviceid = request()->get('deviceidSale' . $i);
            $vehicleId1 = request()->get('vehicleIdSale' . $i);
          }
        }
        //
        $vehicleId1 = preg_replace('/[ ]{2,}|[\t]/', '', trim($vehicleId1));
        $deviceid = preg_replace('/[ ]{2,}|[\t]/', '', trim($deviceid));
        logger('After trim vehicleId ' . $vehicleId1);
        $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/';
        if (preg_match($pattern, $vehicleId1)) {
          $vehcle = str_replace('.', '^', $vehicleId1);
          $SpecialdChar = Arr::add($SpecialdChar, $vehcle, $vehcle);
          // return redirect()->back()->withErrors ('Vehicle ID should be in alphanumeric with _ or - Characters '.$vehicleId1);
        } else if (preg_match($pattern, $deviceid)) {
          $devid = str_replace('.', '^', $deviceid);
          $SpecialdChar = Arr::add($SpecialdChar, $devid, $devid);
          //return redirect()->back()->withErrors ( 'Device ID should be in alphanumeric with _ or - Characters '.$deviceid);
        } else if ($vehicleId1 == null || $deviceid == null) {
          if ($vehicleId1 == null) {
            $emptyId = $deviceid;
          } else {
            $emptyId = $vehicleId1;
          }
          $emptyChar = Arr::add($emptyChar, $emptyId, $emptyId);
          //return redirect()->back()->withErrors ( 'Device Id or Vehicle Id is empty');
        } else if ($vehicleId1 == $deviceid) {
          $vehDevSame = Arr::add($vehDevSame, $vehicleId1, $vehicleId1);
        }

        //  $vehicleId2 = str_replace('.', '-', $vehicleId1);
        else {
          $vehicleId = strtoupper($vehicleId1);

          $vehicleId = !empty($vehicleId) ? $vehicleId : 'GPSVTS_' . substr($deviceid, -7);
          //isset($vehicleRefData['shortName'])?$vehicleRefData['shortName']:'nill';
          logger(request()->get('deviceidtype50') . '--------number of  name::----------' . $i);
          $deviceid = str_replace(' ', '', $deviceid);
          $vehicleId = str_replace(' ', '', $vehicleId);

          $devicearray = Arr::add($devicearray, $vehicleId1, $deviceid);
          $shortName1 = request()->get('shortName' . $i);
          $shortName = strtoupper($shortName1);
          $shortName = !empty($shortName) ? $shortName : $vehicleId;
          $regNo = request()->get('regNo' . $i);
          $regNo = !empty($regNo) ? $regNo : 'XXXX';

          $orgId = !empty($orgId) ? $orgId : 'Default';

          $vehicleType = request()->get('vehicleType' . $i);
          $vehicleType = !empty($vehicleType) ? $vehicleType : 'Bus';
          if ($prepaid == 'yes') {
            $LicenceId = request()->get('LicenceId' . $i);
          }
          $oprName = request()->get('oprName' . $i);
          $oprName = !empty($oprName) ? $oprName : 'Airtel';
          $mobileNo = request()->get('mobileNo' . $i);
          $mobileNo = !empty($mobileNo) ? $mobileNo : '0123456789';
          $odoDistance = request()->get('odoDistance' . $i);
          $odoDistance = !empty($odoDistance) ? $odoDistance : '0';
          $overSpeedLimit = request()->get('overSpeedLimit' . $i);
          $overSpeedLimit = !empty($overSpeedLimit) ? $overSpeedLimit : '60';
          $driverName = request()->get('driverName' . $i);
          $driverName = !empty($driverName) ? $driverName : '';
          $email = request()->get('email' . $i);
          $email = !empty($email) ? $email : '';
          //

          $altShortName = request()->get('altShortName' . $i);
          $altShortName = !empty($altShortName) ? $altShortName : '';
          $sendGeoFenceSMS = request()->get('sendGeoFenceSMS' . $i);
          $sendGeoFenceSMS = !empty($sendGeoFenceSMS) ? $sendGeoFenceSMS : 'no';
          $morningTripStartTime = request()->get('morningTripStartTime' . $i);
          $morningTripStartTime = !empty($morningTripStartTime) ? $morningTripStartTime : '';
          $eveningTripStartTime = request()->get('eveningTripStartTime' . $i);
          $eveningTripStartTime = !empty($eveningTripStartTime) ? $eveningTripStartTime : '';
          $gpsSimNo = request()->get('gpsSimNo' . $i);
          $gpsSimNo = !empty($gpsSimNo) ? $gpsSimNo : '0123456789';
          $Licence = request()->get('Licence' . $i);
          logger('licence type --------->' . $Licence);
          //$Licence=!empty($Licence) ? $Licence : 'Advance';
          $descriptionStatus = request()->get('descr' . $i);
          $descriptionStatus = !empty($descriptionStatus) ? $descriptionStatus : '';

          $Payment_Mode = request()->get('Payment_Mode' . $i);
          $Payment_Mode = !empty($Payment_Mode) ? $Payment_Mode : 'Monthly';

          if (($type == 'Sale') && ($i < 44)) {
            $deviceidtype = request()->get('deviceidtype' . $i);
            $deviceModel = $deviceidtype;
            //  $licence_id = DB::select('select licence_id from Licence where type = :type', ['type' => $Licence]);
            // $payment_mode_id = DB::select('select payment_mode_id from Payment_Mode where type = :type', ['type' => $Payment_Mode]);
            // logger($licence_id);

            // $licence_id = $licence_id[0]->licence_id;
            // $payment_mode_id = $payment_mode_id[0]->payment_mode_id;
          } else {
            $deviceidtype = 'BSTPL';
            $deviceModel = $deviceidtype;
            //  $licence_id = '1';
            //  $payment_mode_id = '4';
          }
          //logger($deviceid . '-------- av  in  ::----------' . $deviceidtype . ' ' . $payment_mode_id);
          if ($deviceid !== null && $deviceidtype !== null) {
            $dev = $redis->hget('H_Device_Cpy_Map', $deviceid);
            $dev1 = $redis->hget('H_Device_Cpy_Map', $vehicleId);
            $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
            //$back=$redis->hget($vehicleDeviceMapId, $deviceid);
            $back = $redis->HEXISTS($vehicleDeviceMapId, $deviceid);
            $backexist = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
            $devBack = $redis->sismember('S_Vehicles_' . $fcode, $deviceid);
            $back1 = $redis->sismember('S_KeyVehicles', $vehicleId);
            // $query = DB::table('Vehicle_details')->select('vehicle_id')->where('vehicle_id', $vehicleId)->get();
            if ($back == 1 || $devBack == 1 || !empty($dev1) || $backexist == 1) {
              $vehicleIdarray = Arr::add($vehicleIdarray, $vehicleId, $vehicleId);
              logger($vehicleIdarray);
            }
            if ($back1 == 1) {
              $KeyVehicles = Arr::add($KeyVehicles, $vehicleId, $vehicleId);
            }
            if (session()->get('cur') == 'dealer') {
              $tempdev = $redis->hget('H_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceid);
            } else if (session()->get('cur') == 'admin') {
              $tempdev = $redis->hget('H_Pre_Onboard_Admin_' . $fcode, $deviceid);
            }
            if (empty($dev1) && $dev == null && empty($tempdev) && $back == 0 && $back1 == 0 && $devBack == 0  && $backexist == 0) {
              $count++;
              $deviceDataArr = array(
                'deviceid' => $deviceid,
                'deviceidtype' => $deviceidtype
              );
              $deviceDataJson = json_encode($deviceDataArr);

              $deviceDataArr = array(
                'deviceid' => $deviceid,
                'deviceidtype' => $deviceModel,
              );
              $deviceDataJson = json_encode($deviceDataArr);
              $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
              $shortName = str_replace(' ', '', $shortName);
              $orgId1 = strtoupper($orgId);
              if ($ownerShip !== 'OWN') {
                $detailJson = $redis->hget('H_DealerDetails_' . $fcode, $ownerShip);
                $dealerDetail = json_decode($detailJson, true);
                $redis->sadd('S_Vehicles_Dealer_' . $ownerShip . '_' . $fcode, $vehicleId);
                $redis->srem('S_Vehicles_Admin_' . $fcode, $vehicleId);
                $redis->hset('H_VehicleName_Mobile_Dealer_' . $ownerShip . '_Org_' . $fcode, $vehicleId . ':' . $deviceid . ':' . $shortName . ':' . $orgId1 . ':' . $gpsSimNo, $vehicleId);
                $timeZone = isset($dealerDetail['timeZone']) ? $dealerDetail['timeZone'] : 'Asia/Kolkata';
                $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
                $servername = $franchiesJson;
                if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
                  return 'Ipaddress Failed !!!';
                }
                $usernamedb = "root";
                $password = "#vamo123";
                $dbname = $fcode;
                logger('franci..----' . $fcode);
                logger('ip----' . $servername);
                $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
                if (!$conn) {
                  die('Could not connect: ' . mysqli_connect_error());
                  return 'Please Update One more time Connection failed';
                } else {
                  logger(' created connection ');
                  $max = "SELECT COUNT(id) FROM BatchMove";
                  $results = mysqli_query($conn, $max);
                  while ($row = mysqli_fetch_array($results)) {
                    $maxcount = $row[0];
                  }
                  $j = $maxcount + $i;
                  $dateTime = Carbon::now();
                  $insertval = "INSERT INTO BatchMove (Device_Id, Dealer_Name, Vehicle_Id) VALUES ('$deviceid','$ownerShip','$vehicleId')";
                  $conn->multi_query($insertval);
                  $conn->close();
                }
              } else if ($ownerShip == 'OWN') {
                $redis->sadd('S_Vehicles_Admin_' . $fcode, $vehicleId);
                $redis->srem('S_Vehicles_Dealer_' . $ownerShip . '_' . $fcode, $vehicleId);
                $redis->hset('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $vehicleId . ':' . $deviceid . ':' . $shortName . ':' . $orgId1 . ':' . $gpsSimNo . ':OWN', $vehicleId);
              }

              $back = $redis->hget($vehicleDeviceMapId, $deviceid);
              if ($back !== null) {
                logger('------temp---------- ');
                $vehicleId = $back;
              } else {
                if (session()->get('cur') == 'dealer') {
                  $redis->sadd('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceid);
                  $redis->hset('H_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceid, $deviceDataJson);
                } else if (session()->get('cur') == 'admin') {
                  $redis->sadd('S_Pre_Onboard_Admin_' . $fcode, $deviceid);
                  $redis->hset('H_Pre_Onboard_Admin_' . $fcode, $deviceid, $deviceDataJson);
                }

                $v = idate("d");
                $monthTemp = idate("m");
                //logger($monthTemp.'------monthTemp---------- ');
                $paymentmonth = 11;
                if ($v > 15) {
                  logger('inside if');
                  $paymentmonth = $paymentmonth + 1;
                }
                if ($monthTemp == 1) {
                  if ($v == 29 || $v == 30 || $v == 31) {
                    $paymentmonth = 0;
                    $new_date = 'February ' . (date('Y', strtotime("0 month")) + 1);
                    $new_date2 = 'February' . (date('Y', strtotime("0 month")) + 1);
                    //logger($new_date.'------new_date feb---------- '.$new_date2);
                  }
                }
                for ($m = 1; $m <= $paymentmonth; $m++) {

                  $new_date = date('F Y', strtotime("$m month"));
                  $new_date2 = date('FY', strtotime("$m month"));
                  //logger($new_date.'------ownership---------- '.$m);
                }
                $new_date1 = date('F d Y', strtotime("+0 month"));
                $refDataArr = array(
                  'deviceId' => $deviceid,
                  'deviceModel' => $deviceModel,
                  'shortName' => $shortName,
                  'regNo' => $regNo,
                  'orgId' => $orgId,
                  'vehicleType' => $vehicleType,
                  'oprName' => $oprName,
                  'mobileNo' => $mobileNo,
                  'odoDistance' => $odoDistance,
                  'gpsSimNo' => $gpsSimNo,
                  'paymentType' => 'yearly',
                  'OWN' => $ownerShip,
                  'expiredPeriod' => $new_date,
                  'overSpeedLimit' => $overSpeedLimit,
                  'driverName' => $driverName,
                  'email' => $email,
                  //'vehicleExpiry' => $vehicleExpiry,
                  'altShortName' => $altShortName,
                  'sendGeoFenceSMS' => $sendGeoFenceSMS,
                  'morningTripStartTime' => $morningTripStartTime,
                  'eveningTripStartTime' => $eveningTripStartTime,
                  'parkingAlert' => 'no',
                  'vehicleMake' => '',
                  'Licence' => $Licence,
                  'Payment_Mode' => $Payment_Mode,
                  'descriptionStatus' => $descriptionStatus,
                  'vehicleExpiry' => $vehicleExpiry,
                  'onboardDate' => $onboardDate,
                  'tankSize' => '0',
                  'licenceissuedDate' => $licissueddate,
                  'communicatingPortNo' => '',
                  'country' => $timeZone,
                );
                $refDataJson = json_encode($refDataArr);
                logger($refDataJson);
                $expireData = $redis->hget('H_Expire_' . $fcode, $new_date2);
                // regNo & VehId Map
                $redis->hset('H_Vehicle_RegNum_Map_' . $fcode, $vehicleId, $regNo);
                $redis->hset('H_Vehicle_RegNum_Map_' . $fcode, $regNo, $vehicleId);

                $redis->hset('H_RefData_' . $fcode, $vehicleId, $refDataJson);

                $LatLong = $redis->hget('H_Franchise_LatLong', $fcode);

                logger('before AuditVehicle table insert');
                try {
                  $mysqlDetails = array();
                  $status = array();
                  $status = array(
                    'fcode' => $fcode,
                    'vehicleId' => $vehicleId,
                    'userName' => $username,
                    'status' => config()->get('constant.created'),
                    'userIpAddress' => session()->get('userIP'),
                  );
                  $mysqlDetails   = array_merge($status, $refDataArr);
                  $modelname = new AuditVehicle();
                  $table = $modelname->getTable();
                  $db = $fcode;
                  AuditTables::ChangeDB($db);
                  $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
                  if (count($tableExist) > 0) {
                    AuditVehicle::create($mysqlDetails);
                  } else {
                    AuditTables::CreateAuditVehicle();
                    AuditVehicle::create($mysqlDetails);
                  }
                  AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                } catch (Exception $e) {
                  AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                  logger('Error inside AuditVehicle Create' . $e->getMessage());
                  logger($mysqlDetails);
                }
                logger('successfully inserted into AuditVehicle');

                if ($prepaid == 'yes') {
                  $redis->hset('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId, $vehicleId);
                  $redis->hset('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleId, $LicenceId);

                  logger('Licence Expiry Date ---->' . $LicenceExpiryDate);
                  $LiceneceDataArr = array(
                    'LicenceissuedDate' => $licissueddate,
                    'LicenceOnboardDate' => $LicenceOnboardDate,
                    'LicenceExpiryDate' => $LicenceExpiryDate,
                  );
                  $LicenceExpiryJson = json_encode($LiceneceDataArr);
                  $redis->hset('H_LicenceExpiry_' . $fcode, $LicenceId, $LicenceExpiryJson);
                  $redis->sadd('S_LicenceList_' . $fcode, $LicenceId);
                  if (session()->get('cur') == 'dealer') {
                    $redis->sadd('S_LicenceList_dealer_' . $username . '_' . $fcode, $LicenceId);
                    $LatLong = $redis->hget('H_Franchise_LatLong', $fcode . ":" . $username);
                  } else if (session()->get('cur') == 'admin') {
                    $redis->sadd('S_LicenceList_admin_' . $fcode, $LicenceId);
                  }


                  $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
                  $servername = $franchiesJson;
                  $servername = "209.97.163.4";
                  if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
                    // $servername = "188.166.237.200";
                    return 'Ipaddress Failed !!!';
                  }
                  $usernamedb = "root";
                  $password = "#vamo123";
                  $dbname = $fcode;
                  logger('franci..----' . $fcode);
                  logger('ip----' . $servername);
                  $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
                  if (!$conn) {
                    die('Could not connect: ' . mysqli_connect_error());
                    return 'Please Update One more time Connection failed';
                  } else {
                    logger('inside mysql to onboard');
                    $ins = "INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$username','$LicenceId','$vehicleId','$deviceid','$Licence','OnBoard')";
                    //$conn->multi_query($ins);
                    if ($conn->multi_query($ins)) {
                      logger('Successfully inserted');
                    } else {
                      logger('not inserted');
                    }
                    $conn->close();
                  }
                }


                //$redis->hset ( 'H_VehicleName_Mobile_Org_' .$fcode, $vehicleId.':'.$deviceid.':'.$shortName.':'.$orgId1.':'.$gpsSimNo, $vehicleId );
                ///$redis->hset('H_Vehicle_Map_Uname_' . $fcode, $vehicleId.'/'.$groupId . ':' . $fcode, $userId);
                ///ram noti
                $newOne = $redis->sadd('S_' . $vehicleId . '_' . $fcode, 'S_' . $groupId . ':' . $fcode);
                $newOneUser = $redis->sadd('S_' . $groupId . ':' . $fcode, $userId);
                ///
                $cpyDeviceSet = 'S_Device_' . $fcode;
                $redis->sadd($cpyDeviceSet, $deviceid);
                $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
                $redis->hmset($vehicleDeviceMapId, $vehicleId, $deviceid, $deviceid, $vehicleId);
                $redis->sadd('S_Vehicles_' . $fcode, $vehicleId);
                $redis->hset('H_Device_Cpy_Map', $deviceid, $fcode);
                $redis->sadd('S_Vehicles_' . $orgId . '_' . $fcode, $vehicleId);
                if ($expireData == null) {
                  $redis->hset('H_Expire_' . $fcode, $new_date2, $vehicleId);
                } else {
                  $redis->hset('H_Expire_' . $fcode, $new_date2, $expireData . ',' . $vehicleId);
                }
                $time = microtime(true);
                $redis->sadd('S_Organisation_Route_' . $orgId . '_' . $fcode, $shortName);
                $time = round($time * 1000);
                $tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,0,S,N';
                //$LatLong=$redis->hget('H_Franchise_LatLong',$fcode);
                if ($LatLong != null) {
                  $data_arr = explode(":", $LatLong);
                  $latitude = $data_arr[0];
                  $longitude = $data_arr[1];
                  $tmpPositon =  $latitude . ',' . $longitude . ',0,N,' . $time . ',0.0,N,N,ON,0,S,N';
                  // newday check
                  $redis->sadd('S_NoDataVehicle', $vehicleId . ',' . $fcode);
                  $redis->hset('H_NoDataVehicleProcess_Time', $vehicleId . ',' . $fcode, $time);
                }
                $redis->hset('H_ProData_' . $fcode, $vehicleId, $tmpPositon);
              }

              $details = $redis->hget('H_Organisations_' . $fcode, $organizationId);
              //logger($details.'before '.$ownerShip);
              if ($type == 'Sale') {
                if ($details == null) {
                  logger('new organistion going to create');
                  $redis->sadd('S_Organisations_' . $fcode, $organizationId);
                  if ($ownerShip != 'OWN') {
                    logger('------login 1---------- ' . session()->get('cur'));
                    $redis->sadd('S_Organisations_Dealer_' . $ownerShip . '_' . $fcode, $organizationId);
                  } else if ($ownerShip == 'OWN') {
                    $redis->sadd('S_Organisations_Admin_' . $fcode, $organizationId);
                  }
                  $orgDataArr = array(
                    'mobile' => '1234567890',
                    'description' => '',
                    'email' => '',
                    'vehicleExpiry' => '',
                    'onboardDate' => '',
                    'address' => '',
                    'mobile' => '',
                    'startTime' => '',
                    'endTime'  => '',
                    'atc' => '',
                    'etc' => '',
                    'mtc' => '',
                    'parkingAlert' => '',
                    'idleAlert' => '',
                    'parkDuration' => '',
                    'idleDuration' => '',
                    'overspeedalert' => '',
                    'sendGeoFenceSMS' => '',
                    'radius' => ''
                  );
                  $orgDataJson = json_encode($orgDataArr);
                  $redis->hset('H_Organisations_' . $fcode, $organizationId, $orgDataJson);
                  $redis->hset('H_Org_Company_Map', $organizationId, $fcode);
                  logger('before Audit Entry for Org-Create');
                  try {
                    $mysqlDetails = array();
                    $status = array();
                    $status = array(
                      'fcode' => $fcode,
                      'organizationName' => $organizationId,
                      'userName' => $username,
                      'status' => config()->get('constant.created'),
                      'userIpAddress' => session()->get('userIP'),
                    );
                    $mysqlDetails   = array_merge($status, $orgDataArr);
                    $modelname = new AuditOrg();
                    $table = $modelname->getTable();
                    $db = $fcode;
                    AuditTables::ChangeDB($db);
                    $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
                    if (count($tableExist) > 0) {
                      AuditOrg::create($mysqlDetails);
                    } else {
                      AuditTables::CreateAuditOrg();
                      AuditOrg::create($mysqlDetails);
                    }
                    AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                  } catch (Exception $e) {
                    AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                    logger('Error inside AuditOrg Create' . $e->getMessage());
                    logger($mysqlDetails);
                  }
                  logger('After Audit Entry for Org-Create');
                }
              }
              if ($type == 'Sale') {

                $groupname1      = request()->get('groupname');
                $groupname       = strtoupper($groupname1);
                logger($userId . '-------------- groupname- out-------------' . $groupId);
                if ($type1 == 'existing' && $groupname !== null && $groupname !== '') {

                  $groupId = explode(":", $groupname)[0];
                  logger('-------------- groupname--------------' . $groupId);
                }

                $redis->sadd($groupId1 . ':' . $fcode1, $vehicleId);
                // logger('###################################################');
                if ($type1 == 'existing' && $groupname !== null && $groupname !== '') {

                  logger('before audit entry for group update');
                  try {
                    // $vehicleIDList=$redis->smembers($groupId1. ':' .$fcode1);
                    $vehicleIDList = $redis->smembers($groupId . ':' . $fcode1);
                    // logger($vehicleID);
                    $mysqlDetails = array(
                      'userName' => $username,
                      'status' => config()->get('constant.new_vehicle_added'),
                      'fcode' => $fcode,
                      'groupName' => $groupname,
                      'vehicleID' => implode(',', $vehicleIDList),
                      'userIpAddress' => session()->get('userIP'),
                      'addedVehicles' => implode(',', $vehicleIDList),
                      'removedVehicles' => '',
                      'vehicleName' => '',
                    );
                    //$table=new AuditGroup->getTable();
                    $modelname = new AuditGroup();
                    $table = $modelname->getTable();
                    //return $table;
                    //return $mysqlDetails;
                    $db = $fcode;
                    AuditTables::ChangeDB($db);
                    $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
                    //return $tableExist;
                    if (count($tableExist) > 0) {
                      AuditGroup::create($mysqlDetails);
                    } else {
                      AuditTables::CreateAuditGroup();
                      AuditGroup::create($mysqlDetails);
                    }
                    AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                  } catch (Exception $e) {
                    AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                    logger('Error inside AuditVehicle New Vehicle Added' . $e->getMessage());
                    logger($mysqlDetails);
                  }
                  //AuditGroup::create($details);
                  logger('after audit entry for group update');
                }
              }
              if (session()->get('cur') == 'dealer') {
                $redis->srem('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceid);
                $redis->hdel('H_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceid);
              } else if (session()->get('cur') == 'admin') {
                $redis->srem('S_Pre_Onboard_Admin_' . $fcode, $deviceid);
                $redis->hdel('H_Pre_Onboard_Admin_' . $fcode, $deviceid);
                if ($ownerShip !== 'OWN') {
                  $redis->sadd('S_Pre_Onboard_Dealer_' . $ownerShip . '_' . $fcode, $deviceid);
                  $redis->hset('H_Pre_Onboard_Dealer_' . $ownerShip . '_' . $fcode, $deviceid, $deviceDataJson);
                }
              }
            }


            if ($dev == null && $tempdev == null && $devBack != 1) {
            } else {
              logger('--------------already present--------------' . $deviceid);

              $vehicleIdarray = Arr::add($vehicleIdarray, $deviceid, $deviceid);
            }
          }
        }
        $deviceid = null;
        $deviceidtype = null;
      }
      logger('--------------count($dbarray--------------' . count($dbarray));

      if ($type == 'Sale') {

        logger('------sale-2--------- ' . $ownerShip);
        $redis->sadd('S_Groups_' . $fcode, $groupId1 . ':' . $fcode1);
        if ($ownerShip != 'OWN') {
          logger('------login 1---------- ' . session()->get('cur'));
          $redis->sadd('S_Groups_Dealer_' . $ownerShip . '_' . $fcode, $groupId1 . ':' . $fcode1);
          $redis->sadd('S_Users_Dealer_' . $ownerShip . '_' . $fcode, $userId);
        } else if ($ownerShip == 'OWN') {
          $redis->sadd('S_Groups_Admin_' . $fcode, $groupId1 . ':' . $fcode1);
          $redis->sadd('S_Users_Admin_' . $fcode, $userId);
        }
        $redis->sadd($userId, $groupId1 . ':' . $fcode1);
        $redis->sadd('S_Users_' . $fcode, $userId);
        if (session()->get('cur') == 'dealer') {
          logger('------login 1---------- ' . session()->get('cur'));
          $OWN = $username;
        } else if (session()->get('cur') == 'admin') {
          $OWN = 'admin';
        }

        if ($type1 == 'new') {
          logger('------sale--3-------- ' . $ownerShip);
          $password = request()->get('password');
          if ($password == null) {
            $password = 'awesome';
          }
          $redis->hmset('H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNoUser, $userId . ':email', $emailUser, $userId . ':password', $password, $userId . ':OWN', $OWN);
          $user = new User;
          $user->name = $userId;
          $user->username = $userId;
          $user->email = $emailUser;
          $user->mobileNo = $mobileNoUser;
          $user->password = Hash::make($password);
          $user->gps_status = "1";
          $user->is_verified = "1";
          // $user->save();
          try {
            $userProperties = [
              'email' => $userId . '@vamosys.com',
              'emailVerified' => false,
              'password' => $password,
              'displayName' => $userId,
              'disabled' => false,
              'uid' => $userId,
            ];
            $let = $auth->createUser($userProperties);
          } catch (FirebaseException $e) {
            logger($e->getMessage());
            // dd($e);
            return redirect()->back()->withInput()->withErrors($e->getMessage());
          }




          $groupNameNew = $groupId1 . ':' . $fcode1;
          // logger($groupId1. ':' .$fcode1);
          $vehicleID = $redis->smembers($groupId1 . ':' . $fcode1);
          // logger($vehicleID);

          logger('before audit entry for group create');
          try {
            $mysqlDetails = array(
              'userName' => $username,
              'status' => config()->get('constant.created'),
              'fcode' => $fcode,
              'groupName' => $groupNameNew,
              'vehicleID' => implode(",", $vehicleID),
              'vehicleName' => $shortName,
              'userIpAddress' => session()->get('userIP'),
            );
            //$table=new AuditGroup->getTable();
            $modelname = new AuditGroup();
            $table = $modelname->getTable();
            //return $table;
            //return $mysqlDetails;
            $db = $fcode;
            AuditTables::ChangeDB($db);
            $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
            //return $tableExist;
            if (count($tableExist) > 0) {
              AuditGroup::create($mysqlDetails);
            } else {
              AuditTables::CreateAuditGroup();
              AuditGroup::create($mysqlDetails);
            }
            AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
          } catch (Exception $e) {
            AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
            logger('Error inside AuditGroup Create' . $e->getMessage());
            logger($mysqlDetails);
          }
          //AuditGroup::create($details);
          logger('after audit entry for group create');
          logger("before audit user create entry");
          try {
            $details = array(
              'fcode' => $fcode,
              'userId' => $userId,
              'userName' => $username,
              'status' => config()->get('constant.created'),
              'email' => $emailUser,
              'mobileNo' => $mobileNoUser,
              'password' => Hash::make($password),
              'zoho' => '-',
              'companyName' => '-',
              'cc_email' => '-',
              'groups' => $groupId1 . ':' . $fcode1,
              'vehicles' => implode(",", $vehicleID),
              'userIpAddress' => session()->get('userIP'),
            );
            $modelname = new AuditUser();
            $table = $modelname->getTable();
            $db = $fcode;
            AuditTables::ChangeDB($db);
            $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
            if (count($tableExist) > 0) {
              AuditUser::create($details);
            } else {
              AuditTables::CreateAuditUser();
              AuditUser::create($details);
            }
            AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
          } catch (Exception $e) {
            AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
            logger('Error inside AuditVehicle Create' . $e->getMessage() . '---------line->' . $e->getLine());
            logger($details);
          }
          logger("before audit user create entry");
        }
      }
      if (session()->get('cur') == 'admin' && $count > 0) {
        $franDetails_json = $redis->hget('H_Franchise', $fcode);
        $franchiseDetails = json_decode($franDetails_json, true);
        if ($prepaid == 'yes') {
          if ($Licence == 'Starter') {
            $franchiseDetails['availableStarterLicence'] = $franchiseDetails['availableStarterLicence'] - $count;
            logger('inside count present' . $franchiseDetails['availableStarterLicence']);
          } else if ($Licence == 'Basic') {
            $franchiseDetails['availableBasicLicence'] = $franchiseDetails['availableBasicLicence'] - $count;
            logger('inside count present' . $franchiseDetails['availableBasicLicence']);
          } else if ($Licence == 'Advance') {
            $franchiseDetails['availableAdvanceLicence'] = $franchiseDetails['availableAdvanceLicence'] - $count;
            logger('inside count present' . $franchiseDetails['availableAdvanceLicence']);
          } else if ($Licence == 'Premium') {
            $franchiseDetails['availablePremiumLicence'] = $franchiseDetails['availablePremiumLicence'] - $count;
            logger('inside count present' . $franchiseDetails['availablePremiumLicence']);
          } else if ($Licence == 'PremiumPlus') {
            $franchiseDetails['availablePremPlusLicence'] = $franchiseDetails['availablePremPlusLicence'] - $count;
            logger('inside count present' . $franchiseDetails['availablePremPlusLicence']);
          }
        } else {
          $franchiseDetails['availableLincence'] = $franchiseDetails['availableLincence'] - $count;
          logger('inside count present' . $franchiseDetails['availableLincence']);
        }
        $detailsJson = json_encode($franchiseDetails);
        $redis->hmset('H_Franchise', $fcode, $detailsJson);
      } else if (session()->get('cur') == 'dealer' && $count > 0 && session()->get('cur1') == 'prePaidAdmin') {
        $dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
        $dealersDetails = json_decode($dealersDetails_json, true);
        if ($Licence == 'Starter') {
          $dealersDetails['avlofStarter'] = $dealersDetails['avlofStarter'] - $count;
        } else  if ($Licence == 'Basic') {
          $dealersDetails['avlofBasic'] = $dealersDetails['avlofBasic'] - $count;
        } else if ($Licence == 'Advance') {
          $dealersDetails['avlofAdvance'] = $dealersDetails['avlofAdvance'] - $count;
        } else if ($Licence == 'Premium') {
          $dealersDetails['avlofPremium'] = $dealersDetails['avlofPremium'] - $count;
        } else if ($Licence == 'PremiumPlus') {
          $dealersDetails['avlofPremiumPlus'] = $dealersDetails['avlofPremiumPlus'] - $count;
        }
        $detailsJson = json_encode($dealersDetails);
        $redis->hmset('H_DealerDetails_' . $fcode, $username, $detailsJson);
      }


      $error = '';
      $success = null;
      if (count($vehicleIdarray) !== 0 || count($SpecialdChar) !== 0 || count($emptyChar) !== 0 || count($vehDevSame) != 0) {
        $error2 = ' ';
        $error = [];
        $error3 = ' ';
        $error4 = '';
        if (count($vehicleIdarray) !== 0) {
          $error0 = implode(",", $vehicleIdarray);
          $error = 'These names already exists ' . $error0;
        }
        if (count($SpecialdChar) !== 0) {
          $error2 = implode(",", $SpecialdChar);
          $error2 = str_replace('^', '.', $error2);
          $error = 'Special Characters are used : ' . $error2;
        }
        if (count($emptyChar) !== 0) {
          $error3 = implode(",", $emptyChar);
          $error = 'Empty Device or Vehicle ID Found: ' . $error3;
        }
        if (count($vehDevSame) != 0) {
          $error4 = implode(",", $vehDevSame);
          $error = 'DeviceId and Vehicle Id should not be same: ' . $error4;
        }
        if ($count > 0) {
          $success = $count . ' Successfully Added';
          session()->flash('alert-class', "alert-success");
          session()->flash('message', $success);
        }
      } else {
        if ($count > 0) {
          $success = $count . ' Successfully Added';
          session()->flash('alert-class', "alert-success");
          session()->flash('message', $success);
        }
      }
      $emailFcode = $redis->hget('H_Franchise', $fcode);
      $emailFile = json_decode($emailFcode, true);
      $email1 = $emailFile['email2'];
      $email2 = $emailFile['email1'];
      $onboardDate = $current->format('d-m-Y');
      $i = 0;
      if ($ownerShip != 'OWN') {
        $emailFcode = $redis->hget('H_DealerDetails_' . $fcode, $ownerShip);
        $emailFile = json_decode($emailFcode, true);
        $email1 = $emailFile['email'];
      }

      /* if ($fcode == 'SMP') {
        $updatefile = Excel::create('SMP_Onboarded_Vehicle_List', function ($excel) use ($count, $onboardDate, $ownerShip, $userId, $devicearray) {
          $excel->sheet('Sheetname', function ($sheet)  use ($count, $onboardDate, $ownerShip, $userId, $devicearray) {
            $onboard = 'UserId : ';
            if ($ownerShip != 'OWN') {
              $onboard = 'DealerId';
              $userId = $ownerShip;
            }
            $sheet->row(1, array('Number of Devices onboarded : ' . $count));
            $sheet->row(2, array('Onboard Date : ' . $onboardDate, $onboard . ' : ' . $userId));
            $sheet->row(4, array('S.No ', ' Vehicle Details ', ' Device Details '));
            $j = 5;
            $sno = 1;
            $sheet->cells('A4:C4', function ($cells) {
              $cells->setBackground(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
              $cells->setFontColor('#ffffff');
            });
            foreach ($devicearray as $fieldName => $oldValue) {
              $sheet->row($j, array($sno++, $fieldName, $oldValue));
              $j++;
            }
          });
          //})->download('xls');
        });

      try {
          $s3 = App::make('aws')->createClient('s3',[
            'endpoint' => config()->get('constant.endpoint'),
        ]);
          $s3->putObject(array(
            'Bucket' => config()->get('constant.bucket'),
            'Key' => "mail/SMP_Onboarded_Vehicle_List.xls",
            'SourceFile' => ($updatefile->store("xls", false, true)['full']),
            'ACL' => 'public-read'
          ));
          // OnboardNotifier:SMP:OWN:bala@gmail.com
          $email1 = "senthamizhselvi.vamosys@gmail.com,support@vamosys.freshdesk.com,nithya.vamosys@gmail.com,pk@vamosys.com,mani@vamosys.com";
          $redis->set("OnboardNotifier:" . $fcode . ":OWN:" . $email1, "yes");
          $redis->expire("OnboardNotifier:" . $fcode . ":OWN:" . $email1, "30");
        } catch (exception $e) {
          logger($e);
        } 
      } 
      if (is_null($success)) {
        return redirect()->to('Business')->withErrors($error);
      } else {
        return redirect()->to('Business');
      }
    } catch (\Exception $e) {
      $username = auth()->id();
      logger(sprintf("EXCEPTIONBYUSER_" . $username . " %s - line - " . $e->getLine() . '------exception---------- ' . $e->getMessage(), __FILE__, __LINE__));
      return redirect()->to('Business')->withErrors($e->getMessage());
    }
  }
   ***/

  public function adddevice()
  {

    $dealerOrUser = request()->get('dealerOrUser'); //  Sale:user or Move:dealer
    // arrays of data
    $LicenceIdArr = request()->get('LicenceId');
    if ($dealerOrUser == "Sale") {
      $deviceIdArr = request()->get('deviceid');
      $vehicleIdArr = request()->get('vehicleId');
    } else {
      $deviceIdArr = request()->get('deviceidSale');
      $vehicleIdArr = request()->get('vehicleIdSale');
    }
    $deviceidtypeArr = request()->get('deviceidtype');
    $shortNameArr = request()->get('shortName');
    $vehicleTypeArr = request()->get('vehicleType');
    $gpsSimNoArr = request()->get('gpsSimNo');

    $detailList =  [
      'deviceIdArr' => $deviceIdArr,
      'vehicleIdArr' => $vehicleIdArr,
      'deviceidtypeArr' => $deviceidtypeArr,
      'shortNameArr' => $shortNameArr,
      'vehicleTypeArr' => $vehicleTypeArr,
      'gpsSimNoArr' => $gpsSimNoArr,
      'LicenceIdArr' => $LicenceIdArr
    ];
    // logger($detailList);
    return BusinessController::addDeviceV1($detailList);
  }

  public static function addDeviceV1($detailList)
  {
    try {

      // return request()->all();
      $username = auth()->id();
      $redis = Redis::connection();
      $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
      $fcode1 = strtoupper($fcode);
      $ownerShip = request()->get('dealerId') ?? "OWN";
      $userId = request()->get('userId');
      $userId = strtoupper($userId);
      $mobileNoUser = request()->get('mobileNoUser');
      $emailUser = request()->get('emailUser');
      $password = request()->get('password');
      $dealerOrUser = request()->get('dealerOrUser'); //  Sale:user or Move:dealer
      $isExistingUser = request()->get('isExistingUser'); // existing or  new
      $numberofdevice = request()->get('numberofdevice'); //
      $licenceType = request()->get('licenceType'); //
      $groupname = request()->get('groupname');
      $groupname = str_replace('$', '.', $groupname);
      $groupname = strtoupper($groupname);
      $OWN = 'admin';
      $franDetails_json = $redis->hget('H_Franchise', $fcode);
      $franchiseDetails = json_decode($franDetails_json, true);
      $prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
      $availableLincence = '0';
      if (isset($franchiseDetails['availableLincence'])) {
        $availableLincence = $franchiseDetails['availableLincence'];
      }

      $orgId = 'Default';
      $organizationId = $orgId;
      $prepaidDealer = session('cur') == "dealer" && session('cur1') == "prePaidAdmin";
      
        
      if ($prepaidDealer) {
        $ownerShip = $username;
        $setOrgKey = 'S_Organisations_Dealer_' . $username . '_' . $fcode;
        $setGroupKey = 'S_Groups_Dealer_' . $username . '_' . $fcode;
        $setUserKey = 'S_Users_Dealer_' . $username . '_' . $fcode;
        $setAddVehicle = 'S_Vehicles_Dealer_' . $ownerShip . '_' . $fcode;
        $setRemoveVehicle = 'S_Vehicles_Admin_'  . $fcode;
        $searchVehicle = 'H_VehicleName_Mobile_Dealer_' . $ownerShip . '_Org_' . $fcode;
        $setLicence  = 'S_LicenceList_dealer_'.$username.'_' . $fcode;
      }else if($dealerOrUser == "Sale") {
        $ownerShip = "OWN";
        $setOrgKey = 'S_Organisations_Admin_' . $fcode;
        $setGroupKey = 'S_Groups_Admin_' . $fcode;
        $setUserKey = 'S_Users_Admin_' . $fcode;
        $setAddVehicle = 'S_Vehicles_Admin_'  . $fcode;
        $setRemoveVehicle = 'S_Vehicles_Dealer_' . $ownerShip . '_' . $fcode;
        $searchVehicle = 'H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode;
        $setLicence  = 'S_LicenceList_admin_' . $fcode;
      }else{
        $setAddVehicle = 'S_Vehicles_Dealer_' . $ownerShip . '_' . $fcode;
        $setRemoveVehicle = 'S_Vehicles_Admin_'  . $fcode;
        $searchVehicle = 'H_VehicleName_Mobile_Dealer_' . $ownerShip . '_Org_' . $fcode;
      }

      if ($dealerOrUser == "Sale") {
        
        if ($isExistingUser == "new") {
          $rules = array(
            'userId' => 'required|alpha_dash',
            'emailUser' => 'required|email',
            'mobileNoUser' => 'required|numeric',
            'password' => 'required|min:6',
          );

          $validator = validator()->make(request()->all(), $rules);
          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
          }
          if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) {
            return redirect()->back()->withErrors(trans('messages.UserNamewithadminnotacceptable'))->withInput();
          }


          $val = $redis->hget('H_UserId_Cust_Map', $userId . ':fcode');
          $val1 = $redis->sismember('S_Users_' . $fcode, $userId);
          $valOrg = $redis->sismember('S_Organisations_' . $fcode, $userId);
          $valGroup = $redis->sismember($setGroupKey, $userId . ':' . $fcode1);
          $valOrg1 = $redis->sismember($setOrgKey, $userId);
          $valUser1 = $redis->sismember($setUserKey, $userId);

          $valGroup1 = $redis->sismember('S_Groups_' . $fcode, $userId . ':' . $fcode1);
          // $userExist = FirebaseController::getUser($userId) !== null;
          $userExist =false;
          if ($valGroup == 1 || $valGroup1 == 1 || $valOrg == 1 || $valOrg1 == 1 || $val1 == 1 || isset($val) || $userExist || $valUser1) {
            return redirect()->back()->withInput()->withErrors(trans('messages.UserNamealreadyexists'));
          }

          $groupname  = $userId . ':' . $fcode1;

          $redis->sadd('S_Users_' . $fcode, $userId);
          $redis->sadd($setUserKey, $userId);
          $redis->sadd('S_Organisations_' . $fcode, $userId);
          $redis->sadd($setOrgKey . $fcode, $userId);
          $redis->sadd('S_Groups_' . $fcode,  $groupname);
          $redis->sadd($setGroupKey,  $groupname);
          $redis->sadd($userId, $groupname);


          $password = request()->get('password') ?? "awesome";
          $redis->hmset('H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNoUser, $userId . ':email', $emailUser, $userId . ':password', $password, $userId . ':OWN', $OWN);

          $user = new User([
            'name' => $userId,
            'username' => $userId,
            'email' => $emailUser,
            'mobileNo' => $mobileNoUser,
            'password' => Hash::make($password),
            'gps_status' => '1',
            'is_verified' => '1'
          ]);
          $user->save();
          try{
            $newUserData = [
              'name' => $userId,
              'mobileNo'=>$mobileNoUser,
              'email'=>$emailUser,
            ];
            $mysqlDetails = [
              'userIpAddress' => session()->get('userIP', '-'),
              'userName'=> $username,
              'type'=>config()->get('constant.user'),
              'name'=>$userId,
              'status'=>config()->get('constant.created'),
              'oldData'=>"",
              'newData'=>json_encode($newUserData),
            ];
            AuditNewController::updateAudit($mysqlDetails);            
          }catch(Exception $e){
            AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
            logger($e->getMessage().'---------'.$e->getLine());
          }
          try {
            $userProperties = [
              'email' => $userId . '@vamosys.com',
              'emailVerified' => false,
              'password' => $password,
              'displayName' => $userId,
              'disabled' => false,
              'uid' => $userId,
            ];
            // logger($userProperties);
            // FirebaseController::createUser($userProperties);
          } catch (FirebaseException $e) {
            logger($e);
            return redirect()->back()->withInput()->withErrors($e->getMessage());
          }
          $organizationId = $userId;
          $orgId = $organizationId;
        }

        if ($isExistingUser == 'existing') {
          $rules = array(
            'userIdtemp' => 'required',
            'groupname' => 'required',
            'orgId' => 'required',
          );
          $validator = validator()->make(request()->all(), $rules);
          if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator)->with([
              'message' => 'Please select user Id !'
            ]);
          }
          $groupname = request('groupname');
          $userId = request('userIdtemp');
          $orgId = request('orgId');
          $organizationId = $orgId;
        }
      }

      $groupId = $groupname;
      $count = 0;
      $current = Carbon::now();
      $LicenceExpiryDate = '';
      $LicenceOnboardDate = '';
      $onboardDate = '';
      $vehicleExpiry = '';
      $licissueddate = $current->format('d-m-Y');
      if ($dealerOrUser == 'Sale') {
        $onboardDate = $current->format('d-m-Y');
        $vehicleExpiry = date('Y-m-d', strtotime(date("d-m-Y", time()) . "+ 1 year"));
        if ($prepaid == 'yes') {
          $LicenceOnboardDate = $current->format('d-m-Y');
          $LicenceExpiryDate = date('d-m-Y', strtotime(date("d-m-Y", time()) . "+ 1 year"));
        }
      } else {
        $orgId = 'Default';
        $organizationId = 'Default';
      }

      $vehicleIdarray = array();
      $KeyVehicles = array();
      $SpecialdChar = array();
      $emptyChar = array();
      $devicearray = array();
      $vehDevSame = array();

      // arrays of data
      $LicenceIdArr = $detailList['LicenceIdArr'];
      $deviceIdArr = $detailList['deviceIdArr'];
      $vehicleIdArr = $detailList['vehicleIdArr'];
      $deviceidtypeArr = $detailList['deviceidtypeArr'];
      $shortNameArr = $detailList['shortNameArr'];
      $vehicleTypeArr = $detailList['vehicleTypeArr'];
      $gpsSimNoArr = $detailList['gpsSimNoArr'];


      for ($i = 0; $i < $numberofdevice; $i++) {

        $deviceid = $deviceIdArr[$i];
        $vehicleId = $vehicleIdArr[$i];

        $vehicleId = preg_replace('/[ ]{2,}|[\t]/', '', trim($vehicleId));
        $deviceid = preg_replace('/[ ]{2,}|[\t]/', '', trim($deviceid));
        $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/';

        if (preg_match($pattern, $vehicleId)) {
          $vehcle = str_replace('.', '^', $vehicleId);
          $SpecialdChar = Arr::add($SpecialdChar, $vehcle, $vehcle);
        } else if (preg_match($pattern, $deviceid)) {
          $devid = str_replace('.', '^', $deviceid);
          $SpecialdChar = Arr::add($SpecialdChar, $devid, $devid);
        } else if ($vehicleId == null || $deviceid == null) {
          if ($vehicleId == null) {
            $emptyId = $deviceid;
          } else {
            $emptyId = $vehicleId;
          }
          $emptyChar = Arr::add($emptyChar, $emptyId, $emptyId);
        } else if ($vehicleId == $deviceid) {
          $vehDevSame = Arr::add($vehDevSame, $vehicleId, $vehicleId);
        } else {
          $vehicleId = strtoupper($vehicleId);

          $vehicleId =  $vehicleId ?? 'GPSVTS_' . substr($deviceid, -7);
          $deviceid = str_replace(' ', '', $deviceid);
          $vehicleId = str_replace(' ', '', $vehicleId);

          $devicearray = Arr::add($devicearray, $vehicleId, $deviceid);
          $shortName = $shortNameArr[$i] ?? $vehicleId;
          if($shortName ==""){
            $shortName = $vehicleId;
          }
          $shortName = strtoupper($shortName);

          $regNo = $regNo ?? 'XXXX';
          $orgId = $orgId ?? 'Default';

          $vehicleType = $vehicleTypeArr[$i] ?? "Bus";
          // $vehicleType = $vehicleType ?? 'Bus';

          $oprName = $oprNameArr[$i] ?? 'Airtel';
          // $oprName = $oprName ?? 'Airtel';

          $gpsSimNo = $gpsSimNoArr[$i] ?? '';
          $gpsSimNo = $gpsSimNo ?? '';

          $deviceidtype = $deviceidtypeArr[$i] ?? "WETRACK 800 AIS";
          $deviceModel = $deviceidtype;

          if ($deviceid !== null && $deviceidtype !== null) {
            $deviceMapData = $redis->hget('H_Device_Cpy_Map', $deviceid);
            $vehicleMapData  = $redis->hget('H_Device_Cpy_Map', $vehicleId);
            $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
            $back = $redis->HEXISTS($vehicleDeviceMapId, $deviceid);
            $backexist = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
            $devBack = $redis->sismember('S_Vehicles_' . $fcode, $deviceid);
            $back1 = $redis->sismember('S_KeyVehicles', $vehicleId);
            if ($back == 1 || $devBack == 1 || !empty($vehicleMapData) || $backexist == 1) {
              $vehicleIdarray = Arr::add($vehicleIdarray, $vehicleId, $vehicleId);
            }
            if ($back1 == 1) {
              $KeyVehicles = Arr::add($KeyVehicles, $vehicleId, $vehicleId);
            }
            // if (session()->get('cur') == 'admin') {
            //   $deviceProData = $redis->hget('H_Pre_Onboard_Admin_' . $fcode, $deviceid);
            // }

            if (empty($vehicleMapData) && $deviceMapData == null && $back == 0 && $back1 == 0 && $devBack == 0  && $backexist == 0) {
              $count++;
              $deviceDataArr = array(
                'deviceid' => $deviceid,
                'deviceidtype' => $deviceidtype
              );
              $deviceDataJson = json_encode($deviceDataArr);

              $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
              $shortName = str_replace(' ', '', $shortName);
              $orgId1 = strtoupper($orgId);
              $redis->sadd($setAddVehicle, $vehicleId);
              // $redis->srem($setRemoveVehicle, $vehicleId);
              $redis->hset($searchVehicle, $vehicleId . ':' . $deviceid . ':' . $shortName . ':' . $orgId1 . ':' . $gpsSimNo, $vehicleId);

              logger($ownerShip." in");
              if ($ownerShip !== 'OWN' && $prepaid !== "yes") {
                // $detailJson = $redis->hget('H_DealerDetails_' . $fcode, $ownerShip);
                // $dealerDetail = json_decode($detailJson, true);
                // $timeZone = isset($dealerDetail['timeZone']) ? $dealerDetail['timeZone'] : 'Asia/Kolkata';

                $OWN = $ownerShip;
                $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
                $servername = $franchiesJson;

                if (request()->server("HTTP_HOST") == "localhost") {
                  $servername = '209.97.163.4';
                }

                if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
                  return abort(404);
                }
                $usernamedb = "root";
                $password = "#vamo123";
                $dbname = $fcode;


                $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
                if (!$conn) {
                  die('Could not connect: ' . mysqli_connect_error());
                  abort(404,'Please Update One more time Connection failed');
                } else {
                  $max = "SELECT COUNT(id) FROM BatchMove";
                  $results = mysqli_query($conn, $max);
                  while ($row = mysqli_fetch_array($results)) {
                    $maxcount = $row[0];
                  }
                  $j = $maxcount + $i;
                  $dateTime = Carbon::now();

                  $insertval = "INSERT INTO BatchMove (Device_Id, Dealer_Name, Vehicle_Id) VALUES ('$deviceid','$ownerShip','$vehicleId')";
                  $conn->multi_query($insertval);
                  $conn->close();
                }
              }


              if (session()->get('cur') == 'admin' && $prepaid !== "yes") {
                logger($ownerShip);
                if ($ownerShip !== 'OWN') {
                  $redis->sadd('S_Pre_Onboard_Dealer_' . $ownerShip . '_' . $fcode, $deviceid);
                  $redis->hset('H_Pre_Onboard_Dealer_' . $ownerShip . '_' . $fcode, $deviceid, $deviceDataJson);
                }
              }
              // for notifiction 
           $redis->sadd('S_' . $vehicleId . '_' . $fcode, 'S_' . $groupname);
           $redis->sadd('S_' . $groupname, $userId);
           ///

              $refDataArr = array(
                'deviceId' => $deviceid,
                'deviceModel' => $deviceModel,
                'shortName' => $shortName,
                'regNo' => $regNo,
                'orgId' => $orgId,
                'vehicleType' => $vehicleType,
                'Licence'=>$licenceType,
                'oprName' => $oprName,
                'OWN' => $ownerShip,
                'gpsSimNo' => $gpsSimNo,
                'vehicleExpiry' => $vehicleExpiry,
                'onboardDate' => $onboardDate,
                'licenceissuedDate' => $licissueddate,
                'tankSize' => '1',
              );
              $refDataJson = json_encode($refDataArr);
              $redis->hset('H_RefData_' . $fcode, $vehicleId, $refDataJson);
              // regNo & VehId Map
              $redis->hmset('H_Vehicle_RegNum_Map_' . $fcode, $vehicleId, $regNo, $regNo, $vehicleId);

              try {

                $mysqlDetails = [
                  'userIpAddress' => session()->get('userIP'),
                  'userName' => $username,
                  'type' => config()->get('constant.vehicle'),
                  'name' => $vehicleId,
                  'status' => config()->get('constant.created'),
                  'oldData' => "",
                  'newData' => $refDataJson
                ];
                AuditNewController::updateAudit($mysqlDetails);
              } catch (Exception $e) {
                AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                logger('Error inside AuditVehicle Create' . $e->getMessage());
              }

              if ($prepaid == 'yes') {
                if (isset($LicenceIdArr[$i])) {
                  $LicenceId = $LicenceIdArr[$i];
                } else {
                  $lic = [
                    "StarterPlus" => "SP",
                    "Starter" => "ST",
                    "Basic" => "BC",
                    "Advance" => "AV",
                    "Premium" => "PM",
                    "PremiumPlus" => "PL"
                  ];
                  $LicenceId = strtoupper($lic[$licenceType] . uniqid());
                }

                $redis->hset('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId, $vehicleId);
                $redis->hset('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleId, $LicenceId);

                $LiceneceDataArr = array(
                  'LicenceissuedDate' => $licissueddate,
                  'LicenceOnboardDate' => $LicenceOnboardDate,
                  'LicenceExpiryDate' => $LicenceExpiryDate,
                );
                $LicenceExpiryJson = json_encode($LiceneceDataArr);
                $redis->hset('H_LicenceExpiry_' . $fcode, $LicenceId, $LicenceExpiryJson);
                $redis->sadd('S_LicenceList_' . $fcode, $LicenceId);
                $redis->sadd($setLicence, $LicenceId);

                $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
                $servername = $franchiesJson;
                $servername = "209.97.163.4";
                if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
                  // $servername = "188.166.237.200";
                  return 'Ipaddress Failed !!!';
                }
                $usernamedb = "root";
                $password = "#vamo123";
                $dbname = $fcode;
                $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
                if (!$conn) {
                  die('Could not connect: ' . mysqli_connect_error());
                  return 'Please Update One more time Connection failed';
                } else {
                  $ins = "INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$username','$LicenceId','$vehicleId','$deviceid','$licenceType','OnBoard')";
                  //$conn->multi_query($ins);
                  if ($conn->multi_query($ins)) {
                    logger('Successfully inserted');
                  } else {
                    logger('not inserted');
                  }
                  $conn->close();
                }
              }

             

              $cpyDeviceSet = 'S_Device_' . $fcode;
              $redis->sadd($cpyDeviceSet, $deviceid);
              $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
              $redis->hmset($vehicleDeviceMapId, $vehicleId, $deviceid, $deviceid, $vehicleId);
              $redis->sadd('S_Vehicles_' . $fcode, $vehicleId);
              $redis->hset('H_Device_Cpy_Map', $deviceid, $fcode);
              $redis->sadd('S_Vehicles_' . $orgId . '_' . $fcode, $vehicleId);

              $time = microtime(true);
              $redis->sadd('S_Organisation_Route_' . $orgId . '_' . $fcode, $shortName);
              $time = round($time * 1000);
              $tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,0,S,N';
              $LatLong = $redis->hget('H_Franchise_LatLong', $fcode);
              if ($LatLong != null) {
                $data_arr = explode(":", $LatLong);
                $latitude = $data_arr[0];
                $longitude = $data_arr[1];
                $tmpPositon =  $latitude . ',' . $longitude . ',0,N,' . $time . ',0.0,N,N,ON,0,S,N';
                // newday check
                $redis->sadd('S_NoDataVehicle', $vehicleId . ',' . $fcode);
                $redis->hset('H_NoDataVehicleProcess_Time', $vehicleId . ',' . $fcode, $time);
              }
              $redis->hset('H_ProData_' . $fcode, $vehicleId, $tmpPositon);


              if ($dealerOrUser == 'Sale') {
                $details = $redis->hget('H_Organisations_' . $fcode, $organizationId);
                if ($details == null) {
                  $redis->sadd('S_Organisations_' . $fcode, $organizationId);
                  if ($ownerShip != 'OWN') {
                    $redis->sadd('S_Organisations_Dealer_' . $ownerShip . '_' . $fcode, $organizationId);
                  } else if ($ownerShip == 'OWN') {
                    $redis->sadd('S_Organisations_Admin_' . $fcode, $organizationId);
                  }
                  $orgDataArr = array(
                    'mobile' => '1234567890',
                    'description' => '',
                    'email' => '',
                    'vehicleExpiry' => '',
                    'onboardDate' => '',
                    'address' => '',
                    'mobile' => '',
                    'startTime' => '',
                    'endTime'  => '',
                    'atc' => '',
                    'etc' => '',
                    'mtc' => '',
                    'parkingAlert' => '',
                    'idleAlert' => '',
                    'parkDuration' => '',
                    'idleDuration' => '',
                    'overspeedalert' => '',
                    'sendGeoFenceSMS' => '',
                    'radius' => ''
                  );
                  $orgDataJson = json_encode($orgDataArr);
                  $redis->hset('H_Organisations_' . $fcode, $organizationId, $orgDataJson);
                  $redis->hset('H_Org_Company_Map', $organizationId, $fcode);
                  try {
                    $mysqlDetails = [
                      'userIpAddress' => session()->get('userIP'),
                      'userName' => $username,
                      'type' => config()->get('constant.organization'),
                      'name' => $organizationId,
                      'status' => config()->get('constant.created'),
                      'oldData' => "",
                      'newData' => $orgDataJson
                    ];
                    AuditNewController::updateAudit($mysqlDetails);
                   
                  } catch (Exception $e) {
                    AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                    logger('Error inside AuditOrg Create' . $e->getMessage());
                    // logger($mysqlDetails);
                  }
                }

                if ($isExistingUser == 'existing') {
                  $groupname       = strtoupper($groupname);
                  $groupId = explode(":", $groupname)[0];
                } else {
                  $groupId = $userId;
                }
                $redis->sadd($groupId . ':' . $fcode1, $vehicleId);
                if ($isExistingUser == 'existing' && $groupname !== null && $groupname !== '') {
                  try {
                    $vehicleIDList = $redis->smembers($groupId . ':' . $fcode1);
                    $mysqlDetails = [
                      'userIpAddress' => session()->get('userIP'),
                      'userName' => $username,
                      'type' => config()->get('constant.group'),
                      'name' => $groupname,
                      'status' => config()->get('constant.new_vehicle_added'),
                      'oldData' => "",
                      'newData' => json_encode([
                        'vehicleId'=>join(',', $vehicleIDList)
                      ])
                    ];
                    AuditNewController::updateAudit($mysqlDetails);
                  } catch (Exception $e) {
                    AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                    logger('Error inside AuditVehicle New Vehicle Added' . $e->getMessage());
                  }
                }
              }
              
            }

            if ($deviceMapData == null  && $devBack != 1) {
            } else {
              $vehicleIdarray = Arr::add($vehicleIdarray, $deviceid, $deviceid);
            }
          }
        }
        $deviceid = null;
        $deviceidtype = null;
      }


      if (session()->get('cur') == 'admin' && $count > 0) {
        $franDetails_json = $redis->hget('H_Franchise', $fcode);
        $franchiseDetails = json_decode($franDetails_json, true);
        if ($prepaid == 'yes') {
          // Licence count--
          if ($licenceType == 'PremiumPlus') {
            $franchiseDetails['availablePremPlusLicence'] = $franchiseDetails['availablePremPlusLicence'] - $count;
          } else {
            $franchiseDetails['available' . $licenceType . 'Licence'] = $franchiseDetails['available' . $licenceType . 'Licence'] - $count;
          }
        } else {
          $franchiseDetails['availableLincence'] = $franchiseDetails['availableLincence'] - $count;
        }
        $detailsJson = json_encode($franchiseDetails);
        $redis->hset('H_Franchise', $fcode, $detailsJson);
      } else if (session()->get('cur') == 'dealer' && $count > 0 && session()->get('cur1') == 'prePaidAdmin') {
        $dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
        $dealersDetails = json_decode($dealersDetails_json, true);
        // Licence count --
        $dealersDetails['avlof' . $licenceType] = $dealersDetails['avlof' . $licenceType] - $count;

        $detailsJson = json_encode($dealersDetails);
        $redis->hset('H_DealerDetails_' . $fcode, $username, $detailsJson);
      }

      $error = '';
      $success = null;
      if (count($vehicleIdarray) !== 0 || count($SpecialdChar) !== 0 || count($emptyChar) !== 0 || count($vehDevSame) != 0) {
        $error2 = ' ';
        $error = [];
        $error3 = ' ';
        $error4 = '';
        if (count($vehicleIdarray) !== 0) {
          $error0 = implode(",", $vehicleIdarray);
          $error = trans('messages.Thesenamesalreadyexists ') . $error0;
        }
        if (count($SpecialdChar) !== 0) {
          $error2 = implode(",", $SpecialdChar);
          $error2 = str_replace('^', '.', $error2);
          $error = trans('messages.SpecialCharactersareused').": $error2";
        }
        if (count($emptyChar) !== 0) {
          $error3 = implode(",", $emptyChar);
          $error = trans('messages.EmptyDeviceorVehicleIDFound') . ": $error3";
        }
        if (count($vehDevSame) != 0) {
          $error4 = implode(",", $vehDevSame);
          $error = trans('messages.DeviceIdandVehicleIdshouldnotbesame').": $error4";
        }
        if ($count > 0) {
          $success = $count .trans('messages.SuccessfullyAdded');
          session()->flash('alert-class', "alert-success");
          session()->flash('message', $success);
        }
      } else {
        if ($count > 0) {
          $success = $count .trans('messages.SuccessfullyAdded');
          session()->flash('alert-class', "alert-success");
          session()->flash('message', $success);
        }
      }

      if (is_null($success)) {
        return redirect()->back()->withInput()->withErrors($error);
      } else {
        return redirect()->to('Business');
      }
    } catch (\Exception $e) {
      // dd($e);
      logger("Onboarding vehicles exception fcode " . $fcode . " userId " . $userId);
      return redirect()->back()->withInput();
    }
  }

  public function deviceDetails()
  {
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

    $devicesList = $redis->smembers('S_Device_' . $fcode);
    $temp = 0;
    $deviceMap = array();
    for ($i = 0; $i < count($devicesList); $i++) {
      $vechicle = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $devicesList[$i]);
      $deviceMap = Arr::add($deviceMap, $i, $vechicle . ',' . $devicesList[$i]);
      $temp++;
    }
    return view('vdm.business.device', array(
      'deviceMap' => $deviceMap
    ));
  }

  public function batchSaleOld()
  {
    try {
      $username = auth()->id();
      $redis = Redis::connection();
      $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
      $fcode1 = strtoupper($fcode);
      $deviceList      = request()->get('vehicleList');
      $deviceType      = request()->get('deviceType');
      $ownerShip      = request()->get('dealerId');
      $userId      = request()->get('userId');
      $userId1    = strtoupper($userId);
      $mobileNo      = request()->get('mobileNo');
      $email      = request()->get('email');
      $password      = request()->get('password');
      $type      = request()->get('type');
      $type1      = request()->get('type1');
      $current = Carbon::now();
      $onboardDate = $current->format('d-m-Y');
      $devicearray = array();
      if ($type1 == 'existing') {
        $userId      = request()->get('userIdtemp');
        if ($userId == null || $userId == 'select') {
          return redirect()->back()->withErrors('Invalid user Id');
        }
      }

      if ($type1 == null) {
        return redirect()->back()->withErrors('select the sale');
      }
      $type = 'Sale';
      $ownerShip = $username;
      $mobArr = explode(',', $mobileNo);

      if ($type1 == null) {
        return redirect()->back()->withErrors('Select the user');
      }
      if ($type1 == 'new') {
        if (session()->get('cur') == 'dealer') {
          $totalReports = $redis->smembers('S_Users_Reports_Dealer_' . $username . '_' . $fcode);
        }
        if ($totalReports != null) {
          foreach ($totalReports as $key => $value) {
            $redis->sadd('S_Users_Reports_' . $userId . '_' . $fcode, $value);
          }
        }
        $rules = array(
          'userId' => 'required|alpha_dash',
          'email' => 'required|email',

        );

        $validator = validator()->make(request()->all(), $rules);

        if ($validator->fails()) {
          return redirect()->back()->withInput()->withErrors($validator);
        } else {
          $val = $redis->hget('H_UserId_Cust_Map', $userId . ':fcode');
          $val1 = $redis->sismember('S_Users_' . $fcode, $userId);
          $valOrg = $redis->sismember('S_Organisations_' . $fcode, $userId);
          $valOrg1 = $redis->sismember('S_Organisations_Admin_' . $fcode, $userId);
          $valGroup = $redis->sismember('S_Groups_' . $fcode, $userId1 . ':' . $fcode1);
          $valGroup1 = $redis->sismember('S_Groups_Admin_' . $fcode, $userId1 . ':' . $fcode1);
          $valFirst = $redis->hget('H_UserId_Cust_Map', ucfirst(strtolower($userId)) . ':fcode');
          $valUpper = $redis->hget('H_UserId_Cust_Map', strtolower($userId) . ':fcode');
        }
        if ($valGroup == 1 || $valGroup1 == 1) {
          return redirect()->back()->withInput()->withErrors('Name already exist');
        }
        if ($valOrg == 1 || $valOrg1 == 1) {
          return redirect()->back()->withInput()->withErrors('Name already exist');
        }
        // $userExist = FirebaseController::getUser($userId) !== null;
        $userExist =false;

        if ($val1 == 1 || isset($val) || isset($valFirst) || isset($valUpper) || $userExist) {
          return redirect()->back()->withInput()->withErrors('User Id already exist');
        }
        if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) {
          return redirect()->back()->withInput()->withErrors('Name with admin not acceptable');
        }

        $mobArr = explode(',', $mobileNo);
        foreach ($mobArr as $mob) {
          $val1 = $redis->sismember('S_Users_' . $fcode, $userId);
          if ($val1 == 1) {
            return redirect()->back()->withInput()->withErrors($mob . ' User Id already exist');
          }
        }
      }
      $organizationId = $userId;
      $orgId = $organizationId;
      $groupId0 = $orgId;
      $groupId = strtoupper($groupId0);
      if ($ownerShip == 'OWN' && $type != 'Sale') {
        $orgId = 'Default';
      }
      if ($userId == null) {
        $orgId = 'Default';
        $organizationId = 'Default';
      }
      if ($type == 'Sale' && $type1 !== 'new') {
        $orgId = request()->get('orgId');
        $orgId = isset($orgId) ? $orgId : 'Default';
        //$organizationId=$orgId;
      }
      if ($deviceList != null) {
        $temp = 0;
        $dbarray = array();
        $dbtemp = 0;
        foreach ($deviceList as $device) {
          $myArray = explode(',', $device);
          $deviceId = $myArray[0];
          $vehicleId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $deviceId);
          $devicearray = Arr::add($devicearray, $vehicleId, $deviceId);
          $deviceDataArr = array(
            'deviceid' => $deviceId,
            'deviceidtype' => $myArray[1],
          );
          $deviceDataJson = json_encode($deviceDataArr);
          $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
          $back = $redis->hget($vehicleDeviceMapId, $deviceId);
          if ($back !== null) {
            $vehicleId = $back;


            $refDataJson1 = $redis->hget('H_RefData_' . $fcode, $vehicleId);
            $refDataJson1 = json_decode($refDataJson1, true);
            $temOrg = isset($refDataJson1['orgId']) ? $refDataJson1['orgId'] : 'default';

            $licenceId = isset($refDataJson1['Licence']) ? $refDataJson1['Licence'] : 'Advance';
            $paymentId = isset($refDataJson1['Payment_Mode']) ? $refDataJson1['Payment_Mode'] : 'Monthly';

            $licence_id = DB::select('select licence_id from Licence where type = :type', ['type' => $licenceId]);
            $payment_mode_id = DB::select('select payment_mode_id from Payment_Mode where type = :type', ['type' => $paymentId]);

            $licence_id = $licence_id[0]->licence_id;
            $payment_mode_id = $payment_mode_id[0]->payment_mode_id;
            try {

              DB::table('Vehicle_details')->insert(
                array(
                  'vehicle_id' => $vehicleId,
                  'fcode' => $fcode,
                  'sold_date' => Carbon::now(),
                  'renewal_date' => Carbon::now(),
                  'sold_time_stamp' => round(microtime(true) * 1000),
                  'month' => date('m'),
                  'year' => date('Y'),
                  'payment_mode_id' => $payment_mode_id,
                  'licence_id' => $licence_id,
                  'belongs_to' => $username,
                  'device_id' => $deviceId,
                  'status' => isset($refDataJson1['descriptionStatus']) ? $refDataJson1['descriptionStatus'] : ''
                )
              );
              $dbarray[$dbtemp++] = array(
                'vehicle_id' => $vehicleId,
                'fcode' => $fcode,
                'sold_date' => Carbon::now(),
                'renewal_date' => Carbon::now(),
                'sold_time_stamp' => round(microtime(true) * 1000),
                'month' => date('m'),
                'year' => date('Y'),
                'payment_mode_id' => $payment_mode_id,
                'licence_id' => $licence_id,
                'belongs_to' => $username,
                'device_id' => $deviceId,
                'orgId' => $orgId,
                'status' => isset($refDataJson1['descriptionStatus']) ? $refDataJson1['descriptionStatus'] : ''
              );
            } catch (\Exception $e) {
            }


            $refDataArr = array(
              'deviceId' => $deviceId,
              'shortName' => isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : '',
              'deviceModel' => isset($refDataJson1['deviceModel']) ? $refDataJson1['deviceModel'] : 'GT06N',
              'regNo' => isset($refDataJson1['regNo']) ? $refDataJson1['regNo'] : 'XXXXX',
              'vehicleMake' => isset($refDataJson1['vehicleMake']) ? $refDataJson1['vehicleMake'] : ' ',
              'vehicleType' =>  isset($refDataJson1['vehicleType']) ? $refDataJson1['vehicleType'] : 'Bus',
              'oprName' => isset($refDataJson1['oprName']) ? $refDataJson1['oprName'] : 'airtel',
              'mobileNo' => isset($refDataJson1['mobileNo']) ? $refDataJson1['mobileNo'] : '0123456789',
              'overSpeedLimit' => isset($refDataJson1['overSpeedLimit']) ? $refDataJson1['overSpeedLimit'] : '60',
              'odoDistance' => isset($refDataJson1['odoDistance']) ? $refDataJson1['odoDistance'] : '0',
              'driverName' => isset($refDataJson1['driverName']) ? $refDataJson1['driverName'] : 'XXX',
              'gpsSimNo' => isset($refDataJson1['gpsSimNo']) ? $refDataJson1['gpsSimNo'] : '0123456789',
              'email' => isset($refDataJson1['email']) ? $refDataJson1['email'] : ' ',
              'orgId' => $orgId,
              'sendGeoFenceSMS' => isset($refDataJson1['sendGeoFenceSMS']) ? $refDataJson1['sendGeoFenceSMS'] : 'no',
              'morningTripStartTime' => isset($refDataJson1['morningTripStartTime']) ? $refDataJson1['morningTripStartTime'] : ' ',
              'eveningTripStartTime' => isset($refDataJson1['eveningTripStartTime']) ? $refDataJson1['eveningTripStartTime'] : ' ',
              'parkingAlert' => isset($refDataJson1['parkingAlert']) ? $refDataJson1['parkingAlert'] : 'no',
              'altShortName' => isset($refDataJson1['altShortName']) ? $refDataJson1['altShortName'] : '',
              'paymentType' => isset($refDataJson1['paymentType']) ? $refDataJson1['paymentType'] : ' ',
              'expiredPeriod' => isset($refDataJson1['expiredPeriod']) ? $refDataJson1['expiredPeriod'] : ' ',
              'fuel' => isset($refDataJson1['fuel']) ? $refDataJson1['fuel'] : 'no',
              'fuelType' => isset($refDataJson1['fuelType']) ? $refDataJson1['fuelType'] : ' ',
              'isRfid' => isset($refDataJson1['isRfid']) ? $refDataJson1['isRfid'] : 'no',
              'OWN' => $ownerShip,

              'Licence' => isset($refDataJson1['Licence']) ? $refDataJson1['Licence'] : '',
              'Payment_Mode' => isset($refDataJson1['Payment_Mode']) ? $refDataJson1['Payment_Mode'] : '',
              'descriptionStatus' => isset($refDataJson1['descriptionStatus']) ? $refDataJson1['descriptionStatus'] : '',
              'vehicleExpiry' => isset($refDataJson1['vehicleExpiry']) ? $refDataJson1['vehicleExpiry'] : '',
              'onboardDate' => $onboardDate,
              'tankSize' => isset($refDataJson1['tankSize']) ? $refDataJson1['tankSize'] : '0',
              'licenceissuedDate' => isset($refDataJson1['licenceissuedDate']) ? $refDataJson1['licenceissuedDate'] : '',
              'communicatingPortNo' => isset($refDataJson1['communicatingPortNo']) ? $refDataJson1['communicatingPortNo'] : '',
            );

            $shortName = isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : '';
            $regNo = isset($refDataJson1['regNo']) ? $refDataJson1['regNo'] : $shortName;
            $refDataJson = json_encode($refDataArr);

            // $redis->hdel ( 'H_RefData_' . $fcode, $vehicleIdOld );
            $redis->hset('H_RefData_' . $fcode, $vehicleId, $refDataJson);
            $time = microtime(true);
            $time = round($time * 1000);
            $tmpPositon = $redis->hget('H_ProData_' . $fcode, $vehicleId);
            $LatLong = $redis->hget('H_Franchise_LatLong', $fcode . ":" . $ownerShip);
            if ($LatLong != null) {
              $data_arr = explode(":", $LatLong);
              $latitude = $data_arr[0];
              $longitude = $data_arr[1];
              $tmpPositon =  $latitude . ',' . $longitude . ',0,N,' . $time . ',0.0,N,N,ON,0,S,N';
              // newday check
              $redis->sadd('S_NoDataVehicle', $vehicleId . ',' . $fcode);
              $redis->hset('H_NoDataVehicleProcess_Time', $vehicleId . ',' . $fcode, $time);
            }
            $redis->hset('H_ProData_' . $fcode, $vehicleId, $tmpPositon);

            try {
              $mysqlDetails = array();
              $status = array();
              $status = array(
                'fcode' => $fcode,
                'vehicleId' => $vehicleId,
                'userName' => $username,
                'status' => config()->get('constant.created'),
                'userIpAddress' => session()->get('userIP', '-'),
              );
              $mysqlDetails   = array_merge($status, $refDataArr);
              $modelname = new AuditVehicle();
              $table = $modelname->getTable();
              $db = $fcode;
              AuditTables::ChangeDB($db);
              $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
              if (count($tableExist) > 0) {
                AuditVehicle::create($mysqlDetails);
              } else {
                AuditTables::CreateAuditVehicle();
                AuditVehicle::create($mysqlDetails);
              }
              AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
            } catch (Exception $e) {
              AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
              logger('Error inside AuditVehicle Create' . $e->getMessage());
            }
          }
          $redis->sadd('S_Vehicles_Dealer_' . $ownerShip . '_' . $fcode, $vehicleId);
          $redis->srem('S_Vehicles_Admin_' . $fcode, $vehicleId);
          $refDataJson = json_decode($refDataJson, true);
          $shortName1 = isset($refDataJson['shortName']) ? $refDataJson['shortName'] : 'default';
          $mobileNo1 = isset($refDataJson['mobileNo']) ? $refDataJson['mobileNo'] : '0123456789';
          $gpsSimNo1 = isset($refDataJson['gpsSimNo1']) ? $refDataJson['gpsSimNo1'] : '0123456789';
          $orgIdOld1 = isset($refDataJson['orgId']) ? $refDataJson['orgId'] : 'default';
          $orgIdOld = strtoupper($orgIdOld1);
          $orgId1 = strtoupper($orgId);
          $redis->hset('H_VehicleName_Mobile_Dealer_' . $ownerShip . '_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortName1 . ':' . $orgId1 . ':' . $gpsSimNo1, $vehicleId);
          $redis->hdel('H_VehicleName_Mobile_Dealer_' . $ownerShip . '_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortName1 . ':DEFAULT:' . $gpsSimNo1, $vehicleId);

          $redis->srem('S_' . $vehicleId . '_' . $fcode, 'S_:' . $fcode);
          $redis->sadd('S_' . $vehicleId . '_' . $fcode, 'S_' . $groupId . ':' . $fcode);
          $redis->del('S_:' . $fcode);
          $newOneUser = $redis->sadd('S_' . $groupId . ':' . $fcode, $userId);
          // regNo & VehId Map
          $redis->hset('H_Vehicle_RegNum_Map_' . $fcode, $vehicleId, $regNo);
          $redis->hset('H_Vehicle_RegNum_Map_' . $fcode, $regNo, $vehicleId);

          ///ram noti       
          $details = $redis->hget('H_Organisations_' . $fcode, $organizationId);
          if ($type == 'Sale' && $type1 == 'new' && $organizationId !== 'default' && $organizationId !== 'Default') {
            if ($details == null) {
              $redis->sadd('S_Organisations_' . $fcode, $organizationId);
              $redis->sadd('S_Organisations_Dealer_' . $ownerShip . '_' . $fcode, $organizationId);

              $orgDataArr = array(
                'mobile' => '1234567890',
                'description' => '',
                'email' => '',
                'address' => '',
                'mobile' => '',
                'startTime' => '',
                'endTime'  => '',
                'atc' => '',
                'etc' => '',
                'mtc' => '',
                'parkingAlert' => '',
                'idleAlert' => '',
                'parkDuration' => '',
                'idleDuration' => '',
                'overspeedalert' => '',
                'sendGeoFenceSMS' => '',
                'radius' => ''
              );
              $orgDataJson = json_encode($orgDataArr);
              $redis->hset('H_Organisations_' . $fcode, $organizationId, $orgDataJson);
              $redis->hset('H_Org_Company_Map', $organizationId, $fcode);
              try {
                $mysqlDetails = array();
                $status = array();
                $status = array(
                  'fcode' => $fcode,
                  'organizationName' => $organizationId,
                  'userName' => $username,
                  'status' => config()->get('constant.created'),
                  'userIpAddress' => session()->get('userIP', '-'),
                );
                $mysqlDetails   = array_merge($status, $orgDataArr);
                $modelname = new AuditOrg();
                $table = $modelname->getTable();
                $db = $fcode;
                AuditTables::ChangeDB($db);
                $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
                if (count($tableExist) > 0) {
                  AuditOrg::create($mysqlDetails);
                } else {
                  AuditTables::CreateAuditOrg();
                  AuditOrg::create($mysqlDetails);
                }
                AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
              } catch (Exception $e) {
                AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                logger('Error inside AuditOrg Create' . $e->getMessage());
              }
            }
          }
          if ($type == 'Sale') {

            $groupname1      = request()->get('groupname');
            $groupname       = strtoupper($groupname1);
            if ($type1 == 'existing' && $groupname !== null && $groupname !== '') {

              $groupId1 = explode(":", $groupname)[0];
              $groupId = strtoupper($groupId1);
            }
            $redis->sadd($groupId . ':' . $fcode1, $vehicleId);
            if ($type1 == 'existing' && $groupname !== null && $groupname !== '') {
              try {
                $vehicleIDList = $redis->smembers($groupId . ':' . $fcode1);
                // logger($vehicleID);
                $mysqlDetails = array(
                  'userName' => $username,
                  'status' => config()->get('constant.new_vehicle_added'),
                  'fcode' => $fcode,
                  'groupName' => $groupname,
                  'vehicleID' => implode(',', $vehicleIDList),
                  'userIpAddress' => session()->get('userIP', '-'),
                  // 'vehicleName'=>$vehicleName
                );
                $modelname = new AuditGroup();
                $table = $modelname->getTable();
                $db = $fcode;
                AuditTables::ChangeDB($db);
                $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
                //return $tableExist;
                if (count($tableExist) > 0) {
                  AuditGroup::create($mysqlDetails);
                } else {
                  AuditTables::CreateAuditGroup();
                  AuditGroup::create($mysqlDetails);
                }
                //AuditGroup::create($details);
                AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
              } catch (Exception $e) {
                AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
                logger('Error inside AuditVehicle New Vehicle Added' . $e->getMessage());
              }
            }
          }

          $redis->srem('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId);
          $redis->hdel('H_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId);
          $temp++;
        }

        if (count($dbarray) !== 0) {
          $query = DB::table('Vehicle_details')->select('vehicle_id')->where('vehicle_id', $vehicleId)->get();
          if ($query == null) {
            DB::table('Vehicle_details')->insert(
              $dbarray
            );
          }
        }

        if ($type == 'Sale') {

          $redis->sadd('S_Groups_' . $fcode, $groupId . ':' . $fcode1);
          $redis->sadd('S_Groups_Dealer_' . $ownerShip . '_' . $fcode, $groupId . ':' . $fcode1);
          $redis->sadd('S_Users_Dealer_' . $ownerShip . '_' . $fcode, $userId);

          $redis->sadd($userId, $groupId . ':' . $fcode1);
          $redis->sadd('S_Users_' . $fcode, $userId);
          $OWN = $username;

          if ($type1 == 'new') {
            $password = request()->get('password');
            if ($password == null) {
              $password = 'awesome';
            }
            try {
              $userProperties = [
                'email' => $userId . '@vamosys.com',
                'emailVerified' => false,
                'password' => $password,
                'displayName' => $userId,
                'disabled' => false,
                'uid' => $userId,
              ];
              // FirebaseController::createUser($userProperties);
            } catch (FirebaseException $e) {
              logger('FirebaseException --->' . $e->getMessage());
              $errMsg = $e->getMessage();
              if ('DUPLICATE_LOCAL_ID' == $errMsg) {
                $errMsg = "User name already exist";
              }
              return redirect()->back()->withInput()->withErrors($errMsg);
            }
            $redis->hmset('H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo, $userId . ':email', $email, $userId . ':password', $password, $userId . ':OWN', $OWN);

            $user = new User([
              'name' => $userId,
              'username' => $userId,
              'email' => $email,
              'mobileNo' => $mobileNo,
              'password' => Hash::make($password),
              'gps_status' => '1',
              'is_verified' => '1'
            ]);

            $user->save();

            $groupNameNew = $groupId . ':' . $fcode1;
            $vehicleID = $redis->smembers($groupId . ':' . $fcode1);

            try {
              $mysqlDetails = array(
                'userName' => $username,
                'status' => config()->get('constant.created'),
                'fcode' => $fcode,
                'groupName' => $email,
                'vehicleID' => implode(",", $vehicleID),
                'vehicleName' => $shortName,
                'userIpAddress' => session()->get('userIP', '-'),
              );
              $modelname = new AuditGroup();
              $table = $modelname->getTable();
              $db = $fcode;
              AuditTables::ChangeDB($db);
              $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
              if (count($tableExist) > 0) {
                AuditGroup::create($mysqlDetails);
              } else {
                AuditTables::CreateAuditGroup();
                AuditGroup::create($mysqlDetails);
              }
              AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
            } catch (Exception $e) {
              AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
            }
            try {
              $details = array(
                'fcode' => $fcode,
                'userId' => $userId,
                'userName' => $username,
                'status' => config()->get('constant.created'),
                'email' => $email,
                'mobileNo' => $mobileNo,
                'password' => Hash::make($password),
                'zoho' => '-',
                'companyName' => '-',
                'cc_email' => '-',
                'groups' => $groupId . ':' . $fcode1,
                'vehicles' => implode(",", $vehicleID),
                'userIpAddress' => session()->get('userIP', '-'),
              );
              $modelname = new AuditUser();
              $table = $modelname->getTable();
              $db = $fcode;
              AuditTables::ChangeDB($db);
              $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
              if (count($tableExist) > 0) {
                AuditUser::create($details);
              } else {
                AuditTables::CreateAuditUser();
                AuditUser::create($details);
              }
              AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
            } catch (Exception $e) {
              AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
            }
          }
        }
      }

      /*
        $date = date('F d Y', strtotime("+0 month"));
        $emailFcode = $redis->hget('H_DealerDetails_' . $fcode, $username);
        $emailFile = json_decode($emailFcode, true);
        $email1 = $emailFile['email'];
        $emailFcode1 = $redis->hget('H_Franchise', $fcode);
        $emailFile1 = json_decode($emailFcode1, true);
        $email2 = $emailFile1['email2'];
        if (session()->get('cur') == 'dealer') {
          $emails = array($email1, $email2);
        } else if (session()->get('cur') == 'admin') {
          $emails = array($email2);
        }
        $count = sizeof($devicearray);
        $i = 0;
        $ownerShip = $username;
        $response = Mail::send('emails.dealeronboard', array('dealerName' => $username, 'Own' => $ownerShip, 'i' => $i, 'nod' => $count, 'date' => $date, 'user' => $userId, 'Devices' => $devicearray), function ($message) use ($emails, $ownerShip) {
          $message->to($emails);
          $message->subject('Onboarded Vehicles - ' . $ownerShip);
        });
      */
      return redirect()->back()->withInput()->with([
        'message' => 'Device Successfully added to User - ' . $userId
      ]);
    } catch (\Exception $e) {
      $username = auth()->id();
      logger(sprintf("EXCEPTIONBYUSER_" . $username . " %s - line - %d " . '------exception---------- ' . $e->getMessage(), __FILE__, __LINE__));
      return redirect()->back()->withInput();
    }
  }

  public function batchSale()
  {
    try {

      $username = auth()->id();
      $redis = Redis::connection();
      $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
      $fcode1 = strtoupper($fcode);
      $deviceList = request()->get('vehicleList');
      $userId = request()->get('userId');
      $userId1 = strtoupper($userId);
      $mobileNo = request()->get('mobileNo');
      $email  = request()->get('email');
      $password = request()->get('password');
      $userType  = request()->get('userType');

      $ownerShip = $username;
      $type = 'Sale';
      $current = Carbon::now();
      $onboardDate = $current->format('d-m-Y');
      $OWN = $username;

      $devicearray = array();
      if ($userType == null) {
        return redirect()->back()->withErrors(trans('messages.selectthesale'));
      }
      if (is_null($deviceList)) {
        return redirect()->back()->withInput()->withErrors(trans('messages.Devicesarenotselected'));
      }

      if ($userType == 'existing') {
        $rules = array(
          'userIdtemp' => 'required',
          'groupname' => 'required',
          'orgId' => 'required',
        );
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
          return redirect()->back()->withInput()->withErrors($validator)->with([
            'message' => trans('messages.PleaseselectuserId')
          ]);
        }

        $userId = request('userIdtemp');
        $organizationId = request('orgId') ?? 'Default';
        $orgId = $organizationId;
        $groupId = request('groupname');
        $groupId = strtoupper($groupId);
        $groupname = Arr::first(explode(":", $groupId));
      }

      if ($userType == 'new') {
        if (session()->get('cur') == 'dealer') {
          $totalReports = $redis->smembers('S_Users_Reports_Dealer_' . $username . '_' . $fcode);
        }
        if ($totalReports != null) {
          foreach ($totalReports as $key => $value) {
            $redis->sadd('S_Users_Reports_' . $userId . '_' . $fcode, $value);
          }
        }
        $rules = array(
          'userId' => 'required|alpha_dash',
          'email' => 'required|email',
          'mobileNo' => 'required|numeric',
          'password' => 'required|min:6',
        );

        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator);
        }
        $val = $redis->hget('H_UserId_Cust_Map', $userId . ':fcode');
        $val1 = $redis->sismember('S_Users_' . $fcode, $userId);
        $valOrg = $redis->sismember('S_Organisations_' . $fcode, $userId);
        $valOrg1 = $redis->sismember('S_Organisations_Admin_' . $fcode, $userId);
        $valGroup = $redis->sismember('S_Groups_' . $fcode, $userId1 . ':' . $fcode1);
        $valGroup1 = $redis->sismember('S_Groups_Admin_' . $fcode, $userId1 . ':' . $fcode1);
        $valFirst = $redis->hget('H_UserId_Cust_Map', ucfirst(strtolower($userId)) . ':fcode');
        $valUpper = $redis->hget('H_UserId_Cust_Map', strtolower($userId) . ':fcode');
        if ($valGroup == 1 || $valGroup1 == 1 || $valOrg == 1 || $valOrg1 == 1) {
          return redirect()->back()->withInput()->withErrors('Name already exist');
        }
        // $userExist = FirebaseController::getUser($userId) !== null;
        $userExist =false;

        if ($val1 == 1 || isset($val) || isset($valFirst) || isset($valUpper) || $userExist) {
          return redirect()->back()->withInput()->withErrors('User Id already exist');
        }
        if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) {
          return redirect()->back()->withInput()->withErrors('Name with admin not acceptable');
        }
        $mobArr = explode(',', $mobileNo);
        foreach ($mobArr as $mob) {
          $val1 = $redis->sismember('S_Users_' . $fcode, $userId);
          if ($val1 == 1) {
            return redirect()->back()->withInput()->withErrors($mob . ' User Id already exist');
          }
        }
        $groupname  = $userId;
        $groupId = strtoupper($groupname . ':' . $fcode1);
        $redis->sadd('S_Users_Admin_' . $fcode, $userId);
        $redis->sadd('S_Groups_Admin_' . $fcode,  $groupId);
        $redis->sadd('S_Users_' . $fcode, $userId);
        $redis->sadd('S_Organisations_' . $fcode, $userId);
        $redis->sadd('S_Organisations_Admin_' . $fcode, $userId);
        $redis->sadd('S_Groups_' . $fcode,  $groupId);
        $redis->sadd($userId, $groupId);


        $password = request()->get('password');
        if ($password == null) {
          $password = 'awesome';
        }
        $redis->hmset('H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo, $userId . ':email', $email, $userId . ':password', $password, $userId . ':OWN', $OWN);

        $user = new User([
          'name' => $userId,
          'username' => $userId,
          'email' => $email,
          'mobileNo' => $mobileNo,
          'password' => Hash::make($password),
          'gps_status' => '1',
          'is_verified' => '1'
        ]);
        try{
          $newUserData = [
            'name' => $userId,
            'mobileNo'=>$mobileNo,
            'email'=>$email,
          ];
          $mysqlDetails = [
            'userIpAddress' => session()->get('userIP'),
            'userName'=> $username,
            'type'=>config()->get('constant.user'),
            'name'=>$userId,
            'status'=>config()->get('constant.created'),
            'oldData'=>"",
            'newData'=>json_encode($newUserData),
          ];
          AuditNewController::updateAudit($mysqlDetails); 
        }catch(Exception $e){
          logger($e->getMessage().'----------------'.$e->getLine());
        }
        $user->save();
        try {
          $userProperties = [
            'email' => $userId . '@vamosys.com',
            'emailVerified' => false,
            'password' => $password,
            'displayName' => $userId,
            'disabled' => false,
            'uid' => $userId,
          ];
          // logger($userProperties);
          // FirebaseController::createUser($userProperties);
        } catch (FirebaseException $e) {
          return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

        $organizationId = $userId;
        $orgId = $organizationId;
      }

      $shortNameArr = request()->get('shortName');
      $vehicleTypeArr = request()->get('vehicleType');
      $gpsSimNoArr =request()->get('gpsSimNo');

      $temp = 0;
      $dbarray = array();
      $dbtemp = 0;
      foreach ($deviceList as $key => $device) {
        $myArray = explode(',', $device);
        $deviceId = $myArray[0];
        $vehicleId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $deviceId);
        $devicearray = Arr::add($devicearray, $vehicleId, $deviceId);
        $deviceDataArr = array(
          'deviceid' => $deviceId,
          'deviceidtype' => $myArray[1],
        );
        $deviceDataJson = json_encode($deviceDataArr);
        if ($vehicleId !== null) {

          $refDataJson1 = $redis->hget('H_RefData_' . $fcode, $vehicleId);
          $refDataJson1 = json_decode($refDataJson1, true);

          $temOrg = $refDataJson1['orgId'] ?? 'default';
          $licenceId = $refDataJson1['Licence'] ?? 'Advance';
          $paymentId = $refDataJson1['Payment_Mode'] ?? 'Monthly';


          try {
            $licence_id = DB::select('select licence_id from Licence where type = :type', ['type' => $licenceId]);
            $payment_mode_id = DB::select('select payment_mode_id from Payment_Mode where type = :type', ['type' => $paymentId]);
            $licence_id = $licence_id[0]->licence_id;
            $payment_mode_id = $payment_mode_id[0]->payment_mode_id;

            DB::table('Vehicle_details')->insert(
              array(
                'vehicle_id' => $vehicleId,
                'fcode' => $fcode,
                'sold_date' => Carbon::now(),
                'renewal_date' => Carbon::now(),
                'sold_time_stamp' => round(microtime(true) * 1000),
                'month' => date('m'),
                'year' => date('Y'),
                'payment_mode_id' => $payment_mode_id,
                'licence_id' => $licence_id,
                'belongs_to' => $username,
                'device_id' => $deviceId,
                'status' => isset($refDataJson1['descriptionStatus']) ? $refDataJson1['descriptionStatus'] : ''
              )
            );
            $dbarray[$dbtemp++] = array(
              'vehicle_id' => $vehicleId,
              'fcode' => $fcode,
              'sold_date' => Carbon::now(),
              'renewal_date' => Carbon::now(),
              'sold_time_stamp' => round(microtime(true) * 1000),
              'month' => date('m'),
              'year' => date('Y'),
              'payment_mode_id' => $payment_mode_id,
              'licence_id' => $licence_id,
              'belongs_to' => $username,
              'device_id' => $deviceId,
              'orgId' => $orgId,
              'status' => isset($refDataJson1['descriptionStatus']) ? $refDataJson1['descriptionStatus'] : ''
            );
          } catch (\Exception $e) {
            logger("Exception" . $e->getMessage() . "------>line Number---> " . $e->getLine());
          }
          $shortName = $shortNameArr[$key] ?? "$vehicleId";
          $vehicleType = $vehicleTypeArr[$key]??"Bus";
          $gpsSimNo =$gpsSimNoArr[$key] ?? "";
          $vehicleExpiry = date('Y-m-d', strtotime(date("d-m-Y", time()) . "+ 1 year"));

          $refDataArr = array(
            'deviceId' => $deviceId,
            'orgId' => $orgId,
            'OWN' => $ownerShip,
            'onboardDate' => $onboardDate,
            'vehicleExpiry' => $vehicleExpiry,
            'shortName'=>$shortName,
            'vehicleType'=>$vehicleType,
            'gpsSimNo'=>$gpsSimNo
          );

          // $shortName = isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : '';
          $regNo = isset($refDataJson1['regNo']) ? $refDataJson1['regNo'] : $shortName;
          $refDataArr = array_merge($refDataJson1, $refDataArr);
          $refDataJson = json_encode($refDataArr);
          $redis->hset('H_RefData_' . $fcode, $vehicleId, $refDataJson);

          $time = microtime(true);
          $time = round($time * 1000);
          $tmpPositon = $redis->hget('H_ProData_' . $fcode, $vehicleId);
          $LatLong = $redis->hget('H_Franchise_LatLong', $fcode . ":" . $ownerShip);
          if ($LatLong != null) {
            $data_arr = explode(":", $LatLong);
            $latitude = $data_arr[0];
            $longitude = $data_arr[1];
            $tmpPositon =  $latitude . ',' . $longitude . ',0,N,' . $time . ',0.0,N,N,ON,0,S,N';
            // newday check
            $redis->sadd('S_NoDataVehicle', $vehicleId . ',' . $fcode);
            $redis->hset('H_NoDataVehicleProcess_Time', $vehicleId . ',' . $fcode, $time);
          }
          $redis->hset('H_ProData_' . $fcode, $vehicleId, $tmpPositon);

          try {
            $mysqlDetails = array();
            $status = array();
            try {

              $mysqlDetails = [
                'userIpAddress' => session()->get('userIP'),
                'userName' => $username,
                'type' => config()->get('constant.vehicle'),
                'name' => $vehicleId,
                'status' => config()->get('constant.created'),
                'oldData' => "",
                'newData' => $refDataJson
              ];
              AuditNewController::updateAudit($mysqlDetails);
            } catch (Exception $e) {
              AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
              logger('Error inside AuditVehicle Create' . $e->getMessage());
            }

          } catch (Exception $e) {
            AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
            logger('Error inside AuditVehicle Create' . $e->getMessage());
          }
        }
        $redis->sadd('S_Vehicles_Dealer_' . $ownerShip . '_' . $fcode, $vehicleId);
        $redis->srem('S_Vehicles_Admin_' . $fcode, $vehicleId);
        $refDataJson = json_decode($refDataJson, true);
        $shortName1 = isset($refDataJson['shortName']) ? $refDataJson['shortName'] : 'default';
        $mobileNo1 = isset($refDataJson['mobileNo']) ? $refDataJson['mobileNo'] : '0123456789';
        $gpsSimNo1 = isset($refDataJson['gpsSimNo1']) ? $refDataJson['gpsSimNo1'] : '0123456789';
        $orgIdOld1 = isset($refDataJson['orgId']) ? $refDataJson['orgId'] : 'default';
        $orgIdOld = strtoupper($orgIdOld1);
        $orgId1 = strtoupper($orgId);
        $redis->hset('H_VehicleName_Mobile_Dealer_' . $ownerShip . '_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortName1 . ':' . $orgId1 . ':' . $gpsSimNo1, $vehicleId);
        $redis->hdel('H_VehicleName_Mobile_Dealer_' . $ownerShip . '_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortName1 . ':DEFAULT:' . $gpsSimNo1, $vehicleId);

        $redis->srem('S_' . $vehicleId . '_' . $fcode, 'S_:' . $fcode);
        $redis->sadd('S_' . $vehicleId . '_' . $fcode, 'S_' . $groupId);
        $redis->del('S_:' . $fcode);
        $redis->sadd('S_' . $groupId, $userId);
        // regNo & VehId Map
        $redis->hset('H_Vehicle_RegNum_Map_' . $fcode, $vehicleId, $regNo);
        $redis->hset('H_Vehicle_RegNum_Map_' . $fcode, $regNo, $vehicleId);

        $details = $redis->hget('H_Organisations_' . $fcode, $organizationId);
        if ($userType == 'new' && strtolower($organizationId) !== 'default') {
          if ($details == null) {
            $redis->sadd('S_Organisations_' . $fcode, $organizationId);
            $redis->sadd('S_Organisations_Dealer_' . $ownerShip . '_' . $fcode, $organizationId);

            $orgDataArr = array(
              'mobile' => '1234567890',
              'description' => '',
              'email' => '',
              'address' => '',
              'mobile' => '',
              'startTime' => '',
              'endTime'  => '',
              'atc' => '',
              'etc' => '',
              'mtc' => '',
              'parkingAlert' => '',
              'idleAlert' => '',
              'parkDuration' => '',
              'idleDuration' => '',
              'overspeedalert' => '',
              'sendGeoFenceSMS' => '',
              'radius' => ''
            );
            $orgDataJson = json_encode($orgDataArr);
            $redis->hset('H_Organisations_' . $fcode, $organizationId, $orgDataJson);
            $redis->hset('H_Org_Company_Map', $organizationId, $fcode);
            try {
              $mysqlDetails = array();
              $mysqlDetails = [
                'userIpAddress' => session()->get('userIP'),
                'userName' => $username,
                'type' => config()->get('constant.organization'),
                'name' => $organizationId,
                'status' => config()->get('constant.created'),
                'oldData' => "",
                'newData' => $orgDataJson
              ];
              AuditNewController::updateAudit($mysqlDetails);

            } catch (Exception $e) {
              AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
              logger('Error inside AuditOrg Create' . $e->getMessage());
            }
          }
        }


        $redis->sadd($groupId, $vehicleId);
        if ($userType == 'existing') {
          try {
            $vehicleIDList = $redis->smembers($groupId);
            $mysqlDetails = [
              'userIpAddress' => session()->get('userIP'),
              'userName' => $username,
              'type' => config()->get('constant.group'),
              'name' => $groupname,
              'status' => config()->get('constant.new_vehicle_added'),
              'oldData' => "",
              'newData' => json_encode([
                'vehicleId'=>join(',', $vehicleIDList)
              ])
            ];
            AuditNewController::updateAudit($mysqlDetails);

          } catch (Exception $e) {
            AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
            logger('Error inside AuditVehicle New Vehicle Added' . $e->getMessage());
          }
        }

        $redis->srem('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId);
        $redis->hdel('H_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId);
        $temp++;
      }

      if (count($dbarray) !== 0) {
        $query = DB::table('Vehicle_details')->select('vehicle_id')->where('vehicle_id', $vehicleId)->get();
        if ($query == null) {
          DB::table('Vehicle_details')->insert(
            $dbarray
          );
        }
      }

      if ($type == 'Sale') {

        $redis->sadd('S_Groups_' . $fcode, $groupId);
        $redis->sadd('S_Groups_Dealer_' . $ownerShip . '_' . $fcode, $groupId);
        $redis->sadd('S_Users_Dealer_' . $ownerShip . '_' . $fcode, $userId);

        $redis->sadd($userId, $groupId);
        $redis->sadd('S_Users_' . $fcode, $userId);
        $OWN = $username;
      }



      return redirect()->back()->withInput()->with([
        'message' => 'Device Successfully added to User - ' . $userId
      ]);
    } catch (\Exception $e) {
      dd($e);
      $username = auth()->id();
      logger(sprintf("EXCEPTIONBYUSER_" . $username . " %s - line - %d " . '------exception---------- ' . $e->getMessage(), __FILE__, __LINE__));
      return redirect()->back()->withInput();
    }
  }
  protected function schedule(Schedule $schedule)
  {
    $schedule->call(function () {
      //DB::table('recent_users')->delete();
    })->everyMinute();
  }

}
