<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use languageSuggession;

class UserPrePageController extends Controller
{

  public function userPreference()
  {

    $totalReportList = array();
    $list = array();
    $totalList = array();
    $reportsList = array();
    $username = auth()->id();
    $user = $username;
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $userPrePage = $redis->hget('H_UserId_Cust_Map', $user . ':userPre');
    $totalReport = null;

    $dealerOrUser = $redis->sismember('S_Dealers_' . $fcode, $user);
    if ($dealerOrUser) {
      $totalReports = $redis->smembers("S_Users_Reports_Dealer_" . $user . '_' . $fcode);
    } else {
      $totalReports = $redis->smembers("S_Users_Reports_" . $user . '_' . $fcode);
    }

    if ($totalReports != null) {
      foreach ($totalReports as $key => $value) {

        $rep = explode(":", $value)[0];
        if ($rep == 'AC') {
          $totalReport[explode(":", $value)[1]][] = $value;
          $totalReport[explode(":", $value)[1]][] = 'SEC_ENGINE_ON:sensor';
        } else if ($rep == 'IGNITION') {
          $totalReport[explode(":", $value)[1]][] = $value;
          $totalReport['Sensor'][] = 'PRI_ENGINE_ON:sensor';
        } else if ($rep == 'RFID') {
          $totalReport[explode(":", $value)[1]][] = $value;
          $totalReport['Sensor'][] = 'RFID_NEW:sensor';
        } else if ($rep == 'FUEL') {
          $totalReport['Sensor'][] = 'FUEL_ANALYTICS:sensor';
        } else if ($rep == 'FUEL_CONSOLIDATE_REPORT') {
          $totalReport[explode(":", $value)[1]][] = $value;
          $totalReport['Sensor'][] = 'VEHICLE_WISE_FUEL:sensor';
          $totalReport['Sensor'][] = 'VEHICLE_WISE_INTRA_FUEL:sensor';
        } else if ($rep == 'LOAD') {
          logger(' load report removed');
        } else {
          $totalReport[explode(":", $value)[1]][] = $value;
        }
      }
      $totalList = $totalReport;
    }
    //return $totalList;
    if ($totalList == null && $totalList == null) {
      session()->flash('message', ' No Reports Found' . '!');
      //return $userPrePage;
      return view('vdm.users.UserPreReports', array(
        'userId' => $user
      ))
        ->with('totalList', $totalList)
        ->with('userPrePage', $userPrePage);
    } else {

      return view('vdm.users.UserPreReports', array(
        'userId' => $user
      ))
        ->with('totalList', $totalList)
        ->with('userPrePage', $userPrePage);
    }
  }

  public function updateUserPreference()
  {

    $userId = request()->get('userId');
    logger($userId);
    $userPrePage = request()->get('userPrePage');
    $list = array();
    $totalList = array();
    $reportsList = array();
    $username = auth()->id();
    $user = $username;
    $redis = Redis::connection();
    $oldUserPrePage = $redis->hget('H_UserId_Cust_Map', $user . ':userPre');
    $redis->hmset('H_UserId_Cust_Map', $userId . ':userPre', $userPrePage);
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $userPrePage = $redis->hget('H_UserId_Cust_Map', $user . ':userPre');
    $totalReport = null;

    $dealerOrUser = $redis->sismember('S_Dealers_' . $fcode, $user);
    if ($dealerOrUser) {
      $totalReports = $redis->smembers("S_Users_Reports_Dealer_" . $user . '_' . $fcode);
    } else {
      $totalReports = $redis->smembers("S_Users_Reports_" . $user . '_' . $fcode);
    }

    if ($totalReports != null) {
      foreach ($totalReports as $key => $value) {

        $rep = explode(":", $value)[0];
        if ($rep == 'AC') {
          $totalReport[explode(":", $value)[1]][] = $value;
          $totalReport[explode(":", $value)[1]][] = 'SEC_ENGINE_ON:sensor';
        } else if ($rep == 'IGNITION') {
          $totalReport[explode(":", $value)[1]][] = $value;
          $totalReport['Sensor'][] = 'PRI_ENGINE_ON:sensor';
        } else if ($rep == 'RFID') {
          $totalReport[explode(":", $value)[1]][] = $value;
          $totalReport['Sensor'][] = 'RFID_NEW:sensor';
        } else if ($rep == 'FUEL') {
          $totalReport['Sensor'][] = 'FUEL_ANALYTICS:sensor';
        } else if ($rep == 'FUEL_CONSOLIDATE_REPORT') {
          $totalReport[explode(":", $value)[1]][] = $value;
          $totalReport['Sensor'][] = 'VEHICLE_WISE_FUEL:sensor';
          $totalReport['Sensor'][] = 'VEHICLE_WISE_INTRA_FUEL:sensor';
        } else if ($rep == 'LOAD') {
          logger(' load report removed');
        } else {
          $totalReport[explode(":", $value)[1]][] = $value;
        }
      }
      $totalList = $totalReport;
    }
    //return $totalList;
    if ($totalList == null && $totalList == null) {
      session()->flash('message', ' No Reports Found' . '!');
      //return $userPrePage;
      return view('vdm.users.UserPreReports', array(
        'userId' => $user
      ))
        ->with('totalList', $totalList)
        ->with('userPrePage', $userPrePage);
    } else {
      //return $userPrePage;  
      session()->flash('success', 'Successfully Updated!...');
      logger('before entry in CustomizeLandingPage ');
      try {
        //$userIpAddress=$_SERVER['REMOTE_ADDR'];
        if (!$oldUserPrePage) $oldUserPrePage = 'Track';
        if (!$userPrePage) $userPrePage = 'Track';
        $userIpAddress = session()->get('userIP');
        $mysqlDetails = array();
        $mysqlDetails = array(
          'fcode' => $fcode,
          'userName' => $username,
          'userIpAddress' => $userIpAddress,
          'status' => config()->get('constant.updated'),
          'oldLandingPage' => $oldUserPrePage,
          'newLandingPage' => $userPrePage,
        );
        $modelname = new CustomizeLandingPage();
        $table = $modelname->getTable();
        $db = $fcode;
        AuditTables::ChangeDB($db);
        $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
        if (count($tableExist) > 0) {
          CustomizeLandingPage::create($mysqlDetails);
        } else {
          AuditTables::CreateCustomizeLandingPage();
          CustomizeLandingPage::create($mysqlDetails);
        }
        AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
      } catch (Exception $e) {
        AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
        logger('Error inside CustomizeLandingPage Update' . $e->getMessage());
        logger($mysqlDetails);
      }
      logger('entry added in CustomizeLandingPage');
      return view('vdm.users.UserPreReports', array(
        'userId' => $user
      ))
        ->with('totalList', $totalList)
        ->with('userPrePage', $userPrePage);
    }
  }



  public function  getFileNames()
  {
    $fileNames = [];
    $s3 = app('aws')->createClient('s3', [
      'endpoint' => config()->get('constant.endpoint'),
    ]);
    $objects = $s3->getListObjectsIterator(array(
      'Bucket' => config()->get('constant.bucket'),
      'Prefix' => 'ReleaseNotes/'
    ));
    foreach ($objects as $object) {
      if (strpos($object['Key'], '.pdf') !== false) {
        $fileName = str_replace('ReleaseNotes/', '', $object['Key']);
        array_push($fileNames, $fileName);
      }
    }
    return $fileNames;
  }

  public function sendSuggession()
  {
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $wordsList = request()->get('wordList');
    $mysqlDetails = array(
      'userName' => $username,
      'fcode' => $fcode,
    );
    logger($mysqlDetails);
    $modelname = new languageSuggession();
    $table = $modelname->getTable();
    $db = config()->get('constant.VAMOSdb');
    $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
    //return $tableExist;
    if (count($tableExist) > 0) {
      foreach ($wordsList as $word) {
        $langDetails = [];
        $langDetails = Arr::add($mysqlDetails, 'wordsList', $word);
        languageSuggession::create($langDetails);
      }
    } else {
      AuditTables::CreatelanguageSuggession();
      foreach ($wordsList as $word) {
        $langDetails = [];
        $langDetails = Arr::add($mysqlDetails, 'wordsList', $word);
        languageSuggession::create($langDetails);
      }
    }
    return 'success';
  }

  public function showLanguageSuggession()
  {

    try {
      $username = auth()->id();
      $redis = Redis::connection();
      $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
      $modelname = new languageSuggession();
      $table = $modelname->getTable();
      $fcode = config()->get('constant.VAMOSdb');
      //AuditTables::CreateAuditFrans();
      $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $fcode)->where('table_name', '=', $table)->get();

      if (count($tableExist) > 0 && (languageSuggession::first() != null)) {
        $columns = array_keys(languageSuggession::first()->toArray());
        $rows = languageSuggession::all();
      } else {
        $rows = [];
        $columns = array(
          0 => 'id',
          1 => 'fcode',
          2 => 'userName',
          3 => 'wordsList',
          4 => 'flag',
        );
      }
    } catch (customException $e) {
      //display custom message customException
      logger($e->errorMessage());
    }
    unset($columns[array_search('fcode', $columns)], $columns[array_search('flag', $columns)]);
    //return 'hello';
    $count = VdmFranchiseController::liveVehicleCountV2();
    $timezoneKey = $redis->exists('Update:Timezone');
    $apnKey = $redis->exists('Update:Apn');

    return view('vdm.franchise.viewLangSugg', array('columns' => $columns, 'rows' => $rows, 'count' => $count))->with('timezoneKey', $timezoneKey)->with('apnKey', $apnKey);
  }

  public function acceptWord($id)
  {
    $task = languageSuggession::where('id', $id)
      ->update(array('flag' => 1));

    session()->flash('message', 'Word Accepted Successfully!');

    return redirect()->to('showLnSugg');
  }

  public function rejectWord($id)
  {
    $task = languageSuggession::where('id', $id)
      ->update(array('flag' => 0));

    session()->flash('errormessage', 'Word Ignored Successfully!');

    return redirect()->to('showLnSugg');
  }

  public function getUserName()
  {
    $redis = Redis::connection();
    $username = auth()->id();
    $newname = $redis->hget('H_Newusernames', $username);
    if ($newname) {
      $names = $username . ':' . $newname;
      return $names;
    }
  }
}
