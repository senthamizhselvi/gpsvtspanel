<?php


namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redis;

use function Psy\debug;

class DeviceController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function indexOld()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		// $fcode = "VAM";
		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		$prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';

		$devicesList = $redis->smembers('S_Device_' . $fcode);
		$temp = 0;
		$deviceMap = array();
		$count = count($devicesList);
		if (request()->server('HTTP_HOST') === "localhost") {
            $devicesList = array_slice($devicesList, 0, 10);
        }
		foreach($devicesList as $i => $device){
			$vechicle = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $device);
			if (is_null($vechicle)) continue;

			$refData 	= $redis->hget('H_RefData_' . $fcode, $vechicle);
			$refData	= json_decode($refData, true);
			$orgId 		= isset($refData['OWN']) ? $refData['OWN'] : '-';
			$date = isset($refData['date']) ? $refData['date'] : '';
			$date = trim($date);
			if ($date == '') {
				$date1 = '';
			} else {
				$date1 = date("d-m-Y", strtotime($date));
			}
			$onDate = isset($refData['onboardDate']) ? $refData['onboardDate'] : $date1;
			$onDate = trim($onDate);

			if ($onDate == "null" || $onDate == "") {
				$onboardDate = $date1;
			} else {
				$onboardDate = $onDate;
			}
			$vehicleExpiry = isset($refData['vehicleExpiry']) ? $refData['vehicleExpiry'] : '';
			$type = isset($refData['Licence']) ? $refData['Licence'] : 'Advance';
			if ($prepaid == 'yes') {
				$LicenceId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vechicle);
				$LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
				$LicenceRef	= json_decode($LicenceRef, true);
				$lic 		= isset($LicenceRef['LicenceissuedDate']) ? $LicenceRef['LicenceissuedDate'] : ' ';
				$LicenceExpiryDate = isset($LicenceRef['LicenceExpiryDate']) ? $LicenceRef['LicenceExpiryDate'] : ' ';
				$deviceMap 	= Arr::add($deviceMap, $i, $vechicle . ',' . $device . ',' . $orgId . ',' . $lic . ',' . $onboardDate . ',' . $vehicleExpiry . ',' . $type . ',' . $LicenceId . ',' . $LicenceExpiryDate);
			} else {
				$lic 		= isset($refData['licenceissuedDate']) ? $refData['licenceissuedDate'] : ' ';
				$deviceMap 	= Arr::add($deviceMap, $i, $vechicle . ',' . $device . ',' . $orgId . ',' . $lic . ',' . $onboardDate . ',' . $vehicleExpiry . ',' . $type);
			}
		}
		return view('vdm.business.device', [
			'deviceMap' => $deviceMap,
			'prepaid' => $prepaid
		]);
	}

	public function index()
	{
		
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		// $fcode = "VAM";
		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		$prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
		$devicesList = $redis->smembers('S_Device_' . $fcode);


		if (request()->server('HTTP_HOST') === "localhost") {
            $devicesList = array_slice($devicesList, 0, 500);
        }
		
		$devicesList = array_unique($devicesList);
		sort($devicesList);
		$vehicleList  = $redis->hmget("H_Vehicle_Device_Map_$fcode",$devicesList);
		$licenceIdList = $redis->hmget("H_Vehicle_LicenceId_Map_$fcode",$vehicleList);
		$licenceRefList = $redis->hmget("H_LicenceExpiry_$fcode",$licenceIdList);
		$refDataList = $redis->hmget("H_RefData_$fcode",$vehicleList); 

		return view('vdm.business.deviceNew', [
			"devicesList"=>$devicesList,
			"vehicleList"=>$vehicleList,
			"licenceIdList"=>$licenceIdList,
			"licenceRefList"=>$licenceRefList,
			"refDataList"=>$refDataList,
			'prepaid' => $prepaid
		]);
	}
	
	public function deviceListAjax()
	{
		$data = request()->all();
		$search = $data['search']['value'];
		$draw = $data['draw'];
		$row = $data['start'];
		$rowperpage = $data['length']; // Rows display per page
		$columnSortOrder = $data['order'][0]['dir']; // asc or desc
		$text_trim = str_replace(' ', '', $search);
		$text_word = strtoupper($text_trim);


		$username = auth()->user()->displayName;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$redisDealerCacheID = 'S_Device_' . $fcode;



		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		$prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';

		$devicesList = $redis->smembers('S_Device_' . $fcode);

		$deviceCount = count($devicesList);
		sort($devicesList, SORT_NATURAL | SORT_FLAG_CASE);

		$temp =  $row + 1;

		if ($search == '') $orgL = array_slice($devicesList, $row, $rowperpage);
		if ($search !== '') {

			$cou = $redis->SCARD($redisDealerCacheID);
			$orgLi = $redis->sScan($redisDealerCacheID, 0, 'count', $cou, 'match', '*' . $search . '*');
			$orgL = $orgLi[1];
			$orgLcount = count($orgL);
			$orgL = array_slice($orgL, $row, $rowperpage);
		}

		if ($columnSortOrder == 'asc') {
			sort($orgL);
		} else {
			rsort($orgL);
		}

		$DATA = array();

		foreach ($orgL as $key => $device) {

			$vechicle = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $device);
			if (is_null($vechicle)) continue;

			$refData 	= $redis->hget('H_RefData_' . $fcode, $vechicle);
			$refData	= json_decode($refData, true);
			$orgId 		= isset($refData['OWN']) ? $refData['OWN'] : ' ';
			$date = isset($refData['date']) ? $refData['date'] : '';
			if ($date == '' || $date == ' ') {
				$date1 = '';
			} else {
				$date1 = date("d-m-Y", strtotime($date));
			}
			$onDate = isset($refData['onboardDate']) ? $refData['onboardDate'] : $date1;
			$onDate = trim($onDate);

			if ($onDate == "null" || $onDate == "") {
				$onboardDate = $date1;
			} else {
				$onboardDate = $onDate;
			}
			//$onboardDate=isset($refData['onboardDate'])?$refData['onboardDate']:'null';
			$vehicleExpiry = isset($refData['vehicleExpiry']) ? $refData['vehicleExpiry'] : '';
			$type = isset($refData['Licence']) ? $refData['Licence'] : 'Advance';
			if ($prepaid == 'yes') {
				$LicenceId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vechicle);
				$LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
				$LicenceRef	= json_decode($LicenceRef, true);
				$lic 		= isset($LicenceRef['LicenceissuedDate']) ? $LicenceRef['LicenceissuedDate'] : ' ';
				$LicenceExpiryDate = isset($LicenceRef['LicenceExpiryDate']) ? $LicenceRef['LicenceExpiryDate'] : ' ';
			} else {
				$lic = isset($refData['licenceissuedDate']) ? $refData['licenceissuedDate'] : '';
			}

			$editBtn = '<a class="btn btn-sm btn-warning" href="vdmVehicles/edit/' . $vechicle . '">Edit Vehicle</a>';
			$moveBtn = '<a class="btn btn-sm btn-danger" href="vdmVehicles/move_vehicle/' . $vechicle . '">Move Vehicle</a>';
			$DATA[] = [
				"ID" => $temp,
				"Licence ID" => $prepaid == 'yes' ? $LicenceId : '',
				"Device ID" => $device,
				"Vehicle Id" => $vechicle,
				"Dealer Name" => $orgId,
				"Licence Issued Date" => $lic,
				"Type" => $type,
				"Onboard Date" => $onboardDate,
				"Licence Expiry" => $prepaid == 'yes' ? $LicenceExpiryDate : '',
				"Vehicle Expiry" => $vehicleExpiry,
				"Move Vehicle" => $moveBtn,
				"Edit Vehicle" => $editBtn,
			];

			$temp++;
		}
		$count = $search == '' ? $deviceCount : $orgLcount;
		$response = [
			"draw" => intval($draw),
			"iTotalRecords" =>  $deviceCount,
			"iTotalDisplayRecords" => $count,
			"aaData" => $DATA
		];

		return response()->json($response);
	}
}
