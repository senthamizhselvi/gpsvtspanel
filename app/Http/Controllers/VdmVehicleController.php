<?php

namespace App\Http\Controllers;

use App\Exports\FormUpdateExport;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Facades\Excel;
use Predis\Client as Client;

class VdmVehicleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexOld()
    {
        $username = auth()->id();
        session()->forget('page');
        session()->put('vCol', 1);
        if ($username == "smpadmin" && session()->get('cur') == 'admin') return view('vdm.vehicles.index');

        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $franDetails_json = $redis->hget('H_Franchise', $fcode);
        $franjsonData = json_decode($franDetails_json, true);
        $prepaid = isset($franjsonData['prepaid']) ? $franjsonData['prepaid'] : 'no';


        if (session()->get('cur') == 'dealer') {
            $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $vehicleListId = 'S_Vehicles_Admin_' . $fcode;
        } else {
            $vehicleListId = 'S_Vehicles_' . $fcode;
        }
        $vehicleList = $redis->smembers($vehicleListId);
        $deviceList = null;
        $deviceId = null;
        $shortName = null;
        $shortNameList = null;
        $portNo = null;
        $portNoList = null;
        $mobileNo = null;
        $mobileNoList = null;
        $orgIdList = null;
        $deviceModelList = null;
        $expiredList = null;
        $statusList = null;
        $onboardDateList = null;
        $licenceList = null;
        $commList = null;
        $locList = null;
        $licenceIdList = null;
        $LicexpiredPeriodList = null;
        $versionList = null;
        $servernameList = null;
        $timezoneList = null;
        $apnList = null;
        $licTypeList = null;
        if (request()->server('HTTP_HOST') === "localhost") {
            $vehicleList = array_slice($vehicleList, 0, 10);
        }
        sort($vehicleList, SORT_NATURAL | SORT_FLAG_CASE);
        foreach ($vehicleList as $key => $vehicle) {

            $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);
            if (!isset($vehicleRefData)) continue;
            $vehicleRefData = json_decode($vehicleRefData, true);
            $deviceId   =  $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicle);

            if ((session()->get('cur') == 'dealer' &&  $redis->sismember('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId) == 0) || session()->get('cur') == 'admin') {
                if ($prepaid == 'yes') {
                    $LicenceId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vehicle);
                    $licenceIdList = Arr::add($licenceIdList, $vehicle, $LicenceId);
                    $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
                    $LicenceRefData = json_decode($LicenceRef, true);
                    $licence = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
                    $licenceList = Arr::add($licenceList, $vehicle, $licence);
                    $LicenceexpiredPeriod = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
                    $LicexpiredPeriodList = Arr::add($LicexpiredPeriodList, $vehicle, $LicenceexpiredPeriod);
                } else {
                    $licence = isset($vehicleRefData['licenceissuedDate']) ? $vehicleRefData['licenceissuedDate'] : '';
                    $licenceList = Arr::add($licenceList, $vehicle, $licence);
                }
                $expiredPeriod = isset($vehicleRefData['vehicleExpiry']) ? $vehicleRefData['vehicleExpiry'] : '-';
                $expiredPeriod = trim($expiredPeriod);
                if ($expiredPeriod == "" || $expiredPeriod == "null") {
                    $expiredPeriod = '-';
                }
                $expiredList = Arr::add($expiredList, $vehicle, $expiredPeriod);
                $deviceList = Arr::add($deviceList, $vehicle, $deviceId);
                $shortName = isset($vehicleRefData['shortName']) ? $vehicleRefData['shortName'] : 'nill';
                $shortNameList = Arr::add($shortNameList, $vehicle, $shortName);
                $portNo = isset($vehicleRefData['portNo']) ? $vehicleRefData['portNo'] : 9964;
                $portNoList = Arr::add($portNoList, $vehicle, $portNo);
                $mobileNo = isset($vehicleRefData['gpsSimNo']) ? $vehicleRefData['gpsSimNo'] : 99999;
                $mobileNoList = Arr::add($mobileNoList, $vehicle, $mobileNo);
                $orgId = isset($vehicleRefData['orgId']) ? $vehicleRefData['orgId'] : 'Default';
                $orgIdList = Arr::add($orgIdList, $vehicle, $orgId);
                $deviceModel = isset($vehicleRefData['deviceModel']) ? $vehicleRefData['deviceModel'] : 'nill';
                $deviceModelList = Arr::add($deviceModelList, $vehicle, $deviceModel);
                $countryTimezone = isset($vehicleRefData['country']) ? $vehicleRefData['country'] : '';
                if ($countryTimezone == '' || $countryTimezone == 'MIT' || $countryTimezone == 'no' || $countryTimezone == "null") {
                    $countryTimezone = isset($franjsonData['timeZone']) ? $franjsonData['timeZone'] : 'Asia/Kolkata';
                }
                // lic list
                $licType = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
                $licTypeList = Arr::add($licTypeList, $vehicle, $licType);
                // device version
                $versionofVehicle = $redis->hget('H_Version_' . $fcode, $vehicle);
                $versions = isset($versionofVehicle) ? $versionofVehicle : '-';
                $versionList = Arr::add($versionList, $vehicle, $versions);
                //timezone and apn
                $timezoneData = $redis->hget('H_DeviceDetail_' . $fcode, $vehicle);
                $timezoneRefData = json_decode($timezoneData, true);
                $timezone = isset($timezoneRefData['timezone']) ? $timezoneRefData['timezone'] : '-';
                $timezoneList = Arr::add($timezoneList, $vehicle, $timezone);
                $apn = isset($timezoneRefData['apn']) ? $timezoneRefData['apn'] : '-';
                $apnList = Arr::add($apnList, $vehicle, $apn);
                //servername
                $serverN = $redis->hget('H_VehicleMapIpaddress_' . $fcode, $vehicle);
                $servername = isset($serverN) ? $serverN : '-';
                $servernameList = Arr::add($servernameList, $vehicle, $servername);
                $statusVehicle = $redis->hget('H_ProData_' . $fcode, $vehicle);
                $statusSeperate = explode(',', $statusVehicle);
                $statusSeperate1 = isset($statusSeperate[7]) ? $statusSeperate[7] : 'N';
                $statusList = Arr::add($statusList, $vehicle, $statusSeperate1);
                $lastComm = isset($statusSeperate[36]) ? $statusSeperate[36] : '';
                $lastLoc = isset($statusSeperate[4]) ? $statusSeperate[4] : '';
                try {
                    date_default_timezone_set($countryTimezone);
                } catch (\Exception $e) {
                    date_default_timezone_set('Asia/Kolkata');
                }
                if ($lastComm == '' || $lastComm == null) {
                    $setted = '';
                } else {
                    $sec = $lastComm / 1000;
                    $datt = date("Y-m-d", $sec);
                    $time1 = date("H:i:s", $sec);
                    $setted = $datt . ' ' . $time1;
                }
                if ($lastLoc == '' || $lastLoc == null) {
                    $setlastLoc = '';
                } else {
                    $sec1 = $lastLoc / 1000;
                    $datt1 = date("Y-m-d", $sec1);
                    $time11 = date("H:i:s", $sec1);
                    $setlastLoc = $datt1 . ' ' . $time11;
                }
                date_default_timezone_set('Asia/Kolkata');
                $commList = Arr::add($commList, $vehicle, $setted);
                $locList = Arr::add($locList, $vehicle, $setlastLoc);
                $date = isset($vehicleRefData['date']) ? $vehicleRefData['date'] : '';
                if ($date == '' || $date == ' ') {
                    $date1 = '';
                } else {
                    $date1 = date("d-m-Y", strtotime($date));
                }
                $onDate = isset($vehicleRefData['onboardDate']) ? $vehicleRefData['onboardDate'] : $date1;
                $onDate = trim($onDate);

                if ($onDate == "null" || $onDate == "") {
                    $onboardDate = $date1;
                } else {
                    $onboardDate = $onDate;
                }
                $onboardDateList = Arr::add($onboardDateList, $vehicle, $onboardDate);
            } else {
                unset($vehicleList[$key]);
            }
        }
        $user = null;

        $user1 = new VdmDealersController;
        $user = $user1->checkuser();
        $indexData = [
            'vehicleList' => $vehicleList,
            'servernameList' => $servernameList,
            'deviceList' => $deviceList,
            'shortNameList' => $shortNameList,
            'commList' => $commList,
            'portNoList' => $portNoList,
            'mobileNoList' => $mobileNoList,
            'user' => $user,
            'orgIdList' => $orgIdList,
            'deviceModelList' => $deviceModelList,
            'expiredList' => $expiredList,
            'statusList' => $statusList,
            'onboardDateList' => $onboardDateList,
            'licenceList' => $licenceList,
            'prepaid' => $prepaid,
            'licenceIdList' => $licenceIdList,
            'LicexpiredPeriodList' => $LicexpiredPeriodList,
            'versionList' => $versionList,
            'timezoneList' => $timezoneList,
            'apnList' => $apnList,
            'locList' => $locList,
            'licTypeList' => $licTypeList
        ];
        return view('vdm.vehicles.oldindex', $indexData);
    }

    public function index()
    {
        $username = auth()->id();
        session()->forget('page');
        session()->put('vCol', 1);
        if ($username == "smpadmin" && session()->get('cur') == 'admin') return view('vdm.vehicles.index');

        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        // $fcode ="SMP";



        if (session()->get('cur') == 'dealer') {
            $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $vehicleListId = 'S_Vehicles_Admin_' . $fcode;
        } else {
            $vehicleListId = 'S_Vehicles_' . $fcode;
        }
        $vehicleList = $redis->smembers($vehicleListId);
        sort($vehicleList, SORT_NATURAL | SORT_FLAG_CASE);
        if (request()->server('HTTP_HOST') === "localhost") {
            $vehicleList = array_slice($vehicleList, 0, 10);
        }

        // $vehicleList = array_merge($vehicleList,['ASS-TN19U0299']); 
        $onboardVehicleList  = $redis->smembers("S_Pre_Onboard_Dealer_" . $username . "_" . $fcode);
        $licenceIdList = $redis->hmget("H_Vehicle_LicenceId_Map_$fcode", $vehicleList);
        $licenceList = $redis->hmget("H_LicenceExpiry_$fcode", $licenceIdList);
        $deviceIdList = $redis->hmget("H_Vehicle_Device_Map_$fcode", $vehicleList);
        $versionList = $redis->hmget("H_Version_$fcode", $vehicleList);
        $deviceDetailList  = $redis->hmget("H_DeviceDetail_$fcode", $vehicleList);
        $servernameList = $redis->hmget("H_VehicleMapIpaddress_$fcode", $vehicleList);
        $proDataList =  $redis->hmget("H_ProData_$fcode", $vehicleList);
        $refDataList = $redis->hmget("H_RefData_$fcode", $vehicleList);

        $indexData = [
            'vehicleList' => $vehicleList,
            'deviceIdList' => $deviceIdList,
            'licenceIdList' => $licenceIdList,
            'licenceList' => $licenceList,
            'versionList' => $versionList,
            'servernameList' => $servernameList,
            'proDataList' => $proDataList,
            'refDataList' => $refDataList,
            'deviceDetailList' => $deviceDetailList,
            'onboardVehicleList' => $onboardVehicleList
        ];
        // dd($indexData);
        return view("vdm.vehicles.indexNew", $indexData);
    }
    public function vehicleListAjax()
    {
        $data = request()->all();
        $search = $data['search']['value'];
        $start = $data['start'];
        $draw = $data['draw'];
        $row = $data['start'];
        $rowperpage = $data['length']; // Rows display per page
        // $rowperpage = 100;
        $columnIndex = $data['order'][0]['column']; // Column index
        $columnName = $data['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $data['order'][0]['dir']; // asc or desc

        $username = auth()->id();
        $redis = Redis::connection();

        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $franDetails_json = $redis->hget('H_Franchise', $fcode);
        $franchiseDetails = json_decode($franDetails_json, true);
        $prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';


        if (session()->get('cur') == 'dealer') {
            $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
            $vehicleNameMob = 'H_VehicleName_Mobile_Dealer_' . $username . '_Org_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $vehicleListId = 'S_Vehicles_Admin_' . $fcode;
            $vehicleNameMob = 'H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode;
        } else {
            $vehicleListId = 'S_Vehicles_' . $fcode;
            $vehicleNameMob = 'H_VehicleName_Mobile_Org_' . $fcode;
        }

        // $text_word1 = $search;
        $text_trim = str_replace(' ', '', $search);
        $text_word = strtoupper(trim($text_trim));
        $vehicleList = [];
        if (trim($text_word) == "") {
            $vehicleList = $redis->smembers($vehicleListId);
            sort($vehicleList, SORT_NATURAL | SORT_FLAG_CASE);
        }


        $cou = $redis->hlen($vehicleNameMob);
        // redis connection
        $slave = $redis->hget('H_Redis_Slave', 'Slave1');
        $redis1 = new Client(array(
            'host'     => $slave,
            'password' => 'ahanram@vamosystems.in',
            'database' => 0,
        ));
        $franRefData = $redis->hget('H_Franchise', $fcode);
        $franjsonData = json_decode($franRefData, true);
        if (session()->get('cur') == 'admin') {
            $check = $redis->hget('H_RefData_' . $fcode, $text_word);
            if ($check !== null) {
                $orgLl = array();
                $orgLl = Arr::add($orgLl, $text_word, $text_word);
                $vehicleL = $orgLl;
            } else {
                $orgLi = $redis1->HScan($vehicleNameMob, 0,  'count', $cou, 'match', '*' . $text_word . '*');
                $vehicleL = $orgLi[1];
            }
        } else {
            $orgLi = $redis1->HScan($vehicleNameMob, 0,  'count', $cou, 'match', '*' . $text_word . '*');
            $vehicleL = $orgLi[1];
        }
        $redis1->quit();
        // 
        $vehicleL = array_unique($vehicleL);
        $DATA = array();
        $index = $row + 1;

        if ($search == '') $vehicleL = array_slice($vehicleList, $row, $rowperpage);
        $vehicleListCount = count($vehicleL);
        if ($search !== '') {

            $statusList = ["Z", "P", "M", "S", "U", "N"];
            $statusSearch = ["Z", "PARKING", "MOVING", "IDLE", "NODATA", "NEWDEVICE"];
            $statusIndex = array_search($text_word, $statusSearch);
            $statusCode = $statusList[$statusIndex];
            if ($statusIndex && in_array($statusCode, $statusList, true)) {
                $filteredList = array();

                $i = 0;
                if (session()->has('indexList')) {
                    $i = session()->get('indexList');
                }
                while (count($filteredList) < (int)$rowperpage) {
                    if (count($vehicleList) <= $i) $i = 0;
                    $vehicle  = $vehicleList[$i];
                    $statusVehicle = $redis->hget('H_ProData_' . $fcode, $vehicle);
                    $statusSeperate = explode(',', $statusVehicle);
                    $statusSeperate1 = isset($statusSeperate[7]) ? $statusSeperate[7] : 'N';
                    if (strpos($statusSeperate1, $statusCode) !== false) {
                        $filteredList[] = $vehicle;
                    }
                    $i++;
                }
                session()->put('indexList', $i);
                $vehicleListCount = count(array_unique($vehicleList));
                $slice = $vehicleListCount - $start;
                $vehicleL = $slice <= 10 ? array_slice($filteredList, 0, $slice) : $filteredList;
            }
        }

        if ($columnSortOrder == 'asc') {
            sort($vehicleL);
        } else {
            rsort($vehicleL);
        }
        foreach ($vehicleL as $key => $vehicle) {

            $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);

            if (is_null($vehicleRefData)) {
                continue;
            }
            $vehicleRefData = json_decode($vehicleRefData, true);

            //$deviceId = $vehicleRefData['deviceId'];
            $deviceId   =  $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicle);

            if ((session()->get('cur') == 'dealer' &&  $redis->sismember('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId) == 0) || session()->get('cur') == 'admin') {

                $LicenceexpiredPeriod = '';
                $LicenceId = '';
                if ($prepaid == 'yes') {
                    $LicenceId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vehicle);
                    $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
                    $LicenceRefData = json_decode($LicenceRef, true);
                    $licence = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
                    $LicenceexpiredPeriod = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
                } else {
                    $licence = isset($vehicleRefData['licenceissuedDate']) ? $vehicleRefData['licenceissuedDate'] : '';
                }
                $expiredPeriod = isset($vehicleRefData['vehicleExpiry']) ? $vehicleRefData['vehicleExpiry'] : '-';
                $nullval = strlen($expiredPeriod);
                if ($nullval == 0 || $expiredPeriod == "null") {
                    $expiredPeriod = '-';
                }
                $shortName = isset($vehicleRefData['shortName']) ? $vehicleRefData['shortName'] : 'nill';
                $portNo = isset($vehicleRefData['portNo']) ? $vehicleRefData['portNo'] : 9964;
                $mobileNo = isset($vehicleRefData['gpsSimNo']) ? $vehicleRefData['gpsSimNo'] : 99999;
                $orgId = isset($vehicleRefData['orgId']) ? $vehicleRefData['orgId'] : 'Default';
                $deviceModel = isset($vehicleRefData['deviceModel']) ? $vehicleRefData['deviceModel'] : 'nill';
                $countryTimezone = isset($vehicleRefData['country']) ? $vehicleRefData['country'] : '';
                if ($countryTimezone == '' || $countryTimezone == 'MIT' || $countryTimezone == 'no' || $countryTimezone == "null") {
                    $countryTimezone = isset($franjsonData['timeZone']) ? $franjsonData['timeZone'] : 'Asia/Kolkata';
                }
                // device version
                $versionofVehicle = $redis->hget('H_Version_' . $fcode, $vehicle);
                $versions = isset($versionofVehicle) ? $versionofVehicle : '-';
                //timezone and apn
                $timezoneData = $redis->hget('H_DeviceDetail_' . $fcode, $vehicle);
                $timezoneRefData = json_decode($timezoneData, true);
                $timezone = isset($timezoneRefData['timezone']) ? $timezoneRefData['timezone'] : '-';
                $apn = isset($timezoneRefData['apn']) ? $timezoneRefData['apn'] : '-';
                //servername
                $serverN = $redis->hget('H_VehicleMapIpaddress_' . $fcode, $vehicle);
                $servername = isset($serverN) ? $serverN : '-';
                $statusVehicle = $redis->hget('H_ProData_' . $fcode, $vehicle);
                $statusSeperate = explode(',', $statusVehicle);
                $statusSeperate1 = isset($statusSeperate[7]) ? $statusSeperate[7] : 'N';
                $lastComm = isset($statusSeperate[36]) ? $statusSeperate[36] : '';
                $lastLoc = isset($statusSeperate[4]) ? $statusSeperate[4] : '';
                try {
                    date_default_timezone_set($countryTimezone);
                } catch (\Exception $e) {
                    date_default_timezone_set('Asia/Kolkata');
                }
                if ($lastComm == '' || $lastComm == null) {
                    $setted = '';
                } else {
                    $sec = $lastComm / 1000;
                    $datt = date("Y-m-d", $sec);
                    $time1 = date("H:i:s", $sec);
                    $setted = $datt . ' ' . $time1;
                }
                if ($lastLoc == '' || $lastLoc == null) {
                    $setlastLoc = '';
                } else {
                    $sec1 = $lastLoc / 1000;
                    $datt1 = date("Y-m-d", $sec1);
                    $time11 = date("H:i:s", $sec1);
                    $setlastLoc = $datt1 . ' ' . $time11;
                }
                date_default_timezone_set('Asia/Kolkata');
                $date = isset($vehicleRefData['date']) ? $vehicleRefData['date'] : '';
                if ($date == '' || $date == ' ') {
                    $date1 = '';
                } else {
                    $date1 = date("d-m-Y", strtotime($date));
                }
                $onDate = isset($vehicleRefData['onboardDate']) ? $vehicleRefData['onboardDate'] : $date1;
                $onDate = trim($onDate);

                if ($onDate == "null" || $onDate == "") {
                    $onboardDate = $date1;
                } else {
                    $onboardDate = $onDate;
                }
            } else {
                unset($vehicleList[$key]);
                continue;
            }

            $status = null;
            $statusSearch = ["P", "M", "S", "U", "N"];
            $statusList = ["Parking", "Moving", "Idle", "No Data", "New Device"];
            $statusColor = ["#8e8e7b", "#00b374", "#ff6500", "#fe068d", "#0a85ff"];
            $statusIndex = array_search($statusSeperate1, $statusSearch, true);
            $status = "<span style='color:" . $statusColor[$statusIndex] . "'>" . $statusList[$statusIndex] . "</span>";


            $actions = "<div class='dropdown dropleft'><button class='btn btn-info text-white btn-sm dropdown-toggle' type='button'  id='dropdownMenuButton$index' data-toggle='dropdown'  aria-haspopup='true' aria-expanded='false'>".__('messages.Action')."</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton$index'>";
            $actions .= "<a class='btn btn-sm text-white btn-success dropdown-item mb-1' href='vdmVehicles/migration/$vehicle'>".__('messages.Migration')."</a>";
            $actions .= "<a class='btn btn-sm text-white btn-primary dropdown-item mb-1' href='vdmVehicles/edit/$vehicle'>".__('messages.Edit')."</a> ";
            $actions .= "<a class='btn btn-sm text-white btn-warning dropdown-item mb-1' href='" . route('calibrate', ['vid' => $vehicle, 'count' => '1']) . "'>".__("messages.Calibrate")."</a>";
            $actions .= "<a class='btn btn-sm text-white btn-info dropdown-item mb-1' href='vdmVehicles/rename/$vehicle'>".__("messages.Rename")."</a> ";
            $actions .= "<a class='btn btn-sm text-white btn-info dropdown-item mb-1' onClick='setVehicle(`$vehicle`)'>".__("messages.Reprocess")."</a>";
            //$actions .= '<a class="btn btn-sm text-white btn-info dropdown-item mb-1" href="vdmVehicles/stops/' . $vehicle . '/normal">Show Stops</a> ';
            //$actions .= '<a class="btn btn-sm text-white btn-danger dropdown-item " href="vdmVehicles/removeStop/' . $vehicle . '/normal">Remove Stops</a> ';
            $actions .= '</div></div>';

            $DATA[] = array(
                "ID" => $index,
                "AssetID" => $vehicle,
                "Licence Expire Date" => $LicenceexpiredPeriod,
                "Licence ID" => $LicenceId,
                "Vehicle Name" => $shortName,
                "Org Name" => $orgId,
                "Device ID" => $deviceId,
                "Server Name" => $servername,
                "Last Loc Time" => $setlastLoc,
                "Last Comm Time" => $setted,
                "Status" => $status,
                "Device Model" => $deviceModel,
                "Version" => $versions,
                "GPS Sim No" => $mobileNo,
                "TimeZone" => $timezone,
                "APN" => $apn,
                "Licence Issued Date" => $licence,
                "Onboard Date" => $onboardDate,
                "Vehicle Expire Date" => $expiredPeriod,
                "Action" => $actions
            );
            $index++;
        }

        ## Response
        $count = $search == '' ? count($vehicleList) : $vehicleListCount;
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" =>  count($vehicleList),
            "iTotalDisplayRecords" => $count,
            "aaData" => $DATA
        );

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */



    public function create($id)
    {

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        //get the Org list
        $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);

        if (session()->get('cur') == 'dealer') {
            $tmpOrgList = $redis->smembers('S_Organisations_Dealer_' . $username . '_' . $fcode);
        } else if (session()->get('cur') == 'admin') {
            $tmpOrgList = $redis->smembers('S_Organisations_Admin_' . $fcode);
        }

        $orgList = null;
        $orgList = Arr::add($orgList, 'Default', 'Default');
        foreach ($tmpOrgList as $org) {
            $orgList = Arr::add($orgList, $org, $org);
        }


        if (session()->get('cur') == 'dealer') {
            $key = 'H_Pre_Onboard_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $key = 'H_Pre_Onboard_Admin_' . $fcode;
        }
        $details = $redis->hget($key, $id);
        $valueData = json_decode($details, true);
        $deviceId = $valueData['deviceid'];
        $deviceModel = $valueData['deviceidtype'];
        $deviceidCheck = $redis->sismember('S_Device_' . $fcode, $deviceId);
        if ($deviceidCheck == 1) {
            //session()->flash ( 'message', 'DeviceId' . $deviceidCheck . 'already exist. Please choose another one' );
            $value = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $deviceId);
            $vehicleId = 'GPSVTS_' . substr($deviceId, -6);

            return redirect()->to('vdmVehicles/' . $value . '/edit1');
        }

        $createnewData = [
            'orgList' => $orgList,
            'deviceId' => $deviceId,
            'deviceModel' => $deviceModel,
        ];

        return view('vdm.vehicles.createnew', $createnewData);
    }


    /**
     * Store a newly created resource in storage.
     * TODO validations should be improved to prevent any attacks
     *
     * @return Response
     */

    /* validator()->extend('alpha_spaces', function($attribute, $value)
        {
        return preg_match('/^[\pL\s]+$/u', $value);
        });
    */

    //if change done in store should done in update1 also
    public function store()
    {


        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
        $rules = array(
            'deviceId' => 'required|alpha_dash',
            'vehicleId' => 'required|alpha_dash',
            'shortName' => 'required|alpha_dash',
            'regNo' => 'required',
            'vehicleType' => 'required',
            'oprName' => 'required',
            'deviceModel' => 'required',
            'odoDistance' => 'required',
            'gpsSimNo' => 'required'

        );
        $validator = validator()->make(request()->all(), $rules);
        $redis = Redis::connection();
        $vehicleId = request()->get('vehicleId');
        $deviceId = request()->get('deviceId');
        $vehicleIdCheck = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
        $deviceidCheck = $redis->sismember('S_Device_' . $fcode, $deviceId);

        if ($vehicleIdCheck == 1) {
            session()->flash('message', 'VehicleId' . $vehicleId . 'already exist. Please choose another one');
            return redirect()->to('vdmVehicles/create/' . $deviceId);
        }
        if ($deviceidCheck == 1) {
            session()->flash('message', 'DeviceId' . $deviceidCheck . 'already exist. Please choose another one');
            return redirect()->to('vdmVehicles/create/' . $deviceId);
        }

        if ($validator->fails()) {
            return redirect()->to('vdmVehicles/create/' . $deviceId)->withErrors($validator);
        } else {
            // store

            $deviceId = request()->get('deviceId');
            $vehicleId = request()->get('vehicleId');
            $shortName = request()->get('shortName');
            $regNo = request()->get('regNo');
            $vehicleMake = request()->get('vehicleMake');
            $vehicleType = request()->get('vehicleType');
            $oprName = request()->get('oprName');
            $mobileNo = request()->get('mobileNo');
            $vehicleExpiry = request()->get('vehicleExpiry');
            $onboardDate = request()->get('onboardDate');
            $overSpeedLimit = request()->get('overSpeedLimit');
            $deviceModel = request()->get('deviceModel');
            $odoDistance = request()->get('odoDistance');
            $driverName = request()->get('driverName');
            $driverMobile = request()->get('driverMobile');
            $gpsSimNo = request()->get('gpsSimNo');
            $email = request()->get('email');
            $sendGeoFenceSMS = request()->get('sendGeoFenceSMS');
            $morningTripStartTime = request()->get('morningTripStartTime');
            $eveningTripStartTime = request()->get('eveningTripStartTime');
            $fuel = request()->get('fuel');
            $orgId = request()->get('orgId');
            $altShortName = request()->get('altShortName');
            $parkingAlert = request()->get('parkingAlert');
            $isRfid = request()->get('isRfid');
            $rfidType = request()->get('rfidType');
            $safetyParking = request()->get('safetyParking');
            $v = idate("d");
            // $paymentType=request()->get ( 'paymentType' );
            $paymentType = 'yearly';
            if ($paymentType == 'halfyearly') {
                $paymentmonth = 6;
            } elseif ($paymentType == 'yearly') {
                $paymentmonth = 11;
            }
            if ($v > 15) {
                $paymentmonth = $paymentmonth + 1;
            }
            for ($i = 1; $i <= $paymentmonth; $i++) {

                $new_date = date('F Y', strtotime("+$i month"));
                $new_date2 = date('FY', strtotime("$i month"));
            }
            $new_date1 = date('F d Y', strtotime("+0 month"));
            if (session()->get('cur') == 'dealer') {
                $ownership = $username;
            } else if (session()->get('cur') == 'admin') {
                $ownership = 'OWN';
            }


            $refDataArr = array(
                'deviceId' => $deviceId,
                'shortName' => $shortName,
                'deviceModel' => $deviceModel,
                'regNo' => $regNo,
                'vehicleMake' => $vehicleMake,
                'vehicleType' => $vehicleType,
                'oprName' => $oprName,
                'mobileNo' => $mobileNo,
                'vehicleExpiry' => $vehicleExpiry,
                'onboardDate' => $onboardDate,
                'overSpeedLimit' => $overSpeedLimit,
                'odoDistance' => $odoDistance,
                'driverName' => $driverName,
                'driverMobile' => $driverMobile,
                'gpsSimNo' => $gpsSimNo,
                'email' => $email,
                'sendGeoFenceSMS' => $sendGeoFenceSMS,
                'morningTripStartTime' => $morningTripStartTime,
                'eveningTripStartTime' => $eveningTripStartTime,
                'orgId' => $orgId,
                'parkingAlert' => $parkingAlert,
                'altShortName' => $altShortName,
                'date' => $new_date1,
                'paymentType' => $paymentType,
                'expiredPeriod' => $new_date,
                'fuel' => $fuel,
                'isRfid' => $isRfid,
                'rfidType' => $rfidType,
                'OWN' => $ownership,
                'safetyParking' => $safetyParking,

            );

            $refDataJson = json_encode($refDataArr);

            // H_RefData
            $expireData = $redis->hget('H_Expire_' . $fcode, $new_date2);

            $redis->hset('H_RefData_' . $fcode, $vehicleId, $refDataJson);

            $cpyDeviceSet = 'S_Device_' . $fcode;

            $redis->sadd($cpyDeviceSet, $deviceId);

            $redis->hmset($vehicleDeviceMapId, $vehicleId, $deviceId, $deviceId, $vehicleId);

            //this is for security check                                    
            $redis->sadd('S_Vehicles_' . $fcode, $vehicleId);

            $redis->hset('H_Device_Cpy_Map', $deviceId, $fcode);
            $redis->sadd('S_Vehicles_' . $orgId . '_' . $fcode, $vehicleId);
            if ($expireData == null) {
                $redis->hset('H_Expire_' . $fcode, $new_date2, $vehicleId);
            } else {
                $redis->hset('H_Expire_' . $fcode, $new_date2, $expireData . ',' . $vehicleId);
            }

            $time = microtime(true);
            /*latitude,longitude,speed,alert,date,distanceCovered,direction,position,status,odoDistance,msgType,insideGeoFence
                13.104870,80.303138,0,N,$time,0.0,N,P,ON,$odoDistance,S,N
                13.04523,80.200222,0,N,0,0.0,null,null,null,0.0,null,N vehicleId=Prasanna_Amaze
            */
            $redis->sadd('S_Organisation_Route_' . $orgId . '_' . $fcode, $shortName);
            $time = round($time * 1000);


            if (session()->get('cur') == 'dealer') {
                $redis->sadd('S_Vehicles_Dealer_' . $username . '_' . $fcode, $vehicleId);
            } else if (session()->get('cur') == 'admin') {
                $redis->sadd('S_Vehicles_Admin_' . $fcode, $vehicleId);
            }
            $tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,' . $odoDistance . ',S,N';
            $LatLong = $redis->hget('H_Franchise_LatLong', $fcode);
            if ($LatLong != null) {
                $data_arr = explode(":", $LatLong);
                $latitude = $data_arr[0];
                $longitude = $data_arr[1];
                $tmpPositon =  $latitude . ',' . $longitude . ',0,N,' . $time . ',0.0,N,N,ON,' . $odoDistance . ',S,N';
                $redis->sadd('S_NoDataVehicle', $vehicleId . ',' . $fcode);
                $redis->hset('H_NoDataVehicleProcess_Time', $vehicleId . ',' . $fcode, $time);
            }
            $redis->hset('H_ProData_' . $fcode, $vehicleId, $tmpPositon);
            // redirect
            session()->flash('message', 'Successfully created ' . $vehicleId . '!');
            return redirect()->to('Business');
        }
    }



    /**
     * Display the specified resource.
     *
     * @param int $id            
     * @return Response
     */
    public function show($id)
    {

        return back();
        $username = auth()->id();

        $redis = Redis::connection();
        $deviceId = $id;
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
        $deviceRefData = $redis->hget('H_RefData_' . $fcode, $deviceId);
        $refDataArr = json_decode($deviceRefData, true);
        $deviceRefData = null;
        if (is_array($refDataArr)) {
            foreach ($refDataArr as $key => $value) {

                $deviceRefData = $deviceRefData . $key . ' : ' . $value . ',<br/>';
            }
        } else {
            echo 'JSON decode failed';
            var_dump($refDataArr);
        }
        $vehicleId = $redis->hget($vehicleDeviceMapId, $deviceId);
        if ($vehicleId == null) {
            return redirect()->to('vdmVehicles/dealerSearch');
        }

        $showData = [
            'deviceId' => $deviceId,
            'deviceRefData' => $deviceRefData,
            'vehicleId' => $vehicleId
        ];
        return view('vdm.vehicles.show', $showData);
    }

    public function edit($id)
    {
        try {

            $username = auth()->id();

            $redis = Redis::connection();
            $vehicleId = $id;

            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
            $deviceId = $redis->hget($vehicleDeviceMapId, $vehicleId);
            $refDataJson = $redis->hget('H_RefData_' . $fcode, $vehicleId);
            $refData = json_decode($refDataJson, true);

            $fuelRefDataJson = $redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId);
            $fuelRefData = json_decode($fuelRefDataJson, true);
            $date = isset($refData['date']) ? $refData['date'] : '';
            $date = trim($date);
            $licencetype = isset($refData['Licence']) ? $refData['Licence'] : 'Advance';
            $date1 = ($date !== '') ? date("d-m-Y", strtotime($date)) : '';

            $onDate = isset($refData['onboardDate']) ? $refData['onboardDate'] : $date1;
            $onDate = trim($onDate);
            $onboardDate = ($onDate == "null" || $onDate == "") ? str_replace(' ', '', $date1) : $onDate;

            $VolIg = isset($refData['VolIg']) ? $refData['VolIg'] : '';
            $vol = ($VolIg == 'no' || $VolIg == 'NULL' || $VolIg == '') ? 'Ignition' : $VolIg;

            $batteryvolt = isset($refData['batteryvolt']) ? $refData['batteryvolt'] : '';
            $batteryvolt = $batteryvolt == '0' ? "" : $batteryvolt;

            $ac1 = isset($refData['ac']) ? $refData['ac'] : '';
            $ac = $ac1 == 'no' ? "digital1" : $ac1;

            $acVolt1 = isset($refData['acVolt']) ? $refData['acVolt'] : '';
            $acVolt = $acVolt1 == '0' ? '' : $acVolt1;

            $enbVal = $redis->get('EnableLog:' . $vehicleId . ':' . $fcode);
            $enable = $enbVal != null ? 'Enable' : 'Disable';

            $refDataJson1 = $refData;
            $refData = null;
            $calibrateType = 'manual';
            if (!isset($refDataJson1['fuelMode'])) {
                $vehicleProData = $redis->hget('H_ProData_' . $fcode, $vehicleId);
                $vehicleProDataList = explode(',', $vehicleProData);
                $direct =  $vehicleProDataList[17] ?? '';
                $fuelType = $fuelRefData['fuelType'] ?? 'serial1';
                $tankshape = $fuelRefData['tankshape'] ?? 'Abnormal';
                if ($direct == '-1.0' && $fuelType !== 'analog') {
                    $calibrateType = "direct";
                } else if ($tankshape !== "Abnormal" && $tankshape !== "") {
                    $calibrateType = 'auto';
                } else {
                    $calibrateType = 'manual';
                }
            }
            $ms = isset($refDataJson1['secondaryEngineHours']) ? $refDataJson1['secondaryEngineHours'] : 0;
            $secondaryEngineHours = 0;
            if ($ms !== 0) {
                $hours = floor(($ms) / (60 * 60 * 1000));
                $hoursms = $ms % (60 * 60 * 1000);
                $minutes = floor(($hoursms) / (60 * 1000));
                $minutesms = $ms % (60 * 1000);
                $sec = floor(($minutesms) / (1000));
                $secondaryEngineHours =  $hours . ":" . $minutes . ":" . $sec;
            }
            $refData =   array(
                'deviceId' => isset($refDataJson1['deviceId']) ? $refDataJson1['deviceId'] : '',
                'shortName' => isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : '',
                'deviceModel' => isset($refDataJson1['deviceModel']) ? $refDataJson1['deviceModel'] : 'GT06N',
                'regNo' => isset($refDataJson1['regNo']) ? $refDataJson1['regNo'] : 'XXXXX',
                'vehicleMake' => isset($refDataJson1['vehicleMake']) ? $refDataJson1['vehicleMake'] : ' ',
                'vehicleType' =>  isset($refDataJson1['vehicleType']) ? $refDataJson1['vehicleType'] : 'Bus',
                'oprName' => isset($refDataJson1['oprName']) ? $refDataJson1['oprName'] : 'airtel',
                'mobileNo' => isset($refDataJson1['mobileNo']) ? $refDataJson1['mobileNo'] : '0123456789',
                'overSpeedLimit' => isset($refDataJson1['overSpeedLimit']) ? $refDataJson1['overSpeedLimit'] : '60',
                'odoDistance' => isset($refDataJson1['odoDistance']) ? $refDataJson1['odoDistance'] : '0',
                'driverName' => isset($refDataJson1['driverName']) ? $refDataJson1['driverName'] : 'XXX',
                'gpsSimNo' => isset($refDataJson1['gpsSimNo']) ? $refDataJson1['gpsSimNo'] : '0123456789',
                'email' => isset($refDataJson1['email']) ? $refDataJson1['email'] : '',
                'orgId' => str_replace('.', '$', (isset($refDataJson1['orgId']) ? $refDataJson1['orgId'] : '')),
                'altShortName' => isset($refDataJson1['altShortName']) ? $refDataJson1['altShortName'] : '',
                'fuel' => isset($refDataJson1['fuel']) ? $refDataJson1['fuel'] : 'no',
                'fuelType' => isset($fuelRefData['fuelType']) ? $fuelRefData['fuelType'] : '',
                'isRfid' => isset($refDataJson1['isRfid']) ? $refDataJson1['isRfid'] : 'no',
                'rfidType' => isset($refDataJson1['rfidType']) ? $refDataJson1['rfidType'] : 'no',
                'Licence' => isset($refDataJson1['Licence']) ? $refDataJson1['Licence'] : '',
                'OWN' => isset($refDataJson1['OWN']) ? $refDataJson1['OWN'] : 'OWN',
                'Payment_Mode' => isset($refDataJson1['Payment_Mode']) ? $refDataJson1['Payment_Mode'] : '',
                'descriptionStatus' => isset($refDataJson1['descriptionStatus']) ? $refDataJson1['descriptionStatus'] : '',
                'ipAddress' => isset($refDataJson1['ipAddress']) ? $refDataJson1['ipAddress'] : '',
                'portNo' => isset($refDataJson1['portNo']) ? $refDataJson1['portNo'] : '0',
                'analog1' => isset($refDataJson1['analog1']) ? $refDataJson1['analog1'] : '',
                'analog2' => isset($refDataJson1['analog2']) ? $refDataJson1['analog2'] : '',
                'digital1' => isset($refDataJson1['digital1']) ? $refDataJson1['digital1'] : '',
                'digital2' => isset($refDataJson1['digital2']) ? $refDataJson1['digital2'] : '',
                'serial1' => isset($refDataJson1['serial1']) ? $refDataJson1['serial1'] : '',
                'serial2' => isset($refDataJson1['serial2']) ? $refDataJson1['serial2'] : '',
                'digitalout' => isset($refDataJson1['digitalout']) ? $refDataJson1['digitalout'] : '',
                'mintemp' => isset($refDataJson1['mintemp']) ? $refDataJson1['mintemp'] : '',
                'maxtemp' => isset($refDataJson1['maxtemp']) ? $refDataJson1['maxtemp'] : '',
                'routeName' => isset($refDataJson1['routeName']) ? $refDataJson1['routeName'] : '',
                'sendGeoFenceSMS' => isset($refDataJson1['sendGeoFenceSMS']) ? $refDataJson1['sendGeoFenceSMS'] : 'no',
                'morningTripStartTime' => isset($refDataJson1['morningTripStartTime']) ? $refDataJson1['morningTripStartTime'] : '',
                'eveningTripStartTime' => isset($refDataJson1['eveningTripStartTime']) ? $refDataJson1['eveningTripStartTime'] : '',
                'parkingAlert' => isset($refDataJson1['parkingAlert']) ? $refDataJson1['parkingAlert'] : 'no',
                'paymentType' => isset($refDataJson1['paymentType']) ? $refDataJson1['paymentType'] : ' ',
                'expiredPeriod' => isset($refDataJson1['expiredPeriod']) ? $refDataJson1['expiredPeriod'] : ' ',
                'vehicleExpiry' => isset($refDataJson1['vehicleExpiry']) ? $refDataJson1['vehicleExpiry'] : '',
                'onboardDate' => isset($refDataJson1['onboardDate']) ? $refDataJson1['onboardDate'] : '',
                'safetyParking' => isset($refDataJson1['safetyParking']) ? $refDataJson1['safetyParking'] : 'no',
                'tankSize' => isset($refDataJson1['tankSize']) ? $refDataJson1['tankSize'] : '0',
                'licenceissuedDate' => isset($refDataJson1['licenceissuedDate']) ? $refDataJson1['licenceissuedDate'] : '',
                'VolIg' => isset($refDataJson1['VolIg']) ? $refDataJson1['VolIg'] : '',
                'batteryvolt' => isset($refDataJson1['batteryvolt']) ? $refDataJson1['batteryvolt'] : '',
                'ac' => isset($refDataJson1['ac']) ? $refDataJson1['ac'] : '',
                'acVolt' => isset($refDataJson1['acVolt']) ? $refDataJson1['acVolt'] : '',
                'DistManipulationPer' => isset($refDataJson1['DistManipulationPer']) ? $refDataJson1['DistManipulationPer'] : '',
                'assetTracker' => isset($refDataJson1['assetTracker']) ? $refDataJson1['assetTracker'] : 'no',
                'communicatingPortNo' => isset($refDataJson1['communicatingPortNo']) ? $refDataJson1['communicatingPortNo'] : '',
                'onboardDate1'  => $onboardDate,
                'driverName'    => isset($refDataJson1['driverName']) ? $refDataJson1['driverName'] : '',
                'driverMobile' => isset($refDataJson1['driverMobile']) ? $refDataJson1['driverMobile'] : '',
                'date' => isset($refDataJson1['date']) ? $refDataJson1['date'] : '',
                'vehicleMode' => isset($refDataJson1['vehicleMode']) ? $refDataJson1['vehicleMode'] : '',
                'countryTimezone' => isset($refDataJson1['country']) ? $refDataJson1['country'] : '',
                'defaultMileage' => isset($refDataJson1['defaultMileage']) ? $refDataJson1['defaultMileage'] : 'no',
                'installationDate' => isset($refDataJson1['installationDate']) ? $refDataJson1['installationDate'] : '',
                'ignitionSpeed' => isset($refDataJson1['ignitionSpeed']) ? $refDataJson1['ignitionSpeed'] : '',
                'sensorCount' => isset($refDataJson1['sensorCount']) ? $refDataJson1['sensorCount'] : '1',
                'noOfTank' => isset($refDataJson1['noOfTank']) ? $refDataJson1['noOfTank'] : '1',
                'gpsSimICCID' => isset($refDataJson1['gpsSimICCID']) ? $refDataJson1['gpsSimICCID'] : '',
                'madeIn' => isset($refDataJson1['madeIn']) ? $refDataJson1['madeIn'] : 'India',
                'manufacturingDate' => isset($refDataJson1['manufacturingDate']) ? $refDataJson1['manufacturingDate'] : '',
                'chassisNumber' => isset($refDataJson1['chassisNumber']) ? $refDataJson1['chassisNumber'] : '',
                'expectedMileage' => isset($refDataJson1['expectedMileage']) ? $refDataJson1['expectedMileage'] : '0',
                'tankInterlinked' => isset($refDataJson1['tankInterlinked']) ? $refDataJson1['tankInterlinked'] : 'no',
                'kalmanFilter' => isset($refDataJson1['kalmanFilter']) ? $refDataJson1['kalmanFilter'] : 'no',
                'fuelBatteryVolt' => isset($refDataJson1['fuelBatteryVolt']) ? $refDataJson1['fuelBatteryVolt'] : 'no',
                'vehicleModel' => isset($refDataJson1['vehicleModel']) ? $refDataJson1['vehicleModel'] : 'Mazda',
                'rigMode' => isset($refDataJson1['rigMode']) ? $refDataJson1['rigMode'] : 'Disable',
                'fuelMode' => isset($refDataJson1['fuelMode']) ? $refDataJson1['fuelMode'] : $calibrateType,
                'secondaryEngineHours' => $secondaryEngineHours,
                'isAc' => isset($refDataJson1['isAc']) ? $refDataJson1['isAc'] : 'no',
            );
            $orgId = isset($refDataJson1['orgId']) ? $refDataJson1['orgId'] : 'NotAvailabe';

            // $routeList  = $redis->smembers('S_RouteList_' . $orgId . '_' . $fcode);
            // $routeLIST = null;
            // $routeLIST = Arr::add($routeLIST, 'nil', 'nill');
            // $routeList = str_replace('.', '$', $routeList);
            // foreach ($routeList as $route) {
            //     $routeLIST = Arr::add($routeLIST, $route, $route);
            // }
            // $routeLIST = str_replace('$', '.', $routeLIST);

            $refData = Arr::add($refData, 'orgId', $orgId);
            $parkingAlert = 0;
            $refData = Arr::add($refData, 'parkingAlert', $parkingAlert);
            if (session()->get('cur') == 'dealer') {
                $tmpOrgList = $redis->smembers('S_Organisations_Dealer_' . $username . '_' . $fcode);
            } else if (session()->get('cur') == 'admin') {
                $tmpOrgList = $redis->smembers('S_Organisations_Admin_' . $fcode);
            } else {
                $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
            }

            $orgList = null;
            $tmpOrgList = str_replace('.', '$', $tmpOrgList);
            $orgList = Arr::add($orgList, 'DEFAULT', 'DEFAULT');
            foreach ($tmpOrgList as $org) {
                $org1 = strtoupper($org);
                if ($org == $org1) {
                    $orgList = Arr::add($orgList, '' . $org, $org);
                }
            }
            $orgList = str_replace('$', '.', $orgList);
            $Payment_Mode1 = array();
            $Licence1 = array();
            $Payment_Mode = DB::select('select type from Payment_Mode');
            foreach ($Payment_Mode as  $org1) {
                $Payment_Mode1 = Arr::add($Payment_Mode1, $org1->type, $org1->type);
            }
            $Licence = DB::select('select type from Licence');
            foreach ($Licence as  $org) {
                $Licence1 = Arr::add($Licence1, $org->type, $org->type);
            }

            $franDetails_json = $redis->hget('H_Franchise', $fcode);
            $franchiseDetails = json_decode($franDetails_json, true);
            $prepaid = $franchiseDetails['prepaid'] ?? 'no';
            if ($prepaid == 'yes') {
                $protocol = VdmFranchiseController::getProtocal($licencetype);
                $licenceId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleId);
                $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $licenceId);
                $LicenceRefData = json_decode($LicenceRef, true);
                $LicenceExpiryDate = $LicenceRefData['LicenceExpiryDate'] ?? '';
                $split = explode("-", $LicenceExpiryDate);
                $refData['vehicleExpiry'] = join('-', array_reverse($split));
            } else {
                $protocol = BusinessController::getProtocal();
            }
            $enableExpiryexist = $redis->sismember('S_EnableVehicleExpiry', $fcode);
            if ($enableExpiryexist != null) {
                $enableVehicleExpiry = 'yes';
            } else {
                $enableVehicleExpiry = 'no';
            }
            $countryTimezoneList = array('' => '-- Select Timezone --');
            $timeList1 = VdmFranchiseController::timeZoneC();
            $countryTimezoneList = array_merge($countryTimezoneList, $timeList1);

            $vehicleTypeList1 = ['Ambulance' => 'Ambulance', 'Bike' => 'Bike', 'Bull' => 'Bull', 'Bus' => 'Bus', 'Car' => 'Car', 'heavyVehicle' => 'Heavy Vehicle', 'Tanker' => 'Tanker', 'Truck' => 'Truck', 'Van' => 'Van', 'JCB' => 'JCB', 'Generator' => 'Generator'];
            $vehicleTypeList2 = ['' => '-- Select --', 'Bag' => 'Bag', 'Bike' => 'Bike', 'Car' => 'Car', 'PersonalTracker' => 'Personal Tracker', 'PetTracker' => 'Pet Tracker', 'Watch' => 'Watch'];
            $oprNameList = ['airtel' => 'Airtel', 'idea' => 'Idea', 'reliance' => 'Reliance', 'vodafone' => 'Vodafone'];
            $ignitionSpeedList = ['' => '-- Select --', '5' => '5', '10' => '10', '15' => '15', '20' => '20'];
            $VolIgList = ['Voltage' => 'Voltage', 'Ignition' => 'Ignition', 'Voltage_Ignition' => 'Voltage+Ignition'];
            $acList = ['digital1' => 'Digital Input1', 'analog1' => 'Analog Input1'];
            $analogInputList = ['no' => 'No', 'fuel' => 'Fuel', 'load' => 'Load'];
            $digitalInputList = ['no' => 'No', 'ac' => 'Ac', 'hire' => 'Hire', 'door' => 'Door'];
            $serialInputList = ['no' => 'No', 'temperature' => 'Temperature', 'rfid' => 'Rfid', 'camera' => 'Camera', 'passenger' => 'Passenger'];
            $enableDebugList = ['Disable' => 'Disable', 'Enable' => 'Enable'];
            $rfidTypeList = ['type1' => 'Type1', 'type2' => 'Type2', 'type3' => 'Type3 (234 reverse)'];
            $timezoneList = ['TIMEZONE' => 'TIMEZONE', 'INDIA' => 'INDIA', 'CHINA' => 'CHINA', 'GMT' => 'GMT'];
            $fuelTypeList = ['' => 'None', 'analog' => 'Analog', 'serial1' => 'LLS Fuel 1(Serial)', 'serial2' => 'LLS Fuel 2'];
            $vehicleModeList = ['' => 'None', 'MovingVehicle' => 'Moving Vehicle', 'Machinery' => 'Machinery', 'Dispenser' => 'Dispenser', 'DG' => 'Diesel Generator(DG)'];
            $fuelModeList = ['calibrated' => 'Calibrated Fuel', 'direct' => 'Direct Fuel'];
            $vehicleModels = $redis->smembers('S_VehicleModels');
            sort($vehicleModels);
            $vehicleModelList = array_combine($vehicleModels, $vehicleModels);

            $teleComProviders = $redis->smembers('S_Telecom_Operators');
            sort($teleComProviders);
            if ($refData['oprName'] == "" || $refData['oprName'] == "OTHER") {
                $redis->srem('S_Telecom_Operators', "");
                $refData['oprName'] = "AIRTEL";
            }
            $oprNameList = array_combine($teleComProviders, $teleComProviders);
            $editData = [
                'vehicleId' => $vehicleId,
                'countryTimezoneList' => $countryTimezoneList,
                'refData' => $refData,
                'orgList' => $orgList,
                'Licence' => $Licence1,
                'Payment_Mode' => $Payment_Mode1,
                'protocol' => $protocol,
                'vol' => $vol,
                'ac' => $ac,
                'batteryvolt' => $batteryvolt,
                'acVolt' => $acVolt,
                'enable' => $enable,
                'enableVehicleExpiry' => $enableVehicleExpiry,
                'vehicleTypeList1' => $vehicleTypeList1,
                'vehicleTypeList2' => $vehicleTypeList2,
                'oprNameList' => $oprNameList,
                'ignitionSpeedList' => $ignitionSpeedList,
                'VolIgList' => $VolIgList,
                'acList' => $acList,
                'analogInputList' => $analogInputList,
                'digitalInputList' => $digitalInputList,
                'serialInputList' => $serialInputList,
                'enableDebugList' => $enableDebugList,
                'rfidTypeList' => $rfidTypeList,
                'timezoneList' => $timezoneList,
                'fuelTypeList' => $fuelTypeList,
                'vehicleModeList' => $vehicleModeList,
                'vehicleModelList' => $vehicleModelList,
                'fuelModeList' => $fuelModeList,
            ];
            return view('vdm.vehicles.edit', $editData);
        } catch (\Exception $e) {
            logger('------exception vehicle Edit ----------> ' . $e->getMessage() . '---line--->' . $e->getLine());
            return redirect()->to('VdmVehicleScan' . $vehicleId);
        }
    }

    public function edit1($id)
    {
        try {

            $username = auth()->id();

            $redis = Redis::connection();
            $vehicleId = $id;
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
            $deviceId = $redis->hget($vehicleDeviceMapId, $vehicleId);

            $details = $redis->hget('H_RefData_' . $fcode, $vehicleId);
            $date = isset($details1['date']) ? $details1['date'] : '';
            $licencetype = isset($details1['Licence']) ? $details1['Licence'] : 'Advance';
            if ($date == '' || $date == ' ') {
                $date1 = '';
            } else {
                $date1 = date("d-m-Y", strtotime($date));
            }
            $onDate = isset($details1['onboardDate']) ? $details1['onboardDate'] : $date1;
            $nullval = strlen($onDate);
            if ($nullval == 0 || $onDate == "null" || $onDate == " ") {
                $onboardDate = str_replace(' ', '', $date1);
            } else {
                $onboardDate = $onDate;
            }
            $countryTimezone = isset($details1['country']) ? $details1['country'] : '';
            $refData = null;
            $refData = Arr::add($refData, 'overSpeedLimit', '60');
            $refData = Arr::add($refData, 'driverName', '');
            $refData = Arr::add($refData, 'driverMobile', '');
            $refData = Arr::add($refData, 'gpsSimNo', '');
            $refData = Arr::add($refData, 'email', ' ');
            $refData = Arr::add($refData, 'odoDistance', '0');
            $refData = Arr::add($refData, 'sendGeoFenceSMS', 'no');
            $refData = Arr::add($refData, 'morningTripStartTime', ' ');
            $refData = Arr::add($refData, 'eveningTripStartTime', ' ');
            $refData = Arr::add($refData, 'altShortName', ' ');
            $refData = Arr::add($refData, 'date', ' ');
            $refData = Arr::add($refData, 'paymentType', ' ');
            $refData = Arr::add($refData, 'expiredPeriod', ' ');
            $refData = Arr::add($refData, 'fuel', 'no');
            $refData = Arr::add($refData, 'Licence', '');
            $refData = Arr::add($refData, 'Payment_Mode', '');
            $refData = Arr::add($refData, 'descriptionStatus', '');
            $refData = Arr::add($refData, 'mintemp', '');
            $refData = Arr::add($refData, 'maxtemp', '');
            $refData = Arr::add($refData, 'safetyParking', '');
            $refData = Arr::add($refData, 'licenceissuedDate', '');
            $refData = Arr::add($refData, 'onboardDate', $onboardDate);
            $refData = Arr::add($refData, 'mobileNo', " ");
            $refData = Arr::add($refData, 'countryTimezone', $countryTimezone);
            $refDataFromDB = json_decode($details, true);

            $refDatatmp = array_merge($refData, $refDataFromDB);

            $refData = $refDatatmp;
            //S_Schl_Rt_CVSM_ALH

            $orgId = isset($refDataFromDB['orgId']) ? $refDataFromDB['orgId'] : 'NotAvailabe';
            $refData = Arr::add($refData, 'orgId', $orgId);
            $parkingAlert = isset($refDataFromDB->parkingAlert) ? $refDataFromDB->parkingAlert : 0;
            $refData = Arr::add($refData, 'parkingAlert', $parkingAlert);
            $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
            if (session()->get('cur') == 'dealer') {
                $tmpOrgList = $redis->smembers('S_Organisations_Dealer_' . $username . '_' . $fcode);
            } else if (session()->get('cur') == 'admin') {
                $tmpOrgList = $redis->smembers('S_Organisations_Admin_' . $fcode);
            }


            $orgList = null;
            $orgList = Arr::add($orgList, 'DEFAULT', 'DEFAULT');
            foreach ($tmpOrgList as $org) {
                $org1 = strtoupper($org);
                if ($org == $org1) {
                    $orgList = Arr::add($orgList, $org, $org);
                }
            }
            $Payment_Mode1 = array();
            $Payment_Mode = DB::select('select type from Payment_Mode');
            foreach ($Payment_Mode as  $org1) {
                $Payment_Mode1 = Arr::add($Payment_Mode1, $org1->type, $org1->type);
            }
            $Licence1 = array();
            $Licence = DB::select('select type from Licence');
            foreach ($Licence as  $org) {
                $Licence1 = Arr::add($Licence1, $org->type, $org->type);
            }
            $franDetails_json = $redis->hget('H_Franchise', $fcode);
            $franchiseDetails = json_decode($franDetails_json, true);
            $prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
            if ($prepaid == 'yes') {
                $protocol = VdmFranchiseController::getProtocal($licencetype);
            } else {
                $protocol = BusinessController::getProtocal();
            }
            $timezoneList = array('' => '-- Select Timezone --');
            $timeList1 = VdmFranchiseController::timeZoneC();
            foreach ($timeList1 as $key => $timez) {
                $timezoneList = Arr::add($timezoneList, $timez, $timez);
            }

            $oprNameList = ['airtel' => 'Airtel', 'idea' => 'Idea', 'reliance' => 'Reliance', 'vodafone' => 'Vodafone'];
            $vehicleTypeList = ['Ambulance' => 'Ambulance', 'Bike' => 'Bike', 'Bull' => 'Bull', 'Bus' => 'Bus', 'Car' => 'Car', 'heavyVehicle' => 'Heavy Vehicle', 'Truck' => 'Truck', 'Van' => 'Van'];
            $yesOrNo = ['yes' => 'Yes', 'no' => 'No'];
            $rfidList = ['type1' => 'Type1', 'type2' => 'Type2'];
            $editData = [
                'vehicleId' => $vehicleId,
                'refData' => $refData,
                'countryTimezoneList' => $timezoneList,
                'orgList' => $orgList,
                'Licence' => $Licence1,
                'Payment_Mode' => $Payment_Mode1,
                'protocol' => $protocol,
                'oprNameList' => $oprNameList,
                'yesOrNo' => $yesOrNo,
                'vehicleTypeList' => $vehicleTypeList,
                'rfidList' => $rfidList
            ];

            return view('vdm.vehicles.edit1', $editData);
        } catch (\Exception $e) {
            return redirect()->to('VdmVehicleScan' . $vehicleId);
        }
    }

    public static function calibrateType($id, $count)
    {

        $username = auth()->id();
        $redis = Redis::connection();
        $vehicleId = $id;
        $sensorId = $count;

        $fcode = $redis->hget("H_UserId_Cust_Map", "$username:fcode");
        $deviceId = $redis->hget("H_Vehicle_Device_Map_$fcode", $vehicleId);

        $details = $redis->hget("H_RefData_$fcode", $vehicleId);
        $refData = json_decode($details, true);
        $noOfTank = $refData["noOfTank"] ?? 1;
        $noOfsensor = $refData["sensorCount"] ?? 1;
        if (isset($refData['fuelMode'])) {
            $caliType  = $refData['fuelMode'];
        } else {
            $vehicleFuelData = $redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId);
            $vehicleFuelData = json_decode($vehicleFuelData, true);
            $vehicleProData = $redis->hget('H_ProData_' . $fcode, $vehicleId);
            $vehicleProDataList = explode(',', $vehicleProData);
            $direct = isset($vehicleProDataList[17]) ? $vehicleProDataList[17] : '';
            $fuelType = isset($refData['fuelType']) ? $refData['fuelType'] : 'serial';
            $tankshape = isset($vehicleFuelData['tankshape']) ? $vehicleFuelData['tankshape'] : 'Abnormal';
            if ($direct == '-1.0' && $fuelType !== 'analog') {
                $caliType = "direct";
            } else if ($tankshape !== "Abnormal" && $tankshape !== "") {
                $caliType = 'auto';
            } else {
                $caliType = 'manual';
            }
        }
        $type = $caliType;

        if ($sensorId == 1) {
            $fuelDetails = $redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId);
            $zensorKey = 'Z_Sensor_' . $vehicleId . '_' . $fcode;
        } else {
            $fuelDetails = $redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId . ":" . $sensorId);
            $zensorKey = 'Z_Sensor' . $sensorId . '_' . $vehicleId . '_' . $fcode;
        }
        $fuelRefData = json_decode($fuelDetails, true);
        $vehicleMode = isset($refData['vehicleMode']) ? $refData['vehicleMode'] : 'MovingVehicle';

        $address1 = array();
        $place = array();
        $tankList = array();
        $tankDetails = array();
        $tankDetail = null;
        $tankData = null;
        for ($i = 1; $i <= $noOfTank; $i++) {
            $tankList = Arr::add($tankList, $i, $i);
            if ($i == 1) {
                $tankDetail = $redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId);
            } else {
                $tankDetail = $redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId . ":" . $i);
            }
            $tankData = json_decode($tankDetail, true);
            $tankDetails = Arr::add($tankDetails, $i, $tankData);
        }
        $tanktype = "1:analog";
        if ($sensorId == "" || $sensorId == null) {
            $tanktype = $redis->hget("H_SensorTank_$fcode", "$vehicleId:1");
        } else {
            $tanktype = $redis->hget("H_SensorTank_$fcode", "$vehicleId:$sensorId");
        }
        $sensorMode = 'MovingVehicle';
        if ($tanktype != null) {
            $tankandfuel = explode(':', $tanktype);
            $tankId = $tankandfuel[0];
            $fuelType = $tankandfuel[1];
            if (sizeof($tankandfuel) > 3) {
                $sensorMode = $tankandfuel[3];
            }
            if ($noOfTank == '1' && $noOfsensor == '1') {
                $sensorMode = $vehicleMode;
            }
        } else {
            $tankId = $sensorId <= $noOfTank ? $sensorId : '1';
            $fuelType = 'analog';
            $sensorMode = $vehicleMode;
        }

        $address1 = $redis->zrange($zensorKey, 0, -1, 'withscores');
        if ($sensorId == 1) {
            $sensorId = "";
        }

        if ($type == "manual") {

            $keys = $redis->keys('*Z_Sensor' . $sensorId . '_' . $vehicleId . '_*');
            $preSensor = [];
            if (!is_null($keys) && count($keys) >= 2) {
                $prevKey = strlen($keys[0]) <= strlen($keys[1]) ? $keys[1] : $keys[0];
                $preSensor = $redis->zrange($prevKey, 0, -1, 'withscores');
            }

            $v = 0;
            foreach ($address1 as $org => $rowId) {
                $rowId = floor($rowId * 10000) / 10000;
                $liter = $org;
                $volt = 0;
                $freq = 0;

                if ($fuelType == 'analog' || $fuelType == 'analog') {
                    if (count($preSensor) >= 1) {
                        $frequ = floor($preSensor[$org] * 10000) / 10000;
                        $volt = $rowId;
                        $freq = $frequ;
                    } else {
                        $volt = $rowId;
                    }
                } else {
                    if (count($preSensor) >= 1) {
                        $volt = floor($preSensor[$org] * 10000) / 10000;
                        $volt = $volt;
                        $freq = $rowId;
                    } else {
                        $freq = $rowId;
                    }
                }
                $place[] = [
                    'liter' => $liter,
                    'volt' => $volt,
                    'freq' => $freq
                ];
            }
        }

        if ($count == null || $count == "" || $count == 1) {
            $refDataJson = $redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId);
            $nameOfTheTank = "1";
        } else {
            $nameOfTheTank = $count;
            if ($caliType == 'manual') {
                $refDataJson =  $tankId <= '1' ? $redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId) : $redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId . ':' . $tankId);
            } else {
                $refDataJson = $redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId . ':' . $count);
            }
        }

        $newVehicle = (bool)is_null($refDataJson);
        $vehicleData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
        $vehicleData1 = json_decode($vehicleData, true);
        $refDataJson1 = json_decode($refDataJson, true);
        $vehicleName = isset($vehicleData1['shortName']) ? $vehicleData1['shortName'] : '';
        if ($count == 1 || $count == null) {
            $tanksize = isset($vehicleData1['tankSize']) ? $vehicleData1['tankSize'] : '0';
            $fuelInput = isset($vehicleData1['fuelType']) ? $vehicleData1['fuelType'] : '';
            $vehicletype = isset($vehicleData1['vehicleMode']) ? $vehicleData1['vehicleMode'] : '';
        } else {
            $tanksize = isset($refDataJson1['tanksize']) ? $refDataJson1['tanksize'] : '0';
            $fuelInput = isset($refDataJson1['fuelType']) ? $refDataJson1['fuelType'] : '';
            $vehicletype = isset($refDataJson1['vehicletype']) ? $refDataJson1['vehicletype'] : '';
        }
        // $deviceModel = isset($vehicleData1['deviceModel']) ? $vehicleData1['deviceModel'] : 'GT06N';

        $maxitheft = isset($refDataJson1['maxitheft']) ? $refDataJson1['maxitheft'] : '0';
        $maxitheft = $maxitheft !== "" ? $maxitheft : "0";
        $minifilling = isset($refDataJson1['minifilling']) ? $refDataJson1['minifilling'] : '0';
        $minifilling = $minifilling !== "" ? $minifilling : "0";
        $tankshape = isset($refDataJson1['tankshape']) ? $refDataJson1['tankshape'] : '';
        $sensorheight = isset($refDataJson1['sensorheight']) ? $refDataJson1['sensorheight'] : '';
        $tankposition = isset($refDataJson1['tankposition']) ? $refDataJson1['tankposition'] : '';

        $Cylength = isset($refDataJson1['length']) ? $refDataJson1['length'] : '';
        $Cylength = ($tankshape == 'Cylinder') ? $Cylength : "";

        $radius = isset($refDataJson1['radius']) ? $refDataJson1['radius'] : '';
        $maxval = isset($refDataJson1['maxval']) ? $refDataJson1['maxval'] : '';
        $maxvolt = isset($refDataJson1['maxvolt']) ? $refDataJson1['maxvolt'] : '';
        $minvolt = isset($refDataJson1['minvolt']) ? $refDataJson1['minvolt'] : '';
        $rectangelCalculate = $refDataJson1['rectangelCalculate'] ?? "no";
        $recLength = $refDataJson1['recLength'] ?? "";
        $recHeight = $refDataJson1['recHeight'] ?? "";
        $recWidth = $refDataJson1['recWidth'] ?? "";

        $sensorNumbers = isset($fuelRefData['sensorNumbers']) ? $fuelRefData['sensorNumbers'] : null;
        if (is_null($sensorNumbers)) {
            $sensorNumbers = [$count];
        }
        $descriptionStatus = $redis->hget("H_CalibrationData_$fcode", $vehicleId) ?? "";
        $calibrateTypeData = [
            'sensorNumbers' => $sensorNumbers,
            'tankDetails' => $tankDetails,
            'navTabcount' => $sensorId,
            'tankCount' => $noOfTank,
            'calibrateType' => $type,
            'minvolt' => $minvolt,
            'vehicleId' => $vehicleId,
            'vehicletype' => $vehicletype,
            'maxitheft' => $maxitheft,
            'tankshape' => $tankshape,
            'minifilling' => $minifilling,
            'radius' => $radius,
            'maxval' => $maxval,
            'maxvolt' => $maxvolt,
            'vehicleName' => $vehicleName,
            'Cylength' => $Cylength,
            'tankposition' => $tankposition,
            'fuelInput' => $fuelInput,
            'nameOfTheTank' => $nameOfTheTank,
            'deviceId' => $deviceId,
            'place' => $place,
            'tanksize' => $tanksize,
            'sensorNo' => $sensorId,
            'sensorType' => $fuelType,
            'sensorCount' => $noOfsensor,
            'tankList' => $tankList,
            'tankId' => $tankId,
            'sensorMode' => $sensorMode,
            'newVehicle' => $newVehicle,
            'descriptionStatus' => $descriptionStatus,
            'rectangelCalculate' => $rectangelCalculate,
            'recLength' => $recLength,
            'recHeight' => $recHeight,
            'recWidth' => $recWidth,
        ];

        return view('vdm.vehicles.calibrateType', $calibrateTypeData);
    }

    public static function autoCalibrate($tankId1)
    {
        $username = auth()->id();
        $vehicleId = request()->get('vehicleId');
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $sensorCount = request()->get('Noofsensor');
        $noOfTank = request()->get('Nooftank');
        $tanksize = request()->get('tanksize');
        $vehicletype = request()->get('vehicletype');
        $fuelType = request()->get('fuelType');
        $maxitheft = request()->get('maxitheft');
        $minifilling = request()->get('minifilling');
        $tankshape = request()->get('tankshape');
        $tankposition = request()->get('tankposition');
        $sensorheight = request()->get('sensorheight');
        $minvolt = request()->get('minvolt');
        $nameoftheTank = request()->get('navTabcount');
        $sensorMode = request()->get('sensorMode');
        // $sensorNumbers = request()->get('sensorNumbers');
        $capacity = request()->get('capacity');
        $deviceId = request()->get('deviceId');
        $sensorType  = request()->get('sensorType');
        $typecali = request()->get('typecali');
        $rectangelCalculate = request()->get('rectangelCalculate');

        $tankIdNumber = $tankId1;
        if ($tankId1 == '' || $tankId1 == null) {
            $tankIdNumber = 1;
        }
        if ($typecali !== 'auto') {
            $tankshape = 'Abnormal';
            if ($sensorCount == '1' && $noOfTank == '1') {
                $vehicletype = $sensorMode;
            }
            if ($nameoftheTank == "1") {
                $fuelType  = $sensorType;
            }
        } else {
            $tankIdNumber = $tankId1;
            if ($tankId1 == '' || $tankId1 == null) {
                $tankIdNumber = 1;
            }

            // if ($sensorNumbers == null) $sensorNumbers = [];
            // foreach ($sensorNumbers as $key => $value) {
            //     $temp = $tankIdNumber . ':' . $fuelType . ':' . $value . ':' . $vehicletype;
            //     $redis->hset('H_SensorTank_' . $fcode, $vehicleId . ':' . $value, $temp);
            // }
            $redis->PUBLISH('sms:topicNetty', $deviceId);
        }
        $fuelKey = $vehicleId;
        if ($tankIdNumber !== 1) {
            $fuelKey = $vehicleId . ':' . $tankId1;
        }
        $oldRefFuelData = $redis->hget('H_Ref_FuelData_' . $fcode, $fuelKey);
        $oldRefFuelData = json_decode($oldRefFuelData, true);

        $length = '';
        $radius = '';
        $recLength = '';
        $recHeight = '';
        $recWidth = '';
        if ($tankshape == 'Cylinder') {
            $length = request()->get('Cylength');
            $radius = request()->get('radius');
            $rectangelCalculate = "no";
        } else {
            if ($rectangelCalculate == "yes") {
                $recLength = request()->get('recLength');
                $recHeight = request()->get('recHeight');
                $recWidth = request()->get('recWidth');
            }
        }

        $maxval = request()->get('maxval');
        $maxvolt = request()->get('maxvolt');
        $refFuelDataArr =  [
            'vehicleId' => $vehicleId,
            'tanksize' => $tanksize,
            'vehicletype' => $vehicletype,
            'fuelType' => $fuelType,
            'maxitheft' => $maxitheft,
            'minifilling' => $minifilling,
            'tankshape' => $tankshape,
            'tankposition' => $tankposition,
            'sensorheight' => $sensorheight,
            'length' => $length,
            'radius' => $radius,
            'maxval' => $maxval,
            'maxvolt' => $maxvolt,
            'minvolt' => $minvolt,
            'rectangelCalculate' => $rectangelCalculate,
            'recLength' => $recLength,
            'recHeight' => $recHeight,
            'recWidth' => $recWidth,
            // 'sensorNumbers' => $sensorNumbers,
        ];
        $refFuelDataArrJson = json_encode($refFuelDataArr);
        $vehicleData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
        $vehicleData = json_decode($vehicleData, true);
        if ($tankId1 == "" || $tankId1 == 1) {
            $redis->hset('H_Ref_FuelData_' . $fcode, $vehicleId, $refFuelDataArrJson);
            $vehicleData['tankSize'] = $tanksize;
            $vehicleData['fuelType'] = $fuelType;
            $vehicleData['vehicleMode'] = $vehicletype;
            $vehicleDataJson = json_encode($vehicleData);
            $redis->hset('H_RefData_' . $fcode, $vehicleId, $vehicleDataJson);
        } else {
            $redis->hset('H_Ref_FuelData_' . $fcode, $vehicleId . ':' . $tankId1, $refFuelDataArrJson);
        }
        try {
            if ($tankId1 == '' || $tankId1 == null) {
                $tankId1 = 1;
            }
            // dd($oldRefFuelData,$refFuelDataArr);
            if ($oldRefFuelData == null) $oldRefFuelData = [];
            if ($refFuelDataArr == null) $refFuelDataArr = [];
            $oldData = array_diff($oldRefFuelData, $refFuelDataArr);
            $newData = array_diff($refFuelDataArr, $oldRefFuelData);

            $mysqlDetails = [
                'userIpAddress' => session()->get('userIP'),
                'userName' => $username,
                'type' => config()->get('constant.autocalibrate'),
                'name' => $vehicleId,
                'status' => config()->get('constant.auto_calibration'),
                'oldData' => json_encode(array_merge(['tank' => $tankId1], $oldData)),
                'newData' => json_encode(array_merge(['tank' => $tankId1], $newData))
            ];
            AuditNewController::updateAudit($mysqlDetails);
            // $status = array(
            //     'userName' => $username,
            //     'userIpAddress' => session()->get('userIP', '-'),
            //     'tankId' => $tankId1,
            // );
            // $mysqlDetails   = array_merge($status, $refFuelDataArr);
            // VdmVehicleController::DbEntry($mysqlDetails, 'AuditAutoCalibrate', $fcode);
        } catch (Exception $e) {
            dd($e);
            $err = $e->getMessage();
            $errLine = $e->getLine();
            logger("EXCEPTIONBYUSER_$username Error => $err ErrorLinr => $errLine");
        }
    }

    public static function calibrateTypeUpdate()
    {

        try {
            $username = auth()->id();
            $vehicleId = request()->get('vehicleId');
            $redis = Redis::connection();
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $sensorCount = request()->get('Noofsensor');
            $noOfTank = request()->get('Nooftank');
            $typecali = request()->get('typecali');

            $vehicleData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
            $vehicleData = json_decode($vehicleData, true);
            $vehicleData['sensorCount'] = $sensorCount;
            $vehicleData['noOfTank'] = $noOfTank;
            $vehicleData['fuelMode'] = $typecali;
            $vehicleDataJson = json_encode($vehicleData);
            $redis->hset('H_RefData_' . $fcode, $vehicleId, $vehicleDataJson);

            $maxitheft = request()->get('maxitheft');
            $minifilling = request()->get('minifilling');
            $tanksize = request()->get('tanksize');
            $modelSubmit = request()->get('modelSubmit');

            $descriptionStatus = request()->get('descriptionStatus');
            $redis->hset("H_CalibrationData_$fcode", $vehicleId, $descriptionStatus);


            $tankId1 = request()->get('navTabcount');
            if ($typecali === 'auto') {
                self::autoCalibrate($tankId1);
            } else if ($typecali === 'manual') {

                $vehicleId = request()->get('vehicleId');
                $sensorNo = request()->get('sensorNo');
                $sensorMode = request()->get('sensorMode');
                $sensorType = request()->get('sensorType');
                $tankId = request()->get('tankId');
                $sensorTypeOld = request()->get('sensorTypeOld');
                $litres = request()->get('litre');
                $volts = request()->get('volt');
                $frequencys = request()->get('frequency');

                $newData1 = [
                    'tankId' => $tankId,
                    'sensorType' => $sensorType,
                    'sensorMode' => $sensorMode,
                ];
                if ($sensorNo == 1) {
                    $sensorNo = "";
                }

                //for update minifill && minitheft
                self::autoCalibrate($tankId);

                if ($sensorNo == "" || $sensorNo == null) {
                    $oldData = $redis->hget('H_SensorTank_' . $fcode, "$vehicleId:1");
                    $redis->hset('H_SensorTank_' . $fcode, "$vehicleId:1", $tankId . ':' . $sensorType . ':1:' . $sensorMode);
                } else {
                    $oldData = $redis->hget('H_SensorTank_' . $fcode, "$vehicleId:$sensorNo");
                    $redis->hset('H_SensorTank_' . $fcode, $vehicleId . ':' . $sensorNo, $tankId . ':' . $sensorType . ':' . $sensorNo . ':' . $sensorMode);
                }
                $oldData = explode(":", $oldData);
                $oldData1 = [
                    'tankId' => $oldData[0],
                    'sensorType' => $oldData[1],
                    'sensorMode' => $oldData[3],
                ];

                $Z_Sensor = 'Z_Sensor' . $sensorNo . '_' . $vehicleId . '_' . $fcode;

                // dd(request()->all());
                $redis->del($Z_Sensor);
                $auditLitre = [];
                $auditVolt = [];
                $auditFrequency = [];
                if ($litres == null) $litres = [];
                if ($sensorType == "analog" || $sensorType == 'analog1') {                    
                    $redis->del('Z_Sensor' . $sensorNo . '_' . $vehicleId . '_analog_' . $fcode);
                    foreach ($litres as $key => $litre) {
                        $volt = $volts[$key];
                        $frequency = $frequencys[$key];

                        if ($litre == null || $litre == '' || $litre <= 0) $litre = "0";
                        if ($volt == null || $volt == '' || $volt <= 0) $volt = "0";
                        if ($frequency == null || $frequency == '' || $frequency <= 0) $frequency = "0";
                        $auditLitre[] = $litre;
                        $auditVolt[] =  $volt;
                        $auditFrequency[] = $frequency;

                        $redis->zadd($Z_Sensor, $volt, $litre);

                        $redis->zadd('Z_Sensor' . $sensorNo . '_' . $vehicleId . '_serial_' . $fcode, $frequency, $litre);
                    }
                } else {
                    $redis->del('Z_Sensor' . $sensorNo . '_' . $vehicleId . '_serial_' . $fcode);
                    foreach ($litres as $key => $litre) {
                        $volt = $volts[$key];
                        $frequency = $frequencys[$key];

                        if ($litre == null || $litre == '' || $litre <= 0) $litre = "5";
                        if ($volt == null || $volt == '' || $volt <= 0) $volt = "0";
                        if ($frequency == null || $frequency == '' || $frequency <= 0) $frequency = "0";
                        $auditLitre[] = $litre;
                        $auditVolt[] =  $volt;
                        $auditFrequency[] = $frequency;
                        $redis->zadd($Z_Sensor, $frequency, $litre);

                        $redis->zadd('Z_Sensor' . $sensorNo . '_' . $vehicleId . '_analog_' . $fcode, $volt, $litre);
                    }
                }
                try {
                    if ($sensorNo == '' || $sensorNo == null) {
                        $sensorNo = 1;
                    }
                    $oldData = array_diff($oldData1, $newData1);
                    $newData = array_diff($newData1, $oldData1);
                    $newData['liter'] = join(',', $auditLitre);
                    $newData['volt'] = join(',', $auditVolt);
                    $newData['frequency'] = join(',', $auditFrequency);
                    $mysqlDetails = [
                        'userIpAddress' => session()->get('userIP'),
                        'userName' => $username,
                        'type' => config()->get('constant.manualcalibrate'),
                        'name' => $vehicleId,
                        'status' => config()->get('constant.manual_calibration'),
                        'oldData' => json_encode($oldData),
                        'newData' => json_encode($newData)
                    ];
                    AuditNewController::updateAudit($mysqlDetails);
                } catch (Exception $e) {
                    logger('Exception_' . $vehicleId . ' error --->' . $e->getMessage() . ' line--->' . $e->getLine());
                    logger('Error inside AuditVehicleManual Update' . $e->getMessage() . '--line-->' . $e->getLine());
                }
            } else if ($typecali === "direct") {

                $directsensorMode = request()->get("directsensorMode");
                $fuelRefData = $redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId);
                $fuelRefData = json_decode($fuelRefData, true);
                $vehicleData['fuelMode'] = 'direct';
                $vehicleData['fuelType'] = 'serial1';
                $vehicleData['tankSize'] = $tanksize;
                $vehicleData['vehicleMode'] = $directsensorMode;
                $vehicleDataJson = json_encode($vehicleData);
                $redis->hset('H_RefData_' . $fcode, $vehicleId, $vehicleDataJson);
                $redis->hset('H_SensorTank_' . $fcode, $vehicleId . ':1',   '1:serial1:1:' . $directsensorMode);

                $fuelRefData['fuelType'] = 'serial1';
                $fuelRefData['maxitheft'] = $maxitheft;
                $fuelRefData['minifilling'] = $minifilling;
                $fuelRefData['tanksize'] = $tanksize;

                $fuelRefData = json_encode($fuelRefData);
                $redis->hset('H_Ref_FuelData_' . $fcode, $vehicleId, $fuelRefData);
            }
            
            if ($modelSubmit == "true") {
                $count = request()->get('navTabcount') ?? '1';
                return redirect()->route('calibrate', ['vid' => $vehicleId, 'count' => $count]);
            }

            return redirect()->to('VdmVehicleScan' . $vehicleId)->with([
                'message' => 'Successfully updated Fuel Configuration !'
            ]);
        } catch (\Exception $e) {
            $username = auth()->id();
            $err = $e->getMessage();
            $errLine = $e->getLine();
            logger("EXCEPTIONBYUSER_$username Error => $err ErrorLinr => $errLine");
            return redirect()->to('VdmVehicleScan' . $vehicleId)->with([
                'message' => "Can't update Fuel Configuration !",
                'alert-class' => "alert-warning"
            ]);
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param int $id            
     * @return Response
     */

    //if change done in updates should done in updateLive also
    public function update($id)
    {
        try {

            $vehicleId = $id;
            $username = auth()->id();
            $redis = Redis::connection();
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
            $refDataJson1 = $redis->hget('H_RefData_' . $fcode, $vehicleId);
            $refDataJson1 = json_decode($refDataJson1, true);
            $oldData = $refDataJson1;
            $shortName1 = isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : $vehicleId;
            $shortName2 = str_replace(' ', '', $shortName1);
            $shortNameOld = strtoupper($shortName2);
            $mobileNoOld = isset($refDataJson1['mobileNo']) ? $refDataJson1['mobileNo'] : '';
            $gpsSimNoOld = isset($refDataJson1['gpsSimNo']) ? $refDataJson1['gpsSimNo'] : '';
            $orgIdOld1 = isset($refDataJson1['orgId']) ? $refDataJson1['orgId'] : 'Default';
            $orgIdOld = strtoupper($orgIdOld1);
            $orgId = isset($refDataJson1['orgId']) ? $refDataJson1['orgId'] : 'Default';
            $orgId1 = strtoupper($orgId);
            $own = isset($refDataJson1['OWN']) ? $refDataJson1['OWN'] : 'OWN';
            $fuelType = isset($refDataJson1['fuelType']) ? $refDataJson1['fuelType'] : '';
            $vehicleMode = isset($refDataJson1['vehicleMode']) ? $refDataJson1['vehicleMode'] : '';


            $rules = array(
                'VehicleName' => 'required',
                'regNo' => 'required',
                'vehicleType' => 'required',
            );
            $validator = validator()->make(request()->all(), $rules);
            if ($validator->fails()) {
                logger()->error(' VdmVehicleConrtoller update validation failed++');
                return back()->withErrors($validator);
            } else {
                // store
                $shortName1 = request()->get('VehicleName');
                $shortName = strtoupper($shortName1);
                $regNo = request()->get('regNo');
                $vehicleMake = request()->get('vehicleMake');
                $vehicleType = request()->get('vehicleType');
                $oprName = request()->get('oprName');
                $vehicleExpiry = request()->get('vehicleExpiry');
                $onboardDate = request()->get('onboardDate');
                $overSpeedLimit = request()->get('overSpeedLimit');
                $overSpeedLimit = !empty($overSpeedLimit) ? $overSpeedLimit : '60';
                $deviceModel = request()->get('deviceModel');
                $driverName = request()->get('driverName');
                $driverMobile = request()->get('driverMobile');
                $orgId = request()->get('orgId');
                $orgId = str_replace('$', '.', $orgId);
                $sendGeoFenceSMS = request()->get('sendGeoFenceSMS');
                $gpsSimNo = request()->get('gpsSimNo');
                $odoDistance = request()->get('odoDistance');
                $eveningTripStartTime = request()->get('eveningTripStartTime');
                $fuel = request()->get('fuel');
                $altShortName = request()->get('altShortName');
                $rfidType = request()->get('rfidType');
                $ipAddress = request()->get('ipAddress');
                $analog1 = request()->get('analog1');

                $serial1 = request()->get('serial1');
                $serial2 = request()->get('serial2');
                $mintemp = request()->get('mintemp');
                $maxtemp = request()->get('maxtemp');
                $safetyParking = request()->get('safetyParking');
                $tankSize = request()->get('tankSize');
                $VolIg = request()->get('VolIg');
                $batteryvolt = request()->get('batteryvolt');
                $ac = request()->get('ac');
                $acVolt = request()->get('acVolt');
                $distManipulationPer = request()->get('DistManipulationPer');
                $assetTracker = request()->get('assetTracker');
                // $vehicleMode = request()->get('vehicleMode');
                $countryTimezone = request()->get('countryTimezone');
                $defaultMileage = request()->get('defaultMileage');
                $installationDate = request()->get('installationDate');
                $ignitionSpeed = request()->get('ignitionSpeed');
                $sensorCount = request()->get('sensorCount');
                $noOfTank = request()->get('noOfTank');
                $gpsSimICCID = request()->get('gpsSimICCID');
                $madeIn = request()->get('madeIn');
                $manufacturingDate = request()->get('manufacturingDate');
                $chassisNumber = request()->get('chassisNumber');
                $expectedMileage = request()->get('expectedMileage');
                $tankInterlinked = request()->get('tankInterlinked');
                // $kalmanFilter = request()->get('kalmanFilter');
                $fuelBatteryVolt = request()->get('fuelBatteryVolt');
                $vehicleModel = request()->get('vehicleModel');
                $rigMode = request()->get('rigMode');
                $fuelMode = request()->get('fuelMode');
                $isAc = request()->get('isAc');

                $vehicleModel = request()->get('vehicleModel');
                if ($vehicleModel == "Other") {
                    $otherVehicleModel = request()->get("otherVehicleModel");
                    if ($otherVehicleModel == null) $otherVehicleModel = "";
                    if ($otherVehicleModel == "") {
                        return back()->withInput()->withErrors("Please Enter Other Vehicle Model Field");
                    }
                    $vehicleModel = ucwords(trim($otherVehicleModel));
                    $redis->sadd('S_VehicleModels', $vehicleModel);
                }
                if (strtolower($oprName) == "other") {
                    $otherTeleComProvider = request()->get("otherTeleComProvider");
                    if ($otherTeleComProvider == null) $otherTeleComProvider = "";
                    if ($otherTeleComProvider == "") {
                        return back()->withInput()->withErrors("Please Enter Other Telecom Operator Field");
                    }
                    $oprName = strtoupper(trim($otherTeleComProvider));
                    $redis->sadd('S_Telecom_Operators', $oprName);
                }

                $secondaryEngineHours = request()->get('secondaryEngineHours');
                if ($secondaryEngineHours) {
                    $slite = explode(":", $secondaryEngineHours);
                    $hrs = isset($slite[0]) ? trim($slite[0]) : 0;
                    $mins = isset($slite[1]) ? trim($slite[1]) : 0;
                    $secs = isset($slite[2]) ? trim($slite[2]) : 0;
                    $hrs = (int)$hrs;
                    $mins = (int)$mins;
                    $secs = (int)$secs;
                    $secondaryEngineHours =  ($hrs * 60000 * 60) + ($mins * 60000) + ($secs * 1000);
                } else {
                    $secondaryEngineHours  = 0;
                }

                if ($rigMode !== "Enable") {
                    $secondaryEngineHours = 0;
                } else {
                    $assetTracker = 'no';
                }

                if ($sensorCount == 1) {
                    $noOfTank = 1;
                }
                $redis = Redis::connection();
                $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
                $vehicleRefData = json_decode($vehicleRefData, true);

                $deviceId = $vehicleRefData['deviceId'];

                $date = isset($vehicleRefData['date']) ? $vehicleRefData['date'] : '';
                if ($date == '' || $date == ' ') {
                    $date1 = '';
                } else {
                    $date1 = date("d-m-Y", strtotime($date));
                }
                $onDate = isset($vehicleRefData['onboardDate']) ? $vehicleRefData['onboardDate'] : $date1;
                $nullval = strlen($onDate);
                if ($nullval == 0 || $onDate == "null" || $onDate == " ") {
                    $onboardDate = $date1;
                } else {
                    $onboardDate = $onDate;
                }
                //$onboardDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:$date1;
                // $paymentType = isset($vehicleRefData['paymentType']) ? $vehicleRefData['paymentType'] : '';
                // $expiredPeriod = isset($vehicleRefData['expiredPeriod']) ? $vehicleRefData['expiredPeriod'] : '';
                $Licence = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
                //$Licence=request()->get ( 'Licence1');    
                //$Licence=!empty($Licence) ? $Licence : 'Advance';
                $descriptionStatus = request()->get('descriptionStatus');
                $descriptionStatus = !empty($descriptionStatus) ? $descriptionStatus : '';

                // $Payment_Mode = request()->get('Payment_Mode1');
                // $Payment_Mode = !empty($Payment_Mode) ? $Payment_Mode : 'Monthly';
                $licence = isset($vehicleRefData['licenceissuedDate']) ? $vehicleRefData['licenceissuedDate'] : '';
                $communicatingPortNo = isset($vehicleRefData['communicatingPortNo']) ? $vehicleRefData['communicatingPortNo'] : '';
                //$vehicleExpiry=isset($vehicleRefData['vehicleExpiry'])?$vehicleRefData['vehicleExpiry']:'';

                //enable log
                $enabledebug = request()->get('enable');
                if ($enabledebug == 'Enable') {
                    $vehgroups = $redis->smembers('S_' . $vehicleId . '_' . $fcode);
                    $validity1 = 'oneDay';
                    $validity = 1 * 86400;
                    $redis->set('EnableLog:' . $vehicleId . ':' . $fcode, $validity1);
                    $redis->expire('EnableLog:' . $vehicleId . ':' . $fcode, $validity);
                    $deviceId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicleId);
                    if ($vehgroups != null) {
                        foreach ($vehgroups as $key => $value) {
                            $groupusers = $redis->smembers($value);
                            if ($groupusers != null) {
                                foreach ($groupusers as $key => $value1) {
                                    $redis->expire('EnableUserLog:' . $value1 . ':' . $fcode, $validity);
                                }
                            }
                        }
                    }
                    $pub = $redis->PUBLISH('sms:topicNetty', $deviceId);
                }
                if ($enabledebug == 'Disable') {
                    $redis->del('EnableLog:' . $vehicleId . ':' . $fcode);
                    $deviceId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicleId);
                    $pub = $redis->PUBLISH('sms:topicNetty', $deviceId);
                }
                $ownership = isset($vehicleRefData['OWN']) ? $vehicleRefData['OWN'] : 'OWN';


                $refDataArr = [
                    'deviceId' => $deviceId,
                    'shortName' => $shortName,
                    'deviceModel' => $deviceModel,
                    'regNo' => $regNo,
                    'vehicleMake' => $vehicleMake ?? '',
                    'vehicleType' => $vehicleType,
                    'oprName' => $oprName,
                    'overSpeedLimit' => $overSpeedLimit,
                    'odoDistance' => $odoDistance,
                    'driverName' => $driverName,
                    'driverMobile' => $driverMobile,
                    'gpsSimNo' => $gpsSimNo,
                    'orgId' => $orgId,
                    'altShortName' => $altShortName,
                    'fuel' =>  $fuel ?? 'no',
                    'fuelType' => $fuelType ?? '',
                    'rfidType' => $rfidType ?? 'no',
                    'Licence' => $Licence,
                    'OWN' => $ownership,
                    'descriptionStatus' => $descriptionStatus,
                    'ipAddress' => $ipAddress,
                    'analog1' => $analog1 ?? 'no',
                    'mintemp' => $mintemp ?? '',
                    'maxtemp' => $maxtemp ?? '',
                    'sendGeoFenceSMS' => $sendGeoFenceSMS ?? 'no',
                    'serial1' => $serial1,
                    'serial2' => $serial2,
                    'eveningTripStartTime' => $eveningTripStartTime,
                    'vehicleExpiry' => $vehicleExpiry,
                    'onboardDate' => $onboardDate,
                    'safetyParking' =>  $safetyParking ?? 'no',
                    'tankSize' =>  $tankSize ?? '0',
                    'licenceissuedDate' => $licence,
                    'VolIg' => $VolIg,
                    'batteryvolt' => $batteryvolt,
                    'ac' => $ac,
                    'acVolt' => $acVolt,
                    'DistManipulationPer' => $distManipulationPer,
                    'assetTracker' =>    $assetTracker ?? 'no',
                    'communicatingPortNo' => $communicatingPortNo,
                    'vehicleMode' =>  $vehicleMode ?? 'MovingVehicle',
                    'country' => $countryTimezone,
                    'defaultMileage' =>   $defaultMileage ?? 'no',
                    'installationDate' => $installationDate,
                    'ignitionSpeed' => $ignitionSpeed,
                    'sensorCount' => $sensorCount ?? '1',
                    'noOfTank' =>  $noOfTank ?? '1',
                    'gpsSimICCID' => $gpsSimICCID,
                    'madeIn' => $madeIn,
                    'manufacturingDate' => $manufacturingDate,
                    'chassisNumber' => $chassisNumber,
                    'expectedMileage' => $expectedMileage,
                    'tankInterlinked' =>  $tankInterlinked ?? 'no',
                    'fuelBatteryVolt' => $fuelBatteryVolt ?? 'no',
                    'vehicleModel' => $vehicleModel,
                    'rigMode' => $rigMode,
                    'fuelMode' => $fuelMode,
                    'secondaryEngineHours' => $secondaryEngineHours,
                    'isAc' => $isAc
                ];

                // for remove null to string //
                $newDate = $refDataArr;
                $refDataArr = InputController::nullCheckArr($refDataArr);

               
                $refDataJson = json_encode($refDataArr);
                // H_RefData
                $refDataJson1 = $redis->hget('H_RefData_' . $fcode, $vehicleId); //ram
                $refDataJson1 = json_decode($refDataJson1, true);

                $torg = isset($refDataJson1['orgId']) ? $refDataJson1['orgId'] : 'default';
                $org = isset($vehicleRefData->orgId) ? $vehicleRefData->orgId : $torg;
                $shrtName = isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : '$vehicleId';
                $oldroute = isset($vehicleRefData->shortName) ? $vehicleRefData->shortName : $shrtName;

                if ($org !== $orgId) {
                    $redis->srem('S_Vehicles_' . $org . '_' . $fcode, $vehicleId);
                    $redis->srem('S_Organisation_Route_' . $org . '_' . $fcode, $oldroute);
                    $redis->sadd('S_Organisation_Route_' . $orgId . '_' . $fcode, $shortName);
                }
                if ($oldroute !== $shortName && $org == $orgId) {
                    $redis->srem('S_Organisation_Route_' . $orgId . '_' . $fcode, $oldroute);
                    $redis->sadd('S_Organisation_Route_' . $orgId . '_' . $fcode, $shortName);
                }
                $vec = $redis->hget('H_ProData_' . $fcode, $vehicleId);
                $details = explode(',', $vec);
                $temp = null;
                $i = 0;
                foreach ($details as $gr) {
                    $i++;

                    if ($temp == null) {
                        $temp = $gr;
                    } else {
                        if ($i == 10 && $vehicleRefData['odoDistance'] !== $odoDistance) {
                            $odoupdate = $redis->sadd('S_OdometerChangedVehicles', $vehicleId);
                            $temp = $temp . ',' . $odoDistance;
                        } else {
                            $temp = $temp . ',' . $gr;
                        }
                    }
                }


                $redis->hset('H_ProData_' . $fcode, $vehicleId, $temp);

                //ram-new-key--
                $orgId1 = strtoupper($orgId);
                $shortNameNew = str_replace(' ', '', $shortName);
                $redis->hdel('H_VehicleName_Mobile_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortNameOld . ':' . $orgIdOld . ':' . $gpsSimNoOld);
                $redis->hset('H_VehicleName_Mobile_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortNameNew . ':' . $orgId1 . ':' . $gpsSimNo, $vehicleId);
                //---
                if ($own !== 'OWN') {
                    $redis->hdel('H_VehicleName_Mobile_Dealer_' . $own . '_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortNameOld . ':' . $orgIdOld . ':' . $gpsSimNoOld);
                    $redis->hset('H_VehicleName_Mobile_Dealer_' . $own . '_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortNameNew . ':' . $orgId1 . ':' . $gpsSimNo, $vehicleId);
                } else if ($own == 'OWN') {
                    $redis->hdel('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortNameOld . ':' . $orgIdOld . ':' . $gpsSimNoOld . ':OWN');
                    $redis->hset('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortNameNew . ':' . $orgId1 . ':' . $gpsSimNo . ':OWN', $vehicleId);
                }
                //

                $redis->sadd('S_Vehicles_' . $orgId . '_' . $fcode, $vehicleId);
                // regNo & VehId Map
                $redis->hset('H_Vehicle_RegNum_Map_' . $fcode, $vehicleId, $regNo);
                $redis->hset('H_Vehicle_RegNum_Map_' . $fcode, $regNo, $vehicleId);

                $redis->hset('H_RefData_' . $fcode, $vehicleId, $refDataJson);
                //FuelRefData update 
                $fuelData = $redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId);
                $fuelDataJson = json_decode($fuelData, true);
                $fuelDataJson['vehicletype'] = $vehicleMode;
                $fuelDataJsonNew = json_encode($fuelDataJson);
                $redis->hset('H_Ref_FuelData_' . $fcode, $vehicleId, $fuelDataJsonNew);

                $pub = $redis->PUBLISH('sms:topicNetty', $deviceId);

                $redis->hmset($vehicleDeviceMapId, $vehicleId, $deviceId, $deviceId, $vehicleId);
                $redis->sadd('S_Vehicles_' . $fcode, $vehicleId);
                //$redis->hset('H_Device_Cpy_Map',$deviceId,$fcode);
                // redirect
                session()->flash('message', 'Successfully updated ' . $vehicleId . '!');


                $refVehicle   = json_encode($vehicleRefData);
                if ($refVehicle != $refDataJson) {
                    $devices = array();
                    $devicestypes = array();
                    $mapping_Array = array(
                        "vehicleId" => "Vehicle Id",
                        "deviceId" => "Device Id",
                        "shortName" => "Vehicle Name",
                        "deviceModel" => "Device Model",
                        "regNo" => "Vehicle Registration Number",
                        "vehicleMake" => "Vehicle Make",
                        "vehicleType" => "Vehicle Type",
                        "oprName" => "Telecom Operator Name",
                        "mobileNo" => "Mobile Number for Alerts",
                        "overSpeedLimit" => "OverSpeed Limit",
                        "odoDistance" => "Odometer Reading",
                        "driverName" => "Driver Name",
                        "driverMobile" => "Driver Mobile Number",
                        "gpsSimNo" => "GPS Sim Number",
                        "email" => "Email for Notification",
                        "orgId" => "Org/College Name",
                        "altShortName" => "Alternate Vehicle Name",
                        "fuel" => "Fuel",
                        "fuelType" => "Fuel Type",
                        "isRfid" => "Is RFID",
                        "rfidType" => "Rfid Type",
                        "Licence" => "Licence",
                        "OWN" => "Owner Ship",
                        "Payment_Mode" => "Payment Mode",
                        "descriptionStatus" => "Description",
                        "ipAddress" => "IP Address",
                        "portNo" => "Port Number",
                        "analog1" => "Analog input 1",
                        "analog2" => "Analog input 2",
                        "digital1" => "Digital input 1",
                        "digital2" => "Digital input 2",
                        "serial1" => "Serial input 1",
                        "serial2" => "Serial input 2",
                        "digitalout" => "Digital output",
                        "mintemp" => "Minimum Temperature",
                        "maxtemp" => "Maximum Temperature",
                        "routeName" => "Route Name",
                        "sendGeoFenceSMS" => "Send GeoFence SMS",
                        "morningTripStartTime" => "Morning Trip StartTime",
                        "eveningTripStartTime" => "Evening Trip StartTime",
                        "parkingAlert" => "Parking Alert",
                        "expiredPeriod" => "Expired Period",
                        "vehicleExpiry" => "Vehicle Expiration Date",
                        "onboardDate" => "Onboard Date",
                        "safetyParking" => "Safety Parking",
                        "tankSize" => "Tank Size",
                        "licenceissuedDate" => "Licence Issued Date",
                        "VolIg" => "Engine ON",
                        "batteryvolt" => "Battery Voltage",
                        "ac" => "AC",
                        "acVolt" => "AC Voltage",
                        "DistManipulationPer" => "Distance Manipulation Percentage",
                        "assetTracker" => "Asset Tracker",
                        'communicatingPortNo' => 'communicatingPortNo',
                        'vehicleMode' => 'Mode of Vehicle',
                        'country' => 'Country Timezone',
                        'defaultMileage' => 'Odo Manipulation',
                        'installationDate' => 'Installation Date',
                        'ignitionSpeed' => 'Speed',
                        'sensorCount' => 'Sensor Count',
                        'noOfTank' => 'Number of Tank',
                        'gpsSimICCID' => " GPSSim ICCID Number",
                        'madeIn' => "Made In",
                        'manufacturingDate' => "Manufacturing Date",
                        'chassisNumber' => "Chassis Number",
                        'expectedMileage' => "Expected Mileage",
                        'tankInterlinked' => "Tank Interlinked",
                        'kalmanFilter' => 'Kalman Filter',
                        'fuelBatteryVolt' => 'Fuel Battery Volt',
                        'vehicleModel' => 'Vehicle Model',
                        'rigMode' => 'Rig Mode',
                        'fuelMode' => 'Fuel Mode',
                        'isAc' => 'AC',

                    );
                    $updated_Value  = json_decode($refDataJson, true);
                    // $oldJsonValue   = json_decode($refVehicle,true);
                    foreach ($updated_Value as $update_Key => $update_Value) {
                        try {
                            if (isset($vehicleRefData[$update_Key])) {
                                if ($vehicleRefData[$update_Key] != $update_Value) {
                                    if (isset($mapping_Array[$update_Key])) {
                                        $devices = Arr::add($devices, $mapping_Array[$update_Key], $vehicleRefData[$update_Key]);
                                        $devicestypes = Arr::add($devicestypes, $mapping_Array[$update_Key], $updated_Value[$update_Key]);
                                    }
                                }
                            } else {
                                $devices = Arr::add($devices, $mapping_Array[$update_Key], $update_Value);
                                $devicestypes = Arr::add($devicestypes, $mapping_Array[$update_Key], $updated_Value[$update_Key]);
                            }
                        } catch (\Exception $e) {
                            $devices = Arr::add($devices, $mapping_Array[$update_Key], $update_Value);
                            $devicestypes = Arr::add($devicestypes, $mapping_Array[$update_Key], $updated_Value[$update_Key]);
                            logger('---Exception-----error in old and new ref data------> ' . $e->getMessage() . '--line-->' . $e->getLine());
                        }
                    }
                    try {
                        $oldDiffRefData = array_diff($oldData, $newDate);
                        $newDiffRefDate = array_diff($newDate, $oldData);

                        $mysqlDetails = [
                            'userIpAddress' => session()->get('userIP'),
                            'userName' => $username,
                            'type' => config()->get('constant.vehicle'),
                            'name' => $vehicleId,
                            'status' => config()->get('constant.updated'),
                            'oldData' => json_encode($oldDiffRefData),
                            'newData' => json_encode($newDiffRefDate)
                        ];
                        AuditNewController::updateAudit($mysqlDetails);

                        // VdmVehicleController::DbEntry($status, 'AuditVehicle', $fcode);
                    } catch (Exception $e) {
                        logger('Exception_' . $vehicleId . ' error --->' . $e->getMessage() . ' line--->' . $e);
                    }
                    if ($ownership == 'OWN') {
                        $gettingMail    =   $redis->hget('H_Franchise', $fcode);
                        $emailKeys      =   'email2';
                    } else {
                        $gettingMail    =   $redis->hget('H_DealerDetails_' . $fcode, $ownership);
                        $emailKeys      =   'email';
                    }
                    $gettingMail = json_decode($gettingMail, true);

                    try {

                        session()->put('email', $gettingMail[$emailKeys]);
                        $emailFcode = $redis->hget('H_Franchise', $fcode);
                        $emailFile = json_decode($emailFcode, true);
                        $email1 = $emailFile['email2'];
                        if ($own != 'OWN') {
                            $emails = array(session()->pull('email'), $email1);
                        } else {
                            $emails = array($email1);
                        }
                        $titles = ['Field Name', 'Old Data', 'New Data'];
                        $updatefile = null;
                        $updatefile = Excel::download(new FormUpdateExport($titles, $devices, $devicestypes), $vehicleId . '.xlsx');
                        try {
                            $s3 = app('aws')->createClient('s3', [
                                'endpoint' => config()->get('constant.endpoint'),
                            ]);
                            $s3->putObject(array(
                                'Bucket' => config()->get('constant.bucket'),
                                'Key' => "mail/" . $vehicleId . ".xls",
                                'SourceFile' => $updatefile,
                                'ACL' => 'public-read'
                            ));
                            // DocumentNotifier:Vehicle:7659DWJ:SMP:OWN:bala@gmail.com
                            $redis->set("DocumentNotifier:Vehicle:" . $vehicleId . ":" . $fcode . ":" . $own . ":" . $email1, "yes");
                            $redis->expire("DocumentNotifier:Vehicle:" . $vehicleId . ":" . $fcode . ":" . $own . ":" . $email1, "30");
                        } catch (Exception $e) {
                            logger('upload doc Exception -->' . $e->getMessage() . '--line-->' . $e->getLine());
                        }
                    } catch (Exception $e) {
                        logger('Mail Exception -->' . $e->getMessage() . '--line-->' . $e->getLine());
                    }
                }
                //ram-vehicleExpiry
                $redis = Redis::connection();
                $parameters = 'fcode=' . $fcode . '&expiryDate=' . $vehicleExpiry . '&vehicleId=' . $vehicleId;

                //TODO - remove ..this is just for testing
                // $ipaddress = 'localhost';
                $ipaddress = $redis->get('ipaddress');
                $url = 'http://' . $ipaddress . ':9000/getVehicleExpiryDetailsUpdate?' . $parameters;
                $url = htmlspecialchars_decode($url);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 3);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                $response = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
                //
                return redirect()->to('VdmVehicleScan' . $vehicleId);
                // $orgLis = [];
                // return view('vdm.vehicles.vehicleScan')->with('vehicleList', $orgLis);
                //            return VdmVehicleController::edit($vehicleId);
            }
        } catch (\Exception $e) {
            $username = auth()->id();
            $vehicleId = $id;
            logger(sprintf("EXCEPTIONBYUSER_" . $username . " file --> %s - line -->" . $e->getLine() . '------exception----------> ' . $e->getMessage(), __FILE__));
            return redirect()->to('VdmVehicleScan' . $vehicleId);
        }
    }


    public function update1()
    {


        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
        $rules = array(
            'deviceId' => 'required|alpha_dash',
            'vehicleId' => 'required|alpha_dash',
            'VehicleName' => 'required',
            'regNo' => 'required',
            'vehicleType' => 'required',
            'oprName' => 'required',
            'deviceModel' => 'required',
            'odoDistance' => 'required',
            'gpsSimNo' => 'required'

        );
        $validator = validator()->make(request()->all(), $rules);
        $redis = Redis::connection();
        $vehicleId = request()->get('vehicleId');
        $vehicleIdOld = request()->get('vehicleIdOld');
        $deviceId = request()->get('deviceId');
        $deviceIdOld = request()->get('deviceIdOld');
        $vehicleIdCheck = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
        $deviceidCheck = $redis->sismember('S_Device_' . $fcode, $deviceId);



        if ($validator->fails()) {
            return back()->withErrors($validator);
        } else {
            // store

            $shortName = request()->get('VehicleName');
            $regNo = request()->get('regNo');
            $vehicleMake = request()->get('vehicleMake');
            $vehicleType = request()->get('vehicleType');
            $oprName = request()->get('oprName');
            $mobileNo = request()->get('mobileNo');
            $vehicleExpiry = request()->get('vehicleExpiry');
            $overSpeedLimit = request()->get('overSpeedLimit');
            $overSpeedLimit = !empty($overSpeedLimit) ? $overSpeedLimit : '60';
            $deviceModel = request()->get('deviceModel');
            $odoDistance = request()->get('odoDistance');
            $driverName = request()->get('driverName');
            $driverMobile = request()->get('driverMobile');
            $gpsSimNo = request()->get('gpsSimNo');
            $email = request()->get('email');
            $sendGeoFenceSMS = request()->get('sendGeoFenceSMS');
            $morningTripStartTime = request()->get('morningTripStartTime');
            $eveningTripStartTime = request()->get('eveningTripStartTime');
            $fuel = request()->get('fuel');
            $isRfid = request()->get('isRfid');
            $rfidType = request()->get('rfidType');
            $orgId = request()->get('orgId');
            $altShortName = request()->get('altShortName');
            $parkingAlert = request()->get('parkingAlert');
            $safetyParking = request()->get('safetyParking');
            $countryTimezone = request()->get('countryTimezone');


            $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleIdOld);
            $vehicleRefData = json_decode($vehicleRefData, true);
            $licenceissuedDate = isset($vehicleRefData['licenceissuedDate']) ? $vehicleRefData['licenceissuedDate'] : '';
            $onboardDate = isset($vehicleRefData['onboardDate']) ? $vehicleRefData['onboardDate'] : '';
            $VolIg = isset($vehicleRefData['VolIg']) ? $vehicleRefData['VolIg'] : '';
            $batteryvolt = isset($vehicleRefData['batteryvolt']) ? $vehicleRefData['batteryvolt'] : '';
            $ac = isset($vehicleRefData['ac']) ? $vehicleRefData['ac'] : '';
            $acVolt = isset($vehicleRefData['acVolt']) ? $vehicleRefData['acVolt'] : '';
            $distManipulationPer = isset($vehicleRefData['DistManipulationPer']) ? $vehicleRefData['DistManipulationPer'] : '';
            $assetTracker = isset($vehicleRefData['assetTracker']) ? $vehicleRefData['assetTracker'] : 'no';
            $Licence = request()->get('Licence1');
            $Licence = !empty($Licence) ? $Licence : 'Advance';
            $descriptionStatus = request()->get('descriptionStatus');
            $descriptionStatus = !empty($descriptionStatus) ? $descriptionStatus : '';
            $vehicleMode = isset($vehicleRefData['vehicleMode']) ? $vehicleRefData['vehicleMode'] : '';

            $mintemp = request()->get('mintemp');
            $mintemp = !empty($mintemp) ? $mintemp : '';
            $maxtemp = request()->get('maxtemp');
            $maxtemp = !empty($maxtemp) ? $maxtemp : '';
            $Payment_Mode = request()->get('Payment_Mode1');
            $Payment_Mode = !empty($Payment_Mode) ? $Payment_Mode : 'Monthly';
            $communicatingPortNo = isset($vehicleRefData['communicatingPortNo']) ? $vehicleRefData['communicatingPortNo'] : '';
            $defaultMileage = isset($vehicleRefData['defaultMileage']) ? $vehicleRefData['defaultMileage'] : 'no';
            $expectedMileage = isset($vehicleRefData['expectedMileage']) ? $vehicleRefData['expectedMileage'] : '0';
            $tankInterlinked = isset($vehicleRefData['tankInterlinked']) ? $vehicleRefData['tankInterlinked'] : 'no';


            $v = idate("d");
            //            $paymentType=request()->get ( 'paymentType' );
            $paymentType = 'yearly';
            if ($paymentType == 'halfyearly') {
                $paymentmonth = 6;
            } elseif ($paymentType == 'yearly') {
                $paymentmonth = 11;
            }
            if ($v > 15) {
                $paymentmonth = $paymentmonth + 1;
            }
            for ($i = 1; $i <= $paymentmonth; $i++) {

                $new_date = date('F Y', strtotime("+$i month"));
                $new_date2 = date('FY', strtotime("$i month"));
            }
            $new_date1 = date('F d Y', strtotime("+0 month"));
            if (session()->get('cur') == 'dealer') {
                $ownership = $username;
            } else if (session()->get('cur') == 'admin') {
                $ownership = 'OWN';
            }
            $refDataArr = array(
                'deviceId' => $deviceId,
                'shortName' => $shortName,
                'deviceModel' => $deviceModel,
                'regNo' => $regNo,
                'vehicleMake' => $vehicleMake,
                'vehicleType' => $vehicleType,
                'oprName' => $oprName,
                'mobileNo' => $mobileNo,
                'vehicleExpiry' => $vehicleExpiry,
                'overSpeedLimit' => $overSpeedLimit,
                'odoDistance' => $odoDistance,
                'driverName' => $driverName,
                'driverMobile' => $driverMobile,
                'gpsSimNo' => $gpsSimNo,
                'email' => $email,
                'sendGeoFenceSMS' => $sendGeoFenceSMS,
                'morningTripStartTime' => $morningTripStartTime,
                'eveningTripStartTime' => $eveningTripStartTime,
                'orgId' => $orgId,
                'parkingAlert' => $parkingAlert,
                'altShortName' => $altShortName,
                'date' => $new_date1,
                'paymentType' => $paymentType,
                'expiredPeriod' => $new_date,
                'fuel' => $fuel,
                'isRfid' => $isRfid,
                'rfidType' => $rfidType,
                'OWN' => $ownership,
                'Licence' => $Licence,
                'Payment_Mode' => $Payment_Mode,
                'descriptionStatus' => $descriptionStatus,
                'mintemp' => $mintemp,
                'maxtemp' => $maxtemp,
                'safetyParking' => $safetyParking,
                'licenceissuedDate' => $licenceissuedDate,
                'onboardDate' => $onboardDate,
                'VolIg' => $VolIg,
                'batteryvolt' => $batteryvolt,
                'ac' => $ac,
                'acVolt' => $acVolt,
                'DistManipulationPer' => $distManipulationPer,
                'assetTracker' => $assetTracker,
                'communicatingPortNo' => $communicatingPortNo,
                'vehicleMode' => $vehicleMode,
                'country' => $countryTimezone,
                'defaultMileage' => $defaultMileage,
                'expectedMileage' => $expectedMileage,
                'tankInterlinked' => $tankInterlinked,
            );

            VdmVehicleController::destroy($vehicleIdOld);
            $refDataJson = json_encode($refDataArr);

            // H_RefData
            $expireData = $redis->hget('H_Expire_' . $fcode, $new_date2);
            $redis->hdel('H_RefData_' . $fcode, $vehicleIdOld);
            $redis->hset('H_RefData_' . $fcode, $vehicleId, $refDataJson);
            $pub = $redis->PUBLISH('sms:topicNetty', $deviceId);

            $cpyDeviceSet = 'S_Device_' . $fcode;
            $redis->srem($cpyDeviceSet, $deviceIdOld);
            $redis->sadd($cpyDeviceSet, $deviceId);
            $redis->hdel($vehicleDeviceMapId, $vehicleIdOld, $deviceIdOld);
            $redis->hmset($vehicleDeviceMapId, $vehicleId, $deviceId, $deviceId, $vehicleId);

            //this is for security check                                    
            $redis->sadd('S_Vehicles_' . $fcode, $vehicleId);

            $redis->hset('H_Device_Cpy_Map', $deviceId, $fcode);
            $redis->sadd('S_Vehicles_' . $orgId . '_' . $fcode, $vehicleId);
            if ($expireData == null) {
                $redis->hdel('H_Expire_' . $fcode, $new_date2, $vehicleIdOld);
                $redis->hset('H_Expire_' . $fcode, $new_date2, $vehicleId);
            } else {
                $redis->hdel('H_Expire_' . $fcode, $new_date2, $expireData . ',' . $vehicleIdOld);
                $redis->hset('H_Expire_' . $fcode, $new_date2, $expireData . ',' . $vehicleId);
            }

            $time = microtime(true);
            // /*latitude,longitude,speed,alert,date,distanceCovered,direction,position,status,odoDistance,msgType,insideGeoFence
            //     13.104870,80.303138,0,N,$time,0.0,N,P,ON,$odoDistance,S,N
            //     13.04523,80.200222,0,N,0,0.0,null,null,null,0.0,null,N vehicleId=Prasanna_Amaze
            // */
            $redis->sadd('S_Organisation_Route_' . $orgId . '_' . $fcode, $shortName);
            $time = round($time * 1000);


            if (session()->get('cur') == 'dealer') {
                $redis->srem('S_Vehicles_Dealer_' . $username . '_' . $fcode, $vehicleIdOld);
                $redis->sadd('S_Vehicles_Dealer_' . $username . '_' . $fcode, $vehicleId);
                $details = $redis->hget('H_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceIdOld);
                $valueData = json_decode($details, true);
                //$deviceid1=$valueData['deviceid'];
                $setProData = array(
                    'deviceid' => $deviceId,
                    'deviceidtype' => $deviceModel,
                );
                $jsonProData = json_encode($setProData);
                $proRem = $redis->hdel('H_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceIdOld);
                $proSet = $redis->hset('H_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId, $jsonProData);
                $sProRem = $redis->srem('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceIdOld);
                $sProSet = $redis->sadd('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId);
            } else if (session()->get('cur') == 'admin') {
                $redis->srem('S_Vehicles_Admin_' . $fcode, $vehicleIdOld);
                $redis->sadd('S_Vehicles_Admin_' . $fcode, $vehicleId);
            }
            $tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,' . $odoDistance . ',S,N';
            $LatLong = $redis->hget('H_Franchise_LatLong', $fcode);
            if ($LatLong != null) {
                $data_arr = explode(":", $LatLong);
                $latitude = $data_arr[0];
                $longitude = $data_arr[1];
                $tmpPositon =  $latitude . ',' . $longitude . ',0,N,' . $time . ',0.0,N,N,ON,' . $odoDistance . ',S,N';
                $redis->sadd('S_NoDataVehicle', $vehicleId . ',' . $fcode);
                $redis->hset('H_NoDataVehicleProcess_Time', $vehicleId . ',' . $fcode, $time);
            }
            $redis->hdel('H_ProData_' . $fcode, $vehicleId, $vehicleIdOld);
            $redis->hset('H_ProData_' . $fcode, $vehicleId, $tmpPositon);
            // redirect

            session()->flash('message', 'Successfully updated ' . $vehicleId . '!');
            return redirect()->to('Business');
        }
    }


    public function renameUpdate()
    {


        $vehicleId1 = request()->get('vehicleId');
        $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/';
        if (preg_match($pattern, $vehicleId1)) {
            return back()->withErrors('Vehicle ID should be in alphanumeric with _ or - Characters ' . $vehicleId1);
        }
        $vehicleId = strtoupper($vehicleId1);
        $deviceId = request()->get('deviceId');
        $vehicleId = preg_replace('/\s+/', '', $vehicleId);
        $deviceId = preg_replace('/\s+/', '', $deviceId);

        $vehicleIdOld = request()->get('vehicleIdOld');
        $deviceIdOld = request()->get('deviceIdOld');
        $expiredPeriodOld = request()->get('expiredPeriodOld');

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        ///get orgId
        $detailsR = $redis->hget('H_RefData_' . $fcode, $vehicleIdOld);
        $refDataFromDBR = json_decode($detailsR, true);
        $expiredPeriod = isset($refDataFromDBR['expiredPeriod']) ? $refDataFromDBR['expiredPeriod'] : 'NotAvailabe';

        $renameData = [
            'deviceId' => $deviceId,
            'deviceIdOld' => $deviceIdOld,
            'vehicleId' => $vehicleId,
            'vehicleIdOld' => $vehicleIdOld,
            'expiredPeriodOld' => $expiredPeriodOld,
        ];
        if ($vehicleId == $deviceIdOld) {
            return back()->withErrors('Device Id and VehicleId should not be same !');
        }

        ///ram vehicleIdCheck
        $vehicleIdcheck = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
        $tempDeviceCheck = $redis->hget('H_Device_Cpy_Map', $vehicleId);

        if ($vehicleIdcheck == 1 || $tempDeviceCheck != null) {
            return back()->withErrors('Asset Id/Vehicle Id already exists !');
        } else {
            $result = VdmVehicleController::vehicleIdMig($vehicleId, $vehicleIdOld, $expiredPeriod, "yes");
            if ($vehicleId == $result) {

                try {
                    $mysqlDetails = [
                        'userIpAddress' => session()->get('userIP'),
                        'userName' => $username,
                        'type' => config()->get('constant.vehicle'),
                        'name' => $vehicleId,
                        'status' => config()->get('constant.renamed'),
                        'oldData' => json_encode([
                            'vehicleId' => $vehicleIdOld,
                            'expiredPeriod' => $expiredPeriodOld,
                        ]),
                        'newData' => json_encode([
                            'vehicleId' => $vehicleId,
                            'expiredPeriodOld' => $expiredPeriod,
                        ])
                    ];
                    AuditNewController::updateAudit($mysqlDetails);
                } catch (Exception $e) {
                    logger('Exception_' . $vehicleId . ' error --->' . $e->getMessage() . ' line--->' . $e->getLine());
                }
                session()->flash('message', 'Asset Id/Vehicle Id has been renamed successfully. !');
                return redirect()->to('VdmVehicleScan' . $vehicleId);
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id            
     * @return Response
     */
    public function destroy($id)
    {

        $username = auth()->id();
        $redis = Redis::connection();

        $vehicleId = $id;
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
        $cpyDeviceSet = 'S_Device_' . $fcode;

        $deviceId = $redis->hget($vehicleDeviceMapId, $vehicleId);

        $redis->srem($cpyDeviceSet, $deviceId);

        $refDataJson1 = $redis->hget('H_RefData_' . $fcode, $vehicleId); //ram
        $refDataJson1 = json_decode($refDataJson1, true);

        $orgId = $refDataJson1['orgId'] ?? "Default";

        $redis->hdel('H_RefData_' . $fcode, $vehicleId);

        $redis->hdel('H_Device_Cpy_Map', $deviceId);

        $redisVehicleId = $redis->hget($vehicleDeviceMapId, $deviceId);

        $redis->hdel($vehicleDeviceMapId, $redisVehicleId);

        $redis->hdel($vehicleDeviceMapId, $deviceId);

        $redis->srem('S_Vehicles_' . $fcode, $redisVehicleId);

        $redis->srem('S_Vehicles_' . $orgId . '_' . $fcode, $vehicleId);

        $groupList = $redis->smembers('S_Groups_' . $fcode);

        foreach ($groupList as $group) {
            $redis->srem($group, $redisVehicleId);
        }



        $redis->srem('S_Vehicles_Dealer_' . session()->get('page') . '_' . $fcode, $vehicleId);
        $redis->srem('S_Vehicles_Dealer_' . $username . '_' . $fcode, $vehicleId);

        $redis->srem('S_Vehicles_Admin_' . $fcode, $vehicleId);




        session()->flash('message', 'Successfully deleted ' . $deviceId . '!');
        return redirect()->to('vdmVehicles');
    }


    public function multi()
    {

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $orgList = $redis->smembers('S_Organisations_' . $fcode);



        $orgArr = array();
        foreach ($orgList as $org) {
            $orgArr = Arr::add($orgArr, $org, $org);
        }
        $orgList = $orgArr;

        return view('vdm.vehicles.multi')->with('orgList', $orgList);
    }



    public function moveDealer()
    {

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $vehicleList = request()->get('vehicleList');
        $dealerId = request()->get('dealerId');
        if ($vehicleList !== null) {
            foreach ($vehicleList as $key => $value) {
                $redis->sadd('S_Vehicles_Dealer_' . $dealerId . '_' . $fcode, $value);
                $redis->srem('S_Vehicles_Admin_' . $fcode, $value);
            }
        }


        // $redis->sadd('S_Vehicles_Dealer_'.$ownerShip.'_'.$fcode,$vehicleId);
        // $redis->srem('S_Vehicles_Admin_'.$fcode,$vehicleId);


        return redirect()->to('vdmVehicles');

        // return view ( 'vdm.vehicles.dealerSearch' )->with('dealerId',$dealerId);       

    }

    public function dealerSearchModel()
    {

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $dealerList = $redis->smembers('S_Dealers_' . $fcode);
        sort($dealerList);
        $dealerArr = array();
        foreach ($dealerList as $dealer) {
            $dealerArr = Arr::add($dealerArr, $dealer, $dealer);
        }
        return view('vdm.vehicles.dealerSearchModal', [
            'dealerArr' => $dealerArr
        ]);
    }

    public function dealerSearch()
    {

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $dealerList = $redis->smembers('S_Dealers_' . $fcode);
        sort($dealerList);
        $dealerArr = array();
        foreach ($dealerList as $dealer) {
            $dealerArr = Arr::add($dealerArr, $dealer, $dealer);
        }
        return view('vdm.vehicles.dealerSearch', ['dealerArr' => $dealerArr]);
    }


    public function findDealerList()
    {

        $redis = Redis::connection();
        $username = request()->get('dealerId');
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        if ($username == null) {
            $username = session()->get('page');
        } else {
            session()->put('page', $username);
        }

        try {
            // $user = FirebaseController::signinWithUserId($username);
		    $user = User::whereRaw('BINARY username = ?', [$username])->firstOrFail();

            Auth::login($user);
        } catch (\Exception $e) {
            logger($e);
            return back()->with([
                'switchErr' => $e->getMessage(),
                'SwitchLoginTrigger' => "true"
            ]);
        }
        session()->put('curSwt', 'switch');
        $detailJson = $redis->hget('H_DealerDetails_' . $fcode, $username);
        $detail = json_decode($detailJson, true);
        $website  = isset($detail['website']) ? $detail['website'] : "";
        $domain = HomeController::get_domain($website);
        $curdomin = $domain == 'gpsvts.net' ? 'gps' : 'notgps';
        session()->put('curdomin', $curdomin);

        return redirect()->to('Business');
    }




    //show stops
    public function stops($id, $demo)
    {
        $redis = Redis::connection();
        $ipaddress = $redis->get('ipaddress');

        $username = auth()->id();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $id);
        $vehicleRefData = json_decode($vehicleRefData, true);

        $orgId = $vehicleRefData['orgId'];
        $type = 0;
        $url = 'http://' . $ipaddress . ':9000/getSuggestedStopsForVechiles?vehicleId=' . $id . '&fcode=' . $fcode . '&orgcode=' . $orgId . '&type=' . $type . '&demo=' . $demo;
        $url = htmlspecialchars_decode($url);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // Include header in result? (0 = yes, 1 = no)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $sugStop = json_decode($response, true);
        if (!$sugStop['error'] == null) {
            //return view ( 'vdm.vehicles.stopgenerate' )->with('vehicleId',$id)->with('demo',$demo);
        }
        $value = $sugStop['suggestedStop'];

        $address = array();
        try {
            foreach ($value as $org => $geoAddress) {
                $rowId1 = json_decode($geoAddress, true);
                $t = 0;

                if (isset($rowId1['time'])) {
                    if ($rowId1['time'] != null &&  $rowId1['time'] != '') {
                        $address = Arr::add($address, $org, $rowId1['geoAddress'] . ' ' . $rowId1['time']);
                    }
                } else {
                    $address = Arr::add($address, $org, $rowId1['geoAddress']);
                }
            }
        } catch (\Exception $e) {
            logger('---------Error------->' . $e->getMessage() . '----line---->' . $e->getLine());
            return view('vdm.vehicles.stopgenerate')->with('vehicleId', $id)->with('demo', $demo);
        }
        $sugStop = $address;
        return view('vdm.vehicles.showStops')->with('sugStop', $sugStop)->with('vehicleId', $id);
    }


    public function removeStop($id, $demo)
    {

        $redis = Redis::connection();
        $ipaddress = $redis->get('ipaddress');

        $username = auth()->id();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $id);
        $vehicleRefData = json_decode($vehicleRefData, true);

        $orgId = $vehicleRefData['orgId'];
        $routeNo = $vehicleRefData['shortName'];

        $dbIpaddress = $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
        // $servername = $dbIpaddress;

        if (strlen($dbIpaddress) > 0 && strlen(trim($dbIpaddress) == 0)) {
            return 'Ipaddress Failed !!!';
        }

        $usernamedb = "root";
        $password = "#vamo123";
        $dbname = $fcode;
        $servername = $dbIpaddress;

        $suggeststop = $redis->LRANGE('L_Suggest_' . $routeNo . '_' . $orgId . '_' . $fcode, 0, -1);
        $suggeststop1 = $redis->LRANGE('L_Suggest_Alt' . $routeNo . '_' . $orgId . '_' . $fcode, 0, -1);
        if (!$suggeststop == null) {
            if ($demo == 'normal') {
                $arraystop = $redis->lrange('L_Suggest_' . $routeNo . '_' . $orgId . '_' . $fcode, 0, -1);
                foreach ($arraystop as $org => $geoAddress) {
                    $redis->hdel('H_Bus_Stops_' . $orgId . '_' . $fcode, $routeNo . ':stop' . $org);
                }
                $redis->del('L_Suggest_' . $routeNo . '_' . $orgId . '_' . $fcode);
                $redis->hdel('H_Stopseq_' . $orgId . '_' . $fcode, $routeNo . ':morning');
                $redis->hdel('H_Stopseq_' . $orgId . '_' . $fcode, $routeNo . ':evening');
                $redis->srem('S_Organisation_Route_' . $orgId . '_' . $fcode, $routeNo);
                $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);

                if (!$conn) {
                    die('Could not connect: ' . mysqli_connect_error());
                } else {
                    $query = "SELECT * FROM StudentSmsDetails where route='" . $routeNo . "'";
                    $results = mysqli_query($conn, $query);
                    while ($row = mysqli_fetch_array($results)) {
                        $mobileNumber = $row['mobileNumber'];
                        $rte = strtoupper($routeNo);
                        if (strpos($rte, 'ALT') !== false) {
                            $redis->hdel('H_Mblno_Alt_' . $orgId . '_' . $fcode, $mobileNumber);
                        } else {
                            $redis->hdel('H_Mblno_' . $orgId . '_' . $fcode, $mobileNumber);
                        }
                    }
                    $DeleteQuery = "DELETE FROM StudentSmsDetails where route='" . $routeNo . "'";
                    $conn->query($DeleteQuery);
                }
                $conn->close();
                //HDEL myhash
                return redirect()->to('vdmVehicles');
            }
        }
        if (!$suggeststop1 == null) {
            if ($demo == 'alternate') {
                $arraystop = $redis->lrange('L_Suggest_Alt' . $routeNo . '_' . $orgId . '_' . $fcode, 0, -1);
                foreach ($arraystop as $org => $geoAddress) {
                    $redis->hdel('H_Bus_Stops_' . $orgId . '_' . $fcode, 'Alt' . $routeNo . ':stop' . $org);
                }
                $redis->del('L_Suggest_Alt' . $routeNo . '_' . $orgId . '_' . $fcode);
                $redis->hdel('H_Stopseq_' . $orgId . '_' . $fcode, 'Alt' . $routeNo . ':morning');
                $redis->hdel('H_Stopseq_' . $orgId . '_' . $fcode, 'Alt' . $routeNo . ':evening');
                //HDEL myhash
                $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);

                if (!$conn) {
                    die('Could not connect: ' . mysqli_connect_error());
                } else {
                    $query = "SELECT * FROM StudentSmsDetails where route='" . $routeNo . "'";
                    $results = mysqli_query($conn, $query);
                    while ($row = mysqli_fetch_array($results)) {
                        $mobileNumber = $row['mobileNumber'];
                        $rte = strtoupper($routeNo);
                        if (strpos($rte, 'ALT') !== false) {
                            $redis->hdel('H_Mblno_Alt_' . $orgId . '_' . $fcode, $mobileNumber);
                        } else {
                            $redis->hdel('H_Mblno_' . $orgId . '_' . $fcode, $mobileNumber);
                        }
                    }
                    $DeleteQuery = "DELETE FROM StudentSmsDetails where route='" . $routeNo . "'";
                    $conn->query($DeleteQuery);
                }
                $conn->close();
                session()->flash('alert-class', "alert-success");
                session()->flash('message', $id . " vehicle update successfully ");
                return redirect()->to('vdmVehicles');
            }
        } else {
            session()->flash('alert-class', "alert-success");
            session()->flash('message', $id . " vehicle update successfully ");
            return redirect()->to('vdmVehicles');
        }
        // L_Suggest_$routeNo_$orgId_$fcode
        //H_Stopseq_$orgId_$fcode $routeNo:morning
        //H_Stopseq_$orgId_$fcode $routeNo:evening
        // return view ( 'vdm.vehicles.showStops' )->with('sugStop',$sugStop)->with('vehicleId',$id);       

    }


    public function stops1($id, $demo)
    {

        $redis = Redis::connection();
        $ipaddress = $redis->get('ipaddress');

        $username = auth()->id();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $id);
        $vehicleRefData = json_decode($vehicleRefData, true);

        $orgId = $vehicleRefData['orgId'];
        $type = 0;
        $url = 'http://' . $ipaddress . ':9000/getSuggestedStopsForVechiles?vehicleId=' . $id . '&fcode=' . $fcode . '&orgcode=' . $orgId . '&type=' . $type . '&demo=' . $demo;
        $url = htmlspecialchars_decode($url);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // Include header in result? (0 = yes, 1 = no)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $sugStop = json_decode($response, true);
        if (!$sugStop['error'] == null) {

            //return view ( 'vdm.vehicles.stopgenerate' )->with('vehicleId',$id)->with('demo',$demo);

        }
        // var_dump($sugStop);
        $value = $sugStop['suggestedStop'];
        //  var_dump($value);

        $address = array();
        try {


            foreach ($value as $org => $geoAddress) {
                $rowId1 = json_decode($geoAddress, true);
                $t = 0;
                foreach ($rowId1 as $org1 => $rowId2) {
                    if ($t == 1) {
                        $address = Arr::add($address, $org, $rowId2 . ' ' . $rowId1['time']);
                    }

                    $t++;
                }
            }
        } catch (\Exception $e) {
            return view('vdm.vehicles.stopgenerate')->with('vehicleId', $id)->with('demo', $demo);
        }
        $sugStop = $address;
        session()->flash('alert-class', "alert-success");
        session()->flash('message', "Update successfully!");
        return view('vdm.vehicles.dealerSearch')->with('sugStop', $sugStop)->with('vehicleId', $id);
    }


    public function removeStop1($id, $demo)
    {

        $redis = Redis::connection();
        $ipaddress = $redis->get('ipaddress');

        $username = auth()->id();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $id);
        $vehicleRefData = json_decode($vehicleRefData, true);

        $orgId = $vehicleRefData['orgId'];
        $routeNo = $vehicleRefData['shortName'];

        $suggeststop = $redis->LRANGE('L_Suggest_' . $routeNo . '_' . $orgId . '_' . $fcode, 0, -1);
        $suggeststop1 = $redis->LRANGE('L_Suggest_Alt' . $routeNo . '_' . $orgId . '_' . $fcode, 0, -1);
        if (!$suggeststop == null) {
            if ($demo == 'normal') {



                $arraystop = $redis->lrange('L_Suggest_' . $routeNo . '_' . $orgId . '_' . $fcode, 0, -1);
                foreach ($arraystop as $org => $geoAddress) {
                    $redis->hdel('H_Bus_Stops_' . $orgId . '_' . $fcode, $routeNo . ':stop' . $org);
                }
                $redis->del('L_Suggest_' . $routeNo . '_' . $orgId . '_' . $fcode);
                $redis->hdel('H_Stopseq_' . $orgId . '_' . $fcode, $routeNo . ':morning');
                $redis->hdel('H_Stopseq_' . $orgId . '_' . $fcode, $routeNo . ':evening');
                $redis->srem('S_Organisation_Route_' . $orgId . '_' . $fcode, $routeNo);



                //HDEL myhash
                return redirect()->to('vdmVehicles/dealerSearch');
            }
        }
        if (!$suggeststop1 == null) {
            if ($demo == 'alternate') {
                $arraystop = $redis->lrange('L_Suggest_Alt' . $routeNo . '_' . $orgId . '_' . $fcode, 0, -1);
                foreach ($arraystop as $org => $geoAddress) {
                    $redis->hdel('H_Bus_Stops_' . $orgId . '_' . $fcode, 'Alt' . $routeNo . ':stop' . $org);
                }
                $redis->del('L_Suggest_Alt' . $routeNo . '_' . $orgId . '_' . $fcode);
                $redis->hdel('H_Stopseq_' . $orgId . '_' . $fcode, 'Alt' . $routeNo . ':morning');
                $redis->hdel('H_Stopseq_' . $orgId . '_' . $fcode, 'Alt' . $routeNo . ':evening');

                //HDEL myhash
                return redirect()->to('vdmVehicles/dealerSearch');
            }
        } else {
            return redirect()->to('vdmVehicles/dealerSearch');
        }
    }



    public function generate()
    {

        $vehicleId = request()->get('vehicleId');
        $type = request()->get('type');
        $demo = request()->get('demo');
        $rules = array(
            'date' => 'required|date:dd-MM-yyyy|',
            'mst' => 'required|date_format:H:i',
            'met' => 'required|date_format:H:i',
            'est' => 'required|date_format:H:i',
            'eet' => 'required|date_format:H:i',
            'type' => 'required'

        );
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            return redirect()->to('vdmVehicles/stops/' . $vehicleId . '/' . $demo)->withErrors($validator);
        } else {
            $date = request()->get('date');
            $mst = request()->get('mst');
            $met = request()->get('met');
            $est = request()->get('est');
            $eet = request()->get('eet');
            $username = auth()->id();
            $redis = Redis::connection();
            $ipaddress = $redis->get('ipaddress');
            $parameters = '?userId=' . $username;
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
            $vehicleRefData = json_decode($vehicleRefData, true);

            $orgId = $vehicleRefData['orgId'];
            $parameters = $parameters . '&vehicleId=' . $vehicleId . '&fcode=' . $fcode . '&orgcode=' . $orgId . '&presentDay=' . $date . '&mST=' . $mst . '&mET=' . $met . '&eST=' . $est . '&eET=' . $eet . '&type=' . $type . '&demo=' . $demo;
            $url = 'http://' . $ipaddress . ':9000/getSuggestedStopsForVechiles?' . $parameters;
            $url = htmlspecialchars_decode($url);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            // Include header in result? (0 = yes, 1 = no)
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 150);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            $response = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            $sugStop1 = json_decode($response, true);
            if (!$sugStop1['error'] == null) {
                session()->flash('alert-class', "alert-danger");
                session()->flash('message', $sugStop1['error']);
                return back()->withInput();
                return view('vdm.vehicles.stopgenerate')->with('vehicleId', $vehicleId)->with('demo', $demo);
            }

            $url = 'http://' . $ipaddress . ':9000/getSuggestedStopsForVechiles?vehicleId=' . $vehicleId . '&fcode=' . $fcode . '&orgcode=' . $orgId . '&type=' . $type . '&demo=' . $demo;
            $url = htmlspecialchars_decode($url);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            // Include header in result? (0 = yes, 1 = no)
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            $response = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            $sugStop = json_decode($response, true);
            if (!$sugStop['error'] == null) {
                session()->flash('alert-class', "alert-danger");
                session()->flash('message', $sugStop1['error']);
                return back()->withInput();
                return view('vdm.vehicles.stopgenerate')->with('vehicleId', $vehicleId)->with('demo', $demo);
            }
            $value = $sugStop['suggestedStop'];
            $address = array();
            foreach ($value as $org => $rowId) {
                $rowId1 = json_decode($rowId, true);
                $t = 0;
                foreach ($rowId1 as $org1 => $rowId2) {
                    if ($t == 1) {
                        $address = Arr::add($address, $org, $rowId2 . ' ' . $rowId1['time']);
                    }

                    $t++;
                }
            }
            $sugStop = $address;
            session()->flash('alert-class', "alert-success");
            session()->flash('message', "Update successfully!");
            return back()->withInput();

            return view('vdm.vehicles.showStops')->with('sugStop', $sugStop)->with('vehicleId', $vehicleId)->with('demo', $demo);
        }
    }

    public function storeMulti()
    {

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $vehicleDetails = request()->get('vehicleDetails');
        $orgId = request()->get('orgId');

        $redis = Redis::connection();
        $redis->set('MultiVehicle:' . $fcode, $vehicleDetails);
        $who = session()->get('cur');
        $parameters = 'key=' . 'MultiVehicle:' . $fcode . '&orgId=' . $orgId . '&who=' . $who . '&username=' . $username;

        $ipaddress = $redis->get('ipaddress');
        $url = 'http://' . $ipaddress . ':9000/addMultipleVehicles?' . $parameters;
        $url = htmlspecialchars_decode($url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // Include header in result? (0 = yes, 1 = no)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return redirect()->to('vdmVehicles');
    }
    public function getVehicleDetails()
    {
        $vehicleId = request()->get('id');
        $redis = Redis::connection();
        //$ipaddress = $redis->get('ipaddress');
        $ipaddress = '188.166.244.126';

        $ch = curl_init();
        $username = auth()->id();
        $parameters = '&userId=' . $username;
        $url = 'http://' . $ipaddress . ':9000/getSelectedVehicleLocation?vehicleId=' . $vehicleId . $parameters;
        $url = htmlspecialchars_decode($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $VehicleData = json_decode($response, true);
        $lastComm1 = $VehicleData['lastComunicationTime'];
        $lastComm = date("d-m-Y H:i:s", $lastComm1 / 1000);
        $regNo = $VehicleData['regNo'];
        $lastSeen = $VehicleData['lastSeen'];
        $driverName = $VehicleData['driverName'];
        $driverMobile = $VehicleData['driverMobile'];
        $kms = $VehicleData['distanceCovered'];
        $speed = $VehicleData['speed'];
        $position = $VehicleData['position'];
        $nearLoc = $VehicleData['address'];
        $sat = $VehicleData['gsmLevel'];
        $load = $VehicleData['loadTruck'];
        $ac = $VehicleData['ignitionStatus'];
        $cel = $VehicleData['temperature'];
        $latitude = $VehicleData['latitude'];
        $longitude = $VehicleData['longitude'];
        $maxSpeed = $VehicleData['maxSpeed'];
        $deviceVolt = $VehicleData['deviceVolt'];
        $direction = $VehicleData['direction'];
        $odoDistance = $VehicleData['odoDistance'];
        $movingTime = $VehicleData['movingTime'];
        $parkedTime = $VehicleData['parkedTime'];
        $noDataTime = $VehicleData['noDataTime'];
        $idleTime = $VehicleData['idleTime'];
        $vehicleType = $VehicleData['vehicleType'];


        if ($position == 'P') {
            $statusList = 'Parking';
            $devicestatus = 'Parked';
            $sec = $parkedTime / 1000;
            $day = floor($sec / (24 * 60 * 60));
            $hour = floor(($sec - ($day * 24 * 60 * 60)) / (60 * 60));
            $min = floor(($sec - ($day * 24 * 60 * 60) - ($hour * 60 * 60)) / 60);
            $time = ($day . 'D:' . $hour . 'H:' . $min . 'm');
        } else if ($position == 'M') {
            $statusList = 'Moving';
            $devicestatus = 'Moving';
            $sec = $movingTime / 1000;
            $day = floor($sec / (24 * 60 * 60));
            $hour = floor(($sec - ($day * 24 * 60 * 60)) / (60 * 60));
            $min = floor(($sec - ($day * 24 * 60 * 60) - ($hour * 60 * 60)) / 60);
            $time = ($day . 'D:' . $hour . 'H:' . $min . 'm');
        } else if ($position == 'S') {
            $statusList = 'Idle';
            $devicestatus = 'Idle';
            $sec = $idleTime / 1000;
            $day = floor($sec / (24 * 60 * 60));
            $hour = floor(($sec - ($day * 24 * 60 * 60)) / (60 * 60));
            $min = floor(($sec - ($day * 24 * 60 * 60) - ($hour * 60 * 60)) / 60);
            $time = ($day . 'D:' . $hour . 'H:' . $min . 'm');
        } else if ($position == 'N') {
            $statusList = 'New Device';

            $time = ('0D:00H:00m');
        } else {
            $statusList = 'NoData';
            $sec = $noDataTime / 1000;
            $day = floor($sec / (24 * 60 * 60));
            $hour = floor(($sec - ($day * 24 * 60 * 60)) / (60 * 60));
            $min = floor(($sec - ($day * 24 * 60 * 60) - ($hour * 60 * 60)) / 60);
            $time = ($day . 'D:' . $hour . 'H:' . $min . 'm');
        }
        $error = ' ';
        if (count($VehicleData) == 0) {
            $error = 'No organization available ,Please select another user';
        }

        $refDataArr = array(

            'maxSpeed' => $maxSpeed,
            'regNo' => $regNo,
            'lastComm' => $lastComm,
            'lastSeen' => $lastSeen,
            'deviceVolt' => $deviceVolt,
            'kms' => $kms,
            'speed' => $speed,
            'position' => $position,
            'nearLoc' => $nearLoc,
            'sat' => $sat,
            'direction' => $direction,
            'ac' => $ac,
            'odoDistance' => $odoDistance,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'statusList' => $statusList,
            'devicestatus' => $devicestatus,
            'time' => $time,
            'vehicleType' => $vehicleType,
            'error' => $error,
        );

        $refDataJson = json_encode($refDataArr);

        return response()->json($refDataArr);
    }

    public function calibrateGet()
    {

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $ipaddress = '128.199.159.130';
        $vehicleId = request()->get('vehicleId');
        $username = auth()->id();
        $statusVehicle = $redis->hget('H_ProData_' . $fcode, $vehicleId);
        $statusSeperate = explode(',', $statusVehicle);
        $volt = isset($statusSeperate[48]) ? $statusSeperate[48] : '0';
        $frequency = isset($statusSeperate[50]) ? $statusSeperate[50] : '0';
        $refDataArr =  [
            'volt' => $volt,
            'frequency' => $frequency
        ];
        return response()->json($refDataArr);
    }

    public function migrationUpdate()
    {
        try {
            $vehicleId1 = request()->get('vehicleId');
            $vehicleId1 = preg_replace('/[ ]{2,}|[\t]/', '', trim($vehicleId1));
            $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/';
            if (preg_match($pattern, $vehicleId1)) {
                session()->flash('message', 'Vehicle ID should be in alphanumeric with _ or - Characters' . $vehicleId1);
                return redirect()->back();
            }
            $vehicleId = strtoupper($vehicleId1);
            $deviceId = request()->get('deviceId');
            $deviceId = preg_replace('/[ ]{2,}|[\t]/', '', trim($deviceId));
            if (preg_match($pattern, $deviceId)) {
                session()->flash('message', 'Device ID should be in alphanumeric with _ or - Characters' . $deviceId);
                return redirect()->back();
            }
            $rules = array(
                'vehicleId' => 'required',
                'deviceId' => 'required',
            );
            $validator = validator()->make(request()->all(), $rules);
            if ($validator->fails()) {
                logger()->error(' VdmVehicleConrtoller update validation failed++');
                return redirect()->back()->withErrors($validator);
            }
            $vehicleId = strtoupper($vehicleId1);
            $vehicleIdOld = request()->get('vehicleIdOld');
            $deviceIdOld = request()->get('deviceIdOld');

            $expiredPeriodOld = request()->get('expiredPeriodOld');
            $username = auth()->id();
            $redis = Redis::connection();
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
            $msg = 'VehicleId/DeviceId Migration not successfully completed';
            if ($vehicleId == $deviceId) {
                session()->flash('message', 'Device Id and VehicleId should not be same !');
                return back()->withInput()->withErrors('Device Id and VehicleId should not be same !');
                // return view('vdm.vehicles.migration')->with('deviceId', $deviceId)->with('expiredPeriodOld', $expiredPeriodOld)->with('vehicleId', $vehicleId)->with('vehicleIdOld', $vehicleIdOld)->with('deviceIdOld', $deviceIdOld);
            }
            //vehicle_device_id_check
            if ($vehicleId == $vehicleIdOld && $deviceId == $deviceIdOld) {
                // session()->flash('message', 'Vehicle is not migrated. Since it has same vehicle Id and Device Id !');
                // return view('vdm.vehicles.migration')->with('deviceId', $deviceId)->with('expiredPeriodOld', $expiredPeriodOld)->with('vehicleId', $vehicleId)->with('vehicleIdOld', $vehicleIdOld)->with('deviceIdOld', $deviceIdOld);
                return back()->withInput()->withErrors('Vehicle is not migrated. Since it has same vehicle Id and Device Id !');
            } else if ($vehicleId == $vehicleIdOld && $deviceId != $deviceIdOld) {
                $deviceIdTemp = $redis->hget($vehicleDeviceMapId, $deviceId);
                if ($deviceIdTemp !== null) {
                    session()->flash('message', 'Device Id Already Present  !');
                    return redirect()->back()->withInput();
                    // return view('vdm.vehicles.migration')->with('deviceId', $deviceId)->with('expiredPeriodOld', $expiredPeriodOld)->with('vehicleId', $vehicleId)->with('vehicleIdOld', $vehicleIdOld)->with('deviceIdOld', $deviceIdOld);
                }
                $tempDeviceCheck = $redis->hget('H_Device_Cpy_Map', $deviceId);
                if ($tempDeviceCheck !== null) {
                    session()->flash('message', 'Device Id already present  !');
                    return redirect()->back()->withInput();
                    // return view('vdm.vehicles.migration')->with('deviceId', $deviceId)->with('expiredPeriodOld', $expiredPeriodOld)->with('vehicleId', $vehicleId)->with('vehicleIdOld', $vehicleIdOld)->with('deviceIdOld', $deviceIdOld);
                }
                $result = VdmVehicleController::deviceIdMig($deviceId, $deviceIdOld, $expiredPeriodOld);
                if ($result == $deviceId) {
                    $details = $redis->hget('H_RefData_' . $fcode, $vehicleId);
                    try {
                        $mysqlDetails = [
                            'userIpAddress' => session()->get('userIP'),
                            'userName' => $username,
                            'type' => config()->get('constant.vehicle'),
                            'name' => $vehicleId,
                            'status' => config()->get('constant.deviceid_migrated'),
                            'oldData' => json_encode(['deviceId' => $deviceIdOld, 'vehicleId' => $vehicleId]),
                            'newData' => json_encode(['deviceId' => $deviceId, 'vehicleId' => $vehicleId])
                        ];
                        AuditNewController::updateAudit($mysqlDetails);
                    } catch (Exception $e) {
                        logger('Exception_' . $vehicleId . ' error --->' . $e->getMessage() . ' line--->' . $e->getLine());
                    }
                    $msg = 'Device ID  successfully Migrated !';
                }
            } else if ($vehicleId != $vehicleIdOld && $deviceId == $deviceIdOld) {
                $vehicleIdTemp = $redis->hget($vehicleDeviceMapId, $vehicleId);
                $vehicleIdChk = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
                if ($vehicleIdTemp !== null || $vehicleIdChk == 1) {
                    session()->flash('message', 'Asset Id/Vehicle Id already exists ' . '!');
                    return view('vdm.vehicles.migration')->with('deviceId', $deviceId)->with('expiredPeriodOld', $expiredPeriodOld)->with('vehicleId', $vehicleId)->with('vehicleIdOld', $vehicleIdOld)->with('deviceIdOld', $deviceIdOld);
                }
                $result = VdmVehicleController::vehicleIdMig($vehicleId, $vehicleIdOld, $expiredPeriodOld, "no");
                if ($vehicleId == $result) {
                    $details = $redis->hget('H_RefData_' . $fcode, $vehicleId);

                    try {
                        $mysqlDetails = [
                            'userIpAddress' => session()->get('userIP'),
                            'userName' => $username,
                            'type' => config()->get('constant.vehicle'),
                            'name' => $vehicleId,
                            'status' => config()->get('constant.vehicleid_migrated'),
                            'oldData' => json_encode(['vehicleId' => $vehicleIdOld, 'deviceId' => $deviceId]),
                            'newData' => json_encode(['vehicleId' => $vehicleId, 'deviceId' => $deviceId])
                        ];
                        AuditNewController::updateAudit($mysqlDetails);
                    } catch (Exception $e) {
                        logger('Exception_' . $vehicleId . ' error --->' . $e->getMessage() . ' line--->' . $e->getLine());
                    }

                    $msg = 'Vehicle Id Migrated successfully !';
                }
            } else if ($vehicleId != $vehicleIdOld && $deviceId != $deviceIdOld) {
                $deviceIdTemp = $redis->hget($vehicleDeviceMapId, $deviceId);
                if ($deviceIdTemp !== null) {
                    session()->flash('message', 'Device Id Already Present ' . '!');
                    return view('vdm.vehicles.migration')->with('deviceId', $deviceId)->with('expiredPeriodOld', $expiredPeriodOld)->with('vehicleId', $vehicleId)->with('vehicleIdOld', $vehicleIdOld)->with('deviceIdOld', $deviceIdOld);
                }
                $tempDeviceCheck = $redis->hget('H_Device_Cpy_Map', $vehicleId);
                if ($tempDeviceCheck !== null) {
                    session()->flash('message', 'Asset Id/Vehicle Id already exists as Device Id');
                    return view('vdm.vehicles.migration')->with('deviceId', $deviceId)->with('expiredPeriodOld', $expiredPeriodOld)->with('vehicleId', $vehicleId)->with('vehicleIdOld', $vehicleIdOld)->with('deviceIdOld', $deviceIdOld);
                }
                $vehicleIdTemp = $redis->hget($vehicleDeviceMapId, $vehicleId);
                $vehicleIdChk = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
                if ($vehicleIdTemp !== null || $vehicleIdChk == 1) {
                    session()->flash('message', 'Asset Id/Vehicle Id already exists ' . '!');
                    return view('vdm.vehicles.migration')->with('deviceId', $deviceId)->with('expiredPeriodOld', $expiredPeriodOld)->with('vehicleId', $vehicleId)->with('vehicleIdOld', $vehicleIdOld)->with('deviceIdOld', $deviceIdOld);
                }
                $result1 = VdmVehicleController::vehicleIdMig($vehicleId, $vehicleIdOld, $expiredPeriodOld, "no");
                $result2 = VdmVehicleController::deviceIdMig($deviceId, $deviceIdOld, $expiredPeriodOld);

                if ($result1 == $vehicleId && $result2 == $deviceId) {
                    $details = $redis->hget('H_RefData_' . $fcode, $vehicleId);
                    try {
                        $mysqlDetails = [
                            'userIpAddress' => session()->get('userIP'),
                            'userName' => $username,
                            'type' => config()->get('constant.vehicle'),
                            'name' => $vehicleId,
                            'status' => config()->get('constant.vehicleid_migrated'),
                            'oldData' => json_encode(['vehicleId' => $vehicleIdOld, 'deviceId' => $deviceId]),
                            'newData' => json_encode(['vehicleId' => $vehicleId, 'deviceId' => $deviceId])
                        ];
                        AuditNewController::updateAudit($mysqlDetails);
                    } catch (Exception $e) {
                        logger('Exception_' . $vehicleId . ' error --->' . $e->getMessage() . ' line--->' . $e->getLine());
                    }
                    $msg = 'Device Id & Vehicle Id  successfully Migrated !';
                }
            }
            $emailFcode = $redis->hget('H_DealerDetails_' . $fcode, $username);
            $emailFile = json_decode($emailFcode, true);
            $email1 = $emailFile['email'];
            $emailFcode1 = $redis->hget('H_Franchise', $fcode);
            $emailFile1 = json_decode($emailFcode1, true);
            $email2 = $emailFile1['email2'];
            if (session()->get('cur') == 'dealer') {
                $emails = array($email1, $email2);
                $mailto = $username;
            } else if (session()->get('cur') == 'admin') {
                $emails = array($email2);
                $mailto = $fcode;
            }
            $response = Mail::send('emails.migrate', array(
                'Own' => $mailto, 'VehicleNew' => $vehicleId, 'Vehicle' => $vehicleIdOld,
                'Device' => $deviceIdOld, 'DeviceNew' => $deviceId
            ), function ($message) use ($emails, $mailto) {
                $message->to($emails);
                $message->subject('Migrated Vehicle - ' . $mailto);
            });
            session()->flash('message', $msg);
            return redirect()->to('VdmVehicleScan' . $vehicleId);
        } catch (\Exception $e) {
            $username = auth()->id();
            $vehicleId1 = request()->get('vehicleId');
            logger(sprintf("EXCEPTIONBYUSER_" . $username . " file --> %s - line --> " . $e->getLine() . " . '------exception----------> ' " . $e->getMessage(), __FILE__));
            return redirect()->to('VdmVehicleScan' . $vehicleId);
        }
    }
    public function deviceIdMig($deviceId, $deviceIdOld, $expiredPeriodOld)
    {

        $deviceId = preg_replace('/[ ]{2,}|[\t]/', '', trim($deviceId));
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
        $vehicleId = $redis->hget($vehicleDeviceMapId, $deviceIdOld);

        //map del/set
        $redis->hdel($vehicleDeviceMapId, $vehicleId);
        $redis->hdel($vehicleDeviceMapId, $deviceIdOld);
        $redis->hset($vehicleDeviceMapId, $vehicleId, $deviceId);
        $redis->hset($vehicleDeviceMapId, $deviceId, $vehicleId);
        //removeing from device list
        $redis->hdel('H_Device_Cpy_Map', $deviceIdOld);
        $redis->hset('H_Device_Cpy_Map', $deviceId, $fcode);
        $cpyDeviceSet = 'S_Device_' . $fcode;
        $redis->srem($cpyDeviceSet, $deviceIdOld);
        $redis->sadd($cpyDeviceSet, $deviceId);
        //refdata device Id change
        $refDataJson1 = $redis->hget('H_RefData_' . $fcode, $vehicleId);
        $refDataJson1 = json_decode($refDataJson1, true);
        $shortName1 = isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : '';
        $shortName2 = preg_replace('/\s+/', '', $shortName1);
        $shortName = strtoupper($shortName2);
        $gpsSimNo = isset($refDataJson1['gpsSimNo']) ? $refDataJson1['gpsSimNo'] : '';
        $orgId = isset($refDataJson1['orgId']) ? $refDataJson1['orgId'] : 'DEFAULT';
        $orgId1 = strtoupper($orgId);
        $deviceModel = isset($refDataJson1['deviceModel']) ? $refDataJson1['deviceModel'] : '';
        if ($deviceModel == 'GV05') {
            $redis->hdel('H_Version_' . $fcode, $vehicleId);
        }
        $refDataJson1['deviceId'] = $deviceId;
        if (session()->get('cur') == 'dealer') {
            $ownership = $username;
            $vehicleNameMob = 'H_VehicleName_Mobile_Dealer_' . $username . '_Org_' . $fcode;
            $cou = $redis->hlen($vehicleNameMob);
            $orgLi = $redis->HScan($vehicleNameMob, 0,  'count', $cou, 'match', '*' . $vehicleId . ':' . $deviceIdOld . '*');
            $orgL = $orgLi[1];
            foreach ($orgL as $key => $value) {
                $redis->hdel('H_VehicleName_Mobile_Dealer_' . $username . '_Org_' . $fcode, $key);
            }
            $redis->hset('H_VehicleName_Mobile_Dealer_' . $username . '_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortName . ':' . $orgId1 . ':' . $gpsSimNo, $vehicleId);
        } else if (session()->get('cur') == 'admin') {
            $ownership = 'OWN';
            $vehicleNameMob = 'H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode;
            $cou = $redis->hlen($vehicleNameMob);
            $orgLi = $redis->HScan($vehicleNameMob, 0,  'count', $cou, 'match', '*' . $vehicleId . ':' . $deviceIdOld . '*');
            $orgL = $orgLi[1];
            foreach ($orgL as $key => $value) {
                $redis->hdel('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $key);
            }
            $redis->hset('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortName . ':' . $orgId1 . ':' . $gpsSimNo . ':OWN', $vehicleId);
        }
        //$refDataJson1['OWN']=$ownership;
        $owner = isset($refDataJson1['OWN']) ? $refDataJson1['OWN'] : '';
        $refDataJson1['OWN'] = $owner;
        $detailsJson = json_encode($refDataJson1);
        $redis->hmset('H_RefData_' . $fcode, $vehicleId, $detailsJson);

        $redis->hmset('H_RefData_' . $fcode, $vehicleId, $detailsJson);
        //pro data
        $tmpPositon = $redis->hget('H_ProData_' . $fcode, $vehicleId);
        $redis->hdel('H_ProData_' . $fcode, $vehicleId);
        if ($deviceId != $deviceIdOld) {
            $time = microtime(true);
            $time = round($time * 1000);
            $tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,0,S,N';
            $LatLong = $redis->hget('H_Franchise_LatLong', $fcode);
            if ($LatLong != null) {
                $data_arr = explode(":", $LatLong);
                $latitude = $data_arr[0];
                $longitude = $data_arr[1];
                $tmpPositon =  $latitude . ',' . $longitude . ',0,N,' . $time . ',0.0,N,N,ON,S,N';
                $redis->sadd('S_NoDataVehicle', $vehicleId . ',' . $fcode);
                $redis->hset('H_NoDataVehicleProcess_Time', $vehicleId . ',' . $fcode, $time);
            }
        }
        $redis->hset('H_ProData_' . $fcode, $vehicleId, $tmpPositon);
        $pub = $redis->PUBLISH('sms:topicNetty', $deviceId);
        return $deviceId;
    }

    static function vehicleIdMig($vehicleId, $vehicleIdOld, $expiredPeriodOld, $migratevehicle)
    {

        $username = auth()->id();
        $redis = Redis::connection();
        $current = Carbon::now();
        $rajeev = $current->format('Y-m-d');
        $tomorrow = Carbon::now()->addDay();
        $vehicleId = preg_replace('/[ ]{2,}|[\t]/', '', trim($vehicleId));
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
        $deviceId = $redis->hget($vehicleDeviceMapId, $vehicleIdOld);

        //map del/set
        $redis->hdel($vehicleDeviceMapId, $vehicleIdOld);
        $redis->hdel($vehicleDeviceMapId, $deviceId);
        $redis->hset($vehicleDeviceMapId, $vehicleId, $deviceId);
        $redis->hset($vehicleDeviceMapId, $deviceId, $vehicleId);
        //refdata
        $refDataJson1 = $redis->hget('H_RefData_' . $fcode, $vehicleIdOld);
        $refDataJson1 = json_decode($refDataJson1, true);
        $expiredPeriod = isset($refDataJson1['expiredPeriod']) ? $refDataJson1['expiredPeriod'] : 'expiredPeriod';
        $orgId = isset($refDataJson1['orgId']) ? $refDataJson1['orgId'] : 'DEFAULT';
        $orgId1 = strtoupper($orgId);
        $shortName1 = isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : '';
        $licene_type = isset($refDataJson1['Licence']) ? $refDataJson1['Licence'] : 'Advance';
        $Payment_Mode = isset($refDataJson1['Payment_Mode']) ? $refDataJson1['Payment_Mode'] : 'Monthly';
        $shortName2 = preg_replace('/\s+/', '', $shortName1);
        $shortName = strtoupper($shortName2);
        $gpsSimNo = isset($refDataJson1['gpsSimNo']) ? $refDataJson1['gpsSimNo'] : '';
        if (session()->get('cur') == 'dealer') {
            $ownership = $username;
        } else if (session()->get('cur') == 'admin') {
            $ownership = 'OWN';
        }
        $refDataJson1['OWN'] = $ownership;
        $detailsJson = json_encode($refDataJson1);
        $redis->hset('H_RefData_' . $fcode, $vehicleId, $detailsJson);
        $redis->hdel('H_RefData_' . $fcode, $vehicleIdOld);
        $pub = $redis->PUBLISH('sms:topicNetty', $deviceId);
        //pro data
        $tmpPositon = $redis->hget('H_ProData_' . $fcode, $vehicleIdOld);
        $redis->hdel('H_ProData_' . $fcode, $vehicleIdOld);
        if ($tmpPositon == null) {
            $time = microtime(true);
            $time = round($time * 1000);
            $tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,0,S,N';
            $LatLong = $redis->hget('H_Franchise_LatLong', $fcode);
            if ($LatLong != null) {
                $data_arr = explode(":", $LatLong);
                $latitude = $data_arr[0];
                $longitude = $data_arr[1];
                $tmpPositon =  $latitude . ',' . $longitude . ',0,N,' . $time . ',0.0,N,N,ON,S,N';
                $redis->sadd('S_NoDataVehicle', $vehicleId . ',' . $fcode);
                $redis->hset('H_NoDataVehicleProcess_Time', $vehicleId . ',' . $fcode, $time);
            }
        }
        $redis->hset('H_ProData_' . $fcode, $vehicleId, $tmpPositon);
        $vec = $redis->hget('H_Expire_' . $fcode, $expiredPeriod);
        if ($vec !== null) {
            $details = explode(',', $vec);
            $temp = null;
            foreach ($details as $gr) {
                if ($gr == $vehicleIdOld) {
                    $gr = $vehicleId;
                }
                if ($temp == null) {
                    $temp = $gr;
                } else {
                    $temp = $temp . ',' . $gr;
                }
            }
            $redis->hset('H_Expire_' . $fcode, $expiredPeriod, $temp);
        }
        $expiredPeriod = $redis->hget('H_Expire_' . $fcode, $vehicleIdOld);
        if (!$expiredPeriod == null && $expiredPeriodOld !== null) {
            $expiredPeriod = str_replace($vehicleIdOld, $vehicleId, $expiredPeriod);
            $redis->hset('H_Expire_' . $fcode, $expiredPeriodOld, $expiredPeriod);
        }
        //prepaid admin
        if (session()->get('cur1') == 'prePaidAdmin') {
            $licenceData = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleIdOld);
            $redis->hdel('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleIdOld);
            $redis->hdel('H_Vehicle_LicenceId_Map_' . $fcode, $licenceData);
            $redis->hset('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleId, $licenceData, $licenceData, $vehicleId);
            if (session()->get('cur') == 'dealer') {
                $pre_vehicles = $redis->hkeys('H_PreRenewal_Licence_Dealer_' . $username . '_' . $fcode);
            } else if (session()->get('cur') == 'admin') {
                $pre_vehicles = $redis->hkeys('H_PreRenewal_Licence_Admin_' . $fcode);
            }
            if (in_array($vehicleIdOld, $pre_vehicles)) {
                if (session()->get('cur') == 'dealer') {
                    $Licence_Id = $redis->hget('H_PreRenewal_Licence_Dealer_' . $username . '_' . $fcode, $vehicleIdOld);
                    $redis->hdel('H_PreRenewal_Licence_Dealer_' . $username . '_' . $fcode, $vehicleIdOld);
                    $redis->hset('H_PreRenewal_Licence_Dealer_' . $username . '_' . $fcode, $vehicleId, $Licence_Id);
                } else if (session()->get('cur') == 'admin') {
                    $Licence_Id = $redis->hget('H_PreRenewal_Licence_Admin_' . $fcode, $vehicleIdOld);
                    $redis->hdel('H_PreRenewal_Licence_Admin_' . $fcode, $vehicleIdOld);
                    $redis->hset('H_PreRenewal_Licence_Admin_' . $fcode, $vehicleId, $Licence_Id);
                }
            }
            $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
            $servername = $franchiesJson;
            $servername = "209.97.163.4";
            if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
                return 'Ipaddress Failed !!!';
            }
            $usernamedb = "root";
            $password = "#vamo123";
            $dbname = $fcode;
            $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
            if (!$conn) {
                die('Could not connect: ' . mysqli_connect_error());
                return 'Please Update One more time Connection failed';
            } else {
                $ins = "INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$username','$licenceData','$vehicleId','$deviceId','$licene_type','Migration')";
                if ($conn->multi_query($ins)) {
                    logger('Successfully inserted');
                } else {
                    logger('not inserted');
                }
                $conn->close();
            }
        }
        $redis->srem('S_Vehicles_' . $fcode, $vehicleIdOld);
        $redis->sadd('S_Vehicles_' . $fcode, $vehicleId);
        $redis->srem('S_Vehicles_' . $orgId . '_' . $fcode, $vehicleIdOld);
        $redis->sadd('S_Vehicles_' . $orgId . '_' . $fcode, $vehicleId);
        //search key reset
        $redis->hdel('H_VehicleName_Mobile_Org_' . $fcode, $vehicleIdOld . ':' . $deviceId . ':' . $shortName . ':' . $orgId1 . ':' . $gpsSimNo);
        $redis->hset('H_VehicleName_Mobile_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortName . ':' . $orgId1 . ':' . $gpsSimNo, $vehicleId);
        if (session()->get('cur') == 'dealer') {
            $mailto = $username;
            $redis->srem('S_Vehicles_Dealer_' . $username . '_' . $fcode, $vehicleIdOld);
            $redis->sadd('S_Vehicles_Dealer_' . $username . '_' . $fcode, $vehicleId);
            //$cou = $redis->scard('S_Vehicles_Dealer_'.$username.'_'.$fcode);
            $vehicleNameMob = 'H_VehicleName_Mobile_Dealer_' . $username . '_Org_' . $fcode;
            $cou = $redis->hlen($vehicleNameMob);
            $orgLi = $redis->HScan($vehicleNameMob, 0,  'count', $cou, 'match', '*' . $vehicleIdOld . ':' . $deviceId . '*');
            $orgL = $orgLi[1];
            foreach ($orgL as $key => $value) {
                $redis->hdel('H_VehicleName_Mobile_Dealer_' . $username . '_Org_' . $fcode, $key);
            }
            $redis->hset('H_VehicleName_Mobile_Dealer_' . $username . '_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortName . ':' . $orgId1 . ':' . $gpsSimNo, $vehicleId);
            $groupList1 = $redis->smembers('S_Groups_Dealer_' . $username . '_' . $fcode);
        } else if (session()->get('cur') == 'admin') {
            $mailto = $fcode;
            $redis->srem('S_Vehicles_Admin_' . $fcode, $vehicleIdOld);
            $redis->sadd('S_Vehicles_Admin_' . $fcode, $vehicleId);
            // $cou = $redis->scard('S_Vehicles_Admin_'.$fcode);
            $vehicleNameMob = 'H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode;
            $cou = $redis->hlen($vehicleNameMob);
            $orgLi = $redis->HScan($vehicleNameMob, 0,  'count', $cou, 'match', '*' . $vehicleIdOld . ':' . $deviceId . '*');
            $orgL = $orgLi[1];
            foreach ($orgL as $key => $value) {
                $redis->hdel('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $key);
            }
            $redis->hset('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $vehicleId . ':' . $deviceId . ':' . $shortName . ':' . $orgId1 . ':' . $gpsSimNo . ':OWN', $vehicleId);
            $groupList1 = $redis->smembers('S_Groups_Admin_' . $fcode);
        }

        /*$oldVehiValues = $redis->smembers('S_' . $vehicleIdOld . '_' . $fcode);
        $redis->del('S_' . $vehicleIdOld . '_' . $fcode);
        foreach ($oldVehiValues as $key => $value) {
            $redis->sadd('S_' . $vehicleId . '_' . $fcode, $value);
        }
        //groups
        $groupList = $redis->smembers('S_Groups_' . $fcode);
        foreach ($groupList as $group) {
            if ($redis->sismember($group, $vehicleIdOld) == 1) {
                $result = $redis->srem($group, $vehicleIdOld);
                $redis->sadd($group, $vehicleId);
            }
        }*/

        if ($redis->EXISTS('S_' . $vehicleIdOld . '_' . $fcode)) {
            $groupList = $redis->smembers('S_' . $vehicleIdOld . '_' . $fcode);
            foreach ($groupList as $group) {
                $group = str_replace('S_', '', trim($group));
                $redis->srem($group, $vehicleIdOld);
                $redis->sadd($group, $vehicleId);
            }
            $redis->rename('S_' . $vehicleIdOld . '_' . $fcode, 'S_' . $vehicleId . '_' . $fcode);
        }

        //notification key
        $UNvalue = $redis->hget('H_UserId_Notification_map_' . $fcode, $vehicleIdOld);
        $redis->hdel('H_UserId_Notification_map_' . $fcode, $vehicleIdOld);
        $redis->hset('H_UserId_Notification_map_' . $fcode, $vehicleId, $UNvalue);

        $DDvalue = $redis->hget('H_Delta_Distance_' . $fcode, $vehicleIdOld);
        $redis->hdel('H_Delta_Distance_' . $fcode, $vehicleIdOld);
        $redis->hset('H_Delta_Distance_' . $fcode, $vehicleId, $DDvalue);

        // New changes
        $LHistforOld = 'L_HistforOutOfOrderData_' . $vehicleIdOld . '_' . $fcode . '_' . $rajeev;
        $L_HistEx = $redis->exists($LHistforOld);
        if ($L_HistEx != '0') {
            $LHistfor = 'L_HistforOutOfOrderData_' . $vehicleId . '_' . $fcode . '_' . $rajeev;
            $redis->rename($LHistforOld, $LHistfor);
        }

        $LAhistOld = 'L_Alarm_Hist_' . $vehicleIdOld . '_' . $fcode;
        $LAhistEx = $redis->exists($LAhistOld);
        if ($LAhistEx != '0') {
            $LAhist = 'L_Alarm_Hist_' . $vehicleId . '_' . $fcode;
            $redis->rename($LAhistOld, $LAhist);
        }

        $LRfitdOld = 'L_Rfid_Hist_' . $vehicleIdOld . '_' . $fcode . '_' . $rajeev;
        $LRfitEx = $redis->exists($LRfitdOld);
        if ($LRfitEx != '0') {
            $LRfitd = 'L_Rfid_Hist_' . $vehicleId . '_' . $fcode . '_' . $rajeev;
            $redis->rename($LRfitdOld, $LRfitd);
        }

        $RouteDeviationOld = 'RouteDeviation:' . $vehicleIdOld . ':' . $fcode;
        $LRouteDeviationEx = $redis->exists($RouteDeviationOld);
        if ($LRouteDeviationEx != '0') {
            $RouteDeviation = 'RouteDeviation:' . $vehicleId . ':' . $fcode;
            $redis->rename($RouteDeviationOld, $RouteDeviation);
        }

        //L_hist
        $LHistOld = 'L_Hist_' . $vehicleIdOld . '_' . $fcode . '_' . $rajeev;
        $LHHistEx = $redis->exists($LHistOld);
        if ($LHHistEx != '0') {
            $LHist = 'L_Hist_' . $vehicleId . '_' . $fcode . '_' . $rajeev;
            $redis->rename($LHistOld, $LHist);
        }

        ///ram L_Sensor_Hist* 
        //$setkey='L_Sensor_Hist_*'. $fcode .'_'. $rajeev;
        $LsensorOld = 'L_Sensor_Hist_' . $vehicleIdOld . '_' . $fcode . '_' . $rajeev;
        $L_SensorEx = $redis->exists($LsensorOld);
        if ($L_SensorEx != '0') {
            $Lsensor = 'L_Sensor_Hist_' . $vehicleId . '_' . $fcode . '_' . $rajeev;
            $redis->rename($LsensorOld, $Lsensor);
        }

        //Z_sensor*
        $ZsensorOld = 'Z_Sensor_' . $vehicleIdOld . '_' . $fcode;
        $L_ZSensorEx = $redis->exists($ZsensorOld);
        if ($L_ZSensorEx != '0') {
            $Zsensor = 'Z_Sensor_' . $vehicleId . '_' . $fcode;
            $redis->rename($ZsensorOld, $Zsensor);
        }

        //nodata
        $nodataOld = 'NoData24:' . $fcode . ':' . $vehicleIdOld;
        $L_nodataEx = $redis->exists($nodataOld);
        if ($L_nodataEx != '0') {
            $nodata = 'NoData24:' . $fcode . ':' . $vehicleId;
            $redis->rename($nodataOld, $nodata);
        }

        //changes done
        if ($migratevehicle == "yes") {
            try {
                $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
                $servername = $franchiesJson;
                $servername = "209.97.163.4";
                if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
                    return 'Ipaddress Failed !!!';
                }
                $usernamedb = "root";
                $password = "#vamo123";
                $dbname = $fcode;
                $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
                if (!$conn) {
                    die('Could not connect: ' . mysqli_connect_error());
                    return 'Please Update One more time Connection failed';
                } else {

                    $update = "UPDATE yearly_vehicle_history SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update);
                    $update1 = "UPDATE vehicle_history SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update1);
                    $update2 = "UPDATE Executive_Details SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update2);
                    $update3 = "UPDATE sensor_history SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update3);
                    $update4 = "UPDATE rfid_history SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update4);
                    $update5 = "UPDATE TollgateDetails SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update5);
                    $update6 = "UPDATE Sms_Audit SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update6);
                    $update7 = "UPDATE ScheduledReport SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update7);
                    $update8 = "UPDATE Poi_History SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update8);
                    $update9 = "UPDATE PERFORMANCE SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
                    $conn->multi_query($update9);
                    $update10 = "UPDATE DailyPerformance SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
                    $conn->multi_query($update10);
                    $update11 = "UPDATE FuelReports SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update11);
                    $update12 = "UPDATE Alarms_History SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update12);
                    $update13 = "UPDATE FUELDETAILS SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
                    $conn->multi_query($update13);
                    $update14 = "UPDATE FuelDetailsTest SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
                    $conn->multi_query($update14);
                    $update15 = "UPDATE ImageDetails SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update15);
                    $update16 = "UPDATE MahindrasData SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update16);
                    $update17 = "UPDATE PERFORMANCETemp SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
                    $conn->multi_query($update17);
                    $update18 = "UPDATE SiteStoppageAlertReport SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update18);
                    $update19 = "UPDATE vehicleExpiryDetails SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update19);
                    $update20 = "UPDATE PERFORMANCETemp SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
                    $conn->multi_query($update20);
                    $update21 = "UPDATE vehicle_history1 SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update21);
                    $update22 = "UPDATE vehicle_history2 SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
                    $conn->multi_query($update22);

                    $conn->close();
                }
            } catch (\Exception $e) {
                logger($vehicleId . '--------------------inside Exception-------------------->>' . $e->getMessage() . '--line--->' . $e->getLine());
            }
        }
        try {
            $licence_id = DB::select('select licence_id from Licence where type = :type', ['type' => $licene_type]);
            $payment_mode_id = DB::select('select payment_mode_id from Payment_Mode where type = :type', ['type' => $Payment_Mode]);
            $licence_id = $licence_id[0]->licence_id;
            $payment_mode_id = $payment_mode_id[0]->payment_mode_id;
            DB::table('Vehicle_details')->where('vehicle_id', $vehicleIdOld)->where('fcode', $fcode)->update(array('vehicle_id' => $vehicleId, 'device_id' => $deviceId));
        } catch (\Exception $e) {
            logger($vehicleId . '--------------------inside Exception--------> ' . $e->getMessage() . '--line--->' . $e->getLine());
        }
        return $vehicleId;
    }
    public function migration($id)
    {
        try {
            $username = auth()->id();
            $redis = Redis::connection();
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $vehicleId = $id;
            $deviceId   = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicleId);
            $orgList = null;
            if (session()->get('cur') == 'dealer') {
                $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
                $tmpOrgList = $redis->smembers('S_Organisations_Dealer_' . $username . '_' . $fcode);
            } else if (session()->get('cur') == 'admin') {
                $vehicleListId = 'S_Vehicles_Admin_' . $fcode;
                $tmpOrgList = $redis->smembers('S_Organisations_Admin_' . $fcode);
            } else {
                $vehicleListId = 'S_Vehicles_' . $fcode;
                $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
            }
            $details = $redis->hget('H_RefData_' . $fcode, $vehicleId);
            if ($redis->sismember($vehicleListId, $vehicleId) == 1 && $details != null) {
                $refDataFromDB = json_decode($details, true);
                $orgId = isset($refDataFromDB['orgId']) ? $refDataFromDB['orgId'] : 'NotAvailabe';
                $expiredPeriod = isset($refDataFromDB['expiredPeriod']) ? $refDataFromDB['expiredPeriod'] : 'NotAvailabe';
                $expiredPeriod = str_replace(' ', '', $expiredPeriod);
                $parkingAlert = isset($refDataFromDB->parkingAlert) ? $refDataFromDB->parkingAlert : 0;
                $orgList = Arr::add($orgList, 'Default', 'Default');
                foreach ($tmpOrgList as $org) {
                    $orgList = Arr::add($orgList, $org, $org);
                }
                $deviceIdOld = $deviceId;
                $vehicleIdOld = $vehicleId;
                $expiredPeriodOld = $expiredPeriod;

                $migrationData = [
                    'deviceId' => $deviceId,
                    'deviceIdOld' => $deviceIdOld,
                    'vehicleId' => $vehicleId,
                    'expiredPeriod' => $expiredPeriod,
                    'vehicleIdOld' => $vehicleIdOld,
                    'expiredPeriodOld' => $expiredPeriodOld,
                ];

                return view('vdm.vehicles.migration', $migrationData);
            } else {
                return redirect()->to('VdmVehicleScan' . $vehicleId)->withErrors($vehicleId . ' Not present in the admin !');
            }
        } catch (\Exception $e) {
            $username = auth()->id();
            $vehicleId = $id;
            logger(sprintf("EXCEPTIONBYUSER_" . $username . " file --> %s - line --> " . $e->getLine() . '------exception----------> ' . $e->getMessage(), __FILE__));
            return redirect()->to('VdmVehicleScan' . $vehicleId);
        }
    }

    public function rename($id)
    {

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $vehicleId = $id;
        $deviceId   = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicleId);
        $orgList = null;

        if (session()->get('cur') == 'dealer') {

            $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
            $tmpOrgList = $redis->smembers('S_Organisations_Dealer_' . $username . '_' . $fcode);
        } else if (session()->get('cur') == 'admin') {

            $vehicleListId = 'S_Vehicles_Admin_' . $fcode;
            $tmpOrgList = $redis->smembers('S_Organisations_Admin_' . $fcode);
        } else {

            $vehicleListId = 'S_Vehicles_' . $fcode;
            $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
        }
        $details = $redis->hget('H_RefData_' . $fcode, $vehicleId);
        if ($redis->sismember($vehicleListId, $vehicleId) == 1 && $details != null) {

            $refDataFromDB = json_decode($details, true);
            $orgId = isset($refDataFromDB['orgId']) ? $refDataFromDB['orgId'] : 'NotAvailabe';
            $expiredPeriod = isset($refDataFromDB['expiredPeriod']) ? $refDataFromDB['expiredPeriod'] : 'NotAvailabe';
            $expiredPeriod = str_replace(' ', '', $expiredPeriod);
            $parkingAlert = isset($refDataFromDB->parkingAlert) ? $refDataFromDB->parkingAlert : 0;
            $orgList = Arr::add($orgList, 'Default', 'Default');
            foreach ($tmpOrgList as $org) {
                $orgList = Arr::add($orgList, $org, $org);
            }
            $deviceIdOld = $deviceId;
            $vehicleIdOld = $vehicleId;
            $expiredPeriodOld = $expiredPeriod;

            $renameData = [
                'deviceId' => $deviceId,
                'deviceIdOld' => $deviceIdOld,
                'vehicleId' => $vehicleId,
                'vehicleIdOld' => $vehicleIdOld,
                'expiredPeriodOld' => $expiredPeriodOld,
            ];

            return view('vdm.vehicles.rename', $renameData);
        } else {
            return redirect()->to('VdmVehicleScan' . $vehicleId)->withErrors($vehicleId . ' Not present in the admin !');
        }
    }
    public function updateLogDays()
    {

        $username   = auth()->id();
        $redis      = Redis::connection();
        $enabledebug = request()->get('debugValue');
        $userId = $username;
        $vehicleId = request()->get('vehicleId');
        $validity1 = request()->get('debugDays');
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        if ($enabledebug == 'Enable') {
            //$redis->hmset ( 'H_UserId_Cust_Map', $userId . ':Enable',$validity1);
            $validity = $validity1 * 86400;
            //$grouplist=$redis->smembers($userId);
            $redis->set('EnableLog:' . $vehicleId . ':' . $fcode, $userId);
            $redis->expire('EnableLog:' . $vehicleId . ':' . $fcode, $validity);
            $deviceId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicleId);
            //$pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
            $response = 'Debug Enabled successfully';
        }
        if ($enabledebug == 'Disable') {
            $grouplist = $redis->smembers($userId);
            //$redis->hmset ( 'H_UserId_Cust_Map', $userId . ':Enable','0');
            $redis->del('EnableLog:' . $vehicleId . ':' . $fcode);
            $deviceId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicleId);
            //$pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
            $response = 'Debug Disabled successfully';
        }
        return $response;
    }


    public static function DbEntry($mysqlDetails, $model, $fcode)
    {
        try {
            logger($model . '--in-->' . $mysqlDetails['vehicleId']);
            $modelClass = '\\App\\Models\\' . $model;
            $modelname = new $modelClass();
            $table = $modelname->getTable();
            $db = $fcode;
            AuditTables::ChangeDB($db);
            if (!Schema::hasTable($table)) {
                if ($model == 'AuditManualCalibrate') {
                    AuditTables::CreateAuditManualCalibrate();
                } else if ($model == 'AuditAutoCalibrate') {
                    AuditTables::CreateAuditAutoCalibrate();
                } else {
                    AuditTables::CreateAuditVehicle();
                }
            }
            $modelClass::create($mysqlDetails);

            // $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
            // if (count($tableExist) > 0) {
            //     $modelClass::create($mysqlDetails);
            // } else {
            //     logger('create table ---->'.$model);
            //     if ($model == 'AuditManualCalibrate') {
            //         AuditTables::CreateAuditManualCalibrate();
            //     } else if ($model == 'AuditAutoCalibrate') {
            //         AuditTables::CreateAuditAutoCalibrate();
            //     } else {
            //         AuditTables::CreateAuditVehicle();
            //     }
            //     $modelClass::create($mysqlDetails);
            // }
            // AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
        } catch (Exception $e) {
            AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
            logger('Exception_' . $mysqlDetails['vehicleId'] . ' error --->' . $e->getMessage() . ' line--->' . $e->getLine());
        }
    }

    public function reprocessVehicle()
    {
        $vehicleId = request()->get('vehicleId');
        $reqDate = date_create(request()->get('date'));
        $date = date_format($reqDate, "Y-m-d");
        $reSubmit = request()->get("reSubmit");
        $response = [
            'message' => 'Reprocessing Failed',
            'status' => 'no'
        ];
        try {

            $redis = Redis::connection();
            $username = auth()->id();
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            if ($reSubmit == "true") {
                $redis->del('Reprocess:' . $vehicleId . ':' . $date);
            }
            $check = $redis->sismember('S_ReprocessData_' . $fcode, $vehicleId . ',' . $date);
            if ($check == 1) {
                $response =  ['message' => 'Under Reprocessing - ' . $vehicleId . ' - ' . $date, 'status' => 'no'];
            } else {
                $status =  $redis->get('Reprocess:' . $vehicleId . ':' . $date);
                if ($status == 'yes') {
                    $response =  ['message' => 'Reprocessing Already Done - ' . $vehicleId . ' - ' . $date, 'status' => 'yes'];
                } else {
                    $redis->sadd('S_ReprocessData_' . $fcode, $vehicleId . ',' . $date);
                    $response = ['message' => 'Reprocessing Started - ' . $vehicleId . ' - ' . $date, 'status' => 'no'];
                }
            }
            return response()->json($response);
        } catch (Exception $e) {
            logger('Error inside reprocess vehicle ' . $vehicleId . '  -------error -->' . $e->getMessage() . '--line-->' . $e->getLine());
            return response()->json($response);
        }
    }
    public function changeExpireDate()
    {
        $list = [
            "TN02BT5895" => "09-04-2020",
            "NL01B1300" => "09-01-2020",
            "TN02BT5760" => "13-05-2020",
            "NL01B1299" => "09-01-2020",
            "TN02BT5753" => "09-04-2020",
            "TN02BT5774" => "09-04-2020",
            "NL01B1491" => "15-05-2020",
            "TN02BT8305" => "09-04-2020",
            "TN02BT5715" => "14-05-2020",
            "TN02BK7517" => "17-05-2020",
            "TN02BQ7736" => "15-06-2020",
            "TN02BM4959" => "09-04-2020",
            "TN02BT8347" => "20-04-2020",
            "NL01B1490" => "16-05-2020",
            "TN02BT5707" => "19-04-2020"
        ];

        $fcode = "VAM";
        $temp = [];
        $temp2 = [];
        $redis = Redis::connection();
        foreach ($list as $vehicleId => $date) {
            $refData = $redis->hget("H_RefData_$fcode", $vehicleId);
            if ($refData == null) {
                $temp[] = $vehicleId;
                continue;
            }
            $refData = json_decode($refData, true);
            $date1 = join("-", array_reverse(explode('-', $date)));
            $refData["vehicleExpiry"] = $date1;
            $refDataJson = json_encode($refData);

            $redis->hset("H_RefData_$fcode", $vehicleId, $refDataJson);

            $licenceId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleId);
            $licExpireData = $redis->hget("H_LicenceExpiry_$fcode", $licenceId);
            $licExpireData = json_decode($licExpireData, true);

            $licExpireData["LicenceExpiryDate"] = $date;
            $licExpireDataJson = json_encode($licExpireData);

            $redis->hset("H_LicenceExpiry_$fcode", $licenceId, $licExpireDataJson);
            $temp2[] = $vehicleId;
        }
        return response()->json([
            'invalid_vehicle' => $temp,
            'valid_vehicle' => $temp2
        ]);
    }
}
