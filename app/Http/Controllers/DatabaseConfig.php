<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Schema;

class DatabaseConfig extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */



	static public function checkDb()
	{

		if (!Schema::hasTable('Licence')) {

			logger('------Licence not avaliable---------- ');
			Schema::create('Licence', function ($table) {
				$table->increments('licence_id')->first();
				$table->text('type')->unique()->after('licence_id');
				$table->text('status')->nullable()->after('type');
				$table->dateTime('create_time_stamp')->after('status');
				$table->date('create_date')->nullable()->after('create_time_stamp');
			});
		}

		if (!Schema::hasTable('Payment_Mode')) {

			logger('------Payment_Mode not avaliable---------- ');
			Schema::create('Payment_Mode', function ($table) {
				$table->increments('payment_mode_id')->first();
				$table->text('type')->unique()->after('payment_mode_id');
				$table->text('status')->nullable()->after('type');
				$table->dateTime('create_time_stamp')->after('status');
				$table->date('create_date')->nullable()->after('create_time_stamp');
			});
		}


		if (!Schema::hasTable('Vehicle_details')) {

			Schema::create('Vehicle_details', function ($table) {
				$table->text('vehicle_id')->first();
				$table->text('fcode')->after('vehicle_id');
				$table->dateTime('sold_date')->after('fcode');
				$table->integer('sold_time_stamp')->after('sold_date');
				$table->integer('month')->after('sold_time_stamp');
				$table->integer('year')->after('month');
				$table->text('status')->nullable()->after('year');
				$table->text('belongs_to')->after('status');
				$table->text('device_id')->after('belongs_to');
				$table->dateTime('renewal_date')->after('device_id');
				$table->text('orgId')->after('renewal_date');
				$table->integer('payment_mode_id');
				$table->integer('licence_id');
				$table->primary(['vehicle_id', 'fcode']);
			});
			//
		}
	}
}
