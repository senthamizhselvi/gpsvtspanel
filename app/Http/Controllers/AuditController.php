<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redis;

use App\Models\AuditFrans;
use App\Models\AuditDealer;
use App\Models\AuditVehicle;
use App\Models\AuditUser;
use App\Models\AuditGroup;
use Exception;
use Illuminate\Support\Facades\DB;

class AuditController extends Controller
{
  public function auditShow($modelData)
  {
    $rows = [];
    $columns = [];
    $indexVal = 0;
    $param = explode("-", $modelData);
    $model = $param[0];
    $countryTimezone = base64_decode($param[1]);    
    try {
      $username = auth()->id();
      $redis = Redis::connection();
      $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
      $modelClass = '\\App\\Models\\' . $model;
      $modelname = new $modelClass();
      $table = $modelname->getTable();
      if ($fcode == 'vamos') $fcode = 'VAMOSYS';
      AuditTables::ChangeDB($fcode);
      $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $fcode)->where('table_name', '=', $table)->get();
      if ($model == 'AuditFrans' || $model == 'loginCustomisation') {
        AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
        $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', 'VAMOSYS')->where('table_name', '=', $table)->get();
      }
      if (count($tableExist) > 0 && ($modelClass::first() != null)) {
        $columns = array_keys($modelClass::first()->toArray());
        if (session()->get('cur') == 'admin') {
          if (($model == 'AuditFrans' || $model == 'loginCustomisation') && strpos($username, 'admin') !== false) {
            $rows = $modelClass::where('fcode', $fcode)->orderBy('created_at', 'DESC')->get();
          } else {
            if ($model == 'RenewalDetails') {
              $rows = $modelClass::all();
            } else {
              $rows = $modelClass::orderBy('created_at', 'DESC')->get();
              //return $rows;
            }
          }
        } else if (session()->get('cur') == 'dealer') {
          if ($model == 'RenewalDetails') {
            $rows = $modelClass::where('User_Name', $username)->get();
          } else {
            if ($model == 'AuditDealer' && session()->get('cur') == 'dealer') {
              $rows = $modelClass::where('DealerId', $username)->orderBy('created_at', 'DESC')->get();
            } else if ($model == 'loginCustomisation') {
              $rows = $modelClass::where('username', $username)->orderBy('created_at', 'DESC')->get();
            } else {
              $rows = $modelClass::where('UserName', $username)->orderBy('created_at', 'DESC')->get();
            }
          }
        } else if ($model == 'AuditFrans') {
          $rows = $modelClass::orderBy('created_at', 'DESC')->get();
        }
      } else {
        $rows = [];
        if ($model == 'AuditVehicle') {
          $columns = array(
            0 => 'id',
            1 => 'fcode',
            2 => 'vehicleId',
            3 => 'userName',
            4 => 'status',
            5 => 'deviceId',
            6 => 'deviceModel',
            7 => 'shortName',
            8 => 'regNo',
            9 => 'orgId',
            10 => 'vehicleType',
            11 => 'oprName',
            12 => 'mobileNo',
            13 => 'odoDistance',
            14 => 'gpsSimNo',
            15 => 'paymentType',
            16 => 'OWN',
            17 => 'expiredPeriod',
            18 => 'overSpeedLimit',
            19 => 'driverName',
            20 => 'email',
            21 => 'altShortName',
            22 => 'sendGeoFenceSMS',
            23 => 'morningTripStartTime',
            24 => 'eveningTripStartTime',
            25 => 'parkingAlert',
            26 => 'vehicleMake',
            27 => 'Licence',
            28 => 'Payment_Mode',
            29 => 'descriptionStatus',
            30 => 'vehicleExpiry',
            31 => 'onboardDate',
            32 => 'tankSize',
            33 => 'licenceissuedDate',
            34 => 'communicatingPortNo',
            35 => 'oldVehicleId',
            36 => 'oldDeviceId',
            37 => 'created_at',
          );
        } else if ($model == 'AuditDealer') {
          $columns = array(
            0 => 'id',
            1 => 'fcode',
            2 => 'dealerId',
            3 => 'userName',
            4 => 'status',
            5 => 'email',
            6 => 'mobileNo',
            7 => 'zoho',
            8 => 'mapKey',
            9 => 'addressKey',
            10 => 'notificationKey',
            11 => 'gpsvtsApp',
            12 => 'website',
            13 => 'smsSender',
            14 => 'smsProvider',
            15 => 'providerUserName',
            16 => 'providerPassword',
            17 => 'numofBasic',
            18 => 'numofAdvance',
            19 => 'numofPremium',
            20 => 'avlofBasic',
            21 => 'avlofAdvance',
            22 => 'avlofPremium',
            23 => 'LicenceissuedDate',
            24 => 'created_at',
          );
        } else if ($model == 'AuditFrans') {
          $columns = array(
            0 => 'id',
            1 => 'username',
            2 => 'status',
            3 => 'fname',
            4 => 'description',
            5 => 'fcode',
            6 => 'landline',
            7 => 'mobileNo1',
            8 => 'mobileNo2',
            9 => 'prepaid',
            10 => 'email1',
            11 => 'email2',
            12 => 'userId',
            13 => 'fullAddress',
            14 => 'otherDetails',
            15 => 'numberofLicence',
            16 => 'availableLincence',
            17 => 'addLicence',
            18 => 'numberofBasicLicence',
            19 => 'numberofAdvanceLicence',
            20 => 'numberofPremiumLicence',
            21 => 'availableBasicLicence',
            22 => 'availableAdvanceLicence',
            23 => 'availablePremiumLicence',
            24 => 'addBasicLicence',
            25 => 'addAdvanceLicence',
            26 => 'addPremiumLicence',
            27 => 'website',
            28 => 'trackPage',
            29 => 'smsSender',
            30 => 'smsProvider',
            31 => 'providerUserName',
            32 => 'providerPassword',
            33 => 'timeZone',
            34 => 'apiKey',
            35 => 'mapKey',
            36 => 'addressKey',
            37 => 'notificationKey',
            38 => 'gpsvtsApp',
            39 => 'backUpDays',
            40 => 'dbType',
            41 => 'zoho',
            42 => 'auth',
            43 => 'created_at',
          );
        } else if ($model == 'AuditUser') {
          $columns = array(
            0 => 'id',
            1 => 'fcode',
            2 => 'userId',
            3 => 'userName',
            4 => 'status',
            5 => 'email',
            6 => 'mobileNo',
            7 => 'password',
            8 => 'cc_email',
            9 => 'zoho',
            10 => 'companyName',
            11 => 'created_at',
          );
        } else if ($model == 'RenewalDetails') {
          $columns = array(
            0 => 'ID',
            1 => 'User_Name',
            2 => 'Licence_Id',
            3 => 'Vehicle_Id',
            4 => 'Device_Id',
            5 => 'Type',
            6 => 'Processed_Date',
            7 => 'Status',
          );
        } else if ($model == 'AuditGroup') {
          $columns = array(
            0 => 'id',
            1 => 'fcode',
            2 => 'groupName',
            3 => 'userName',
            4 => 'status',
            5 => 'addedVehicles',
            6 => 'removedVehicles',
            7 => 'vehicleID',
            8 => 'vehicleName',
            9 => 'created_at',
          );
        } else if ($model == 'AuditOrg') {
          $columns = array(
            0 => 'id',
            1 => 'fcode',
            2 => 'organizationName',
            3 => 'userName',
            4 => 'status',
            5 => 'description',
            6 => 'email',
            7 => 'address',
            8 => 'mobile',
            9 => 'startTime',
            10 => 'endTime',
            11 => 'atc',
            12 => 'etc',
            13 => 'mtc',
            14 => 'parkingAlert',
            15 => 'idleAlert',
            16 => 'parkDuration',
            17 => 'idleDuration',
            18 => 'overspeedalert',
            19 => 'sendGeoFenceSMS',
            20 => 'radius',
            21 => 'smsSender',
            22 => 'sosAlert',
            23 => 'live',
            24 => 'smsProvider',
            25 => 'providerUserName',
            26 => 'providerPassword',
            27 => 'geofense',
            28 => 'safemove',
            29 => 'deleteHistoryEod',
            30 => 'harshBreak',
            31 => 'dailySummary',
            32 => 'dailyDieselSummary',
            33 => 'fuelLevelBelow',
            34 => 'fuelLevelBelowValue',
            35 => 'noDataDuration',
            36 => 'SchoolGeoFence',
            37 => 'morningEntryStartTime',
            38 => 'morningEntryEndTime',
            39 => 'eveningEntryStartTime',
            40 => 'eveningEntryEndTime',
            41 => 'morningExitStartTime',
            42 => 'morningExitEndTime',
            43 => 'eveningExitStartTime',
            44 => 'eveningExitEndTime',
            45 => 'isRfid',
            46 => 'PickupStartTime',
            47 => 'PickupEndTime',
            48 => 'DropStartTime',
            49 => 'DropEndTime',
            50 => 'escalationsms',
            51 => 'smsList',
            52 => 'created_at',
          );
        } else if (($model == 'CustomizeLandingPage')) {
          $columns = array(
            0 => 'id',
            1 => 'fcode',
            2 => 'userName',
            3 => 'userIpAddress',
            4 => 'status',
            5 => 'oldLandingPage',
            6 => 'newLandingPage',
            7 => 'created_at',
          );
        } else if (($model == 'loginCustomisation')) {

          $columns = array(
            0 => 'id',
            1 => 'username',
            2 => 'fcode',
            3 => 'status',
            4 => 'userIpAddress',
            5 => 'Domain',
            6 => 'Template',
            7 => 'bcolor',
            8 => 'bcolor1',
            9 => 'fcolor',
            10 => 'logo',
            11 => 'background',
            12 => 'created_at',
          );
        } else {
          $columns = [];
        }
      }
      AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
    } catch (Exception $e) {
      logger($e->getMessage());
    }

    if ($model == 'AuditFrans' && strpos($username, 'admin') !== false) {
      if (session()->get('cur1') == 'prePaidAdmin') {
        unset($columns[array_search('id', $columns)], $columns[array_search('fcode', $columns)], $columns[array_search('numberofLicence', $columns)], $columns[array_search('availableLincence', $columns)], $columns[array_search('addLicence', $columns)], $columns[array_search('username', $columns)]);
        //$columns = str_replace('Details','Shuffled Licences', $columns);
      } else {
        unset($columns[array_search('id', $columns)], $columns[array_search('fcode', $columns)], $columns[array_search('numberofBasicLicence', $columns)], $columns[array_search('numberofAdvanceLicence', $columns)], $columns[array_search('numberofPremiumLicence', $columns)], $columns[array_search('numberofPremPlusLicence', $columns)], $columns[array_search('availableBasicLicence', $columns)], $columns[array_search('availableAdvanceLicence', $columns)], $columns[array_search('availablePremiumLicence', $columns)], $columns[array_search('availablePremPlusLicence', $columns)], $columns[array_search('addBasicLicence', $columns)], $columns[array_search('addPremiumLicence', $columns)], $columns[array_search('addPrePlusLicence', $columns)], $columns[array_search('username', $columns)], $columns[array_search('addAdvanceLicence', $columns)], $columns[array_search('Details', $columns)]);
      }
    }
    if ($model == 'AuditDealer') {
      if (session()->get('cur1') == 'prePaidAdmin') {
        unset($columns[array_search('id', $columns)], $columns[array_search('fcode', $columns)]);
        //$columns = str_replace('Details','Shuffled Licences', $columns);
      } else {
        unset($columns[array_search('id', $columns)], $columns[array_search('fcode', $columns)], $columns[array_search('numofBasic', $columns)], $columns[array_search('numofAdvance', $columns)], $columns[array_search('numofPremium', $columns)], $columns[array_search('numofPremiumPlus', $columns)], $columns[array_search('avlofBasic', $columns)], $columns[array_search('avlofAdvance', $columns)], $columns[array_search('avlofPremium', $columns)], $columns[array_search('avlofPremiumPlus', $columns)], $columns[array_search('addBasicLicence', $columns)], $columns[array_search('addAdvanceLicence', $columns)], $columns[array_search('addPremiumLicence', $columns)], $columns[array_search('addPrePlusLicence', $columns)], $columns[array_search('Details', $columns)]);
      }
    }
    if ($model == 'AuditUser') {
      unset($columns[array_search('id', $columns)], $columns[array_search('fcode', $columns)], $columns[array_search('password', $columns)]);
    }
    if ($model == 'AuditVehicle' && session()->get('cur1') == 'prePaidAdmin') {
      unset($columns[array_search('vehicleExpiry', $columns)]);
    }
    // added for reports
    $AllReport = [];
    $RemovedReportArr = [];
    $AddedReportArr = [];
    if ($model == 'AuditFrans' || $model == 'AuditDealer' || $model == 'AuditUser') {

      if (count($rows) > 0) {
        $ReportArr = [];
        $singleReport = [];
        foreach ($rows as $keys => $values) {
          $Consolidatedvehicles = [];
          $Useradmin = [];
          $Analytics = [];
          $Sensor = [];
          $Statistics = [];
          $Tracking = [];
          $Performance = [];
          $reportColumns = ['reports', 'addedReports', 'removedReports'];
          if ($values['status'] == config()->get('constant.reportsEdited') || ($values['status'] == config()->get('constant.created') && $model == 'AuditUser')) {
            foreach ($reportColumns as $reportColumn) {
              $Consolidatedvehicles = [];
              $Useradmin = [];
              $Analytics = [];
              $Sensor = [];
              $Statistics = [];
              $Tracking = [];
              $Performance = [];
              $Fuel = [];
              if ($reportColumn == 'reports') {
                $ReportArr = explode(',', $values['reports']);
              } else if ($reportColumn == 'addedReports') {
                $ReportArr = explode(',', $values['addedReports']);
              } else if ($reportColumn == 'removedReports') {
                $ReportArr = explode(',', $values['removedReports']);
              }


              //return $ReportArr;

              foreach ($ReportArr as $report) {

                if (strpos($report, 'Consolidatedvehicles') !== false) {
                  array_push($Consolidatedvehicles, explode(':', $report)[0]);
                } else if (strpos($report, 'Analytics') !== false) {
                  array_push($Analytics, explode(':', $report)[0]);
                } else if (strpos($report, 'Sensor') !== false) {
                  array_push($Sensor, explode(':', $report)[0]);
                } else if (strpos($report, 'Statistics') !== false) {
                  array_push($Statistics, explode(':', $report)[0]);
                } else if (strpos($report, 'Tracking') !== false) {
                  array_push($Tracking, explode(':', $report)[0]);
                } else if (strpos($report, 'Useradmin') !== false) {
                  array_push($Useradmin, explode(':', $report)[0]);
                } else if (strpos($report, 'Performance') !== false) {
                  array_push($Performance, explode(':', $report)[0]);
                }else if(strpos($report, 'Fuel') !== false){
                  array_push($Fuel, explode(':', $report)[0]);
                }
              }
              $singleReport['Consolidatedvehicles'] = $Consolidatedvehicles;
              //return $singleReport['Consolidatedvehicles'];
              $singleReport['Analytics'] = $Analytics;
              $singleReport['Sensor'] = $Sensor;
              $singleReport['Statistics'] = $Statistics;
              $singleReport['Tracking'] = $Tracking;
              $singleReport['Useradmin'] = $Useradmin;
              $singleReport['Performance'] = $Performance;
              $singleReport['Fuel'] = $Fuel;
              //$AllReport[$keys]=$singleReport;
              if (count($ReportArr) > 0) {
                if ($reportColumn == 'reports') {
                  $AllReport[$keys] = $singleReport;
                  //return $singleReport;
                } else if ($reportColumn == 'addedReports')
                  $AddedReportArr[$keys] = $singleReport;
                else if ($reportColumn == 'removedReports') {
                  $RemovedReportArr[$keys] = $singleReport;
                }
              }

            }
          }
        }
      }
    }
    
    if ($fcode == 'VAMOSYS') {
      $count = VdmFranchiseController::liveVehicleCountV2();
      $apnKey = $redis->exists('Update:Apn');
      $timezoneKey = $redis->exists('Update:Timezone');
      $auditData=[
        'columns' => $columns, 
        'rows' => $rows, 
        'AllReport' => $AllReport, 
        'AddedReportArr' => $AddedReportArr, 
        'RemovedReportArr' => $RemovedReportArr,
        'model'=>$model,
        'apnKey'=>$apnKey,
        'timezoneKey'=>$timezoneKey,
        'count'=>$count,
      ];
      return view('vdm.franchise.audit', $auditData);
    } else {
      unset($columns[array_search('id', $columns)], $columns[array_search('fcode', $columns)]);
      $indexData=[
        'columns' => $columns, 
        'rows' => $rows, 
        'AllReport' => $AllReport, 
        'AddedReportArr' => $AddedReportArr, 
        'RemovedReportArr' => $RemovedReportArr,
        'model'=>$model,
      ];
      // return ($rows->first());
      return view('vdm.audit.index', $indexData);
    }
  }
}
