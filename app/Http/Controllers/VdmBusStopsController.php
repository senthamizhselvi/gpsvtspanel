<?php


namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redis;

class VdmBusStopsController extends Controller
{




    /**
     * 
     * This show is invoked from vdm school controller
     * 
     */
    public function show($id)
    {

        $in = explode(":", $id);
        
        $username = auth()->id();
        $redis = Redis::connection();
        $orgId = $in[0];
        $routeNo = $in[1];
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $morningList = $redis->get('K_Morning_StopSeq_' . $orgId . '_' .  $routeNo . '_' . $fcode);

        $eveningList = $redis->get('K_Evening_StopSeq_' . $orgId . '_' .  $routeNo . '_' . $fcode);

        $stopList = explode(',', $morningList);

        $mobileNosList = array();
        $stopNameList = array();

        foreach ($stopList as $stop) {
            $stopData = $redis->hget('H_Bus_Stops_' . $orgId . '_' . $fcode, $routeNo . ':stop' . $stop);

            //H_Bus_Stops_PONV1_SMO -- R1:1
            $stopJson = json_decode($stopData, true);
            $mobileNosList = Arr::add($mobileNosList, 'stopName:' . $stop,  $stopJson['mobileNo']);
            $stopNameList = Arr::add($stopNameList, 'stopName:' . $stop,  $stopJson['stopName']);
        }

        return view('vdm.busStops.index', array(
            'stopNameList' => $stopNameList,
            'routeNo' => $routeNo,
            'orgId' => $orgId,
            'morningList' => $morningList,
            'eveningList' => $eveningList,
            'mobileNosList' => $mobileNosList
        ));
    }



    public function edit($id)
    {

        
        $username = auth()->id();
        $redis = Redis::connection();
        $in = explode(":", $id);
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $schoolId = $in[0];
        $routeNo = $in[1];
        $routeType = $in[2];
        $stopNo = $in[3];
        $stopDetails = $redis->hget('H_Bus_Stops_' . $schoolId . '_' . $fcode, $routeNo . ':' . $routeType . ':' . $stopNo);
        var_dump($stopDetails);
    }
}
