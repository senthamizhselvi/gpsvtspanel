<?php

namespace App\Http\Controllers;

use Input;
use Illuminate\Support\Facades\Redis;
use Predis\Client as Client;
use App\Exports\VehiclesExport;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Facades\Excel;

class VdmVehicleScanController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
	
     */
    public function index()
    {
        session()->forget('page');
        session()->put('vCol', 1);
        $orgLis = [];
        return view('vdm.vehicles.vehicleScan', ['vehicleList' => $orgLis]);
    }
    public function store()
    {
        $text_word1 = request()->get('text_word');
        return self::scanNew($text_word1);
    }

    public static function scanNew($id)
    {
        
        $username = auth()->id();
        session()->forget('page');
        session()->put('vCol', 1);
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $franDetails_json = $redis->hget('H_Franchise', $fcode);
        $franchiseDetails = json_decode($franDetails_json, true);
        $prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
        if (session()->get('cur') == 'dealer') {
            $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
            $vehicleNameMob = 'H_VehicleName_Mobile_Dealer_' . $username . '_Org_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $vehicleListId = 'S_Vehicles_' . $fcode;
            $vehicleNameMob = 'H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode;
        } else {
            $vehicleListId = 'S_Vehicles_' . $fcode;
            $vehicleNameMob = 'H_VehicleName_Mobile_Org_' . $fcode;
        }
        $text_word1 = trim($id);
        $text_trim = str_replace(' ', '', $text_word1);
        $text_word = strtoupper($text_trim);
        $vehicleList = $redis->smembers($vehicleListId);
        //$cou = $redis->scard($vehicleListId);
        $cou = $redis->hlen($vehicleNameMob);
        $slave = $redis->hget('H_Redis_Slave', 'Slave1');
        $redis1 = new Client(array(
            'host'     => $slave,
            'password' => 'ahanram@vamosystems.in',
            'database' => 0,
        ));
        $franRefData = $redis->hget('H_Franchise', $fcode);
        $franjsonData = json_decode($franRefData, true);
        if (session()->get('cur') == 'admin') {
            $check = $redis->hget('H_RefData_' . $fcode, $text_word);

            if ($check !== null) {
                $orgLl = array();
                $orgLl = Arr::add($orgLl, $text_word, $text_word);
                $orgL = $orgLl;
            } else {
                $orgLi = $redis1->HScan($vehicleNameMob, 0,  'count', $cou, 'match', '*' . $text_word . '*');
                $orgL = $orgLi[1];
            }
        } else {
            $orgLi = $redis1->HScan($vehicleNameMob, 0,  'count', $cou, 'match', '*' . $text_word . '*');
            $orgL = $orgLi[1];
        }
        $redis1->quit();
        $deviceList = null;
        $deviceId = null;
        $shortName = null;
        $shortNameList = null;
        $portNo = null;
        $portNoList = null;
        $mobileNo = null;
        $mobileNoList = null;
        $orgIdList = null;
        $deviceModelList = null;
        $expiredList = null;
        $statusList = null;
        //$onboardDate = null;
        $onboardDateList = null;
        $owner = null;
        $licenceissuedDateList = null;
        $commList = null;
        $licenceIdList = null;
        $LicexpiredPeriodList = null;
        $timezoneList = null;
        $apnList = null;
        $calibrateType = null;
        $sensorCount = '1';
        $noOfTank = '1';
        $vehicleFuelData = null;
        $sensorCountList =null;
        $noOfTankList =null;
        $calibrateTypeList=null;
        $licTypeList=null;
        if (request()->server('HTTP_HOST') === "localhost") {
            $orgL = array_slice($orgL,0,20);
        }
        // $orgL  = array_unique(array_values($orgL));
        foreach ($orgL as $key => $vehicle) {
            $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);
            if($vehicleRefData == null || trim($vehicle) == ""){
                unset($orgL[$key]);
                continue;
            } 

            $pattern = '/[ ]{2,}|[\t]/';
            if (preg_match($pattern, $vehicle)) {
                info('\t contains vehicle id '.$id);
                $vehicleId = preg_replace('/[ ]{2,}|[\t]/', '', trim($vehicle));
                VdmVehicleController::vehicleIdMig($vehicleId, $id, null, "yes");
            }
            //logger($key.'$vehicle ' .$vehicle);

            $vehicleRefData = json_decode($vehicleRefData, true);
            $deviceId = isset($vehicleRefData['deviceId']) ? $vehicleRefData['deviceId'] : '';
            $sensorCount = isset($vehicleRefData['sensorCount']) ? $vehicleRefData['sensorCount'] : '1';
            $noOfTank = isset($vehicleRefData['noOfTank']) ? $vehicleRefData['noOfTank'] : '1';
            $sensorCountList = Arr::add($sensorCountList,$vehicle,$sensorCount);
            $noOfTankList = Arr::add($noOfTankList,$vehicle,$noOfTank);

            $vehicleFuelData = $redis->hget('H_Ref_FuelData_' . $fcode, $vehicle);
            $vehicleFuelData = json_decode($vehicleFuelData, true);

            // $vehicleProData = $redis->hget('H_ProData_' . $fcode, $vehicle);
            // $vehicleProDataList = explode(',',$vehicleProData);
            // $direct = isset($vehicleProDataList[17]) ? $vehicleProDataList[17] : '-1';
            // $fuelType = isset($vehicleRefData['fuelType'])?$vehicleRefData['fuelType']:'serial';
            // $tankshape = isset($vehicleFuelData['tankshape']) ? $vehicleFuelData['tankshape'] : 'Abnormal';
            // if($direct == '-1' && $fuelType == 'serial'){
            //     $calibrateType = "direct";
            // }else if ($tankshape !== "Abnormal" && $tankshape !=="") {
            //     $calibrateType = 'auto';
            // } else {
            //     $calibrateType = 'manual';
            // }
            // $calibrateTypeList = Arr::add($calibrateTypeList,$vehicle,$calibrateType);
            // $calibrateType = count($redis->zrange('Z_Sensor_' . $vehicle . '_' . $fcode, 0, -1, 'withscores')) > 0 ? 'manual' : 'auto';

            //$deviceId = $vehicleRefData['deviceId'];
            if ((session()->get('cur') == 'dealer' &&  $redis->sismember('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId) == 0) || session()->get('cur') == 'admin') {
                if ($prepaid == 'yes') {
                    $LicenceId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vehicle);
                    $licenceIdList = Arr::add($licenceIdList, $vehicle, $LicenceId);
                    $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
                    $LicenceRefData = json_decode($LicenceRef, true);
                    $licenceissued = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
                    $licenceissuedDateList = Arr::add($licenceissuedDateList, $vehicle, $licenceissued);
                    $LicenceexpiredPeriod = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
                    $LicexpiredPeriodList = Arr::add($LicexpiredPeriodList, $vehicle, $LicenceexpiredPeriod);
                } else {
                    $licenceissued = isset($vehicleRefData['licenceissuedDate']) ? $vehicleRefData['licenceissuedDate'] : '';
                    $licenceissuedDateList = Arr::add($licenceissuedDateList, $vehicle, $licenceissued);
                }
                // lic list
                $licType = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
                $licTypeList = Arr::add($licTypeList, $vehicle, $licType);

                $deviceList = Arr::add($deviceList, $vehicle, $deviceId);
                $shortName = isset($vehicleRefData['shortName']) ? $vehicleRefData['shortName'] : 'nill';
                $shortNameList = Arr::add($shortNameList, $vehicle, $shortName);
                $portNo = isset($vehicleRefData['portNo']) ? $vehicleRefData['portNo'] : 9964;
                $portNoList = Arr::add($portNoList, $vehicle, $portNo);
                $mobileNo = isset($vehicleRefData['gpsSimNo']) ? $vehicleRefData['gpsSimNo'] : 99999;
                $mobileNoList = Arr::add($mobileNoList, $vehicle, $mobileNo);
                $orgId = isset($vehicleRefData['orgId']) ? $vehicleRefData['orgId'] : 'Default';
                $orgIdList = Arr::add($orgIdList, $vehicle, $orgId);
                $deviceModel = isset($vehicleRefData['deviceModel']) ? $vehicleRefData['deviceModel'] : 'nill';
                $deviceModelList = Arr::add($deviceModelList, $vehicle, $deviceModel);
                $expiredPeriod = isset($vehicleRefData['vehicleExpiry']) ? $vehicleRefData['vehicleExpiry'] : '-';
                $countryTimezone = isset($vehicleRefData['country']) ? $vehicleRefData['country'] : '';
                if ($countryTimezone == '' || $countryTimezone == 'no' || $countryTimezone == 'MIT' || $countryTimezone == "null") {
                    $countryTimezone = isset($franjsonData['timeZone']) ? $franjsonData['timeZone'] : 'Asia/Kolkata';
                }
                $nullval = strlen($expiredPeriod);
                if ($nullval == 0 || $expiredPeriod == "null") {
                    $expiredPeriod = '-';
                }

                $expiredList = Arr::add($expiredList, $vehicle, $expiredPeriod);

                $date = isset($vehicleRefData['date']) ? $vehicleRefData['date'] : '';

                if ($date == '' || $date == ' ') {
                    $date1 = '';
                } else {
                    $date1 = date("d-m-Y", strtotime($date));
                }
                $onDate = isset($vehicleRefData['onboardDate']) ? $vehicleRefData['onboardDate'] : $date1;
                $nullval1 = strlen($onDate);

                if ($nullval1 == 0 || $onDate == "null" || $onDate == " ") {
                    $onboardDate = $date1;
                } else {
                    $onboardDate = $onDate;
                }
                //$onboardDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:$date1;
                $onboardDateList = Arr::add($onboardDateList, $vehicle, $onboardDate);
                $statusVehicle = $redis->hget('H_ProData_' . $fcode, $vehicle);
                $statusSeperate = explode(',', $statusVehicle);
                $statusSeperate1 = isset($statusSeperate[7]) ? $statusSeperate[7] : 'N';
                // $statusList = Arr::add($statusList, $vehicle, $statusSeperate1);
                $ignition = isset($statusSeperate[16]) ? $statusSeperate[16] : 'OFF';
                if ($statusSeperate1 == 'O') {
                    if ($ignition == 'ON') {
                        $statusList = Arr::add($statusList, $vehicle, "S");
                    } else {
                        $statusList = Arr::add($statusList, $vehicle, "P");
                    }
                } else {
                    $statusList = Arr::add($statusList, $vehicle, $statusSeperate1);
                }
                $lastComm = isset($statusSeperate[36]) ? $statusSeperate[36] : '';
                if ($lastComm == '' || $lastComm == null) {
                    $setted = '';
                } else {
                    $sec = $lastComm / 1000;
                    //logger(" vehicle timezone ".$countryTimezone." vehicle ".$vehicle);
                    try {
                        date_default_timezone_set($countryTimezone);
                    } catch (\Exception $e) {
                        logger($e);
                        date_default_timezone_set('Asia/Kolkata');
                    }
                    $datt = date("Y-m-d", $sec);
                    $time1 = date("H:i:s", $sec);
                    //$endTime = strtotime("+330 minutes", strtotime($time1));
                    //$time2=date('G:i:s', $endTime);
                    $setted = $datt . ' ' . $time1;
                    date_default_timezone_set('Asia/Kolkata');
                }
                $commList = Arr::add($commList, $vehicle, $setted);
                $owntype = isset($vehicleRefData['OWN']) ? $vehicleRefData['OWN'] : '';
                $owner = Arr::add($owner, $vehicle, $owntype);

                //timezone and apn
                $timezoneData = $redis->hget('H_DeviceDetail_' . $fcode, $vehicle);
                $timezoneRefData = json_decode($timezoneData, true);
                $timezone = isset($timezoneRefData['timezone']) ? $timezoneRefData['timezone'] : '-';
                $timezoneList = Arr::add($timezoneList, $vehicle, $timezone);
                $apn = isset($timezoneRefData['apn']) ? $timezoneRefData['apn'] : '-';
                $apnList = Arr::add($apnList, $vehicle, $apn);
            } else {
                unset($orgL[$key]);
            }
        }
        $demo = 'ahan';
        $user = null;
        $user1 = new VdmDealersController;
        $user = $user1->checkuser();
        $dealerId = $redis->smembers('S_Dealers_' . $fcode);
        $orgArr = array();
        foreach ($dealerId as $org) {
            $orgArr = Arr::add($orgArr, $org, $org);
        }
        $dealerId = $orgArr;
        sort($orgL, SORT_NATURAL | SORT_FLAG_CASE);
        $vehicleScanData = [
            'vehicleList' => $orgL,
            'licTypeList'=>$licTypeList,
            'deviceList' => $deviceList,
            'sensorCountList' => $sensorCountList,
            'noOfTankList' => $noOfTankList,
            'calibrateTypeList' => $calibrateTypeList,
            'owntype' => $owner,
            'shortNameList' => $shortNameList,
            'portNoList' => $portNoList,
            'mobileNoList' => $mobileNoList,
            'demo' => $demo,
            'user' => $user,
            'orgIdList' => $orgIdList,
            'deviceModelList' => $deviceModelList,
            'expiredList' => $expiredList,
            'tmp' => 0,
            'statusList' => $statusList,
            'dealerId' => $dealerId,
            'onboardDateList' => $onboardDateList,
            'licenceissuedDate' => $licenceissuedDateList,
            'commList' => $commList,
            'licenceIdList' => $licenceIdList,
            'LicexpiredPeriodList' => $LicexpiredPeriodList,
            'timezoneList' => $timezoneList,
            'apnList' => $apnList
        ];
        return view('vdm.vehicles.vehicleScan', $vehicleScanData);
    }

    public function sendExcel()
    {   
        return Excel::download(new VehiclesExport(), 'VehicleList.xlsx');
    }

    /*
* Show the form for creating a new resource.
* @return Response
*/
}
