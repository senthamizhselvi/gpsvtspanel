<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redis;

class VdmGeoFenceController extends Controller
{

    public function view($id)
    {

        
        $username = auth()->id();
        $redis = Redis::connection();
        $vehicle = $id;
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $gfJson = $redis->hget('H_Vehicle_' . $fcode . '_GF', $id);

        $in = json_decode($gfJson, true);

        return view('vdm.geoFence.view', $in);
    }

    public function edit($id)
    {

        
        $username = auth()->id();
        $redis = Redis::connection();
        $geoFenceId = $id;

        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $gfJson = $redis->hget('H_Vehicle_' . $fcode . '_GF', $id);

        $in = json_decode($gfJson, true);

        $poiName = isset($in['poiName']) ? $in['poiName'] : null;
        $mobileNos = isset($in['mobileNos']) ? $in['mobileNos'] : null;
        $geoLocation = isset($in['geoLocation']) ? $in['geoLocation'] : null;
        $geoAddress = isset($in['geoAddress']) ? $in['geoAddress'] : null;
        $direction = isset($in['direction']) ? $in['direction'] : null;
        $proximityLevel = isset($in['proximityLevel']) ? $in['proximityLevel'] : null;
        $geoFenceType = isset($in['geoFenceType']) ? $in['geoFenceType'] : null;
        $message = isset($in['message']) ? $in['message'] : null;
        $email = isset($in['email']) ? $in['email'] : null;


        return view('vdm.geoFence.edit', array(
            'geoFenceId' => $geoFenceId,
            'poiName' => $poiName,
            'mobileNos' => $mobileNos,
            'geoLocation' => $geoLocation,
            'geoAddress' => $geoAddress,
            'direction' => $direction,
            'proximityLevel' => $proximityLevel,
            'geoFenceType' => $geoFenceType,
            'message' => $message,
            'email' => $email
        ));
    }

    public function update($id)
    {
        
        //TODO Add validation

        $username = auth()->id();
        $poiName       = request()->get('poiName');
        $mobileNos       = request()->get('mobileNos');
        $geoLocation       = request()->get('geoLocation');
        $geoAddress       = request()->get('geoAddress');
        $direction       = request()->get('direction');
        $geoFenceType       = request()->get('geoFenceType');
        $proximityLevel       = request()->get('proximityLevel');
        $message           = request()->get('message');
        $email           = request()->get('email');
        $redis = Redis::connection();
        $poiList = null;
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $poiList = Arr::add($poiList, 'poiName', $poiName);
        $poiList = Arr::add($poiList, 'mobileNos', $mobileNos);
        $poiList = Arr::add($poiList, 'geoLocation', $geoLocation);
        $poiList = Arr::add($poiList, 'geoAddress', $geoAddress);
        $poiList = Arr::add($poiList, 'direction', $direction);
        $poiList = Arr::add($poiList, 'geoFenceType', $geoFenceType);
        $poiList = Arr::add($poiList, 'proximityLevel', $proximityLevel);
        $poiList = Arr::add($poiList, 'message', $message);
        $poiList = Arr::add($poiList, 'email', $email);

        $out = json_encode($poiList);

        $redis->hset('H_Vehicle_' . $fcode . '_GF', $id, $out);
        logger(' update id ' . $id);
        $tok = explode(':', $id);
        $vehicleId = $tok[0];
        session()->flash('message', 'Successfully updated ' . $id . '!');

        return redirect()->to('vdmGeoFence/' . $vehicleId);
    }

    public function create()
    {
        
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $vehicleList = $redis->smembers('S_Vehicles_' . $fcode);

        $userVehicles = null;

        foreach ($vehicleList as $key => $value) {
            $userVehicles = Arr::add($userVehicles, $value, $value);
        }
        $vehicleList = $userVehicles;

        return view('vdm.geoFence.create',['vehicleList'=> $vehicleList]);
    }

    public function store()
    {
        
        //TODO Add validation
        $rules = array(
            'vehicleId' => 'required',
            'geoFenceId' => 'required',
            'poiName' => 'required'

        );
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            return redirect()->to('vdmGeoFence/create')->withErrors($validator);
        } else {
            $username = auth()->id();
            $vehicleId      = request()->get('vehicleId');
            $geoFenceId       = request()->get('geoFenceId');
            $poiName       = request()->get('poiName');
            $mobileNos       = request()->get('mobileNos');
            $geoLocation       = request()->get('geoLocation');
            $geoAddress       = request()->get('geoAddress');
            $direction       = request()->get('direction');
            $geoFenceType       = request()->get('geoFenceType');
            $proximityLevel       = request()->get('proximityLevel');
            $message        = request()->get('message');
            $email        = request()->get('email');
            $redis = Redis::connection();
            $poiList = null;

            $poiList = Arr::add($poiList, 'geoFenceId', $geoFenceId);
            $poiList = Arr::add($poiList, 'poiName', $poiName);
            $poiList = Arr::add($poiList, 'mobileNos', $mobileNos);
            $poiList = Arr::add($poiList, 'geoLocation', $geoLocation);
            $poiList = Arr::add($poiList, 'geoAddress', $geoAddress);
            $poiList = Arr::add($poiList, 'direction', $direction);
            $poiList = Arr::add($poiList, 'geoFenceType', $geoFenceType);
            $poiList = Arr::add($poiList, 'proximityLevel', $proximityLevel);
            $poiList = Arr::add($poiList, 'message', $message);
            $poiList = Arr::add($poiList, 'email', $email);

            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $out = json_encode($poiList);

            $redis->rpush('L_' . $vehicleId . '_' . $fcode . '_GF', $geoFenceId);
            $redis->hset('H_Vehicle_' . $fcode . '_GF', $vehicleId . ':' . $geoFenceId, $out);
            $id = $vehicleId;

            // redirect
            session()->flash('message', 'Successfully updated ' . $vehicleId . ':' . $geoFenceId . '!');

            return redirect()->to('vdmGeoFence/' . $id);
        }
    }


    public function destroy($id)
    {
        
        $username = auth()->id();
        $redis = Redis::connection();
        $tok = explode(':', $id);
        $vehicleId = $tok[0];
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $redis->lrem('L_' . $vehicleId . '_' . $fcode . '_GF', 1, $tok[1]);
        $redis->hdel('H_Vehicle_' . $fcode . '_GF', $id);
        session()->flash('message', 'Successfully deleted ' . $id . '!');
        return redirect()->to('vdmGeoFence/' . $vehicleId);
    }
}
