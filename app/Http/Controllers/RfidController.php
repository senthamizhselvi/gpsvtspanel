<?php

namespace App\Http\Controllers;

use App\Exports\AddtagsExport;

use Illuminate\Support\Facades\Redis;
use App\Models\User;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class RfidController extends Controller
{

    public function show($id)
    {
        return $this->showTags();
    }

    public function create()
    {
        return redirect('Business')->with('addtagPopUp','true');
        // return view('vdm.rfid.create');
    }

    public function create1()
    {
        
        $id = 1;
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        if (session()->get('cur') == 'dealer') {
            $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $vehicleListId = 'S_Vehicles_Admin_' . $fcode;
        } else {
            $vehicleListId = 'S_Vehicles_' . $fcode;
        }
        $vehicleList = $redis->smembers($vehicleListId);
        $vehRfidYesList = null;
        foreach ($vehicleList as $vehicle) {

            $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);

            if (isset($vehicleRefData)) {
                logger('$vehicle ' . $vehicleRefData);
            } else {
                continue;
            }
            $vehicleRefData = json_decode($vehicleRefData, true);
            $isRfid = isset($vehicleRefData['isRfid']) ? $vehicleRefData['isRfid'] : 'no';
            if ($isRfid == 'yes') {
                $vehRfidYesList = Arr::add($vehRfidYesList, $vehicle, $vehicle);
            }
        }
        return view('vdm.rfid.create', ['vehRfidYesList' => $vehRfidYesList]);
    }

    public function store($tag = 0, $error = null)
    {
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $tags = ['tags' => request()->get('tags')];
        if ($tag != 0) {
            $tags = ['tags' => $tag];
        }
        $rules = array(
            'tags' => 'required|numeric|min:1'
        );

        $validator = validator()->make($tags, $rules);

        if ($validator->fails()) {
            return redirect()->to('rfid/create')->withErrors($validator);
        } else {
            $tags = request()->get('tags');
            session([
                'showAddTags'=> $tags
            ]);
            return  redirect()->route('showAddTags');
            // if ($tag != 0) {
            //     $tags = $tag;
            // }

            // $orgListId = 'S_Organisations_' . $fcode;

            // if (session()->get('cur') == 'dealer') {
            //     $orgListId = 'S_Organisations_Dealer_' . $username . '_' . $fcode;
            // } else if (session()->get('cur') == 'admin') {
            //     $orgListId = 'S_Organisations_Admin_' . $fcode;
            // }

            // $orgList = $redis->smembers($orgListId);
            // sort($orgList);
            // $orgList = str_replace('.', '$', $orgList);
            // $orgArray = array();

            // foreach ($orgList as $org) {
            //     $orgArray = Arr::add($orgArray, $org, $org);
            // }
            // $vehRfidYesList = null;
            // $vehRfidYesList = Arr::add($vehRfidYesList, 'select', 'select');
            // $vehList = null;
            // $vehList = Arr::add($vehList, 'select', 'select');
            // $orgList = str_replace('$', '.', $orgArray);
            // $addTagsData = [
            //     'vehList' => $vehList,
            //     'tags' => $tags,
            //     'vehRfidYesList' => $vehRfidYesList,
            //     'orgList' => $orgList,
            // ];
            // return view('vdm.rfid.addTags', $addTagsData)->with(['sessionError'=>$error]);
        }
    }

    public function showAddTags()
    {
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $orgListId = 'S_Organisations_' . $fcode;

        if (session()->get('cur') == 'dealer') {
            $orgListId = 'S_Organisations_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $orgListId = 'S_Organisations_Admin_' . $fcode;
        }
        $orgList = $redis->smembers($orgListId);
        sort($orgList);
        $orgList = str_replace('.', '$', $orgList);
        $orgArray = array();

        foreach ($orgList as $org) {
            $orgArray = Arr::add($orgArray, $org, $org);
        }
        $vehRfidYesList = null;
        $vehRfidYesList = Arr::add($vehRfidYesList, 'select', 'select');
        $vehList = null;
        $vehList = Arr::add($vehList, 'select', 'select');
        $orgList = str_replace('$', '.', $orgArray);
        $tags = session('showAddTags');
        $addTagsData = [
            'vehList' => $vehList,
            'tags' => $tags,
            'vehRfidYesList' => $vehRfidYesList,
            'orgList' => $orgList,
        ];
        return view('vdm.rfid.addTags', $addTagsData);
    }

    public function addTags()
    {
        

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        // dd(request()->all());
        // store
        $tagArray = null;
        $tagid = null;
        $updatedTags = null;
        $tags = request()->get('tags');
        $tags = request()->get('tagid');
        $tagnames = request()->get('tagname');
        $mobiles = request()->get('mobile');
        $designations = request()->get('designation');
        $empids = request()->get('empid');
        $departments = request()->get('department');
        $org = request()->get('org');

        foreach ($tags as $key => $tag) {
            $tagid = trim($tag, "");
            $tagname = $tagnames[$key];
            $mobile = $mobiles[$key];
            $designation = $designations[$key];
            $empid = $empids[$key];
            $department = $departments[$key];
            if ($tagid !== null && $tagid !== '' && $mobile !== null && $mobile !== '' && $mobile !== 'select' && $org !== null && $org !== '' && $org !== 'select') {
                $val = $redis->sismember('S_Rfid_Tags_' . $fcode, $tagid);
                $val1 = $redis->hget('H_Rfid_Map', $tagid);
                if ($val == 1 || $val1 !== null) {
                    $tagArray = Arr::add($tagArray, $tagid, $tagid);
                } else {
                    $redis->sadd('S_Rfid_Tags_' . $fcode, $tagid);
                    $redis->hset('H_Rfid_Map', $tagid, $fcode);
                    $redis->sadd('S_Rfid_Org_' . $org . '_' . $fcode, $tagid);
                    $refDataArr = array(
                        'tagid' => $tagid,
                        'mobile' => $mobile,
                        'tagname' => $tagname,
                        'designation' => $designation,
                        'employeeId' => $empid,
                        'department' => $department,
                    );
                    $refDataJson = json_encode($refDataArr);
                    if (session()->get('cur') == 'dealer') {
                        $redis->hset('H_Rfid_Dealer_' . $org . '_' . $fcode, $tagid, $refDataJson);
                    } else if (session()->get('cur') == 'admin') {
                        $redis->hset('H_Rfid_Admin_' . $org . '_' . $fcode, $tagid, $refDataJson);
                    }
                    $updatedTags = Arr::add($updatedTags, $tagid, $tagid);
                }
            }
        }

        $error = '';
        $tagCount = 1;
        if (!is_null($tagArray)) {
            // return $tagArray;
            $tagCount = count($tagArray);
            $error = implode(", ", $tagArray);
            $error = ' Already exist tag id: ' . $error;
            if (!is_null($updatedTags)) {
                $updatedTags = implode(", ", $updatedTags);
                session()->flash('alert-class', "alert-success");
                session()->flash('message', 'Tags are created successfully ' . $updatedTags);
            }
        } elseif ($tagid != null) {
            if (!is_null($updatedTags)) {
                $updatedTags = implode(", ", $updatedTags);
                $error = 'Tags are created successfully ' . $updatedTags;
                // session()->flash('alert-class', "alert-success");
                // session()->flash('message', $error);
                // return $this->showTag($org);
                return redirect()->route("showTag",["orgId"=>$org])->with([
                    'alert-class'=>"alert-success",
                    'message'=> $error
                ]);
            }
        } else {
            $error = 'Enter the values in fields ' . $error;
        }
        return redirect()->back()->withInput()->withErrors($error);
    }

    public function index1()
    {
        return redirect()->route('showTag', [
            'orgId' => request()->get('orgid')
        ]);
    }

    public function index()
    {
        return $this->showTags();
    }

    public function getOrgList()
    {
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $orgListId = 'S_Organisations_' . $fcode;

        if (session()->get('cur') == 'dealer') {
            $orgListId = 'S_Organisations_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $orgListId = 'S_Organisations_Admin_' . $fcode;
        }
        $orgList = $redis->smembers($orgListId);
        sort($orgList);
        $orgList = str_replace('.', '$', $orgList);
        $orgArray = array();
        foreach ($orgList as $org) {
            $orgArray = Arr::add($orgArray, $org, $org);
        }
        $orgList = str_replace('$', '.', $orgArray);
        return $orgList;
    }

    public function showTag($orgId = null)
    {
    
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        if (session()->get('cur') == 'dealer') {
            $key = 'H_Rfid_Dealer_' . $orgId . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $key = 'H_Rfid_Admin_' . $orgId . '_' . $fcode;
        }
        $values = $redis->hgetall($key);
        $taglist = null;
        $distanceList = null;
        $mobileList = null;
        $belongsToList = null;
        $tagnameList = null;
        $swipevalueList = null;
        $designationList = null;
        $empIdList = null;
        //	$empNameList = null;
        $deptList = null;
        $tagKeys = array_keys($values);
        sort($tagKeys);
        foreach ($tagKeys as $key) {
            $valueT = json_decode($values[$key], true);
            $tagid = isset($valueT['tagid']) ? $valueT['tagid'] : 'nill';
            $taglist = Arr::add($taglist, $key, $tagid);
            $distance = isset($valueT['distance']) ? $valueT['distance'] : '0';
            $distanceList = Arr::add($distanceList, $key, $distance);
            $mobile = isset($valueT['mobile']) ? $valueT['mobile'] : 'nill';
            $mobileList = Arr::add($mobileList, $key, $mobile);
            $belongsTo = isset($valueT['belongsTo']) ? $valueT['belongsTo'] : 'nill';
            $belongsToList = Arr::add($belongsToList, $key, $belongsTo);
            $tagname = isset($valueT['tagname']) ? $valueT['tagname'] : 'nill';
            $tagnameList = Arr::add($tagnameList, $key, $tagname);
            $swipevalue = isset($valueT['swipevalue']) ? $valueT['swipevalue'] : 'nill';
            $swipevalueList = Arr::add($swipevalueList, $key, $swipevalue);
            $empId = isset($valueT['employeeId']) ? $valueT['employeeId'] : '-';
            $empIdList = Arr::add($empIdList, $key, $empId);
            $dept = isset($valueT['department']) ? $valueT['department'] : '-';
            $deptList = Arr::add($deptList, $key, $dept);
            $designation = isset($valueT['designation']) ? $valueT['designation'] : '-';
            if ($designation == '') {
                $designation = '-';
            } else {
                $designation = $designation;
            }
            $designationList = Arr::add($designationList, $key, $designation);
        }
        $update = "show";
        $orgList = $this->getOrgList();
        $indexData = [
            'values' => $values,
            'taglist' => $taglist,
            'distanceList' => $distanceList,
            'mobileList' => $mobileList,
            'belongsToList' => $belongsToList,
            'tagnameList' => $tagnameList,
            'orgId' => $orgId,
            'designationList' => $designationList,
            'form' => $update,
            'empIdList' => $empIdList,
            'deptList' => $deptList,
            'orgList' => $orgList,
            'show' => false,
        ];
        return view('vdm.rfid.index', $indexData);
    }

    public function showTags()
    {
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $orgList = $this->getOrgList();
        $orgId = '';
        $orgList[''] = '-- SELECT --';
        $keys = array_keys($orgList);
        sort($keys);
        $temp = [];
        foreach ($keys as $key) {
            $temp = Arr::set($temp, $key, $orgList[$key]);
        }
        $orgList  = $temp;

        $indexData = [
            'show' => true,
            'orgList' => $orgList,
            'orgId' => $orgId
        ];
        return view('vdm.rfid.index', $indexData);
    }

    public function destroy($id)
    {

        
        $username = auth()->id();
        $myArray = explode(';', $id);
        $id = $myArray[0];
        $tagid = $id;
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $redis->srem('S_Rfid_Tags_' . $fcode, $tagid);
        $redis->hdel('H_Rfid_Map', $tagid);

        if (session()->get('cur') == 'dealer') {
            $keyt = 'H_Rfid_Dealer_' . $myArray[1] . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $keyt = 'H_Rfid_Admin_' . $myArray[1] . '_' . $fcode;
        }
        $orgname = $myArray[1];
        $redis->srem('S_Rfid_Org_' . $orgname . '_' . $fcode, $tagid);
        $redis->hdel($keyt, $tagid);

        $error = 'Tags are Deleted successfully :'.$id;
        return redirect()->back()->with([ 
            'alert-class'=>"alert-success",
            'message'=> $error
        ]);
    }

    public function edit($id)
    {
        
        $username = auth()->id();

        $redis = Redis::connection();
        $myArray = explode(';', $id);
        $id = $myArray[0];
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $key = '';
        $vehicleId = $myArray[0];
        $orgId = $myArray[1];
        if (session()->get('cur') == 'dealer') {
            $key = 'H_Rfid_Dealer_' . $orgId . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $key = 'H_Rfid_Admin_' . $orgId . '_' . $fcode;
        }
        $values = $redis->hget($key, $id);

        $tagid = $id;

        $valueT = json_decode($values, true);
        $tagname = isset($valueT['tagname']) ? $valueT['tagname'] : 'nill';
        $orgname = $orgId;
        $mobile = isset($valueT['mobile']) ? $valueT['mobile'] : 'nill';
        $swipeby = isset($valueT['swipevalue']) ? $valueT['swipevalue'] : 'nill';
        $designation = isset($valueT['designation']) ? $valueT['designation'] : '';
        $employeeId = isset($valueT['employeeId']) ? $valueT['employeeId'] : '';
        //$employeeName=isset($valueT['employeeName'])?$valueT['employeeName']:'';
        $department = isset($valueT['department']) ? $valueT['department'] : '';
        $swipeby = explode(',', $swipeby);
        if (session()->get('cur') == 'dealer') {
            $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $vehicleListId = 'S_Vehicles_Admin_' . $fcode;
        } else {
            $vehicleListId = 'S_Vehicles_' . $fcode;
        }
        // $vehicleList = $redis->smembers($vehicleListId);
        // $vehRfidYesList = null;
        // $vehList = null;
        // $vehRfidYesList = Arr::add($vehRfidYesList, 'select', 'select');
        // $vehList = Arr::add($vehList, 'select', 'select');
        // foreach ($vehicleList as $vehicle) {
        //     $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);

        //     if (isset($vehicleRefData)) {
        //     } else {
        //         continue;
        //     }
        //     $vehicleRefData = json_decode($vehicleRefData, true);
        //     $isRfid = isset($vehicleRefData['isRfid']) ? $vehicleRefData['isRfid'] : 'no';
        //     $org = isset($vehicleRefData['orgId']) ? $vehicleRefData['orgId'] : 'Default';
        //     if ($isRfid == 'yes' && $org == $orgname) {
        //         $vehRfidYesList = Arr::add($vehRfidYesList, $vehicle, $vehicle);
        //     }
        //     if ($org == $orgname) {

        //         $vehList = Arr::add($vehList, $vehicle, $vehicle);
        //     }
        // }
        $orgListId = 'S_Organisations_' . $fcode;

        if (session()->get('cur') == 'dealer') {
            $orgListId = 'S_Organisations_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $orgListId = 'S_Organisations_Admin_' . $fcode;
        }


        $orgList = $redis->smembers($orgListId);
        $orgArray = array();
        $orgList = str_replace('.', '$', $orgList);
        foreach ($orgList as $org) {
            $orgArray = Arr::add($orgArray, $org, $org);
        }
        $orgList = str_replace('$', '.', $orgArray);

        $editData = [
            // 'vehList' => $vehList,
            // 'vehRfidYesList' => $vehRfidYesList,
            'tagid' => $tagid,
            'tagname' => $tagname,
            'orgname' => $orgname,
            'mobile' => $mobile,
            'swipeby' => $swipeby,
            'orgList' => $orgList,
            'designation' => $designation,
            'department' => $department,
            'employeeId' => $employeeId
        ];
        return view('vdm.rfid.edit', $editData);
    }

    public function update()
    {
        

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $key = '';
        $orgId = request()->get('org');
        if (session()->get('cur') == 'dealer') {
            $key = 'H_Rfid_Dealer_' . $orgId . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $key = 'H_Rfid_Admin_' . $orgId . '_' . $fcode;
        }

        // store
        $tagArray = null;

        $tagid = request()->get('tagid');
        $tagidtemp = request()->get('tagidtemp');
        $mobile = request()->get('mobile');
        $designation = request()->get('designation');
        $empid = request()->get('employeeId');
        $department = request()->get('department');
        $tagid = trim($tagid, "");
        $org = request()->get('org');
        $orgT = request()->get('orgT');

        if ($tagid !== null && $tagid !== '' && $mobile !== null && $mobile !== '' && $mobile !== 'select' && $org !== null && $org !== '' && $org !== 'select') {
            $val = $redis->sismember('S_Rfid_Tags_' . $fcode, $tagid);
            $val1 = $redis->hget('H_Rfid_Map', $tagid);
            if (($val == 1 || $val1 !== null) && $tagid !== $tagidtemp) {
                $tagArray = Arr::add($tagArray, $tagid, $tagid);
            } else {
                $tagname = request()->get('tagname');


                $redis->srem('S_Rfid_Tags_' . $fcode, $tagidtemp);
                $redis->sadd('S_Rfid_Tags_' . $fcode, $tagid);
                $redis->hdel('H_Rfid_Map', $tagidtemp);
                $redis->hset('H_Rfid_Map', $tagid, $fcode);
                $redis->srem('S_Rfid_Org_' . $orgT . '_' . $fcode, $tagidtemp);
                $redis->sadd('S_Rfid_Org_' . $org . '_' . $fcode, $tagid);

                $refDataArr = array(
                    'tagid' => $tagid,
                    'mobile' => $mobile,
                    'tagname' => $tagname,
                    'designation' => $designation,
                    'employeeId' => $empid,
                    'department' => $department,
                );
                $refDataJson = json_encode($refDataArr);

                if (session()->get('cur') == 'dealer') {

                    $redis->hdel('H_Rfid_Dealer_' . $orgT . '_' . $fcode, $tagidtemp);
                    $redis->hset('H_Rfid_Dealer_' . $org . '_' . $fcode, $tagid, $refDataJson);
                } else if (session()->get('cur') == 'admin') {

                    $redis->hdel('H_Rfid_Admin_' . $orgT . '_' . $fcode, $tagidtemp);
                    $redis->hset('H_Rfid_Admin_' . $org . '_' . $fcode, $tagid, $refDataJson);
                }
            }
        }
        $error = '';
        if ($tagArray != null) {
            $error = implode(" ", $tagArray);
            $error = 'Enter correct details for  ' . $error;
        } elseif ($tagid != null) {
            $error = 'Edit successfully ' . $error;
        } else {
            $error = 'Enter the values in fields ' . $error;
        }
        return redirect()->route("showTag",["orgId"=>$orgId])->with([
            'alert-class'=>"alert-success",
            'message'=> 'Successfully Tag Updated - ' . $tagid
        ]);

        $values = $redis->hget($key, $tagid);

        $valueT = json_decode($values, true);
        $tagid = isset($valueT['tagid']) ? $valueT['tagid'] : 'nill';
        $mobile = isset($valueT['mobile']) ? $valueT['mobile'] : 'nill';
        $belongsTo = isset($valueT['belongsTo']) ? $valueT['belongsTo'] : 'nill';
        $tagname = isset($valueT['tagname']) ? $valueT['tagname'] : 'nill';
        $designation = isset($valueT['designation']) ? $valueT['designation'] : '-';
        $employeeId = isset($valueT['employeeId']) ? $valueT['employeeId'] : '-';
        $department = isset($valueT['department']) ? $valueT['department'] : '-';
        if ($designation == '') {
            $designation = '-';
        } else {
            $designation = $designation;
        }
        $update = "update";
        $orgList = $this->getOrgList();
        $indexData = [
            'tagid' => $tagid,
            'mobile' => $mobile,
            'tagname' => $tagname,
            'orgId' => $orgId,
            'designation' => $designation,
            'form' => $update,
            'employeeId' => $employeeId,
            'department' => $department,
            'orgList' => $orgList,
            'show' => false
        ];
        session()->flash('alert-class', "alert-success");
        session()->flash('message', 'Successfully Tag Updated - ' . $tagid);
        return view('vdm.rfid.index', $indexData);
    }


    public function getVehicle()
    {
        

        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $orgId = request()->get('id');

        if (session()->get('cur') == 'dealer') {
            $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $vehicleListId = 'S_Vehicles_Admin_' . $fcode;
        } else {
            $vehicleListId = 'S_Vehicles_' . $fcode;
        }
        $vehicleList = $redis->smembers($vehicleListId);
        $vehRfidYesList = null;
        $vehList = null;
        $i = 0;
        $j = 0;
        $vehRfidYesList = Arr::add($vehRfidYesList, 'select', 'select');
        $vehList = Arr::add($vehList, 'select', 'select');
        foreach ($vehicleList as $vehicle) {

            $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);

            if (isset($vehicleRefData)) {
                logger('$vehicle ' . $vehicleRefData);
            } else {
                continue;
            }
            $vehicleRefData = json_decode($vehicleRefData, true);
            $org = isset($vehicleRefData['orgId']) ? $vehicleRefData['orgId'] : 'Default';
            $isRfid = isset($vehicleRefData['isRfid']) ? $vehicleRefData['isRfid'] : 'no';

            if ($isRfid == 'yes' && $org == $orgId) {
                $i++;
                $vehRfidYesList = Arr::add($vehRfidYesList, $vehicle, $vehicle);
            }
            if ($org == $orgId) {
                $j++;
                $vehList = Arr::add($vehList, $vehicle, $vehicle);
            }
        }



        $refDataArr = array(
            'rfidlist' => $vehRfidYesList,
            'vehicle' => $vehList
        );

        return response()->json($refDataArr);
    }

    //Download Tags
    public function downloadTags()
    {
        // return Excel::download(new AddtagsExport(), 'addTags.xlsx');
        $numberoftags = request()->get('numberoftags');
        $nod = $numberoftags + 1;
        $type = 'xls';
        $data = ['Vehicle_Id', 'Device_Id', 'GPS Sim No', 'Vehicle Name', 'Vehicle Type', 'Register No'];
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getProtection()->setSheet(true);
        $sheet->getStyle('A2:F' . $nod)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
        $sheet
            ->SetCellValue('A1', 'Tag Id')
            ->SetCellValue('B1', 'Mobile Number')
            ->SetCellValue('C1', 'Tag Name / Employee Name')
            ->SetCellValue('D1', 'Designation')
            ->SetCellValue('E1', 'Employee Id')
            ->SetCellValue('F1', 'Department');
        $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(25);
        $sheet->getStyle('A1:F1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
        $sheet->getStyle('A1:F1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

        $sheet->getStyle('A1:F1')->getAlignment()->applyFromArray(
            array('horizontal' => 'center', 'text-transform' => 'uppercase')
        );
        $sheet->getStyle('A2:F2')->getAlignment()->setWrapText(true);
        $designation = 'Driver,Helper,Staff,Student';
        $values = ['D' => $designation];
        foreach ($values as $key => $value) {
            for ($i = 2; $i <= $nod; $i++) {

                $objValidation = $sheet->getCell($key . $i)->getDataValidation();
                $objValidation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                $objValidation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
                $objValidation->setAllowBlank(false);
                $objValidation->setShowInputMessage(true);
                $objValidation->setShowErrorMessage(true);
                $objValidation->setShowDropDown(true);
                $objValidation->setErrorTitle('Input error');
                $objValidation->setError('Value is not in list.');
                $objValidation->setPromptTitle('Pick from list');
                $objValidation->setPrompt('Please pick a value from the drop-down list.');
                $objValidation->setFormula1('"' . $value . '"'); //note this!

            }
        }
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Tags.xlsx"');
        $writer->save("php://output");
    }

    public function uploadTags()
    {
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $tagArray = null;
        $updatedTags = null;
        $tag = request()->get('tag');
        $insert = [];
        if (request()->hasFile('import_file')) {
            $filename = request()->file('import_file')->getClientOriginalName();
            if ((strpos($filename, '.xls') == false) && (strpos($filename, '.csv') == false) && (strpos($filename, '.xlsx') == false)) {
                $error = 'Please upload valid extension(.xls,.csv) file';
                // back to same page.
                return redirect()->back()->withInput()->withErrors($error);
                // return $this->store($tag, $error);
            } else {
                $path = request()->file('import_file')->getRealPath();
                // $reader = IOFactory::createReader('Xlsx');
                // $spreadsheet = $reader->load($path);
                $spreadsheet = IOFactory::load($path);
                $datas = $spreadsheet->getActiveSheet()->toArray();
                if(count($datas[0])>6 || $datas[0][0] !== "Tag Id") {
                    $error = 'Please upload valid extension(.xls,.csv) file';
                    return redirect()->back()->withInput()->withErrors($error);
                    // return $this->store($tag, $error);
                }
                unset($datas[0]);

                if (!empty($datas) && count($datas)) {
                    foreach ($datas as $key => $value) {
                        $insert[] = [
                            'tagId' => trim($value[0]),
                            'mobileNo' => $value[1],
                            'tagName' => $value[2],
                            'designation' => $value[3],
                            'employeeId' => $value[4],
                            'department' => $value[5]
                        ];
                    }
                }
            }
            $numberoftags = request()->get('numberoftags');
            if (count($insert) < $numberoftags) {
                return redirect()->back()->withInput()->withErrors('Please upload number of devices that you entered');
            } else {
                $org = request()->get('org');
                for ($i = 0; $i < $numberoftags; $i++) {
                    $tagValue = $insert[$i];
                    $tagid = $tagValue['tagId'];
                    $mobile = $tagValue['mobileNo'];
                    $tagname = $tagValue['tagName'];
                    $designation = $tagValue['designation'];
                    $employeeId = $tagValue['employeeId'];
                    $department = $tagValue['department'];
                    if ($tagid !== null && $tagid !== '' && $mobile !== null && $mobile !== '' && $mobile !== 'select' && $org !== null && $org !== '' && $org !== 'select') {
                        $val = $redis->sismember('S_Rfid_Tags_' . $fcode, $tagid);
                        $val1 = $redis->hget('H_Rfid_Map', $tagid);
                        if ($val == 1 || $val1 !== null) {
                            $tagArray = Arr::add($tagArray, $tagid, $tagid);
                        } else {
                            $redis->sadd('S_Rfid_Tags_' . $fcode, $tagid);
                            $redis->hset('H_Rfid_Map', $tagid, $fcode);
                            $redis->sadd('S_Rfid_Org_' . $org . '_' . $fcode, $tagid);
                            $refDataArr = array(
                                'tagid' => $tagid,
                                'mobile' => $mobile,
                                'tagname' => $tagname,
                                'designation' => $designation,
                                'employeeId' => $employeeId,
                                'department' => $department,
                            );
                            $refDataJson = json_encode($refDataArr);
                            if (session()->get('cur') == 'dealer') {
                                $redis->hset('H_Rfid_Dealer_' . $org . '_' . $fcode, $tagid, $refDataJson);
                            } else if (session()->get('cur') == 'admin') {
                                $redis->hset('H_Rfid_Admin_' . $org . '_' . $fcode, $tagid, $refDataJson);
                            }
                            $updatedTags = Arr::add($updatedTags, $tagid, $tagid);
                        }
                    }
                }
            }
        }
        $error = '';
        if (!is_null($tagArray)) {
            $error = implode(", ", $tagArray);
            $error = 'Already exist tag id: ' . $error;
            if (!is_null($updatedTags)) {
                $updatedTags = implode(", ", $updatedTags);
                session()->flash('alert-class', "alert-success");
                session()->flash('message', 'Tags are created successfully ' . $updatedTags);
            }
        } elseif ($tagid != null) {
            $error = 'Tags are created successfully ' . $error;
            // return $this->showTag($org);
            return redirect()->route("showTag",["orgId"=>$org])->with([
                'alert-class'=>"alert-success",
                'message'=> $error
            ]);
        } else {
            $error = 'Enter the values in fields ' . $error;
        }
        return redirect()->back()->withInput()->withErrors($error);


        // return $this->store($numberoftags, $error);
        //return 'import uploads';
    }
}
