<?php

namespace App\Http\Controllers;

use Auth, Session, Log, Arr, View, Input, Redirect;
use Illuminate\Support\Facades\Redis;

class VdmSchoolController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
        $username = auth()->id();

        return view('vdm.schools.create');
    }

    public function store()
    {

        
        
        $username = auth()->id();
        $rules = array(
            'schoolId' => 'required',
            'routes' => 'required'
        );
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            // store

            $schoolId  = request()->get('schoolId');
            $routes = request()->get('routes');

            $redis = Redis::connection();
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $redis->sadd('S_Schools_' . $fcode, $schoolId);

            $routesArr = explode(",", $routes);

            foreach ($routesArr as $route) {
                if (empty($route)) {
                    continue;
                }
                $redis->sadd('S_School_Route_' . $schoolId . '_' . $fcode, $route);
            }

            // redirect
            session()->flash('message', 'Successfully created ' . $schoolId . '!');
            return redirect()->to('vdmSchools');
        }
    }

    public function index()
    {
        
        $username = auth()->id();

        $redis = Redis::connection();

        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $schoolListId = 'S_Schools_' . $fcode;

        $schoolList = $redis->smembers($schoolListId);



        foreach ($schoolList as $school) {
        }

        return view('vdm.schools.index', array(
            'schoolList' => $schoolList
        ));
    }
}
