<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redis;

class VdmBusRoutesController extends Controller
{



    public function index($id)
    {
        
        $username = auth()->id();
        $redis = Redis::connection();
        $org = $id;
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $routeList = $redis->smembers('S_Organisation_Route_' . $id . '_' . $fcode);
        return view('vdm.busRoutes.index', [
            'routeList' => $routeList
        ]);
    }


    /**   
     * 
     * This show is invoked from vdm org controller
     * 
     */
    public function show($id)
    {
        
        $username = auth()->id();
        $redis = Redis::connection();
        $orgId = $id;
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $routeList = $redis->smembers('S_Organisation_Route_' . $orgId . '_' . $fcode);
        return view('vdm.busRoutes.index', [
            'routeList' => $routeList,
            'orgId' => $orgId
        ]);
    }

    public function create()
    {
        
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $orgListId = 'S_Organisations_' . $fcode;

        if (session()->get('cur') == 'dealer') {
            $orgListId = 'S_Organisations_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $orgListId = 'S_Organisations_Admin_' . $fcode;
        }

        $orgArr = $redis->smembers($orgListId);

        $orgList = array();
        foreach ($orgArr as $key => $value) {
            $orgList = Arr::add($orgList, $value, $value);
        }

        return view('vdm.busRoutes.create', [
            'orgList' => $orgList
        ]);
    }

    public function store()
    {

        
        $username = auth()->id();
        $rules = array(
            'orgId' => 'required',
            'routeId' => 'required',
            'stops' => 'required'
        );
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            // store

            $orgId       = request()->get('orgId');
            $routeId      = request()->get('routeId');
            $routeId     =    str_replace(' ', '', $routeId);
            $stops      = request()->get('stops');
            $redis = Redis::connection();
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

            $allStopsArr = explode("\r\n", $stops);

            $stopsData = array();

            $morningSeqList = request()->get('morningSeq');
            $eveningSeqList = request()->get('eveningSeq');

            $redis->set('K_Morning_StopSeq_' . $orgId . '_' . $routeId . '_' . $fcode, $morningSeqList);
            $redis->set('K_Evening_StopSeq_' . $orgId . '_' . $routeId . '_' . $fcode, $eveningSeqList);
            $redis->hset('H_Stopseq_' . $orgId . '_' . $fcode, $routeId . ':morning', $morningSeqList);
            $redis->hset('H_Stopseq_' . $orgId . '_' . $fcode, $routeId . ':evening', $eveningSeqList);

            foreach ($allStopsArr as $stop) {
                $stopsDetailsArr = explode(':', $stop);
                $key = $routeId  . ':' . $stopsDetailsArr[0];
                $redis->hdel('H_Bus_Stops_' . $orgId . '_' . $fcode, $key);
            }
            $redis->del('L_Suggest_' . $routeId . '_' . $orgId . '_' . $fcode);
            //stop:geo:mobile:stopname
            $i = 0;
            $temp = null;
            foreach ($allStopsArr as $stop) {
                $i++;
                $stopsDetailsArr = explode(':', $stop);
                $key = $routeId  . ':' . $stopsDetailsArr[0];
                $origMobileNos = '';
                $stopDetails = $redis->hget('H_Bus_Stops_' . $orgId . '_' . $fcode, $key);


                if (isset($stopDetails)) {
                    $stopDetailsNewArr = json_decode($stopDetails, true);

                    $origMobileNos = $stopDetailsNewArr['mobileNo'];
                }

                $stopsData = array();
                if (isset($stopsDetailsArr[1])) { //geoLocation
                    $stopsData = Arr::add($stopsData, 'geoLocation', trim($stopsDetailsArr[1]));
                } else {
                    continue;
                }
                if (isset($stopsDetailsArr[2])) { //mobile
                    $cumMobileNos = $stopsDetailsArr[2] . ',' . $origMobileNos;
                    $stopsData = Arr::add($stopsData, 'mobileNo', $cumMobileNos);
                }
                if (isset($stopsDetailsArr[3])) {
                    $stopsData = Arr::add($stopsData, 'stopName', $stopsDetailsArr[3]);
                }
                $stopsDataJson = json_encode($stopsData);

                $redis->sadd('S_Organisation_Route_' . $orgId . '_' . $fcode, $routeId);

                try {

                    $stopsDetails = explode(',', $stopsDetailsArr[1]);
                    $refDataArr = array(
                        'rowId' => 0,
                        'geoAddress' => $stopsDetailsArr[3],
                        'latitude' => $stopsDetails[0],
                        'longitude' => $stopsDetails[1],

                    );
                    if ($temp != $stopsDetailsArr[3]) {
                        $redis->rpush('L_Suggest_' . $routeId . '_' . $orgId . '_' . $fcode, json_encode($refDataArr));
                    }
                } catch (\Exception $e) {
                }
                $temp = $stopsDetailsArr[3];

                $redis->hset('H_Bus_Stops_' . $orgId . '_' . $fcode, $key, $stopsDataJson);
            }

            session()->flash('message', 'Successfully created route with stops' . $routeId . '!');
            return redirect()->to('vdmBusRoutes/' . $orgId);
        }
    }

    /*
    For set the road limit 
*/

    public function _roadSpeed()
    {
        $username           = auth()->id();
        $redis              = Redis::connection();
        $fcode              = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $_uIbinding         = array();
        $getValue           = $redis->hgetall('H_SpeedRange_' . $fcode);

        $roadSpeedValue     = $getValue;

        if (count($roadSpeedValue) > 0) {
            foreach ($roadSpeedValue as $key => $value) {
                $splitValue = json_decode($value, true);
                $_uIbinding = Arr::add($_uIbinding, $key, $splitValue);
            }
            return view('vdm.busRoutes.roadSpeedLimit')->with('roadSpeedValue', $_uIbinding);
        } else
            return view('vdm.busRoutes.roadSpeedLimit');
    }

    /*
    road speed range insert 
*/


    public function _speedRange()
    {
        $_comma             = ':';
        $username           = auth()->id();
        $redis              = Redis::connection();
        $fcode              = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $_speedRangeValue   = request()->all();

        $rules  = array(
            'roadName'  => 'required',
            'speed'     => 'required|numeric',
            'fromlatlng' => 'required',
            'tolatlng'  => 'required',
        );

        $validator          = validator()->make($_speedRangeValue, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {

            $rangeValues    = array();
            $keyValue       = $redis->hget('H_SpeedRange_' . $fcode, $_speedRangeValue['roadName']);

            if ($keyValue) {
                foreach (json_decode($keyValue) as $key => $value) {
                    array_push($rangeValues, $value);
                }
            }

            array_push($rangeValues, $_speedRangeValue['fromlatlng'] . $_comma . $_speedRangeValue['tolatlng'] . $_comma . $_speedRangeValue['speed']);

            $redis->hset('H_SpeedRange_' . $fcode, $_speedRangeValue['roadName'], json_encode($rangeValues));
        }
        return redirect()->to('vdmBusRoutes/roadSpeed');
    }

    /*
        delete function
    */
    public function _deleteRoad($id)
    {
        $username           = auth()->id();
        $redis              = Redis::connection();
        $fcode              = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        if (isset($id)) {

            $_checkvalue    = explode(':', $id);
            $_getValue      = $redis->hget('H_SpeedRange_' . $fcode, $_checkvalue[0]);

            if (count($_checkvalue) == 4) {
                $rangeValues = array();
                foreach (json_decode($_getValue) as $key => $value) {

                    if ($value == $_checkvalue[1] . ':' . $_checkvalue[2] . ':' . $_checkvalue[3]) {
                    } else {
                        array_push($rangeValues, $value);
                    }
                }
                $redis->hset('H_SpeedRange_' . $fcode, $_checkvalue[0], json_encode($rangeValues));
            } else {
                $redis->hdel('H_SpeedRange_' . $fcode, $_checkvalue[0]);
            }
        }
        return redirect()->to('vdmBusRoutes/roadSpeed');
    }




    public function _editSpeed()
    {

        $sessionValue       = session()->get('roodSpeed');
        $splitValue         = explode(':', $sessionValue);
        session()->forget('roodSpeed');
        $username           = auth()->id();
        $redis              = Redis::connection();
        $fcode              = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $_speedRangeValue   = request()->all();

        $rules  = array(
            'roadName'  => 'required',
            'speed'     => 'required|numeric',
            'fromlatlng' => 'required',
            'tolatlng'  => 'required',
        );

        $validator          = validator()->make($_speedRangeValue, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $_getValue      = $redis->hget('H_SpeedRange_' . $fcode, $splitValue[0]);

            $rangeValues = array();
            $storedValue = $_speedRangeValue['fromlatlng'] . ':' . $_speedRangeValue['tolatlng'] . ':' . $_speedRangeValue['speed'];

            foreach (json_decode($_getValue) as $key => $value) {
                $checking = $splitValue[1] . ':' . $splitValue[2] . ':' . $splitValue[3];
                $value == $checking ? array_push($rangeValues, $storedValue) : array_push($rangeValues, $value);
            }
            $redis->hdel('H_SpeedRange_' . $fcode, $splitValue[0]);
            $redis->hset('H_SpeedRange_' . $fcode, $_speedRangeValue['roadName'], json_encode($rangeValues));
        }
        return redirect()->to('vdmBusRoutes/roadSpeed');
    }

    public function _updateRoad($value)
    {

        session()->put('roodSpeed', $value);
        $splitValue        = explode(':', $value);
        return view('vdm.busRoutes.editSpeedLimit')->with('roadName', $splitValue[0])->with('speed', $splitValue[3])->with('fromlatlng', $splitValue[1])->with('tolatlng', $splitValue[2]);
    }
}
