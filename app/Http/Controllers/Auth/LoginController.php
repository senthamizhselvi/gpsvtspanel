<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FirebaseController;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Auth\CreateSessionCookie\FailedToCreateSessionCookie;
use Illuminate\Validation\ValidationException;

use App\Models\User;
use Firebase\Auth\Token\Exception\InvalidToken;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $auth;
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function login(Request $request)
    {
        try {
            $username = $request['username'] . '@vamosys.com';
            $password = $request['password'];
            $remember_me  = (!empty($request['remember_me'])) ? TRUE : FALSE;
            session()->put('phplang', request()->get('lang'));
            $signInResult = FirebaseController::signInWithEmailAndPassword($username, $password)->data();
            $user = new User($signInResult);
            Auth::login($user, $remember_me);
            return redirect('Business');
        } catch (FirebaseException $e) {
            throw ValidationException::withMessages([$this->username() => [trans('auth.failed')],]);
        }
    }

    public function username()
    {
        return 'username';
    }

    public function googleLogin(Request $request)
    {
        $auth = app('firebase.auth');
        $socialTokenId = $request->input('token', '');
        try {
            $verifiedIdToken = $auth->verifyIdToken($socialTokenId);
            $user = new User();
            $user->displayName = $verifiedIdToken->claims()->get('name');
            $user->email = $verifiedIdToken->claims()->get('email');
            $user->localId = $verifiedIdToken->claims()->get('user_id');

            Auth::login($user);
            return redirect('Business');
        } catch (\InvalidArgumentException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        } catch (InvalidToken $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
