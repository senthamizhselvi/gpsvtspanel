<?php

namespace App\Http\Controllers;

// use DB, Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use App\Exports\CalibrationExport;
use App\Exports\vehicleStatusExport;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class VdmVehicleViewController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$user = new VdmVehicleController;
		$page = $user->index();
		session()->put('vCol', 2);
		return $page;
	}

	public function move_vehicle($id)
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$vehicleId = $id;

		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
		$deviceId = $redis->hget($vehicleDeviceMapId, $vehicleId);
		$dealerId = $redis->smembers('S_Dealers_' . $fcode);
		$orgArr = array();
		foreach ($dealerId as $org) {
			$orgArr = Arr::add($orgArr, $org, $org);
		}
		$dealerId = $orgArr;
		$ownset[] = 'OWN';
		$ownarray = array();
		foreach ($ownset as $owq) {
			$ownarray = Arr::add($ownarray, $owq, $owq);
		}
		$ownset = $ownarray;

		$details = $redis->hget('H_RefData_' . $fcode, $vehicleId);
		$refData = json_decode($details, true);
		$OWN = isset($refData['OWN']) ? $refData['OWN'] : '';
		if ($OWN != 'OWN') {
			$dealerId1 = $ownset;
		} else {
			$dealerId1 = $dealerId;
		}
		$expiredPeriod = isset($refDataFromDB['expiredPeriod']) ? $refDataFromDB['expiredPeriod'] : 'NotAvailabe';
		$expiredPeriod = str_replace(' ', '', $expiredPeriod);
		$move_vehicleData = [
			'vehicleId' => $vehicleId,
			'deviceId' => $deviceId,
			'expiredPeriod' => $expiredPeriod,
			'dealerId' => $dealerId,
			'OWN' => $OWN
		];
		return view('vdm.vehicles.move_vehicle', $move_vehicleData);
	}
	public function moveVehicleUpdate()
	{


		$vehicleId = request()->get('vehicleId');
		$deviceId = request()->get('deviceId');
		$vehicleId = preg_replace('/\s+/', '', $vehicleId);
		$deviceId = preg_replace('/\s+/', '', $deviceId);
		$dealerId = request()->get('dealerId');
		$vehicleIdOld = request()->get('vehicleIdOld');
		$dealerIdOld = request()->get('dealerIdOld');
		$deviceIdOld = request()->get('deviceIdOld');
		$expiredPeriodOld = request()->get('expiredPeriodOld');
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		///get orgId
		$detailsR = $redis->hget('H_RefData_' . $fcode, $vehicleId);
		$refDataFromDBR = json_decode($detailsR, true);
		$orgId = isset($refDataFromDBR['orgId']) ? $refDataFromDBR['orgId'] : 'Default';
		$OWN = isset($refDataFromDBR['OWN']) ? $refDataFromDBR['OWN'] : '';

		$detailsR1 = $redis->hget('H_RefData_' . $fcode, $vehicleIdOld);
		$refDataFromDBR1 = json_decode($detailsR1, true);
		$OWN1 = isset($refDataFromDBR1['OWN']) ? $refDataFromDBR1['OWN'] : 'OWN';
		if ($OWN1 == 'OWN') {
			$emptyG = 'S_Groups_Admin_' . $fcode;
		} else {
			$emptyG = 'S_Groups_Dealer_' . $OWN1 . '_' . $fcode;
		}
		//$redis->srem ( 'S_Vehicles_' . $orgId.'_'.$fcode, $vehicleIdOld);
		// $redis->sadd ( 'S_Vehicles_' . $orgId.'_'.$fcode, $vehicleId);

		$groupList = $redis->smembers('S_Groups_' . $fcode);
		foreach ($groupList as $group) {
			if ($redis->sismember($group, $vehicleIdOld) == 1) {
				$result = $redis->srem($group, $vehicleIdOld);
				//$redis->sadd($group,$vehicleId);
				$groupval = $redis->smembers($group);
				if (count($groupval) == 0) {
					$redis->srem($emptyG, $group);
					$redis->srem('S_Groups_' . $fcode, $group);

					$userList = $redis->smembers('S_' . $group);
					foreach ($userList as $username) {
						$redis->srem($username, $group);
					}
					$redis->del('S_' . $group);
				}
			}
		}
		$deviceId = isset($refDataFromDBR['deviceId']) ? $refDataFromDBR['deviceId'] : '';
		$orgId1 = strtoupper($orgId);
		$expiredPeriod = $redis->hget('H_Expire_' . $fcode, $vehicleIdOld);
		if (!$expiredPeriod == null) {
			$expiredPeriod = str_replace($vehicleIdOld, $vehicleIdOld, $expiredPeriod);
			$redis->hset('H_Expire_' . $fcode, $expiredPeriodOld, $expiredPeriod);
		}
		$refDataJson1 = $redis->hget('H_RefData_' . $fcode, $vehicleIdOld);
		$refDataJson1 = json_decode($refDataJson1, true);
		$dealerIdOld = $refDataJson1['OWN'];
		$shortName1 = isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : '';
		$shortName = strtoupper($shortName1);
		$gpsSimNo = isset($refDataJson1['gpsSimNo']) ? $refDataJson1['gpsSimNo'] : '';
		$orgId3 = isset($refDataJson1['orgId']) ? $refDataJson1['orgId'] : 'DEFAULT';
		$orgId2 = 'DEFAULT';
		$current = Carbon::now();
		$licenceissue = $current->format('d-m-Y');
		$refDataArr = array(
			'deviceId' => isset($refDataJson1['deviceId']) ? $refDataJson1['deviceId'] : '',
			'shortName' => isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : 'nill',
			'deviceModel' => isset($refDataJson1['deviceModel']) ? $refDataJson1['deviceModel'] : 'GT06N',
			'regNo' => isset($refDataJson1['regNo']) ? $refDataJson1['regNo'] : 'XXXXX',
			'vehicleMake' => isset($refDataJson1['vehicleMake']) ? $refDataJson1['vehicleMake'] : ' ',
			'vehicleType' =>  isset($refDataJson1['vehicleType']) ? $refDataJson1['vehicleType'] : 'Bus',
			'oprName' => isset($refDataJson1['oprName']) ? $refDataJson1['oprName'] : 'airtel',
			'mobileNo' => isset($refDataJson1['mobileNo']) ? $refDataJson1['mobileNo'] : '0123456789',
			//'vehicleExpiry' =>isset($refDataJson1['vehicleExpiry'])?$refDataJson1['vehicleExpiry']:'null',
			'onboardDate' => '',
			'overSpeedLimit' => isset($refDataJson1['overSpeedLimit']) ? $refDataJson1['overSpeedLimit'] : '60',
			'odoDistance' => isset($refDataJson1['odoDistance']) ? $refDataJson1['odoDistance'] : '0',
			'driverName' => isset($refDataJson1['driverName']) ? $refDataJson1['driverName'] : 'XXX',
			'gpsSimNo' => isset($refDataJson1['gpsSimNo']) ? $refDataJson1['gpsSimNo'] : '0123456789',
			'email' => isset($refDataJson1['email']) ? $refDataJson1['email'] : ' ',
			'orgId' => 'DEFAULT',
			'sendGeoFenceSMS' => isset($refDataJson1['sendGeoFenceSMS']) ? $refDataJson1['sendGeoFenceSMS'] : 'no',
			'morningTripStartTime' => isset($refDataJson1['morningTripStartTime']) ? $refDataJson1['morningTripStartTime'] : ' ',
			'eveningTripStartTime' => 'TIMEZONE',
			// 'eveningTripStartTime' => isset($refDataJson1['eveningTripStartTime'])?$refDataJson1['eveningTripStartTime']:' ',
			'parkingAlert' => isset($refDataJson1['parkingAlert']) ? $refDataJson1['parkingAlert'] : 'no',
			'altShortName' => isset($refDataJson1['altShortName']) ? $refDataJson1['altShortName'] : 'nill',
			'paymentType' => isset($refDataJson1['paymentType']) ? $refDataJson1['paymentType'] : ' ',
			//'expiredPeriod'=>isset($refDataJson1['expiredPeriod'])?$refDataJson1['expiredPeriod']:' ',
			'fuel' => isset($refDataJson1['fuel']) ? $refDataJson1['fuel'] : 'no',
			'fuelType' => isset($refDataJson1['fuelType']) ? $refDataJson1['fuelType'] : ' ',
			'isRfid' => isset($refDataJson1['isRfid']) ? $refDataJson1['isRfid'] : 'no',
			'rfidType' => isset($refDataJson1['rfidType']) ? $refDataJson1['rfidType'] : 'no',
			'OWN' => $dealerId,
			'Licence' => isset($refDataJson1['Licence']) ? $refDataJson1['Licence'] : 'Advance',
			'Payment_Mode' => isset($refDataJson1['Payment_Mode']) ? $refDataJson1['Payment_Mode'] : 'Monthly',
			'descriptionStatus' => isset($refDataJson1['descriptionStatus']) ? $refDataJson1['descriptionStatus'] : '',
			'mintemp' => isset($refDataJson1['mintemp']) ? $refDataJson1['mintemp'] : '',
			'maxtemp' => isset($refDataJson1['maxtemp']) ? $refDataJson1['maxtemp'] : '',
			'safetyParking' => isset($refDataJson1['safetyParking']) ? $refDataJson1['safetyParking'] : 'no',
			'licenceissuedDate' => $licenceissue,
			'VolIg' => isset($refDataJson1['VolIg']) ? $refDataJson1['VolIg'] : '',
			'batteryvolt' => isset($refDataJson1['batteryvolt']) ? $refDataJson1['batteryvolt'] : '',
			'ac' => isset($refDataJson1['ac']) ? $refDataJson1['ac'] : '',
			'acVolt' => isset($refDataJson1['acVolt']) ? $refDataJson1['acVolt'] : '',
			'communicatingPortNo' => isset($refDataJson1['communicatingPortNo']) ? $refDataJson1['communicatingPortNo'] : '',
		);

		$refDataJson = json_encode($refDataArr);
		$refDataArr2 = array(
			'deviceId' => isset($refDataJson1['deviceId']) ? $refDataJson1['deviceId'] : ' ',
			'shortName' => isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : 'nill',
			'deviceModel' => isset($refDataJson1['deviceModel']) ? $refDataJson1['deviceModel'] : 'GT06N',
			'regNo' => isset($refDataJson1['regNo']) ? $refDataJson1['regNo'] : 'XXXXX',
			'vehicleMake' => isset($refDataJson1['vehicleMake']) ? $refDataJson1['vehicleMake'] : ' ',
			'vehicleType' =>  isset($refDataJson1['vehicleType']) ? $refDataJson1['vehicleType'] : 'Bus',
			'oprName' => isset($refDataJson1['oprName']) ? $refDataJson1['oprName'] : 'airtel',
			'mobileNo' => isset($refDataJson1['mobileNo']) ? $refDataJson1['mobileNo'] : '0123456789',
			// 'vehicleExpiry' =>isset($refDataJson1['vehicleExpiry'])?$refDataJson1['vehicleExpiry']:'null',
			'onboardDate' => '',
			'overSpeedLimit' => isset($refDataJson1['overSpeedLimit']) ? $refDataJson1['overSpeedLimit'] : '60',
			'odoDistance' => isset($refDataJson1['odoDistance']) ? $refDataJson1['odoDistance'] : '0',
			'driverName' => isset($refDataJson1['driverName']) ? $refDataJson1['driverName'] : 'XXX',
			'gpsSimNo' => isset($refDataJson1['gpsSimNo']) ? $refDataJson1['gpsSimNo'] : '0123456789',
			'email' => isset($refDataJson1['email']) ? $refDataJson1['email'] : ' ',
			'orgId' => 'DEFAULT',
			'sendGeoFenceSMS' => isset($refDataJson1['sendGeoFenceSMS']) ? $refDataJson1['sendGeoFenceSMS'] : 'no',
			'morningTripStartTime' => isset($refDataJson1['morningTripStartTime']) ? $refDataJson1['morningTripStartTime'] : ' ',
			'eveningTripStartTime' => 'TIMEZONE',
			// 'eveningTripStartTime' => isset($refDataJson1['eveningTripStartTime'])?$refDataJson1['eveningTripStartTime']:' ',
			'parkingAlert' => isset($refDataJson1['parkingAlert']) ? $refDataJson1['parkingAlert'] : 'no',
			'altShortName' => isset($refDataJson1['altShortName']) ? $refDataJson1['altShortName'] : 'nill',
			'paymentType' => isset($refDataJson1['paymentType']) ? $refDataJson1['paymentType'] : ' ',
			// 'expiredPeriod'=>isset($refDataJson1['expiredPeriod'])?$refDataJson1['expiredPeriod']:' ',
			'fuel' => isset($refDataJson1['fuel']) ? $refDataJson1['fuel'] : 'no',
			'fuelType' => isset($refDataJson1['fuelType']) ? $refDataJson1['fuelType'] : ' ',
			'isRfid' => isset($refDataJson1['isRfid']) ? $refDataJson1['isRfid'] : 'no',
			'rfidType' => isset($refDataJson1['rfidType']) ? $refDataJson1['rfidType'] : 'no',
			'OWN' => 'OWN',
			'Licence' => isset($refDataJson1['Licence']) ? $refDataJson1['Licence'] : 'Advance',
			'Payment_Mode' => isset($refDataJson1['Payment_Mode']) ? $refDataJson1['Payment_Mode'] : 'Monthly',
			'descriptionStatus' => isset($refDataJson1['descriptionStatus']) ? $refDataJson1['descriptionStatus'] : '',
			'mintemp' => isset($refDataJson1['mintemp']) ? $refDataJson1['mintemp'] : '',
			'maxtemp' => isset($refDataJson1['maxtemp']) ? $refDataJson1['maxtemp'] : '',
			'safetyParking' => isset($refDataJson1['safetyParking']) ? $refDataJson1['safetyParking'] : 'no',
			'licenceissuedDate' => '',
			'VolIg' => isset($refDataJson1['VolIg']) ? $refDataJson1['VolIg'] : '',
			'batteryvolt' => isset($refDataJson1['batteryvolt']) ? $refDataJson1['batteryvolt'] : '',
			'ac' => isset($refDataJson1['ac']) ? $refDataJson1['ac'] : '',
			'acVolt' => isset($refDataJson1['acVolt']) ? $refDataJson1['acVolt'] : '',
			'communicatingPortNo' => isset($refDataJson1['communicatingPortNo']) ? $refDataJson1['communicatingPortNo'] : '',
		);

		$refDataJson2 = json_encode($refDataArr2);


		// if($dealerIdOld!='OWN')
		// {
		///ram noti
		$vehiDel = $redis->del('S_' . $vehicleIdOld . '_' . $fcode);
		///
		$qq = $redis->sismember('S_Vehicles_Admin_' . $fcode, $vehicleIdOld);
		$oo = $redis->sismember('S_Vehicles_Dealer_' . $dealerId . '_' . $fcode, $vehicleIdOld);
		if ($qq == '1') {
			$redis->hdel('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $vehicleIdOld . ':' . $deviceIdOld . ':' . $shortName . ':' . $orgId2 . ':' . $gpsSimNo . ':OWN', $vehicleIdOld);
			$redis->hset('H_VehicleName_Mobile_Dealer_' . $dealerId . '_Org_' . $fcode, $vehicleIdOld . ':' . $deviceIdOld . ':' . $shortName . ':' . $orgId2 . ':' . $gpsSimNo, $vehicleIdOld);
			$redis->srem('S_Vehicles_Admin_' . $fcode, $vehicleIdOld);
			$redis->srem('S_Vehicles_Dealer_' . $dealerIdOld . '_' . $fcode, $vehicleIdOld);
			$redis->sadd('S_Vehicles_Dealer_' . $dealerId . '_' . $fcode, $vehicleIdOld);
			$redis->hdel('H_RefData_' . $fcode, $vehicleIdOld);
			$redis->hset('H_RefData_' . $fcode, $vehicleIdOld, $refDataJson);
			$pub = $redis->PUBLISH('sms:topicNetty', $deviceIdOld);
			DB::table('Vehicle_details')
				->where('vehicle_id', $vehicleIdOld)
				->where('fcode', $fcode)
				->update([
					'belongs_to' => $dealerId,
					'sold_date' => Carbon::now()
				]);
		} elseif ($qq == '0' || $oo == '1') {
			$redis->hdel('H_VehicleName_Mobile_Dealer_' . $dealerIdOld . '_Org_' . $fcode, $vehicleIdOld . ':' . $deviceIdOld . ':' . $shortName . ':' . $orgId3 . ':' . $gpsSimNo, $vehicleIdOld);
			$redis->hset('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $vehicleIdOld . ':' . $deviceIdOld . ':' . $shortName . ':' . $orgId2 . ':' . $gpsSimNo . ':OWN', $vehicleIdOld);
			$redis->srem('S_Vehicles_Dealer_' . $dealerId . '_' . $fcode, $vehicleIdOld);
			$redis->srem('S_Vehicles_Dealer_' . $dealerIdOld . '_' . $fcode, $vehicleIdOld);
			$redis->sadd('S_Vehicles_Admin_' . $fcode, $vehicleIdOld);
			$redis->hdel('H_RefData_' . $fcode, $vehicleIdOld);
			$redis->hset('H_RefData_' . $fcode, $vehicleIdOld, $refDataJson2);
			$pub = $redis->PUBLISH('sms:topicNetty', $deviceIdOld);
			DB::table('Vehicle_details')
				->where('vehicle_id', $vehicleIdOld)
				->where('fcode', $fcode)
				->update([
					'belongs_to' => 'OWN',
					'sold_date' => ''
				]);
		}
		// }
		if ($dealerIdOld != 'OWN') {
			$redis->hdel('H_Pre_Onboard_Dealer_' . $dealerIdOld . '_' . $fcode, $deviceIdOld);
			$redis->srem('S_Pre_Onboard_Dealer_' . $dealerId . '_' . $fcode, $deviceIdOld);
		} else {
			$deviceDataArr = array(
				'deviceid' => $deviceIdOld,
				'deviceidtype' => isset($refDataJson1['deviceModel']) ? $refDataJson1['deviceModel'] : 'GT06N',
			);
			$deviceDataJson = json_encode($deviceDataArr);
			$redis->hset('H_Pre_Onboard_Dealer_' . $dealerId . '_' . $fcode, $deviceIdOld, $deviceDataJson);
			$redis->sadd('S_Pre_Onboard_Dealer_' . $dealerId . '_' . $fcode, $deviceIdOld);
		}

		if ($dealerIdOld != 'OWN') {
			$error = 'Vehicle moved successfully ';
		} else {
			$error = ' Vehicle movement is not allowed';
		}
		$emailFcode = $redis->hget('H_Franchise', $fcode);
		$emailFile = json_decode($emailFcode, true);
		$email1 = $emailFile['email2'];
		$email2 = $emailFile['email1'];

		session()->flash('message', $deviceIdOld . ' is updated successfully. ');
		session()->flash('alert-class', 'alert-success');
		return redirect()->to('deviceMove' . $deviceIdOld);
	}

	public function vehicleStatusV2()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		if (session()->get('cur') == 'dealer') {
			$vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$vehicleListId = 'S_Vehicles_Admin_' . $fcode;
		} else {
			$vehicleListId = 'S_Vehicles_' . $fcode;
		}
		$deviceList = null;
		$shortNameList = null;
		$orgIdList = null;
		$regNoList = null;
		$statusList = null;
		$commList = null;
		$locList = null;
		$emptyRef = null;
		$latlonList = null;
		$speedList = null;
		$addressList = null;
		$ignList = null;
		$groupList = null;

		$vehicleList = $redis->smembers($vehicleListId);
		foreach ($vehicleList as $key => $vehicle) {
			$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);
			if (isset($vehicleRefData)) {
				$emptyRef = Arr::add($emptyRef, $vehicle, $vehicle);
			} else {
				continue;
			}
			$vehicleRefData = json_decode($vehicleRefData, true);
			$deviceId   =  $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicle);
			if ((session()->get('cur') == 'dealer' &&  $redis->sismember('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId) == 0) || session()->get('cur') == 'admin') {
				$deviceList = Arr::add($deviceList, $vehicle, $deviceId);
				$shortName = isset($vehicleRefData['shortName']) ? $vehicleRefData['shortName'] : 'nill';
				$shortNameList = Arr::add($shortNameList, $vehicle, $shortName);
				$orgId = isset($vehicleRefData['orgId']) ? $vehicleRefData['orgId'] : 'Default';
				$orgIdList = Arr::add($orgIdList, $vehicle, $orgId);
				$regNo = isset($vehicleRefData['regNo']) ? $vehicleRefData['regNo'] : '-';
				if ($regNo == 'xxxx') {
					$regNo = '-';
				}
				$regNoList = Arr::add($regNoList, $vehicle, $regNo);
				$statusVehicle = $redis->hget('H_ProData_' . $fcode, $vehicle);
				$statusSeperate = explode(',', $statusVehicle);
				$statusSeperate1 = isset($statusSeperate[7]) ? $statusSeperate[7] : 'N';
				$statusList = Arr::add($statusList, $vehicle, $statusSeperate1);
				$lastComm = isset($statusSeperate[36]) ? $statusSeperate[36] : '';
				$lastLoc = isset($statusSeperate[4]) ? $statusSeperate[4] : '';
				if ($lastComm == '' || $lastComm == null) {
					$setted = 'Not Yet Communicated';
				} else {
					$sec = $lastComm / 1000;
					$datt = date("Y-m-d", $sec);
					$time1 = date("H:i:s", $sec);
					$setted = $datt . ' ' . $time1;
				}
				if ($lastLoc == '' || $lastLoc == null || $statusSeperate1 == 'N') {
					$setlastLoc = 'Not Yet Located';
				} else {
					$sec1 = $lastLoc / 1000;
					$datt1 = date("Y-m-d", $sec1);
					$time11 = date("H:i:s", $sec1);
					$setlastLoc = $datt1 . ' ' . $time11;
				}
				$commList = Arr::add($commList, $vehicle, $setted);
				$locList = Arr::add($locList, $vehicle, $setlastLoc);
				$lat = isset($statusSeperate[0]) ? $statusSeperate[0] : '0';
				$lon = isset($statusSeperate[1]) ? $statusSeperate[1] : '0';
				$latlonList = Arr::add($latlonList, $vehicle, $lat . ',' . $lon);
				$speed = isset($statusSeperate[2]) ? $statusSeperate[2] : '-';
				$speedList = Arr::add($speedList, $vehicle, $speed);
				$ign = isset($statusSeperate[16]) ? $statusSeperate[16] : '-';
				$ignList = Arr::add($ignList, $vehicle, $ign);
				$url = 'http://188.166.244.126:9000/getVehicleAddress?lat=' . $lat . '&lon=' . $lon;
				//http://209.97.163.4:9000/getVehicleAddress?lat=16.83735&lon=81.39871
				$url = str_replace(" ", '%20', $url);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_URL, $url);
				$address = curl_exec($ch);
				curl_close($ch);
				$addressList = Arr::add($addressList, $vehicle, $address);
				$groups = $redis->smembers('S_' . $vehicle . '_' . $fcode);
				$groupname = "";
				$groups = implode('<br/>', str_replace('S_', '', $groups));
				$groupList = Arr::add($groupList, $vehicle, $groups);
			}
		}
		$vehicleStatusData = [
			'vehicleList' => $vehicleList,
			'deviceList' => $deviceList,
			'shortNameList' => $shortNameList,
			'commList' => $commList,
			'orgIdList' => $orgIdList,
			'statusList' => $statusList,
			'regNoList' => $regNoList,
			'locList' => $locList,
			'latlonList' => $latlonList,
			'speedList' => $speedList,
			'addressList' => $addressList,
			'ignList' => $ignList,
			'groupList' => $groupList
		];
		return view('vdm.vehicles.vehicleStatus', $vehicleStatusData);
	}

	public function vehicleStatus()
	{
		return view('vdm.vehicles.vehicleStatus', []);
	}

	public function vehicleStatusXls()
	{

		return Excel::download(new vehicleStatusExport(), 'vehicle_Status.xlsx');
		// $username = auth()->id();
		// $redis = Redis::connection();
		// $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		// if (session()->get('cur') == 'admin') {
		// 	$emailFcode = $redis->hget('H_Franchise', $fcode);
		// 	$emailFile = json_decode($emailFcode, true);
		// 	$email1 = $emailFile['email2'];
		// } else if (session()->get('cur') == 'dealer') {
		// 	$emailFcode = $redis->hget('H_DealerDetails_' . $fcode, $username);
		// 	$emailFile = json_decode($emailFcode, true);
		// 	$email1 = $emailFile['email'];
		// }
		// $data[] = 'Vehicles Status';
		// session()->flash('message', 'Vehicles Status Sent to ' . $email1 . ' Id Successfully');
		// return redirect()->back();
	}

	public function vehicleStatusAjax()
	{
		$data = request()->all();
		$search = $data['search']['value'];
		$text_trim = str_replace(' ', '', $search);
		$search = strtoupper($text_trim);
		$draw = $data['draw'];
		$row = $data['start'];
		$rowperpage = $data['length']; // Rows display per page
		$columnSortOrder = $data['order'][0]['dir']; // asc or desc
		$start = $data['start'];

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		if (session()->get('cur') == 'dealer') {
			$url = 'http://209.97.163.4:9000/getVehicleStatus?userId=' . $username . '&fcode=' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$url = 'http://209.97.163.4:9000/getVehicleStatus?userId=ADMIN&fcode=' . $fcode;
		}

		$url = str_replace(" ", '%20', $url);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if (session()->has('vehicleStatus')) {
			$results = session()->get('vehicleStatus');
		} else {
			curl_setopt($ch, CURLOPT_URL, $url);
			$response = curl_exec($ch);
			curl_close($ch);
			$results = json_decode($response, true);
			$expiresAt = Carbon::now()->addMinutes(2);
			session(['vehicleStatus' => $results]);
		}
		// return $results;
		if (is_null($results)) $results = [];
		$resultsCount = count($results);
		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		// return $result;
		$data = array();

		if ($search == '') {
			$orgL = array_slice($results, $row, $rowperpage + 1);
			$orgCount = count($orgL);
		}

		if ($search !== '') {
			$temp = array();
			foreach ($results as $i => $result) {
				if (strpos($result['vehicleId'], $search) !== false) {
					$temp[] = $result;
				}
			}
			$orgL = array_slice($temp, $row, $rowperpage + 1);
			$orgCount = count($temp);

			$statusList = ["P", "M", "S", "U", "N"];
			$statusSearch = ["PARKING", "MOVING", "IDLE", "NODATA", "NEWDEVICE"];
			$statusIndex = array_search($search, $statusSearch);
			$statusCode = $statusList[$statusIndex];
			if ($statusIndex && in_array($statusCode, $statusList, true)) {
				$tmp = array();
				foreach ($results as $key => $result) {
					if (count($tmp) >= (int)$rowperpage) break;
					if (strpos($result['position'], $statusCode) !== false) {
						$tmp[] = $result;
					}
				}
				$orgListCount = count($results);
				$slice = $orgListCount - $start;
				$orgL = $slice <= 10 ? array_slice($tmp, 0, $slice) : $tmp;
				$orgCount = count($results);
			}
		}


		if ($columnSortOrder == 'asc') {
			usort($results, function ($a, $b) {
				return strcmp($a['vehicleId'], $b['vehicleId']);
			});
		} else {
			usort($results, function ($a, $b) {
				return strcmp($a['vehicleId'], $b['vehicleId']);
			});
		}
		foreach ($orgL as $i => $result) {

			$vehicle = $result['vehicleId'];
			$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);
			$vehicleRefData = json_decode($vehicleRefData, true);
			$deviceId = $result['deviceId'];
			$shortName = $result['vehicleName'];
			$orgId = $result['orgId'];
			$regNo = $result['regNo'];
			$countryTimezone = isset($vehicleRefData['country']) ? $vehicleRefData['country'] : '';
			if ($countryTimezone == '' || $countryTimezone == 'no' || $countryTimezone == "null") {
				$countryTimezone = isset($franchiseDetails['timeZone']) ? $franchiseDetails['timeZone'] : 'Asia/Kolkata';
			}

			$statusSearch = ["P", "M", "S", "U", "N"];
			$statusList = ["Parking", "Moving", "Idle", "No Data", "New Device"];
			$statusColor = ["#8e8e7b", "#00b374", "#ff6500", "#ff0000", "#0a85ff"];
			$statusIndex = array_search($result['position'], $statusSearch, true);
			$vehStatus = "<span style='color:" . $statusColor[$statusIndex] . "'>" . $statusList[$statusIndex] . "</span>";

			$lastComm = $result['lastComm'];
			$lastLoc = $result['lastLoc'];
			try {
				date_default_timezone_set($countryTimezone);
			} catch (\Exception $e) {
				date_default_timezone_set('Asia/Kolkata');
			}
			if ($lastComm == '' || $lastComm == null || $lastComm == 0) {
				$setted = 'Not Yet Communicated';
			} else {
				$sec = $lastComm / 1000;
				$datt = date("Y-m-d", $sec);
				$time1 = date("H:i:s", $sec);
				$setted = $datt . ' ' . $time1;
			}
			if ($lastLoc == '' || $lastLoc == null || $result['position'] == 'N') {
				$setlastLoc = 'Not Yet Located';
			} else {
				$sec1 = $lastLoc / 1000;
				$datt1 = date("Y-m-d", $sec1);
				$time11 = date("H:i:s", $sec1);
				$setlastLoc = $datt1 . ' ' . $time11;
			}
			date_default_timezone_set('Asia/Kolkata');
			$comm =  $setted;
			$loc = $setlastLoc;
			$latlon =  $result['lat'] . ',' . $result['lon'];
			$speed = $result['speed'];
			$ign = $result['ignition'];
			$address = $result['address'];
			$group = $result['groups'];

			$gmapLink = '<a href="https://www.google.com/maps?q=loc:' . $latlon . '" target="_blank" style="text-decoration: underline; color:#000099;" />G-Link</td>';

			$data[] = [
				"AssetID" => $vehicle,
				"Vehicle Name" => $shortName,
				"Device ID" => $deviceId,
				"Org Name" => $orgId,
				"Reg No" => $regNo,
				"Groups" => $group,
				"Position" => $vehStatus,
				"Ignition Status" => $ign,
				"Last Comm" => $comm,
				"Last Loc" => $loc,
				"G-Map" => $gmapLink,
				"Address" => $address,
				"Speed" => $speed,
			];
		}

		// $count = $search == '' ? count($results) : count($results);
		$count = $search == '' ? $resultsCount : $orgCount;
		$response = array(
			"draw" => intval($draw),
			"iTotalRecords" =>  $resultsCount,
			"iTotalDisplayRecords" => $count,
			"aaData" => $data
		);

		return json_encode($response, true);
	}

	public function exportExcel()
	{
		// return Excel::download(new CalibrationExport(), 'CalibrationData.xlsx');
		$litres = request()->get('litre');
		$volts = request()->get('volt');
		$frequencys = request()->get('frequency');

		$nod = 150;
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->getProtection()->setSheet(true);
		$sheet->getStyle('A2:C' . $nod)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
		$sheet
			->SetCellValue('A1', "Litre")
			->SetCellValue('B1', "Volt")
			->SetCellValue('C1', "Frequency");
		$spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(25);
		$sheet->getStyle('A1:C1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
		$sheet->getStyle('A1:C1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

		$sheet->getStyle('A1:C1')->getAlignment()->applyFromArray(
			array('horizontal' => 'center', 'text-transform' => 'uppercase')
		);
		$sheet->getStyle('A2:C2')->getAlignment()->setWrapText(true);
		$i = 2;
		if ($litres == null) $litres = [];
		foreach ($litres as $key => $litre) {
			$volt = $volts[$key];
			$frequency = $frequencys[$key];
			if (is_null($litre)) $litre = 0;
			if (is_null($volt)) $volt = 0;
			if (is_null($frequency)) $frequency = 0;

			if ($litre != 0) {
				$sheet->SetCellValue('A' . $i, $litre);
				$sheet->SetCellValue('B' . $i, $volt);
				$sheet->SetCellValue('C' . $i, $frequency);
				$i++;
			}
		}
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="CalibrationData.xlsx"');
		$writer->save("php://output");
	}

	public function uploadCalibrate()
	{
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$tagArray = null;
		$voltLitredarray = null;
		$frequencyLitredArray = null;
		$calibrateCount1  =  request()->get('jval');
		$vehicleId  =  request()->get('vehicleId');
		$sensorType = request()->get('sensorType');
		$newCalibrate = $calibrateCount1 * 2;
		$litres = null;
		$volts = null;
		$frequencys = null;
		if (!request()->hasFile('import_file')) {
			return redirect()->back()->withErrors('Please upload valid extension(.xls,.csv) file');
		}
		$filename = request()->file('import_file')->getClientOriginalName();
		if ((strpos($filename, '.xls') == false) && (strpos($filename, '.csv') == false) && (strpos($filename, '.xlsx') == false)) {
			return redirect()->back()->withErrors('Please upload valid extension(.xls,.csv) file');
		}
		$file_array = explode(".", $_FILES['import_file']['name']);
		$file_extension = end($file_array);
		$path = request()->file('import_file')->getRealPath();
		$reader = IOFactory::createReader('Xlsx');
		$spreadsheet = $reader->load($_FILES['import_file']['tmp_name']);
		$data = $spreadsheet->getActiveSheet()->toArray();
		unset($data[0]);
		$pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\\?\\\']/';
		if (empty($data)) {
			return redirect()->back()->withInput()->witzhErrors('Please upload valid data file.');
		}
		foreach ($data as $row) {
			$litreData = $row[0];
			$voltData = $row[1];
			$frequencyData = $row[2];
			if ($litreData == null || $litreData == '' || $litreData <= 0 || is_string($litreData)) continue;
			if ($voltData == null || $voltData == '' || $voltData <= 0 || is_string($voltData)) $voltData = 0;
			if ($frequencyData == null || $frequencyData == '' || $frequencyData <= 0 || is_string($frequencyData)) $frequencyData = 0;
			if ($sensorType == "analog" || $sensorType == "analog2") {
				if ($litreData == 0 || $voltData == 0) {
					return redirect()->back()->withInput()->withErrors('Sensor Type is analog. so Litre and Volt not be zero or charactor');
				}
			} else {
				if ($litreData == 0 || $frequencyData == 0) {
					return redirect()->back()->withInput()->withErrors('Sensor Type is LSS Fuel. so Litre and Frequency not be zero or charactor');
				}
			}
			if (!preg_match($pattern, $litreData) && !preg_match($pattern, $voltData) && !preg_match($pattern, $frequencyData)) {
				$litres[] = $row[0];
				$volts[] = $row[1];
				$frequencys[] = $row[2];
			} else {
				if (preg_match($pattern, $frequencyData)) {
					$frequencyData = str_replace('.', '^', $frequencyData);
					$frequencyLitredArray = Arr::add($frequencyLitredArray, $frequencyData, $frequencyData);
				}
				if (preg_match($pattern, $litreData)) {
					$litreData = str_replace('.', '^', $litreData);
					$voltLitredarray = Arr::add($voltLitredarray, $litreData, $litreData);
				}
			}
		}
		if (is_null($litres)) {
			return redirect()->back()->withInput()->withErrors('Please upload valid data file.');
		}
		if ($frequencyLitredArray != null) {
			$err1 = implode(",", $frequencyLitredArray);
			$err1 = str_replace('^', '.', $err1);
			return redirect()->withErrors(array('Special Characters are used in Frequency: ' . $err1));
		}
		if ($voltLitredarray != null) {
			$err1 = implode(",", $voltLitredarray);
			$err1 = str_replace('^', '.', $err1);
			return redirect()->back()->withErrors(array('Special Characters are used in Volt : ' . $err1));
		}

		$sensorType = request()->get('sensorType');
		$tankId = request()->get('tankId');
		$sensorNo = request()->get('sensorNo');
		if ($sensorNo == 1) {
			$sensorNo = "";
		}
		if ($sensorNo == "" || $sensorNo == null) {
			$redis->hset('H_SensorTank_' . $fcode, $vehicleId . ':1', $tankId . ':' . $sensorType);
		} else {
			$redis->hset('H_SensorTank_' . $fcode, $vehicleId . ':' . $sensorNo, $tankId . ':' . $sensorType);
		}
		$Z_Sensor = 'Z_Sensor' . $sensorNo . '_' . $vehicleId . '_' . $fcode;

		if ($sensorType == "analog" || $sensorType == "analog2") {
			$redis->del('Z_Sensor' . $sensorNo . '_' . $vehicleId . '_analog_' . $fcode);
			foreach ($litres as $key => $litre) {
				if ($litre == null || $litre == '' || $litre <= 0) return redirect()->back()->withInput()->withErrors('Please upload valid data file.');
				$volt = $volts[$key];
				$frequency = $frequencys[$key];
				if ($litre == null || $litre == '' || $litre <= 0) $litre = "0";
				if ($volt == null || $volt == '' || $volt <= 0) $volt = "0";
				if ($frequency == null || $frequency == '' || $frequency <= 0) $frequency = "0";
				$redis->zadd($Z_Sensor, $volt, $litre);
				$redis->zadd('Z_Sensor' . $sensorNo . '_' . $vehicleId . '_serial_' . $fcode, $frequency, $litre);
			}
		} else {
			$redis->del('Z_Sensor' . $sensorNo . '_' . $vehicleId . '_serial_' . $fcode);
			foreach ($litres as $key => $litre) {
				if ($litre == null || $litre == '' || $litre <= 0) return redirect()->back()->withInput()->withErrors('Please upload valid data file.');
				$volt = $volts[$key];
				$frequency = $frequencys[$key];

				if ($litre == null || $litre == '' || $litre <= 0) $litre = "5";
				if ($volt == null || $volt == '' || $volt <= 0) $volt = "0";
				if ($frequency == null || $frequency == '' || $frequency <= 0) $frequency = "0";

				$redis->zadd($Z_Sensor, $frequency, $litre);
				$redis->zadd('Z_Sensor' . $sensorNo . '_' . $vehicleId . '_analog_' . $fcode, $volt, $litre);
			}
		}


		session()->flash('message', 'Calibrated Successfully');
		return redirect()->to('VdmVehicleScan' . $vehicleId);
	}

	public function downloadAutoCalibrate()
	{
		try {
			$type = 'xls';
			$data = [' Litre ', ' Volt '];
			$maxvolt = request()->get('maxvolt');
			$minvolt = request()->get('minvolt') ?? 0; 
			$tanksize = request()->get('tanksize');
			$tankshape = request()->get('tankshape');
			$fueltype = request()->get('fuelType');

			if ($maxvolt == null || $maxvolt == '' || $maxvolt <= 0 || $tanksize == null || $tanksize == '' || $tanksize <= 0 || $tankshape == null || $tankshape == '' || $fueltype == null || $fueltype == '') {
				// return [$maxvolt ,$tanksize,$tankshape,$fueltype];
				$err = [];
				if ($maxvolt == null || $maxvolt == '' || $maxvolt <= 0) $err[] = "Maximum Volt";
				if ($tanksize == null || $tanksize == '' || $tanksize <= 0) $err[] = "Tank Size";
				if ($tankshape == null || $tankshape == '') $err[] = "Tank Shape";
				if ($fueltype == null || $fueltype == '') $err[] = "Fuel Type";
				$errorMsg = join(", ", $err);
				return redirect()->back()->withInput()->withErrors("please, Enter $errorMsg Values");
			}


			$i = 2;
			$increment = 0.1;
			if ($fueltype == "analog" || $fueltype == "analog2") $increment = 0.001;
			$res = [];
			$index = 0;

			if ($tankshape == "Cylinder") {
				$liter = 5;
				for ($index; $index < $maxvolt; $index += $increment) {
					if ($index == 0) continue;
					$volt = $index;
					$fuelLiter = self::getCylinderLiterVolt($volt);
					if ($fueltype == "analog" || $fueltype == "analog2") $fuelLiter = round($fuelLiter);
					if ($fuelLiter % 5 === 0) {
						if (isset($res[$fuelLiter])) continue;
						$res[$fuelLiter] = $volt;
						$liter += 5;
					}
				}
			}
			if ($tankshape === "Rectangle") {
				$fuelLiter = self::getRectangleLiter();
				$dividedFuel = ($fuelLiter / 5);
				$incrementVolt = (($maxvolt - $minvolt) / $dividedFuel);
				$fuelIncrement = 5;
				for ($index; $index < $maxvolt; $index += $incrementVolt) {
					$fuel = $fuelIncrement;
					if ($fuelLiter < $fuel) continue;
					$volt = $index + $incrementVolt;
					$res[$fuel] = $volt;
					if ($volt > $maxvolt) {
						$res[$fuel] = $maxvolt;
					}
					$fuelIncrement += 5;
				}
			}
			if ($tanksize % 5 !== 0) $res[$tanksize] = $maxvolt;
			if (isset($res["0"])) unset($res["0"]);

			
			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
			$sheet
				->SetCellValue('A1', 'Litre')
				->SetCellValue('B1', 'Volt');
			$sheet->getProtection()->setSheet(true);
			$sheet->getStyle('A2:B51')->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
			$sheet->getStyle('A1:B1')->getAlignment()->applyFromArray(
				array('horizontal' => 'center', 'text-transform' => 'uppercase')
			);
			$sheet->getStyle('A2:B51')->getAlignment()->applyFromArray(
				array('horizontal' => 'center', 'text-transform' => 'uppercase')
			);
			$sheet->getStyle('A2:B2')->getAlignment()->setWrapText(true);
			$sheet->getStyle('A1:B1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
			$sheet->getStyle('A1:B1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
	
			foreach ($res as $key => $value) {
				$litre = number_format((int)$key, 0, '.', '');
				$volt = number_format((float)$value, 2, '.', '');

				if (is_null($volt)) $volt = 0;

				if ($volt == null  || $litre == null || $litre == 0) {
					continue;
				}
				$sheet->SetCellValue('A' . $i, $litre);
				$sheet->SetCellValue('B' . $i, $volt);
				$i++;
			}
			// $sheet->freezeFirstRowAndColumn();


			$writer = new Xlsx($spreadsheet);
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="CalibrationData.xlsx"');
			$writer->save("php://output");

		} catch (Exception $e) {
			ddd($e);
			logger($e);
			return redirect()->back()->withInput();
		}
	}

	static function getCylinderLiterVolt($volt)
	{
		$pi = pi();
		$radius =  request()->get('radius');
		$length =  request()->get('Cylength');
		$maxvolt =  request()->get('maxvolt');
		$minvolt =  request()->get('minvolt');

		$height = $radius * 2;
		$fuelLiter = 0;

		$position =  request()->get('tankposition');
		if ($position == "horizontal") {
			if ($volt > $maxvolt) {
				$height = 2 * $radius;
			} else {
				$height = ($volt / $maxvolt) * (2 * $radius);
			}
			$leftside = ($radius - $height) * sqrt((2 * $radius * $height) - ($height * $height));
			$rightside = (($radius * $radius) * acos(($radius - $height) / $radius));
			$area = $rightside - $leftside;
			$fuelLiter = ($area * $length) / 1000;
		}
		if ($position === 'vertical') {
			if ($volt > $maxvolt) {
				$height = $length;
			} else {
				$height = ($volt / $maxvolt) * $length;
			}
			$fuelLiter = ($pi * ($radius * $radius) * $height) / 1000;
		}
		$fuelLiter = $fuelLiter / 1000;
		return $fuelLiter;
	}

	static function getRectangleLiter()
	{
		$height =  request()->get('recHeight') ?? 0;
		$width =  request()->get('recWidth') ?? 0;
		$length =  request()->get('recLength') ?? 0;
		$fuelLiter = $length * $width * $height / 1000;
		if ($fuelLiter == 0) $fuelLiter =  request()->get('tanksize');
		return $fuelLiter;
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id            
	 * @return Response
	 */
}
