<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InputController extends Controller
{
    // check a value
    public  static function nullCheck($value,$defult='')
    {
        return $value ?? $defult;
    }

    // check a whole array value
    public  static function nullCheckArr($orgArr,$defult='')
    {
        $tempArr=[];
        foreach($orgArr as $key =>$value){
            $tempArr[$key] = InputController::nullCheck($value,$defult);
        }
        return $tempArr;
    }
}
