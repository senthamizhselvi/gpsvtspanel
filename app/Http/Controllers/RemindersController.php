<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Redis;

class RemindersController extends Controller
{

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
		return view('password.remind');
	}
	public function popupModel()
	{
		return view('vdm.login.popupModel');
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		$data = request()->all();
		$redis = Redis::connection();
		$username = request()->get('uname');
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		if (($fcode != null && $username != null) || $username == 'vamos') {
			try {
				$emailTemp = $redis->hget('H_UserId_Cust_Map', $username . ':email');
				session()->put('email', $emailTemp);
				$ad = $redis->hget('H_Franchise', $fcode);
				$refData11    = json_decode($ad, true);
				$userID = isset($refData11['userId']) ? $refData11['userId'] : '';
				$adminuser = $redis->sismember('S_Users_Admin_' . $fcode, $username);
				$deal = $redis->sismember('S_Dealers_' . $fcode, $username);

				$dealerL = $redis->smembers('S_Dealers_' . $fcode);
				foreach ($dealerL as $key => $value1) {
					$dealeruser = $redis->sismember('S_Users_Dealer_' . $value1 . '_' . $fcode, $username);
					if ($dealeruser == 1) {
						break;
					}
				}
				if ($userID == $username || $adminuser == 1) {

					$addetails = $redis->hget('H_Franchise', $fcode);
					$refData    = json_decode($addetails, true);
					$web = isset($refData['website']) ? $refData['website'] : 'gpsvts.net';
					$ipaddress = isset($refData['website']) ? $refData['website'] : 'gpsvts.net';
				} else if ($deal == 1) {
					$addetails = $redis->hget('H_DealerDetails_' . $fcode, $username);
					$refData    = json_decode($addetails, true);
					$web = isset($refData['website']) ? $refData['website'] : 'gpsvts.net';
					$ipaddress = isset($refData['website']) ? $refData['website'] : 'gpsvts.net';
				} else if ($dealeruser == 1) {
					$addetails = $redis->hget('H_DealerDetails_' . $fcode, $value1);
					$refData    = json_decode($addetails, true);
					$web = isset($refData['website']) ? $refData['website'] : 'gpsvts.net';
					$ipaddress = isset($refData['website']) ? $refData['website'] : 'gpsvts.net';
				}

				if ($web == '') {
					$web = 'gpsvts.net';
				}
				if ($ipaddress == '') {
					$ipaddress = 'gpsvts.net';
				}
				$temp = $username . $fcode . time() . $ipaddress;
				$hashurl = Hash::make($temp);
				$hashurl = str_replace("/", "a", $hashurl);
				$url = 'http://' . $ipaddress . '/gps/public/password/reset/' . $hashurl;
				$response = Mail::send('emails.reset', array('url' => $url), function ($message) {
					$message->to(session()->pull('email'))->subject('PASSWORD RESET!');
				});
				$redis->set($hashurl, $username);
				$redis->expire($hashurl, 3600);
				try {
					$emailT = explode('@', $emailTemp);
					$mailId = substr($emailT[0], 0, -3) . '***';
					$emailTemp = $mailId . '@' . $emailT[1];
				} catch (\Exception $e) {
				}

				return redirect()->to('login')->with('flash_notice', 'Please check ' . $emailTemp . ' mail for password details.');
			} catch (\Exception $e) {
				return redirect()->to('login')->with('flash_notice', 'Invalid mail Id.');
			}
		} else {
			return redirect()->to('login')->with('flash_notice', 'Invalid user please check the Username.');
		}
	}

	public function request()
	{
		$redis = Redis::connection();
		$userId = request()->only('userId');
		$email = null;

		$response = Password::remind($email, function ($message) {
			$message->subject('Password Reminder');
		});

		switch ($response) {
			case Password::INVALID_USER:
				return redirect()->back()->with('error', Lang::get($response));

			case Password::REMINDER_SENT:
				return redirect()->to('login')->with('flash_notice', 'Please check your mail for password details.');
		}
	}

	public function reset($token)
	{
		$redis = Redis::connection();
		$username = $redis->get($token);
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$ad = $redis->sismember('S_Franchises', $username);
		$deal = $redis->sismember('S_Dealers_' . $fcode, $username);
		$web = '';
		$dealeruser = '';
		$ad = $redis->hget('H_Franchise', $fcode);
		$refData11    = json_decode($ad, true);
		$userID = isset($refData11['userId']) ? $refData11['userId'] : '';
		$adminuser = $redis->sismember('S_Users_Admin_' . $fcode, $username);
		$deal = $redis->sismember('S_Dealers_' . $fcode, $username);

		$dealerL = $redis->smembers('S_Dealers_' . $fcode);
		foreach ($dealerL as $key => $value1) {
			$dealeruser = $redis->sismember('S_Users_Dealer_' . $value1 . '_' . $fcode, $username);
			if ($dealeruser == 1) {
				break;
			}
		}

		if ($userID == $username || $adminuser == 1) {

			$addetails = $redis->hget('H_Franchise', $fcode);
			$refData    = json_decode($addetails, true);
			$web = isset($refData['website']) ? $refData['website'] : 'gpsvts.net';
		} else if ($deal == 1) {
			$addetails = $redis->hget('H_DealerDetails_' . $fcode, $username);
			$refData    = json_decode($addetails, true);
			$web = isset($refData['website']) ? $refData['website'] : 'gpsvts.net';
		} else if ($dealeruser == 1) {
			$addetails = $redis->hget('H_DealerDetails_' . $fcode, $value1);
			$refData    = json_decode($addetails, true);
			$web = isset($refData['website']) ? $refData['website'] : 'gpsvts.net';
		} else {
			$web = 'gpsvts.net';
		}
		if ($username != null) {

			return view('password.reset', [
				'token' => $token,
				'userId' => $username,
				'web' => $web
			]);
		} else {
			return view('password.expire');
		}
	}

	public function update()
	{
		$redis = Redis::connection();
		$userId = request()->get('userId');
		$token = request()->get('token');
		$password = request()->get('password');
		$passwordCon = request()->get('password_confirmation');

		logger(" user name " . $userId . ' token ' . $token . 'password 1 ' . $password . ' password conf ' . $passwordCon);

		if ($password == null || $passwordCon == null) {
			return redirect()->back()->withErrors('Enter Both password');
		} else if ($password != $passwordCon) {
			return redirect()->back()->withErrors('Password not match');
		}

		$id = User::whereRaw('BINARY username = ?', [$userId])->pluck('id');

		DB::table('users')
			->where('id', $id)
			->update(array('password' => Hash::make($password)));
		$redis->hmset('H_UserId_Cust_Map', $userId . ':password', $password);

		$redis->del($token);
		return redirect()->to('login')->with('flash_notice', 'Your password has been reset');
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) abort(404);

		return view('password.reset', [
			'token' => $token
		]);
	}

	/*
		check old password
	*/

	public function menuResetPassword()
	{

		$username 	= auth()->id();
		$tableValue = DB::select('select username, password from users where username = :username', ['username' => $username]);
		$oldPwd 	= request()->get('pwd');

		foreach ($tableValue as $key => $value) {
			if (Hash::check($oldPwd, array_values(get_object_vars($value))[1]) == 1) {
				logger('Sucess');
				return 'sucess';
			} else {

				logger('fail');
				return 'fail';
			}
		}
	}

	/*
		update password
	*/

	public function menuUpdatePassword()
	{

		$redis 			= Redis::connection();
		$username 		= auth()->id();
		$password 		= request()->get('pwd');
		$oldpassword 	= request()->get('old');
		try {

			$tableValue = DB::select('select username, password, email, id from users where username = :username', ['username' => $username]);
			foreach ($tableValue as $key => $value) {
				if (Hash::check($oldpassword, array_values(get_object_vars($value))[1]) == 1) {
					DB::table('users')->where('id', array_values(get_object_vars($value))[3])->update(array('password' => Hash::make($password)));
					$redis->hmset('H_UserId_Cust_Map', $username . ':password', $password);

					try {

						session()->put('email', array_values(get_object_vars($value))[2]);
						$response = Mail::send('emails.menuReset', array('url' => $username), function ($message) {
							$message->to(session()->pull('email'))->subject('PASSWORD RESET!');
						});
					} catch (Exception $e) {
						logger($e->getMessage());
					}
				} else {
					logger('  old password wrong  ');

					return 'oldPwd';
				}
			}
			return 'sucess';
		} catch (Exception $e) {
			logger($e->getMessage());
			return '';
		}
	}
}
