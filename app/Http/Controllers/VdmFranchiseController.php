<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\AuditFrans;
use Illuminate\Support\Facades\Redis;
use DateTime, DateTimeZone;
use Predis\Client as Client;
use App\Models\User;
use App\Exports\LicenceListExport;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Kreait\Firebase\Exception\FirebaseException;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class VdmFranchiseController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


	public function index()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();

		$redis = Redis::connection();

		$fcodeArray = $redis->smembers('S_Franchises');

		$disableArr	= $redis->smembers('S_Franchises_Disable');
		$disable = null;
		foreach ($disableArr as $key => $value) {
			$disable = Arr::add($disable, $value, $value);
		}

		$fnameArray = null;
		$prepaidArray = null;
		$franArray = array();
		foreach ($fcodeArray as $key => $value) {
			$details = $redis->hget('H_Franchise', $value);
			$details_json = json_decode($details, true);
			$fnameArray = Arr::add($fnameArray, $details_json['fname'], $value);
			$isPrePaid1 = ucfirst(isset($details_json['prepaid']) ? $details_json['prepaid'] : 'No');
			if ($isPrePaid1 == "Yes") {
				$isPrePaid = "Prepaid";
			} else {
				$isPrePaid = "Normal";
			}
			$enableExpiryexist = $redis->sismember('S_EnableVehicleExpiry', $value);
			if ($enableExpiryexist != null) {
				$enableExpiry = 'yes';
			} else {
				$enableExpiry = 'no';
			}
			$franArray[$value] = ['fname' =>  $details_json['fname'], 'fcode' => $value, 'prepaid' => $isPrePaid, 'enableExpiry' => $enableExpiry];
		}
		ksort($franArray);

		$indexData = [
			'fcodeArray' => $fcodeArray,
			'disable' => $disable,
			'franArray' => $franArray,
			'index' => 1
		];
		return view('vdm.franchise.index', $indexData);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$createData = [
			'smsP' => VdmFranchiseController::smsP(),
			'dbIp' =>  VdmFranchiseController::dbIp(),
			'timeZoneC' => VdmFranchiseController::timeZoneC(),
			'backType' => VdmFranchiseController::backTypeC(),
		];
		return view('vdm.franchise.create', $createData);
	}



	public function licenceType()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$fcodeArray = $redis->smembers('S_Franchises');
		foreach ($fcodeArray as $key => $fcode) {
			$details = $redis->hget('H_Franchise', $fcode);
			$details_json = json_decode($details, true);
			$isPrePaid1 = ucfirst(isset($details_json['prepaid']) ? $details_json['prepaid'] : 'No');
			if ($isPrePaid1 == "Yes") {
				$vehicleList = $redis->smembers('S_Vehicles_' . $fcode);
				foreach ($vehicleList as $key => $vehicleId) {
					$vehicleRefData1 = $redis->hget('H_RefData_' . $fcode, $vehicleId);
					$refDataJson1 = json_decode($vehicleRefData1, true);
					$LicenceId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleId);
					$type = substr($LicenceId, 0, 2);
					if ($type == 'BC') {
						$Licence = 'Basic';
					} else if ($type == 'AV') {
						$Licence = 'Advance';
					} else if ($type == 'PM') {
						$Licence = 'Premium';
					} else if ($type == 'PL') {
						$Licence = 'PremiumPlus';
					} else {
						$Licence = '';
					}
					$refData =   array(
						'deviceId' => isset($refDataJson1['deviceId']) ? $refDataJson1['deviceId'] : '',
						'shortName' => isset($refDataJson1['shortName']) ? $refDataJson1['shortName'] : 'nill',
						'deviceModel' => isset($refDataJson1['deviceModel']) ? $refDataJson1['deviceModel'] : 'GT06N',
						'regNo' => isset($refDataJson1['regNo']) ? $refDataJson1['regNo'] : 'XXXXX',
						'vehicleMake' => isset($refDataJson1['vehicleMake']) ? $refDataJson1['vehicleMake'] : ' ',
						'vehicleType' =>  isset($refDataJson1['vehicleType']) ? $refDataJson1['vehicleType'] : 'Bus',
						'oprName' => isset($refDataJson1['oprName']) ? $refDataJson1['oprName'] : 'airtel',
						'mobileNo' => isset($refDataJson1['mobileNo']) ? $refDataJson1['mobileNo'] : '0123456789',
						'overSpeedLimit' => isset($refDataJson1['overSpeedLimit']) ? $refDataJson1['overSpeedLimit'] : '60',
						'odoDistance' => isset($refDataJson1['odoDistance']) ? $refDataJson1['odoDistance'] : '0',
						'driverName' => isset($refDataJson1['driverName']) ? $refDataJson1['driverName'] : 'XXX',
						'gpsSimNo' => isset($refDataJson1['gpsSimNo']) ? $refDataJson1['gpsSimNo'] : '0123456789',
						'email' => isset($refDataJson1['email']) ? $refDataJson1['email'] : '',
						'orgId' => isset($refDataJson1['orgId']) ? $refDataJson1['orgId'] : '',
						'altShortName' => isset($refDataJson1['altShortName']) ? $refDataJson1['altShortName'] : 'nill',
						'fuel' => isset($refDataJson1['fuel']) ? $refDataJson1['fuel'] : 'no',
						'fuelType' => isset($refDataJson1['fuelType']) ? $refDataJson1['fuelType'] : ' ',
						'isRfid' => isset($refDataJson1['isRfid']) ? $refDataJson1['isRfid'] : 'no',
						'rfidType' => isset($refDataJson1['rfidType']) ? $refDataJson1['rfidType'] : 'no',
						'Licence' => $Licence,
						'OWN' => isset($refDataJson1['OWN']) ? $refDataJson1['OWN'] : 'OWN',
						'Payment_Mode' => isset($refDataJson1['Payment_Mode']) ? $refDataJson1['Payment_Mode'] : '',
						'descriptionStatus' => isset($refDataJson1['descriptionStatus']) ? $refDataJson1['descriptionStatus'] : '',
						'ipAddress' => isset($refDataJson1['ipAddress']) ? $refDataJson1['ipAddress'] : '',
						'portNo' => isset($refDataJson1['portNo']) ? $refDataJson1['portNo'] : '0',
						'analog1' => isset($refDataJson1['analog1']) ? $refDataJson1['analog1'] : '',
						'analog2' => isset($refDataJson1['analog2']) ? $refDataJson1['analog2'] : '',
						'digital1' => isset($refDataJson1['digital1']) ? $refDataJson1['digital1'] : '',
						'digital2' => isset($refDataJson1['digital2']) ? $refDataJson1['digital2'] : '',
						'serial1' => isset($refDataJson1['serial1']) ? $refDataJson1['serial1'] : '',
						'serial2' => isset($refDataJson1['serial2']) ? $refDataJson1['serial2'] : '',
						'digitalout' => isset($refDataJson1['digitalout']) ? $refDataJson1['digitalout'] : '',
						'mintemp' => isset($refDataJson1['mintemp']) ? $refDataJson1['mintemp'] : '',
						'maxtemp' => isset($refDataJson1['maxtemp']) ? $refDataJson1['maxtemp'] : '',
						'routeName' => isset($refDataJson1['routeName']) ? $refDataJson1['routeName'] : '',
						'sendGeoFenceSMS' => isset($refDataJson1['sendGeoFenceSMS']) ? $refDataJson1['sendGeoFenceSMS'] : 'no',
						'morningTripStartTime' => isset($refDataJson1['morningTripStartTime']) ? $refDataJson1['morningTripStartTime'] : '',
						'eveningTripStartTime' => isset($refDataJson1['eveningTripStartTime']) ? $refDataJson1['eveningTripStartTime'] : '',
						'parkingAlert' => isset($refDataJson1['parkingAlert']) ? $refDataJson1['parkingAlert'] : 'no',
						'paymentType' => isset($refDataJson1['paymentType']) ? $refDataJson1['paymentType'] : ' ',
						'expiredPeriod' => isset($refDataJson1['expiredPeriod']) ? $refDataJson1['expiredPeriod'] : ' ',
						'vehicleExpiry' => isset($refDataJson1['vehicleExpiry']) ? $refDataJson1['vehicleExpiry'] : 'null',
						'onboardDate' => isset($refDataJson1['onboardDate']) ? $refDataJson1['onboardDate'] : 'null',
						'safetyParking' => isset($refDataJson1['safetyParking']) ? $refDataJson1['safetyParking'] : 'no',
						'tankSize' => isset($refDataJson1['tankSize']) ? $refDataJson1['tankSize'] : '',
						'licenceissuedDate' => isset($refDataJson1['licenceissuedDate']) ? $refDataJson1['licenceissuedDate'] : '',
						'VolIg' => isset($refDataJson1['VolIg']) ? $refDataJson1['VolIg'] : '',
						'batteryvolt' => isset($refDataJson1['batteryvolt']) ? $refDataJson1['batteryvolt'] : '',
						'ac' => isset($refDataJson1['ac']) ? $refDataJson1['ac'] : '',
						'acVolt' => isset($refDataJson1['acVolt']) ? $refDataJson1['acVolt'] : '',
						'distManipulationPer' => isset($refDataJson1['distManipulationPer']) ? $refDataJson1['distManipulationPer'] : '',
						'assetTracker' => isset($refDataJson1['assetTracker']) ? $refDataJson1['assetTracker'] : 'no',
						'communicatingPortNo' => isset($refDataJson1['communicatingPortNo']) ? $refDataJson1['communicatingPortNo'] : '',
						'onboardDate'  => isset($refDataJson1['onboardDate']) ? $refDataJson1['onboardDate'] : '',
						'driverName'    => isset($refDataJson1['driverName']) ? $refDataJson1['driverName'] : '',
						'driverMobile' => isset($refDataJson1['driverMobile']) ? $refDataJson1['driverMobile'] : '',
						'date' => isset($refDataJson1['date']) ? $refDataJson1['date'] : '',
					);
					$refDataJson = json_encode($refData);
					$redis->hmset('H_RefData_' . $fcode, $vehicleId, $refDataJson);
				}
			}
		}
		session()->flash('message', 'Franchise LicenceId Type successfully modified !');
		return redirect()->to('vdmFranchises');
	}


	public static function dbIp()
	{
		$dbIp 	= array();
		$redis 	= Redis::connection();
		$ipList = $redis->smembers('S_DB_Details');
		foreach ($ipList as  $key => $ip) {
			$dbIp = Arr::add($dbIp, $key, $ip);
		}
		return $dbIp;
	}


	public static function backTypeC()
	{
		$backType = array();
		$backType = Arr::add($backType, 'sqlite', 'Sqlite');
		$backType = Arr::add($backType, 'mysql', 'Mysql');
		return $backType;
	}

	public static function smsP()
	{
		$redis = Redis::connection();
		$smsProvider = $redis->lrange('L_SMS_Provider', 0, -1);
		$smsP = array();
		foreach ($smsProvider as $sms) {
			$smsP = Arr::add($smsP, $sms, $sms);
		}
		return $smsP;
	}

	public static function getProtocal($Ltype)
	{
		$redis = Redis::connection();
		/*
		if ($Ltype == 'Basic') {
			$protocal = $redis->lrange('L_Protocal', 0, -1);
		} else if ($Ltype == 'Advance') {
			$protocal = $redis->lrange('L_Protocal', 0, -1);
		} else if ($Ltype == 'Premium') {
			$protocal = $redis->lrange('L_Protocal', 0, -1);
		} else if ($Ltype == 'PremiumPlus') {
			$protocal = $redis->lrange('L_Protocal', 0, -1);
		} else {
			$protocal = $redis->lrange('L_Protocal', 0, -1);
		}
		*/
		$protocal = $redis->lrange('L_Protocal', 0, -1);
		sort($protocal);
		$getProtocal = array();
		foreach ($protocal as $pro) {
			$value 	= explode(":", $pro);
			$check = isset($getProtocal[$value[0]]);
			$len = strlen($check);
			if ($len == 0) {
				$getProtocal = Arr::add($getProtocal, $value[0], $value[0] . ' (' . $value[1] . ') ');
			} else {
				$getProtocal = Arr::add($getProtocal, $value[0] . '1', $value[0] . ' (' . $value[1] . ') ');
			}
		}
		return $getProtocal;
	}

	public static function timeZoneC()
	{
		$zones_array = array();
		$timezoneList = array();
		$timestamp = time();
		$dummy_datetime_object = new DateTime();
		foreach (timezone_identifiers_list() as $key => $zone) {
			date_default_timezone_set($zone);
			$utc = 'UTC/GMT ' . date('P', $timestamp);
			$timezoneList = Arr::add($timezoneList, $zone, $zone . ' - ' . $utc);
			$zones_array[$key]['zone'] = $zone;
			$zones_array[$key]['diff_from_GMT'] = $utc;
			$tz = new DateTimeZone($zone);
			$zones_array[$key]['offset'] = $tz->getOffset($dummy_datetime_object);
		}
		// additional timeZone

		$timezoneList = Arr::add($timezoneList, 'America/Panama', 'America/Peru - UTC/GMT -05:00 ');
		return $timezoneList;
	}

	public static function timeZoneCOld()
	{
		$timeZoneC = array();
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+12', 'Etc/GMT+12');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+11', 'Etc/GMT+11');
		$timeZoneC = Arr::add($timeZoneC, 'MIT', 'MIT');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Apia', 'Pacific/Apia');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Midway', 'Pacific/Midway');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Niue', 'Pacific/Niue');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Pago_Pago', 'Pacific/Pago_Pago');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Samoa', 'Pacific/Samoa');
		$timeZoneC = Arr::add($timeZoneC, 'US/Samoa', 'US/Samoa');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Apia', 'Pacific/Apia');
		$timeZoneC = Arr::add($timeZoneC, 'America/Adak', 'America/Adak');
		$timeZoneC = Arr::add($timeZoneC, 'America/Atka', 'America/Atka');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+10', 'Etc/GMT+10');
		$timeZoneC = Arr::add($timeZoneC, 'HST', 'HST');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Fakaofo', 'Pacific/Fakaofo');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Honolulu', 'Pacific/Honolulu');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Johnston', 'Pacific/Johnston');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Rarotonga', 'Pacific/Rarotonga');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Tahiti', 'Pacific/Tahiti');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/HST10', 'SystemV/HST10');
		$timeZoneC = Arr::add($timeZoneC, 'US/Aleutian', 'US/Aleutian');
		$timeZoneC = Arr::add($timeZoneC, 'US/Hawaii', 'US/Hawaii');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Marquesas', 'Pacific/Marquesas');
		$timeZoneC = Arr::add($timeZoneC, 'AST', 'AST');
		$timeZoneC = Arr::add($timeZoneC, 'America/Anchorage', 'America/Anchorage');
		$timeZoneC = Arr::add($timeZoneC, 'America/Juneau', 'America/Juneau');
		$timeZoneC = Arr::add($timeZoneC, 'America/Nome', 'America/Nome');
		$timeZoneC = Arr::add($timeZoneC, 'America/Yakutat', 'America/Yakutat');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+9', 'Etc/GMT+9');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Gambier', 'Pacific/Gambier');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/YST9', 'SystemV/YST9');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/YST9YDT', 'SystemV/YST9YDT');
		$timeZoneC = Arr::add($timeZoneC, 'US/Alaska', 'US/Alaska');
		$timeZoneC = Arr::add($timeZoneC, 'America/Dawson', 'America/Dawson');
		$timeZoneC = Arr::add($timeZoneC, 'America/Ensenada', 'America/Ensenada');
		$timeZoneC = Arr::add($timeZoneC, 'America/Los_Angeles', 'America/Los_Angeles');
		$timeZoneC = Arr::add($timeZoneC, 'America/Tijuana', 'America/Tijuana');
		$timeZoneC = Arr::add($timeZoneC, 'America/Vancouver', 'America/Vancouver');
		$timeZoneC = Arr::add($timeZoneC, 'America/Whitehorse', 'America/Whitehorse');
		$timeZoneC = Arr::add($timeZoneC, 'Canada/Pacific', 'Canada/Pacific');
		$timeZoneC = Arr::add($timeZoneC, 'Canada/Yukon', 'Canada/Yukon');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+8', 'Etc/GMT+8');
		$timeZoneC = Arr::add($timeZoneC, 'Mexico/BajaNorte', 'Mexico/BajaNorte');
		$timeZoneC = Arr::add($timeZoneC, 'PST', 'PST');
		$timeZoneC = Arr::add($timeZoneC, 'PST8PDT', 'PST8PDT');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Pitcairn', 'Pacific/Pitcairn');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/PST8', 'SystemV/PST8');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/PST8PDT', 'SystemV/PST8PDT');
		$timeZoneC = Arr::add($timeZoneC, 'US/Pacific', 'US/Pacific');
		$timeZoneC = Arr::add($timeZoneC, 'US/Pacific-New', 'US/Pacific-New');
		$timeZoneC = Arr::add($timeZoneC, 'America/Boise', 'America/Boise');
		$timeZoneC = Arr::add($timeZoneC, 'America/Cambridge_Bay', 'America/Cambridge_Bay');
		$timeZoneC = Arr::add($timeZoneC, 'America/Chihuahua', 'America/Chihuahua');
		$timeZoneC = Arr::add($timeZoneC, 'America/Dawson_Creek', 'America/Dawson_Creek');
		$timeZoneC = Arr::add($timeZoneC, 'America/Denver', 'America/Denver');
		$timeZoneC = Arr::add($timeZoneC, 'America/Edmonton', 'America/Edmonton');
		$timeZoneC = Arr::add($timeZoneC, 'America/Hermosillo', 'America/Hermosillo');
		$timeZoneC = Arr::add($timeZoneC, 'America/Inuvik', 'America/Inuvik');
		$timeZoneC = Arr::add($timeZoneC, 'America/Mazatlan', 'America/Mazatlan');
		$timeZoneC = Arr::add($timeZoneC, 'America/Phoenix', 'America/Phoenix');
		$timeZoneC = Arr::add($timeZoneC, 'America/Shiprock', 'America/Shiprock');
		$timeZoneC = Arr::add($timeZoneC, 'America/Yellowknife', 'America/Yellowknife');
		$timeZoneC = Arr::add($timeZoneC, 'Canada/Mountain', 'Canada/Mountain');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+7', 'Etc/GMT+7');
		$timeZoneC = Arr::add($timeZoneC, 'MST', 'MST');
		$timeZoneC = Arr::add($timeZoneC, 'MST7MDT', 'MST7MDT');
		$timeZoneC = Arr::add($timeZoneC, 'Mexico/BajaSur', 'Mexico/BajaSur');
		$timeZoneC = Arr::add($timeZoneC, 'Navajo', 'Navajo');
		$timeZoneC = Arr::add($timeZoneC, 'PNT', 'PNT');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/MST7', 'SystemV/MST7');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/MST7MDT', 'SystemV/MST7MDT');
		$timeZoneC = Arr::add($timeZoneC, 'US/Arizona', 'US/Arizona');
		$timeZoneC = Arr::add($timeZoneC, 'US/Mountain', 'US/Mountain');
		$timeZoneC = Arr::add($timeZoneC, 'America/Belize', 'America/Belize');
		$timeZoneC = Arr::add($timeZoneC, 'America/Cancun', 'America/Cancun');
		$timeZoneC = Arr::add($timeZoneC, 'America/Chicago', 'America/Chicago');
		$timeZoneC = Arr::add($timeZoneC, 'America/Costa_Rica', 'America/Costa_Rica');
		$timeZoneC = Arr::add($timeZoneC, 'America/El_Salvador', 'America/El_Salvador');
		$timeZoneC = Arr::add($timeZoneC, 'America/Guatemala', 'America/Guatemala');
		$timeZoneC = Arr::add($timeZoneC, 'America/Managua', 'America/Managua');
		$timeZoneC = Arr::add($timeZoneC, 'America/Menominee', 'America/Menominee');
		$timeZoneC = Arr::add($timeZoneC, 'America/Merida', 'America/Merida');
		$timeZoneC = Arr::add($timeZoneC, 'America/Mexico_City', 'America/Mexico_City');
		$timeZoneC = Arr::add($timeZoneC, 'America/Monterrey', 'America/Monterrey');
		$timeZoneC = Arr::add($timeZoneC, 'America/North_Dakota/Center', 'America/North_Dakota/Center');
		$timeZoneC = Arr::add($timeZoneC, 'America/Rainy_River', 'America/Rainy_River');
		$timeZoneC = Arr::add($timeZoneC, 'America/Rankin_Inlet', 'America/Rankin_Inlet');
		$timeZoneC = Arr::add($timeZoneC, 'America/Regina', 'America/Regina');
		$timeZoneC = Arr::add($timeZoneC, 'America/Swift_Current', 'America/Swift_Current');
		$timeZoneC = Arr::add($timeZoneC, 'America/Tegucigalpa', 'America/Tegucigalpa');
		$timeZoneC = Arr::add($timeZoneC, 'America/Winnipeg', 'America/Winnipeg');
		$timeZoneC = Arr::add($timeZoneC, 'CST', 'CST');
		$timeZoneC = Arr::add($timeZoneC, 'CST6CDT', 'CST6CDT');
		$timeZoneC = Arr::add($timeZoneC, 'Canada/Central', 'Canada/Central');
		$timeZoneC = Arr::add($timeZoneC, 'Canada/East-Saskatchewan', 'Canada/East-Saskatchewan');
		$timeZoneC = Arr::add($timeZoneC, 'Canada/Saskatchewan', 'Canada/Saskatchewan');
		$timeZoneC = Arr::add($timeZoneC, 'Chile/EasterIsland', 'Chile/EasterIsland');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+6', 'Etc/GMT+6');
		$timeZoneC = Arr::add($timeZoneC, 'Mexico/General', 'Mexico/General');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Easter', 'Pacific/Easter');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Galapagos', 'Pacific/Galapagos');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/CST6', 'SystemV/CST6');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/CST6CDT', 'SystemV/CST6CDT');
		$timeZoneC = Arr::add($timeZoneC, 'US/Central', 'US/Central');
		$timeZoneC = Arr::add($timeZoneC, 'America/Bogota', 'America/Bogota');
		$timeZoneC = Arr::add($timeZoneC, 'America/Cayman', 'America/Cayman');
		$timeZoneC = Arr::add($timeZoneC, 'America/Detroit', 'America/Detroit');
		$timeZoneC = Arr::add($timeZoneC, 'America/Eirunepe', 'America/Eirunepe');
		$timeZoneC = Arr::add($timeZoneC, 'America/Fort_Wayne', 'America/Fort_Wayne');
		$timeZoneC = Arr::add($timeZoneC, 'America/Grand_Turk', 'America/Grand_Turk');
		$timeZoneC = Arr::add($timeZoneC, 'America/Guayaquil', 'America/Guayaquil');
		$timeZoneC = Arr::add($timeZoneC, 'America/Havana', 'America/Havana');
		$timeZoneC = Arr::add($timeZoneC, 'America/Indiana/Indianapolis', 'America/Indiana/Indianapolis');
		$timeZoneC = Arr::add($timeZoneC, 'America/Indiana/Knox', 'America/Indiana/Knox');
		$timeZoneC = Arr::add($timeZoneC, 'America/Indiana/Marengo', 'America/Indiana/Marengo');
		$timeZoneC = Arr::add($timeZoneC, 'America/Indiana/Vevay', 'America/Indiana/Vevay');
		$timeZoneC = Arr::add($timeZoneC, 'America/Indianapolis', 'America/Indianapolis');
		$timeZoneC = Arr::add($timeZoneC, 'America/Iqaluit', 'America/Iqaluit');
		$timeZoneC = Arr::add($timeZoneC, 'America/Jamaica', 'America/Jamaica');
		$timeZoneC = Arr::add($timeZoneC, 'America/Kentucky/Louisville', 'America/Kentucky/Louisville');
		$timeZoneC = Arr::add($timeZoneC, 'America/Kentucky/Monticello', 'America/Kentucky/Monticello');
		$timeZoneC = Arr::add($timeZoneC, 'America/Knox_IN', 'America/Knox_IN');
		$timeZoneC = Arr::add($timeZoneC, 'America/Lima', 'America/Lima');
		$timeZoneC = Arr::add($timeZoneC, 'America/Louisville', 'America/Louisville');
		$timeZoneC = Arr::add($timeZoneC, 'America/Montreal', 'America/Montreal');
		$timeZoneC = Arr::add($timeZoneC, 'America/Nassau', 'America/Nassau');
		$timeZoneC = Arr::add($timeZoneC, 'America/New_York', 'America/New_York');
		$timeZoneC = Arr::add($timeZoneC, 'America/Nipigon', 'America/Nipigon');
		$timeZoneC = Arr::add($timeZoneC, 'America/Panama', 'America/Panama');
		$timeZoneC = Arr::add($timeZoneC, 'America/Pangnirtung', 'America/Pangnirtung');
		$timeZoneC = Arr::add($timeZoneC, 'America/Port-au-Prince', 'America/Port-au-Prince');
		$timeZoneC = Arr::add($timeZoneC, 'America/Porto_Acre', 'America/Porto_Acre');
		$timeZoneC = Arr::add($timeZoneC, 'America/Rio_Branco', 'America/Rio_Branco');
		$timeZoneC = Arr::add($timeZoneC, 'America/Thunder_Bay', 'America/Thunder_Bay');
		$timeZoneC = Arr::add($timeZoneC, 'America/Toronto', 'America/Toronto');
		$timeZoneC = Arr::add($timeZoneC, 'Brazil/Acre', 'Brazil/Acre');
		$timeZoneC = Arr::add($timeZoneC, 'Canada/Eastern', 'Canada/Eastern');
		$timeZoneC = Arr::add($timeZoneC, 'Cuba', 'Cuba');
		$timeZoneC = Arr::add($timeZoneC, 'EST', 'EST');
		$timeZoneC = Arr::add($timeZoneC, 'EST5EDT', 'EST5EDT');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+5', 'Etc/GMT+5');
		$timeZoneC = Arr::add($timeZoneC, 'IET', 'IET');
		$timeZoneC = Arr::add($timeZoneC, 'Jamaica', 'Jamaica');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/EST5', 'SystemV/EST5');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/EST5EDT', 'SystemV/EST5EDT');
		$timeZoneC = Arr::add($timeZoneC, 'US/East-Indiana', 'US/East-Indiana');
		$timeZoneC = Arr::add($timeZoneC, 'US/Eastern', 'US/Eastern');
		$timeZoneC = Arr::add($timeZoneC, 'US/Indiana-Starke', 'US/Indiana-Starke');
		$timeZoneC = Arr::add($timeZoneC, 'US/Michigan', 'US/Michigan');
		$timeZoneC = Arr::add($timeZoneC, 'America/Anguilla', 'America/Anguilla');
		$timeZoneC = Arr::add($timeZoneC, 'America/Antigua', 'America/Antigua');
		$timeZoneC = Arr::add($timeZoneC, 'America/Aruba', 'America/Aruba');
		$timeZoneC = Arr::add($timeZoneC, 'America/Asuncion', 'America/Asuncion');
		$timeZoneC = Arr::add($timeZoneC, 'America/Barbados', 'America/Barbados');
		$timeZoneC = Arr::add($timeZoneC, 'America/Boa_Vista', 'America/Boa_Vista');
		$timeZoneC = Arr::add($timeZoneC, 'America/Campo_Grande', 'America/Campo_Grande');
		$timeZoneC = Arr::add($timeZoneC, 'America/Caracas', 'America/Caracas');
		$timeZoneC = Arr::add($timeZoneC, 'America/Cuiaba', 'America/Cuiaba');
		$timeZoneC = Arr::add($timeZoneC, 'America/Curacao', 'America/Curacao');
		$timeZoneC = Arr::add($timeZoneC, 'America/Dominica', 'America/Dominica');
		$timeZoneC = Arr::add($timeZoneC, 'America/Glace_Bay', 'America/Glace_Bay');
		$timeZoneC = Arr::add($timeZoneC, 'America/Goose_Bay', 'America/Goose_Bay');
		$timeZoneC = Arr::add($timeZoneC, 'America/Grenada', 'America/Grenada');
		$timeZoneC = Arr::add($timeZoneC, 'America/Guadeloupe', 'America/Guadeloupe');
		$timeZoneC = Arr::add($timeZoneC, 'America/Guyana', 'America/Guyana');
		$timeZoneC = Arr::add($timeZoneC, 'America/Halifax', 'America/Halifax');
		$timeZoneC = Arr::add($timeZoneC, 'America/La_Paz', 'America/La_Paz');
		$timeZoneC = Arr::add($timeZoneC, 'America/Manaus', 'America/Manaus');
		$timeZoneC = Arr::add($timeZoneC, 'America/Martinique', 'America/Martinique');
		$timeZoneC = Arr::add($timeZoneC, 'America/Montserrat', 'America/Montserrat');
		$timeZoneC = Arr::add($timeZoneC, 'America/Port_of_Spain', 'America/Port_of_Spain');
		$timeZoneC = Arr::add($timeZoneC, 'America/Porto_Velho', 'America/Porto_Velho');
		$timeZoneC = Arr::add($timeZoneC, 'America/Puerto_Rico', 'America/Puerto_Rico');
		$timeZoneC = Arr::add($timeZoneC, 'America/Santiago', 'America/Santiago');
		$timeZoneC = Arr::add($timeZoneC, 'America/Santo_Domingo', 'America/Santo_Domingo');
		$timeZoneC = Arr::add($timeZoneC, 'America/St_Kitts', 'America/St_Kitts');
		$timeZoneC = Arr::add($timeZoneC, 'America/St_Lucia', 'America/St_Lucia');
		$timeZoneC = Arr::add($timeZoneC, 'America/St_Thomas', 'America/St_Thomas');
		$timeZoneC = Arr::add($timeZoneC, 'America/St_Vincent', 'America/St_Vincent');
		$timeZoneC = Arr::add($timeZoneC, 'America/Thule', 'America/Thule');
		$timeZoneC = Arr::add($timeZoneC, 'America/Tortola', 'America/Tortola');
		$timeZoneC = Arr::add($timeZoneC, 'America/Virgin', 'America/Virgin');
		$timeZoneC = Arr::add($timeZoneC, 'Antarctica/Palmer', 'Antarctica/Palmer');
		$timeZoneC = Arr::add($timeZoneC, 'Atlantic/Bermuda', 'Atlantic/Bermuda');
		$timeZoneC = Arr::add($timeZoneC, 'Atlantic/Stanley', 'Atlantic/Stanley');
		$timeZoneC = Arr::add($timeZoneC, 'Brazil/West', 'Brazil/West');
		$timeZoneC = Arr::add($timeZoneC, 'Canada/Atlantic', 'Canada/Atlantic');
		$timeZoneC = Arr::add($timeZoneC, 'Chile/Continental', 'Chile/Continental');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+4', 'Etc/GMT+4');
		$timeZoneC = Arr::add($timeZoneC, 'PRT', 'PRT');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/AST4', 'SystemV/AST4');
		$timeZoneC = Arr::add($timeZoneC, 'SystemV/AST4ADT', 'SystemV/AST4ADT');
		$timeZoneC = Arr::add($timeZoneC, 'America/St_Johns', 'America/St_Johns');
		$timeZoneC = Arr::add($timeZoneC, 'CNT', 'CNT');
		$timeZoneC = Arr::add($timeZoneC, 'Canada/Newfoundland', 'Canada/Newfoundland');
		$timeZoneC = Arr::add($timeZoneC, 'AGT', 'AGT');
		$timeZoneC = Arr::add($timeZoneC, 'America/Araguaina', 'America/Araguaina');
		$timeZoneC = Arr::add($timeZoneC, 'America/Bahia', 'America/Bahia');
		$timeZoneC = Arr::add($timeZoneC, 'America/Belem', 'America/Belem');
		$timeZoneC = Arr::add($timeZoneC, 'America/Buenos_Aires', 'America/Buenos_Aires');
		$timeZoneC = Arr::add($timeZoneC, 'America/Catamarca', 'America/Catamarca');
		$timeZoneC = Arr::add($timeZoneC, 'America/Cayenne', 'America/Cayenne');
		$timeZoneC = Arr::add($timeZoneC, 'America/Cordoba', 'America/Cordoba');
		$timeZoneC = Arr::add($timeZoneC, 'America/Fortaleza', 'America/Fortaleza');
		$timeZoneC = Arr::add($timeZoneC, 'America/Godthab', 'America/Godthab');
		$timeZoneC = Arr::add($timeZoneC, 'America/Jujuy', 'America/Jujuy');
		$timeZoneC = Arr::add($timeZoneC, 'America/Maceio', 'America/Maceio');
		$timeZoneC = Arr::add($timeZoneC, 'America/Mendoza', 'America/Mendoza');
		$timeZoneC = Arr::add($timeZoneC, 'America/Miquelon', 'America/Miquelon');
		$timeZoneC = Arr::add($timeZoneC, 'America/Montevideo', 'America/Montevideo');
		$timeZoneC = Arr::add($timeZoneC, 'America/Paramaribo', 'America/Paramaribo');
		$timeZoneC = Arr::add($timeZoneC, 'America/Recife', 'America/Recife');
		$timeZoneC = Arr::add($timeZoneC, 'America/Rosario', 'America/Rosario');
		$timeZoneC = Arr::add($timeZoneC, 'America/Sao_Paulo', 'America/Sao_Paulo');
		$timeZoneC = Arr::add($timeZoneC, 'Antarctica/Rothera', 'Antarctica/Rothera');
		$timeZoneC = Arr::add($timeZoneC, 'BET', 'BET');
		$timeZoneC = Arr::add($timeZoneC, 'Brazil/East', 'Brazil/East');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+3', 'Etc/GMT+3');
		$timeZoneC = Arr::add($timeZoneC, 'America/Noronha', 'America/Noronha');
		$timeZoneC = Arr::add($timeZoneC, 'Atlantic/South_Georgia', 'Atlantic/South_Georgia');
		$timeZoneC = Arr::add($timeZoneC, 'Brazil/DeNoronha', 'Brazil/DeNoronha');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+2', 'Etc/GMT+2');
		$timeZoneC = Arr::add($timeZoneC, 'America/Scoresbysund', 'America/Scoresbysund');
		$timeZoneC = Arr::add($timeZoneC, 'Atlantic/Azores', 'Atlantic/Azores');
		$timeZoneC = Arr::add($timeZoneC, 'Atlantic/Cape_Verde', 'Atlantic/Cape_Verde');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+1', 'Etc/GMT+1');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Abidjan', 'Africa/Abidjan');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Accra', 'Africa/Accra');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Bamako', 'Africa/Bamako');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Banjul', 'Africa/Banjul');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Bissau', 'Africa/Bissau');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Casablanca', 'Africa/Casablanca');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Conakry', 'Africa/Conakry');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Dakar', 'Africa/Dakar');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/El_Aaiun', 'Africa/El_Aaiun');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Freetown', 'Africa/Freetown');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Lome', 'Africa/Lome');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Monrovia', 'Africa/Monrovia');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Nouakchott', 'Africa/Nouakchott');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Ouagadougou', 'Africa/Ouagadougou');


		$timeZoneC = Arr::add($timeZoneC, 'Africa/Sao_Tome', 'Africa/Sao_Tome');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Timbuktu', 'Africa/Timbuktu');
		$timeZoneC = Arr::add($timeZoneC, 'America/Danmarkshavn', 'America/Danmarkshavn');
		$timeZoneC = Arr::add($timeZoneC, 'Atlantic/Canary', 'Atlantic/Canary');
		$timeZoneC = Arr::add($timeZoneC, 'Atlantic/Faeroe', 'Atlantic/Faeroe');
		$timeZoneC = Arr::add($timeZoneC, 'Atlantic/Madeira', 'Atlantic/Madeira');
		$timeZoneC = Arr::add($timeZoneC, 'Atlantic/Reykjavik', 'Atlantic/Reykjavik');
		$timeZoneC = Arr::add($timeZoneC, 'Atlantic/St_Helena', 'Atlantic/St_Helena');
		$timeZoneC = Arr::add($timeZoneC, 'Eire', 'Eire');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT', 'Etc/GMT');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT+0', 'Etc/GMT+0');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-0', 'Etc/GMT-0');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT0', 'Etc/GMT0');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/Greenwich', 'Etc/Greenwich');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/UCT', 'Etc/UCT');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/UTC', 'Etc/UTC');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/Universal', 'Etc/Universal');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/Zulu', 'Etc/Zulu');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Belfast', 'Europe/Belfast');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Dublin', 'Europe/Dublin');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Lisbon', 'Europe/Lisbon');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/London', 'Europe/London');
		$timeZoneC = Arr::add($timeZoneC, 'GB', 'GB');
		$timeZoneC = Arr::add($timeZoneC, 'GB-Eire', 'GB-Eire');
		$timeZoneC = Arr::add($timeZoneC, 'GMT', 'GMT');
		$timeZoneC = Arr::add($timeZoneC, 'GMT0', 'GMT0');
		$timeZoneC = Arr::add($timeZoneC, 'Greenwich', 'Greenwich');
		$timeZoneC = Arr::add($timeZoneC, 'Iceland', 'Iceland');
		$timeZoneC = Arr::add($timeZoneC, 'Portugal', 'Portugal');
		$timeZoneC = Arr::add($timeZoneC, 'UCT', 'UCT');
		$timeZoneC = Arr::add($timeZoneC, 'UTC', 'UTC');
		$timeZoneC = Arr::add($timeZoneC, 'Universal', 'Universal');
		$timeZoneC = Arr::add($timeZoneC, 'WET', 'WET');
		$timeZoneC = Arr::add($timeZoneC, 'Zulu', 'Zulu');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Algiers', 'Africa/Algiers');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Bangui', 'Africa/Bangui');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Brazzaville', 'Africa/Brazzaville');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Ceuta', 'Africa/Ceuta');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Douala', 'Africa/Douala');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Kinshasa', 'Africa/Kinshasa');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Lagos', 'Africa/Lagos');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Libreville', 'Africa/Libreville');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Luanda', 'Africa/Luanda');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Malabo', 'Africa/Malabo');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Ndjamena', 'Africa/Ndjamena');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Niamey', 'Africa/Niamey');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Porto-Novo', 'Africa/Porto-Novo');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Tunis', 'Africa/Tunis');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Windhoek', 'Africa/Windhoek');
		$timeZoneC = Arr::add($timeZoneC, 'Arctic/Longyearbyen', 'Arctic/Longyearbyen');
		$timeZoneC = Arr::add($timeZoneC, 'Atlantic/Jan_Mayen', 'Atlantic/Jan_Mayen');
		$timeZoneC = Arr::add($timeZoneC, 'CET', 'CET');
		$timeZoneC = Arr::add($timeZoneC, 'ECT', 'ECT');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-1', 'Etc/GMT-1');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Amsterdam', 'Europe/Amsterdam');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Andorra', 'Europe/Andorra');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Belgrade', 'Europe/Belgrade');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Berlin', 'Europe/Berlin');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Bratislava', 'Europe/Bratislava');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Brussels', 'Europe/Brussels');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Budapest', 'Europe/Budapest');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Copenhagen', 'Europe/Copenhagen');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Gibraltar', 'Europe/Gibraltar');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Ljubljana', 'Europe/Ljubljana');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Luxembourg', 'Europe/Luxembourg');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Madrid', 'Europe/Madrid');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Malta', 'Europe/Malta');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Monaco', 'Europe/Monaco');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Oslo', 'Europe/Oslo');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Paris', 'Europe/Paris');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Prague', 'Europe/Prague');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Rome', 'Europe/Rome');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/San_Marino', 'Europe/San_Marino');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Sarajevo', 'Europe/Sarajevo');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Skopje', 'Europe/Skopje');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Stockholm', 'Europe/Stockholm');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Tirane', 'Europe/Tirane');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Vaduz', 'Europe/Vaduz');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Vatican', 'Europe/Vatican');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Vienna', 'Europe/Vienna');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Warsaw', 'Europe/Warsaw');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Zagreb', 'Europe/Zagreb');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Zurich', 'Europe/Zurich');
		$timeZoneC = Arr::add($timeZoneC, 'MET', 'MET');
		$timeZoneC = Arr::add($timeZoneC, 'Poland', 'Poland');
		$timeZoneC = Arr::add($timeZoneC, 'ART', 'ART');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Blantyre', 'Africa/Blantyre');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Bujumbura', 'Africa/Bujumbura');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Cairo', 'Africa/Cairo');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Gaborone', 'Africa/Gaborone');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Harare', 'Africa/Harare');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Johannesburg', 'Africa/Johannesburg');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Kigali', 'Africa/Kigali');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Lubumbashi', 'Africa/Lubumbashi');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Lusaka', 'Africa/Lusaka');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Maputo', 'Africa/Maputo');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Maseru', 'Africa/Maseru');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Mbabane', 'Africa/Mbabane');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Tripoli', 'Africa/Tripoli');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Amman', 'Asia/Amman');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Beirut', 'Asia/Beirut');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Damascus', 'Asia/Damascus');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Gaza', 'Asia/Gaza');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Istanbul', 'Asia/Istanbul');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Jerusalem', 'Asia/Jerusalem');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Nicosia', 'Asia/Nicosia');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Tel_Aviv', 'Asia/Tel_Aviv');
		$timeZoneC = Arr::add($timeZoneC, 'CAT', 'CAT');
		$timeZoneC = Arr::add($timeZoneC, 'EET', 'EET');
		$timeZoneC = Arr::add($timeZoneC, 'Egypt', 'Egypt');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-2', 'Etc/GMT-2');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Athens', 'Europe/Athens');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Bucharest', 'Europe/Bucharest');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Chisinau', 'Europe/Chisinau');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Helsinki', 'Europe/Helsinki');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Istanbul', 'Europe/Istanbul');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Kaliningrad', 'Europe/Kaliningrad');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Kiev', 'Europe/Kiev');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Minsk', 'Europe/Minsk');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Nicosia', 'Europe/Nicosia');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Riga', 'Europe/Riga');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Simferopol', 'Europe/Simferopol');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Sofia', 'Europe/Sofia');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Tallinn', 'Europe/Tallinn');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Tiraspol', 'Europe/Tiraspol');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Uzhgorod', 'Europe/Uzhgorod');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Vilnius', 'Europe/Vilnius');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Zaporozhye', 'Europe/Zaporozhye');
		$timeZoneC = Arr::add($timeZoneC, 'Israel', 'Israel');
		$timeZoneC = Arr::add($timeZoneC, 'Libya', 'Libya');
		$timeZoneC = Arr::add($timeZoneC, 'Turkey', 'Turkey');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Addis_Ababa', 'Africa/Addis_Ababa');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Asmera', 'Africa/Asmera');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Dar_es_Salaam', 'Africa/Dar_es_Salaam');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Djibouti', 'Africa/Djibouti');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Kampala', 'Africa/Kampala');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Khartoum', 'Africa/Khartoum');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Mogadishu', 'Africa/Mogadishu');
		$timeZoneC = Arr::add($timeZoneC, 'Africa/Nairobi', 'Africa/Nairobi');
		$timeZoneC = Arr::add($timeZoneC, 'Antarctica/Syowa', 'Antarctica/Syowa');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Aden', 'Asia/Aden');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Baghdad', 'Asia/Baghdad');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Bahrain', 'Asia/Bahrain');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Kuwait', 'Asia/Kuwait');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Qatar', 'Asia/Qatar');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Riyadh', 'Asia/Riyadh');
		$timeZoneC = Arr::add($timeZoneC, 'EAT', 'EAT');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-3', 'Etc/GMT-3');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Moscow', 'Europe/Moscow');
		$timeZoneC = Arr::add($timeZoneC, 'Indian/Antananarivo', 'Indian/Antananarivo');
		$timeZoneC = Arr::add($timeZoneC, 'Indian/Comoro', 'Indian/Comoro');
		$timeZoneC = Arr::add($timeZoneC, 'Indian/Mayotte', 'Indian/Mayotte');
		$timeZoneC = Arr::add($timeZoneC, 'W-SU', 'W-SU');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Riyadh87', 'Asia/Riyadh87');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Riyadh88', 'Asia/Riyadh88');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Riyadh89', 'Asia/Riyadh89');
		$timeZoneC = Arr::add($timeZoneC, 'Mideast/Riyadh87', 'Mideast/Riyadh87');
		$timeZoneC = Arr::add($timeZoneC, 'Mideast/Riyadh88', 'Mideast/Riyadh88');
		$timeZoneC = Arr::add($timeZoneC, 'Mideast/Riyadh89', 'Mideast/Riyadh89');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Tehran', 'Asia/Tehran');
		$timeZoneC = Arr::add($timeZoneC, 'Iran', 'Iran');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Aqtau', 'Asia/Aqtau');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Baku', 'Asia/Baku');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Dubai', 'Asia/Dubai');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Muscat', 'Asia/Muscat');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Oral', 'Asia/Oral');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Tbilisi', 'Asia/Tbilisi');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Yerevan', 'Asia/Yerevan');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-4', 'Etc/GMT-4');
		$timeZoneC = Arr::add($timeZoneC, 'Europe/Samara', 'Europe/Samara');
		$timeZoneC = Arr::add($timeZoneC, 'Indian/Mahe', 'Indian/Mahe');
		$timeZoneC = Arr::add($timeZoneC, 'Indian/Mauritius', 'Indian/Mauritius');
		$timeZoneC = Arr::add($timeZoneC, 'Indian/Reunion', 'Indian/Reunion');
		$timeZoneC = Arr::add($timeZoneC, 'NET', 'NET');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Kabul', 'Asia/Kabul');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Aqtobe', 'Asia/Aqtobe');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Ashgabat', 'Asia/Ashgabat');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Ashkhabad', 'Asia/Ashkhabad');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Bishkek', 'Asia/Bishkek');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Dushanbe', 'Asia/Dushanbe');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Karachi', 'Asia/Karachi');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Samarkand', 'Asia/Samarkand');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Tashkent', 'Asia/Tashkent');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Yekaterinburg', 'Asia/Yekaterinburg');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-5', 'Etc/GMT-5');
		$timeZoneC = Arr::add($timeZoneC, 'Indian/Kerguelen', 'Indian/Kerguelen');
		$timeZoneC = Arr::add($timeZoneC, 'Indian/Maldives', 'Indian/Maldives');
		$timeZoneC = Arr::add($timeZoneC, 'PLT', 'PLT');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Calcutta', 'Asia/Calcutta');
		$timeZoneC = Arr::add($timeZoneC, 'IST', 'IST');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Katmandu', 'Asia/Katmandu');
		$timeZoneC = Arr::add($timeZoneC, 'Antarctica/Mawson', 'Antarctica/Mawson');
		$timeZoneC = Arr::add($timeZoneC, 'Antarctica/Vostok', 'Antarctica/Vostok');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Almaty', 'Asia/Almaty');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Colombo', 'Asia/Colombo');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Dacca', 'Asia/Dacca');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Dhaka', 'Asia/Dhaka');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Novosibirsk', 'Asia/Novosibirsk');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Omsk', 'Asia/Omsk');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Qyzylorda', 'Asia/Qyzylorda');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Thimbu', 'Asia/Thimbu');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Thimphu', 'Asia/Thimphu');
		$timeZoneC = Arr::add($timeZoneC, 'BST', 'BST');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-6', 'Etc/GMT-6');
		$timeZoneC = Arr::add($timeZoneC, 'Indian/Chagos', 'Indian/Chagos');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Rangoon', 'Asia/Rangoon');
		$timeZoneC = Arr::add($timeZoneC, 'Indian/Cocos', 'Indian/Cocos');
		$timeZoneC = Arr::add($timeZoneC, 'Antarctica/Davis', 'Antarctica/Davis');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Bangkok', 'Asia/Bangkok');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Hovd', 'Asia/Hovd');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Jakarta', 'Asia/Jakarta');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Krasnoyarsk', 'Asia/Krasnoyarsk');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Phnom_Penh', 'Asia/Phnom_Penh');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Pontianak', 'Asia/Pontianak');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Saigon', 'Asia/Saigon');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Vientiane', 'Asia/Vientiane');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-7', 'Etc/GMT-7');
		$timeZoneC = Arr::add($timeZoneC, 'Indian/Christmas', 'Indian/Christmas');
		$timeZoneC = Arr::add($timeZoneC, 'VST', 'VST');
		$timeZoneC = Arr::add($timeZoneC, 'Antarctica/Casey', 'Antarctica/Casey');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Brunei', 'Asia/Brunei');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Chongqing', 'Asia/Chongqing');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Chungking', 'Asia/Chungking');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Harbin', 'Asia/Harbin');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Hong_Kong', 'Asia/Hong_Kong');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Irkutsk', 'Asia/Irkutsk');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Kashgar', 'Asia/Kashgar');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Kuala_Lumpur', 'Asia/Kuala_Lumpur');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Kuching', 'Asia/Kuching');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Macao', 'Asia/Macao');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Macau', 'Asia/Macau');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Makassar', 'Asia/Makassar');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Manila', 'Asia/Manila');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Shanghai', 'Asia/Shanghai');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Singapore', 'Asia/Singapore');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Taipei', 'Asia/Taipei');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Ujung_Pandang', 'Asia/Ujung_Pandang');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Ulaanbaatar', 'Asia/Ulaanbaatar');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Ulan_Bator', 'Asia/Ulan_Bator');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Urumqi', 'Asia/Urumqi');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Perth', 'Australia/Perth');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/West', 'Australia/West');
		$timeZoneC = Arr::add($timeZoneC, 'CTT', 'CTT');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-8', 'Etc/GMT-8');
		$timeZoneC = Arr::add($timeZoneC, 'Hongkong', 'Hongkong');
		$timeZoneC = Arr::add($timeZoneC, 'PRC', 'PRC');
		$timeZoneC = Arr::add($timeZoneC, 'Singapore', 'Singapore');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Choibalsan', 'Asia/Choibalsan');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Dili', 'Asia/Dili');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Jayapura', 'Asia/Jayapura');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Pyongyang', 'Asia/Pyongyang');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Seoul', 'Asia/Seoul');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Tokyo', 'Asia/Tokyo');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Yakutsk', 'Asia/Yakutsk');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-9', 'Etc/GMT-9');
		$timeZoneC = Arr::add($timeZoneC, 'JST', 'JST');
		$timeZoneC = Arr::add($timeZoneC, 'Japan', 'Japan');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Palau', 'Pacific/Palau');
		$timeZoneC = Arr::add($timeZoneC, 'ROK', 'ROK');
		$timeZoneC = Arr::add($timeZoneC, 'ACT', 'ACT');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Adelaide', 'Australia/Adelaide');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Broken_Hill', 'Australia/Broken_Hill');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Darwin', 'Australia/Darwin');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/North', 'Australia/North');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/South', 'Australia/South');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Yancowinna', 'Australia/Yancowinna');
		$timeZoneC = Arr::add($timeZoneC, 'AET', 'AET');
		$timeZoneC = Arr::add($timeZoneC, 'Antarctica/DumontDUrville', 'Antarctica/DumontDUrville');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Sakhalin', 'Asia/Sakhalin');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Vladivostok', 'Asia/Vladivostok');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/ACT', 'Australia/ACT');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Brisbane', 'Australia/Brisbane');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Canberra', 'Australia/Canberra');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Hobart', 'Australia/Hobart');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Lindeman', 'Australia/Lindeman');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Melbourne', 'Australia/Melbourne');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/NSW', 'Australia/NSW');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Queensland', 'Australia/Queensland');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Sydney', 'Australia/Sydney');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Tasmania', 'Australia/Tasmania');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Victoria', 'Australia/Victoria');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-10', 'Etc/GMT-10');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Guam', 'Pacific/Guam');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Port_Moresby', 'Pacific/Port_Moresby');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Saipan', 'Pacific/Saipan');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Truk', 'Pacific/Truk');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Yap', 'Pacific/Yap');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/LHI', 'Australia/LHI');
		$timeZoneC = Arr::add($timeZoneC, 'Australia/Lord_Howe', 'Australia/Lord_Howe');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Magadan', 'Asia/Magadan');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-11', 'Etc/GMT-11');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Efate', 'Pacific/Efate');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Guadalcanal', 'Pacific/Guadalcanal');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Kosrae', 'Pacific/Kosrae');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Noumea', 'Pacific/Noumea');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Ponape', 'Pacific/Ponape');
		$timeZoneC = Arr::add($timeZoneC, 'SST', 'SST');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Norfolk', 'Pacific/Norfolk');
		$timeZoneC = Arr::add($timeZoneC, 'Antarctica/McMurdo', 'Antarctica/McMurdo');
		$timeZoneC = Arr::add($timeZoneC, 'Antarctica/South_Pole', 'Antarctica/South_Pole');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Anadyr', 'Asia/Anadyr');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Kamchatka', 'Asia/Kamchatka');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-12', 'Etc/GMT-12');
		$timeZoneC = Arr::add($timeZoneC, 'Kwajalein', 'Kwajalein');
		$timeZoneC = Arr::add($timeZoneC, 'NST', 'NST');
		$timeZoneC = Arr::add($timeZoneC, 'NZ', 'NZ');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Auckland', 'Pacific/Auckland');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Fiji', 'Pacific/Fiji');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Funafuti', 'Pacific/Funafuti');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Kwajalein', 'Pacific/Kwajalein');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Majuro', 'Pacific/Majuro');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Nauru', 'Pacific/Nauru');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Tarawa', 'Pacific/Tarawa');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Wake', 'Pacific/Wake');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Wallis', 'Pacific/Wallis');
		$timeZoneC = Arr::add($timeZoneC, 'NZ-CHAT', 'NZ-CHAT');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Chatham', 'Pacific/Chatham');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-13', 'Etc/GMT-13');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Enderbury', 'Pacific/Enderbury');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Tongatapu', 'Pacific/Tongatapu');
		$timeZoneC = Arr::add($timeZoneC, 'Etc/GMT-14', 'Etc/GMT-14');
		$timeZoneC = Arr::add($timeZoneC, 'Pacific/Kiritimati', 'Pacific/Kiritimati');
		$timeZoneC = Arr::add($timeZoneC, 'Asia/Kolkata', 'Asia/Kolkata');



		return $timeZoneC;
	}

	public function fransearch()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$fransId1 = array('' => '-- SELECT --');
		$prePaidFransId = array(''  => '-- SELECT --');
		$fransId = $redis->smembers('S_Franchises');
		foreach ($fransId as $fran) {
			$franDetails_json = $redis->hget('H_Franchise', $fran);
			$franchiseDetails = json_decode($franDetails_json, true);
			$prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
			if ($prepaid == 'yes') {
				$prePaidFransId = Arr::add($prePaidFransId, $fran, $fran);
			} else {
				$fransId1 = Arr::add($fransId1, $fran, $fran);
			}
		}

		//$fransId = $orgArr;

		// $apnKey = $redis->exists('Update:Apn');
		// $timezoneKey = $redis->exists('Update:Timezone');
		// $count = VdmFranchiseController::liveVehicleCountV2();
		$fransId = $fransId1;
		array_multisort($fransId1);
		array_multisort($prePaidFransId);
		return [
			'fransId' => $fransId1,
			'prePaidFransId' => $prePaidFransId,
		];
		// return view('vdm.franchise.frans')->with('fransId', $fransId1)->with('prePaidFransId', $prePaidFransId)->with('apnKey', $apnKey)->with('timezoneKey', $timezoneKey)->with('count', $count);
	}



	public function users()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$userId = $redis->smembers('S_Franchises');
		$orgArr = array();
		foreach ($userId as $org) {
			$temp = $redis->smembers('S_Users_' . $org);
			foreach ($temp as $key) {
				$orgArr = Arr::add($orgArr, $key, $key);
			}
		}
		$userId = $orgArr;
		$usersData = [
			'userId' => $userId,
		];
		return view('vdm.franchise.users', $usersData);
	}


	public function buyAddress()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$addressCount = $redis->get('VAMOSADDRESS_CONTROL');
		if ($addressCount == null) {
			$addressCount = 0;
		}
		$buyAddressData = [
			'addressCount' => $addressCount,
		];
		return view('vdm.franchise.buyAddress', $buyAddressData);
	}


	public function updateAddCount()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$redis = Redis::connection();
		$redis->set('VAMOSADDRESS_CONTROL', request()->get('addressCount'));
		return redirect()->to('vdmFranchises');
	}

	public function findFransList()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$redis = Redis::connection();
		$username = request()->get('frans');
		if ($username == null) {
			$username = request()->get('prePaidFrans');
		}
		$franDetails_json = $redis->hget('H_Franchise', $username);
		$franchiseDetails = json_decode($franDetails_json, true);

		$username = isset($franchiseDetails['userId']) ? $franchiseDetails['userId'] : null;

		if ($username == null) {
			$username = session()->get('page');
		} else {
			session()->put('page', $username);
		}

		try {
			// $user = FirebaseController::signinWithUserId($username);
			$user = User::whereRaw('BINARY username = ?', [$username])->firstOrFail();
			Auth::login($user);
		} catch (\Exception $e) {
			logger()->error($e->getMessage());
			return redirect()->to('vdmFranchises')->withErrors($e->getMessage());
		}
		session()->put('frnSwt', 'fransswitch');
		if (session()->get('frnSwt') == 'fransswitch') {
			logger('Inside the vamos admin session');
		}

		return redirect()->to('Business');
	}


	public function findUsersList()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$redis = Redis::connection();
		$username = request()->get('users');

		if ($username == null) {
			$username = session()->get('page');
		} else {
			session()->put('page', $username);
		}

		try {
			// $user = FirebaseController::signinWithUserId($username);
			$user = User::whereRaw('BINARY username = ?', [$username])->firstOrFail();
			Auth::login($user);
		} catch (\Exception $e) {
			return redirect()->to('vdmFranchises/users');
		}
		return redirect()->to('Business');
	}
	/**

		Frabchise is created by VAMOS Admin
		Franchise name
		Franchise ID (company ID)
		Franchise description
		Franchise full address
		Franchise landline no
		Franchise mobile number 1
		Franchise mobile number 2
		Franchise email id1
		Franchise email id2
		Franchise other details
		Franchise login details
		
	 * 
	 * @return Response
	 */
	public function store()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();

		$rules = array(
			'fname' => 'required',
			'fcode' => 'required',
			'description' => 'required',
			'fullAddress' => 'required',
			'landline' => 'required',
			'mobileNo1' => 'required',
			'email1' => 'required|email',
			'email2' => 'required|email',
			'userId' => 'required',
			'website' => 'required',
			'trackPage' => 'required'
		);
		$validator = validator()->make(request()->all(), $rules);
		$userId = request()->get('userId');
		$fcode = request()->get('fcode');
		$dbIpindex = request()->get('ipadd');
		$val = $redis->sismember('S_Franchises', $fcode);
		$val1 = $redis->sismember('S_Users_' . $fcode, $userId);


		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator)->withInput();
		} else if ($val == 1) {
			session()->flash('message', $fcode . 'Franchise already exist ' . '!');
			return redirect()->back()->withInput();
		} else if ($val1 == 1) {
			session()->flash('message', $userId . ' already exist. Please use different id ' . '!');
			return redirect()->back()->withInput();
		} else {
			// store
			$dbIpAr = VdmFranchiseController::dbIp();
			$dbIp = $dbIpAr[$dbIpindex];
			$fname = request()->get('fname');
			$fcode1 = request()->get('fcode');
			$description = request()->get('description');
			$fullAddress = request()->get('fullAddress');
			$landline = request()->get('landline');
			$mobileNo1 = request()->get('mobileNo1');
			$mobileNo2 = request()->get('mobileNo2');
			$website = request()->get('website');
			$website2 = request()->get('website2');
			$website3 = request()->get('website3');
			$website4 = request()->get('website4');
			$trackPage = request()->get('trackPage');
			$email1 = request()->get('email1');
			$email2 = request()->get('email2');
			$userId = request()->get('userId');
			$otherDetails = request()->get('otherDetails');
			$smsSender = request()->get('smsSender');
			$smsProvider = request()->get('smsProvider');
			$providerUserName = request()->get('providerUserName');
			$providerPassword = request()->get('providerPassword');
			$backUpDays = request()->get('backUpDays');
			//$eFDSchedular=request()->get ('eFDSchedular');
			$timeZone = request()->get('timeZone');
			$apiKey = request()->get('apiKey');
			$apiKey2 = request()->get('apiKey2');
			$apiKey3 = request()->get('apiKey3');
			$apiKey4 = request()->get('apiKey4');
			$mapKey = request()->get('mapKey');
			$addressKey = request()->get('addressKey');
			$notificationKey = request()->get('notificationKey');
			$dbType = request()->get('dbType');
			$zoho = request()->get('zoho');
			$auth = request()->get('auth');
			$gpsvtsAppKey = request()->get('gpsvtsAppKey');
			$subDomain = request()->get('subDomain');
			$userId1 = $userId;
			$fcode = preg_replace('/\s/', '', $fcode1);
			if (preg_match('/[^A-Za-z]+/', $fcode)) {
				logger($fcode . ' <<<--fcode------------>>> ' . preg_match('/[^A-Za-z]+/', $fcode));
				return redirect()->back()->withInput()->withErrors('Numeric value not allowed in fcode');
			}
			// $refDataArr = array('regNo'=>$regNo,'vehicleMake'=>$vehicleMake,'vehicleType'=>$vehicleType,'oprName'=>$oprName,
			// 'mobileNo'=>$mobileNo,'vehicleCap'=>$vehicleCap,'deviceModel'=>$deviceModel);


			$redis->sadd('S_Franchises', $fcode);
			if (strpos($userId1, 'admin') !== false || strpos($userId1, 'Admin') !== false || strpos($userId1, 'ADMIN') !== false) {
				$userId = $userId1;
			} else {
				$userId = preg_replace('/' . $userId1 . '/', $userId1 . 'admin', $userId1);
			}
			if (($website == $website2 && ($website != null && $website2 != null)) || ($website == $website3 && ($website != null && $website3 != null)) || ($website == $website4 && ($website != null && $website4 != null)) || ($website2 == $website3 && ($website2 != null && $website3 != null)) || ($website2 == $website4 && ($website2 != null && $website4 != null)) || ($website3 == $website4 && ($website3 != null && $website4 != null))) {
				return redirect()->back()->withInput()->withErrors('Website names should not be same');
			}
			if (($apiKey == $apiKey2 && ($apiKey != null && $apiKey2 != null)) || ($apiKey == $apiKey3 && ($apiKey != null && $apiKey3 != null)) || ($apiKey == $apiKey4 && ($apiKey != null && $apiKey4 != null)) || ($apiKey2 == $apiKey3 && ($apiKey2 != null && $apiKey3 != null)) || ($apiKey2 == $apiKey4 && ($apiKey2 != null && $apiKey4 != null)) || ($apiKey3 == $apiKey4 && ($apiKey3 != null && $apiKey4 != null))) {
				return redirect()->back()->withInput()->withErrors('Map / API keys should not be same');
			}
			$pattern = "/[^0-9\,]/";
			if ((preg_match($pattern, $mobileNo1)) || (preg_match($pattern, $mobileNo2))) {
				return redirect()->to('vdmFranchises/' . $fcode . '/edit')->withInput()->withErrors('Only Numeric values are allowed in Mobile Numbers');
			}
			$prepaid = request()->get('prepaid');
			if ($prepaid == 'yes') {
				$numberofStarterLicence = request()->get('numberofStarterLicence', '0');
				$numberofBasicLicence = request()->get('numberofBasicLicence', '0');
				$numberofAdvanceLicence = request()->get('numberofAdvanceLicence', '0');
				$numberofPremiumLicence = request()->get('numberofPremiumLicence', '0');
				$numberofPremPlusLicence = request()->get('numberofPremPlusLicence', '0');

				$availableStarterLicence		= $numberofStarterLicence;
				$availableBasicLicence		= $numberofBasicLicence;
				$availableAdvanceLicence	= $numberofAdvanceLicence;
				$availablePremiumLicence	= $numberofPremiumLicence;
				$availablePremPlusLicence   = $numberofPremPlusLicence;

				$licDetails = [
					'numberofStarterLicence'	=> $numberofStarterLicence,
					'numberofBasicLicence'	=> $numberofBasicLicence,
					'numberofAdvanceLicence' => $numberofAdvanceLicence,
					'numberofPremiumLicence' => $numberofPremiumLicence,
					'numberofPremPlusLicence' => $numberofPremPlusLicence,
					'availableBasicLicence'	=> $availableBasicLicence,
					'availableAdvanceLicence' => $availableAdvanceLicence,
					'availablePremiumLicence'	=> $availablePremiumLicence,
					'availablePremPlusLicence'  => $availablePremPlusLicence,
				];
			} else {
				$numberofLicence = request()->get('numberofLicence');
				$licDetails = [
					'numberofLicence' => $numberofLicence,
					'availableLincence' => $numberofLicence,
				];
			}
			$details = array(
				'fname' => $fname,
				'description' => $description,
				'landline' => $landline,
				'mobileNo1' => $mobileNo1,
				'mobileNo2' => $mobileNo2,
				'prepaid'	=> $prepaid,
				'email1' => $email1,
				'email2' => $email2,
				'userId' => $userId,
				'fullAddress' => $fullAddress,
				'otherDetails' => $otherDetails,
				'website' => $website,
				'website2' => $website2,
				'website3' => $website3,
				'website4' => $website4,
				'trackPage' => $trackPage,
				'smsSender' => $smsSender,
				'smsProvider' => $smsProvider,
				'providerUserName' => $providerUserName,
				'providerPassword' => $providerPassword,
				'timeZone' => $timeZone,
				'apiKey' => $apiKey,
				'apiKey2' => $apiKey2,
				'apiKey3' => $apiKey3,
				'apiKey4' => $apiKey4,
				'mapKey' => $mapKey,
				'addressKey' => $addressKey,
				'notificationKey' => $notificationKey,
				'gpsvtsApp' => $gpsvtsAppKey,
				'backUpDays' => $backUpDays,
				'dbType' => $dbType,
				'zoho' => $zoho,
				'auth' => $auth,
				'subDomain' => $subDomain,
				'languageCode' => 'en',
			);
			$details = array_merge($details, $licDetails);


			/*$redis->hmset ( 'H_Franchise', $fcode.':fname',$fname,$fcode.':description',$description,
					$fcode.':landline',$landline,$fcode.':mobileNo1',$mobileNo1,$fcode.':mobileNo2',$mobileNo2,
					$fcode.':email1',$email1,$fcode.':email2',$email2,$fcode.':userId',$userId);*/ //ram what to do migration
			$geolocation = VdmFranchiseController::geocode($fullAddress);
			if ($geolocation != null) {
				$LatLong = implode(":", $geolocation);
			} else {
				$LatLong = '0:0';
			}
			$redis->hset('H_Franchise_LatLong', $fcode, $LatLong);

			$redis->hmset('H_Franchise_Mysql_DatabaseIP', $fcode, $dbIp);
			$detailsJson = json_encode($details);
			$redis->hmset('H_Franchise', $fcode, $detailsJson);

			try {
				$mysqlDetails = array();
				$status = array();
				$status = array(
					'userIpAddress' => session()->get('userIP'),
					'userName'=> $userId,
					'type'=>config()->get('constant.admin'),
					'name'=>$userId,
					'status'=>config()->get('constant.created'),
					'oldData'=>"",
					'newData'=>$detailsJson,
				);
				AuditNewController::updateAudit($status);
			} catch (Exception $e) {
				logger('Error inside Audit_Frans Create' . $e->getMessage());
			}
			$password = 'awesome';

			try {
				$userProperties = [
					'email' => $userId . '@vamosys.com',
					'emailVerified' => false,
					'password' => $password,
					'displayName' => $userId,
					'disabled' => false,
					'uid' => $userId,
				];
				// $auth = app('firebase.auth');
				// $let = FirebaseController::createUser($userProperties);
				// FirebaseException
			} catch (FirebaseException $e) {
				logger('FirebaseException --->' . $e->getMessage());
				return redirect()->back()->withInput()->withErrors($e->getMessage());
			}


			$redis->hmset('H_Fcode_Timezone_Schedular', $fcode, $timeZone);
			$redis->sadd('S_Users_' . $fcode, $userId);
			$redis->hmset('H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo1, $userId . ':email', $email1, $userId . ':password', $password, $userId . ':zoho', $zoho, $userId . 'auth', $auth, $userId . ':OWN', 'admin');
			$defaultReports = $redis->smembers('S_Default_ReportList');
			foreach ($defaultReports as $key => $value) {
				$redis->sadd('S_Users_Reports_Admin_' . $fcode, $value);
			}
			$user = new User;
			$user->name = $fname;
			$user->username = $userId;
			$user->email = $email1;
			$user->mobileNo = $mobileNo1;
			//$user->zoho=$zoho;
			$user->password = Hash::make($password);
			$user->save();


			/** 
			 * Add vamos admin user for each franchise
			 * 
			 */
			$user = new User;
			$vamosid = 'vamos' . $fcode;
			$user->name = 'vamos' . $fname;
			$user->mobileNo = '1234567890';
			$user->username = $vamosid;
			$user->email = 'support@vamosys.com';
			$user->password = Hash::make($password);
			$user->save();

			$redis->sadd('S_Users_' . $fcode, $vamosid);
			$redis->hmset('H_UserId_Cust_Map', $vamosid . ':fcode', $fcode);
			$redis->hset('H_Google_Android_ServerKey', $fcode, $notificationKey);
			$redis->hset('H_MobileAppId', $fcode, $gpsvtsAppKey);
			$servername = $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
			if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
				return 'Ipaddress Failed !!!';
			}
			$usernamedb  =  "root";
			$password    =  "#vamo123";
			$dbname      =  $fcode;
			$conn  =  mysqli_connect($servername, $usernamedb, $password);
			$create_db = "CREATE DATABASE IF NOT EXISTS $dbname";
			if ($conn->query($create_db)) {
				$conn  =  mysqli_connect($servername, $usernamedb, $password, $dbname);
				if (!$conn) {
					die('Could not connect:' . mysqli_connect_error());
					return 'Please Update One more time Connection failed';
				} else {
					$create = "CREATE TABLE IF NOT EXISTS BatchMove (id INT  NOT NULL PRIMARY KEY AUTO_INCREMENT,Device_Id VARCHAR(50),Dealer_Name varchar(50),Vehicle_Id varchar(50),Date_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";
					$renewal = "CREATE TABLE IF NOT EXISTS Renewal_Details(ID INT AUTO_INCREMENT PRIMARY KEY,User_Name varchar(50),Licence_Id varchar(50),Vehicle_Id varchar(50),Device_Id varchar(50),Type ENUM('Basic', 'Advance', 'Premium', 'PremiumPlus') NOT NULL,Processed_Date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,Status ENUM('Migration', 'Rename', 'OnBoard','Cancel','Renew') NOT NULL)";
					$result = $conn->query($renewal);
					// log::onfo('Renewal_Details table was created in '.$fcode.'Database'.$result);
					if ($conn->query($create)) {
						$ins = "INSERT INTO BatchMove (Device_Id,Vehicle_Id) values ('DEVICE_POWER','VEHICLE_POWER')";
						$conn->multi_query($ins);
					} else {
						logger('Database not connected...');
					}
				}
			}

			/*				
			Mail::queue('emails.welcome', array('fname'=>$fname,'userId'=>$userId,'password'=>$password), function($message)
			{
				logger("Inside email :" . request()->get ( 'email1' ));
				
				$message->to(request()->get ( 'email1' ))->subject('Welcome to VAMO Systems');
			});
			*/

			// redirect
			session()->flash('message', 'Successfully created ' . $fname . '!');
			return redirect()->to('vdmFranchises');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function show($id)
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();

		$fcode = $id;
		$redis = Redis::connection();


		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		$prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
		if (isset($franchiseDetails['description']) == 1)
			$description = $franchiseDetails['description'];
		else
			$description = '';
		if (isset($franchiseDetails['landline']) == 1)
			$landline = $franchiseDetails['landline'];
		else
			$landline = '';
		if (isset($franchiseDetails['mobileNo1']) == 1)
			$mobileNo1 = $franchiseDetails['mobileNo1'];
		else
			$mobileNo1 = '';
		// zoho ram
		if (isset($franchiseDetails['zoho']) == 1)
			$zoho = $franchiseDetails['zoho'];
		else
			$zoho = '-';
		if (isset($franchiseDetails['auth']) == 1)
			$auth = $franchiseDetails['auth'];
		else
			$auth = '-';
		//
		if (isset($franchiseDetails['mobileNo2']) == 1)
			$mobileNo2 = $franchiseDetails['mobileNo2'];
		else
			$mobileNo2 = '';

		if (isset($franchiseDetails['email1']) == 1)
			$email1 = $franchiseDetails['email1'];
		else
			$email1 = '';
		if (isset($franchiseDetails['email2']) == 1)
			$email2 = $franchiseDetails['email2'];
		else
			$email2 = '';
		if (isset($franchiseDetails['userId']) == 1)
			$userId = $franchiseDetails['userId'];
		else
			$userId = '';
		if (isset($franchiseDetails['fullAddress']) == 1)
			$fullAddress = $franchiseDetails['fullAddress'];
		else
			$fullAddress = '';
		if (isset($franchiseDetails['otherDetails']) == 1)
			$otherDetails = $franchiseDetails['otherDetails'];
		else
			$otherDetails = '';
		if (isset($franchiseDetails['website']) == 1)
			$website = $franchiseDetails['website'];
		else
			$website = '';
		if (isset($franchiseDetails['website2']) == 1)
			$website2 = $franchiseDetails['website2'];
		else
			$website2 = '';
		if (isset($franchiseDetails['website3']) == 1)
			$website3 = $franchiseDetails['website3'];
		else
			$website3 = '';
		if (isset($franchiseDetails['website4']) == 1)
			$website4 = $franchiseDetails['website4'];
		else
			$website4 = '';
		if (isset($franchiseDetails['trackPage']) == 1)
			$trackPage = $franchiseDetails['trackPage'];
		else
			$trackPage = 'Normal View';
		if (isset($franchiseDetails['smsSender']) == 1)
			$smsSender = $franchiseDetails['smsSender'];
		else
			$smsSender = '';
		if (isset($franchiseDetails['smsProvider']) == 1)
			$smsProvider = $franchiseDetails['smsProvider'];
		else
			$smsProvider = 'nill';
		if (isset($franchiseDetails['providerUserName']) == 1)
			$providerUserName = $franchiseDetails['providerUserName'];
		else
			$providerUserName = '';
		if (isset($franchiseDetails['providerPassword']) == 1)
			$providerPassword = $franchiseDetails['providerPassword'];
		else
			$providerPassword = '';
		// if(isset($franchiseDetails['eFDSchedular'])==1)
		// 	$eFDSchedular=$franchiseDetails['eFDSchedular'];
		// else
		// 	$eFDSchedular='';
		if (isset($franchiseDetails['timeZone']) == 1)
			$timeZone = $franchiseDetails['timeZone'];
		else
			$timeZone = 'Asia/Kolkata';
		if ($timeZone == '')
			$timeZone = 'Asia/Kolkata';
		if (isset($franchiseDetails['apiKey']) == 1)
			$apiKey = $franchiseDetails['apiKey'];
		else
			$apiKey = 'Nill';
		if (isset($franchiseDetails['apiKey2']) == 1)
			$apiKey2 = $franchiseDetails['apiKey2'];
		else
			$apiKey2 = 'Nill';
		if (isset($franchiseDetails['apiKey3']) == 1)
			$apiKey3 = $franchiseDetails['apiKey3'];
		else
			$apiKey3 = 'Nill';
		if (isset($franchiseDetails['apiKey4']) == 1)
			$apiKey4 = $franchiseDetails['apiKey4'];
		else
			$apiKey4 = 'Nill';
		if (isset($franchiseDetails['mapKey']) == 1)
			$mapKey = $franchiseDetails['mapKey'];
		else
			$mapKey = 'Nill';
		if (isset($franchiseDetails['addressKey']) == 1)
			$addressKey = $franchiseDetails['addressKey'];
		else
			$addressKey = 'Nill';
		if (isset($franchiseDetails['notificationKey']) == 1)
			$notificationKey = $franchiseDetails['notificationKey'];
		else
			$notificationKey = $redis->hget('H_Google_Android_ServerKey', $fcode);
		if (isset($franchiseDetails['backUpDays']) == 1)
			$backUpDays = $franchiseDetails['backUpDays'];
		else
			$backUpDays = '60';
		if (isset($franchiseDetails['dbType']) == 1)
			$dbType = $franchiseDetails['dbType'];
		else
			$dbType = 'mysql';
		if (isset($franchiseDetails['gpsvtsApp']) == 1)
			$gpsvtsAppKey = $franchiseDetails['gpsvtsApp'];
		else
			$gpsvtsAppKey = $redis->hget('H_MobileAppId', $fcode);
		$dbIp = $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
		if ($dbIp == '') {
			$dbIp = '188.166.244.126';
		}
		//$key = array_search($dbIp, VdmFranchiseController::dbIp());
		//$dbIp=$key;
		$numberofStarterLicence = 0;
		$availableStarterLicence = 0;

		if ($prepaid == 'yes') {
			$numberofStarterLicence = isset($franchiseDetails['numberofStarterLicence']) ? $franchiseDetails['numberofStarterLicence'] : '0';
			$availableStarterLicence = isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : '0';

			$numberofStarterPlusLicence = isset($franchiseDetails['numberofStarterPlusLicence']) ? $franchiseDetails['numberofStarterPlusLicence'] : '0';
			$availableStarterPlusLicence = isset($franchiseDetails['availableStarterPlusLicence']) ? $franchiseDetails['availableStarterPlusLicence'] : '0';


			$numberofBasicLicence = isset($franchiseDetails['numberofBasicLicence']) ? $franchiseDetails['numberofBasicLicence'] : '0';
			$availableBasicLicence		= isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';

			$numberofAdvanceLicence = isset($franchiseDetails['numberofAdvanceLicence']) ? $franchiseDetails['numberofAdvanceLicence'] : '0';
			$availableAdvanceLicence	= isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';

			$numberofPremiumLicence = isset($franchiseDetails['numberofPremiumLicence']) ? $franchiseDetails['numberofPremiumLicence'] : '0';
			$availablePremiumLicence	= isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';

			$numberofPremPlusLicence = isset($franchiseDetails['numberofPremPlusLicence']) ? $franchiseDetails['numberofPremPlusLicence'] : '0';
			$availablePremPlusLicence = isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0';

			$numberofLicence = 0;
			$availableLincence = 0;
		} else {
			$numberofLicence = isset($franchiseDetails['numberofLicence']) ? $franchiseDetails['numberofLicence'] : '0';
			$availableLincence = isset($franchiseDetails['availableLincence']) ? $franchiseDetails['availableLincence'] : '0';
			$numberofStarterLicence = 0;
			$numberofStarterPlusLicence = 0;
			$numberofBasicLicence = 0;
			$numberofAdvanceLicence = 0;
			$numberofPremiumLicence = 0;
			$numberofPremPlusLicence = 0;
			$availableStarterLicence = 0;
			$availableStarterPlusLicence = 0;
			$availableBasicLicence = 0;
			$availablePremiumLicence = 0;
			$availableAdvanceLicence = 0;
			$availablePremPlusLicence = 0;
		}

		$websites1 = array(
			'1' => $website,
			'2'	=> $website2,
			'3'	=> $website3,
			'4'	=> $website4
		);
		$websites = implode(",", $websites1);
		$apiKeys1 = array(
			'1' => $apiKey,
			'2'	=> $apiKey2,
			'3'	=> $apiKey3,
			'4'	=> $apiKey4
		);
		$apiKeys = implode(", ", $apiKeys1);

		$showData = [
			'fname' => $franchiseDetails['fname'],
			'fcode' => $fcode,
			'franchiseDetails' => $franchiseDetails,
			'description' => $description,
			'landline' => $landline,
			'mobileNo1' => $mobileNo1,
			'mobileNo2' => $mobileNo2,
			'email1' => $email1,
			'email2' => $email2,
			'zoho' => $zoho,
			'auth' => $auth,
			'userId' => $userId,
			'fullAddress' => $fullAddress,
			'otherDetails' => $otherDetails,
			'numberofStarterLicence' => $numberofStarterLicence,
			'availableStarterLicence' => $availableStarterLicence,
			'numberofStarterPlusLicence' => $numberofStarterPlusLicence,
			'availableStarterPlusLicence' => $availableStarterPlusLicence,
			'numberofBasicLicence' => $numberofBasicLicence,
			'numberofAdvanceLicence' => $numberofAdvanceLicence,
			'numberofPremiumLicence' => $numberofPremiumLicence,
			'numberofPremPlusLicence' => $numberofPremPlusLicence,
			'availableBasicLicence' => $availableBasicLicence,
			'availableAdvanceLicence' => $availableAdvanceLicence,
			'availablePremiumLicence' => $availablePremiumLicence,
			'availablePremPlusLicence' => $availablePremPlusLicence,
			'websites' => $websites,
			'trackPage' => $trackPage,
			'smsSender' => $smsSender,
			'smsProvider' => $smsProvider,
			'providerUserName' => $providerUserName,
			'providerPassword' => $providerPassword,
			'timeZone' => $timeZone,
			'apiKeys' => $apiKeys,
			'mapKey' => $mapKey,
			'addressKey' => $addressKey,
			'notificationKey' => $notificationKey,
			'gpsvtsAppKey' => $gpsvtsAppKey,
			'dbIp' => $dbIp,
			'dbType' => $dbType,
			'backUpDays' => $backUpDays,
			'dbIpAr' => VdmFranchiseController::dbIp(),
			'smsP' => VdmFranchiseController::smsP(),
			'timeZoneC' => VdmFranchiseController::timeZoneC(),
			'backType' => VdmFranchiseController::backTypeC(),
			'numberofLicence' => $numberofLicence,
			'availableLincence' => $availableLincence,
			'prepaid' => $prepaid
		];

		return view('vdm.franchise.show', $showData);
		// ->with('timeZoneC', VdmFranchiseController::timeZoneC())
		// ->with('backType', VdmFranchiseController::backTypeC())
		// ->with('numberofLicence', $numberofLicence)
		// ->with('availableLincence', $availableLincence)
		// ->with('prepaid', $prepaid);
		// ->with('apnKey', $apnKey)
		// ->with('timezoneKey', $timezoneKey)
		// ->with('count', $count);
	}

	public function prePaidCnv($fcode)
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		$fname = isset($franchiseDetails['fname']) ? $franchiseDetails['fname'] : '';
		$prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
		$ablLicence = isset($franchiseDetails['availableLincence']) ? $franchiseDetails['availableLincence'] : '';
		if ($prepaid == 'yes') {
			session()->flash('message', $fname . ' Franchise is already prepaid');
			return redirect()->to('vdmFranchises');
		}

		$details = array(
			'fname' => isset($franchiseDetails['fname']) ? $franchiseDetails['fname'] : '',
			'description' => isset($franchiseDetails['description']) ? $franchiseDetails['description'] : '',
			'landline' => isset($franchiseDetails['landline']) ? $franchiseDetails['landline'] : '',
			'mobileNo1' => isset($franchiseDetails['mobileNo1']) ? $franchiseDetails['mobileNo1'] : '',
			'mobileNo2' => isset($franchiseDetails['mobileNo2']) ? $franchiseDetails['mobileNo2'] : '',
			'prepaid'	=> isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'yes',
			'email1' => isset($franchiseDetails['email1']) ? $franchiseDetails['email1'] : '',
			'email2' => isset($franchiseDetails['email2']) ? $franchiseDetails['email2'] : '',
			'userId' => isset($franchiseDetails['userId']) ? $franchiseDetails['userId'] : '',
			'fullAddress' => isset($franchiseDetails['fullAddress']) ? $franchiseDetails['fullAddress'] : '',
			'otherDetails' => isset($franchiseDetails['otherDetails']) ? $franchiseDetails['otherDetails'] : '',
			'numberofBasicLicence'	=> isset($franchiseDetails['numberofBasicLicence']) ? $franchiseDetails['numberofBasicLicence'] : '0',
			'numberofAdvanceLicence' => isset($franchiseDetails['numberofAdvanceLicence']) ? $franchiseDetails['numberofAdvanceLicence'] : '0',
			'numberofPremiumLicence' => isset($franchiseDetails['numberofPremiumLicence']) ? $franchiseDetails['numberofPremiumLicence'] : '0',
			'numberofPremPlusLicence' => isset($franchiseDetails['numberofPremPlusLicence']) ? $franchiseDetails['numberofPremPlusLicence'] : '0',
			'availableBasicLicence'	=> isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : $ablLicence,
			'availableAdvanceLicence' => isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0',
			'availablePremiumLicence'	=> isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0',
			'availablePremPlusLicence' => isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0',
			'website' => isset($franchiseDetails['website']) ? $franchiseDetails['website'] : '',
			'website2' => isset($franchiseDetails['website2']) ? $franchiseDetails['website2'] : '',
			'website3' => isset($franchiseDetails['website3']) ? $franchiseDetails['website3'] : '',
			'website4' => isset($franchiseDetails['website4']) ? $franchiseDetails['website4'] : '',
			'trackPage' => isset($franchiseDetails['trackPage']) ? $franchiseDetails['trackPage'] : '',
			'smsSender' => isset($franchiseDetails['smsSender']) ? $franchiseDetails['smsSender'] : '',
			'smsProvider' => isset($franchiseDetails['smsProvider']) ? $franchiseDetails['smsProvider'] : '',
			'providerUserName' => isset($franchiseDetails['providerUserName']) ? $franchiseDetails['providerUserName'] : '',
			'providerPassword' => isset($franchiseDetails['providerPassword']) ? $franchiseDetails['providerPassword'] : '',
			'timeZone' => isset($franchiseDetails['timeZone']) ? $franchiseDetails['timeZone'] : '',
			'apiKey' => isset($franchiseDetails['apiKey']) ? $franchiseDetails['apiKey'] : '',
			'apiKey2' => isset($franchiseDetails['apiKey2']) ? $franchiseDetails['apiKey2'] : '',
			'apiKey3' => isset($franchiseDetails['apiKey3']) ? $franchiseDetails['apiKey3'] : '',
			'apiKey4' => isset($franchiseDetails['apiKey4']) ? $franchiseDetails['apiKey4'] : '',
			'mapKey' => isset($franchiseDetails['mapKey']) ? $franchiseDetails['mapKey'] : '',
			'addressKey' => isset($franchiseDetails['addressKey']) ? $franchiseDetails['addressKey'] : '',
			'notificationKey' => isset($franchiseDetails['notificationKey']) ? $franchiseDetails['notificationKey'] : '',
			'gpsvtsApp' => isset($franchiseDetails['gpsvtsApp']) ? $franchiseDetails['gpsvtsApp'] : '',
			'backUpDays' => isset($franchiseDetails['backUpDays']) ? $franchiseDetails['backUpDays'] : '',
			'dbType' => isset($franchiseDetails['dbType']) ? $franchiseDetails['dbType'] : '',
			'zoho' => isset($franchiseDetails['zoho']) ? $franchiseDetails['zoho'] : '',
			'auth' => isset($franchiseDetails['auth']) ? $franchiseDetails['auth'] : '',
			'subDomain' => isset($franchiseDetails['subDomain']) ? $franchiseDetails['subDomain'] : ''
		);
		$detailsJson = json_encode($details);
		$redis->hmset('H_Franchise', $fcode, $detailsJson);
		//databases creation
		$servername = $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
		if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
			return 'Ipaddress Failed !!!';
		}
		$usernamedb  =  "root";
		$password    =  "#vamo123";
		$dbname      =  $fcode;
		$conn  =  mysqli_connect($servername, $usernamedb, $password);
		$create_db = "CREATE DATABASE IF NOT EXISTS $dbname";
		$conn  =  mysqli_connect($servername, $usernamedb, $password, $dbname);
		if (!$conn) {
			die('Could not connect: ' . mysqli_connect_error());
			return 'Please Update One more time Connection failed';
		} else {
			if (!$conn) {
				die('Could not connect:' . mysqli_connect_error());
				return 'Please Update One more time Connection failed';
			} else {
				$renewal = "CREATE TABLE IF NOT EXISTS Renewal_Details(ID INT AUTO_INCREMENT PRIMARY KEY,User_Name varchar(50),Licence_Id varchar(50),Vehicle_Id varchar(50),Device_Id varchar(50),Type ENUM('Basic', 'Advance', 'Premium') NOT NULL,Processed_Date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,Status ENUM('Migration', 'Rename', 'OnBoard','Cancel','Renew') NOT NULL)";
				$result = $conn->query($renewal);
			}
			$conn->close();
		}
		$dealerList = $redis->smembers('S_Dealers_' . $fcode);
		foreach ($dealerList as $key => $dealerId) {
			$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $dealerId);
			$detail = json_decode($detailJson, true);
			$preOn = $redis->scard('S_Pre_Onboard_Dealer_' . $dealerId . '_' . $fcode);
			$onb = $redis->scard('S_Vehicles_Dealer_' . $dealerId . '_' . $fcode);
			$totalLicence = $preOn + $onb;
			$dealDetail = array(
				'email'	=> isset($detail['email']) ? $detail['email'] : '',
				'mobileNo' => isset($detail['mobileNo']) ? $detail['mobileNo'] : '',
				'website' => isset($detail['website']) ? $detail['website'] : '',
				'smsSender' => isset($detail['smsSender']) ? $detail['smsSender'] : '',
				'smsProvider' => isset($detail['smsProvider']) ? $detail['smsProvider'] : '',
				'providerUserName' => isset($detail['providerUserName']) ? $detail['providerUserName'] : '',
				'providerPassword' => isset($detail['providerPassword']) ? $detail['providerPassword'] : '',
				'numofBasic'	=> isset($detail['numofBasic']) ? $detail['numofBasic'] : '0',
				'numofAdvance'	=> isset($detail['numofAdvance']) ? $detail['numofAdvance'] : '0',
				'numofPremium'	=> isset($detail['numofPremium']) ? $detail['numofPremium'] : '0',
				'numofPremiumPlus' => isset($detail['numofPremiumPlus']) ? $detail['numofPremiumPlus'] : '0',
				'avlofBasic'	=> isset($detail['avlofBasic']) ? $detail['avlofBasic'] : $preOn,
				'avlofAdvance'	=> isset($detail['avlofAdvance']) ? $detail['avlofAdvance'] : '0',
				'avlofPremium'	=> isset($detail['avlofPremium']) ? $detail['avlofPremium'] : '0',
				'avlofPremiumPlus' => isset($detail['avlofPremiumPlus']) ? $detail['avlofPremiumPlus'] : '0',
				'zoho' => isset($detail['zoho']) ? $detail['zoho'] : '',
				'mapKey' => isset($detail['mapKey']) ? $detail['mapKey'] : '',
				'addressKey' => isset($detail['addressKey']) ? $detail['addressKey'] : '',
				'notificationKey' => isset($detail['notificationKey']) ? $detail['notificationKey'] : '',
				'gpsvtsApp' => isset($detail['gpsvtsApp']) ? $detail['gpsvtsApp'] : '',
				'LicenceissuedDate' => isset($detail['LicenceissuedDate']) ? $detail['LicenceissuedDate'] : '',
			);
			$dealDetail = json_encode($dealDetail);
			$redis->hset('H_DealerDetails_' . $fcode, $dealerId, $dealDetail);
			$vehicleListId = $redis->smembers('S_Vehicles_Dealer_' . $dealerId . '_' . $fcode);
			$bc = 0;
			$ad = 0;
			foreach ($vehicleListId as $key => $vehicleId) {
				$exsPre = $redis->sismember('S_Pre_Onboard_Dealer_' . $dealerId . '_' . $fcode, $vehicleId);
				if ($exsPre != 1) {
					$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);

					$vehicleRefData = json_decode($vehicleRefData, true);
					$type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : 'Advance';
					$onboardDate = isset($vehicleRefData['onboardDate']) ? $vehicleRefData['onboardDate'] : '';
					if ($type == 'Basic') {
						$LicenceId = strtoupper('BC' . uniqid());
						$bc = $bc + 1;
					} else if ($type == 'Advance') {
						$LicenceId = strtoupper('AV' . uniqid());
						$ad = $ad + 1;
					}
					$deviceid = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicleId);
					$LicenceissuedDate = $onboardDate;
					$LicenceOnboardDate = $onboardDate;
					$toDate = strtotime($onboardDate);
					$LicenceExpiryDate = date("d-m-Y", strtotime("+12 month", $toDate));
					$LiceneceDataArr = array(
						'LicenceissuedDate' => $LicenceissuedDate,
						'LicenceOnboardDate' => $LicenceOnboardDate,
						'LicenceExpiryDate' => $LicenceExpiryDate,
					);
					$LicenceExpiryJson = json_encode($LiceneceDataArr);
					$redis->hset('H_LicenceExpiry_' . $fcode, $LicenceId, $LicenceExpiryJson);
					$redis->hset('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId, $vehicleId);
					$redis->hset('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleId, $LicenceId);
					$redis->sadd('S_LicenceList_' . $fcode, $LicenceId);
					$redis->sadd('S_LicenceList_dealer_' . $dealerId . '_' . $fcode, $LicenceId);
					try {
						$franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
						$servername = $franchiesJson;
						//$servername="209.97.163.4";
						if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
							return 'Ipaddress Failed !!!';
						}
						$usernamedb = "root";
						$password = "#vamo123";
						$dbname = $fcode;
						$conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
						if (!$conn) {
							die('Could not connect: ' . mysqli_connect_error());
							return 'Please Update One more time Connection failed';
						} else {
							$ins = "INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$dealerId','$LicenceId','$vehicleId','$deviceid','$type','OnBoard')";
							if ($conn->multi_query($ins)) {
								logger('Successfully inserted');
							} else {
								logger('not inserted');
							}
							$conn->close();
						}
					} catch (Exception $e) {
						logger('Mysql issues');
					}
				}
			}
			$dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $dealerId);
			$dealersDetails = json_decode($dealersDetails_json, true);
			$dealersDetails['numofBasic'] = $dealersDetails['numofBasic'] + $bc;
			$dealersDetails['numofAdvance'] = $dealersDetails['numofAdvance'] + $ad;
			$detailsJsonDeal = json_encode($dealersDetails);
			$redis->hmset('H_DealerDetails_' . $fcode, $dealerId, $detailsJsonDeal);
		}

		$adminVehId = $redis->smembers('S_Vehicles_Admin_' . $fcode);
		$adBc = 0;
		$adAd = 0;
		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		$userId = isset($franchiseDetails['userId']) ? $franchiseDetails['userId'] : '';
		foreach ($adminVehId as $key => $vehicle) {
			$deviceid = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicle);
			$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);
			$vehicleRefData = json_decode($vehicleRefData, true);
			$type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : 'Advance';
			$onboardDate = isset($vehicleRefData['onboardDate']) ? $vehicleRefData['onboardDate'] : '';
			if ($type == 'Basic') {
				$LicenceId = strtoupper('BC' . uniqid());
				$adBc = $adBc + 1;
			} else if ($type == 'Advance') {
				$LicenceId = strtoupper('AV' . uniqid());
				$adAd = $adAd + 1;
			}
			$LicenceissuedDate = $onboardDate;
			$LicenceOnboardDate = $onboardDate;
			$data = str_replace('-', '/', $onboardDate);
			$LicenceExpiryDate = date('m-d-Y', strtotime($data . "+ 1 year"));
			$LiceneceDataArr = array(
				'LicenceissuedDate' => $LicenceissuedDate,
				'LicenceOnboardDate' => $LicenceOnboardDate,
				'LicenceExpiryDate' => $LicenceExpiryDate,
			);
			$LiceneceDataArr = json_encode($LiceneceDataArr);
			$redis->hset('H_LicenceExpiry_' . $fcode, $LicenceId, $LiceneceDataArr);
			$redis->sadd('S_LicenceList_admin_' . $fcode, $LicenceId);
			$redis->sadd('S_LicenceList_' . $fcode, $LicenceId);
			$redis->hset('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId, $vehicle);
			$redis->hset('H_Vehicle_LicenceId_Map_' . $fcode, $vehicle, $LicenceId);
			try {
				$franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
				$servername = $franchiesJson;
				//$servername="209.97.163.4";
				if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
					return 'Ipaddress Failed !!!';
				}
				$usernamedb = "root";
				$password = "#vamo123";
				$dbname = $fcode;
				$conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
				if (!$conn) {
					die('Could not connect: ' . mysqli_connect_error());
					return 'Please Update One more time Connection failed';
				} else {
					$ins = "INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$userId','$LicenceId','$vehicle','$deviceid','$type','OnBoard')";
					if ($conn->multi_query($ins)) {
						logger('Successfully inserted');
					} else {
						logger('not inserted');
					}
					$conn->close();
				}
			} catch (Exception $e) {
				logger('Mysql issues');
			}
		}
		$ablLicence = $ablLicence + $adBc;
		$franchiseDetails['numberofBasicLicence'] = $franchiseDetails['numberofBasicLicence'] + $ablLicence;
		$franchiseDetails['numberofAdvanceLicence'] = $franchiseDetails['numberofAdvanceLicence'] + $adAd;
		$detailsJson = json_encode($franchiseDetails);
		$redis->hmset('H_Franchise', $fcode, $detailsJson);

		session()->flash('message', $fname . ' Franchise has been Coverted into Prepaid Franchise !');
		return redirect()->to('vdmFranchises');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function edit($id)
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		// return request()->server();
		$username = auth()->id();

		$redis = Redis::connection();
		$fcode = $id;

		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		$prepaid = $franchiseDetails['prepaid'] ?? 'no';
		$description = $franchiseDetails['description'] ?? '';
		$landline = $franchiseDetails['landline'] ?? '';
		$mobileNo1 = $franchiseDetails['mobileNo1'] ?? '';
		$mobileNo2 = $franchiseDetails['mobileNo2'] ?? "";
		$zoho = $franchiseDetails['zoho'] ?? "";
		$auth = $franchiseDetails['auth'] ?? "";
		$email1 = $franchiseDetails['email1'] ?? "";
		$email2 = $franchiseDetails['email2'] ?? "";
		$userId = $franchiseDetails['userId'] ?? "";
		$fullAddress = $franchiseDetails['fullAddress'] ?? "";
		$otherDetails = $franchiseDetails['otherDetails'] ?? "";
		$website = $franchiseDetails['website'] ?? "";
		$website2 = $franchiseDetails['website2'] ?? "";
		$website3 = $franchiseDetails['website3'] ?? "";
		$website4 = $franchiseDetails['website4'] ?? "";
		$trackPage = $franchiseDetails['trackPage'] ?? 'Normal View';
		$smsSender = $franchiseDetails['smsSender'] ?? "";
		$smsProvider = $franchiseDetails['smsProvider'] ?? "nill";
		$providerUserName = $franchiseDetails['providerUserName'] ?? "";
		$providerPassword = $franchiseDetails['providerPassword'] ?? "";
		$enableStarter = $franchiseDetails['enableStarter'] ?? "no";
		$timeZone = $franchiseDetails['timeZone'] ?? 'Asia/Kolkata';
		$apiKey = $franchiseDetails['apiKey'] ?? "";
		$apiKey2 = $franchiseDetails['apiKey2'] ?? "";
		$apiKey3 = $franchiseDetails['apiKey3'] ?? "";
		$apiKey4 = $franchiseDetails['apiKey4'] ?? "";
		$mapKey = $franchiseDetails['mapKey'] ?? "";
		$addressKey = $franchiseDetails['addressKey'] ?? "";
		$notificationKey = $franchiseDetails['notificationKey'] ?? $redis->hget('H_Google_Android_ServerKey', $fcode);
		$backUpDays = $franchiseDetails['backUpDays'] ?? "60";
		$dbType = $franchiseDetails['dbType'] ?? "mysql";
		$gpsvtsAppKey = $franchiseDetails['gpsvtsApp'] ?? $redis->hget('H_MobileAppId', $fcode);
		$subDomain = $franchiseDetails['subDomain'] ?? "";
		$languageCode = $franchiseDetails['languageCode'] ?? "en";
		$authenticated = $franchiseDetails['authenticated'] ?? "no";
		$defaultMap = $franchiseDetails['defaultMap'] ?? "no";

		$textLocalSmsKey = $franchiseDetails['textLocalSmsKey'] ?? "";
		$smsEntity = $franchiseDetails['smsEntity'] ?? "";

		$dbIp = $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
		if ($dbIp == '') {
			$dbIp = '188.166.244.126';
		}
		$key = array_search($dbIp, VdmFranchiseController::dbIp());
		$dbIp = $key;

		$licList = [];

		if ($prepaid == 'yes') {


			$licList['numberofStarterLicence']  = isset($franchiseDetails['numberofStarterLicence']) ? $franchiseDetails['numberofStarterLicence'] : '0';
			$licList['numberofStarterPlusLicence']  = isset($franchiseDetails['numberofStarterPlusLicence']) ? $franchiseDetails['numberofStarterPlusLicence'] : '0';
			$licList['numberofBasicLicence'] = isset($franchiseDetails['numberofBasicLicence']) ? $franchiseDetails['numberofBasicLicence'] : '0';
			$licList['numberofAdvanceLicence'] = isset($franchiseDetails['numberofAdvanceLicence']) ? $franchiseDetails['numberofAdvanceLicence'] : '0';
			$licList['numberofPremiumLicence'] = isset($franchiseDetails['numberofPremiumLicence']) ? $franchiseDetails['numberofPremiumLicence'] : '0';
			$licList['numberofPremPlusLicence'] = isset($franchiseDetails['numberofPremPlusLicence']) ? $franchiseDetails['numberofPremPlusLicence'] : '0';
			// availableStarterPlusLicence
			$licList['availableStarterLicence']		= isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : '0';
			$licList['availableStarterPlusLicence'] = isset($franchiseDetails['availableStarterPlusLicence']) ? $franchiseDetails['availableStarterPlusLicence'] : '0';
			$licList['availableBasicLicence']		= isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';
			$licList['availableAdvanceLicence']	= isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';
			$licList['availablePremiumLicence']	= isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';
			$licList['availablePremPlusLicence'] = isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0';
			$licList['numberofLicence'] = 0;
			$licList['availableLincence'] = 0;
		} else {
			$licList['numberofLicence'] =  isset($franchiseDetails['numberofLicence']) ? $franchiseDetails['numberofLicence'] : '0';
			$licList['availableLincence'] = isset($franchiseDetails['availableLincence']) ? $franchiseDetails['availableLincence'] : '0';

			$licList['numberofStarterLicence']  =  '0';
			$licList['numberofStarterPlusLicence'] = '0';
			$licList['numberofBasicLicence'] =  '0';
			$licList['numberofAdvanceLicence'] =  '0';
			$licList['numberofPremiumLicence'] = '0';
			$licList['numberofPremPlusLicence'] =  '0';
			$licList['availableStarterLicence'] =  '0';
			$licList['availableStarterPlusLicence'] = '0';
			$licList['availableBasicLicence'] =  '0';
			$licList['availableAdvanceLicence']	= '0';
			$licList['availablePremiumLicence']	= '0';
			$licList['availablePremPlusLicence'] =  '0';
		}

		// return $franchiseDetails;
		$defaultMapList = ['osm' => 'OSM', 'googleMap' => "Google"];

		$editData = [
			"fcode" => $fcode,
			"franchiseDetails" => $franchiseDetails,
			"enableStarter" => $enableStarter,
			"description" => $description,
			"landline" => $landline,
			"mobileNo1" => $mobileNo1,
			"mobileNo2" => $mobileNo2,
			"email1" => $email1,
			"email2" => $email2,
			"zoho" => $zoho,
			"auth" => $auth,
			"userId" => $userId,
			"fullAddress" => $fullAddress,
			"otherDetails" => $otherDetails,
			"website" => $website,
			"website2" => $website2,
			"website3" => $website3,
			"website4" => $website4,
			"trackPage" => $trackPage,
			"smsSender" => $smsSender,
			"smsProvider" => $smsProvider,
			"providerUserName" => $providerUserName,
			"providerPassword" => $providerPassword,
			"timeZone" => $timeZone,
			"apiKey" => $apiKey,
			"apiKey2" => $apiKey2,
			"apiKey3" => $apiKey3,
			"apiKey4" => $apiKey4,
			"mapKey" => $mapKey,
			"addressKey" => $addressKey,
			"notificationKey" => $notificationKey,
			"gpsvtsAppKey" => $gpsvtsAppKey,
			"dbIp" => $dbIp,
			"dbType" => $dbType,
			"backUpDays" => $backUpDays,
			"dbIpAr" => VdmFranchiseController::dbIp(),
			"smsP" => VdmFranchiseController::smsP(),
			"timeZoneC" => VdmFranchiseController::timeZoneC(),
			"backType" => VdmFranchiseController::backTypeC(),
			"prepaid" => $prepaid,
			"subDomain" => $subDomain,
			"languageCode" => $languageCode,
			"authenticated" => $authenticated,
			'fname' => $franchiseDetails['fname'],
			'textLocalSmsKey' => $textLocalSmsKey,
			'smsEntity' => $smsEntity,
			'defaultMap' => $defaultMap,
			'defaultMapList' => $defaultMapList
		];
		return view('vdm.franchise.edit', $editData, $licList);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function update($id)
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$fcode = $id;
		$username = auth()->id();
		$redis = Redis::connection();

		$rules = array(

			'description' => 'required',
			'fullAddress' => 'required',
			'landline' => 'required',
			'mobileNo1' => 'required',
			'email1' => 'required|email',
			'email2' => 'required|email',
			'website' => 'required',
			'trackPage' => 'required'
		);
		$validator = validator()->make(request()->all(), $rules);


		if ($validator->fails()) {
			session()->flash('message', 'Update failed. Please enter all required fields. !');

			return redirect()->back()->withInput()->withErrors($validator);
		} else {
			// store

			$dbIpindex = request()->get('ipadd');
			$dbIpAr = VdmFranchiseController::dbIp();
			$dbIp = $dbIpAr[$dbIpindex];
			$description = request()->get('description');
			$fullAddress = request()->get('fullAddress');
			$landline = request()->get('landline');
			$mobileNo1 = request()->get('mobileNo1');
			$zoho = request()->get('zoho');
			$auth = request()->get('auth');
			$mobileNo2 = request()->get('mobileNo2');
			$email1 = request()->get('email1');
			$email2 = request()->get('email2');
			$otherDetails = request()->get('otherDetails');
			$website = request()->get('website');
			$website2 = request()->get('website2');
			$website3 = request()->get('website3');
			$website4 = request()->get('website4');
			$trackPage = request()->get('trackPage');
			$smsSender = request()->get('smsSender');
			$smsProvider = request()->get('smsProvider');
			$providerUserName = request()->get('providerUserName');
			$providerPassword = request()->get('providerPassword');
			$timeZone = request()->get('timeZone');
			$apiKey = request()->get('apiKey');
			$apiKey2 = request()->get('apiKey2');
			$apiKey3 = request()->get('apiKey3');
			$apiKey4 = request()->get('apiKey4');
			$mapKey = request()->get('mapKey');
			$addressKey = request()->get('addressKey');
			$notificationKey = request()->get('notificationKey');
			$gpsvtsAppKey = request()->get('gpsvtsAppKey');
			$backUpDays = request()->get('backUpDays');
			$dbType = request()->get('dbType');
			$subDomain = request()->get('subDomain');
			$languageCode = request()->get('languageCode');
			$authenticated = request()->get('authenticated');
			$enableStarter = request()->get('enableStarter');
			$textLocalSmsKey = request()->get('textLocalSmsKey');
			$smsEntity = request()->get('smsEntity');
			$defaultMap = request()->get('defaultMap');
			$redis = Redis::connection();

			if ($authenticated == "yes") {
				$sites = ['website2' => $website2, 'website3' => $website3, 'website4' => $website4];
				if (str_contains($website, "vamosys.com")) {
					$invalid = null;
					foreach ($sites as $key => $site) {
						if ($site == "") continue;
						if (!str_contains($site, "vamosys.com")) {
							$invalid[] = $key;
						}
					}
					if (!is_null($invalid)) {
						$invalid = join(', ', $invalid);
						return redirect()->back()->withErrors($invalid . " url is not authorized for approval")->withInput();
					}
				} else {
					return redirect()->back()->withErrors("The website url is not authorized for approval")->withInput();
				}
			}

			$franDetails_json = $redis->hget('H_Franchise', $fcode);
			$franchiseDetails = json_decode($franDetails_json, true);

			$userId = $franchiseDetails['userId'];
			$fname = $franchiseDetails['fname'];

			$pattern = "/[^0-9\,]/";
			if ((preg_match($pattern, $mobileNo1)) || (preg_match($pattern, $mobileNo2))) {
				return redirect()->back()->withInput()->withErrors('Only Numeric values are allowed in Mobile Numbers');
			}
			$prepaid = request()->get('prepaid');
			if ($prepaid == 'yes') {
				$addStarterLicence = request()->get('addStarterLicence');
				$addStarterPlusLicence = request()->get('addStarterPlusLicence');
				$addBasicLicence = request()->get('addBasicLicence');
				$addAdvanceLicence = request()->get('addAdvanceLicence');
				$addPremiumLicence = request()->get('addPremiumLicence');
				$addPrePlusLicence = request()->get('addPrePlusLicence');
				$availableStarterLicence = request()->get('availableStarterLicence');
				$availableBasicLicence = request()->get('availableBasicLicence');
				$availableAdvanceLicence = request()->get('availableAdvanceLicence');
				$availablePremiumLicence = request()->get('availablePremiumLicence');

				// StarterPlus
				if ($addStarterPlusLicence == null) {
					$numberofStarterPlusLicence = request()->get('numberofStarterPlusLicence');
					$availableStarterPlusLicence = request()->get('availableStarterPlusLicence');
				} else if ($addStarterPlusLicence > 0) {
					$numberofStarterPlusLicence = request()->get('numberofStarterPlusLicence') + $addStarterPlusLicence;
					$availableStarterPlusLicence = request()->get('availableStarterPlusLicence') + $addStarterPlusLicence;
				} else {
					$avl = request()->get('availableStarterPlusLicence');
					$abl = (-1) * $addStarterPlusLicence;
					if ($avl >= $abl) {
						$numberofStarterPlusLicence = request()->get('numberofStarterPlusLicence') - $abl;
						$availableStarterPlusLicence = $avl - $abl;
					} else {
						return redirect()->back()->withErrors('Please enter valid Starter Plus Licence !')->withInput();
					}
				}

				//Starter
				if ($addStarterLicence == null) {
					$numberofStarterLicence = request()->get('numberofStarterLicence');
					$availableStarterLicence = request()->get('availableStarterLicence');
				} else if ($addStarterLicence > 0) {
					$numberofStarterLicence = request()->get('numberofStarterLicence') + $addStarterLicence;
					$availableStarterLicence = request()->get('availableStarterLicence') + $addStarterLicence;
				} else {
					$avl = request()->get('availableStarterLicence');
					$abl = (-1) * $addStarterLicence;
					if ($avl >= $abl) {
						$numberofStarterLicence = request()->get('numberofStarterLicence') - $abl;
						$availableStarterLicence = $avl - $abl;
					} else {
						return redirect()->back()->withErrors('Please enter valid Starter Licence !')->withInput();
					}
				}
				//basic
				if ($addBasicLicence == null) {
					$numberofBasicLicence = request()->get('numberofBasicLicence');
					$availableBasicLicence = request()->get('availableBasicLicence');
				} else if ($addBasicLicence > 0) {
					$numberofBasicLicence = request()->get('numberofBasicLicence') + $addBasicLicence;
					$availableBasicLicence = request()->get('availableBasicLicence') + $addBasicLicence;
				} else {
					$avl = request()->get('availableBasicLicence');
					$abl = (-1) * $addBasicLicence;
					if ($avl >= $abl) {
						$numberofBasicLicence = request()->get('numberofBasicLicence') - $abl;
						$availableBasicLicence = $avl - $abl;
					} else {
						return redirect()->back()->withErrors('Please enter valid Basic Licence !')->withInput();
					}
				}
				//advance
				if ($addAdvanceLicence == null) {
					$numberofAdvanceLicence = request()->get('numberofAdvanceLicence');
					$availableAdvanceLicence = request()->get('availableAdvanceLicence');
				} else if ($addAdvanceLicence > 0) {
					$numberofAdvanceLicence = request()->get('numberofAdvanceLicence') + $addAdvanceLicence;
					$availableAdvanceLicence = request()->get('availableAdvanceLicence') + $addAdvanceLicence;
				} else {
					$avl = request()->get('availableAdvanceLicence');
					$aal = (-1) * $addAdvanceLicence;
					if ($avl >= $aal) {
						$numberofAdvanceLicence = request()->get('numberofAdvanceLicence') - $aal;
						$availableAdvanceLicence = $avl - $aal;
					} else {
						return redirect()->back()->withErrors('Please enter valid Advance Licence !')->withInput();
					}
				}
				//Premium
				if ($addPremiumLicence == null) {
					$numberofPremiumLicence = request()->get('numberofPremiumLicence');
					$availablePremiumLicence = request()->get('availablePremiumLicence');
				} else if ($addPremiumLicence > 0) {
					$numberofPremiumLicence = request()->get('numberofPremiumLicence') + $addPremiumLicence;
					$availablePremiumLicence = request()->get('availablePremiumLicence') + $addPremiumLicence;
				} else {
					$avl = request()->get('availablePremiumLicence');
					$apl = (-1) * $addPremiumLicence;
					if ($avl >= $apl) {
						$numberofPremiumLicence = request()->get('numberofPremiumLicence') - $apl;
						$availablePremiumLicence = $avl - $apl;
					} else {
						return redirect()->back()->withErrors('Please enter valid Premium Licence !')->withInput();
					}
				}

				//Premium Plus
				if ($addPrePlusLicence == null) {
					$numberofPremPlusLicence = request()->get('numberofPremPlusLicence');
					$availablePremPlusLicence = request()->get('availablePremPlusLicence');
				} else if ($addPrePlusLicence > 0) {
					$numberofPremPlusLicence = request()->get('numberofPremPlusLicence') + $addPrePlusLicence;
					$availablePremPlusLicence = request()->get('availablePremPlusLicence') + $addPrePlusLicence;
				} else {
					$avl = request()->get('availablePremPlusLicence');
					$apl = (-1) * $addPrePlusLicence;
					if ($avl >= $apl) {
						$numberofPremPlusLicence = request()->get('numberofPremPlusLicence') - $apl;
						$availablePremPlusLicence = $avl - $apl;
					} else {
						return redirect()->back()->withErrors('Please enter valid Premium Licence !')->withInput();
					}
				}
				if ($availableStarterLicence == null) {
					$availableStarterLicence = 0;
				}
				if ($availableBasicLicence == null) {
					$availableBasicLicence = 0;
				}
				if ($availableAdvanceLicence == null) {
					$availableAdvanceLicence = 0;
				}
				if ($availablePremiumLicence == null) {
					$availablePremiumLicence = 0;
				}
				if ($availablePremPlusLicence == null) {
					$availablePremPlusLicence = 0;
				}
				if ($numberofPremPlusLicence == null) {
					$numberofPremPlusLicence = 0;
				}
				if ($numberofPremiumLicence == null) {
					$numberofPremiumLicence = 0;
				}
				if ($numberofAdvanceLicence == null) {
					$numberofAdvanceLicence = 0;
				}
				if ($numberofBasicLicence == null) {
					$numberofBasicLicence = 0;
				}
				if ($numberofStarterLicence == null) {
					$numberofStarterLicence = 0;
				}

				//prepaid store
				$details = array(
					'fname' => $fname,
					'description' => $description,
					'landline' => $landline,
					'mobileNo1' => $mobileNo1,
					'zoho' => $zoho,
					'auth' => $auth,
					'mobileNo2' => $mobileNo2,
					'email1' => $email1,
					'email2' => $email2,
					'userId' => $userId,
					'fullAddress' => $fullAddress,
					'otherDetails' => $otherDetails,
					'otherDetails' => $otherDetails,
					'numberofStarterPlusLicence'	=> $numberofStarterPlusLicence,
					'availableStarterPlusLicence'	=> $availableStarterPlusLicence,
					'numberofStarterLicence'	=> $numberofStarterLicence,
					'availableStarterLicence'	=> $availableStarterLicence,
					'numberofBasicLicence'	=> $numberofBasicLicence,
					'availableBasicLicence'	=> $availableBasicLicence,
					'numberofAdvanceLicence' => $numberofAdvanceLicence,
					'availableAdvanceLicence' => $availableAdvanceLicence,
					'numberofPremiumLicence'  => $numberofPremiumLicence,
					'availablePremiumLicence' => $availablePremiumLicence,
					'numberofPremPlusLicence' => $numberofPremPlusLicence,
					'availablePremPlusLicence'  => $availablePremPlusLicence,
					'website' => $website,
					'website2' => $website2,
					'website3' => $website3,
					'website4' => $website4,
					'trackPage' => $trackPage,
					'smsSender' => $smsSender,
					'smsProvider' => $smsProvider,
					'providerUserName' => $providerUserName,
					'providerPassword' => $providerPassword,
					//'eFDSchedular'=>$eFDSchedular,
					'timeZone' => $timeZone,
					'apiKey' => $apiKey,
					'apiKey2' => $apiKey2,
					'apiKey3' => $apiKey3,
					'apiKey4' => $apiKey4,
					'mapKey' => $mapKey,
					'addressKey' => $addressKey,
					'notificationKey' => $notificationKey,
					'gpsvtsApp' => $gpsvtsAppKey,
					'backUpDays' => $backUpDays,
					'dbType' => $dbType,
					'prepaid' => $prepaid,
					'subDomain' => $subDomain,
					'languageCode' => $languageCode,
					'enableStarter' => $enableStarter,
					'authenticated' => $authenticated,
					'textLocalSmsKey' => $textLocalSmsKey,
					'smsEntity' => $smsEntity,
					'defaultMap' => $defaultMap
				);
			} else {
				$numberofLicence = request()->get('addLicence');

				if ($numberofLicence == null) {
					$numberofLicence = 0;
					$availableLincence = request()->get('availableLincenceO');
					$numberofLicence = request()->get('numberofLicenceO');
				} else if ($numberofLicence > 0) {
					$availableLincence = request()->get('availableLincenceO') + $numberofLicence;
					$numberofLicence = request()->get('numberofLicenceO') + $numberofLicence;
				} else if ($numberofLicence < 0) {
					$noofLic = (-1) * $numberofLicence;
					if (request()->get('availableLincenceO') < $noofLic) {
						if (request()->get('availableLincenceO') == 0) {
							session()->flash('message', 'Update Failed !');
							return redirect()->to('vdmFranchises');
						} else {
							$availableLic = (request()->get('availableLincenceO') - request()->get('availableLincenceO'));
							$numberofLic = (request()->get('numberofLicenceO') - request()->get('availableLincenceO'));
						}
					}
					if (request()->get('availableLincenceO') >= $noofLic) {
						$availableLic = request()->get('availableLincenceO') - $noofLic;
						$numberofLic = request()->get('numberofLicenceO') - $noofLic;
					}
					if ($availableLic >= 0) {
						$availableLincence = $availableLic;
					} else {
						$availableLincence = 0;
					}
					if ($numberofLic >= 0) {
						$numberofLicence = $numberofLic;
					} else {
						$numberofLicence = 0;
					}
				}

				//normal store

				$details = array(
					'fname' => $fname,
					'description' => $description,
					'landline' => $landline,
					'mobileNo1' => $mobileNo1,
					'zoho' => $zoho,
					'auth' => $auth,
					'mobileNo2' => $mobileNo2,
					'email1' => $email1,
					'email2' => $email2,
					'userId' => $userId,
					'fullAddress' => $fullAddress,
					'otherDetails' => $otherDetails,
					'numberofLicence' => $numberofLicence,
					'availableLincence' => $availableLincence,
					'website' => $website,
					'website2' => $website2,
					'website3' => $website3,
					'website4' => $website4,
					'trackPage' => $trackPage,
					'smsSender' => $smsSender,
					'smsProvider' => $smsProvider,
					'providerUserName' => $providerUserName,
					'providerPassword' => $providerPassword,
					//'eFDSchedular'=>$eFDSchedular,
					'timeZone' => $timeZone,
					'apiKey' => $apiKey,
					'apiKey2' => $apiKey2,
					'apiKey3' => $apiKey3,
					'apiKey4' => $apiKey4,
					'mapKey' => $mapKey,
					'addressKey' => $addressKey,
					'notificationKey' => $notificationKey,
					'gpsvtsApp' => $gpsvtsAppKey,
					'backUpDays' => $backUpDays,
					'dbType' => $dbType,
					'prepaid' => $prepaid,
					'subDomain' => $subDomain,
					'languageCode' => $languageCode,
					'enableStarter' => $enableStarter,
					'authenticated' => $authenticated,
					'textLocalSmsKey' => $textLocalSmsKey,
					'smsEntity' => $smsEntity,
					'defaultMap' => $defaultMap
				);
			}

			$geolocation = VdmFranchiseController::geocode($fullAddress);
			if ($geolocation != null) {
				$LatLong = implode(":", $geolocation);
			} else {
				$LatLong = '0:0';
			}
			$redis->hset('H_Franchise_LatLong', $fcode, $LatLong);



			/*if($numberofLicence<session()->get('available'))
				{
					return redirect()->to ( 'vdmFranchises/update' )->withErrors ( 'Please check the License count' );
				}
				else{*/

			//}

			// $refDataArr = array('regNo'=>$regNo,'vehicleMake'=>$vehicleMake,'vehicleType'=>$vehicleType,'oprName'=>$oprName,
			// 'mobileNo'=>$mobileNo,'vehicleCap'=>$vehicleCap,'deviceModel'=>$deviceModel);

			$getOldIp 	= $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);


			//TODO
			/**
			 * If code is not unique this method fail and return - suggesting
			 * that the ID should be unique.
			 *
			 * Possible improvement..implement ajax call - verify the code while typing itself
			 *
			 */


			/*$redis->hmset ( 'H_Franchise', $fcode.':description',$description,
					$fcode.':landline',$landline,$fcode.':mobileNo1',$mobileNo1,$fcode.':mobileNo2',$mobileNo2,
					$fcode.':email1',$email1,$fcode.':email2',$email2);*/


			$detailsJson = json_encode($details);
			$redis->hmset('H_Franchise_Mysql_DatabaseIP', $fcode, $dbIp);
			$redis->hmset('H_Franchise', $fcode, $detailsJson);

			try {
				$oldData = array_diff($franchiseDetails,$details);
				$newData = array_diff($details,$franchiseDetails);
			 	$status = array(
					'userIpAddress' => session()->get('userIP'),
					'userName'=> $userId,
					'type'=>config()->get('constant.admin'),
					'name'=>$userId,
					'status'=>config()->get('constant.updated'),
					'oldData'=>json_encode($oldData),
					'newData'=>json_encode($newData),
				);
				AuditNewController::updateAudit($status);
			} catch (Exception $e) {
				logger('Error inside Audit_Frans Update' . $e->getMessage());
			}

			$redis->hmset('H_Fcode_Timezone_Schedular', $fcode, $timeZone);

			$redis->sadd('S_Users_' . $fcode, $userId);
			$redis->hmset(
				'H_UserId_Cust_Map',
				$userId . ':fcode',
				$fcode,
				$userId . ':mobileNo',
				$mobileNo1,
				$userId . ':email',
				$email1
			);
			$redis->hset('H_Google_Android_ServerKey', $fcode, $notificationKey);
			$redis->hset('H_MobileAppId', $fcode, $gpsvtsAppKey);

			DB::table('users')
				->where('username', $userId)
				->update(array('email' => $email1));

			$mappingFn = array(
				'fname' => 'Franchise Name',
				'description' => 'Description',
				'landline' => 'Landline Number',
				'mobileNo1' => 'Mobile Number1',
				'zoho' => 'Zoho Organisation',
				'auth' => 'Zoho Authentication',
				'mobileNo2' => 'Mobile Number2',
				'email1' => 'Email 1',
				'email2' => 'Email 2',
				'userId' => 'User Id',
				'fullAddress' => 'Full Address',
				'otherDetails' => 'Other Details',
				'numberofLicence' => 'Number of Licence',
				'availableLincence' => 'Available licence',
				'website' => 'website',
				'website2' => 'website2',
				'website3' => 'websit3',
				'website4' => 'website4',
				'trackPage' => 'TrackPage',
				'smsSender' => 'smsSender',
				'smsProvider' => 'smsProvider',
				'providerUserName' => 'SMS Provider User Name',
				'providerPassword' => 'SMS Provider Password',
				//'eFDSchedular'=>$eFDSchedular,
				'timeZone' => 'Time Zone',
				'apiKey' => 'Api Key',
				'apiKey2' => 'Api Key2',
				'apiKey3' => 'Api Key3',
				'apiKey4' => 'Api Key4',
				'mapKey' => 'Map Key',
				'addressKey' => 'Address Key',
				'notificationKey' => 'Notification Key',
				'gpsvtsApp' => 'App Key',
				'backUpDays' => 'DB BackupDays',
				'dbType' => 'DB Type',
				'dbIP' => 'DB IP',
				'subDomain' => 'SubDomain Name',
				'authenticated' => 'Authenticated',

			);
			$oldFranch = array();
			$NewFranch = array();

			try {

				$fransNew 	=	json_decode($detailsJson, true);

				if (($franchiseDetails !== $fransNew) || ($getOldIp != $dbIp)) {

					if ($getOldIp != $dbIp) {
						$oldFranch 	= Arr::add($oldFranch, $mappingFn['dbIP'], $getOldIp);
						$NewFranch 	= Arr::add($NewFranch, $mappingFn['dbIP'], $dbIp);
					}
					foreach ($fransNew as $key => $value) {
						if (isset($franchiseDetails[$key]) != 1 && isset($mappingFn[$key]) == 1) {
							$oldFranch 	= Arr::add($oldFranch, $mappingFn[$key], '');
							$NewFranch 	= Arr::add($NewFranch, $mappingFn[$key], $value);
						} else if ($franchiseDetails[$key] != $value && isset($mappingFn[$key]) == 1) {

							$oldFranch 	= Arr::add($oldFranch, $mappingFn[$key], $franchiseDetails[$key]);
							$NewFranch 	= Arr::add($NewFranch, $mappingFn[$key], $value);
						}
					}
				}

				session()->put('email', $email2);
			} catch (Exception $e) {
				logger($e->getMessage());
			}
		}

		// redirect
		return redirect()->to('vdmFranchises')->with([
			'message' => "Successfully Updated $fname !"
		]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function destroy($id)
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();

		$fcode = $id;

		//GeoMaxLimit
		$redis->del('GeoCodeMaxLimit_' . $fcode);
		$redis->del('GeoLocationVehicle_' . $fcode);
		//grop remove
		$group = $redis->smembers('S_Groups_' . $fcode);
		foreach ($group as $key => $getGrpName) {
			$veh = $redis->smembers($getGrpName);
			foreach ($veh as $key => $vehL) {

				$redis->srem($getGrpName, $vehL);
			}
			$redis->srem('S_Groups_' . $fcode, $getGrpName);
		}
		$redis->del('S_Groups_Admin_' . $fcode);
		$redis->del('S_Groups_' . $fcode);

		//users remove
		$usr = $redis->smembers('S_Users_' . $fcode);
		foreach ($usr as $key => $username1) {
			//reports Delete under the user
			$redis->del('S_Users_Reports_' . $username1 . '_' . $fcode);
			$redis->hdel('H_UserId_Cust_Map', $username1 . ':fcode', $username1 . ':mobileNo', $username1 . ':email', $username1 . ':password', $username1 .
				':zoho', $username1 . ':OWN', $username1 . ':companyName');
			$redis->hdel("H_Notification_Map_User", $username1);

			DB::table('users')->where('username', $username1)->delete();
			$redis->srem('S_Users_' . $fcode, $username1);
		}
		$redis->del('S_Users_Admin_' . $fcode);
		$redis->del('S_Users_' . $fcode);

		//vehicle remove
		$vehicleList = $redis->smembers('S_Vehicles_' . $fcode);
		foreach ($vehicleList as $key => $vehicleId) {
			$redis->hdel('H_RefData_' . $fcode, $vehicleId);
			$redis->hdel('H_ProData_' . $fcode, $vehicleId);
			$Lhist = $redis->keys('L_Hist_' . $vehicleId . '_' . $fcode . '_*');
			foreach ($Lhist as $key => $LhistID) {
				$redis->del($LhistID);
			}

			$Lsensor = $redis->keys('L_Sensor_Hist_' . $vehicleId . '_' . $fcode . '_*');

			foreach ($Lsensor as $key => $LsensorId) {
				$redis->del($LsensorId);
			}

			$Zsensor = $redis->del('Z_Sensor_' . $vehicleId . '_' . $fcode);
			/*$setZsensor='Z_Sensor_*'. $fcode;*/
			$nodata = $redis->del('NoData24:' . $fcode . ':' . $vehicleId);


			DB::table('Vehicle_details')->where('vehicle_id', $vehicleId)->where('fcode', $fcode)->delete();


			$redis->srem('S_Vehicles_' . $fcode, $vehicleId);
		}
		$redis->del('H_RefData_' . $fcode);
		$redis->del('H_ProData_' . $fcode);
		$redis->del('S_Vehicles_Admin_' . $fcode);
		$redis->del('S_Vehicles_' . $fcode);
		$redis->del('H_LicenceExpiry_' . $fcode);
		$licenceKeys = $redis->keys('S_LicenceList_*_' . $fcode);
		foreach ($licenceKeys as $key => $name) {
			$redis->del($name);
		}
		$redis->del('S_ExpiredLicence_Vehicle' . $fcode);
		$redis->del('H_Vehicle_LicenceId_Map_' . $fcode);


		//Device Remove
		$DeviceList = $redis->smembers('S_Device_' . $fcode);
		foreach ($DeviceList as $key => $DeviceId) {
			$redis->hdel('H_Device_Cpy_Map', $DeviceId);
			$redis->srem('S_Device_' . $fcode, $DeviceId);
		}
		$redis->del('S_Device_' . $fcode);
		//Expire Key Remove
		$redis->del('H_Expire_' . $fcode);

		//org Email alerts 
		$redis->del('H_EMAIL_' . $fcode);

		//Report remove admin
		$redis->del('S_Users_Reports_Admin_' . $fcode);


		//Dealers Remove
		$dealerLi = $redis->smembers('S_Dealers_' . $fcode);
		foreach ($dealerLi as $key => $dealerID) {

			/*$redis->del('S_Users_Reports_'.$username1.'_'.$fcode);*/

			$redis->hdel('H_UserId_Cust_Map', $dealerID . ':fcode', $dealerID . ':email', $dealerID . ':mobileNo', $dealerID . ':password', $dealerID .
				':zoho', $dealerID . ':OWN', $dealerID . ':companyName');
			$redis->hdel('H_DealerDetails_' . $fcode, $dealerID);
			$redis->hdel('H_Google_Android_ServerKey', $fcode . ':' . $dealerID);
			$redis->del('S_Vehicles_Dealer_' . $dealerID . '_' . $fcode);
			DB::table('users')->where('username', $dealerID)->delete();

			$groups = $redis->smembers('S_Groups_Dealer_' . $dealerID . '_' . $fcode);
			foreach ($groups as $key => $value1) {
				$redis->del($value1);
				$redis->del('S_' . $value1);
				$redis->srem('S_Groups_Dealer_' . $dealerID . '_' . $fcode, $value1);
				//$redis->sadd('S_Groups_Admin_'.$fcode,$value1);
			}

			$orgDel = $redis->smembers('S_Organisations_Dealer_' . $dealerID . '_' . $fcode);
			foreach ($orgDel as $key => $value2) {
				$redis->hdel('H_Organisations_' . $fcode, $value2);
				$redis->srem('S_Organisations_Dealer_' . $dealerID . '_' . $fcode, $value2);
				//$redis->sadd('S_Organisations_Admin'.$fcode,$value2);
				$redis->del('H_Site_' . $value2 . '_' . $fcode);
				$redis->del('S_' . $value2);
				$redis->hdel('H_Scheduler_' . $fcode, 'getReadyForMorningSchoolTrips_' . $value2);
				$redis->hdel('H_Scheduler_' . $fcode, 'getReadyForAfternoonSchoolTrips_' . $value2);
				$redis->hdel('H_Scheduler_' . $fcode, 'getReadyForEveningSchoolTrips_' . $value2);
				$redis->del('H_Poi_' . $value2 . '_' . $fcode);
				$redis->del('S_Poi_' . $value2 . '_' . $fcode);
				$redis->del('S_RouteList_' . $value2 . '_' . $fcode);
			}
			$users = $redis->smembers('S_Users_Dealer_' . $dealerID . '_' . $fcode);
			foreach ($users as $key => $value3) {
				$redis->del($value3);
				$redis->srem('S_Users_Dealer_' . $dealerID . '_' . $fcode, $value3);
				$redis->hdel('H_UserSmsInfo_' . $fcode, $value3);
				$redis->srem('S_Users_Virtual_' . $fcode, $value3);
				$redis->srem('S_Users_AssetTracker_' . $fcode, $value3);
				$redis->del('S_Orgs_' . $value3 . '_' . $fcode);
				$redis->hdel('H_Notification_Map_User', $value3);
				//$redis->sadd('S_Users_Admin_'.$fcode,$value3);
				$redis->hdel('H_UserId_Cust_Map', $value3 . ':fcode', $value3 . ':mobileNo', $value3 . ':email', $value3 . ':password');

				DB::table('users')->where('username', $value3)->delete();
			}
			$redis->del('S_Pre_Onboard_Dealer_' . $dealerID . '_' . $fcode);
			$redis->del('H_Pre_Onboard_Dealer_' . $dealerID . '_' . $fcode);
			$redis->del('H_VehicleName_Mobile_Dealer_' . $dealerID . '_Org_' . $fcode);
			$redis->hdel('H_Google_Android_ServerKey', $fcode . ':' . $dealerID);


			$redis->srem('S_Dealers_' . $fcode, $dealerID);
		}
		$redis->del('S_Dealers_' . $fcode);

		//vehicle->device map key Delete
		$redis->del('H_Vehicle_Device_Map_' . $fcode);

		$redis->del('S_Users_Virtual_' . $fcode);

		$redis->del('S_Users_AssetTracker_' . $fcode);

		//ORG LIST
		$orgList = $redis->smembers('S_Organisations_' . $fcode);
		foreach ($orgList as $key => $orgName) {
			$redis->del('S_Vehicles_' . $orgName . '_' . $fcode);
			$redis->del('S_Organisation_Route_' . $orgName . '_' . $fcode);
		}
		$orgAdminList = $redis->smembers('S_Organisations_Admin_' . $fcode);
		foreach ($orgAdminList as $key => $orgAdminName) {
			$redis->del('S_Vehicles_' . $orgAdminName . '_' . $fcode);
			$redis->del('S_Organisation_Route_' . $orgAdminName . '_' . $fcode);

			$redis->hdel('H_Org_Company_Map', $orgAdminName);
		}
		$redis->del('H_Scheduler_' . $fcode);
		$redis->del('H_Organisations_' . $fcode);
		$setkeys = $redis->keys('S_*_' . $fcode);
		foreach ($setkeys as $key => $value) {
			$redis->del($value);
		}
		$hashkeys = $redis->keys('H_*_' . $fcode);
		foreach ($hashkeys as $key => $value) {
			$redis->del($value);
		}
		$zkeys = $redis->keys('Z_*_' . $fcode);
		foreach ($zkeys as $key => $value) {
			$redis->del($value);
		}
		$lkeys = $redis->keys('L_*_' . $fcode);
		foreach ($lkeys as $key => $value) {
			$redis->del($value);
		}
		$gkeys = $redis->keys('*:' . $fcode);
		foreach ($gkeys as $key => $value) {
			$redis->del($value);
		}

		$tempKey = $redis->keys('Temp_Towing_*' . $fcode);
		foreach ($tempKey as $key => $value) {
			$redis->del($value);
		}

		$Evekey = $redis->keys('K_Evening_StopSeq_*' . $fcode);
		foreach ($Evekey as $key => $value) {
			$redis->del($value);
		}
		$Morkey = $redis->keys('K_Morning_StopSeq_*' . $fcode);
		foreach ($Morkey as $key => $value) {
			$redis->del($value);
		}
		$FuelKey = $redis->keys('LHistForFuel_*' . $fcode);
		foreach ($FuelKey as $key => $value) {
			$redis->del($value);
		}



		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);

		$userId = $franchiseDetails['userId'];
		$fname = $franchiseDetails['fname'];
		$email1 = $franchiseDetails['email1'];


		$fDetails    =   $redis->hget('H_Franchise', $fcode);
		$redis->del('back_H_RefData_' . $fcode);
		$redis->hdel('H_Franchise_LatLong', $fcode);

		$redis->hdel('H_Franchise', $fcode);




		$redis->srem('S_Franchises', $fcode);

		$redis->srem('S_Users_' . $fcode, $userId);
		$redis->hdel('H_UserId_Cust_Map', $userId . ':fcode', $userId . ':mobileNo', $userId . ':email');

		try {
			$status = array(
				'userIpAddress' => session()->get('userIP'),
				'userName'=> $userId,
				'type'=>config()->get('constant.admin'),
				'name'=>$userId,
				'status'=>config()->get('constant.deleted'),
				'oldData'=>$fDetails,
				'newData'=>json_encode([]),
			);
			AuditNewController::updateAudit($status);
		} catch (Exception $e) {
			logger('Error inside Audit_Frans Delete' . $e->getMessage());
		}


		DB::table('users')->where('username', $userId)->delete();

		$vamosid = 'vamos' . $fcode;


		$redis->srem('S_Users_' . $fcode, $vamosid);
		$redis->hdel('H_UserId_Cust_Map', $vamosid . ':fcode');
		$servername = $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
		$redis->hdel('H_Franchise_Mysql_DatabaseIP', $fcode);
		$delnotify = $redis->hdel('H_Google_Android_ServerKey', $fcode);
		$delappkey = $redis->hdel('H_MobileAppId', $fcode);

		///***********MYSQL********************
		//$servername = "209.97.163.4";    		
		if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
			return 'Ipaddress Failed !!!';
		}
		$usernamedb  =  "root";
		$password    =  "#vamo123";
		$dbname      =  $fcode;
		$conn  =  mysqli_connect($servername, $usernamedb, $password);
		$drp = "DROP DATABASE `$dbname`";
		if ($conn->query($drp) == TRUE) {
			logger('Sucessfully dropped database' . $dbname . '!');
		} else {
			logger('Error dropping database: ' . $conn->error);
		}
		$conn->close();


		session()->put('email1', $email1);

		// Mail::queue('emails.welcome', array('fname' => $fname, 'userId' => $userId), function ($message) {
		// 	$message->to(session()->pull('email1'))->subject('User Id deleted');
		// });

		return redirect()->to('vdmFranchises')->with([
			'message' => "Successfully deleted fname: $fcode !"
		]);
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */



	public function reports($id)
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$totalReportList = array();
		$list = array();
		$totalList = array();
		$reportsList = array();
		$report = null;
		$username = auth()->id();
		$fcode = $id;
		$redis = Redis::connection();
		$totalReport = null;
		$totalReport = $redis->smembers("S_TotalReports");
		// sort($totalReport);
		foreach ($totalReport as $key => $getReport) {
			// logger($getReport);
			$specReports = $redis->smembers($getReport);
			$reportType = explode("_", $getReport);
			foreach ($specReports as $key => $ReportName) {
				$report[] = $ReportName . ':' . head($reportType);
			}
			$reportsList[] = $getReport;
			$totalReportList[] = $report;
			$totalList[$getReport] = $report;
			$report = null;
		}
		$userReports = $redis->smembers("S_Users_Reports_Admin_" . $fcode);
		$apnKey = $redis->exists('Update:Apn');
		$timezoneKey = $redis->exists('Update:Timezone');
		// return $totalList;
		return view('vdm.franchise.reports', array(
			'fcode' => $fcode,
			'apnKey' => $apnKey,
			'timezoneKey' => $timezoneKey,
			'reportsList' => $reportsList,
			'totalReportList' => $totalReportList,
			'totalList' => $totalList,
			'userReports' => $userReports
		));
	}


	public function updateReports()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$fcode = request()->get('fcode');
		$username = auth()->id();
		$reportName = request()->get('reportName');
		$redis = Redis::connection();

		if ($reportName != null) {
			$prevReportList = $redis->smembers('S_Users_Reports_Admin_' . $fcode);
			$redis->del('S_Users_Reports_Admin_' . $fcode);
			foreach ($reportName as $key => $value) {
				$redis->sadd('S_Users_Reports_Admin_' . $fcode, $value);
			}
			// $defaultReports = $redis->smembers('S_Default_ReportList');
			// foreach ($defaultReports as $key => $value) {
			// 	$redis->sadd('S_Users_Reports_Admin_' . $fcode, $value);
			// }
			$removeList = '';
			$currentReportList = $redis->smembers('S_Users_Reports_Admin_' . $fcode);
			foreach ($prevReportList as $key => $value) {
				if (in_array($value, $currentReportList)) {
					logger("GotErix");
				} else {
					$removeList = $value . ',' . $removeList;
				}
			}
			$addedList = '';
			$currentReportList = $redis->smembers('S_Users_Reports_Admin_' . $fcode);
			foreach ($currentReportList as $key => $value) {
				if (!in_array($value, $prevReportList)) {
					$addedList = $value . ',' . $addedList;
				}
			}
			$parameters = 'fcode=' . $fcode . '&removeList=' . $removeList;

			$availableReports = '';
			foreach ($reportName as $key => $value) {
				$availableReports = $availableReports . ',' . $value;
			}
			try {
				$franDetails_json = $redis->hget('H_Franchise', $fcode);
				$franDetails = json_decode($franDetails_json, true);
				$userId = $franDetails['userId'] ?? $fcode;
				$status = array(
					'userIpAddress' => session()->get('userIP'),
					'userName'=> $userId,
					'type'=>config()->get('constant.admin'),
					'name'=>$userId,
					'status'=>config()->get('constant.reportsEdited'),
					'oldData'=>json_encode(['removedReports' => $removeList,]),
					'newData'=>json_encode(['addedReports' => $addedList,'reports' => $availableReports]),
				);
				AuditNewController::updateAudit($status);
			} catch (Exception $e) {
				logger('Error inside Audit_Frans Create' . $e->getMessage());
			}
		} else {
			$redis->del('S_Users_Reports_Admin_' . $fcode);
			$defaultReports = $redis->smembers('S_Default_ReportList');
			foreach ($defaultReports as $key => $value) {
				$redis->sadd('S_Users_Reports_Admin_' . $fcode, $value);
			}
		}

		return redirect()->to('vdmFranchises')->with(['message' => 'Updated Successfully']);
	}

	public function disable($id)
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();

		$fcode = $id;
		$redis = Redis::connection();

		$re = $redis->sadd('S_Franchises_Disable', $fcode);
		if ($re == 1) {
			$franDetails_json = $redis->hget('H_Franchise', $fcode);
			$franDetails = json_decode($franDetails_json, true);
			$fname = $franDetails['fname'];
			$userId = $franDetails['userId'];
			try {
				$status = array();
				$status = array(
					'userIpAddress' => session()->get('userIP'),
					'userName'=> $userId,
					'type'=> config()->get('constant.admin'),
					'name'=>$userId,
					'status'=>config()->get('constant.disabled'),
					'oldData'=>json_encode(['enable'=>"true"]),
					'newData'=>json_encode(['enable'=>"false"]),
				);
				AuditNewController::updateAudit($status);

			} catch (Exception $e) {
				logger('Error inside Audit_Frans Disable' . $e->getMessage());
			}
			return redirect()->to('vdmFranchises')->with([
				'message' => "$fname Franchise has been disabled successfully !"
			]);
		} else {

			$franDetails_json = $redis->hget('H_Franchise', $fcode);
			$franDetails = json_decode($franDetails_json, true);
			$fname = $franDetails['fname'];

			session()->flash('message', $fname . ' Franchise has been Already Disabled !');
			return redirect()->to('vdmFranchises')->with([
				'message' => "$fname Franchise has been Already Disabled !"
			]);
		}
	}
	public function Enable($id)
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();

		$fcode = $id;
		$redis = Redis::connection();

		$re = $redis->srem('S_Franchises_Disable', $fcode);
		if ($re == 1) {
			$franDetails_json = $redis->hget('H_Franchise', $fcode);
			$franDetails = json_decode($franDetails_json, true);
			$fname = $franDetails['fname'];
			$userId = $franDetails['userId'];
			try {
				$mysqlDetails = array();

				$status = array(
					'userIpAddress' => session()->get('userIP'),
					'userName'=> $userId,
					'type'=> config()->get('constant.admin'),
					'name'=>$userId,
					'status'=>config()->get('constant.enabled'),
					'oldData'=>json_encode(['enable'=>"false"]),
					'newData'=>json_encode(['enable'=>"true"]),
				);
				AuditNewController::updateAudit($status);

			} catch (Exception $e) {
				logger('Error inside Audit_Frans Enable' . $e->getMessage());
			}
			logger('after audit entry for Franchise Disable');
			return redirect()->to('vdmFranchises')->with([
				'message' => "$fname Franchise has been enabled successfully !"
			]);
		} else {
			$franDetails_json = $redis->hget('H_Franchise', $fcode);
			$franDetails = json_decode($franDetails_json, true);
			$fname = $franDetails['fname'];

			return redirect()->to('vdmFranchises')->with([
				'message' => "$fname Franchise has been already Enabled !"
			]);
		}
	}
	public function loadRemove($id)
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$redis->srem('Sensor_IndivdiualReportsList', 'FUEL_MACHINE');
		$redis->sadd('Sensor_IndivdiualReportsList', 'FUEL_ANALYTICS');
		$fcode = $id;
		$chk = $redis->sismember('S_Users_Reports_Admin_' . $fcode, 'FUEL_MACHINE:Sensor');
		if ($chk == 1) {
			$redis->srem('S_Users_Reports_Admin_' . $fcode, 'FUEL_MACHINE:Sensor');
			$redis->sadd('S_Users_Reports_Admin_' . $fcode, 'FUEL_ANALYTICS:Sensor');
		}
		$usr = $redis->smembers('S_Users_' . $fcode);
		foreach ($usr as $key => $username1) {
			$chk1 = $redis->sismember('S_Users_Reports_' . $username1 . '_' . $fcode, 'FUEL_MACHINE:Sensor');
			if ($chk1 == 1) {
				$redis->srem('S_Users_Reports_' . $username1 . '_' . $fcode, 'FUEL_MACHINE:Sensor');
				$redis->srem('S_Users_Reports_' . $username1 . '_' . $fcode, 'FUEL_ANALYTICS:Sensor');
			}
		}

		$dealerLi = $redis->smembers('S_Dealers_' . $fcode);
		foreach ($dealerLi as $key => $dealerID) {
			$users = $redis->smembers('S_Users_Dealer_' . $dealerID . '_' . $fcode);
			foreach ($users as $key => $username) {
				$chk2 = $redis->sismember('S_Users_Reports_' . $username . '_' . $fcode, 'FUEL_MACHINE:Sensor');
				if ($chk2 == 1) {
					$redis->srem('S_Users_Reports_' . $username . '_' . $fcode, 'FUEL_MACHINE:Sensor');
					$redis->sadd('S_Users_Reports_' . $username . '_' . $fcode, 'FUEL_ANALYTICS:Sensor');
				}
			}
			$chk3 = $redis->sismember('S_Users_Reports_Dealer_' . $dealerID . '_' . $fcode, 'FUEL_MACHINE:Sensor');
			if ($chk3 == 1) {
				$redis->srem('S_Users_Reports_Dealer_' . $dealerID . '_' . $fcode, 'FUEL_MACHINE:Sensor');
				$redis->sadd('S_Users_Reports_Dealer_' . $dealerID . '_' . $fcode, 'FUEL_ANALYTICS:Sensor');
			}
		}
		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		$fname = $franchiseDetails['fname'];

		return redirect()->to('vdmFranchises')->with([
			'message' => "$fname Franchise has Fuel Machine renamed as Fuel Analitical !"
		]);
	}

	public static function geocode($address)
	{
		// url encode the address
		try {
			$address = urlencode($address);
			// google map geocode api url
			$url = 'https://maps.google.com/maps/api/geocode/json?address=' . $address . '&key=AIzaSyDYlJJZZPkYLGKL1KbLGSETApseUTUiF3w';
			// get the json response
			$resp_json = file_get_contents($url);
			// decode the json
			$resp = json_decode($resp_json, true);
			// response status will be 'OK', if able to geocode given address 
			if ($resp['status'] == 'OK') {
				// get the important data
				$lati = $resp['results'][0]['geometry']['location']['lat'];
				$longi = $resp['results'][0]['geometry']['location']['lng'];
				$formatted_address = $resp['results'][0]['formatted_address'];
				// verify if data is complete
				if ($lati && $longi && $formatted_address) {
					// put the data in the array
					$data_arr = array();
					array_push(
						$data_arr,
						$lati,
						$longi,
						$formatted_address
					);
					return $data_arr;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (\Exception $e) {
			return false;
		}
	}


	public function fransOnboard()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$redis = Redis::connection();
		$fcodeList = array('' => '-- Select --');
		$fcodeList1 = $redis->smembers('S_Franchises');
		foreach ($fcodeList1 as $key => $fcode) {
			$fcodeList = Arr::add($fcodeList, $fcode, $fcode);
		}
		$apnKey = $redis->exists('Update:Apn');
		$timezoneKey = $redis->exists('Update:Timezone');
		$count = VdmFranchiseController::liveVehicleCountV2();
		return view('vdm.franchise.fransOnboard')->with('fcodeList', $fcodeList)->with('apnKey', $apnKey)->with('timezoneKey', $timezoneKey)->with('count', $count);
	}
	public function fransOnUpdate()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$redis = Redis::connection();
		$rules = array(
			'fcodeList' => 'required',
			'frmDate' => 'required'
		);
		$validator = validator()->make(request()->all(), $rules);
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator);
		}
		$fcode = request()->get('fcodeList');
		//$fcode = 'VAM';
		$fromDate = request()->get('frmDate');
		//$toDate=request()->get('toDate');
		// $file = Excel::create('Onboarded List ' . $fcode, function ($excel) use ($redis, $fcode, $fromDate) {

		// 	$franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
		// 	$servername = $franchiesJson;
		// 	//$servername="209.97.163.4";
		// 	if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
		// 		return 'Ipaddress Failed !!!';
		// 	}
		// 	$usernamedb = "root";
		// 	$password = "#vamo123";
		// 	$dbname = $fcode;
		// 	//logger('franci. ---- '.$fcode.' ip---- '.$servername);
		// 	$conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
		// 	if (!$conn) {
		// 		die('Could not connect: ' . mysqli_connect_error());
		// 		return 'Please Update One more time Connection failed';
		// 	} else {
		// 		$fromDate = date('Y-m-d', strtotime($fromDate));
		// 		//$toDate   = date('Y-m-d', strtotime($toDate));
		// 		logger(' created connection ');
		// 		$qry = "select * from communicating_Devices where lastUpdated between '" . $fromDate . " 00:00:00' and '" . $fromDate . " 23:59:59'";
		// 		logger($qry);
		// 		$results = mysqli_query($conn, $qry);
		// 		$conn->close();
		// 	}

		// 	$excel->sheet('Onboarded List ' . $fcode, function ($sheet)  use ($redis, $results) {
		// 		$communicating = 0;
		// 		$nonCommunicating = 0;
		// 		$k = 5;
		// 		$sheet->row(5, ['VehicleId', 'DelearName', 'IMEI', 'OnboardedDate', 'VehicleName', 'GPSSimNo', 'Communicating PortNo', 'LastCommunicated Date', 'Last communicated (In Days)', '30 days before Onboarded', ' Expiry Date ']);
		// 		$sheet->getStyle('A5:U5')->getAlignment()->applyFromArray(
		// 			array('horizontal' => 'center', 'text-transform' => 'uppercase')
		// 		);
		// 		$sheet->cells('A5:U5', function ($cells) {
		// 			$cells->setBackground(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
		// 			$cells->setFontColor('#ffffff');
		// 		});
		// 		$sheet->cells('A1:C3', function ($cells) {
		// 			$cells->setFontColor(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKBLUE);
		// 		});
		// 		if ($results != null) {
		// 			while ($row = mysqli_fetch_array($results)) {
		// 				if (($row['communicated'] < 30) && ($row['status'] != 'N')) {
		// 					$communicating = $communicating + 1;
		// 				} else {
		// 					$nonCommunicating = $nonCommunicating + 1;
		// 				}
		// 				$k++;
		// 				$sheet->row($k, [$row['vehicleId'], $row['dealerName'], $row['deviceId'], $row['onboardedDate'], $row['vehicleName'], $row['gpsSimNo'], $row['communicatingPort'], $row['lastCommunicated'], $row['communicated'], $row['30DaysBeforeOnboarded'], $row['expiryDatelastUpdated']]);
		// 			}
		// 			$total = $communicating + $nonCommunicating;
		// 			$sheet->mergeCells('A1:C1');
		// 			$sheet->mergeCells('A2:C2');
		// 			$sheet->mergeCells('A3:C3');
		// 			$sheet->row(1, ['TOTAL DEVICES  :', '', '', $total]);
		// 			$sheet->row(2, ['CURRENT COMMUNICATING DEVICES  :', '', '', $communicating]);
		// 			$sheet->row(3, ['NON COMMUNICATED DEVICES FOR LATEST 30 DAYS :', '', '', $nonCommunicating]);
		// 		} else {
		// 			logger('No data found for communicating device download');
		// 		}
		// 	});
		// })->download('xls');
		return redirect()->back();
	}

	public function premiumPlusIndexset()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$fcodeArray = $redis->smembers('S_Franchises');
		foreach ($fcodeArray as $key => $fcode) {
			$details = $redis->hget('H_Franchise', $fcode);
			$details_json = json_decode($details, true);
			$isPrePaid1 = ucfirst(isset($details_json['prepaid']) ? $details_json['prepaid'] : 'No');
			if ($isPrePaid1 == "Yes") {
				$dealerList = $redis->smembers('S_Dealers_' . $fcode);
				foreach ($dealerList as $key => $dealerId) {
					$dealerData  = $redis->hget('H_DealerDetails_' . $fcode, $dealerId);
					$detail = json_decode($dealerData, true);

					$details = array(
						'email'	=> isset($detail['email']) ? $detail['email'] : '0',
						'mobileNo' => isset($detail['mobileNo']) ? $detail['mobileNo'] : '0',
						'zoho'   => isset($detail['zoho']) ? $detail['zoho'] : '0',
						'mapKey' => isset($detail['mapKey']) ? $detail['mapKey'] : '0',
						'addressKey' => isset($detail['addressKey']) ? $detail['addressKey'] : '0',
						'notificationKey' => isset($detail['notificationKey']) ? $detail['notificationKey'] : '0',
						'gpsvtsApp' => isset($detail['gpsvtsApp']) ? $detail['gpsvtsApp'] : '0',
						'website' => isset($detail['website']) ? $detail['website'] : '0',
						'smsSender' => isset($detail['smsSender']) ? $detail['smsSender'] : '0',
						'smsProvider' => isset($detail['smsProvider']) ? $detail['smsProvider'] : '0',
						'providerUserName' => isset($detail['providerUserName']) ? $detail['providerUserName'] : '0',
						'providerPassword' => isset($detail['providerPassword']) ? $detail['providerPassword'] : '0',
						'numofBasic'	=> isset($detail['numofBasic']) ? $detail['numofBasic'] : '0',
						'numofAdvance'	=> isset($detail['numofAdvance']) ? $detail['numofAdvance'] : '0',
						'numofPremium'	=> isset($detail['numofPremium']) ? $detail['numofPremium'] : '0',
						'numofPremiumPlus'	=> isset($detail['numofPremiumPlus']) ? $detail['numofPremiumPlus'] : '0',
						'avlofBasic'	=> isset($detail['avlofBasic']) ? $detail['avlofBasic'] : '0',
						'avlofAdvance'	=> isset($detail['avlofAdvance']) ? $detail['avlofAdvance'] : '0',
						'avlofPremium'	=> isset($detail['avlofPremium']) ? $detail['avlofPremium'] : '0',
						'avlofPremiumPlus'	=> isset($detail['avlofPremiumPlus']) ? $detail['avlofPremiumPlus'] : '0',
						'LicenceissuedDate' => isset($detail['LicenceissuedDate']) ? $detail['LicenceissuedDate'] : '0',
					);
					$detailJson = json_encode($details);
					$redis->hset('H_DealerDetails_' . $fcode, $dealerId, $detailJson);
				}
			}
		}
		session()->flash('message', 'Franchise LicenceId Type successfully modified !');
		return redirect()->to('vdmFranchises');
	}


	public function auditShow($model)
	{

		try {
			$username = auth()->id();
			$redis = Redis::connection();
			$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
			$modelname = new $model();
			$table = $modelname->getTable();

			if ($fcode == 'vamos') $fcode = config()->get('constant.VAMOSdb');
			AuditTables::ChangeDB($fcode);

			$tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $fcode)->where('table_name', '=', $table)->get();

			if (count($tableExist) > 0 && ($model::first() != null)) {
				$columns = array_keys($model::first()->toArray());
				$rows = $model::all();
			} else {

				$columns = [];
				$rows = [];
			}
			AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
		} catch (customException $e) {
			//display custom message
			logger($e->errorMessage());
		}
		unset($columns[array_search('id', $columns)], $columns[array_search('fcode', $columns)]);

		$userName = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$details = $redis->hget('H_Franchise', $userName);
		$details_json = json_decode($details, true);
		$isPrePaid1 = ucfirst(isset($details_json['prepaid']) ? $details_json['prepaid'] : 'No');
		if ($isPrePaid1 == "Yes") {
			$isPrePaid = "Prepaid";
		} else {
			$isPrePaid = "Normal";
		}


		return view('vdm.franchise.audit', array('columns' => $columns, 'rows' => $rows));
	}
	public function dateModification1()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$fcodeArray = $redis->smembers('S_Franchises');
		foreach ($fcodeArray as $key => $fcode) {
			$franDetails_json = $redis->hget('H_Franchise', $fcode);
			$franchiseDetails = json_decode($franDetails_json, true);
			$prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
			if ($prepaid == 'yes') {
				$licenceList = $redis->hkeys('H_LicenceExpiry_' . $fcode);
				foreach ($licenceList as $key => $LicenceId) {
					$LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
					$LicenceRefData = json_decode($LicenceRef, true);

					$LicenceissuedDate = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
					$LicenceOnboardDate = isset($LicenceRefData['LicenceOnboardDate']) ? $LicenceRefData['LicenceOnboardDate'] : '';
					$LicenceExpiryDate = date("d-m-Y", strtotime(date("d-m-Y", strtotime($LicenceissuedDate)) . " + 1 year"));
					$vehicleExpiry = date("Y-m-d", strtotime(date("Y-m-d", strtotime($LicenceissuedDate)) . " + 1 year"));
					$LiceneceDataArr = array(
						'LicenceissuedDate' => $LicenceissuedDate,
						'LicenceOnboardDate' => $LicenceOnboardDate,
						'LicenceExpiryDate' => $LicenceExpiryDate,
					);
					$LicenceExpiryJson = json_encode($LiceneceDataArr);

					$redis->hmset('H_LicenceExpiry_' . $fcode, $LicenceId, $LicenceExpiryJson);
					$vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId);
					$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
					$vehicleRefData = json_decode($vehicleRefData, true);

					$vehicleRefData[""] = $vehicleExpiry;
					$refDataJson = json_encode($vehicleRefData);

					$redis->hmset('H_RefData_' . $fcode, $vehicleId, $refDataJson);
				}
			}
		}

		session()->flash('message', ' successfully modified !!!');
		return redirect()->to('vdmFranchises');
	}
	public function liveVehicleCount()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$redis = Redis::connection();
		$count = VdmFranchiseController::liveVehicleCountV2();
		return redirect()->to('vdmFranchises');
	}

	public function enableVehicleExpiry($fcode, $option)
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$redis = Redis::connection();
		$username = auth()->id();
		$msg = 'not successfully modified';
		if ($option == 'no') {
			$redis->sadd('S_EnableVehicleExpiry', $fcode);
			$msg = 'successfully enabled';
		} elseif ($option == 'yes') {
			$redis->srem('S_EnableVehicleExpiry', $fcode);
			$msg = 'successfully disabled';
		}
		session()->flash('message', $msg);
		return redirect()->to('vdmFranchises');
	}

	public function updatetimezone()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$redis = Redis::connection();
		$username = auth()->id();
		$timezonekeys = $redis->keys('S_Timezone_Updated_*');
		foreach ($timezonekeys as $key => $value) {
			$redis->del($value);
		}
		$validity = 1 * 86400;
		$redis->set('Update:Timezone', '24hrs');
		$redis->expire('Update:Timezone', $validity);

		session()->flash('message', 'Timezone updated Successfully');
		return redirect()->to('vdmFranchises');
	}

	public function updateapn()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$redis = Redis::connection();
		$username = auth()->id();
		$apnkeys = $redis->keys('S_Apn_Updated_*');
		foreach ($apnkeys as $key => $value) {
			$redis->del($value);
		}
		$validity = 1 * 86400;
		$redis->set('Update:Apn', '24hrs');
		$redis->expire('Update:Apn', $validity);

		session()->flash('message', 'APN Updated successfully');
		return redirect()->to('vdmFranchises');
	}

	public static function liveVehicleCountV2()
	{
		$redis = Redis::connection();
		$slave = $redis->hget('H_Redis_Slave', 'Slave1');
		$redis1 = new Client(array(
			'host'     => $slave,
			'password' => 'ahanram@vamosystems.in',
			'database' => 0,
		));
		$count = count($redis1->keys('NoData:*'));
		$redis1->quit();
		return $count;
	}

	public function dateModification()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$fcodeArray = $redis->smembers('S_Franchises');
		$updatefile = Excel::create('VehicleExpiry', function ($excel) use ($redis, $fcodeArray) {
			$excel->sheet('Sheetname', function ($sheet)  use ($redis, $fcodeArray) {
				$j = 2;
				$sheet->row(1, array('VehicleId', 'LicenceId', 'licenceissue', 'VehicleExp', 'licenceExp', 'lastcom', 'lastloc', 'OWN', 'fcode'));
				foreach ($fcodeArray as $key => $fcode) {
					$franDetails_json = $redis->hget('H_Franchise', $fcode);
					$franchiseDetails = json_decode($franDetails_json, true);
					$prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
					if ($prepaid == 'yes') {
						$licenceList = $redis->hkeys('H_LicenceExpiry_' . $fcode);
						foreach ($licenceList as $key => $LicenceId) {
							$LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
							$LicenceRefData = json_decode($LicenceRef, true);
							$LicenceissuedDate = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
							$LicenceExpiryDate = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
							$vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId);
							$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
							$vehicleRefData = json_decode($vehicleRefData, true);
							$vehExpiry = isset($vehicleRefData['vehicleExpiry']) ? $vehicleRefData['vehicleExpiry'] : '';
							$OWN = isset($vehicleRefData['OWN']) ? $vehicleRefData['OWN'] : '';
							if ($vehExpiry == null || $vehExpiry == 'null' || $vehExpiry == '') {
								$LicenceExpiryDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($LicenceExpiryDate))));
								$vehicleRefData["vehicleExpiry"] = $LicenceExpiryDate;
								$refDataJson = json_encode($vehicleRefData);
								$redis->hmset('H_RefData_' . $fcode, $vehicleId, $refDataJson);
								$statusVehicle = $redis->hget('H_ProData_' . $fcode, $vehicleId);
								$statusSeperate = explode(',', $statusVehicle);
								$lastComm = isset($statusSeperate[36]) ? $statusSeperate[36] : '';
								$lastLoc = isset($statusSeperate[4]) ? $statusSeperate[4] : '';
								if ($lastComm == '' || $lastComm == null) {
									$setted = '';
								} else {
									$sec = $lastComm / 1000;
									$datt = date("Y-m-d", $sec);
									$time1 = date("H:i:s", $sec);
									$setted = $datt . ' ' . $time1;
								}
								if ($lastLoc == '' || $lastLoc == null) {
									$setlastLoc = '';
								} else {
									$sec1 = $lastLoc / 1000;
									$datt1 = date("Y-m-d", $sec1);
									$time11 = date("H:i:s", $sec1);
									$setlastLoc = $datt1 . ' ' . $time11;
								}
								//EXCEL
								$sheet->row($j, array($vehicleId, $LicenceId, $LicenceissuedDate, $vehExpiry, $LicenceExpiryDate, $setted, $setlastLoc, $OWN, $fcode));
								$j++;
							}
						}
					}
				}
			});
		})->download('xls');
		session()->flash('message', ' successfully modified !!!');
		return redirect()->to('vdmFranchises');
	}

	public function getLicenceCount()
	{
		return Excel::download(new LicenceListExport(), 'Licence_List.xlsx');

		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$fcodeArray = $redis->smembers('S_Franchises');
		$fcodeArray = ["VAM"];


		$updatefile = Excel::create('Licence List', function ($excel) use ($redis, $fcodeArray) {
			$excel->sheet('Sheetname', function ($sheet) use ($redis, $fcodeArray) {

				$sheet->row(1, array('Dealer Name', 'Total Licence ', 'Available', 'Licence count', 'Renewal', 'Actual Total', 'Need to add'));
				foreach ($fcodeArray as $key => $fcode) {
					$dealerList = $redis->smembers('S_Dealers_' . $fcode);
					$j = 2;
					$emptyRef = array();
					foreach ($dealerList as $key => $dealer) {
						$dealersDetails = $redis->hget('H_DealerDetails_' . $fcode, $dealer);
						$dealerDetail = json_decode($dealersDetails, true);
						$LicenceList = $redis->smembers('S_LicenceList_dealer_' . $dealer . '_' . $fcode);
						$licCount = 0;
						foreach ($LicenceList as $key => $LicenceId) {
							$vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId);
							$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
							$vehicleRefData = json_decode($vehicleRefData, true);
							if (isset($vehicleRefData)) {
								$emptyRef = Arr::add($emptyRef, $LicenceId, $vehicleId);
							} else {
								continue;
							}
							$type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
							if ($type == "Basic") {
								$licCount++;
							}
						}
						$numofBasic = isset($dealerDetail['numofBasic']) ? $dealerDetail['numofBasic'] : '0';
						$avlofBasic = isset($dealerDetail['avlofBasic']) ? $dealerDetail['avlofBasic'] : '0';
						// get data from Mysql
						$franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
						$servername = $franchiesJson;
						//$servername = "209.97.163.4";
						if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
							return 'Ipaddress Failed !!!';
						}
						$usernamedb = "root";
						$password = "#vamo123";
						$dbname = $fcode;
						$conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
						if (!$conn) {
							die('Could not connect: ' . mysqli_connect_error());
							return 'Please Update One more time Connection failed';
						} else {
							$renewalCountQ = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Basic' AND User_Name ='$dealer' ";
							$results = mysqli_query($conn, $renewalCountQ);
							while ($row = mysqli_fetch_array($results)) {
								$renewalcount = $row[0];
							}
						}
						$conn->close();
						$actualCount = $licCount + $renewalcount;
						$sheet->row($j, array($dealer, $numofBasic, $avlofBasic, $licCount, $renewalcount, $actualCount, ($numofBasic - $actualCount)));
						$j++;
					}
				}
			});
		})->download('xls');
		session()->flash('message', ' Licence List Downloaded !!!');
		return redirect()->to('vdmFranchises');
		//return "hi Senthamizh";
	}

	public function getDealerList($fcode)
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = request()->get('dealerId');
		$current = Carbon::now();
		session()->forget('page');
		session()->put('vCol', 1);
		$redis = Redis::connection();
		//$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$LicenceList1 = 'S_LicenceList_dealer_' . $username . '_' . $fcode;
		$vehicleList = null;
		$emptyRef = null;
		$LtypeList = null;
		$deviceList = null;
		$deviceModelList = null;
		$licenceissuedList = null;
		$LicenceExpiryList = null;
		$LicenceOnboardList = null;
		$ExpiredLicenceList = null;
		$availableBasicLicence = null;
		$availableAdvanceLicence = null;
		$availablePremiumLicence = null;
		$availablePremPlusLicence = null;
		$dealerList = null;
		$wholeLicence = null;
		$renewedLicenceList = null;
		$dbValues = null;
		$loginas = 'admin';
		$franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
		if ($username != null && $username != '') {
			$qry = "select Licence_Id,Status,Vehicle_Id from Renewal_Details where User_Name='$username' and Status='Renew'";
			$LicenceList1 = 'S_LicenceList_dealer_' . $username . '_' . $fcode;
			$loginas = 'dealer';
		} else {
			$dealersDetails_json = $redis->hget('H_Franchise', $fcode);
			$dealersDetails = json_decode($dealersDetails_json, true);
			$username = isset($dealersDetails['userId']) ? $dealersDetails['userId'] : '';
			$qry = "select Licence_Id,Status,Vehicle_Id from Renewal_Details where User_Name='$username' and Status='Renew'";
			$LicenceList1 = 'S_LicenceList_admin_' . $fcode;
		}
		$servername = "209.97.163.4";
		// $servername     =  $franchiesJson;
		if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
			return 'Ipaddress Failed !!!';
		}
		$usernamedb = "root";
		$password = "#vamo123";
		$dbname = $fcode;
		$conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
		if (!$conn) {
			die('Could not connect: ' . mysqli_connect_error());
			return 'Please Update One more time Connection failed';
		} else {

			try {
				$results = mysqli_query($conn, $qry);
				if ($results->num_rows > 0) {
					$dbValues = (mysqli_fetch_all($results, MYSQLI_ASSOC));
				} else {
					logger("empty renewal data");
				}
			} catch (Exception $e) {
				logger('Exception Message: ' . $e->getMessage());
			}
			$conn->close();
		}
		if ($dbValues != null) {
			foreach ($dbValues as $key => $dbValue) {
				$vehicleId = $dbValue['Vehicle_Id'];
				$LicenceIdOld = $dbValue['Licence_Id'];
				$wholeLicence[] = $LicenceIdOld;
				$renewedLicenceList[] = $LicenceIdOld;
				$LicenceId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleId);
				$vehicleList = Arr::add($vehicleList, $LicenceIdOld, $vehicleId);
				$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
				$vehicleRefData = json_decode($vehicleRefData, true);
				if (isset($vehicleRefData)) {
					$emptyRef = Arr::add($emptyRef, $LicenceIdOld, $vehicleId);
				} else {
					continue;
				}
				$type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
				$LtypeList = Arr::add($LtypeList, $LicenceIdOld, $type);
				$deviceId = isset($vehicleRefData['deviceId']) ? $vehicleRefData['deviceId'] : '-';
				$deviceList = Arr::add($deviceList, $LicenceIdOld, $deviceId);

				//licence Red date
				$LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
				$LicenceRefData = json_decode($LicenceRef, true);
				$LicenceissuedDate = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
				$licenceissuedList = Arr::add($licenceissuedList, $LicenceIdOld, $LicenceissuedDate);
				$LicenceExpiryDate = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
				$LicenceExpiryList = Arr::add($LicenceExpiryList, $LicenceIdOld, $LicenceExpiryDate);
				$LicenceOnboardDate = isset($LicenceRefData['LicenceOnboardDate']) ? $LicenceRefData['LicenceOnboardDate'] : '';
				$LicenceOnboardList = Arr::add($LicenceOnboardList, $LicenceIdOld, $LicenceOnboardDate);
			}
		}

		$LicenceList = $redis->smembers($LicenceList1);
		// $LicenceList = array_slice($LicenceList, 0, 10);
		foreach ($LicenceList as $key => $LicenceId) {
			$wholeLicence[] = $LicenceId;
			$vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId);
			$vehicleList = Arr::add($vehicleList, $LicenceId, $vehicleId);
			//vehicle Ref data
			$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
			$vehicleRefData = json_decode($vehicleRefData, true);
			if (isset($vehicleRefData)) {
				$emptyRef = Arr::add($emptyRef, $LicenceId, $vehicleId);
			} else {
				continue;
			}
			$type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
			$LtypeList = Arr::add($LtypeList, $LicenceId, $type);
			$deviceId = isset($vehicleRefData['deviceId']) ? $vehicleRefData['deviceId'] : '-';
			$deviceList = Arr::add($deviceList, $LicenceId, $deviceId);
			//licence Red date
			$LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
			$LicenceRefData = json_decode($LicenceRef, true);
			$LicenceissuedDate = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
			$licenceissuedList = Arr::add($licenceissuedList, $LicenceId, $LicenceissuedDate);
			$LicenceExpiryDate = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
			$LicenceExpiryList = Arr::add($LicenceExpiryList, $LicenceId, $LicenceExpiryDate);
			$LicenceOnboardDate = isset($LicenceRefData['LicenceOnboardDate']) ? $LicenceRefData['LicenceOnboardDate'] : '';
			$LicenceOnboardList = Arr::add($LicenceOnboardList, $LicenceId, $LicenceOnboardDate);
			//licence Expiry Checking
			$expiry_date = $LicenceExpiryDate;
			$today = date('Y-m-d', time());
			$exp = date('Y-m-d', strtotime($expiry_date));
			$expDate =  date_create($exp);
			$todayDate = date_create($today);
			$diff =  date_diff($todayDate, $expDate);
			if ($diff->format("%R%a") >= 0) {
			} else {
				$redis->sadd('S_ExpiredLicence_Vehicle_' . $fcode, $LicenceId);
			}
		}
		$ExpiredLicenceList = $redis->smembers('S_ExpiredLicence_Vehicle_' . $fcode);
		if ($loginas == 'dealer') {
			$dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
			$dealersDetails = json_decode($dealersDetails_json, true);
			$noOfBasicLicence = isset($dealersDetails['numofBasic']) ? $dealersDetails['numofBasic'] : '0';
			$noOfAdvanceLicence = isset($dealersDetails['numofAdvance']) ? $dealersDetails['numofAdvance'] : '0';
			$numOfPremiumLicence = isset($dealersDetails['numofPremium']) ? $dealersDetails['numofPremium'] : '0';
			$numOfPremPlusLicence = isset($dealersDetails['numofPremiumPlus']) ? $dealersDetails['numofPremiumPlus'] : '0';
			$availableBasicLicence = isset($dealersDetails['avlofBasic']) ? $dealersDetails['avlofBasic'] : '0';
			$availableAdvanceLicence = isset($dealersDetails['avlofAdvance']) ? $dealersDetails['avlofAdvance'] : '0';
			$availablePremiumLicence = isset($dealersDetails['avlofPremium']) ? $dealersDetails['avlofPremium'] : '0';
			$availablePremPlusLicence = isset($dealersDetails['avlofPremiumPlus']) ? $dealersDetails['avlofPremiumPlus'] : '0';
		} else {

			$noOfBasicLicence = isset($dealersDetails['numberofBasicLicence']) ? $dealersDetails['numberofBasicLicence'] : '0';
			$noOfAdvanceLicence = isset($dealersDetails['numberofAdvanceLicence']) ? $dealersDetails['numberofAdvanceLicence'] : '0';
			$numOfPremiumLicence = isset($dealersDetails['numberofPremiumLicence']) ? $dealersDetails['numberofPremiumLicence'] : '0';
			$numOfPremPlusLicence = isset($dealersDetails['numberofPremPlusLicence']) ? $dealersDetails['numberofPremPlusLicence'] : '0';
			$availableBasicLicence = isset($dealersDetails['availableBasicLicence']) ? $dealersDetails['availableBasicLicence'] : '0';
			$availableAdvanceLicence = isset($dealersDetails['availableAdvanceLicence']) ? $dealersDetails['availableAdvanceLicence'] : '0';
			$availablePremiumLicence = isset($dealersDetails['availablePremiumLicence']) ? $dealersDetails['availablePremiumLicence'] : '0';
			$availablePremPlusLicence = isset($dealersDetails['availablePremPlusLicence']) ? $dealersDetails['availablePremPlusLicence'] : '0';
		}

		if ($availableBasicLicence == null) {
			$availableBasicLicence = 0;
		}
		if ($availableAdvanceLicence == null) {
			$availableAdvanceLicence = 0;
		}
		if ($availablePremiumLicence == null) {
			$availablePremiumLicence = 0;
		}
		if ($availablePremPlusLicence == null) {
			$availablePremPlusLicence = 0;
		}

		$dealerId = $redis->smembers('S_Dealers_' . $fcode);
		$dealerIdList = array('' => 'Select Dealer Name');
		foreach ($dealerId as $org) {
			$dealerIdList = Arr::add($dealerIdList, $org, $org);
		}
		$dealerId = $dealerIdList;
		if ($LicenceList = null) {
			sort($LicenceList, SORT_NATURAL | SORT_FLAG_CASE);
		}

		return view('vdm.franchise.licenceList', [
			'LicenceList' => $wholeLicence,
			'vehicleList' => $vehicleList,
			'LtypeList' => $LtypeList,
			'deviceList' => $deviceList,
			'deviceModelList' => $deviceModelList,
			'dealerList' => $dealerList,
			'licenceissuedList' => $licenceissuedList,
			'LicenceExpiryList' => $LicenceExpiryList,
			'LicenceOnboardList' => $LicenceOnboardList,
			'ExpiredLicenceList' => $ExpiredLicenceList,
			'availableBasicLicence' => $availableBasicLicence,
			'availableAdvanceLicence' => $availableAdvanceLicence,
			'availablePremiumLicence' => $availablePremiumLicence,
			'availablePremPlusLicence' => $availablePremPlusLicence,
			'dealerId' => $dealerId,
			'uname' => $username,
			'fcode' => $fcode,
			'noOfBasicLicence' => $noOfBasicLicence,
			'noOfAdvanceLicence' => $noOfAdvanceLicence,
			'numOfPremiumLicence' => $numOfPremiumLicence,
			'numOfPremPlusLicence' => $numOfPremPlusLicence,
			'renewedLicenceList' => $renewedLicenceList
		]);
	}

	public function licConvert()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();

		$redis = Redis::connection();

		$apnKey = $redis->exists('Update:Apn');
		$timezoneKey = $redis->exists('Update:Timezone');
		$count = VdmFranchiseController::liveVehicleCountV2();

		$licenceTypes = $redis->smembers('S_LicenceType');
		$licenceList = ['' => '-- select --'];
		sort($licenceTypes);
		foreach ($licenceTypes as $value) {
			$licenceList = Arr::add($licenceList, $value, $value);
		}

		$licConvertData = [
			'apnKey' => $apnKey,
			'timezoneKey' => $timezoneKey,
			'count' => $count,
			'licenceList' => $licenceList,
			'index' => 1
		];
		return view('vdm.franchise.licConvert', $licConvertData);
	}
	public function updateLicConvert()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$deviceId = request()->get('imei');
		$newLicType = request()->get('licType');
		$fcode = $redis->hget('H_Device_Cpy_Map', $deviceId);
		$vehicleId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $deviceId);
		if (is_null($fcode) || is_null($vehicleId) || $vehicleId == "") {
			return redirect()->back()->withInput()->withErrors('Enter valid IMEI/Device Id');
		}

		$LicenceList = 'S_LicenceList_admin_' . $fcode;
		$ownData = json_decode($redis->hget('H_Franchise', $fcode), true);
		$availableStarterLicence = isset($ownData['availableStarterLicence']) ? $ownData['availableStarterLicence'] : '0';
		$availableBasicLicence = isset($ownData['availableBasicLicence']) ? $ownData['availableBasicLicence'] : '0';
		$availableAdvanceLicence = isset($ownData['availableAdvanceLicence']) ? $ownData['availableAdvanceLicence'] : '0';
		$availablePremiumLicence = isset($ownData['availablePremiumLicence']) ? $ownData['availablePremiumLicence'] : '0';
		$availablePremiumPlusLicence = isset($ownData['availablePremPlusLicence']) ? $ownData['availablePremPlusLicence'] : '0';

		$isPrePaid = isset($ownData['prepaid']) ? $ownData['prepaid'] : 'no';
		if ($isPrePaid == 'no') {
			return redirect()->back()->withInput()->withErrors('Entered IMEI/Device is not Prepaid Licence');
		}
		$refData = json_decode($redis->hget("H_RefData_" . $fcode, $vehicleId), true);
		$own = isset($refData['OWN']) ? $refData['OWN'] : 'OWN';
		$oldLicType = isset($refData['Licence']) ? $refData['Licence'] : 'Basic';

		if ($oldLicType == $newLicType) {
			return redirect()->back()->withInput()->withErrors('Licence Type Should not be Same');
		}
		$oldIndex = 'available' . $oldLicType . 'Licence';
		$newIndex = 'available' . $newLicType . 'Licence';

		if ($own != 'OWN') {
			$ownData = json_decode($redis->hget('H_DealerDetails_' . $fcode, $own), true);
			$availableStarterLicence = isset($ownData['avlofStarter']) ? $ownData['avlofStarter'] : '0';
			$availableBasicLicence = isset($ownData['avlofBasic']) ? $ownData['avlofBasic'] : '0';
			$availableAdvanceLicence = isset($ownData['avlofAdvance']) ? $ownData['avlofAdvance'] : '0';
			$availablePremiumLicence = isset($ownData['avlofPremium']) ? $ownData['avlofPremium'] : '0';
			$availablePremiumPlusLicence = isset($ownData['avlofPremiumPlus']) ? $ownData['avlofPremiumPlus'] : '0';
			$LicenceList = 'S_LicenceList_dealer_' . $own . '_' . $fcode;
			$oldIndex = 'avlof' . $oldLicType;
			$newIndex = 'avlof' . $newLicType;
		}
		$newLicCount = 'available' . $newLicType . 'Licence';
		$oldLicCount =  'available' . $oldLicType . 'Licence';
		if ($$newLicCount  <= 0) {
			return redirect()->back()->withInput()->withErrors('Insufficient Licence');
		}

		if ($newLicType == 'Starter') {
			$newLic_Id = strtoupper('ST' . uniqid());
		} else if ($newLicType == 'Basic') {
			$newLic_Id = strtoupper('BC' . uniqid());
		} else if ($newLicType == 'Advance') {
			$newLic_Id = strtoupper('AV' . uniqid());
		} else if ($newLicType == 'Premium') {
			$newLic_Id = strtoupper('PM' . uniqid());
		} else if ($newLicType == 'PremiumPlus') {
			$newLic_Id = strtoupper('PL' . uniqid());
		}

		$oldLic_Id = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleId);
		$expData = $redis->hget('H_LicenceExpiry_' . $fcode, $oldLic_Id);

		try {

			$redis->srem($LicenceList, $oldLic_Id);
			$redis->sadd($LicenceList, $newLic_Id);
			$redis->hdel('H_LicenceExpiry_' . $fcode, $oldLic_Id);
			$redis->hset('H_LicenceExpiry_' . $fcode, $newLic_Id, $expData);

			$redis->hdel('H_Vehicle_LicenceId_Map_' . $fcode, $oldLic_Id);
			$redis->hset('H_Vehicle_LicenceId_Map_' . $fcode, $newLic_Id, $vehicleId);
			$ownData[$oldIndex] = (isset($ownData[$oldIndex]) ? $ownData[$oldIndex] : 0) + 1;
			$ownData[$newIndex] = (isset($ownData[$newIndex]) ? $ownData[$newIndex] : 0) - 1;
			if ($own == 'OWN') {
				$redis->hset('H_Franchise', $fcode, json_encode($ownData));
			} else {
				$redis->hset('H_DealerDetails_' . $fcode, $own, json_encode($ownData));
			}

			$refData['Licence'] = $newLicType;
			$redis->hset('H_RefData_' . $fcode, $vehicleId, json_encode($refData));

			$franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
			$servername = $franchiesJson;
			if (request()->server('HTTP_HOST') == "157.245.193.219" || request()->server('HTTP_HOST') === "localhost") {
				$servername = "209.97.163.4";
			}
			if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
				return 'Ipaddress Failed !!!';
			}
			try {
				$usernamedb = "root";
				$password = "#vamo123";
				$dbname = $fcode;
				$conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
				if (!$conn) {
					die('Could not connect: ' . mysqli_connect_error());
					return 'Please Update One more time Connection failed';
				} else {
					$LicenceIdmap = $oldLic_Id . ' -> ' . $newLic_Id;
					$type = $oldLicType . ' -> ' . $newLicType;
					$ins = "INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$own','$LicenceIdmap','$vehicleId','$deviceId','$type','Converted')";
					$conn->multi_query($ins);
				}
				$conn->close();
			} catch (Exception $e) {
				logger($e->getMessage() . "------------ > audit errer");
			}
			return redirect()->to('vdmFranchises')->with([
				'message' => "Licence Converted successfully"
			]);
		} catch (Exception $e) {
			logger($e->getMessage());
			return redirect()->back()->withInput()->with([
				'message' => "Licence Conversion Failed"
			]);
		}
	}

	public function authenticateDealer($fcode)
	{
		Log::info('inside authenticated dealer list');
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$dealerList = $redis->smembers('S_Dealers_' . $fcode,);
		$authenticatedDealer = array();
		$authenticatedList = array();
		foreach ($dealerList as $dealerName) {
			$dealerJson = $redis->hget('H_DealerDetails_' . $fcode, $dealerName);
			$dealerDetails = json_decode($dealerJson, true);
			$website = isset($dealerDetails['website']) ? $dealerDetails['website'] : '';
			$authenticated = isset($dealerDetails['authenticated']) ? $dealerDetails['authenticated'] : 'no';
			$checkdomain = $redis->get('Authenticated_Domain');
			if ($website != null  || (strpos($website, $checkdomain) !== false)) {
				Log::info($dealerName);
				$authenticatedDealer = Arr::add($authenticatedDealer, $dealerName, $dealerName);
				$authenticatedList = Arr::add($authenticatedList, $dealerName, $authenticated);
			}
		}
		$dealerInfo = [
			'authenticatedDealer' => $authenticatedDealer,
			'authenticatedList' => $authenticatedList,
		];
		return view('vdm.franchise.authenticate', $dealerInfo);
	}
}
