<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Auth\CreateSessionCookie\FailedToCreateSessionCookie;
use Auth;
use App\Models\User;
use Exception;
use Firebase\Auth\Token\Exception\InvalidToken;

class FirebaseController extends Controller
{

    public static function auth()
    {
        return app('firebase.auth');
    }
    public static function disableUser($uid)
    {
        return self::auth()->disableUser($uid);
    }

    public static function enableUser($uid)
    {
        return self::auth()->enableUser($uid);
    }

    public static function signInWithEmailAndPassword($username, $password)
    {
        return self::auth()->signInWithEmailAndPassword($username, $password);
    }

    public static function createCustomToken($uid)
    {
        return self::auth()->createCustomToken($uid);
    }

    public static  function signInWithCustomToken($customToken)
    {
        return self::auth()->signInWithCustomToken($customToken);
    }

    public static  function verifyIdToken($verifyIdToken)
    {
        return self::auth()->verifyIdToken($verifyIdToken);
    }

    public static  function createUser($userData)
    {
        return self::auth()->createUser($userData);
    }

    public static  function updateUser($uid, $userData)
    {
        return self::auth()->updateUser($uid, $userData);
    }

    public static  function changeUserEmail($uid, $mail)
    {
        return self::auth()->changeUserEmail($uid, $mail);
    }

    // The new custom claims will propagate to the user's ID token the
    // next time a new one is issued.
    public static  function setCustomUserClaims($uid, $userData)
    {
        return self::auth()->setCustomUserClaims($uid, $userData);
    }

    // Retrieve a user's current custom claims
    public static  function getUser($uid)
    {
        try{
            return self::auth()->getUser($uid);
        }catch(FirebaseException $e){
            return null;
        }
    }

    // get custom claims
    public static  function customClaims($uid)
    {
        return self::getUser($uid)->customClaims;
    }

    public static  function deleteUser($uid)
    {
        return self::auth()->deleteUser($uid);
    }

    public static function signInAsUser($uid)
    {
        return self::auth()->signInAsUser($uid);
    }

    public static function signInAsUserToken($uid)
    {
        return self::signInAsUser($uid)->idToken();
    }
    
    public static function signinWithUserId($uid)
    {
        $firebaseUser = self::getUser($uid);
        $user = new User([
            'localId' => $firebaseUser->uid,
            'email' => $firebaseUser->email,
            'displayName' => $firebaseUser->displayName
        ]);
        return $user;
    }

    public static function createSessionCookie($idToken, $oneWeek=null)
    {
        if($oneWeek == null){
            $oneWeek = new \DateInterval('P7D');
        }
        try{
            return self::auth()->createSessionCookie($idToken, $oneWeek);
        }catch(FailedToCreateSessionCookie $e){
            // dd($e);
            return null;
        }
    }
}
