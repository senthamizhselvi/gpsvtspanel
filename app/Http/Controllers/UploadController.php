<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Support\Facades\Redis;
use App\Models\loginCustomisation;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UploadController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public static function get_domain($url)
	{
		$nowww = str_replace('www.', '', $url);
		$domain = parse_url($nowww);
		if (!empty($domain["host"])) {
			return $domain["host"];
		} else {
			return $domain["path"];
		}
	}



	public function loginCustom($dealerName)
	{
		return view('vdm.login.frame')->with('dealerName', $dealerName);
	}

	public function template1($dealerName)
	{
		return view('vdm.login.template1_design')->with('dealerName', $dealerName);
	}

	public function template2($dealerName)
	{
		return view('vdm.login.template2_design')->with('dealerName', $dealerName);
	}

	public function template3($dealerName)
	{
		return view('vdm.login.template3_design')->with('dealerName', $dealerName);
	}
	public function template4($dealerName)
	{
		return view('vdm.login.template4_design')->with('dealerName', $dealerName);
	}




	public function view()
	{
		return view('imageUpload');
	}

	public function upload()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$s3 = app('aws')->createClient('s3', [
			'endpoint' => config()->get('constant.endpoint'),
		]);
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		if (session()->get('cur') == 'admin') {
			$detailJson = $redis->hget('H_Franchise', $fcode);
			$detail = json_decode($detailJson, true);
		} else if (session()->get('cur') == 'dealer') {
			$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $username);
			$detail = json_decode($detailJson, true);
		}
		if (isset($detail['website']) == 1) {
			$website = $detail['website'];
		} else {
			$website = '';
		}
		if ($website == '') {
			return redirect()->back()->withErrors('These Dealer Does not have websit ' . $username);
		}
		$rules = array(
			'background' => 'mimes:jpeg,bmp,png',
			'logo' => 'mimes:jpeg,bmp,png'
		);
		$current_link = request()->server('HTTP_HOST');
		$current_link1 = UploadController::get_domain($current_link);
		$validator = validator()->make(request()->all(), $rules);
		if ($validator->fails()) {
			return redirect()->back()->withInput()->withErrors($validator);
		}
		$template_name	=	request()->get('template_name');
		$websits = UploadController::get_domain($website);
		$folder_name = $websits;
		$upload_folder = public_path() . '/temp/';
		DB::table('login_Customisation')->where('Domain', $websits)->delete();
		if ($template_name == '3') {
			if (!file_exists($upload_folder)) {
				$result = File::makeDirectory($upload_folder, 0777, true);
			}
			$file  = request()->file('logo');
			$file->move($upload_folder, "logo." . $file->getClientOriginalExtension());
			$file_name = "logo." . $file->getClientOriginalExtension();
			$s3->putObject(array(
				'Bucket' => config()->get('constant.bucket'),
				'endpoint' => 'sgp1.digitaloceanspaces.com',
				'Key' => "picture/" . $folder_name . '/logo.' . $file->getClientOriginalExtension(),
				'SourceFile' => $upload_folder . "logo." . $file->getClientOriginalExtension(),
				'ACL' => 'public-read'
			));
			$logo_loc = config()->get('constant.endpoint') . '/' . config()->get('constant.bucket') . '/picture/' . $folder_name . '/logo.' . $file->getClientOriginalExtension();

			try {
				$mysqlDetails = array();
				$mysqlDetails = array(
					'username' => $username,
					'status' => config()->get('constant.updated'),
					'fcode' => $fcode,
					'Domain' => $websits,
					'Template' => $template_name,
					'bcolor' => request()->get('bgcolor'),
					'themecolor' => request()->get('themecolor'),
					'bcolor1' => '',
					'fcolor' => request()->get('fontcolor'),
					'logo' => $logo_loc,
					'background' => '',
					'userIpAddress' => session()->get('userIP'),

				);
				$modelname = new loginCustomisation();
				$table = $modelname->getTable();
				$tableExist = DB::table('information_schema.tables')->where('table_schema', '=', 'VAMOSYS')->where('table_name', '=', $table)->get();
				//return $tableExist;
				if (count($tableExist) > 0) {
					loginCustomisation::create($mysqlDetails);
				} else {
					AuditTables::CreateLoginCustomisation();
					loginCustomisation::create($mysqlDetails);
				}
			} catch (Exception $e) {
				logger('Error inside Audit_LoginCustomisation Update' . $e->getMessage());
			}
			DB::table('login_Customisation')->insert(
				array(
					'Domain' => $websits,
					'Template' => $template_name,
					'bcolor' => request()->get('bgcolor'),
					'bcolor1' => '',
					'fcolor' => request()->get('fontcolor'),
					'themecolor' => request()->get('themecolor'),
					'logo' => $logo_loc,
					'background' => '',
					'fcode'   => $fcode,
					'UserName' => $username,
				)
			);
			File::deleteDirectory($upload_folder);
		} else {
			if (!file_exists($upload_folder)) {
				$result = File::makeDirectory($upload_folder, 0777, true);
			}
			$file  = request()->file('logo');
			$file->move($upload_folder, "logo." . $file->getClientOriginalExtension());
			// dd($s3);
			$s3->putObject(array(
				'Bucket' => config()->get('constant.bucket'),
				'endpoint' => 'sgp1.digitaloceanspaces.com',
				'Key' => "picture/" . $folder_name . '/logo.' . $file->getClientOriginalExtension(),
				'SourceFile' => $upload_folder . "logo." . $file->getClientOriginalExtension(),
				'ACL' => 'public-read'
			));
			$logo_loc = config()->get('constant.endpoint') . '/' . config()->get('constant.bucket') . '/picture/' . $folder_name . '/logo.' . $file->getClientOriginalExtension();
			//$file = $request->file('background');
			$file = request()->file('background');
			$file->move($upload_folder, "background." . $file->getClientOriginalExtension());
			$s3->putObject(array(
				'Bucket' => config()->get('constant.bucket'),
				'Key' => "picture/" . $folder_name . '/background.' . $file->getClientOriginalExtension(),
				'SourceFile' => $upload_folder . "background." . $file->getClientOriginalExtension(),
				'ACL' => 'public-read'
			));
			$bg_loc = config()->get('constant.endpoint') . '/' . config()->get('constant.bucket') . '/picture/' . $folder_name . '/background.' . $file->getClientOriginalExtension();

			try {
				$mysqlDetails = array();
				$mysqlDetails = array(
					'username' => $username,
					'status' => config()->get('constant.updated'),
					'fcode' => $fcode,
					'Domain' => $websits,
					'Template' => $template_name,
					'bcolor' => request()->get('bgcolor'),
					'bcolor1' => request()->get('bgcolor1'),
					'fcolor' => request()->get('fontcolor'),
					'logo' => $logo_loc,
					'background' => $bg_loc,
					'userIpAddress' => session()->get('userIP'),
				);
				$modelname = new loginCustomisation();
				$table = $modelname->getTable();
				$tableExist = DB::table('information_schema.tables')->where('table_schema', '=', 'VAMOSYS')->where('table_name', '=', $table)->get();
				//return $tableExist;
				if (count($tableExist) > 0) {
					loginCustomisation::create($mysqlDetails);
				} else {
					AuditTables::CreateLoginCustomisation();
					loginCustomisation::create($mysqlDetails);
				}
			} catch (Exception $e) {
				logger('Error inside Audit_LoginCustomisation Update' . $e->getMessage());
			}

			DB::table('login_Customisation')->insert(
				array(
					'Domain' => $websits,
					'Template' => $template_name,
					'bcolor' => request()->get('bgcolor'),
					'bcolor1' => request()->get('bgcolor1'),
					'fcolor' => request()->get('fontcolor'),
					'logo' => $logo_loc,
					'background' => $bg_loc,
					'fcode'   => $fcode,
					'UserName' => $username,
				)
			);
			File::deleteDirectory($upload_folder);
		}

		$txt1 = "Theme applied Sucessfully";
		$response = "<div style='display: flex;flex-wrap: wrap;justify-content: center;	align-items: center;width: 100%;height: 100%;'><h2>" . $txt1 . "</h2></div>";
		return $response;
	}

	public function sourceData()
	{

		$s3 = app('aws')->createClient('s3', [
			'endpoint' => config()->get('constant.endpoint'),
		]);
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		if (session()->get('cur') == 'admin') {
			$detailJson = $redis->hget('H_Franchise', $fcode);
		} else if (session()->get('cur') == 'dealer') {
			$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $username);
		}

		$detail = json_decode($detailJson, true);
		if (isset($detail['website']) == 1) {
			$website = $detail['website'];
		} else {
			$website = '';
		}
		if ($website == '') {
			return redirect()->back()->withErrors('These dealer Does not have websit ' . $username);
		}
		$websits = UploadController::get_domain($website);
		$content = request()->get('sourceCode');
		$folder_name = $websits;
		$upload_folder = public_path() . '/temp/';

		if (!file_exists($upload_folder)) {
			$result = File::makeDirectory($upload_folder, 0777, true);
		}

		$file  = request()->file('logo');
		$file->move($upload_folder, "logo." . $file->getClientOriginalExtension());
		$logo = config()->get('constant.endpoint') . '/' . config()->get('constant.bucket') . '/picture/' . $folder_name . '/logo.' . $file->getClientOriginalExtension();
		$res = $s3->putObject(array(
			'Bucket' => config()->get('constant.bucket'),
			'Key' => "picture/temp/" . $folder_name . '/logo.' . $file->getClientOriginalExtension(),
			'SourceFile' => $upload_folder . "logo." . $file->getClientOriginalExtension(),
			'ACL' => 'public-read'
		));

		$file = request()->file('background');
		$file->move($upload_folder, "background." . $file->getClientOriginalExtension());
		$background = config()->get('constant.endpoint') . '/' . config()->get('constant.bucket') . '/picture/' . $folder_name . '/logo.' . $file->getClientOriginalExtension();
		$res = $s3->putObject(array(
			'Bucket' => config()->get('constant.bucket'),
			'Key' => "picture/temp/" . $folder_name . '/background.' . $file->getClientOriginalExtension(),
			'SourceFile' => $upload_folder . "background." . $file->getClientOriginalExtension(),
			'ACL' => 'public-read'
		));

		File::put($upload_folder . "sourceData.txt", "$content");
		$res = $s3->putObject(array(
			'Bucket' => config()->get('constant.bucket'),
			'Key' => "picture/temp/" . $folder_name . '/sourceData.txt',
			'SourceFile' => $upload_folder . "sourceData.txt",
			'ACL' => 'public-read'
		));
		File::deleteDirectory($upload_folder);

		$txt1 = "Theme Updated Successfully. ";
		$txt2 = "Will get reflected within 24 hrs once approved by Super admin.";
		$tex3 = "For further details Contact support@gpsvts.freshdesk.com";
		// try {
		// 	Mail::send('emails.tem4Email', array('domain' => $websits, 'fcode' => $fcode, 'userId' => $username), function ($message) use ($username) {
		// 		$message->to("arunkumar.vamosys@gmail.com")->subject('Login page customisation -' . $username);
		// 		$message->cc("arunkumar.vamosys@gmail.com");
		// 		// support@vamosys.freshdesk.com
		// 	});
		// } catch (\Exception $e) {
		// 	logger('Mail Error ' . $e->getMessage());
		// }

		try {
			$mysqlDetails = array();
			$mysqlDetails = array(
				'username' => $username,
				'status' => config()->get('constant.updated'),
				'fcode' => $fcode,
				'Domain' => $websits,
				'Template' => '4',
				'logo' => $logo,
				'background' => $background,
				'userIpAddress' => session()->get('userIP'),

			);
			$modelname = new loginCustomisation();
			$table = $modelname->getTable();
			$tableExist = DB::table('information_schema.tables')->where('table_schema', '=', 'VAMOSYS')->where('table_name', '=', $table)->get();
			//return $tableExist;
			if (count($tableExist) > 0) {
				loginCustomisation::create($mysqlDetails);
			} else {
				AuditTables::CreateLoginCustomisation();
				loginCustomisation::create($mysqlDetails);
			}
		} catch (Exception $e) {
			logger('Error inside Audit_LoginCustomisation Update' . $e->getMessage());
		}
		echo "<h2 style='position: absolute;top: 50%;left: 35%; margin-top: -25px;margin-left: -50px;text-align: -webkit-center;'>" . $txt1 . "<br/>" . $txt2 . "<br/>" . $tex3 . "</h2>";
	}

	public function UploadPDFfile()
	{

		if (request()->file('file') == null) {
			session()->flash('message', 'Please Select File ... ');
			return redirect()->back();
		} else {

			$s3 = app('aws')->createClient('s3', [
				'endpoint' => config()->get('constant.endpoint'),
			]);
			$upload_folder = public_path() . '/temp/';
			if (!file_exists($upload_folder)) {
				$result = File::makeDirectory($upload_folder, 0777, true);
			}
			$file  = request()->file('file');
			$file->move($upload_folder, $file->getClientOriginalName());
			$file_name = $file->getClientOriginalName();
			$s3->putObject(array(
				'Bucket' => config()->get('constant.bucket'),
				'Key' => "ReleaseNotes/" . $file->getClientOriginalName(),
				'SourceFile' => $upload_folder . $file->getClientOriginalName(),
				'ACL' => 'public-read'
			));
			File::deleteDirectory($upload_folder);
			session()->flash('message', 'Uploaded Successfully... ');
			return redirect()->back();
		}
	}
}
