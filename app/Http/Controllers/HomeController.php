<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Request;

class HomeController extends Controller
{


	public function index()
	{
		$username = auth()->id();
		$response_data["username"] = $username;
		// return view('home')->with($response_data);
		return  redirect()->to('dashboard');
	}

	public function showLogin()
	{
		$current_link = request()->server('HTTP_HOST');
		$current_link1 = HomeController::get_domain($current_link);
		$url = 'no';
		$webLogo = null;

		if (strpos(url()->current(), 'gpsvts.net') == true) {
			$url = 'yes';
		}
		try {
			$endPoint = config()->get('constant.endpoint');
			$s3 = app()->make('aws')->createClient('s3', [
				'endpoint' => $endPoint,
			]);
			$objects = $s3->ListObjects(array(
				'Bucket' => config()->get('constant.bucket'),
				'Prefix' => 'picture/' . $current_link1
			));
			$datas = $objects->get('Contents');
			if (is_null($datas)) {
				$datas = [];
			}
			foreach ($datas as $object) {
				if (preg_match("/" . $current_link1 . ".(jpg|png|gif|jpeg|JPG|PNG|GIF|JPEG)/", $object['Key'])) {
					$webLogo = config()->get('constant.endpoint') . '/' . config()->get('constant.bucket') . '/' . $object['Key'];
				}
			}
			$login_data = DB::table('login_Customisation')->where('Domain',$current_link1)->get()->first();
			$template = $login_data->Template;
			$themecolor = $login_data->themecolor;
			if($themecolor == null){
				$themecolor = "";
			}
			$loginData = [
				'bcolor' => $login_data->bcolor,
				'bcolor1' => $login_data->bcolor1,
				'fcolor' => $login_data->fcolor,
				'logo' => $login_data->logo,
				'backgrounds' => $login_data->background,
				'themecolor'=>$themecolor,
				'url' => $url,
			];
			// $template=5;
			// $loginData = [
			// 	'bcolor' => "yellow",
			// 	'bcolor1' => "red",
			// 	'fcolor' => "black",
			// 	'themecolor'=>"#f26f21",
			// 	'logo' => "https://sgp1.digitaloceanspaces.com/vamotest/picture/cpanel.vamosys.com.png",
			// 	'backgrounds' => "https://cpanel.vamosys.com/cpanel/public/assets/imgs/bg.jpg",
			// 	'url' => "",
			// ];
			if($template <= 4){
				return view('vdm.login.template'.$template, $loginData);
			}else{
				return view('auth.login', $loginData);
			}
			// if ($template == 1) {
			// 	return view('vdm.login.template1', $loginData);
			// } else if ($template == 2) {
			// 	return view('vdm.login.template2', $loginData);
			// } else if ($template == 3) {
			// 	return view('vdm.login.template3', $loginData);
			// } else if ($template == 4) {
			// 	return view('vdm.login.template4', $loginData);
			// } else {
			// 	return view('auth.login', $loginData);
			// }
		} catch (Exception $e) {
			logger('showLogin Exception' . $e->getMessage());
		}finally{
			// dd(auth());
			if (auth()->check()) {
				$username = auth()->id();
				$redis = Redis::connection();
				$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
				if ($fcode == "GPSVA") {
					$d = $redis->sismember('S_Dealers_' . $fcode, $username);
					if ($d == 1 || $username == 'gpsvtsadmin') {
						return  redirect()->to('Business');
					}
				} else if (strpos($username, 'admin') !== false) {
					return  redirect()->to('Business');
				} else {
					$redis = Redis::connection();
					$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
					$val1 = $redis->sismember('S_Dealers_' . $fcode, $username);
					$val = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
					if ($val1 == 1 && isset($val)) {
						return  redirect()->to('DashBoard');
					}
				}
				return redirect()->to('track');
			}
			return view('auth.login', ['url' => $url, 'webLogo' => $webLogo]);
			// return view('auth.login')->with('url', $url);

		}
	}


	public function admin()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}

		$username = auth()->id();

		if ($username != 'vamos') {
			Auth::logout();
			return redirect()->to('login')->withErrors('Unauthorized user. Futher attempts will be	treated as hacking, will be prosecuted under Cyber laws.');
		} else {

			return redirect()->route('vdmFranchises.index');

			// return view('admin')->with('apnKey', $apnKey)->with('timezoneKey', $timezoneKey)->with('count', $count);
		}
	}


	public function ipAddressManager()
	{

		if (!auth()->check()) {
			return redirect()->to('login');
		}

		$username = auth()->id();
		if ($username != 'vamos') {
			return redirect()->to('login')->with('flash_notice', 'Unauthorized user. Futher attempts will be
					treated as hacking, will be prosecuted under Cyber laws.');
		}

		$redis = Redis::connection();
		$ipAddress = $redis->hget('H_IP_Address', 'ipAddress');
		$gt06nCount = $redis->hget('H_IP_Address', 'gt06nCount');
		$tr02Count = $redis->hget('H_IP_Address', 'tr02Count');
		$gt03aCount = $redis->hget('H_IP_Address', 'gt03aCount');
		//$count = count($redis->keys ( 'NoData:*' ));
		$apnKey = $redis->exists('Update:Apn');
		$timezoneKey = $redis->exists('Update:Timezone');
		$count = VdmFranchiseController::liveVehicleCountV2();

		return view('IPAddress', array(
			'ipAddress' => $ipAddress
		))->with('gt06nCount', $gt06nCount)->with('tr02Count', $tr02Count)->with('gt03aCount', $gt03aCount)->with('apnKey', $apnKey)->with('timezoneKey', $timezoneKey)->with('count', $count);
	}

	public function saveIpAddress()
	{
		if (!auth()->check()) {
			return redirect()->to('login');
		}

		$username = auth()->id();
		if ($username != 'vamos') {
			return redirect()->to('login')->with('flash_notice', 'Unauthorized user. Futher attempts will be
					treated as hacking, will be prosecuted under Cyber laws.');
		}

		$rules = array(
			'ipAddress' => 'required',
			'gt06nCount' => 'numeric',
			'tr02Count' => 'numeric',
			'gt03aCount' => 'numeric'

		);

		$validator = validator()->make(request()->all(), $rules);
		if ($validator->fails()) {
			return redirect()->to('ipAddressManager')->withErrors($validator);
		} else {
			$redis = Redis::connection();
			$ipAddress = request()->get('ipAddress');
			$gt06nCount = request()->get('gt06nCount');
			$tr02Count = request()->get('tr02Count');
			$gt03aCount = request()->get('gt03aCount');
			$redis->hmset(
				'H_IP_Address',
				'ipAddress',
				$ipAddress,
				'gt06nCount',
				$gt06nCount,
				'tr02Count',
				$tr02Count,
				'gt03aCount',
				$gt03aCount
			);

			$init = $redis->lindex('L_GT06N_AVBL_PORTS', 0);
			$currentCount   = isset($init) ? $init : 10000;

			$endCount = $currentCount + $gt06nCount;

			for ($count = $currentCount; $count <= $endCount; $count++) {
				$redis->rpush('L_GT06N_AVBL_PORTS', $count);
			}
		}
		session()->flash('message', 'Successfully added ipAddress details' . '!');

		return redirect()->to('admin');
	}

	public function reverseGeoLocation()
	{
		$lat = request()->get('lat');
		$lng = request()->get('lng');

		//https://maps.google.com/maps/api/geocode/json?latlng

		$url = "https://maps.google.com/maps/api/geocode/json?latlng=" . $lat . "," . $lng . "&key=AIzaSyBQFgD9_Pm59zGz0ZfLYCUiH_7zbuZ_bFM";
		$data = @file_get_contents($url);
		$jsondata = json_decode($data, true);
		if (is_array($jsondata) && $jsondata['status'] == "OK") {
			echo $jsondata['results']['1']['formatted_address'];
		} else {
			logger("empty");
		}
	}

	public function livelogin()
	{
		// show the form
		return view('livelogin');
	}

	public static function get_domain($url)
	{
		$nowww = str_replace('www.', '', $url);
		$domain = parse_url($nowww);
		if (!empty($domain["host"])) {
			return $domain["host"];
		} else {
			return $domain["path"];
		}
	}

	public function doLogin()
	{
		session()->put('lang', request('lang'));
		$rules = array(
			'userName'    => 'required',
			'password' => 'required|min:6'
		);
		// run the validation rules on the inputs from the form
		$validator = validator()->make(request()->all(), $rules);
		$remember = (request()->has('remember')) ? true : false;
		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return redirect()->to('login')
				->withErrors($validator) // send back all errors to the login form
				->withInput(request()->except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {
			// create our user data for the authentication
			$userdata = array(
				'userName' 	=> request()->get('userName'),
				'password' 	=> request()->get('password')
			);
			// attempt to do the login
			if (Auth::attempt($userdata, $remember)) {
				// dd(auth());
				$username = auth()->id();
				$redis = Redis::connection();
				$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
				$url = Request::url();
				if ($redis->sismember('S_Franchises_Disable', $fcode)) {
					return redirect()->to('login')
						->withInput(request()->except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
				}
				$disableDeler = $redis->smembers('S_Dealers_' . $fcode);
				foreach ($disableDeler as $key => $dealerId) {
					$lock = $redis->hget('H_UserId_Cust_Map', $dealerId . ':lock');
					if ($lock == 'Disabled') {
						$dealerusersList = 'S_Users_Dealer_' . $dealerId . '_' . $fcode;
						if ($redis->sismember($dealerusersList, $username)) {
							Auth::logout();
							return redirect()->to('login')->withInput(request()->except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
						}
					}
				}
				$current_link = request()->server('HTTP_HOST');
				$domainName = HomeController::get_domain($current_link);
				$val1 = $redis->sismember('S_Dealers_' . $fcode, $username);
				$val = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
				$checkdomain = "cpanel.vamosys.com";
				$checkdomain = "localhost";
				if ($val1 == 1 && isset($val)) {
					$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $username);
					$detail = json_decode($detailJson, true);
					$checkdomain = isset($detail['subDomain']) ? $detail['subDomain'] : 'cpanel.vamosys.com';
				}
				//need to remove after domain changed
				if ($checkdomain == 'cpanel.gpsvts.net') {
					$checkdomain = "cpanel.vamosys.com";
				}
				if ($domainName == "cpanel.gpsvts.net") {
					$key = md5(microtime(true) . mt_Rand());
					$redis->set($username . ':key', $key);
					$redis->EXPIRE($username . ':key', 60);
					Auth::logout();
					$userNAme = HomeController::encrypt($username, "XSBRMLT");
					$pwd = HomeController::encrypt(request()->get('password'), $key);
					$dmn = HomeController::encrypt($domainName, $key);
					return redirect()->to('https://cpanel.vamosys.com/cpanel/public/loginpass/' . $userNAme . '/' . $pwd . '/' . $dmn);
				}
				//end
				if ($domainName == "cpanel.vamosys.com" || (strpos($domainName, $checkdomain) !== false)) {
					if ($username == 'vamos') {
						//	return  redirect()->to('track');	
						Auth::logout();
						$byPass = 'no';
						return view('vdm.login.otplogin')->with('username', $username)->with('byPass', $byPass);
					}
					if (strpos($username, 'admin') !== false || strpos($username, 'ADMIN') !== false) {
						if ($username == 'smpadmin' || $username == 'vamoadmin') {
							Auth::logout();
							$byPass = 'no';
							return view('vdm.login.otplogin')->with('username', $username)->with('byPass', $byPass);
						}
						return  redirect()->to('Business');
					} else if ($val1 == 1 && isset($val)) {
						$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $username);
						$detail = json_decode($detailJson, true);
						if (isset($detail['website']) == 1) {
							$website = $detail['website'];
						} else {
							$website = '';
						}
						$domain = HomeController::get_domain($website);
						if ($domain == 'gpsvts.net') {
							session()->put('curdomin', 'gps');
						} else {
							session()->put('curdomin', 'notgps');
						}
						//return  redirect()->to('dashboard');
						$subdomain = "cpanel.vamosys.com";
						if (isset($detail['subDomain']) == 1) {
							$subdomain = isset($detail['subDomain']) ? $detail['subDomain'] : 'cpanel.vamosys.com';
						}
						if ($subdomain == null || $subdomain == '') {
							$subdomain = 'cpanel.vamosys.com';
						}
						//need to remove after domain changed
						if ($subdomain == 'cpanel.gpsvts.net') {
							$subdomain = "cpanel.vamosys.com";
						}
						//end
						if ($domainName == "cpanel.vamosys.com" || $domainName == $subdomain) {
							return  redirect()->to('dashboard');
						} else {
							Auth::logout();
							return redirect()->to('login')->withInput(request()->except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
						}
					} else {
						if (auth()->check()) {
							Auth::logout();
						}
						return redirect()->to('login')->withInput(request()->except('password'))->with('flash_notice', 'Entered Username is not an admin User !!');
					}
				} else if ((strpos($username, 'admin') !== false) || ($val1 == 1 && isset($val)) || ($username == 'vamos')) {
					logger('---------------is dealer adminauth:--------------' . $username);
					$key = md5(microtime(true) . mt_Rand());
					$redis->set($username . ':key', $key);
					$redis->EXPIRE($username . ':key', 60);
					Auth::logout();
					$userNAme = HomeController::encrypt($username, "XSBRMLT");
					$pwd = HomeController::encrypt(request()->get('password'), $key);
					$dmn = HomeController::encrypt($domainName, $key);
					return redirect()->to('https://cpanel.vamosys.com/cpanel/public/loginpass/' . $userNAme . '/' . $pwd . '/' . $dmn);
				} else {
					logger('---------------is enduserauth:--------------' . $username);
					$userDomainName = HomeController::getUserDomainName($fcode, $username);
					//if(strpos($domainName,$userDomainName) !== false )	{
					if (strpos('true', $userDomainName) !== false ) {
						if ($fcode == 'GPSVA') {
							Auth::logout();
							return redirect()->to('login')->withInput(request()->except('password'))->with('flash_notice', 'You are restricted in web application. Please use Mobile application');
						}
						$userPrePage = $redis->hget('H_UserId_Cust_Map', $username . ':userPre');
						$groups = $redis->smembers($username);
						$vehicles = $redis->smembers($groups[0]);
						session()->put('group', $groups[0]);
						session()->put('vehicle', $vehicles[0]);
						session()->put('userPrePage', $userPrePage);
						if ($userPrePage == "" || $userPrePage == null) {
							return  redirect()->to('track');
						} else {
							return  redirect()->to('userPage');
						}
					} else {
						Auth::logout();
						return redirect()->to('login')->withInput(request()->except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
					}
				}
			} else {
				// validation not successful, send back to form
				return redirect()->to('login')->withInput(request()->except('password'))->withErrors('Your username/password combination was incorrect.')->with('flash_notice', 'Your username/password combination was incorrect.');
			}
		}
	}


	public function getUserIP()
	{
		$userIP = request()->get('userIP');
		session()->put('userIP', $userIP);
		return $userIP;
	}
	public function getFcode()
	{

		$usernames = request()->get('id');
		$userIP = request()->get('userIP');
		session()->put('userIP', $userIP);
		$redis    = Redis::connection();
		$fcodeKeys   = $redis->hget('H_UserId_Cust_Map', $usernames . ':fcode');
		return $fcodeKeys;
	}

	public function getApi()
	{
		$apiKey = '';
		$username = request()->get('id');
		$redis    = Redis::connection();
		$fcodeKey   = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$dealerName = $redis->hget('H_UserId_Cust_Map', $username . ':OWN');
		if ($dealerName != '' && $dealerName != 'admin') {
			$detailJson     = $redis->hget('H_DealerDetails_' . $fcodeKey, $dealerName);
			$detailsDealer  = json_decode($detailJson, true);
			if (strpos($_SERVER['SERVER_NAME'], 'www.') !== false) {
				$hostName = preg_replace('/www./', '', $_SERVER['SERVER_NAME'], 1);
			} else {
				$hostName = $_SERVER['SERVER_NAME'];
			}
			if ($hostName == preg_replace('/www./', '', $detailsDealer['website'], 1)) {
				if (isset($detailsDealer['mapKey']) == 1) $apiKey = $detailsDealer['mapKey'];
			} else if ($hostName == preg_replace('/www./', '', $detailsDealer['website1'], 1)) {
				if (isset($detailsDealer['mapKey1']) == 1) $apiKey = $detailsDealer['mapKey1'];
			} else {
				$apiKey = HomeController::getFranchiseApi($fcodeKey);
			}

			if ($apiKey == '') {
				$apiKey = HomeController::getFranchiseApi($fcodeKey);
			}
		} else {
			$apiKey = HomeController::getFranchiseApi($fcodeKey);
		}
		return $apiKey;
	}

	public function getFranchiseApi($fcodeKey)
	{
		$redis = Redis::connection();
		if (strpos($_SERVER['SERVER_NAME'], 'www.') !== false) {
			$hostName = preg_replace('/www./', '', $_SERVER['SERVER_NAME'], 1);
		} else {
			$hostName = $_SERVER['SERVER_NAME'];
		}
		$franchiseDetails = $redis->hget('H_Franchise', $fcodeKey);
		$getFranchise = json_decode($franchiseDetails, true);
		$websites = array(isset($getFranchise['website']) ? preg_replace('/www./', '', $getFranchise['website'], 1) : '', isset($getFranchise['website2']) ? preg_replace('/www./', '', $getFranchise['website2'], 1) : '', isset($getFranchise['website3']) ? preg_replace('/www./', '', $getFranchise['website3'], 1) : '', isset($getFranchise['website4']) ? preg_replace('/www./', '', $getFranchise['website4'], 1) : '');
		if (in_array($hostName, $websites)) {
			$key = array_search($hostName, $websites);
			if ($key == 0) {
				$apiKey = $getFranchise['apiKey'];
			} else {
				$apiIndex = (int)$key + 1;
				$apiKey = $getFranchise['apiKey' . $apiIndex];
			}
		} else {
			$apiKey = '';
		}
		return $apiKey;
	}

	public function authName()
	{
		$assetValue = array();
		$username   = auth()->id();
		$redis      = Redis::connection();
		$fcodeKey   = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$dealerName = $redis->hget('H_UserId_Cust_Map', $username . ':OWN');
		if ($dealerName != '' && $dealerName != 'admin') {
			$detailJson     = $redis->hget('H_DealerDetails_' . $fcodeKey, $dealerName);
			$detailsDealer  = json_decode($detailJson, true);
			if (isset($detailsDealer['mapKey']) == 1) {
				$apiKey = $detailsDealer['mapKey'];
				if ($apiKey != '') {
					$assetValue[] = $apiKey;
					$assetValue[] = $username;
				} else {
					$apiKey = HomeController::getFranchiseApi($fcodeKey);
					$assetValue[] = $apiKey;
					$assetValue[] = $username;
				}
			} else {
				$apiKey = HomeController::getFranchiseApi($fcodeKey);
				$assetValue[] = $apiKey;
				$assetValue[] = $username;
			}
		} else {
			$apiKey = HomeController::getFranchiseApi($fcodeKey);
			$assetValue[] = $apiKey;
			$assetValue[] = $username;
		}
		return $assetValue;
	}


	public function getDealerName()
	{
		$username   = auth()->id();
		$redis      = Redis::connection();

		$dealerName = $redis->hget('H_UserId_Cust_Map', $username . ':OWN');

		return $dealerName;
	}
	public function getDealer()
	{
		$username    =  request()->get('id');
		$redis       =  Redis::connection();
		$dealerName  =  $redis->hget('H_UserId_Cust_Map', $username . ':OWN');
		return $dealerName;
	}

	public function doLogout()
	{
		try {
			session()->put('curdomin', 'notgps');
			if (session()->get('curSwt') == 'switch') {
				if (!auth()->check()) {
					return redirect()->to('login');
				}
				$username = auth()->id();
				$redis = Redis::connection();
				$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
				$franDetails_json = $redis->hget('H_Franchise', $fcode);
				$franchiseDetails = json_decode($franDetails_json, true);
				$userId = isset($franchiseDetails['userId']) ? $franchiseDetails['userId'] : '';
				if ($userId != null) {
					$user=User::whereRaw('BINARY username = ?', [$userId])->firstOrFail();

					// $user = FirebaseController::signinWithUserId($userId);
					Auth::login($user);
					session()->put('curSwt', 'switchLog');
					return redirect()->to('Business');
				} else {
					Auth::logout();
					return redirect()->to('login');
				}
			}
			if (session()->get('frnSwt') == 'fransswitch') {
				// $user = FirebaseController::signinWithUserId('vamos');
				$user=User::whereRaw('BINARY username = ?', ["vamos"])->firstOrFail();

				Auth::login($user);
				session()->put('curSwt', 'switchLog');
				session()->put('frnSwt', 'switchLoG');
				return redirect()->to('vdmFranchises');
			}
		} catch (Exception $e) {
			logger('Exception logout' . $e->getMessage() . '----line--->' . $e->getLine());
		}

		Auth::logout();
		return redirect()->to('login');
	}

	public function fileUpload()
	{

		if (0 < $_FILES['file']['error']) {

			echo 'Error : ' . $_FILES['file']['error'] . '<br>';
		} else {

			move_uploaded_file($_FILES['file']['tmp_name'], '/var/www/gps/public/uploads/' . $_FILES['file']['name']);

			//move_uploaded_file( $_FILES['file']['tmp_name'], '/home/vamo/raj/script/'.$_FILES['file']['name'] );
			//move_uploaded_file( $_FILES['file']['tmp_name'], '/Applications/XAMPP/xamppfiles/htdocs/vamo/public/'.$_FILES['file']['name'] );

			logger("File uploaded successfully..");
		}
	}

	public function getFileNames()
	{
		$dir     =  '/var/www/gps/public/uploads';
		$files1  =  scandir($dir);
		$retArr = array();
		foreach ($files1 as $key => $value) {

			if (strpos($value, '.xlsx') || strpos($value, '.xls')) {
				array_push($retArr, $value);
			} else {
				logger('errrorr....');
			}
		}
		return $retArr;
	}
	public function mobileVerify()
	{
		$mobileNo1 = request()->get('val');
		$username = request()->get('usr');
		$redis = Redis::connection();
		$pin = mt_rand(100000, 999999);
		$string = str_shuffle($pin);
		$otp = $string;
		$mobileNos = $redis->hget('H_UserId_Cust_Map', $username . ':mobileOtp');
		$mobileNo2 = explode(":", $mobileNos);
		if (in_array($mobileNo1, $mobileNo2)) {
			foreach ($mobileNo2 as $key => $num) {
				if ($num == $mobileNo1) {
					$mobileNo = $num;
					$emailId = $redis->hget('H_OtpMobile_Email_Map', $mobileNo);
					$re = $redis->hset('H_Vamos_OTP', $mobileNo, $otp);
					$redis->set($mobileNo . ":OTP", $otp);
					$data = $redis->get($mobileNo . ":OTP");
					// OTP json For mail
					$bodyOfMsg = 'Your OTP number is ' . $otp . ' .';
					$subject = 'VAMOSS - OTP';
					$bccId = 'nithya.vamosys@gmail.com';
					$senderId = 'gpsvts.net';
					$mailDataArr = array(
						'toStr' => $emailId,
						'subject' => $subject,
						'body' => $bodyOfMsg,
						'bccstr' => $bccId,
						'sender' => $senderId
					);
					$mailDataJson = json_encode($mailDataArr);
					$redis->lpush('L_Mail_Q', $mailDataJson);
					$redis->EXPIRE($mobileNo . ":OTP", 400);
					return 'success';
				}
			}
		} else {
			return 'false';
		}
	}

	public function otpverify()
	{
		$otp = request()->get('val');
		$mobileNo = request()->get('valu');
		$username = request()->get('usr');
		$redis = Redis::connection();
		$data = $redis->hget('H_Vamos_OTP', $mobileNo);
		if ($data == $otp) {
			$userdata = array(
				'userName' 	=> $username,
				'password' 	=> $redis->hget('H_UserId_Cust_Map', $username . ':password')
			);
			$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
			$remember = (request()->has('remember')) ? true : false;
			if (Auth::attempt($userdata, $remember)) {
				logger(' inside the login ');
				$username = auth()->id();
				$current = Carbon::now();
				DB::table('onetimepass')->insert(array('Phone_num' => $mobileNo, 'OTP' => $data, 'User_Name' => $username));
				return 'success';
			} else {
				Auth::logout();
				return 'failed';
			}
		} else {
			Auth::logout();
			return 'failed';
		}
	}
	public function otpcancel()
	{
		Auth::logout();
		return 'success';
	}

	public function resendftn()
	{
		$mobileNo1 = request()->get('valu');
		$username = request()->get('usr');
		$redis = Redis::connection();
		$mobileNos = $redis->hget('H_UserId_Cust_Map', $username . ':mobileOtp');
		$mobileNo2 = explode(":", $mobileNos);
		$pin = mt_rand(100000, 999999);
		$string = str_shuffle($pin);
		$otp = $string;
		foreach ($mobileNo2 as $key => $num) {
			if ($num == $mobileNo1) {
				$mobileNo = $num;
				$emailId = $redis->hget('H_OtpMobile_Email_Map', $mobileNo);
				$re = $redis->$redis->hset('H_Vamos_OTP', $mobileNo, $otp);
				$redis->set($mobileNo . ":OTP", $otp);
				$data = $redis->get($mobileNo . ":OTP");
				// OTP json For mail
				$bodyOfMsg = 'Your OTP number is ' . $data . ' .';
				$subject = 'VAMOSS - OTP';
				$bccId = 'nithya.vamosys@gmail.com';
				$senderId = 'gpsvts.net';
				$mailDataArr = array(
					'toStr' => $emailId,
					'subject' => $subject,
					'body' => $bodyOfMsg,
					'bccstr' =>  $bccId,
					'sender' => $senderId
				);
				$mailDataJson = json_encode($mailDataArr);
				$redis->lpush('L_Mail_Q', $mailDataJson);
				$redis->EXPIRE($mobileNo . ":OTP", 400);
				return 'success';
			}
		}
	}
	public function getVehicle()
	{
		$vehNameArray = array();
		$vehicles = array();
		$redis           =  Redis::connection();
		$username                =       request()->get('username');
		$grpName                =       request()->get('groupName');
		$fcode                  =       $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		if ($grpName != "") {
			$vehicles = $redis->smembers($grpName . ':' . $fcode);
		} else {
			$groups = $redis->smembers($username);
			$vehicles = $redis->smembers($groups[0]);
		}
		foreach ($vehicles as $key => $value) {
			$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $value);
			$vehicleRefData = json_decode($vehicleRefData, true);
			$vehdetail = array("vehicleId" => $value, "shortName" => $vehicleRefData['shortName']);
			$vehNameArray[$key] = $vehdetail;
		}
		return $vehNameArray;
	}
	public function byPassUsers($id, $id2, $id3)
	{
		$userName = HomeController::decrypts($id, "XSBRMLT");
		$redis = Redis::connection();
		$key = $redis->get($userName . ':key');
		$redis->del($userName . ':key');
		if ($key == null) {
			return redirect()->to('login')->withInput(request()->except('password'))->with('flash_notice', 'Unable to login . Please try again !!!');
		}
		session()->put('userSwithDmn', HomeController::decrypts($id3, $key));
		$password = HomeController::decrypts($id2, $key);
		$userdata = array(
			'userName' 	=> $userName,
			'password' 	=> $password
		);
		$remember = (request()->has('remember')) ? true : false;
		if (Auth::attempt($userdata, $remember)) {
			$username = auth()->id();
			$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
			$url = Request::url();
			if ($redis->sismember('S_Franchises_Disable', $fcode)) {
				return redirect()->to('login')
					->withInput(request()->except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
			}
			$disableDeler = $redis->smembers('S_Dealers_' . $fcode);
			foreach ($disableDeler as $key => $dealerId) {
				$lock = $redis->hget('H_UserId_Cust_Map', $dealerId . ':lock');
				if ($lock == 'Disabled') {
					$dealerusersList = 'S_Users_Dealer_' . $dealerId . '_' . $fcode;
					if ($redis->sismember($dealerusersList, $username)) {
						return redirect()->to('login')->withInput(request()->except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
					}
				}
			}
			if ($fcode == "GPSVA") {
				$d = $redis->sismember('S_Dealers_' . $fcode, $username);
				if ($d == 1 || $username == 'gpsvtsadmin') {
					return  redirect()->to('Business');
				} else {
					$d = $redis->smembers('S_Dealers_' . $fcode);
					foreach ($d as $key => $org) {
						$user = $redis->sismember('S_Users_Dealer_' . $org . '_' . $fcode, $username);
						$user1 = $redis->sismember('S_Users_Admin_' . $fcode, $username);
						if ($user1 == 1 || $user == 1) {
							return redirect()->to('login')->withInput(request()->except('password'))->with('flash_notice', 'You are restricted in web application. Please use Mobile application');
						}
					}
				}
			} else if (strpos($username, 'admin') !== false) {
				//do nothing
				if ($username == 'smpadmin' || $username == 'vamoadmin') {
					Auth::logout();
					$byPass = 'yes';
					return view('vdm.login.otplogin')->with('username', $username)->with('byPass', $byPass);
				}
				//auth()->session(['cur' => 'admin']);
				return  redirect()->to('Business');
			} else {
				$redis = Redis::connection();
				$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
				$val1 = $redis->sismember('S_Dealers_' . $fcode, $username);
				$val = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
				if ($val1 == 1 && isset($val)) {
					$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $username);
					$detail = json_decode($detailJson, true);
					if (isset($detail['website']) == 1) {
						$website = $detail['website'];
					} else {
						$website = '';
					}
					$domain = HomeController::get_domain($website);
					if ($domain == 'gpsvts.net') {
						session()->put('curdomin', 'gps');
					} else {
						session()->put('curdomin', 'notgps');
					}
					return  redirect()->to('dashboard');
				}
			}
			if ($username == 'vamos') {
				Auth::logout();
				$byPass = 'yes';
				return view('vdm.login.otplogin')->with('username', $username)->with('byPass', $byPass);
			}
			$userPrePage = null;
			if ($username != 'vamos') {
				$userPrePage = $redis->hget('H_UserId_Cust_Map', $username . ':userPre');
				$groups = $redis->smembers($username);
				$vehicles = $redis->smembers($groups[0]);
				session()->put('group', $groups[0]);
				session()->put('vehicle', $vehicles[0]);
				session()->put('userPrePage', $userPrePage);
			}
			if ($userPrePage == "" || $userPrePage == null) {
				return  redirect()->to('track');
			} else {
				return  redirect()->to('userPage');
			}
		} else {
			return redirect()->to('login')->withInput(request()->except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
		}
	}
	public function decrypts($crypttext, $salt)
	{
		$crypttext = str_replace(array('-', '_'), array('+', '/'), $crypttext);
		$decoded_64 = base64_decode($crypttext);
		$td = mcrypt_module_open('cast-256', '', 'ecb', '');
		$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		mcrypt_generic_init($td, $salt, $iv);
		$decrypted_data = mdecrypt_generic($td, $decoded_64);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		return trim($decrypted_data);
	}
	public function encrypt($plaintext, $salt)
	{
		$td = mcrypt_module_open('cast-256', '', 'ecb', '');
		$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		mcrypt_generic_init($td, $salt, $iv);
		$encrypted_data = mcrypt_generic($td, $plaintext);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		$encoded_64 = base64_encode($encrypted_data);
		$data = str_replace(array('+', '/'), array('-', '_'), $encoded_64);
		return trim($data);
	}
	public function doLogin1()
	{

		$current_link = request()->server('HTTP_HOST');
		$domainName = HomeController::get_domain($current_link);
		session()->put('lang', request()->get('lang'));
		$rules = array(
			'userName'    => 'required',
			'password' => 'required|min:6'
		);
		$validator = validator()->make(request()->all(), $rules);
		$remember = (request()->has('remember')) ? true : false;
		if ($validator->fails()) {
			return redirect()->to('login')->withErrors($validator)->withInput(request()->except('password'));
		} else {
			$userdata = array(
				'userName'  => strtoupper(request()->get('userName')),
				'password'  => request()->get('password')
			);
			logger('Login details ' . request()->get('userName') . ' ' . request()->get('password'));
			if (Auth::attempt($userdata, $remember)) {
				$username = auth()->id();
				$redis = Redis::connection();
				$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
				$url = Request::url();
				if ($redis->sismember('S_Franchises_Disable', $fcode)) {
					return redirect()->to('login')->withInput(request()->except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
				}
				$disableDeler = $redis->smembers('S_Dealers_' . $fcode);
				foreach ($disableDeler as $key => $dealerId) {
					$lock = $redis->hget('H_UserId_Cust_Map', $dealerId . ':lock');
					if ($lock == 'Disabled') {
						$dealerusersList = 'S_Users_Dealer_' . $dealerId . '_' . $fcode;
						if ($redis->sismember($dealerusersList, $username)) {
							return redirect()->to('login')->withInput(request()->except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
						}
					}
				}
				if (strpos($username, 'admin') !== false) {
					$key = md5(microtime(true) . mt_Rand());
					+$redis->set($username . ':key', $key);
					+$redis->EXPIRE($username . ':key', 60);
					Auth::logout();
					$userNAme = HomeController::encrypt($username, "XSBRMLT");
					$pwd = HomeController::encrypt(request()->get('password'), $key);
					$dmn = HomeController::encrypt($domainName, $key);
					$datas = array(
						'val' => $userNAme,
						'val1' => 'admin',
						'val2' => $pwd,
						'dmn' => $dmn,
					);
					$data = json_encode($datas);
					return Response::json($data);
					//return redirect()->to('http://209.97.163.4/gps/public/loginpass/'.$userNAme.'/'.$pwd);        
				} else {
					$redis = Redis::connection();
					$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
					$val1 = $redis->sismember('S_Dealers_' . $fcode, $username);
					$val = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
					if ($val1 == 1 && isset($val)) {
						$key = md5(microtime(true) . mt_Rand());
						+$redis->set($username . ':key', $key);
						+$redis->EXPIRE($username . ':key', 60);
						Auth::logout();
						$userNAme = HomeController::encrypt($username, "XSBRMLT");
						$pwd = HomeController::encrypt(request()->get('password'), $key);
						$dmn = HomeController::encrypt($domainName, $key);
						$datas = array(
							'val' => $userNAme,
							'val1' => 'admin',
							'val2' => $pwd,
							'dmn' => $dmn,
						);
						$data = json_encode($datas);
						return Response::json($data);
						//return redirect()->to('http://209.97.163.4/gps/public/loginpass/'.$userNAme.'/'.$pwd);       
					}
				}
				if ($username == 'vamos') {
					$key = md5(microtime(true) . mt_Rand());
					+$redis->set($username . ':key', $key);
					+$redis->EXPIRE($username . ':key', 60);
					Auth::logout();
					$userNAme = HomeController::encrypt($username, "XSBRMLT");
					$pwd = HomeController::encrypt(request()->get('password'), $key);
					$dmn = HomeController::encrypt($domainName, $key);
					$datas = array(
						'val' => $userNAme,
						'val1' => 'admin',
						'val2' => $pwd,
						'dmn' => $dmn,
					);
					$data = json_encode($datas);
					return Response::json($data);
				}
				$userPrePage = null;
				if ($username != 'vamos') {
					$userPrePage = $redis->hget('H_UserId_Cust_Map', $username . ':userPre');
					$groups = $redis->smembers($username);
					$vehicles = $redis->smembers($groups[0]);
					session()->put('group', $groups[0]);
					session()->put('vehicle', $vehicles[0]);
					session()->put('userPrePage', $userPrePage);
				}
				//session()->get('userPrePage');
				//return $_SESSION["userPrePage"];
				if ($userPrePage == "" || $userPrePage == null) {
					$key = md5(microtime(true) . mt_Rand());
					+$redis->set($username . ':key', $key);
					+$redis->EXPIRE($username . ':key', 60);
					Auth::logout();
					$userNAme = HomeController::encrypt($username, "XSBRMLT");
					$pwd = HomeController::encrypt(request()->get('password'), $key);
					$datas = array(
						'val' => $userNAme,
						'val1' => 'users',
						'val2' => $pwd,
						'loc' => 'track',
					);

					$data = json_encode($datas);
					return Response::json($data);
					//  return  redirect()->to('track');
				} else {
					$key = md5(microtime(true) . mt_Rand());
					+$redis->set($username . ':key', $key);
					+$redis->EXPIRE($username . ':key', 60);
					Auth::logout();
					$userNAme = HomeController::encrypt($username, "XSBRMLT");
					$pwd = HomeController::encrypt(request()->get('password'), $key);
					$datas = array(
						'val' => $userNAme,
						'val1' => 'users',
						'val2' => $pwd,
						'loc' => 'userPage',
					);
					$data = json_encode($datas);
					return Response::json($data);



					//return  redirect()->to('userPage');
					/*$datas=array (
                'val' =>'userPage',
                'val1' =>'users',
                'val2' =>'null',
              );
              $data = json_encode ( $datas);
              return Response::json($data);*/
				}
				//endof attempt
			} else {
				return redirect()->to('login')->withInput(request()->except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
				// validation not successful, send back to form 
				//return redirect()->to('login');
			}
		}
	}

	public function switchLogin($username)
	{
		$redis = Redis::connection();
		Auth::logout();
		// $user = FirebaseController::signinWithUserId($username);
		$user = User::whereRaw('BINARY username = ?', [$username])->firstOrFail();
		Auth::login($user);
		$userPrePage = null;
		if ($username != 'vamos') {
			$userPrePage = $redis->hget('H_UserId_Cust_Map', $username . ':userPre');
			$groups = $redis->smembers($username);
			$vehicles = $redis->smembers($groups[0]);
			session()->put('group', $groups[0]);
			session()->put('vehicle', $vehicles[0]);
			session()->put('userPrePage', $userPrePage);
			session()->put('swUser', $username);
		}
		if ($userPrePage == "" || $userPrePage == null) {
			return  redirect()->to('track');
		} else {
			return  redirect()->to('userPage');
		}
	}

	public function getUserDomainName($fcode, $username)
	{
		$redis = Redis::connection();
		$adminUser = $redis->sismember('S_Users_Admin_' . $fcode, $username);
		$web = null;
		$dealerName1 = null;
		$current_link = request()->server('HTTP_HOST');
		$domainName = HomeController::get_domain($current_link);
		$web1 = 'empty';
		$web2 = 'empty';
		$web3 = 'empty';
		$isWebsite = 'false';
		if ($adminUser == 1) {
			$addetails = $redis->hget('H_Franchise', $fcode);
			$refData    = json_decode($addetails, true);
			//$web=isset($refData['website'])?$refData['website']:'';
			$web = isset($refData['website']) ? preg_replace('/www./', '', $refData['website'], 1) : 'gpsvts.net';
			$web1 = isset($refData['website2']) ? preg_replace('/www./', '', $refData['website2'], 1) : 'gpsvts.net';
			$web2 = isset($refData['website3']) ? preg_replace('/www./', '', $refData['website3'], 1) : 'gpsvts.net';
			$web3 = isset($refData['website4']) ? preg_replace('/www./', '', $refData['website4'], 1) : 'gpsvts.net';
		} else {
			$dealers = $redis->smembers('S_Dealers_' . $fcode);
			foreach ($dealers as $key => $value) {
				$dealerUser = $redis->sismember('S_Users_Dealer_' . $value . '_' . $fcode, $username);
				if ($dealerUser == 1) {
					$dealerName1 = $value;
					break;
				}
			}
			$addetails = $redis->hget('H_DealerDetails_' . $fcode, $dealerName1);
			$refData    = json_decode($addetails, true);
			//$web=isset($refData['website'])?$refData['website']:'gpsvts.net';
			$web = isset($refData['website']) ? preg_replace('/www./', '', $refData['website'], 1) : 'gpsvts.net';
			$web1 = isset($refData['website1']) ? preg_replace('/www./', '', $refData['website1'], 1) : 'gpsvts.net';
		}
		if ($web == null && $web == '') {
			$web = 'gpsvts.net';
		}
		//if($web!=null && $web1!=null && $web2!=null) {  
		if (($web != null && strpos($domainName, $web) !== false)  || ($web1 != null && strpos($domainName, $web1) !== false) || ($web2 != null && strpos($domainName, $web2) !== false)  || ($web3 != null && strpos($domainName, $web3) !== false)) {
			$isWebsite = 'true';
		}



		return $isWebsite;
	}
}
