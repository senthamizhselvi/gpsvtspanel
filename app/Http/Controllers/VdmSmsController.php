<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class VdmSmsController extends Controller
{

    public function filter()
    {
        if (!Auth::check()) {
            return redirect()->to('login');
        }
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        //get school list
        $orgsList = $redis->smembers('S_Organisations_' . $fcode);

        if (session()->get('cur') == 'dealer') {
            $orgsList = $redis->smembers('S_Organisations_Dealer_' . $username . '_' . $fcode);
        } else if (session()->get('cur') == 'admin') {
            $orgsList = $redis->smembers('S_Organisations_Admin_' . $fcode);
        }

        $orgsArr = array();
        foreach ($orgsList as $org) {
            $orgsArr = Arr::add($orgsArr, $org, $org);
        }

        return view('vdm.sms.index', [
            'orgsArr' => $orgsArr
        ]);
    }



    public function show()
    {

        $orgId = request()->get('orgId');
        $tripType = request()->get('tripType');
        $date = request()->get('date');
        $vehicleId = request()->get('vehicleId');
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

        $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
        $vehicleRefData = json_decode($vehicleRefData, true);

        $routeNo = $vehicleRefData['shortName'];

        $ipaddress = $redis->get('ipaddress');
        $parameters = '?userId=' . $username;
        $parameters = $parameters . '&vehicleId=' . $vehicleId . '&date=' . $date;
        $url = 'http://' . $ipaddress . ':9000/getSchoolBusSMSReport' . $parameters;

        //    $url = "http://vamosys.com:9000/getSchoolBusSMSReport?userId=alhadeed&routeNo=R16A&vehicleId=CV-D5994&date=2015-04-30";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        // Include header in result? (0 = yes, 1 = no)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $result = json_decode($response, true);

        $vehicleId = $result['vehicleId'];


        $deliveryDate = $result['deliveryDate'];
        $stopNames = $result['stopNames'];

        $mobileNos = $result['mobileNos'];
        $deliveryTime = $result['deliveryTime'];

        return view('vdm.sms.report', [
            'stopNames' => $stopNames,
            'mobileNos' => $mobileNos,
            'deliveryTime' => $deliveryTime,
            'vehicleId' => $vehicleId,
            'routeNo' => $routeNo,
            'deliveryDate' => $deliveryDate
        ]);
    }
}
