<?php

namespace App\Http\Controllers;

use App\Exports\UserExport;
use App\Models\User;
use Illuminate\Support\Facades\Redis;
use Kreait\Firebase\Exception\FirebaseException;
use App\Models\AuditUser;
use App\Models\AuditDealer;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Facades\Excel;

class VdmUserController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$redisUserCacheId = 'S_Users_' . $fcode;
		if (session()->get('cur') == 'dealer') {
			$redisUserCacheId = 'S_Users_Dealer_' . $username . '_' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$redisUserCacheId = 'S_Users_Admin_' . $fcode;
		}

		$userList = $redis->smembers($redisUserCacheId);

		sort($userList);
		$userGroups = null;
		$userGroupsArr = null;
		foreach ($userList as $key => $value) {
			$userGroups = $redis->smembers($value);
			$userGroupsArr = Arr::add($userGroupsArr, $value, $userGroups);
		}

		$indexData = [
			'fcode' => $fcode,
			'userGroupsArr' => $userGroupsArr,
			'userList' => $userList
		];
		return view('vdm.users.index', $indexData);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function search()
	{
		$orgLis = [];
		return view('vdm.users.scan', ['userList' => $orgLis]);
	}

	public function scan()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$redisUserCacheId = 'S_Users_' . $fcode;
		if (session()->get('cur') == 'dealer') {
			$redisUserCacheId = 'S_Users_Dealer_' . $username . '_' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$redisUserCacheId = 'S_Users_Admin_' . $fcode;
		}
		$text_word = request()->get('text_word');
		$cou = $redis->SCARD($redisUserCacheId);
		$orgLi = $redis->sScan($redisUserCacheId, 0, 'count', $cou, 'match', '*' . $text_word . '*');
		$orgL = $orgLi[1];
		$userGroups = null;
		$userGroupsArr = null;
		foreach ($orgL as $key => $value) {
			$userGroups = $redis->smembers($value);
			$userGroupsArr = Arr::add($userGroupsArr, $value, $userGroups);
		}
		$scanData = [
			'fcode' => $fcode,
			'userGroupsArr' => $userGroupsArr,
			'userList' => $orgL
		];
		return view('vdm.users.scan', $scanData);
	}
	/**		
	 * Show the form for creating a new resource.		
	 * @return Response		
	 */
	public function create()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$redisGrpId = 'S_Groups_' . $fcode;
		if (session()->get('cur') == 'dealer') {
			$redisGrpId = 'S_Groups_Dealer_' . $username . '_' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$redisGrpId = 'S_Groups_Admin_' . $fcode;
		}

		$vehicleGroups = null;
		$size = $redis->scard($redisGrpId);
		if ($size > 0) {
			$groups = $redis->smembers($redisGrpId);
			sort($groups);
			$groups = str_replace('.', '$', $groups);
			foreach ($groups as $key => $value) {
				$uppercheck = strtoupper($value);
				if ($uppercheck == $value) {
					$vehicleGroups = Arr::add($vehicleGroups, $value, $value);
				}
			}
			$vehicleGroups = str_replace('$', '.', $vehicleGroups);
		}

		$user = null;
		$userName = null;
		$user1 = new VdmDealersController;
		$user = $user1->checkuser();
		$userType = '1';
		$createData = [
			'vehicleGroups' => $vehicleGroups,
			'user' => $user,
			'userName' => $userName,
			'userType' => $userType,
		];

		return view('vdm.users.create', $createData);
	}

	/**
	 * Store a newly created resource in storage.
	 * TODO validations should be improved to prevent any attacks
	 * 
	 * @return Response
	 */
	public function store()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$userId = request()->get('userId');
		$rules = array(
			'userId' => 'required|alpha_dash',
			'email' => 'required|email',
			'vehicleGroups' => 'required',
			'password' => 'required|min:6'
		);

		$validator = validator()->make(request()->all(), $rules);

		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator)->withInput();
		} else {
			$userId = request()->get('userId');
			$val = $redis->hget('H_UserId_Cust_Map', $userId . ':fcode');
			$val1 = $redis->sismember('S_Users_' . $fcode, $userId);
			// $userExist = FirebaseController::getUser($userId) !== null;
			$userExist = false;
			if ($val1 == 1 || isset($val) || $userExist) {
				return redirect()->back()->withInput()->withErrors($userId . ' already exist. Please use different id !');
			}
			if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) {
				return redirect()->back()->withInput()->withErrors('Name with admin not acceptable');
			}
		}

		// store

		$userId = request()->get('userId');
		$email = request()->get('email');
		$cc_email = request()->get('cc_email');
		$vehicleGroups = request()->get('vehicleGroups');
		$vehicleGroups = str_replace('$', '.', $vehicleGroups);
		$mobileNo = request()->get('mobileNo');
		$zoho = request()->get('zoho');
		$companyName = request()->get('companyName');
		$enabledebug = request()->get('enable');
		$gender = request()->get('gender');

		foreach ($vehicleGroups as $grp) {
			$redis->sadd($userId, $grp);
			$redis->sadd('S_' . $grp, $userId);
		}

		if (session()->get('cur') == 'dealer') {
			$totalReports = $redis->smembers('S_Users_Reports_Dealer_' . $username . '_' . $fcode);
		} else if (session()->get('cur') == 'admin') {
			$totalReports = $redis->smembers('S_Users_Reports_Admin_' . $fcode);
		}
		if ($totalReports != null) {
			foreach ($totalReports as $key => $value) {
				$redis->sadd('S_Users_Reports_' . $userId . '_' . $fcode, $value);
			}
		}

		$virtualaccount = request()->get('virtualaccount',false);
		$assetuser = request()->get('assetuser',false);

		$notificationset = 'S_VAMOS_NOTIFICATION_' . $fcode;
		$notificationset1 = 'S_VAMOS_NOTIFICATION';
		$groupList0 = $redis->smembers($notificationset);
		if ($groupList0 == null) {
			$groupList = $redis->smembers($notificationset1);
		} else {
			$groupList = $groupList0;
		}
		$notiString = implode(",", $groupList);
		$redis->hset("H_Notification_Map_User", $userId, $notiString);
		$userType = '';
		if ($virtualaccount == 'value') {
			$redis->sadd('S_Users_Virtual_' . $fcode, $userId);
			if ($userType != "")
				$userType = "Virtual Account" . ',' . $userType;
			else
				$userType = "Virtual Account";
		}
		if ($assetuser == 'value') {
			$redis->sadd('S_Users_AssetTracker_' . $fcode, $userId);
			if ($userType != "")
				$userType = "Asset User" . ',' . $userType;
			else
				$userType = "Asset User";
		}
		$redis->sadd('S_Users_' . $fcode, $userId);
		if (session()->get('cur') == 'dealer') {
			$redis->sadd('S_Users_Dealer_' . $username . '_' . $fcode, $userId);
			$OWN = $username;
		} else if (session()->get('cur') == 'admin') {
			$redis->sadd('S_Users_Admin_' . $fcode, $userId);
			$OWN = 'admin';
		}

		$password = request()->get('password');
		if ($password == null) {
			$password = 'awesome';
		}

		try {
			$userProperties = [
				'email' => $userId . '@vamosys.com',
				'emailVerified' => false,
				'password' => $password,
				'displayName' => $userId,
				'disabled' => false,
				'uid' => $userId,
			];
			// FirebaseController::createUser($userProperties);
		} catch (FirebaseException $e) {
			logger('FirebaseException --->' . $e->getMessage());
			$errMsg = $e->getMessage();
			if ('DUPLICATE_LOCAL_ID' == $errMsg) {
				$errMsg = "User name already exist";
			}
			return redirect()->back()->withInput()->withErrors($errMsg);
		}

		$redis->hmset('H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo, $userId . ':email', $email, $userId . ':password', $password, $userId .
			':zoho', $zoho, $userId . ':OWN', $OWN, $userId . ':companyName', $companyName, $userId . ':cc_email', $cc_email, $userId . ':gender', $gender);
		$user = new User([
			'name'=>$userId,
			'username'=>$userId,
			'email'=>$email,
			'mobileNo'=>$mobileNo,
			'password'=>Hash::make($password),
			'gps_status'=>'1',
			'is_verified'=>'1'
		]);
		$user->save();

		try {
			$availableReports = "";
			if ($totalReports != null) {
				$availableReports = implode(', ', $totalReports);
			}
			$vehicleList = array();
			foreach ($vehicleGroups as $key => $gropName) {
				$gpVeh = $redis->smembers($gropName);
				foreach ($gpVeh as $key => $vehicle) {
					$vehicleList = Arr::add($vehicleList, $vehicle, $vehicle);
				}
			}
			$newUserData = [
				'mobileNo'=>$mobileNo,
				'email'=>$email,
				'cc_email'=>$cc_email,
				'zoho'=>$zoho,
				'companyName'=>$companyName,
			];
			$oldData = [];
			$newData = $newUserData;
			$newData['addedGroups'] = join(",",str_replace(":$fcode","",$vehicleGroups));
			// $oldData['removedGroups'] = "";
			$mysqlDetails = [
				'userIpAddress' => session()->get('userIP', '-'),
				'userName'=> $username,
				'type'=>config()->get('constant.user'),
				'name'=>$userId,
				'status'=>config()->get('constant.created'),
				'oldData'=>"",
				'newData'=>json_encode($newData),
			];
			AuditNewController::updateAudit($mysqlDetails);
		} catch (Exception $e) {
			logger('Exception_' . $username . ' CreateAuditUser error -->' . $e->getMessage() . ' line -->' . $e->getLine());
		}


		//$notificationset = 'S_VAMOS_NOTIFICATION';
		//$notificationGroups =  $redis->smembers ( $notificationset);
		$notificationset = 'S_VAMOS_NOTIFICATION_' . $fcode;
		$notificationset1 = 'S_VAMOS_NOTIFICATION';
		$groupList0 = $redis->smembers($notificationset);
		if ($groupList0 == null) {
			$notificationGroups = $redis->smembers($notificationset1);
		} else {
			$notificationGroups = $groupList0;
		}
		if (count($notificationGroups) > 0) {
			$notification = implode(",", $notificationGroups);
			$redis->hset("H_Notification_Map_User", $userId, $notification);
		}
		if ($enabledebug == 'Enable') {
			$redis->hmset('H_UserId_Cust_Map', $userId . ':Enable', 'oneDay');
			$validity = 1 * 86400;
			$grouplist = $redis->smembers($userId);
			foreach ($grouplist as $key => $groupId) {
				$vehicleList = $redis->smembers($groupId);
				foreach ($vehicleList as $key => $vehicleId) {
					$redis->set('EnableLog:' . $vehicleId . ':' . $fcode, $userId);
					$redis->expire('EnableLog:' . $vehicleId . ':' . $fcode, $validity);
					$deviceId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicleId);
					$pub = $redis->PUBLISH('sms:topicNetty', $deviceId);
				}
			}
		} else {
			$redis->hmset('H_UserId_Cust_Map', $userId . ':Enable', '0');
		}

		// redirect
		session()->flash('message', 'Successfully created ' . $userId . '!');
		return redirect()->to('vdmUsers');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function show($id)
	{

		$username = auth()->id();

		$redis = Redis::connection();
		$userId = $id;
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$mobileNo = $redis->hget('H_UserId_Cust_Map', $userId . ':mobileNo');
		$email = $redis->hget('H_UserId_Cust_Map', $userId . ':email');
		$vehicleGroups = $redis->smembers($userId);
		$value = false;
		if ($redis->sismember('S_Users_Virtual_' . $fcode, $userId) == 1) {
			$value = true;
		}

		$showData = [
			'userId' => $userId,
			'vehicleGroups' => $vehicleGroups,
			'mobileNo' => $mobileNo,
			'email' => $email,
			'value' => $value
		];
		return view('vdm.users.show', $showData);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function edit($id)
	{

		$userId = $id;

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$mobileNo = $redis->hget('H_UserId_Cust_Map', $userId . ':mobileNo');
		$email = $redis->hget('H_UserId_Cust_Map', $userId . ':email');
		$cc_email = $redis->hget('H_UserId_Cust_Map', $userId . ':cc_email');
		$zoho = $redis->hget('H_UserId_Cust_Map', $userId . ':zoho');
		$companyName = $redis->hget('H_UserId_Cust_Map', $userId . ':companyName');
		// $gender = $redis->hget('H_UserId_Cust_Map', $userId . ':gender');
		// $systemName = $redis->hget('H_UserId_Cust_Map', $userId . ':systemName');
		//Enable log
		// $enablelogLits=$redis->keys('EnableUserLog:'.$userId.':'.$fcode);
		$enablelogLits = $redis->exists('EnableUserLog:' . $userId . ':' . $fcode);
		if ($enablelogLits == '1') {
			$enable = 'Enable';
		} else {
			$enable = 'Disable';
		}

		$redisGrpId = 'S_Groups_' . $fcode;
		if (session()->get('cur') == 'dealer') {
			$redisGrpId = 'S_Groups_Dealer_' . $username . '_' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$redisGrpId = 'S_Groups_Admin_' . $fcode;
		}


		$groupList = $redis->smembers($redisGrpId);
		sort($groupList);
		$groupList = str_replace('.', '$', $groupList);
		$vehicleGroups = null;

		$selectedGroups = $redis->smembers($userId);
		//$selectedGroups = str_replace('.', '$', $selectedGroups);

		foreach ($groupList as $key => $value) {
			$uppercheck = strtoupper($value);
			if ($uppercheck == $value) {
				$vehicleGroups = Arr::add($vehicleGroups, $value, $value);
			}
		}
		$vehicleGroups = str_replace('$', '.', $vehicleGroups);
		$value = false;
		if ($redis->sismember('S_Users_Virtual_' . $fcode, $userId) == 1) {
			$value = true;
		}
		$assetuser = false;
		if ($redis->sismember('S_Users_AssetTracker_' . $fcode, $userId) == 1) {
			$assetuser = true;
		}
		$editData = [
			'userId' => $userId,
			'vehicleGroups' => $vehicleGroups,
			'mobileNo' => $mobileNo,
			'email' => $email,
			'zoho' => $zoho,
			'selectedGroups' => $selectedGroups,
			'value' => $value,
			'assetuser' => $assetuser,
			'companyName' => $companyName,
			'cc_email' => $cc_email,
			'enable' => $enable,
		];
		return view('vdm.users.edit', $editData);
	}

	public function notification($id)
	{

		$userId = $id;

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$notificationset = 'S_VAMOS_NOTIFICATION_' . $fcode;
		$notificationset1 = 'S_VAMOS_NOTIFICATION';

		$notification = $redis->hget("H_Notification_Map_User", $userId);
		$notificationArray = array();
		if ($notification !== null) {
			$notificationArray = explode(",", $notification);
		}
		$groupList0 = $redis->smembers($notificationset);
		if ($groupList0 == null) {
			$groupList = $redis->smembers($notificationset1);
		} else {
			$groupList = $groupList0;
		}
		$notificationGroups = null;
		sort($groupList);
		foreach ($groupList as $key => $value) {
			$notificationGroups = Arr::add($notificationGroups, $value, $value);
		}


		return view('vdm.users.notification', [
			'userId' => $userId,
			'notificationGroups' => $notificationGroups,
			'notificationArray' => $notificationArray
		]);
	}

	public function updateFn()
	{
		$userId = request()->get('userId');

		$username = auth()->id();
		$redis = Redis::connection();
		$oldNotification = $redis->hget("H_Notification_Map_User", $userId);
		$oldNotificationArray = array();
		$notificationGroups = array();
		if ($oldNotification !== null) {
			$oldNotificationArray = explode(",", $oldNotification);
		}
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$notificationGroups = request()->get('notificationGroups');

		if (is_null($notificationGroups)) $notificationGroups = [];

		if (count($notificationGroups) > 0) {
			$notification = implode(",", $notificationGroups);
			$availableNotification = implode(",", $notificationGroups);
			$redis->hset("H_Notification_Map_User", $userId, $notification);
		} else {
			$availableNotification = "";
			$redis->hset("H_Notification_Map_User", $userId, '');
		}
		try {
			$addedNoti = [];
			$removedNoti = [];
			if (count($notificationGroups) > 0) {
				$addedNoti = array_diff($notificationGroups, $oldNotificationArray);
				$removedNoti = array_diff($oldNotificationArray, $notificationGroups);
			}

			if (count($removedNoti) != 0) $removedNotifications = implode(",", $removedNoti);
			else $removedNotifications = '';
			if (count($addedNoti) != 0) $addedNotifications = implode(",", $addedNoti);
			else $addedNotifications = '';

			$oldData = [];
			$newData = [];
			$newData['addedNotification'] = $addedNotifications;
			$oldData['removedNotification'] = $removedNotifications;
			$mysqlDetails = [
				'userIpAddress' => session()->get('userIP', '-'),
				'userName'=> $username,
				'type'=>config()->get('constant.user'),
				'name'=>$userId,
				'status'=>config()->get('constant.notificationEdited'),
				'oldData'=>json_encode($oldData),
				'newData'=>json_encode($newData),
			];
			AuditNewController::updateAudit($mysqlDetails);
		} catch (Exception $e) {
			AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
			logger('Exception_' . $userId . ' CreateAuditUser error--->' . $e->getMessage() . ' line-->' . $e->getLine());
			logger('Error inside AuditUser Notification Edit' . $e->getMessage());
		}
	}

	public function updateNotification()
	{
		$userId = request()->get('userId');
		$notify = new VdmUserController;
		$notify->updateFn();
		return redirect()->to('vdmUsers')->with(['message' => "Successfully updated  Notification  $userId !"]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function update($id)
	{

		$userId = $id;

		$username 	= auth()->id();
		$redis 		= Redis::connection();
		$fcode 		= $redis->hget('H_UserId_Cust_Map', $userId . ':fcode');
		$oldMob 	= $redis->hget('H_UserId_Cust_Map', $userId . ':mobileNo');
		$oldmail	= $redis->hget('H_UserId_Cust_Map', $userId . ':email');
		$old_cc		= $redis->hget('H_UserId_Cust_Map', $userId . ':cc_email');
		// $oldgender      = $redis->hget('H_UserId_Cust_Map', $userId .  ':gender');
		$oldzoho       = $redis->hget('H_UserId_Cust_Map', $userId .  ':zoho');
		$oldcompanyName       = $redis->hget('H_UserId_Cust_Map', $userId .  ':companyName');
		// $oldsystemName      = $redis->hget('H_UserId_Cust_Map', $userId .  ':systemName');
		$oldVirtual = $redis->sismember('S_Users_Virtual_' . $fcode,  $userId);
		$oldAsset = $redis->sismember('S_Users_AssetTracker_' . $fcode,  $userId);
		$oldGroup	= (array) $redis->smembers($userId);
		
		// $isVirtualuserOld = $oldVirtual == 1 ? "true":"fasle";
		// $isAssetUserOld = $oldAsset == 1 ? "true":"fasle";
		$oldUserData =[
			'mobileNo'=>$oldMob,
			'email'=>$oldmail,
			'cc_email'=>$old_cc,
			'zoho'=>$oldzoho,
			'companyName'=>$oldcompanyName,
			// 'virtualaccount'=>$oldVirtual,
			// 'assetuser'=>$oldAsset,
			// 'enabledebug'=>$enabledebug
		];
		// return $oldUserData;
		$own = 'OWN';

		$rules = array(
			'mobileNo' => 'required',
			'email' => 'required',
			'vehicleGroups' => 'required'
		);
		$validator = validator()->make(request()->all(), $rules);
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator)->withInput();
		} else {
			// store
			$vehicleGroups = request()->get('vehicleGroups');
			$vehicleGroups = str_replace('$', '.', $vehicleGroups);

			$result = array_diff($oldGroup, $vehicleGroups);
			foreach ($result as $delGrp) {
				// $delVehiList = $redis->smembers('S_' . $delGrp);
				$checkUD = $redis->srem('S_' . $delGrp, $userId);
			}
			$resultAdd = array_diff($vehicleGroups, $oldGroup);
			foreach ($resultAdd as $addGrp) {
				// $addVehiList = $redis->smembers('S_' . $addGrp);
				$addU = $redis->sadd('S_' . $addGrp, $userId);
			}
			///
			$mobileNo = request()->get('mobileNo');
			$email = request()->get('email');
			$cc_email = request()->get('cc_email');
			$zoho = request()->get('zoho');
			$companyName = request()->get('companyName');
			$virtualaccount = request()->get('virtualaccount');
			$assetuser = request()->get('assetuser');
			// $isVirtualuserNew = isset($virtualaccount) ? "true":"fasle";
			// $isAssetUserNew = isset($assetuser) ? "true":"fasle";

			// return request()->all();

			$newUserData =[
				'mobileNo'=>$mobileNo,
				'email'=>$email,
				'cc_email'=>$cc_email,
				'zoho'=>$zoho,
				'companyName'=>$companyName,
				// 'virtualaccount'=>isset($virtualaccount),
				// 'assetuser'=>isset($assetuser),
			];
			$redis->del($userId);
			$redis->sadd($userId, $vehicleGroups);

			$redis->hmset('H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo, $userId . ':email', $email, $userId . ':zoho', $zoho, $userId . ':companyName', $companyName, $userId . ':cc_email', $cc_email);

			try {
				// $vehicleList = array();
				// foreach ($vehicleGroups as $key => $gropName) {
				// 	$gpVeh = $redis->smembers($gropName);
				// 	foreach ($gpVeh as $key => $vehicle) {
				// 		$vehicleList = Arr::add($vehicleList, $vehicle, $vehicle);
				// 	}
				// }
				// $oldUserData['removedGroups'] = implode(", </br>", str_replace(":$fcode","",$result));
				// $oldData = $oldUserData;
				// $newUserData['addedGroups'] = implode(", </br>", str_replace(":$fcode","",$resultAdd));
				// $newData = $newUserData;
				$oldData = array_diff($oldUserData,$newUserData);
				$newData = array_diff($newUserData,$oldUserData);
				// dd($oldData,$newData,$oldUserData,$newUserData);
				if(count($resultAdd)){
					$newData['addedGroups'] = join(",",str_replace(":$fcode","",$resultAdd));
				}
				if(count($result)){
					$oldData['removedGroups'] = join(",",str_replace(":$fcode","",$result));
				}
				$mysqlDetails = [
					'userIpAddress' => session()->get('userIP', '-'),
					'userName'=> $username,
					'type'=>config()->get('constant.user'),
					'name'=>$id,
					'status'=>config()->get('constant.updated'),
					'oldData'=>json_encode($oldData),
					'newData'=>json_encode($newData),
				];
				AuditNewController::updateAudit($mysqlDetails);
			} catch (Exception $e) {
				AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
				logger('Exception_' . $userId . ' CreateAuditUser error--->' . $e->getMessage() . ' line-->' . $e->getLine());
				logger('Error inside AuditUser Update' . $e->getMessage());
			}


			if ($virtualaccount == 'value') {
				$redis->sadd('S_Users_Virtual_' . $fcode, $userId);
			} else {
				$redis->srem('S_Users_Virtual_' . $fcode, $userId);
			}
			if ($assetuser == 'value') {
				$redis->sadd('S_Users_AssetTracker_' . $fcode, $userId);
			} else {
				$redis->srem('S_Users_AssetTracker_' . $fcode, $userId);
			}


			// $mailId = array();
			// $franDetails_json = $redis->hget('H_Franchise', $fcode);
			// $franchiseDetails = json_decode($franDetails_json, true);
			// if (isset($franchiseDetails['email1']) == 1) {
			// 	$mailId[] = $franchiseDetails['email2'];
			// }

			// if (session()->get('cur') == 'dealer') {
			// 	$own = $redis->hget('H_UserId_Cust_Map', $username . ':OWN');
			// 	$mailId[] =   $redis->hget('H_UserId_Cust_Map', $username . ':email');
			// }

			// $oldList = array();
			// $newList = array();

			// if ($mobileNo != $oldMob) {
			// 	$oldList = Arr::add($oldList, 'Mobile No ', $oldMob);
			// 	$newList = Arr::add($newList, 'Mobile No ', $mobileNo);
			// }
			// if ($companyName != $oldcompanyName) {
			// 	$oldList = Arr::add($oldList, 'Company Name ', $oldcompanyName);
			// 	$newList = Arr::add($newList, 'Company Name ', $companyName);
			// }
			// if ($oldmail != $email) {
			// 	$oldList = Arr::add($oldList, 'Email ', $oldmail);
			// 	$newList = Arr::add($newList, 'Email ', $email);
			// }
			// if ($old_cc != $cc_email) {
			// 	$oldList = Arr::add($oldList, 'Cc Email ', $old_cc);
			// 	$newList = Arr::add($newList, 'Cc Email ', $cc_email);
			// }
			// if ($zoho != $oldzoho) {
			// 	$oldList = Arr::add($oldList, 'Zoho', $oldzoho);
			// 	$newList = Arr::add($newList, 'Zoho ', $zoho);
			// }

			// if ($redis->sismember('S_Users_Virtual_' . $fcode,  $userId) != $oldVirtual) {
			// 	$oldList = Arr::add($oldList, 'Virtual Account ', (($oldVirtual == 1) ? true : false));
			// 	$newList = Arr::add($newList, 'Virtual Account ', (($redis->sismember('S_Users_Virtual_' . $fcode,  $userId) == 1) ? true : false));
			// }
			// if ($redis->sismember('S_Users_AssetTracker_' . $fcode,  $userId) != $oldAsset) {
			// 	$oldList = Arr::add($oldList, 'Asset User ', (($oldAsset == 1) ? true : false));
			// 	$newList = Arr::add($newList, 'Asset User ', (($redis->sismember('S_Users_AssetTracker_' . $fcode,  $userId) == 1) ? true : false));
			// }
			// if ($vehicleGroups != $oldGroup) {
			// 	$oldList = Arr::add($oldList, 'Group List ', implode(",", $vehicleGroups));
			// 	$newList = Arr::add($newList, 'Group List ', implode(",", $oldGroup));
			// }

			//enable log
			$enabledebug = request()->get('enable');

			if ($enabledebug == 'Enable') {
				$validity = 86400;
				$grouplist = $redis->smembers($userId);
				foreach ($grouplist as $key => $groupId) {
					$vehicleList = $redis->smembers($groupId);
					foreach ($vehicleList as $key => $vehicleId) {
						$redis->set('EnableLog:' . $vehicleId . ':' . $fcode, $userId);
						$redis->expire('EnableLog:' . $vehicleId . ':' . $fcode, $validity);
						$deviceId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicleId);
						$redis->PUBLISH('sms:topicNetty', $deviceId);
					}
				}
				$redis->set('EnableUserLog:' . $userId . ':' . $fcode, 'yes');
				$redis->expire('EnableUserLog:' . $userId . ':' . $fcode, $validity);
			}
			if ($enabledebug == 'Disable') {
				$grouplist = $redis->smembers($userId);
				$redis->del('EnableUserLog:' . $userId . ':' . $fcode);
				foreach ($grouplist as $key => $groupId) {
					$vehicleList = $redis->smembers($groupId);
					foreach ($vehicleList as $key => $vehicleId) {
						$redis->del('EnableLog:' . $vehicleId . ':' . $fcode);
						$deviceId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicleId);
						$redis->PUBLISH('sms:topicNetty', $deviceId);
					}
				}
			}
			return redirect()->to('vdmUsers')->with(['message' => "Successfully updated $userId !"]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function destroy($id)
	{

		$username = auth()->id();
		$userId = $id;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$redis->srem('S_Users_' . $fcode, $userId);
		$redis->srem('S_Users_Dealer_' . $username . '_' . $fcode, $userId);
		$redis->srem('S_Users_Admin_' . $fcode, $userId);
		$getUser = $redis->smembers($userId);
		foreach ($getUser as $key => $getU) {
			$redis->srem('S_' . $getU, $userId);
		}

		$redis->del($userId);
		$redis->del('S_Orgs_' . $userId . '_' . $fcode);

		$email = $redis->hget('H_UserId_Cust_Map', $userId . ':email');
		$redis->hdel('H_UserId_Cust_Map', $userId . ':fcode', $userId . ':mobileNo', $userId . ':email', $userId . ':password', $userId . ':companyName', $userId . ':gender', $userId . ':zoho', $userId . ':systemName');
		$redis->hdel('H_Notification_Map_User', $userId);

		DB::table('users')->where('username', $userId)->delete();
		try {
			// $auth = app('firebase.auth');
			// FirebaseController::deleteUser($userId);
		} catch (FirebaseException $e) {
			logger($e->getMessage());
			return redirect()->back()->withErrors($e->getMessage());
		}

		try {

			$mysqlDetails = [
				'userIpAddress' => session()->get('userIP', '-'),
				'userName'=> $username,
				'type'=>config()->get('constant.user'),
				'name'=>$userId,
				'status'=>config()->get('constant.deleted'),
				'oldData'=>"",
				'newData'=>"",
			];
			AuditNewController::updateAudit($mysqlDetails);
		} catch (Exception $e) {
			AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
			logger('Exception_' . $userId . ' CreateAuditUser error--->' . $e->getMessage() . ' line-->' . $e->getLine());
			logger('Error inside AuditUser Delete' . $e->getMessage());
		}

		session()->put('email', $email);

		//	Email Service activeted - 06-jul-2018 

		// $emailFcode = $redis->hget('H_DealerDetails_' . $fcode, $username);
		// $emailFile = json_decode($emailFcode, true);
		// $email1 = $emailFile['email'];

		// $emailFcode1 = $redis->hget('H_Franchise', $fcode);
		// $emailFile1 = json_decode($emailFcode1, true);
		// $email2 = $emailFile1['email2'];

		// if (session()->get('cur') == 'dealer') {
		// 	$emails = array($email1);
		// } else if (session()->get('cur') == 'admin') {
		// 	$emails = array($email2);
		// }

		// Mail::queue('emails.welcome', array('fname' => $fcode, 'userId' => $userId), function ($message) use ($emails, $userId) {
		// 	logger("Inside email :" . session()->get('email'));

		// 	$message->to($emails)->subject('User Id : ' . $userId . ' deleted');
		// });

		session()->flash('message', 'Successfully deleted ' . $userId . '!');
		return redirect()->to('vdmUsers');
	}

	public function userIdCheck()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$userId = request()->get('id');
		$valFirst = $redis->hget('H_UserId_Cust_Map', ucfirst(strtolower($userId)) . ':fcode');
		$valUpper = $redis->hget('H_UserId_Cust_Map', strtolower($userId) . ':fcode');
		$val1 = $redis->sismember('S_Users_' . $fcode, $userId);
		$val = $redis->hget('H_UserId_Cust_Map', $userId . ':fcode');
		$val1 = $redis->sismember('S_Users_' . $fcode, $userId);
		if ($val1 == 1 || isset($val) || isset($valFirst) || isset($valUpper)) {
			return ["data" => 'Already exist. Please use different id'];
		}
		if ($userId == 'ADMIN' || $userId == 'Admin' || $userId == 'admin') {
			return ["data" => 'Name with admin not acceptable'];
		}
		return ["data" => false];
	}


	public function notificationFrontend()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$notification = $redis->hget("H_Notification_Map_User", $username);
		$notificationArray = array();
		if ($notification !== null) {
			$notificationArray = explode(",", $notification);
		}
		$notificationset = 'S_VAMOS_NOTIFICATION_' . $fcode;
		$notificationset1 = 'S_VAMOS_NOTIFICATION';
		$groupList0 = $redis->smembers($notificationset);
		if ($groupList0 == null) {
			$groupList = $redis->smembers($notificationset1);
		} else {
			$groupList = $groupList0;
		}
		$notificationGroups = array();
		foreach ($groupList as $key => $value) {

			$notificationGroups = Arr::add($notificationGroups, $value, $value);
			$notificationGroups[$value] = 'false';

			if (sizeof($notificationArray) > 0) {
				foreach ($notificationArray as $ky => $val) {

					if ($val ==  $value) {
						$notificationGroups[$value] = 'true';
					}
				}
			}
		}

		return (sizeof($notificationGroups) > 0) ? $notificationGroups : 'fail';
	}

	public function notificationFrontendUpdate()
	{

		try {
			$notify = new VdmUserController;
			$notify->updateFn();
			return 'success';
		} catch (\Exception $e) {
			return 'fail';
		}
	}

	public function reportsOld($id)
	{

		$totalReportList = array();
		$list = array();
		$totalList = array();
		$reportsList = array();
		$username = auth()->id();
		$user = $id;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$totalReport = null;
		if (session()->get('cur') == 'dealer') {
			$dealeReportLen = $redis->scard('S_Users_Reports_Dealer_' . $username . '_' . $fcode);
			$adminLength =      $redis->scard('S_Users_Reports_Admin_' . $fcode);
			if ($adminLength >= $dealeReportLen) {
				$totalReports = $redis->smembers('S_Users_Reports_Dealer_' . $username . '_' . $fcode);
			} else {
				$totalReports = $redis->smembers('S_Users_Reports_Admin_' . $fcode);
				$redis->del('S_Users_Reports_Dealer_' . $username . '_' . $fcode);
				foreach ($totalReports as $key => $value) {
					$redis->sadd('S_Users_Reports_Dealer_' . $username . '_' . $fcode, $value);
				}
			}
		} else if (session()->get('cur') == 'admin') {
			$totalReports = $redis->smembers('S_Users_Reports_Admin_' . $fcode);
		}
		$isVirtualuser = $redis->sismember("S_Users_Virtual_" . $fcode, $user);
		$virtualReports = array();
		if ($isVirtualuser) {
			$virtualReports = $redis->smembers('S_UserVirtualReports');
		}
		if ($totalReports != null) {
			foreach ($totalReports as $key => $value) {
				if (in_array($value, $virtualReports)) {
					logger('checking');
				} else {
					$totalReport[explode(":", $value)[1]][] = $value;
				}
			}
			$totalList = $totalReport;
		}
		if ($totalList == null) {
			$totalReport = $redis->keys("*_Reports");
			$report[] = array();
			foreach ($totalReport as $key => $getReport) {
				$specReports = $redis->smembers($getReport);
				foreach ($specReports as $key => $ReportName) {
					// $report[]=$ReportName.':'.$reportType[0];
				}
				$reportsList[] = $getReport;
				$totalReportList[] = $report;
				$totalList[$getReport] = $report;
				$report = null;
				$dealerOrUser = $redis->sismember('S_Dealers_' . $fcode, $user);
				if ($dealerOrUser) {
					return redirect()->to('vdmDealers');
				} else {
					return redirect()->to('vdmUsers');
				}
			}
		}
		$dealerOrUser = $redis->sismember('S_Dealers_' . $fcode, $user);
		if ($dealerOrUser) {
			$userReports = $redis->smembers("S_Users_Reports_Dealer_" . $user . '_' . $fcode);
		} else {
			$userReports = $redis->smembers("S_Users_Reports_" . $user . '_' . $fcode);
		}
		$sorttotalList = array_keys($totalList);
		sort($sorttotalList);
		$totalListtemp = null;
		foreach ($sorttotalList as $key => $value) {
			$totalListtemp[$value] =  $totalList[$value];
		}
		$totalList = $totalListtemp;
		$reportData = [
			'reportsList' => $reportsList,
			'totalReportList' => $totalReportList,
			'totalList' => $totalList,
			'userReports' => $userReports,
			'userId' => $user
		];
		if ($userReports == null && $totalReportList == null) {
			session()->flash('message', ' No Reports Found !');
		}
		return view('vdm.users.reports', $reportData);
	}
	public function reports($id)
	{

		$totalReportList = array();
		$list = array();
		$totalList = array();
		$reportsList = array();
		$username = auth()->id();
		$user = $id;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$totalReport = null;
		if (session()->get('cur') == 'dealer') {
			$dealeReportLen = $redis->scard('S_Users_Reports_Dealer_' . $username . '_' . $fcode);
			$adminLength = $redis->scard('S_Users_Reports_Admin_' . $fcode);
			if ($adminLength >= $dealeReportLen) {
				$totalReports = $redis->smembers('S_Users_Reports_Dealer_' . $username . '_' . $fcode);
			} else {
				$totalReports = $redis->smembers('S_Users_Reports_Admin_' . $fcode);
				$redis->del('S_Users_Reports_Dealer_' . $username . '_' . $fcode);
				foreach ($totalReports as $key => $value) {
					$redis->sadd('S_Users_Reports_Dealer_' . $username . '_' . $fcode, $value);
				}
			}
		} else if (session()->get('cur') == 'admin') {
			$totalReports = $redis->smembers('S_Users_Reports_Admin_' . $fcode);
		}
		// $isVirtualuser = $redis->sismember("S_Users_Virtual_" . $fcode, $user);
		// $virtualReports = array();
		// if ($isVirtualuser) {
		// 	$virtualReports = $redis->smembers('S_UserVirtualReports');
		// }
		$notAccessReports = [];
		$isDealer = $redis->sismember('S_Dealers_' . $fcode, $user);

		if (!$isDealer && session()->get('cur1') == "prePaidAdmin") {
			$maxReport = 0;
			$radio = [
				'StarterPlus' => 1,
				'Starter' => 2,
				'Basic' => 3,
				'Advance' => 4,
				'Premium' => 5,
				'PremiumPlus' => 6
			];
			$groupList = $redis->smembers($user);
			if (is_null($groupList)) $groupList = [];
			foreach ($groupList as $group) {
				$vehicleList =  $redis->smembers($group);
				if (is_null($vehicleList)) $vehicleList = [];
				foreach ($vehicleList as $vehicle) {
					$refData = $redis->hget("H_RefData_$fcode", $vehicle);
					$refData = json_decode($refData, true);
					$licence = $refData["Licence"] ?? "PremiumPlus";
					if ($maxReport <= $radio[$licence]) {
						$maxReport = $radio[$licence];
						if ($maxReport == 6) break;
					}
				}
				if ($maxReport == 6) break;
			}
			if (array_search($maxReport, $radio) == 0) {
				return redirect()->to('vdmUsers')->withErrors('There is no vehicles under this User');
			}
			$notAccessReports = $redis->smembers("S_NotAccessReports_" . array_search($maxReport, $radio));
		}
		if ($totalReports != null) {
			sort($totalReports);
			foreach ($totalReports as $key => $value) {
				if (!in_array(last(explode(":", $value)), $notAccessReports)) {
					$totalReport[last(explode(":", $value))][] = $value;
				}
			}
			$totalList = $totalReport;
		}
		if ($isDealer) {
			$userReports = $redis->smembers("S_Users_Reports_Dealer_" . $user . '_' . $fcode);
		} else {
			$userReports = $redis->smembers("S_Users_Reports_" . $user . '_' . $fcode);
		}
		$sorttotalList = array_keys($totalList);
		sort($sorttotalList);
		$totalListtemp = null;
		foreach ($sorttotalList as $key => $value) {
			$totalListtemp[$value] =  $totalList[$value];
		}
		$totalList = $totalListtemp;
		$reportData = [
			// 'reportsList' => $reportsList,
			// 'totalReportList' => $totalReportList,
			'totalList' => $totalList,
			'userReports' => $userReports,
			'userId' => $user
		];
		if ($userReports == null && $totalReportList == null) {
			session()->flash('message', ' No Reports Found !');
		}

		return view('vdm.users.reports', $reportData);
	}

	public function updateReports()
	{

		$userId = request()->get('userId');
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $userId . ':fcode');
		$reportName = request()->get('reportName');
		$isDealer = false;
		$isDealer = $redis->sismember('S_Dealers_' . $fcode, $userId);
		$removedReports = '';
		$addedReports = '';
		$availableReports = '';
		if ($isDealer) {
			$deleteDealerReports = $redis->smembers('S_Users_Reports_Dealer_' . $userId . '_' . $fcode);
		} else {
			$deleteUserReports = $redis->smembers("S_Users_Reports_" . $userId . '_' . $fcode);
		}
		if ($reportName != null) {
			if ($isDealer) {
				$prevReportList = $redis->smembers('S_Users_Reports_Dealer_' . $userId . '_' . $fcode);
				$redis->del("S_Users_Reports_Dealer_" . $userId . '_' . $fcode);
				foreach ($reportName as $key => $value) {
					$redis->sadd("S_Users_Reports_Dealer_" . $userId . '_' . $fcode, $value);
				}
				$removeList = array();
				$addList = array();
				$currentReportList = $redis->smembers('S_Users_Reports_Dealer_' . $userId . '_' . $fcode);
				foreach ($prevReportList as $key => $value) {
					if (in_array($value, $currentReportList)) {
						// $addList[] = $value;
					} else {
						$removeList[] = $value;
						$removedReports = $removedReports . ',' . $value;
					}
				}
				// added for added reports 
				foreach ($currentReportList as $key => $value) {
					if (!in_array($value, $prevReportList)) {
						//$addList[]=$value;
						$addedReports = $addedReports . ',' . $value;
					}
				}
				$userList = $redis->smembers("S_Users_Dealer_" . $userId . '_' . $fcode);
				foreach ($userList as $key => $dealer) {
					$userReport = $redis->smembers("S_Users_Reports_" . $dealer . '_' . $fcode); {
						if ($userReport == null) {

							foreach ($reportName as $key => $value) {
								$redis->sadd("S_Users_Reports_" . $dealer . '_' . $fcode, $value);
							}
						}
					}
				}

				if ($removeList != null) {
					$userList = $redis->smembers("S_Users_Dealer_" . $userId . '_' . $fcode);
					foreach ($userList as $key => $dealer) {
						$redis->srem("S_Users_Reports_" . $dealer . '_' . $fcode, $removeList);
					}
				}
			} else{
				$prevReportList = $redis->smembers('S_Users_Reports_' . $userId . '_' . $fcode);
				$redis->del("S_Users_Reports_" . $userId . '_' . $fcode);
				$redis->sadd("S_Users_Reports_" . $userId . '_' . $fcode, $reportName);
				$currentReportList = $redis->smembers("S_Users_Reports_" . $userId . '_' . $fcode);
				// added for removed reports
				// return $prevReportList;
				foreach ($prevReportList as $key => $value) {
					if (!in_array($value, $currentReportList)) {
						//$addList[]=$value;
						$removedReports = $removedReports . ',' . $value;
					}
				}

				// added for added reports 
				foreach ($currentReportList as $key => $value) {
					if (!in_array($value, $prevReportList)) {
						//$addList[]=$value;
						$addedReports = $addedReports . ',' . $value;
					}
				}
			}
		} else {

			if ($isDealer) {
				$currentReportList = $redis->smembers("S_Users_Reports_Dealer_" . $userId . '_' . $fcode);
				foreach ($currentReportList as $key => $value) {
					$removeList[] = $value;
				}
				$redis->del("S_Users_Reports_Dealer_" . $userId . '_' . $fcode);
				$userList = $redis->smembers("S_Users_Dealer_" . $userId . '_' . $fcode);
				foreach ($userList as $key => $dealer) {
					$redis->del("S_Users_Reports_" . $dealer . '_' . $fcode);
				}
			} else {
				$currentReportList = $redis->smembers("S_Users_Reports_" . $userId . '_' . $fcode);
				foreach ($currentReportList as $key => $value) {
					$removedReports = $removedReports . ',' . $value;
				}
				$redis->del("S_Users_Reports_" . $userId . '_' . $fcode);
			}
		}
		// return [$removedReports,$addedReports];
		// if ($reportName != null) {
		// 	// $availableReports= join(',',$reportName);
		// 	foreach ($reportName as $key => $value) {
		// 		$availableReports = $availableReports . ',' . $value;
		// 	}
		// } else {
		// 	if($isDealer){
		// 		foreach ($deleteDealerReports as $key => $value) {
		// 			$removedReports = $removedReports . ',' . $value;
		// 		}
		// 	}else{
		// 		foreach ($deleteUserReports as $key => $value) {
		// 			$removedReports = $removedReports . ',' . $value;
		// 		}
		// 	}
		// }
		try {
			if($isDealer){
				$type = config()->get('constant.dealer');
			}else{
				$type = config()->get('constant.user');
			}  
			$oldData = [
				'removedReports'=>$removedReports,
			];
			$newData = [
				'addedReports' => $addedReports,
			];
			$mysqlDetails = [
				'userIpAddress' => session()->get('userIP', '-'),
				'userName'=> $username,
				'type'=>$type,
				'name'=>$userId,
				'status'=>config()->get('constant.reportsEdited'),
				'oldData'=>json_encode($oldData),
				'newData'=>json_encode($newData),
			];
			AuditNewController::updateAudit($mysqlDetails);

		} catch (Exception $e) {
			// AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
			logger('Exception_' . $userId . ' CreateAuditUser error--->' . $e->getMessage() . ' line-->' . $e->getLine());
			logger('Error inside AuditDealer Reports Edit ' . $e->getMessage());
		}

		session()->flash('message', 'Successfully updated ' . $userId . ' reports !');
		return redirect()->to($isDealer ? 'vdmDealers' : 'vdmUsers')->with('message', "Successfully updated $userId  reports !");
	}

	public function scanNew($id)
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$redisUserCacheId = 'S_Users_' . $fcode;
		if (session()->get('cur') == 'dealer') {

			$redisUserCacheId = 'S_Users_Dealer_' . $username . '_' . $fcode;
		} else if (session()->get('cur') == 'admin') {
			$redisUserCacheId = 'S_Users_Admin_' . $fcode;
		}
		$userList = $redis->smembers($redisUserCacheId);
		$text_word1 = $id;
		$text_word = str_replace(' ', '', $text_word1);
		$text_word2 = strtoupper($text_word1);
		$cou = $redis->SCARD($redisUserCacheId);
		$orgLi = $redis->sScan($redisUserCacheId, 0, 'count', $cou, 'match', '*' . $text_word . '*');
		$orgL = $orgLi[1];
		$userGroups = null;
		$userGroupsArr = null;

		foreach ($orgL as $key => $value) {
			$userGroups = $redis->smembers($value);
			$userGroupsArr = Arr::add($userGroupsArr, $value, $userGroups);
		}
		return view('vdm.users.scan')->with('fcode', $fcode)->with('userGroupsArr', $userGroupsArr)->with('userList', $orgL);
	}

	public function sendExcel()
	{
		return Excel::download(new UserExport(), 'User_Details.xlsx');
	}
}
