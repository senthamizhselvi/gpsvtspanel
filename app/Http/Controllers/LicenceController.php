<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class LicenceController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function indexT()
	{

		$username = auth()->user()->displayName;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);

		if (session()->get('cur') == 'dealer') {
			$tmpOrgList = $redis->smembers('S_Organisations_Dealer_' . $username . '_' . $fcode);
		} else if (session()->get('cur') == 'admin') {
			$tmpOrgList = $redis->smembers('S_Organisations_Admin_' . $fcode);
		}
		$orgList = null;
		$orgList = Arr::add($orgList, '', 'select');
		$orgList = Arr::add($orgList, 'Default', 'Default');
		foreach ($tmpOrgList as $org) {
			$orgList = Arr::add($orgList, $org, $org);
		}
		if (session()->get('cur') == 'admin') {

			$franDetails_json = $redis->hget('H_Franchise', $fcode);
			$franchiseDetails = json_decode($franDetails_json, true);
			if (isset($franchiseDetails['availableLincence']) == 1)
				$availableLincence = $franchiseDetails['availableLincence'];
			else
				$availableLincence = '0';

			$numberofdevice = 0;
			$dealerId = null;
			$userList = null;
			$deviceAddCopyData = [
				'orgList' => $orgList,
				'availableLincence' => $availableLincence,
				'numberofdevice' => $numberofdevice,
				'dealerId' => $dealerId,
				'userList' => $userList
			];
			return view('vdm.business.deviceAddCopy', $deviceAddCopyData);
		} else if (session()->get('cur') == 'dealer') {

			$key = 'H_Pre_Onboard_Dealer_' . $username . '_' . $fcode;

			$details = $redis->hgetall($key);
			$devices = null;
			$devicestypes = null;
			$i = 0;
			foreach ($details as $key => $value) {
				$valueData = json_decode($value, true);
				$devices = Arr::add($devices, $i, $valueData['deviceid']);
				$devicestypes = Arr::add($devicestypes, $i, $valueData['deviceidtype']);
				$i++;
			}

			$dealerId = $redis->smembers('S_Dealers_' . $fcode);

			$orgArr = array();
			if ($dealerId != null) {
				foreach ($dealerId as $org) {
					$orgArr = Arr::add($orgArr, $org, $org);
				}
				$dealerId = $orgArr;
			} else {
				$dealerId = null;
				$dealerId = $orgArr;
			}

			$redisUserCacheId = 'S_Users_' . $fcode;
			$redisGrpCacheId = 'S_Users_';

			if (session()->get('cur') == 'dealer') {
				$redisUserCacheId = 'S_Users_Dealer_' . $username . '_' . $fcode;
			} else if (session()->get('cur') == 'admin') {
				$redisUserCacheId = 'S_Users_Admin_' . $fcode;
			}

			$userList = $redis->smembers($redisUserCacheId);

			$orgArra = array();
			$orgArra = Arr::add($orgArra, 'select', 'select');
			foreach ($userList as $org) {
				if (!$redis->sismember('S_Users_Virtual_' . $fcode, $org) == 1) {
					$orgArra = Arr::add($orgArra, $org, $org);
				}
			}
			$userList = $orgArra;

			$business1Data = [
				'devices' => $devices,
				'devicestypes' => $devicestypes,
				'dealerId' => $dealerId,
				'userList' => $userList,
				'orgList' => $orgList
			];
			return view('vdm.business.business1', $business1Data);
		}
	}

	public function index()
	{

		$username = auth()->user()->displayName;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		//get the Org list
		$month = LicenceController::getMonthT();

		$year = LicenceController::getYear();

		$own = LicenceController::getDealer();
		$Payment_Mode1 = array();
		$Payment_Mode = DB::select('select type from Payment_Mode');
		foreach ($Payment_Mode as  $org1) {
			$Payment_Mode1 = Arr::add($Payment_Mode1, $org1->type, $org1->type);
		}
		$Licence1 = array();
		$Licence = DB::select('select type from Licence');
		$Licence1 = Arr::add($Licence1, 'Both', 'Both');
		foreach ($Licence as  $org) {
			$Licence1 = Arr::add($Licence1, $org->type, $org->type);
		}

		$licenceData = [
			'year' => $year,
			'month' => $month,
			'Payment_Mode' => $Payment_Mode1,
			'Licence' => $Licence1,
			'own' => $own,
			'monthT' => date('m'),
			'yearT' => date('Y'),
			'modeT' => null,
			'typeT' => null,
			'ownT' => null
		];
		return view('vdm.licence.licence', $licenceData);
	}

	public static function getDealer()
	{
		$username = auth()->user()->displayName;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$own = array();
		$own = Arr::add($own, 'OWN', 'OWN');
		$dealerId = $redis->smembers('S_Dealers_' . $fcode);

		foreach ($dealerId as $or) {
			$own = Arr::add($own, $or, $or);
		}

		return $own;
	}

	public static function getMonthT()
	{
		$month = array();
		$month = Arr::add($month, 1, 'January');
		$month = Arr::add($month, 2, 'February');
		$month = Arr::add($month, 3, 'March');
		$month = Arr::add($month, 4, 'April');
		$month = Arr::add($month, 5, 'May');
		$month = Arr::add($month, 6, 'June');
		$month = Arr::add($month, 7, 'July');
		$month = Arr::add($month, 8, 'August');
		$month = Arr::add($month, 9, 'September');
		$month = Arr::add($month, 10, 'October');
		$month = Arr::add($month, 11, 'November');
		$month = Arr::add($month, 12, 'December');

		return $month;
	}

	public static function getYear()
	{
		$year = array();
		$year = Arr::add($year, 2015, 2015);
		$year = Arr::add($year, 2016, 2016);
		$year = Arr::add($year, 2017, 2017);
		$year = Arr::add($year, 2018, 2018);
		$year = Arr::add($year, 2019, 2019);
		$year = Arr::add($year, 2020, 2020);
		return $year;
	}

	public function store()
	{

		$username = auth()->user()->displayName;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		//get the Org list
		$month      = request()->get('month');
		$year      = request()->get('year');
		$mode      = request()->get('Payment_Mode');
		$type      = request()->get('Licence');
		$own      = request()->get('own');

		$monthT = $month;
		$yearT = $year;
		$modeT = $mode;
		$typeT = $type;
		if (session()->get('cur') == 'dealer') {
			$own = $username;
		}
		$ownT = $own;

		$dateT = new DashBoardController;

		//$daylast=new DateTime('last day of this month'); 

		$preMonthly = 0;
		$monthly = 0;
		$perQuater = 0;
		$quaterly = 0;
		$perHalfyearfly = 0;
		$halfyearfly = 0;
		$yearfly = 0;
		$yearfly2 = 0;
		$type1 = 1;
		$type2 = 2;
		if ($type == 'Basic') {
			$type1 = 1;
			$type2 = 1;
		} else if ($type == 'Advance') {
			$type1 = 2;
			$type2 = 2;
		}


		$preMonthly = DB::table('Vehicle_details')
			->where('fcode', $fcode)->where('belongs_to', $own)->where('payment_mode_id', 1)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 16, $month - 1, $year), $dateT->getDateT(59, 59, 23, 15, $month, $year)))->count();
		$monthly = DB::table('Vehicle_details')
			->where('fcode', $fcode)->where('belongs_to', $own)->where('payment_mode_id', 1)->whereIn('licence_id', array($type1, $type2))->where('renewal_date', '<=', $dateT->getDateT(59, 59, 23, 15, $month, $year))->count();

		if ($month == 1 || $month == 4 || $month == 7 || $month == 10) {
			$perQuater = DB::table('Vehicle_details')
				->where('fcode', $fcode)->where('belongs_to', $own)->where('payment_mode_id', 2)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 16, $month - 1, $year), $dateT->getDateT(59, 59, 23, 15, $month + 2, $year)))->count();
			$quaterly = DB::table('Vehicle_details')
				->where('fcode', $fcode)->where('belongs_to', $own)->where('payment_mode_id', 2)->whereIn('licence_id', array($type1, $type2))->where('renewal_date', '<=', $dateT->getDateT(59, 59, 23, 15, $month + 2, $year))->count();
		}
		if ($month == 4 || $month == 10) {
			$perHalfyearfly = DB::table('Vehicle_details')
				->where('fcode', $fcode)->where('belongs_to', $own)->where('payment_mode_id', 3)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 16, $month - 1, $year), $dateT->getDateT(59, 59, 23, 15, $month + 5, $year)))->count();
			$halfyearfly = DB::table('Vehicle_details')
				->where('fcode', $fcode)->where('belongs_to', $own)->where('payment_mode_id', 3)->whereIn('licence_id', array($type1, $type2))->where('renewal_date', '<=', $dateT->getDateT(59, 59, 23, 15, $month + 5, $year))->count();
		}
		$yearfly = DB::table('Vehicle_details')
			->where('fcode', $fcode)->where('belongs_to', $own)->where('payment_mode_id', 4)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 0, $month, $year - 1), $dateT->getDateT(0, 0, 0, 0, $month + 1, $year - 1)))->count();

		$yearfly2 = DB::table('Vehicle_details')
			->where('fcode', $fcode)->where('belongs_to', $own)->where('payment_mode_id', 5)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 0, $month, $year - 2), $dateT->getDateT(0, 0, 0, 0, $month + 1, $year - 2)))->count();

		$yearfly3 = DB::table('Vehicle_details')
			->where('fcode', $fcode)->where('belongs_to', $own)->where('payment_mode_id', 6)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 0, $month, $year - 3), $dateT->getDateT(0, 0, 0, 0, $month + 1, $year - 3)))->count();

		$yearfly4 = DB::table('Vehicle_details')
			->where('fcode', $fcode)->where('belongs_to', $own)->where('payment_mode_id', 7)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 0, $month, $year - 4), $dateT->getDateT(0, 0, 0, 0, $month + 1, $year - 4)))->count();
		$yearfly5 = DB::table('Vehicle_details')
			->where('fcode', $fcode)->where('belongs_to', $own)->where('payment_mode_id', 8)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 0, $month, $year - 5), $dateT->getDateT(0, 0, 0, 0, $month + 1, $year - 5)))->count();

		$month = LicenceController::getMonthT();

		$year = LicenceController::getYear();


		$own = LicenceController::getDealer();
		$Payment_Mode1 = array();
		$Payment_Mode = DB::select('select type from Payment_Mode');
		foreach ($Payment_Mode as  $org1) {
			$Payment_Mode1 = Arr::add($Payment_Mode1, $org1->type, $org1->type);
		}
		$Licence1 = array();
		$Licence = DB::select('select type from Licence');
		$Licence1 = Arr::add($Licence1, 'Both', 'Both');
		foreach ($Licence as  $org) {
			$Licence1 = Arr::add($Licence1, $org->type, $org->type);
		}

		$licenceCopy = [
			'year' => $year,
			'month' => $month,
			'Payment_Mode' => $Payment_Mode1,
			'Licence' => $Licence1,
			'own' => $own,
			'preMonthly' => $preMonthly,
			'monthly' => $monthly,
			'perQuater'=>$perQuater,
			'quaterly'=>$quaterly,
			'perHalfyearfly'=>$perHalfyearfly,
			'halfyearfly'=>$halfyearfly,
			'yearfly'=>$yearfly,
			'yearfly2'=>$yearfly2,
			'yearfly3'=>$yearfly3,
			'yearfly4'=>$yearfly4,
			'yearfly5'=>$yearfly5,
			'monthT'=>$monthT,
			'yearT'=>$yearT,
			'modeT'=>$modeT,
			'typeT'=>$typeT,
			'ownT'=>$ownT
		];
		return view('vdm.licence.licenceCopy',$licenceCopy);
	}

	public function ViewDevices($value)
	{
		$values = explode(";", $value);

		$username = auth()->user()->displayName;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$dateT = new DashBoardController;
		$type1 = 1;
		$type2 = 2;
		if ($values[3] == 'Basic') {
			$type1 = 1;
			$type2 = 1;
		} else if ($values[3] == 'Advance') {
			$type1 = 2;
			$type2 = 2;
		}
		$details = null;

		if ($values[2] == 1) {
			$details = DB::table('Vehicle_details')
				->where('fcode', $fcode)->where('belongs_to', $values[4])->where('payment_mode_id', 1)->whereIn('licence_id', array($type1, $type2))->where('renewal_date', '<=', $dateT->getDateT(59, 59, 23, 15, $values[0], $values[1]))->get();
		} else if ($values[2] == 2 && ($values[0] == 1 || $values[0] == 4 || $values[0] == 7 || $values[0] == 10)) {
			$details = DB::table('Vehicle_details')
				->where('fcode', $fcode)->where('belongs_to', $values[4])->where('payment_mode_id', 2)->whereIn('licence_id', array($type1, $type2))->where('renewal_date', '<=', $dateT->getDateT(59, 59, 23, 15, $values[0] + 2, $values[1]))->get();;
		} else if ($values[2] == 3 && ($values[0] == 4 || $values[0] == 10)) {
			$details = DB::table('Vehicle_details')
				->where('fcode', $fcode)->where('belongs_to', $values[4])->where('payment_mode_id', 3)->whereIn('licence_id', array($type1, $type2))->where('renewal_date', '<=', $dateT->getDateT(59, 59, 23, 15, $values[0] + 5, $values[1]))->get();
		} else if ($values[2] == 4) {
			$details = DB::table('Vehicle_details')
				->where('fcode', $fcode)->where('belongs_to', $values[4])->where('payment_mode_id', 4)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 0, $values[0], $values[1] - 1), $dateT->getDateT(0, 0, 0, 0, $values[0] + 1, $values[1] - 1)))->get();
		} else if ($values[2] == 5) {
			$details = DB::table('Vehicle_details')
				->where('fcode', $fcode)->where('belongs_to', $values[4])->where('payment_mode_id', 5)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 0, $values[0], $values[1] - 2), $dateT->getDateT(0, 0, 0, 0, $values[0] + 1, $values[1] - 2)))->get();
		} else if ($values[2] == 6) {
			$details = DB::table('Vehicle_details')
				->where('fcode', $fcode)->where('belongs_to', $values[4])->where('payment_mode_id', 6)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 0, $values[0], $values[1] - 3), $dateT->getDateT(0, 0, 0, 0, $values[0] + 1, $values[1] - 3)))->get();
		} else if ($values[2] == 7) {
			$details = DB::table('Vehicle_details')
				->where('fcode', $fcode)->where('belongs_to', $values[4])->where('payment_mode_id', 7)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 0, $values[0], $values[1] - 4), $dateT->getDateT(0, 0, 0, 0, $values[0] + 1, $values[1] - 4)))->get();
		} else if ($values[2] == 8) {
			$details = DB::table('Vehicle_details')
				->where('fcode', $fcode)->where('belongs_to', $values[4])->where('payment_mode_id', 8)->whereIn('licence_id', array($type1, $type2))->whereBetween('renewal_date', array($dateT->getDateT(0, 0, 0, 0, $values[0], $values[1] - 5), $dateT->getDateT(0, 0, 0, 0, $values[0] + 1, $values[1] - 5)))->get();
		}


		$lastdue = strtotime($dateT->getDateT(59, 59, 23, 0, date('m') + 2, date('Y')));

		$predue = strtotime($dateT->getDateT(59, 59, 23, 0, $values[0], $values[1]));
		$lastdue = $lastdue - $predue;

		$i = 0;	// }
		if (($values[2] == 8 || $values[2] == 7 || $values[2] == 6 || $values[2] == 5 || $values[2] == 4) && ($lastdue) > 0) {
			$i = 1;
		}
		$value = (string)$value;

		return view('vdm.licence.deviceView',[
			'i'=>$i,
			'details'=>$details,
			'valueT'=>$value
		]);
	}

	public function update()
	{
		$value = request()->get('tempVal');
		$vehicles = request()->get('vehicleList');
		$values = explode(";", $value);
		$username = auth()->user()->displayName;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$temp = "'";
		$i = 0;
		if (count($vehicles) > 0) {
			$payee = 4;
			foreach ($vehicles as $key => $val) {

				$valT = explode(';', $val);

				if ($i == 0) {
					$payee = $valT[1];
					$temp = $temp . $valT[0] . "'";
				} else {
					$temp = $temp . ",'" . $valT[0] . "'";
				}

				$i++;
			}
			$Pmode = 4;
			switch ($payee) {
				case '4':
					$Pmode = '"+1 year"';
					break;
				case '5':
					$Pmode = '"+2 year"';
					break;
				case '6':
					$Pmode = '"+3 year"';
					break;
				case '7':
					$Pmode = '"+4 year"';
					break;
				case '7':
					$Pmode = '"+5 year"';
					break;

				default:
					# code...
					break;
			}

			$conn = DB::connection()->getPdo();

			$sql = 'update Vehicle_details set renewal_date=(SELECT date (Vehicle_details1.renewal_date,' . $Pmode . ') as date FROM Vehicle_details as Vehicle_details1  where vehicle_id=Vehicle_details.vehicle_id) where vehicle_id in (' . $temp . ');';

			$stmt = $conn->prepare($sql);


			$stmt->execute();

			return redirect()->to('Licence')->withErrors('Successfully renewaled');
		} else {
			return redirect()->back()->withErrors('Nothing renewaled');
		}
	}
}
