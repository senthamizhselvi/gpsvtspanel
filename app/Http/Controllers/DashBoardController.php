<?php

namespace App\Http\Controllers;

use App\Models\RenewalDetails;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Schema;

class DashBoardController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


	public function index()
	{

		$username =  auth()->id();

		session()->forget('page');

		$redis = Redis::connection();

		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$temp = array();
		$dealerId = array();
		$userGroupsArr = array();
		$vehiclecount = array();
		$numberofStarterLicence = 0;
		$availableStarterLicence = 0;

		if (session()->get('cur') == 'dealer') {
			if (session()->get('cur1') == 'prePaidAdmin') {
				$vehicleCnt = $redis->scard('S_Vehicles_Dealer_' . $username . '_' . $fcode);
				$fname = $username;


				$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $username);
				$detail = json_decode($detailJson, true);

				$numberofStarterPlusLicence = isset($detail['numofStarterPlus']) ? $detail['numofStarterPlus'] : '0';
				$availableStarterPlusLicence = isset($detail['avlofStarterPuls']) ? $detail['avlofStarterPuls'] : '0';
				$numberofStarterLicence = isset($detail['numofStarter']) ? $detail['numofStarter'] : '0';
				$availableStarterLicence = isset($detail['avlofStarter']) ? $detail['avlofStarter'] : '0';
				$numberofBasicLicence = isset($detail['numofBasic']) ? $detail['numofBasic'] : '0';
				$numberofAdvanceLicence = isset($detail['numofAdvance']) ? $detail['numofAdvance'] : '0';
				$numberofPremiumLicence = isset($detail['numofPremium']) ? $detail['numofPremium'] : '0';
				$numberofPremPlusLicence = isset($detail['numofPremiumPlus']) ? $detail['numofPremiumPlus'] : '0';
				$availableBasicLicence = isset($detail['avlofBasic']) ? $detail['avlofBasic'] : '0';
				$availableAdvanceLicence = isset($detail['avlofAdvance']) ? $detail['avlofAdvance'] : '0';
				$availablePremiumLicence = isset($detail['avlofPremium']) ? $detail['avlofPremium'] : '0';
				$availablePremPlusLicence = isset($detail['avlofPremiumPlus']) ? $detail['avlofPremiumPlus'] : '0';

				$website = isset($detail['website']) ? $detail['website'] : '';
				$address = isset($detail['email']) ? $detail['email'] : '';
				$users_count = $redis->scard('S_Users_Dealer_' . $username . '_' . $fcode);
				$Group_count = $redis->scard('S_Groups_Dealer_' . $username . '_' . $fcode);

				$ttlused = ($numberofStarterPlusLicence + $numberofStarterLicence + $numberofBasicLicence + $numberofAdvanceLicence + $numberofPremiumLicence + $numberofPremPlusLicence) - ($availableStarterPlusLicence + $availableStarterLicence + $availableBasicLicence + $availableAdvanceLicence + $availablePremiumLicence + $availablePremPlusLicence);

				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'StarterPlus' AND User_Name ='$username' ";
				$sp_onboard_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Starter' AND User_Name ='$username' ";
				$st_onboard_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Basic' AND User_Name ='$username' ";
				$bc_onboard_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Advance' AND User_Name ='$username'";
				$ad_onboard_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Premium' AND User_Name ='$username'";
				$pr_onboard_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'PremiumPlus' AND User_Name ='$username'";
				$prpl_onboard_count = DashBoardController::getSqlcount($max);
				//renew
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'StarterPlus' AND User_Name ='$username'";
				$sp_renew_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Starter' AND User_Name ='$username'";
				$st_renew_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Basic' AND User_Name ='$username'";
				$bc_renew_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Advance' AND User_Name ='$username'";
				$ad_renew_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Premium' AND User_Name ='$username'";
				$pr_renew_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'PremiumPlus' AND User_Name ='$username'";
				$prpl_renew_count = DashBoardController::getSqlcount($max);


				$dealer = null;
				$dealerNoSt = 0;
				$dealerNoBc = 0;
				$dealerNoAd = 0;
				$dealerNoPr = 0;
				$dealerNoPrPl = 0;
				$dealer_count = 0;

				$st_dealer_tol = 0;
				$sp_dealer_tol = 0;
				$bc_dealer_tol = 0;
				$ad_dealer_tol = 0;
				$pr_dealer_tol = 0;
				$prpl_dealer_tol = 0;

				$dashcardData = [
					'vehicleCnt' => $vehicleCnt,
					'fname' => $fname,
					'website' => $website,
					'address' => $address,
					'users_count' => $users_count,
					'dealer_count' => $dealer_count,
					'Group_count' => $Group_count,
					'numberofStarterPlusLicence' => $numberofStarterPlusLicence,
					'availableStarterPlusLicence' => $availableStarterPlusLicence,
					'numberofStarterLicence' => $numberofStarterLicence,
					'availableStarterLicence' => $availableStarterLicence,
					'numberofBasicLicence' => $numberofBasicLicence,
					'availableBasicLicence' => $availableBasicLicence,
					'numberofAdvanceLicence' => $numberofAdvanceLicence,
					'availableAdvanceLicence' => $availableAdvanceLicence,
					'numberofPremiumLicence' => $numberofPremiumLicence,
					'availablePremiumLicence' => $availablePremiumLicence,
					'numberofPremPlusLicence' => $numberofPremPlusLicence,
					'availablePremPlusLicence' => $availablePremPlusLicence,
					'sp_onboard_count' => $sp_onboard_count,
					'st_onboard_count' => $st_onboard_count,
					'bc_onboard_count' => $bc_onboard_count,
					'ad_onboard_count' => $ad_onboard_count,
					'pr_onboard_count' => $pr_onboard_count,
					'sp_renew_count' => $sp_renew_count,
					'st_renew_count' => $st_renew_count,
					'bc_renew_count' => $bc_renew_count,
					'ad_renew_count' => $ad_renew_count,
					'pr_renew_count' => $pr_renew_count,
					'dealer' => $dealer,
					'dealerNoSt' => $dealerNoSt,
					'dealerNoBc' => $dealerNoBc,
					'dealerNoAd' => $dealerNoAd,
					'dealerNoPr' => $dealerNoPr,
					'sp_dealer_tol' => $sp_dealer_tol,
					'st_dealer_tol' => $st_dealer_tol,
					'bc_dealer_tol' => $bc_dealer_tol,
					'ad_dealer_tol' => $ad_dealer_tol,
					'pr_dealer_tol' => $pr_dealer_tol,
					'ttlused' => $ttlused,
					'prpl_dealer_tol' => $prpl_dealer_tol,
					'dealerNoPrPl' => $dealerNoPrPl,
					'prpl_onboard_count' => $prpl_onboard_count,
					'prpl_renew_count' => $prpl_renew_count,
				];

				return view('vdm.dashboard.dashcard', $dashcardData);
			}
			//Vehicles count is taken from redis storage for temporarily
			$vehicles = $redis->smembers('S_Vehicles_Dealer_' . $username . '_' . $fcode);
			foreach ($vehicles as $key => $vehicle) {
				$device = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicle);
				if ($redis->sismember('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $device) == 0) {
					$vehiclecount = Arr::add($vehiclecount, $vehicle, $vehicle);
				}
			}
			$count = count($vehiclecount);

			$month = date("m");
			$year = date("Y");

			$prsentMonthCount = DB::table('Vehicle_details')->where('fcode', $fcode)->where('belongs_to', $username)->where('month', $month)->count();
			$prevMonthCount = DB::table('Vehicle_details')->where('fcode', $fcode)->where('belongs_to', $username)->where('month', $month - 1)->count();

			$redisUserCacheId = 'S_Users_Dealer_' . $username . '_' . $fcode;
			$userList = $redis->smembers($redisUserCacheId);
			foreach ($userList as $key => $value) {
				$userGroups = $redis->smembers($value);
				$vcount = 0;
				foreach ($userGroups as $key => $value1) {
					$groupvehi = $redis->scard($value1);
					$vcount = $vcount + $groupvehi;
				}
				$userGroupsArr = Arr::add($userGroupsArr, $value, $vcount);
			}
		} else if (session()->get('cur') == 'admin') {
			//Vehicles count is taken from redis storage for temporarily
			$count = $redis->scard('S_Vehicles_Admin_' . $fcode);
			$dealer = $redis->smembers('S_Dealers_' . $fcode);
			if (session()->get('cur1') == 'prePaidAdmin') {
				$vehicleCnt = $redis->scard('S_Vehicles_Admin_' . $fcode);
				$details = $redis->hget('H_Franchise', $fcode);
				$franchiseDetails = json_decode($details, true);
				$fname = isset($franchiseDetails['fname']) ? $franchiseDetails['fname'] : '';
				$address = isset($franchiseDetails['fullAddress']) ? $franchiseDetails['fullAddress'] : '';
				$website = isset($franchiseDetails['website']) ? $franchiseDetails['website'] : '';

				$numberofStarterPlusLicence = isset($franchiseDetails['numberofStarterPlusLicence']) ? $franchiseDetails['numberofStarterPlusLicence'] : '0';
				$numberofStarterLicence = isset($franchiseDetails['numberofStarterLicence']) ? $franchiseDetails['numberofStarterLicence'] : '0';
				$numberofBasicLicence = isset($franchiseDetails['numberofBasicLicence']) ? $franchiseDetails['numberofBasicLicence'] : '0';
				$numberofAdvanceLicence = isset($franchiseDetails['numberofAdvanceLicence']) ? $franchiseDetails['numberofAdvanceLicence'] : '0';
				$numberofPremiumLicence = isset($franchiseDetails['numberofPremiumLicence']) ? $franchiseDetails['numberofPremiumLicence'] : '0';
				$numberofPremPlusLicence = isset($franchiseDetails['numberofPremPlusLicence']) ? $franchiseDetails['numberofPremPlusLicence'] : '0';


				$availableStarterPlusLicence = isset($franchiseDetails['availableStarterPlusLicence']) ? $franchiseDetails['availableStarterPlusLicence'] : '0';
				$availableStarterLicence = isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : '0';
				$availableBasicLicence = isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';
				$availableAdvanceLicence = isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';
				$availablePremiumLicence = isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';
				$availablePremPlusLicence = isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0';

				$ttlused = ($numberofStarterPlusLicence + $numberofStarterLicence + $numberofBasicLicence + $numberofAdvanceLicence + $numberofPremiumLicence + $numberofPremPlusLicence) - ($availableStarterLicence + $availableBasicLicence + $availableAdvanceLicence + $availablePremiumLicence + $availablePremPlusLicence);


				$users_count = $redis->scard('S_Users_Admin_' . $fcode);
				$dealer_count = $redis->scard('S_Dealers_' . $fcode);
				$Group_count = $redis->scard('S_Groups_Admin_' . $fcode);

				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'StarterPlus' AND User_Name ='$username' ";
				$sp_onboard_count = DashBoardController::getSqlcount($max);
				// AuditTables::ChangeDB("VAM");
				// $max1 = RenewalDetails::where(["Status"=>"OnBoard","Type"=>"StarterPlus","User_Name"=>$username])->get();
				// return [ 
				// 	$sp_onboard_count,
				// 	$max1
				// ];
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Starter' AND User_Name ='$username' ";
				$st_onboard_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Basic' AND User_Name ='$username' ";
				$bc_onboard_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Advance' AND User_Name ='$username'";
				$ad_onboard_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Premium' AND User_Name ='$username'";
				$pr_onboard_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'PremiumPlus' AND User_Name ='$username'";
				$prpl_onboard_count = DashBoardController::getSqlcount($max);

				//renew
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'StarterPlus' AND User_Name ='$username'";
				$sp_renew_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Starter' AND User_Name ='$username'";
				$st_renew_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Basic' AND User_Name ='$username'";
				$bc_renew_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Advance' AND User_Name ='$username'";
				$ad_renew_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Premium' AND User_Name ='$username'";
				$pr_renew_count = DashBoardController::getSqlcount($max);
				$max = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'PremiumPlus' AND User_Name ='$username'";
				$prpl_renew_count = DashBoardController::getSqlcount($max);

				//Dealers Deatails
				$dealerNoSp = null;
				$dealerNoSt = null;
				$dealerNoBc = null;
				$dealerNoAd = null;
				$dealerNoPr = null;
				$dealerNoPrPl = null;

				$dealerAvlSp = null;
				$dealerAvlSt = null;
				$dealerAvlBc = null;
				$dealerAvlAd = null;
				$dealerAvlPr = null;
				$dealerAvlPrPl = null;
				$starterPlusCount = array();
				$starterCount = array();
				$basicCount = array();
				$advanceCount = array();
				$premiumCount = array();
				$premiumPlusCount = array();

				$sp_dealer_tol = 0;
				$st_dealer_tol = 0;
				$bc_dealer_tol = 0;
				$ad_dealer_tol = 0;
				$pr_dealer_tol = 0;
				$prpl_dealer_tol = 0;
				if (request()->server('HTTP_HOST') === "localhost") {
					$dealer  = array_slice($dealer, 0, 10);
				}
				foreach ($dealer as $key => $orgId) {
					$starterPlusVehicle = 0;
					$starterVehicle = 0;
					$basicVehicle = 0;
					$advanceVehicle = 0;
					$premiumVehicle = 0;
					$premiumPlusVehicle = 0;
					$vehicleListId = 'S_Vehicles_Dealer_' . $orgId . '_' . $fcode;
					$vehicleList = $redis->smembers($vehicleListId);
					if (request()->server('HTTP_HOST') === "localhost") {
						$vehicleList  = array_slice($vehicleList, 0, 10);
					}
					foreach ($vehicleList as $key => $vehicle) {
						$vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);
						$vehicleRefData = json_decode($vehicleRefData, true);
						$LicenceType = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
						if ($LicenceType == 'StarterPlus') {
							$starterPlusVehicle = $starterPlusVehicle + 1;
						} else if ($LicenceType == 'Starter') {
							$starterVehicle = $starterVehicle + 1;
						} else if ($LicenceType == 'Basic') {
							$basicVehicle = $basicVehicle + 1;
						} else if ($LicenceType == 'Advance') {
							$advanceVehicle = $advanceVehicle + 1;
						} else if ($LicenceType == 'Premium') {
							$premiumVehicle = $premiumVehicle + 1;
						} else if ($LicenceType == 'PremiumPlus') {
							$premiumPlusVehicle = $premiumPlusVehicle + 1;
						}
					}
					$starterPlusCount = Arr::add($starterPlusCount, $orgId, $starterPlusVehicle);
					$starterCount = Arr::add($starterCount, $orgId, $starterVehicle);
					$basicCount = Arr::add($basicCount, $orgId, $basicVehicle);
					$advanceCount = Arr::add($advanceCount, $orgId, $advanceVehicle);
					$premiumCount = Arr::add($premiumCount, $orgId, $premiumVehicle);
					$premiumPlusCount = Arr::add($premiumPlusCount, $orgId, $premiumPlusVehicle);

					$detailJson = $redis->hget('H_DealerDetails_' . $fcode, $orgId);
					$detail = json_decode($detailJson, true);

					$numofStarterPlus = isset($detail['numofStarterPlus']) ? $detail['numofStarterPlus'] : '0';
					$sp_dealer_tol += $numofStarterPlus;
					$dealerNoSp = Arr::add($dealerNoSp, $orgId, $numofStarterPlus);

					$numofStarter = isset($detail['numofStarter']) ? $detail['numofStarter'] : '0';
					$st_dealer_tol += $numofStarter;
					$dealerNoSt = Arr::add($dealerNoSt, $orgId, $numofStarter);

					$numofBasic = isset($detail['numofBasic']) ? $detail['numofBasic'] : '0';
					$bc_dealer_tol += $numofBasic;
					$dealerNoBc = Arr::add($dealerNoBc, $orgId, $numofBasic);

					$numofAdvance = isset($detail['numofAdvance']) ? $detail['numofAdvance'] : '0';
					$ad_dealer_tol  += $numofAdvance;
					$dealerNoAd = Arr::add($dealerNoAd, $orgId, $numofAdvance);

					$numofPremium = isset($detail['numofPremium']) ? $detail['numofPremium'] : '0';
					$pr_dealer_tol += $numofPremium;
					$dealerNoPr = Arr::add($dealerNoPr, $orgId, $numofPremium);

					$numofPremiumPlus = isset($detail['numofPremiumPlus']) ? $detail['numofPremiumPlus'] : '0';
					$prpl_dealer_tol +=  $numofPremiumPlus;
					$dealerNoPrPl = Arr::add($dealerNoPrPl, $orgId, $numofPremiumPlus);

					$avlofStarterPlus = isset($detail['avlofStarterPlus']) ? $detail['avlofStarterPlus'] : '0';
					$dealerAvlSp = Arr::add($dealerAvlSp, $orgId, $avlofStarterPlus);

					$avlofStarter = isset($detail['avlofStarter']) ? $detail['avlofStarter'] : '0';
					$dealerAvlSt = Arr::add($dealerAvlSt, $orgId, $avlofStarter);

					$avlofBasic = isset($detail['avlofBasic']) ? $detail['avlofBasic'] : '0';
					$dealerAvlBc = Arr::add($dealerAvlBc, $orgId, $avlofBasic);

					$avlofAdvance = isset($detail['avlofAdvance']) ? $detail['avlofAdvance'] : '0';
					$dealerAvlAd = Arr::add($dealerAvlAd, $orgId, $avlofAdvance);

					$avlofPremium = isset($detail['avlofPremium']) ? $detail['avlofPremium'] : '0';
					$dealerAvlPr = Arr::add($dealerAvlPr, $orgId, $avlofPremium);

					$avlofPremiumPlus = isset($detail['avlofPremiumPlus']) ? $detail['avlofPremiumPlus'] : '0';
					$dealerAvlPrPl = Arr::add($dealerAvlPrPl, $orgId, $avlofPremiumPlus);
				}

				$dashcardData = [
					'vehicleCnt' => $vehicleCnt,
					'fname' => $fname,
					'website' => $website,
					'address' => $address,
					'users_count' => $users_count,
					'dealer_count' => $dealer_count,
					'Group_count' => $Group_count,
					'numberofStarterPlusLicence' => $numberofStarterPlusLicence,
					'availableStarterPlusLicence' => $availableStarterPlusLicence,
					'numberofStarterLicence' => $numberofStarterLicence,
					'availableStarterLicence' => $availableStarterLicence,
					'numberofBasicLicence' => $numberofBasicLicence,
					'availableBasicLicence' => $availableBasicLicence,
					'numberofAdvanceLicence' => $numberofAdvanceLicence,
					'availableAdvanceLicence' => $availableAdvanceLicence,
					'numberofPremiumLicence' => $numberofPremiumLicence,
					'availablePremiumLicence' => $availablePremiumLicence,
					'numberofPremPlusLicence' => $numberofPremPlusLicence,
					'availablePremPlusLicence' => $availablePremPlusLicence,
					'sp_onboard_count' => $sp_onboard_count,
					'st_onboard_count' => $st_onboard_count,
					'bc_onboard_count' => $bc_onboard_count,
					'ad_onboard_count' => $ad_onboard_count,
					'pr_onboard_count' => $pr_onboard_count,
					'sp_renew_count' => $sp_renew_count,
					'st_renew_count' => $st_renew_count,
					'bc_renew_count' => $bc_renew_count,
					'ad_renew_count' => $ad_renew_count,
					'pr_renew_count' => $pr_renew_count,
					'dealer' => $dealer,
					'dealerNoSp' => $dealerNoSp,
					'dealerNoSt' => $dealerNoSt,
					'dealerNoBc' => $dealerNoBc,
					'dealerNoAd' => $dealerNoAd,
					'dealerNoPr' => $dealerNoPr,
					'sp_dealer_tol' => $sp_dealer_tol,
					'st_dealer_tol' => $st_dealer_tol,
					'bc_dealer_tol' => $bc_dealer_tol,
					'ad_dealer_tol' => $ad_dealer_tol,
					'pr_dealer_tol' => $pr_dealer_tol,
					'ttlused' => $ttlused,
					'prpl_dealer_tol' => $prpl_dealer_tol,
					'dealerNoPrPl' => $dealerNoPrPl,
					'prpl_onboard_count' => $prpl_onboard_count,
					'prpl_renew_count' => $prpl_renew_count,
					'dealerAvlSp' => $dealerAvlSp,
					'dealerAvlSt' => $dealerAvlSt,
					'dealerAvlBc' => $dealerAvlBc,
					'dealerAvlPr' => $dealerAvlPr,
					'dealerAvlPrPl' => $dealerAvlPrPl,
					'dealerAvlAd' => $dealerAvlAd,
					'starterPlusCount' => $starterPlusCount,
					'starterCount' => $starterCount,
					'basicCount' => $basicCount,
					'advanceCount' => $advanceCount,
					'premiumCount' => $premiumCount,
					'premiumPlusCount' => $premiumPlusCount,
				];
				return view('vdm.dashboard.dashcard', $dashcardData);
			} else {
				foreach ($dealer as $org1) {
					$count1 = $redis->scard('S_Vehicles_Dealer_' . $org1 . '_' . $fcode);
					$dealerId = Arr::add($dealerId, $org1, $count1);
				}
				$month = date("m");
				$year = date("Y");
				$prevMonthCount = DB::table('Vehicle_details')->where('fcode', $fcode)->where('belongs_to', $username)->where('month', $month - 1)->count();
				$prsentMonthCount = DB::table('Vehicle_details')->where('fcode', $fcode)->where('belongs_to', $username)->where('month', $month)->count();
			}
		}

		$nextMonthCount = [];

		$vechile = array();
		$vechileEx = 0;
		$vechileEx1 = 0;
		// viwe data
		$viewData['title'] = __("title.private.dashboard");
		$viewData['count'] = $count;
		$viewData['username'] = $username;
		$viewData['dealerId'] = $dealerId;
		$viewData['vechile'] = $vechile;
		$viewData['temp'] = $temp;
		$viewData['vechileEx'] = $vechileEx;
		$viewData['vechileEx1'] = $vechileEx1;
		$viewData['prsentMonthCount'] = $prsentMonthCount;
		$viewData['nextMonthCount'] = $nextMonthCount;
		$viewData['prevMonthCount'] = $prevMonthCount;
		$viewData['userGroupsArr'] = $userGroupsArr;
		return view('vdm.vehicles.dashboard', $viewData);
	}
	public function indexToDelete()
	{

		$username =  auth()->id();

		session()->forget('page');

		$redis = Redis::connection();

		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$dealerId = null;
		$count = 0;
		$new_date = date('FY', strtotime("+1 month"));
		$expireData = $redis->hget('H_Expire_' . $fcode, $new_date);

		$vechile = array();
		$temp = array();
		if ($expireData !== null) {

			$vehiclesExpire = explode(',', $expireData);
			if (session()->get('cur') == 'dealer') {
				foreach ($vehiclesExpire as $org) {
					$vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
					$value = $redis->SISMEMBER($vehicleListId, $org);
					if ($value == 1) {
						$vechile = Arr::add($vechile, $org, $org);
					}
				}
			} else if (session()->get('cur') == 'admin') {
				$dealer = $redis->smembers('S_Dealers_' . $fcode);
				foreach ($dealer as $org1) {
					$temp3 = 0;
					$vechile1 = array();
					foreach ($vehiclesExpire as $org) {
						$vehicleListId = 'S_Vehicles_' . $fcode;
						$value = $redis->SISMEMBER($vehicleListId, $org);
						if ($value == 1) {
							$vechile = Arr::add($vechile, $org, $org);
						} else {
							$vehicleListId = 'S_Vehicles_Dealer_' . $org1 . '_' . $fcode;
							$value1 = $redis->SISMEMBER($vehicleListId, $org);
							if ($value1 == 1) {
								$vechile1 = Arr::add($vechile1, $org, $org);
								$temp2 = count($vechile1);
								if ($temp2 != 0) {
									try {
										$temp3 = $temp[$org1] + $temp2;
									} catch (\Exception $e) {
										$temp3 = $temp2;
									}
									unset($temp[$org1]);
									$temp = Arr::add($temp, $org1, strval($temp3));
								}
							}
							$value1 = 0;
							//
						}
					}
				}
			}
		}
		$new_date1 = date('FY', strtotime("+12 month"));
		$presentMonth = $redis->hget('H_Expire_' . $fcode, $new_date1);

		$prsentMonthCount = DashBoardController::getCount($presentMonth, $fcode, $username);
		$new_date2 = date('FY', strtotime("+13 month"));
		$nextMonth = $redis->hget('H_Expire_' . $fcode, $new_date2);
		$nextMonthCount = DashBoardController::getCount($nextMonth, $fcode, $username);


		$new_date3 = date('FY', strtotime("+11 month"));
		$prevMonth = $redis->hget('H_Expire_' . $fcode, $new_date3);
		$prevMonthCount = DashBoardController::getCount($prevMonth, $fcode, $username);

		if (session()->get('cur') == 'dealer') {
			$vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
			$count = $redis->scard($vehicleListId);
			$vechileEx = ' ';
			$vechileEx1 = ' ';
		} else if (session()->get('cur') == 'admin') {
			$vechileEx = 'Number of Vehicles Expired this month for dealers :';
			$vechileEx1 = ' ';
			$vehicleListId = 'S_Vehicles_Admin_' . $fcode;
			$count = $redis->scard($vehicleListId);
			$dealerId = $redis->smembers('S_Dealers_' . $fcode);
			$orgArr = array();
			foreach ($dealerId as $org) {
				$vehicleListId = 'S_Vehicles_Dealer_' . $org . '_' . $fcode;
				$count = $count + $redis->scard($vehicleListId);
				$orgArr = Arr::add($orgArr, $org, $redis->scard($vehicleListId));
			}
			$dealerId = $orgArr;
		} else {
			$vehicleListId = 'S_Vehicles_' . $fcode;
		}
		$dashboardData = [
			'count' => $count,
			'dealerId' => $dealerId,
			'vechile' => $vechile,
			'temp' => $temp,
			'vechileEx' => $vechileEx,
			'vechileEx1' => $vechileEx1,
			'prsentMonthCount' => $prsentMonthCount,
			'nextMonthCount' => $nextMonthCount,
			'prevMonthCount' => $prevMonthCount,
		];

		return view('vdm.vehicles.dashboard', $dashboardData);
	}

	public function getprevmonth($curmonth, $prevmonth)
	{
		$monthlist 	= [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
		return ($curmonth < $prevmonth) ? $monthlist[(count($monthlist) - ($prevmonth - $curmonth)) - 1] : $monthlist[(count($monthlist) - ($curmonth - $prevmonth)) - 1];
	}

	public function getnextmonth($curmonth, $nextmonth)
	{
		$monthlist 	= [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
		return ((($curmonth + $nextmonth) < 12) ? $monthlist[($curmonth + $nextmonth) - 1] : $monthlist[(($curmonth + $nextmonth) - count($monthlist)) - 1]);
	}


	public static function getSqlcount($max)
	{

		$username =  auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$servername = $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
		$servername = '209.97.163.4';
		if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
			return 'Ipaddress Failed !!!';
		}
		$usernamedb  =  "root";
		$password    =  "#vamo123";
		$dbname      =  $fcode;
		try {
			$conn  =  mysqli_connect($servername, $usernamedb, $password, $dbname);
			if (!$conn) {
				die('Could not connect:' . mysqli_connect_error());
				return 'Please Update One more time Connection failed';
			} else {
				try {
					$renewal = "CREATE TABLE IF NOT EXISTS Renewal_Details(ID INT AUTO_INCREMENT PRIMARY KEY,User_Name varchar(50),Licence_Id varchar(50),Vehicle_Id varchar(50),Device_Id varchar(50),Type ENUM('Basic', 'Advance', 'Premium') NOT NULL,Processed_Date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,Status ENUM('Migration', 'Rename', 'OnBoard','Cancel','Renew') NOT NULL)";
					$result = $conn->query($renewal);
					$results = mysqli_query($conn, $max);
					if ($results) {
						while ($row = mysqli_fetch_array($results)) {
							$maxcount = $row[0];
						}
					} else {
						$maxcount = 0;
					}
				} catch (Exception $e) {
					$renewal = "CREATE TABLE IF NOT EXISTS Renewal_Details(ID INT AUTO_INCREMENT PRIMARY KEY,User_Name varchar(50),Licence_Id varchar(50),Vehicle_Id varchar(50),Device_Id varchar(50),Type ENUM('Basic', 'Advance', 'Premium') NOT NULL,Processed_Date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,Status ENUM('Migration', 'Rename', 'OnBoard','Cancel','Renew') NOT NULL)";
					$result = $conn->query($renewal);
				}

				return $maxcount;
			}
		} catch (Exception $e) {
			return 0;
		}
	}

	public function getDateT($second, $min, $hour, $day, $month, $year)
	{
		$xmasThisYear = Carbon::createFromDate($year, $month, $day);
		$xmasThisYear->hour = $hour;
		$xmasThisYear->minute = $min;
		$xmasThisYear->second = $second;

		return $xmasThisYear;
	}


	public static function getCount($presentData, $fcode, $username)
	{
		$redis = Redis::connection();
		$vechilePre = array();
		$tempPre = array();
		if ($presentData !== null) {

			$vehiclesExpire = explode(',', $presentData);
			if (session()->get('cur') == 'dealer') {
				foreach ($vehiclesExpire as $orgPre) {
					$vehicleListIdPre = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
					$valuePre = $redis->SISMEMBER($vehicleListIdPre, $orgPre);
					if ($valuePre == 1) {
						$vechilePre = Arr::add($vechilePre, $orgPre, $orgPre);
					}
				}
			} else if (session()->get('cur') == 'admin') {

				foreach ($vehiclesExpire as $orgPre) {
					$vehicleListIdPre = 'S_Vehicles_' . $fcode;
					$valuePre = $redis->SISMEMBER($vehicleListIdPre, $orgPre);

					if ($valuePre == 1) {
						$vechilePre = Arr::add($vechilePre, $orgPre, $orgPre);
					} else {
						logger('else if--->' . $orgPre);
					}
				}
			}
		}
		return count($vechilePre);
	}

	protected function schedule(Schedule $schedule)
	{
		$schedule->call(function () {
			//DB::table('recent_users')->delete();

		})->everyMinute();
	}
}
