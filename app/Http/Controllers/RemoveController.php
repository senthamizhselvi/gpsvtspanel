<?php

namespace App\Http\Controllers;

use App\Models\AuditVehicle;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;

class RemoveController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		if (session()->get('cur') == 'admin') {
			$franDetails_json = $redis->hget('H_Franchise', $fcode);
			$franchiseDetails = json_decode($franDetails_json, true);
			$prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
			if ($prepaid == 'yes') {
				$availableBasicLicence = isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';
				$availableAdvanceLicence = isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';
				$availablePremiumLicence = isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';
				$availableLincence = 0;
			} else {
				$availableLincence = isset($franchiseDetails['availableLincence']) ? $franchiseDetails['availableLincence'] : '0';
				$availableBasicLicence = 0;
				$availableAdvanceLicence = 0;
				$availablePremiumLicence = 0;
			}

			$numberofdevice = 0;
			$parentRemoveDeviceData = [
				'availableBasicLicence' => $availableBasicLicence,
				'availableAdvanceLicence' => $availableAdvanceLicence,
				'availablePremiumLicence' => $availablePremiumLicence,
				'availableLincence' => $availableLincence,
				'numberofdevice' => $numberofdevice,
				'prepaid' => $prepaid
			];
			if(session()->get('cur1') =='prePaidAdmin'){
				return  view('vdm.business.prepaidRemoveDevice',$parentRemoveDeviceData);
			}else{
				return view('vdm.business.removeDevice', $parentRemoveDeviceData);
			}
		}
	}

	public function create()
	{
		DatabaseConfig::checkDb();
		return self::index();
	}

	public function checkDevice()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$deviceid = request()->get('id');

		$dev = $redis->hget('H_Device_Cpy_Map', $deviceid);
		$vid = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $deviceid);
		$details = $redis->Hget('H_RefData_' . $fcode, $vid);

		$details = json_decode($details, true);

		if (is_null($details)) {
			return ['error' => 'Device not exist'];
		}
		$owner = $details['OWN'];


		$error = ' ';
		if ($dev == null || $owner != 'OWN') {
			$error = 'Device Id not already present in admin ' . $deviceid;
		}
		$refDataArr = array(
			'error' => $error
		);
		return response()->json($refDataArr);
	}

	public function checkvehicle()
	{
		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$vehicleId = request()->get('id');
		$vehicleU = strtoupper($vehicleId);

		$vehicleIdCheck = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
		$vehicleIdCheck2 = $redis->sismember('S_KeyVehicles', $vehicleU);

		$details = $redis->Hget('H_RefData_' . $fcode, $vehicleId);

		$details = json_decode($details, true);
		if (is_null($details)) {
			return ['error' => 'Vehicle not exist'];
		}
		$owner = $details['OWN'];

		$error = ' ';
		if (($vehicleIdCheck != 1 && $vehicleIdCheck2 != 1) || ($owner != 'OWN')) {
			$error = 'Vehicle Id not already present in admin ' . $vehicleId;
		}

		$refDataArr = array(
			'error' => $error
		);
		return response()->json($refDataArr);
	}

	public function store()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		$prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
		if ($prepaid == 'yes') {

			$onBasicdevice = request()->get('onBasicdevice');
			$onAdvancedevice = request()->get('onAdvancedevice');
			$onPremiumdevice = request()->get('onPremiumdevice');
			$rules = array(
				'LicenceType' => 'required'
			);
			$validator = validator()->make(request()->all(), $rules);

			if ($validator->fails()) {
				return redirect()->back()->withInput()->withErrors($validator);
			} else {
				$LicenceType = request()->get('LicenceType');
				if ($LicenceType == 'Basic') {
					$numberofdevice = $onBasicdevice;
				} else if ($LicenceType == 'Advance') {
					$numberofdevice = $onAdvancedevice;
				} else {
					$numberofdevice = $onPremiumdevice;
				}
				$availableLincence = 0;
			}
		} else {
			$rules = array(
				'numberofdevice' => 'required|numeric|min:1'
			);
			$validator = validator()->make(request()->all(), $rules);
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			} else {
				$numberofdevice = request()->get('numberofdevice');
				$LicenceType = 0;
			}
		}
		
		$parentRemoveDeviceData = [
			'LicenceType' => $LicenceType,
			'numberofdevice' => $numberofdevice,
		];
		return view('vdm.business.parentRemove', $parentRemoveDeviceData);
	}
	 
	public function removedevice()
	{

		$username = auth()->id();

		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		$prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';
		$notRemovedList = null;
		$removedList = null;
		$deviceRemoveCount = 0;
		$notbasic = null;
		$notadvance = null;
		$notpremium = null;

		$nod = request()->get('numberofdevice');
		$type = request()->get('type');
		if ($prepaid == 'yes') {
			$Ltype = request()->get('LicenceType');
		}
		$deviceIdsArr = request()->get('deviceIds');
		// $deviceIdArr = request()->get('deviceid');

		$devicearray = array();
		for ($i = 0; $i < $nod; $i++) {
			if ($type == 'Device') {
				$did = $deviceIdsArr[$i];
				$vid = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $did);
				$existId = $redis->sismember('S_Vehicles_Admin_' . $fcode, $vid);
			}else{
				$vidid = $deviceIdsArr[$i];
				$vid = strtoupper($vidid);
				$did = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vid);
				$existId = $redis->sismember('S_Device_' . $fcode, $did);
			}
			if(!$existId || $vid == null || $did == null) {
                $notRemovedList = Arr::add($notRemovedList, $deviceIdsArr[$i], $deviceIdsArr[$i]);
				continue;
            }
			$details = $redis->Hget('H_RefData_' . $fcode, $vid);
			$devicearray = Arr::add($devicearray, $vid, $did);
			$details = json_decode($details, true);
			$shortName = $details['shortName'];
			$LicenceType = isset($details['Licence']) ? $details['Licence'] : '';
			$owner = $details['OWN'];
			if ($prepaid == 'yes') {
				$Ltype = request()->get('LicenceType');
				if ($Ltype != $LicenceType) {
					if ($Ltype == 'Basic') {
						$notbasic = Arr::add($notbasic, $did, $did);
					} else if ($Ltype == 'Advance') {
						$notadvance = Arr::add($notadvance, $did, $did);
					} else if ($Ltype == 'Premium') {
						$notpremium = Arr::add($notpremium, $did, $did);
					}
					continue;
				}
			}

			$shortName1 = strtoupper($shortName);
			$orgId = $details['orgId'];
			$orgId1 = strtoupper($orgId);

			$mon = isset($details['mobileNo']) ? $details['mobileNo'] : '';
			$gpsSimNo = isset($details['gpsSimNo']) ? $details['gpsSimNo'] : '';

			if ($owner == 'OWN') {
				$deviceRemoveCount = $deviceRemoveCount + 1;
				$redis->hdel('H_VehicleName_Mobile_Org_' . $fcode, $vid . ':' . $did . ':' . $shortName1 . ':' . $orgId1 . ':' . $gpsSimNo);
				$redis->hdel('H_VehicleName_Mobile_Admin_OWN_Org_' . $fcode, $vid . ':' . $did . ':' . $shortName1 . ':' . $orgId1 . ':' . $gpsSimNo . ':OWN');
				$redis->srem('S_Organisations_Admin_' . $fcode, $shortName);
				///ram noti
				$redis->del('S_' . $vid . '_' . $fcode);
				///
				$redis->hdel('H_ProData_' . $fcode, $vid);
				$redis->hdel('H_Device_Cpy_Map', $did);
				$redis->srem('S_Vehicles_Admin_' . $fcode, $vid);
				$redis->srem('S_Vehicles_' . $fcode, $vid);

				$vdetails = $redis->Hget('H_RefData_' . $fcode, $vid);

				$redis->hdel('H_RefData_' . $fcode, $vid);
				try{
					$mysqlDetails = [
						'userIpAddress' => session()->get('userIP'),
						'userName' => $username,
						'type' => config()->get('constant.vehicle'),
						'name' => $vid,
						'status' => config()->get('constant.deleted'),
						'oldData' => $vdetails,
						'newData' => ""
					];
					AuditNewController::updateAudit($mysqlDetails);
				}catch(Exception $e){
					AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
					logger('Error inside AuditVehicle Delete' . $e->getMessage());
				}
				$LicenceId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vid);
				$redis->hdel('H_Vehicle_LicenceId_Map_' . $fcode, $vid);
				$redis->hdel('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId);
				$redis->hdel('H_LicenceExpiry_' . $fcode, $LicenceId);
				$redis->srem('S_LicenceList_' . $fcode, $LicenceId);
				$redis->srem('S_ExpiredLicence_Vehicle' . $fcode, $LicenceId);
				$redis->srem('S_Device_' . $fcode, $did);
				$redis->srem('S_Vehicles_' . $fcode, $vid);
				$redis->hdel('H_Vehicle_Device_Map_' . $fcode, $vid);
				DB::table('Vehicle_details')
					->where('vehicle_id', $vid)
					->where('fcode', $fcode)
					->delete();
				$redis->hdel('H_Vehicle_Device_Map_' . $fcode, $did);

				$groupList = $redis->smembers('S_Groups_' . $fcode);
				foreach ($groupList as $key => $group) {
					$vehicleList = $redis->smembers($group);
					$avid = strtoupper($vid);
					if(in_array($avid,$vehicleList)){
						$redis->srem($group, $avid);
						$redis->smembers($group);
						$coun = $redis->scard($group);
						if ($coun == 0) {
							$redisUserCacheId = 'S_Users_' . $fcode;
							$userList = $redis->smembers($redisUserCacheId);
							foreach ($userList as $key => $value1) {
								$userGroups = $redis->smembers($value1);
								$group1 = $group;
								if(in_array($group1,$userGroups)){
									$redis->srem($value1, $group1);
								}
								// $count1 = $redis->scard($value1);
							}
							$redis->srem('S_Groups_Admin_' . $fcode, $group);
							$redis->srem('S_Groups_' . $fcode, $group);
						}
					}
					
				}
				$removedList = Arr::add($removedList, $did, $did);
			} else {
				$notRemovedList = Arr::add($notRemovedList, $did, $did);
			}
		}
		
		$franDetails_json = $redis->hget('H_Franchise', $fcode);
		$franchiseDetails = json_decode($franDetails_json, true);
		if ($deviceRemoveCount > 0) {
			$franchiseDetails['availableLincence'] = $franchiseDetails['availableLincence'] + $deviceRemoveCount;
		}

		$detailsJson = json_encode($franchiseDetails);
		$redis->hmset('H_Franchise', $fcode, $detailsJson);
		if ($notRemovedList == null && $removedList != null) {
			$removedList = implode(" ", $removedList);
			return redirect()->back()->with([
				'alert-class'=>'alert-success',
				'message'=>$removedList . ' Device removed successfully !'
			]);
		} else if ($notRemovedList != null && $removedList == null) {
			$notRemovedList = implode(" ", $notRemovedList);
			return redirect()->back()->with([
				'alert-class'=> 'alert-danger',
				'message'=> $notRemovedList.' Device Id not already present in admin'
			]);
		} else if ($notRemovedList != null && $removedList != null) {
			$notRemovedList = implode(" ", $notRemovedList);
			$removedList = implode(" ", $removedList);
			
			return redirect()->back()->with([
				'alert-class'=> 'alert-danger',
				'message'=>$removedList . ' Device Remove successfully and '.$notRemovedList.' Device Id not already present in admin'
			]);
		} else {
			return redirect()->back()->with([
				'alert-class'=> 'alert-danger',
				'message'=>'Device not removed'
			]);
		}
	}
}
