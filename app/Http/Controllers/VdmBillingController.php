<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use App\Models\AuditFrans;
use App\Models\AuditDealer;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class VdmBillingController extends Controller
{

  public function index()
  {
  
    $username = auth()->id();
    session()->forget('page');
    session()->put('vCol', 1);
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $LicenceList1 = 'S_LicenceList_' . $fcode;
    if (session()->get('cur') == 'dealer') {
      $LicenceList1 = 'S_LicenceList_dealer_' . $username . '_' . $fcode;
    } else if (session()->get('cur') == 'admin') {
      $LicenceList1 = 'S_LicenceList_admin_' . $fcode;
    } else {
      $LicenceList1 = 'S_LicenceList_' . $fcode;
    }
    $vehicleList = null;
    $emptyRef = null;
    $LtypeList = null;
    $deviceList = null;
    $deviceModelList = null;
    $licenceissuedList = null;
    $LicenceExpiryList = null;
    $LicenceOnboardList = null;
    $ExpiredLicenceList = null;
    $dealerList = null;
    $VehicleNameList=null;
    $gpsSimNoList=null;
    $orgIdList=null;
    $LicenceList = $redis->smembers($LicenceList1);
    if (request()->server('HTTP_HOST') === "localhost") {
      $LicenceList = array_slice($LicenceList, 0, 10);
    }
    foreach ($LicenceList as $key => $LicenceId) {
      $vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId);
      $vehicleList = Arr::add($vehicleList, $LicenceId, $vehicleId);
      //vehicle Ref data
      $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
      $vehicleRefData = json_decode($vehicleRefData, true);
      if (isset($vehicleRefData)) {
        $emptyRef = Arr::add($emptyRef, $LicenceId, $vehicleId);
      } else {
        continue;
      }
      $type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
      $LtypeList = Arr::add($LtypeList, $LicenceId, $type);
      $deviceId = isset($vehicleRefData['deviceId']) ? $vehicleRefData['deviceId'] : '-';
      $deviceList = Arr::add($deviceList, $LicenceId, $deviceId);
      $deviceModel = isset($vehicleRefData['deviceModel']) ? $vehicleRefData['deviceModel'] : 'nill';
      $deviceModelList = Arr::add($deviceModelList, $LicenceId, $deviceModel);
      $dealerId = isset($vehicleRefData['OWN']) ? $vehicleRefData['OWN'] : '-';
      $dealerList = Arr::add($dealerList, $LicenceId, $dealerId);
      $VehicleName = isset($vehicleRefData['shortName']) ? $vehicleRefData['shortName'] : '-';
      $VehicleNameList = Arr::add($VehicleNameList, $LicenceId, $VehicleName);
      $orgId = isset($vehicleRefData['orgId']) ? $vehicleRefData['orgId'] : '-';
      $orgIdList = Arr::add($orgIdList, $LicenceId, $orgId);
      $gpsSimNo = isset($vehicleRefData['gpsSimNo']) ? $vehicleRefData['gpsSimNo'] : '-';
      $gpsSimNoList = Arr::add($gpsSimNoList, $LicenceId, $gpsSimNo);  
      //licence Red date
      $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
      $LicenceRefData = json_decode($LicenceRef, true);
      $LicenceissuedDate = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
      $LicenceExpiryDate = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
      $LicenceOnboardDate = isset($LicenceRefData['LicenceOnboardDate']) ? $LicenceRefData['LicenceOnboardDate'] : '';
      if ($type == "StarterPlus") {
        $LicenceExpiryDate = "-";
      }
      $licenceissuedList = Arr::add($licenceissuedList, $LicenceId, $LicenceissuedDate);
      $LicenceExpiryList = Arr::add($LicenceExpiryList, $LicenceId, $LicenceExpiryDate);
      $LicenceOnboardList = Arr::add($LicenceOnboardList, $LicenceId, $LicenceOnboardDate);

      //licence Expiry Checking
      $expiry_date = $LicenceExpiryDate;
      $today = date('Y-m-d', time());
      $exp = date('Y-m-d', strtotime($expiry_date));
      $expDate =  date_create($exp);
      $todayDate = date_create($today);
      $diff =  date_diff($todayDate, $expDate);
      if ($diff->format("%R%a") >= 0 || $LicenceExpiryDate == "-") {
        $redis->srem('S_ExpiredLicence_Vehicle_'.$fcode,$LicenceId);
      } else {
        $redis->sadd('S_ExpiredLicence_Vehicle_' . $fcode, $LicenceId);
      }
    }
    $ExpiredLicenceList = $redis->smembers('S_ExpiredLicence_Vehicle_' . $fcode);

    sort($LicenceList, SORT_NATURAL | SORT_FLAG_CASE);

    $pay_indexData = [
      'LicenceList' => $LicenceList,
      'vehicleList' => $vehicleList,
      'LtypeList' => $LtypeList,
      'deviceList' => $deviceList,
      'deviceModelList' => $deviceModelList,
      'dealerList' => $dealerList,
      'VehicleNameList' => $VehicleNameList,
      'gpsSimNoList' => $gpsSimNoList,
      'orgIdList' => $orgIdList,
      'licenceissuedList' => $licenceissuedList,
      'LicenceExpiryList' => $LicenceExpiryList,
      'LicenceOnboardList' => $LicenceOnboardList,
      'ExpiredLicenceList' => $ExpiredLicenceList,
    ];
    $licenceDetails = self::getLicenceDetails();
    // $expiredlistData = array_merge($expiredlistData, $licenceDetails);
    return view('vdm.billing.pay_index', $pay_indexData,$licenceDetails);
  }

  public function renewal($id)
  {
   
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $LicenceIdOld = $id;
    $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceIdOld);
    $LicenceRefData = json_decode($LicenceRef, true);
    $LicenceissuedDateOld = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
    $LicenceExpiryDateOld = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
    $LicenceOnboardDateOld = isset($LicenceRefData['LicenceOnboardDate']) ? $LicenceRefData['LicenceOnboardDate'] : '';
    $vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceIdOld);
    $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
    $vehicleRefData = json_decode($vehicleRefData, true);
    $type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : 'Advance';

    if (session()->get('cur') == 'admin') {
      $franDetails_json = $redis->hget('H_Franchise', $fcode);
      $franchiseDetails = json_decode($franDetails_json, true);
      $availableStarterLicence = isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : '0';
      $availableBasicLicence = isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';
      $availableAdvanceLicence = isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';
      $availablePremiumLicence = isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';
      $availablePremPlusLicence = isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0';
    } else if (session()->get('cur') == 'dealer') {
      $dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
      $dealersDetails = json_decode($dealersDetails_json, true);
      $availableStarterLicence = isset($dealersDetails['avlofStarter']) ? $dealersDetails['avlofStarter'] : '0';
      $availableBasicLicence = isset($dealersDetails['avlofBasic']) ? $dealersDetails['avlofBasic'] : '0';
      $availableAdvanceLicence = isset($dealersDetails['avlofAdvance']) ? $dealersDetails['avlofAdvance'] : '0';
      $availablePremiumLicence = isset($dealersDetails['avlofPremium']) ? $dealersDetails['avlofPremium'] : '0';
      $availablePremPlusLicence = isset($dealersDetails['avlofPremiumPlus']) ? $dealersDetails['avlofPremiumPlus'] : '0';
    }
    if ($type == 'Starter' && $availableStarterLicence <= 0) {
      return redirect()->back()->withErrors('Starter Licence Type is not available to renew');
    } else if ($type == 'Basic' && $availableBasicLicence <= 0) {
      return redirect()->back()->withErrors('Basic Licence Type is not available to renew');
    } else if ($type == 'Advance' && $availableAdvanceLicence <= 0) {
      return redirect()->back()->withErrors('Advance Licence Type is not available to renew');
    } else if ($type == 'Premium' && $availablePremiumLicence <= 0) {
      return redirect()->back()->withErrors('Premium Licence Type is not available to renew');
    } else if ($type == 'PremiumPlus' && $availablePremPlusLicence <= 0) {
      return redirect()->back()->withErrors('Premium Plus Licence Type is not available to renew');
    }
    if (session()->get('cur') == 'dealer') {
      $LicenceList1 = 'S_LicenceList_dealer_' . $username . '_' . $fcode;
    } else if (session()->get('cur') == 'admin') {
      $LicenceList1 = 'S_LicenceList_admin_' . $fcode;
    }
    $current = Carbon::now();
    $LicenceissuedDate = $current->format('d-m-Y');
    $LicenceOnboardDate = $current->format('d-m-Y');
    $LicenceExpiryDate = date("d-m-Y", strtotime(date("d-m-Y", strtotime($LicenceExpiryDateOld)) . " + 1 year"));
    $VehicleExpiryDate = date("Y-m-d", strtotime(date("d-m-Y", strtotime($LicenceExpiryDateOld)) . " + 1 year"));
    $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
    if (session()->get('cur') == 'admin') {
      $franDetails_json = $redis->hget('H_Franchise', $fcode);
      $franchiseDetails = json_decode($franDetails_json, true);
      if ($type == 'Starter') {
        $LicenceId = strtoupper('ST' . uniqid());
        $franchiseDetails['availableStarterLicence'] = $franchiseDetails['availableStarterLicence'] - 1;
      } else if ($type == 'Basic') {
        $LicenceId = strtoupper('BC' . uniqid());
        $franchiseDetails['availableBasicLicence'] = $franchiseDetails['availableBasicLicence'] - 1;
      } else if ($type == 'Advance') {
        $LicenceId = strtoupper('AV' . uniqid());
        $franchiseDetails['availableAdvanceLicence'] = $franchiseDetails['availableAdvanceLicence'] - 1;
      } else if ($type == 'Premium') {
        $LicenceId = strtoupper('PM' . uniqid());
        $franchiseDetails['availablePremiumLicence'] = $franchiseDetails['availablePremiumLicence'] - 1;
      } else if ($type == 'PremiumPlus') {
        $LicenceId = strtoupper('PL' . uniqid());
        $franchiseDetails['availablePremPlusLicence'] = $franchiseDetails['availablePremPlusLicence'] - 1;
      }
      $detailsJson = json_encode($franchiseDetails);
      $redis->hmset('H_Franchise', $fcode, $detailsJson);
    } else if (session()->get('cur') == 'dealer') {
      $dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
      $dealersDetails = json_decode($dealersDetails_json, true);
      if ($type == 'Starter') {
        $LicenceId = strtoupper('ST' . uniqid());
        $dealersDetails['avlofStarter'] = $dealersDetails['avlofStarter'] - 1;
      } else if ($type == 'Basic') {
        $LicenceId = strtoupper('BC' . uniqid());
        $dealersDetails['avlofBasic'] = $dealersDetails['avlofBasic'] - 1;
      } else if ($type == 'Advance') {
        $LicenceId = strtoupper('AV' . uniqid());
        $dealersDetails['avlofAdvance'] = $dealersDetails['avlofAdvance'] - 1;
      } else if ($type == 'Premium') {
        $LicenceId = strtoupper('PM' . uniqid());
        $dealersDetails['avlofPremium'] = $dealersDetails['avlofPremium'] - 1;
      } else if ($type == 'PremiumPlus') {
        $LicenceId = strtoupper('PL' . uniqid());
        $dealersDetails['avlofPremiumPlus'] = $dealersDetails['avlofPremiumPlus'] - 1;
      }
      $detailsJson = json_encode($dealersDetails);
      $redis->hmset('H_DealerDetails_' . $fcode, $username, $detailsJson);
    }
    $LiceneceDataArr = array(
      'LicenceissuedDate' => $LicenceissuedDate,
      'LicenceOnboardDate' => $LicenceOnboardDateOld,
      'LicenceExpiryDate' => $LicenceExpiryDate,
    );
    $redis->hset('H_PreRenewal_Licence_' . $fcode, $vehicleId, $LicenceId);
    // update vehicleExp in renewal licence


    $LiceneceDataArr = json_encode($LiceneceDataArr);
    $redis->hset('H_LicenceExpiry_' . $fcode, $LicenceId, $LiceneceDataArr);

    $LicenceIdmap = $LicenceIdOld . '->' . $LicenceId;
    $DeviceId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicleId);
    $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
    $servername = $franchiesJson;
    $servername = "209.97.163.4";
    if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
      return 'Ipaddress Failed !!!';
    }
    $usernamedb = "root";
    $password = "#vamo123";
    $dbname = $fcode;
    $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
    if (!$conn) {
      die('Could not connect: ' . mysqli_connect_error());
      return 'Please Update One more time Connection failed';
    } else {
      $ins = "INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$username','$LicenceIdmap','$vehicleId','$DeviceId','$type','Renew')";
      $conn->multi_query($ins);
    }
    $conn->close();
    session()->flash('message', $vehicleId . ' Vehicle successfully Renewed ! ');
    return redirect()->to('Billing');
  }

  public function expiredList()
  {
  
    session()->forget('page');
    session()->put('vCol', 1);
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $LicenceList1 = 'S_LicenceList_' . $fcode;
    if (session()->get('cur') == 'dealer') {
      $LicenceList1 = 'S_LicenceList_dealer_' . $username . '_' . $fcode;
    } else if (session()->get('cur') == 'admin') {
      $LicenceList1 = 'S_LicenceList_admin_' . $fcode;
    } else {
      $LicenceList1 = 'S_LicenceList_' . $fcode;
    }
    $vehicleList = null;
    $emptyRef = null;
    $LtypeList = null;
    $deviceList = null;
    $deviceModelList = null;
    $licenceissuedList = null;
    $LicenceExpiryList = null;
    $LicenceOnboardList = null;
    $ExpiredLicenceList = null;
    $dealerList = null;
    $licenceExpList = null;
    $LicenceList = $redis->smembers($LicenceList1);
    // return $redis->smembers('S_ExpiredLicence_Vehicle_' . $fcode);

    // dd(count($LicenceList));
    // $LicenceList = array_slice($LicenceList, 0, 50);
    foreach ($LicenceList as $key => $LicenceId) {
      $vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId);
      $vehicleList = Arr::add($vehicleList, $LicenceId, $vehicleId);
      //vehicle Ref data
      $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
      $vehicleRefData = json_decode($vehicleRefData, true);
      if (isset($vehicleRefData)) {
        $emptyRef = Arr::add($emptyRef, $LicenceId, $vehicleId);
      } else {
        continue;
      }
      $type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
      $LtypeList = Arr::add($LtypeList, $LicenceId, $type);
      $deviceId = isset($vehicleRefData['deviceId']) ? $vehicleRefData['deviceId'] : '-';
      $deviceList = Arr::add($deviceList, $LicenceId, $deviceId);
      $deviceModel = isset($vehicleRefData['deviceModel']) ? $vehicleRefData['deviceModel'] : 'nill';
      $deviceModelList = Arr::add($deviceModelList, $LicenceId, $deviceModel);
      //licence Red date
      $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
      $LicenceRefData = json_decode($LicenceRef, true);
      $LicenceissuedDate = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
      $licenceissuedList = Arr::add($licenceissuedList, $LicenceId, $LicenceissuedDate);
      $LicenceExpiryDate = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
      if ($type == "StarterPlus") {
        $LicenceExpiryDate = "-";
      }

      $LicenceExpiryList = Arr::add($LicenceExpiryList, $LicenceId, $LicenceExpiryDate);
      $LicenceOnboardDate = isset($LicenceRefData['LicenceOnboardDate']) ? $LicenceRefData['LicenceOnboardDate'] : '';
    
      $LicenceOnboardList = Arr::add($LicenceOnboardList, $LicenceId, $LicenceOnboardDate);


      $expiry_date = $LicenceExpiryDate;
      $today = date('Y-m-d', time());
      $exp = date('Y-m-d', strtotime($expiry_date));
      $expDate =  date_create($exp);
      $todayDate = date_create($today);
      $diff =  date_diff($todayDate, $expDate);
      if ($diff->format("%R%a") >= 0 || $LicenceExpiryDate == "-") {
        $redis->srem('S_ExpiredLicence_Vehicle_'.$fcode,$LicenceId);
      } else {
        $licenceExpList = Arr::add($licenceExpList, $LicenceId, $LicenceId);
      }
    }
    $ExpiredLicenceList = $redis->smembers('S_ExpiredLicence_Vehicle_' . $fcode);
    $expiredlistData = [
      'licenceExpList' => $licenceExpList,
      'vehicleList' => $vehicleList,
      'LtypeList' => $LtypeList,
      'deviceList' => $deviceList,
      'deviceModelList' => $deviceModelList,
      'dealerList' => $dealerList,
      'licenceissuedList' => $licenceissuedList,
      'LicenceExpiryList' => $LicenceExpiryList,
      'LicenceOnboardList' => $LicenceOnboardList,
      'ExpiredLicenceList' => $ExpiredLicenceList
    ];
    $licenceDetails = self::getLicenceDetails();
    // $expiredlistData = array_merge($expiredlistData, $licenceDetails);

    return view('vdm.billing.expiredlist', $expiredlistData,$licenceDetails);
  }

  public function preRenewalList()
  {
    
    $username = auth()->id();
    session()->forget('page');
    session()->put('vCol', 1);
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $LicenceList1 = 'S_LicenceList_' . $fcode;
    $vehicleListId = 'S_Vehicles_' . $fcode;
    if (session()->get('cur') == 'dealer') {
      $LicenceList1 = 'S_LicenceList_dealer_' . $username . '_' . $fcode;
      $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
    } else if (session()->get('cur') == 'admin') {
      $LicenceList1 = 'S_LicenceList_admin_' . $fcode;
      $vehicleListId = 'S_Vehicles_Admin_' . $fcode;
    }
    $vehicleList = null;
    $emptyRef = null;
    $LtypeList = null;
    $deviceList = null;
    $deviceModelList = null;
    $licenceissuedList = null;
    $LicenceExpiryList = null;
    $LicenceOnboardList = null;
    $ExpiredLicenceList = null;
    $dealerList = null;
    $preLicenceList = null;
    $LeftofLicence = null;
    $vehicles = null;
    $preLicenceList = null;
    $vehicles = $redis->hkeys('H_PreRenewal_Licence_' . $fcode);
    foreach ($vehicles as $key => $Vehicle_Id) {
      $LicenceIds = $redis->hget('H_PreRenewal_Licence_' . $fcode, $Vehicle_Id);
      if ($redis->sismember($vehicleListId, $Vehicle_Id)) {
        $preLicenceList = Arr::add($preLicenceList, $LicenceIds, $LicenceIds);
      }
    }
    foreach ($vehicles as $key => $vehicleId) {
      $LicenceId = $redis->hget('H_PreRenewal_Licence_' . $fcode, $vehicleId);
      if ($redis->sismember($vehicleListId, $vehicleId)) {
        $vehicleList = Arr::add($vehicleList, $LicenceId, $vehicleId);
      }
      //vehicle Ref data
      $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);

      $vehicleRefData = json_decode($vehicleRefData, true);
      if (isset($vehicleRefData)) {
        $emptyRef = Arr::add($emptyRef, $LicenceId, $vehicleId);
      } else {
        continue;
      }

      $type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
      $LtypeList = Arr::add($LtypeList, $LicenceId, $type);
      $deviceId = isset($vehicleRefData['deviceId']) ? $vehicleRefData['deviceId'] : '-';
      $deviceList = Arr::add($deviceList, $LicenceId, $deviceId);
      $deviceModel = isset($vehicleRefData['deviceModel']) ? $vehicleRefData['deviceModel'] : 'nill';
      $deviceModelList = Arr::add($deviceModelList, $LicenceId, $deviceModel);
      $dealerId = isset($vehicleRefData['OWN']) ? $vehicleRefData['OWN'] : '-';
      $dealerList = Arr::add($dealerList, $LicenceId, $dealerId);
      //licence Red date
      $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
      $LicenceRefData = json_decode($LicenceRef, true);
      $LicenceissuedDate = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
      $LicenceExpiryDate = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
      $LicenceOnboardDate = isset($LicenceRefData['LicenceOnboardDate']) ? $LicenceRefData['LicenceOnboardDate'] : '';

      if ($type == "StarterPlus") {
        $LicenceExpiryDate = "-";
      }

      $licenceissuedList = Arr::add($licenceissuedList, $LicenceId, $LicenceissuedDate);
      $LicenceExpiryList = Arr::add($LicenceExpiryList, $LicenceId, $LicenceExpiryDate);
      $LicenceOnboardList = Arr::add($LicenceOnboardList, $LicenceId, $LicenceOnboardDate);
    }

    $list = 'yes';
    $prerenewalData = [
      'preLicenceList' => $preLicenceList,
      'vehicleList' => $vehicleList,
      'LeftofLicence' => $LeftofLicence,
      'list' => $list,
      'LtypeList' => $LtypeList,
      'deviceList' => $deviceList,
      'deviceModelList' => $deviceModelList,
      'dealerList' => $dealerList,
      'licenceissuedList' => $licenceissuedList,
      'LicenceExpiryList' => $LicenceExpiryList,
      'LicenceOnboardList' => $LicenceOnboardList,
      'ExpiredLicenceList' => $ExpiredLicenceList,
    ];
    $licenceDetails = self::getLicenceDetails();
    // $prerenewalData = array_merge($prerenewalData, $licenceDetails);
    return view('vdm.billing.prerenewal', $prerenewalData,$licenceDetails);
  }

  public function preRenewal()
  {
    
    $username = auth()->id();
    session()->forget('page');
    session()->put('vCol', 1);
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $LicenceList1 = 'S_LicenceList_' . $fcode;
    if (session()->get('cur') == 'dealer') {
      $LicenceList1 = 'S_LicenceList_dealer_' . $username . '_' . $fcode;
    } else if (session()->get('cur') == 'admin') {
      $LicenceList1 = 'S_LicenceList_admin_' . $fcode;
    } else {
      $LicenceList1 = 'S_LicenceList_' . $fcode;
    }
    $vehicleList = null;
    $emptyRef = null;
    $LtypeList = null;
    $deviceList = null;
    $deviceModelList = null;
    $licenceissuedList = null;
    $LicenceExpiryList = null;
    $LicenceOnboardList = null;
    $ExpiredLicenceList = null;
    $dealerList = null;
    $preLicenceList = null;
    $LeftofLicence = null;
    $vehicles = null;
    $vehicles = $redis->hkeys('H_PreRenewal_Licence_' . $fcode);
    $LicenceList = $redis->smembers($LicenceList1);
    // ddd($LicenceList);
    // $LicenceList = array_slice($LicenceList, 0, 100);
    foreach ($LicenceList as $key => $LicenceId) {
      $vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId);
      $vehicleList = Arr::add($vehicleList, $LicenceId, $vehicleId);
      //vehicle Ref data
      $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
      if (in_array($vehicleId, $vehicles)) {
        continue;
      }
      $vehicleRefData = json_decode($vehicleRefData, true);
      if (isset($vehicleRefData)) {
        $emptyRef = Arr::add($emptyRef, $LicenceId, $vehicleId);
      } else {
        continue;
      }
      $type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
      $LtypeList = Arr::add($LtypeList, $LicenceId, $type);
      $deviceId = isset($vehicleRefData['deviceId']) ? $vehicleRefData['deviceId'] : '-';
      $deviceList = Arr::add($deviceList, $LicenceId, $deviceId);
      $deviceModel = isset($vehicleRefData['deviceModel']) ? $vehicleRefData['deviceModel'] : 'nill';
      $deviceModelList = Arr::add($deviceModelList, $LicenceId, $deviceModel);
      $dealerId = isset($vehicleRefData['OWN']) ? $vehicleRefData['OWN'] : '-';
      $dealerList = Arr::add($dealerList, $LicenceId, $dealerId);
      //licence Red date
      $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
      $LicenceRefData = json_decode($LicenceRef, true);
      $LicenceissuedDate = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
      $licenceissuedList = Arr::add($licenceissuedList, $LicenceId, $LicenceissuedDate);
      $LicenceExpiryDate = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
      $LicenceExpiryList = Arr::add($LicenceExpiryList, $LicenceId, $LicenceExpiryDate);
      $LicenceOnboardDate = isset($LicenceRefData['LicenceOnboardDate']) ? $LicenceRefData['LicenceOnboardDate'] : '';
      $LicenceOnboardList = Arr::add($LicenceOnboardList, $LicenceId, $LicenceOnboardDate);


      $date1 = $LicenceExpiryDate;
      $now = time();
      $LicenceExpiryDate1 = strtotime($LicenceExpiryDate);
      $date2 = date('d-m-Y', strtotime('+1 months'));
      $date3 = date('d-m-Y', time());
      if ((strtotime($date1) <= strtotime($date2)) && (strtotime($date1) >= strtotime($date3))) {
        $preLicenceList = Arr::add($preLicenceList, $LicenceId, $LicenceId);
        $datediff = $LicenceExpiryDate1 - $now;
        $daysleft = round($datediff / (60 * 60 * 24));
        //$daysleft=date_diff($date1->diff($date3);
        $LeftofLicence = Arr::add($LeftofLicence, $LicenceId, $daysleft);
      } else {
      }
    }


    $list = 'no';
    $prerenewalData = [
      'preLicenceList' => $preLicenceList,
      'vehicleList' => $vehicleList,
      'LeftofLicence' => $LeftofLicence,
      'list' => $list,
      'LtypeList' => $LtypeList,
      'deviceList' => $deviceList,
      'deviceModelList' => $deviceModelList,
      'dealerList' => $dealerList,
      'licenceissuedList' => $licenceissuedList,
      'LicenceExpiryList' => $LicenceExpiryList,
      'LicenceOnboardList' => $LicenceOnboardList,
      'ExpiredLicenceList' => $ExpiredLicenceList,
    ];
    $licenceDetails = self::getLicenceDetails();
    $prerenewalData = array_merge($prerenewalData, $licenceDetails);
    return view('vdm.billing.prerenewal', $prerenewalData,$licenceDetails);
  }


  public function store()
  {
    
    $username = auth()->id();
    $redis = Redis::connection();
    $rules = array(
      'renewalLicence' => 'required'
    );
    $validator = validator()->make(request()->all(), $rules);
    if ($validator->fails()) {
      return redirect()->back()->withErrors('Please choose the vehicle to renewal!');
    }
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $renewalLicence = request()->get('renewalLicence');

    $noStLicence = 0;
    $noBcLicence = 0;
    $noAdLicence = 0;
    $noPrLicence = 0;
    $noPrPlLicence = 0;
    $starterLicence = null;
    $basicLicence = null;
    $advenceLicence = null;
    $primiumLicence = null;
    $primiumPlusLicence = null;
    $notAvailbleST = null;
    $notAvailbleBC = null;
    $notAvailbleAD = null;
    $notAvailblePR = null;
    $notAvailblePRPL = null;
    foreach ($renewalLicence as $key => $OldLicenceId) {
      $vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $OldLicenceId);
      $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
      $vehicleRefData = json_decode($vehicleRefData, true);
      $type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
      if ($type == 'Starter') {
        $noStLicence = $noStLicence + 1;
        $starterLicence = Arr::add($starterLicence, $OldLicenceId, $OldLicenceId);
      } else if ($type == 'Basic') {
        $noBcLicence = $noBcLicence + 1;
        $basicLicence = Arr::add($basicLicence, $OldLicenceId, $OldLicenceId);
      } else if ($type == 'Advance') {
        $noAdLicence = $noAdLicence + 1;
        $advenceLicence = Arr::add($advenceLicence, $OldLicenceId, $OldLicenceId);
      } else if ($type == 'Premium') {
        $noPrLicence = $noPrLicence + 1;
        $primiumLicence = Arr::add($primiumLicence, $OldLicenceId, $OldLicenceId);
      } else if ($type == 'PremiumPlus') {
        $noPrPlLicence = $noPrPlLicence + 1;
        $primiumPlusLicence = Arr::add($primiumPlusLicence, $OldLicenceId, $OldLicenceId);
      }
    }
    if (session()->get('cur') == 'admin') {
      $franDetails_json = $redis->hget('H_Franchise', $fcode);
      $franchiseDetails = json_decode($franDetails_json, true);
      $availableStarterLicence = isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : '0';
      $availableBasicLicence = isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';
      $availableAdvanceLicence = isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';
      $availablePremiumLicence = isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';
      $availablePremPlusLicence = isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0';
    } else if (session()->get('cur') == 'dealer') {
      $dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
      $dealersDetails = json_decode($dealersDetails_json, true);
      $availableStarterLicence = isset($dealersDetails['avlofStarter']) ? $dealersDetails['avlofStarter'] : '0';
      $availableBasicLicence = isset($dealersDetails['avlofBasic']) ? $dealersDetails['avlofBasic'] : '0';
      $availableAdvanceLicence = isset($dealersDetails['avlofAdvance']) ? $dealersDetails['avlofAdvance'] : '0';
      $availablePremiumLicence = isset($dealersDetails['avlofPremium']) ? $dealersDetails['avlofPremium'] : '0';
      $availablePremPlusLicence = isset($dealersDetails['avlofPremiumPlus']) ? $dealersDetails['avlofPremiumPlus'] : '0';
    }
    $st = 0;
    $bc = 0;
    $ad = 0;
    $pr = 0;
    $prpl = 0;
    if ($noStLicence <= $availableStarterLicence && $starterLicence != null) {
      foreach ($starterLicence as $key => $LicenceId) {
        $newlicenceId = VdmBillingController::renewaloldLicence($LicenceId);
        $bc = $bc + 1;
      }
    } else {
      if ($starterLicence != null) {
        $notAvailbleST = "Starter";
      }
    }
    if ($noBcLicence <= $availableBasicLicence && $basicLicence != null) {
      foreach ($basicLicence as $key => $LicenceId) {
        $newlicenceId = VdmBillingController::renewaloldLicence($LicenceId);
        $bc = $bc + 1;
      }
    } else {
      if ($basicLicence != null) {
        $notAvailbleBC = "Basic";
      }
    }

    if ($noAdLicence <= $availableAdvanceLicence && $advenceLicence != null) {
      foreach ($advenceLicence as $key => $LicenceId) {
        $newlicenceId = VdmBillingController::renewaloldLicence($LicenceId);
        $ad = $ad + 1;
      }
    } else {
      if ($advenceLicence != null) {
        $notAvailbleAD = "Advance";
      }
    }
    if ($noPrLicence <= $availablePremiumLicence && $primiumLicence != null) {
      foreach ($primiumLicence as $key => $LicenceId) {
        $newlicenceId = VdmBillingController::renewaloldLicence($LicenceId);
        $pr = $pr + 1;
      }
    } else {
      if ($primiumLicence != null) {
        $notAvailblePR = "Premium";
      }
    }
    if ($noPrPlLicence <= $availablePremPlusLicence && $primiumPlusLicence != null) {
      foreach ($primiumPlusLicence as $key => $LicenceId) {
        $newlicenceId = VdmBillingController::renewaloldLicence($LicenceId);
        $prpl = $prpl + 1;
      }
    } else {
      if ($primiumPlusLicence != null) {
        $notAvailblePRPL = "primiumPlusLicence";
      }
    }

    $error = "";
    if ($bc != 0 && $ad != 0 && $pr != 0 && $prpl != 0) {
      session()->flash('message', 'Successfully Renewed Basic:' . $bc . ' , Advance:' . $ad . ' , Premium:' . $pr . ' , Premium Plus:' . $prpl . ' !');
    } else if ($bc != 0 && $ad != 0 && $pr != 0 && $prpl == 0) {
      session()->flash('message', 'Successfully Renewed Basic:' . $bc . ' , Advance:' . $ad . ' , Premium:' . $pr . ' !');
    } else if ($bc == 0 && $ad != 0 && $pr != 0 && $prpl != 0) {
      session()->flash('message', 'Successfully Renewed Advance:' . $ad . ' , Premium:' . $pr . ' , Premium Plus:' . $prpl . ' !');
    } else if ($bc != 0 && $ad == 0 && $pr != 0 && $prpl != 0) {
      session()->flash('message', 'Successfully Renewed Basic:' . $bc . ' , Premium:' . $pr . ' , Premium Plus:' . $prpl . ' !');
    } else if ($bc != 0 && $ad != 0 && $pr == 0 && $prpl == 0) {
      session()->flash('message', 'Successfully Renewed Basic:' . $bc . ' , Advance:' . $ad . ' !');
    } else if ($bc != 0 && $ad == 0 && $pr != 0 && $prpl == 0) {
      session()->flash('message', 'Successfully Renewed Basic:' . $bc . ' , Premium:' . $pr . ' !');
    } else if ($bc != 0 && $ad == 0 && $pr == 0 && $prpl != 0) {
      session()->flash('message', 'Successfully Renewed Basic:' . $bc . ' , Premium Plus:' . $prpl . ' !');
    } else if ($bc == 0 && $ad != 0 && $pr != 0 && $prpl == 0) {
      session()->flash('message', 'Successfully Renewed Advance:' . $ad . ' , Premium:' . $pr . ' !');
    } else if ($bc == 0 && $ad != 0 && $pr == 0 && $prpl != 0) {
      session()->flash('message', 'Successfully Renewed Advance:' . $ad . ' , Premium Plus:' . $prpl . ' !');
    } else if ($bc == 0 && $ad == 0 && $pr != 0 && $prpl != 0) {
      session()->flash('message', 'Successfully Renewed Premium:' . $pr . ' , Premium Plus:' . $prpl . ' !');
    } else if ($bc != 0 && $ad == 0 && $pr == 0 && $prpl == 0) {
      session()->flash('message', 'Successfully Renewed Basic:' . $bc . ' !');
    } else if ($bc == 0 && $ad != 0 && $pr == 0 && $prpl == 0) {
      session()->flash('message', 'Successfully Renewed Advance:' . $ad . ' !');
    } else if ($bc == 0 && $ad == 0 && $pr != 0 && $prpl == 0) {
      session()->flash('message', 'Successfully Renewed Premium:' . $pr . ' !');
    } else if ($bc == 0 && $ad == 0 && $pr == 0 && $prpl != 0) {
      session()->flash('message', 'Successfully Renewed Premium Plus:' . $prpl . ' !');
    } else {
      //session()->flash ( 'message', 'Unable to renew !' ); 
    }
    if ($notAvailbleST != null || $notAvailbleBC != null || $notAvailbleAD != null || $notAvailblePR != null || $notAvailblePRPL != null) {
      $error = $notAvailbleST . ' ' . $notAvailbleBC . ' ' . $notAvailbleAD . ' ' . $notAvailblePR . ' ' . $notAvailblePRPL . ' Licence type not available !';
    }

    return redirect()->to('Billing')->withErrors($error);
  }
  public function renewaloldLicence($id)
  {
    
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $LicenceIdOld = $id;
    $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceIdOld);
    $LicenceissuedDateOld = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
    $LicenceExpiryDateOld = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
    $LicenceOnboardDateOld = isset($LicenceRefData['LicenceOnboardDate']) ? $LicenceRefData['LicenceOnboardDate'] : '';
    $vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceIdOld);

    $LicenceRefData = json_decode($LicenceRef, true);
    $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
    $vehicleRefData = json_decode($vehicleRefData, true);
    $type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';

    $redis->hdel('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceIdOld);
    $redis->hdel('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleId);
    $redis->hdel('H_LicenceExpiry_' . $fcode, $LicenceIdOld);
    if (session()->get('cur') == 'dealer') {
      $LicenceList1 = 'S_LicenceList_dealer_' . $username . '_' . $fcode;
    } else if (session()->get('cur') == 'admin') {
      $LicenceList1 = 'S_LicenceList_admin_' . $fcode;
    }
    $redis->srem($LicenceList1, $LicenceIdOld);
    $redis->srem('S_LicenceList_' . $fcode, $LicenceIdOld);
    $current = Carbon::now();
    $LicenceissuedDate = $current->format('d-m-Y');
    $LicenceOnboardDate = $current->format('d-m-Y');
    $LicenceExpiryDate = date('d-m-Y', strtotime(date("d-m-Y", time()) . "+ 1 year"));
    //$vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
    $vehExpiryDate = date('Y-m-d', strtotime(date("Y-m-d", time()) . "+ 1 year"));

    if (session()->get('cur') == 'admin') {
      $franDetails_json = $redis->hget('H_Franchise', $fcode);
      $franchiseDetails = json_decode($franDetails_json, true);
      if ($type == 'Starter') {
        $LicenceId = strtoupper('ST' . uniqid());
        $franchiseDetails['availableStarterLicence'] = $franchiseDetails['availableStarterLicence'] - 1;
      } else if ($type == 'Basic') {
        $LicenceId = strtoupper('BC' . uniqid());
        $franchiseDetails['availableBasicLicence'] = $franchiseDetails['availableBasicLicence'] - 1;
      } else if ($type == 'Advance') {
        $LicenceId = strtoupper('AV' . uniqid());
        $franchiseDetails['availableAdvanceLicence'] = $franchiseDetails['availableAdvanceLicence'] - 1;
      } else if ($type == 'Premium') {
        $LicenceId = strtoupper('PM' . uniqid());
        $franchiseDetails['availablePremiumLicence'] = $franchiseDetails['availablePremiumLicence'] - 1;
      } else if ($type == 'PremiumPlus') {
        $LicenceId = strtoupper('PL' . uniqid());
        $franchiseDetails['availablePremPlusLicence'] = $franchiseDetails['availablePremPlusLicence'] - 1;
      }
      $detailsJson = json_encode($franchiseDetails);
      $redis->hmset('H_Franchise', $fcode, $detailsJson);
    } else if (session()->get('cur') == 'dealer') {
      $dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
      $dealersDetails = json_decode($dealersDetails_json, true);
      if ($type == 'Starter') {
        $LicenceId = strtoupper('ST' . uniqid());
        $dealersDetails['avlofStarter'] = $dealersDetails['avlofStarter'] - 1;
      } else if ($type == 'Basic') {
        $LicenceId = strtoupper('BC' . uniqid());
        $dealersDetails['avlofBasic'] = $dealersDetails['avlofBasic'] - 1;
      } else if ($type == 'Advance') {
        $LicenceId = strtoupper('AV' . uniqid());
        $dealersDetails['avlofAdvance'] = $dealersDetails['avlofAdvance'] - 1;
      } else if ($type == 'Premium') {
        $LicenceId = strtoupper('PM' . uniqid());
        $dealersDetails['avlofPremium'] = $dealersDetails['avlofPremium'] - 1;
      } else if ($type == 'PremiumPlus') {
        $LicenceId = strtoupper('PL' . uniqid());
        $dealersDetails['avlofPremiumPlus'] = $dealersDetails['avlofPremiumPlus'] - 1;
      }
      $detailsJson = json_encode($dealersDetails);
      $redis->hmset('H_DealerDetails_' . $fcode, $username, $detailsJson);
    }
    //$LicenceId = strtoupper(base64_encode($LicenceIdOld));
    $redis->hset('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId, $vehicleId);
    $redis->hset('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleId, $LicenceId);
    $LiceneceDataArr = array(
      'LicenceissuedDate' => $LicenceissuedDate,
      'LicenceOnboardDate' => $LicenceOnboardDate,
      'LicenceExpiryDate' => $LicenceExpiryDate,
    );
    $LiceneceDataArr = json_encode($LiceneceDataArr);
    $redis->hset('H_LicenceExpiry_' . $fcode, $LicenceId, $LiceneceDataArr);
    // set Vehicleexpiry
    $vehicleRefData['vehicleExpiry'] = $vehExpiryDate;
    $vehicleRefDataArr = json_encode($vehicleRefData);
    $redis->hset('H_RefData_' . $fcode, $vehicleId, $vehicleRefDataArr);

    if (session()->get('cur') == 'dealer') {
      $LicenceList1 = 'S_LicenceList_dealer_' . $username . '_' . $fcode;
    } else if (session()->get('cur') == 'admin') {
      $LicenceList1 = 'S_LicenceList_admin_' . $fcode;
    }
    $redis->sadd($LicenceList1, $LicenceId);
    $redis->sadd('S_LicenceList_' . $fcode, $LicenceId);
    $LicenceIdmap = $LicenceIdOld . '->' . $LicenceId;
    $DeviceId = $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicleId);
    $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
    $servername = $franchiesJson;
    $servername = "209.97.163.4";
    if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
      return 'Ipaddress Failed !!!';
    }
    $usernamedb = "root";
    $password = "#vamo123";
    $dbname = $fcode;
    $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
    if (!$conn) {
      die('Could not connect: ' . mysqli_connect_error());
      return 'Please Update One more time Connection failed';
    } else {
      $ins = "INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$username','$LicenceIdmap','$vehicleId','$DeviceId','$type','Renew')";
      if ($conn->multi_query($ins)) {
        logger(' Inserted the licence ');
      } else {
        logger('Not Inserted the licence ');
      }
    }
    $conn->close();
    return $LicenceId;
  }

  public function licenceList()
  {
    
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $status = request()->get('val');

    if (session()->get('cur') == 'dealer') {
      $LicenceList1 = 'S_LicenceList_dealer_' . $username . '_' . $fcode;
    } else if (session()->get('cur') == 'admin') {
      $LicenceList1 = 'S_LicenceList_admin_' . $fcode;
    } else {
      $LicenceList1 = 'S_LicenceList_' . $fcode;
    }
    $emptyRef = null;
    $LicenceList = $redis->smembers($LicenceList1);
    foreach ($LicenceList as $key => $LicenceId) {
      $vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId);

      $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
      $vehicleRefData = json_decode($vehicleRefData, true);
      if (isset($vehicleRefData)) {
        $emptyRef = Arr::add($emptyRef, $LicenceId, $vehicleId);
      } else {
        continue;
      }
      $type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';

      $deviceId = isset($vehicleRefData['deviceId']) ? $vehicleRefData['deviceId'] : '-';

      $deviceModel = isset($vehicleRefData['deviceModel']) ? $vehicleRefData['deviceModel'] : 'nill';

      $dealerId = isset($vehicleRefData['OWN']) ? $vehicleRefData['OWN'] : '-';

      //licence Red date
      $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
      $LicenceRefData = json_decode($LicenceRef, true);
      $LicenceissuedDate = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';

      $LicenceExpiryDate = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';

      $LicenceOnboardDate = isset($LicenceRefData['LicenceOnboardDate']) ? $LicenceRefData['LicenceOnboardDate'] : '';

      $expiry_date = $LicenceExpiryDate;
      $today = date('Y-m-d', time());
      $exp = date('Y-m-d', strtotime($expiry_date));
      $expDate =  date_create($exp);
      $todayDate = date_create($today);
      $diff =  date_diff($todayDate, $expDate);
      if ($diff->format("%R%a") > 0) {
        $satus = 'Active';
      } else {
        $satus = 'Expired';
      }

      $return_arr[] = array(
        'LicenceId'     => $LicenceId,
        'VehicleId'     => $vehicleId,
        'LicenceType'   => $type,
        'DeviceId'      => $deviceId,
        'DeviceModel'   => $deviceModel,
        'DealerName'    => $dealerId,
        'LicenceissuedDate' => $LicenceissuedDate,
        'LicenceExpiryDate' => $LicenceExpiryDate,
        'LicenceOnboardDate'  => $LicenceOnboardDate,
        'status'        => $satus
      );
    }

    $LicenceLists = json_encode($return_arr);
    return response()->json($LicenceLists);
  }

  public function LicenceCancel($veh)
  {
   
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $vehicleId = $veh;
    $OldLicenceId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vehicleId);
    $newlicenceId = $redis->hget('H_PreRenewal_Licence_' . $fcode, $vehicleId);
    $redis->hdel('H_PreRenewal_Licence_' . $fcode, $vehicleId);
    $redis->hdel('H_LicenceExpiry_' . $fcode, $newlicenceId);
    $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
    $vehicleRefData = json_decode($vehicleRefData, true);
    $type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';

    if (session()->get('cur') == 'admin') {
      $franDetails_json = $redis->hget('H_Franchise', $fcode);
      $franchiseDetails = json_decode($franDetails_json, true);
      if ($type == 'Starter') {
        $franchiseDetails['availableStarterLicence'] = $franchiseDetails['availableStarterLicence'] + 1;
      } else if ($type == 'Basic') {
        $franchiseDetails['availableBasicLicence'] = $franchiseDetails['availableBasicLicence'] + 1;
      } else if ($type == 'Advance') {
        $franchiseDetails['availableAdvanceLicence'] = $franchiseDetails['availableAdvanceLicence'] + 1;
      } else if ($type == 'Premium') {
        $franchiseDetails['availablePremiumLicence'] = $franchiseDetails['availablePremiumLicence'] + 1;
      } else if ($type == 'PremiumPlus') {
        $franchiseDetails['availablePremPlusLicence'] = $franchiseDetails['availablePremPlusLicence'] + 1;
      }
      $detailsJson = json_encode($franchiseDetails);
      $redis->hmset('H_Franchise', $fcode, $detailsJson);
    } else if (session()->get('cur') == 'dealer') {
      $dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
      $dealersDetails = json_decode($dealersDetails_json, true);
      if ($type == 'Starter') {
        $dealersDetails['avlofStarter'] = $dealersDetails['avlofStarter'] + 1;
      } else if ($type == 'Basic') {
        $dealersDetails['avlofBasic'] = $dealersDetails['avlofBasic'] + 1;
      } else if ($type == 'Advance') {
        $dealersDetails['avlofAdvance'] = $dealersDetails['avlofAdvance'] + 1;
      } else if ($type == 'Premium') {
        $dealersDetails['avlofPremium'] = $dealersDetails['avlofPremium'] + 1;
      } else if ($type == 'PremiumPlus') {
        $dealersDetails['avlofPremiumPlus'] = $dealersDetails['avlofPremiumPlus'] + 1;
      }
      $detailsJson = json_encode($dealersDetails);
      $redis->hmset('H_DealerDetails_' . $fcode, $username, $detailsJson);
    }
    session()->flash('message', $vehicleId . ' Vehicle successfully Cancelled ! One Licence Added to the ' . $type . ' Licence!');
    return redirect()->to('Billing');
  }


  //Licence Convertion
  public function licConvert()
  {
    
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

    $licConvertData = self::getLicenceDetails();

    return view('vdm.billing.licConvert', $licConvertData);
  }

  public function licUpdate()
  {
  
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $rules = array(
      'LicenceTypes' => 'required',
      'numOfLic' => 'required',
      'cnvLicenceTypes' => 'required'
    );
    $validator = validator()->make(request()->all(), $rules);
    if ($validator->fails()) {
      return redirect()->back()->withErrors('Please Enter Vehicle Id / Licence Id !');
    }
    if (session()->get('cur') == 'admin') {
      $franDetails_json = $redis->hget('H_Franchise', $fcode);
      $franchiseDetails = json_decode($franDetails_json, true);
      $availableStarterLicence = isset($franchiseDetails['availableStarterLicence']) ? $franchiseDetails['availableStarterLicence'] : '0';
      $availableBasicLicence = isset($franchiseDetails['availableBasicLicence']) ? $franchiseDetails['availableBasicLicence'] : '0';
      $availableAdvanceLicence = isset($franchiseDetails['availableAdvanceLicence']) ? $franchiseDetails['availableAdvanceLicence'] : '0';
      $availablePremiumLicence = isset($franchiseDetails['availablePremiumLicence']) ? $franchiseDetails['availablePremiumLicence'] : '0';
      $availablePremPlusLicence = isset($franchiseDetails['availablePremPlusLicence']) ? $franchiseDetails['availablePremPlusLicence'] : '0';
    } else if (session()->get('cur') == 'dealer') {
      $dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
      $dealersDetails = json_decode($dealersDetails_json, true);
      $availableStarterLicence = isset($dealersDetails['avlofStarter']) ? $dealersDetails['avlofStarter'] : '0';
      $availableBasicLicence = isset($dealersDetails['avlofBasic']) ? $dealersDetails['avlofBasic'] : '0';
      $availableAdvanceLicence = isset($dealersDetails['avlofAdvance']) ? $dealersDetails['avlofAdvance'] : '0';
      $availablePremiumLicence = isset($dealersDetails['avlofPremium']) ? $dealersDetails['avlofPremium'] : '0';
      $availablePremPlusLicence = isset($dealersDetails['avlofPremiumPlus']) ? $dealersDetails['avlofPremiumPlus'] : '0';
    }
    if ($availableStarterLicence == null) {
      $availableStarterLicence = 0;
    }
    if ($availableBasicLicence == null) {
      $availableBasicLicence = 0;
    }
    if ($availableAdvanceLicence == null) {
      $availableAdvanceLicence = 0;
    }
    if ($availablePremiumLicence == null) {
      $availablePremiumLicence = 0;
    }
    if ($availablePremPlusLicence == null) {
      $availablePremPlusLicence = 0;
    }
    $LicenceTypes = request()->get('LicenceTypes');
    $numOfLic = request()->get('numOfLic');
    $oldnumOfLic = $numOfLic;
    $cnvLicenceTypes = request()->get('cnvLicenceTypes');

    $radio = [
      'Starter' => 0.5,
      'Basic' => 1,
      'Advance' => 1.5,
      'Premium' => 2,
      'PremiumPlus' => 3
    ];
    $licenceList = [
      'Starter' => 'Starter',
      'Basic' => 'Basic',
      'Advance' => 'Advance',
      'Premium' => 'Premium',
      'PremiumPlus' => 'PremPlus'
    ];
    $st_ratio = 0.5;
    $bc_ratio = 1;
    $ad_ratio = 1.5;
    $pr_ratio = 2;
    $prpl_ratio = 3;
    $addedcount = 0;
    // dd(request()->all());
    if ($LicenceTypes == $cnvLicenceTypes) {
      return redirect()->back()->withInput()->withErrors('Licence type should not be same !');
    } else {
      $reminingLic = (int)request()->get('reminingLic');


      // test
      // $convertLicence = (($radio[$LicenceTypes] * $numOfLic) - ($radio[$LicenceTypes]));

      $convertLicence = (int)((($numOfLic - $reminingLic) * $radio[$LicenceTypes]) / $radio[$cnvLicenceTypes]);
      $addedcount = $convertLicence;
      if (session()->get('cur') == 'admin') {
        $franDetails_json = $redis->hget('H_Franchise', $fcode);
        $franchiseDetails = json_decode($franDetails_json, true);

        $franchiseDetails['available' . $licenceList[$LicenceTypes] . 'Licence'] = $franchiseDetails['available' . $licenceList[$LicenceTypes] . 'Licence'] - $numOfLic + $reminingLic;
        $franchiseDetails['numberof' . $licenceList[$LicenceTypes] . 'Licence'] = $franchiseDetails['numberof' . $licenceList[$LicenceTypes] . 'Licence'] - $numOfLic + $reminingLic;
        $franchiseDetails['available' . $licenceList[$cnvLicenceTypes] . 'Licence'] = $franchiseDetails['available' . $licenceList[$cnvLicenceTypes] . 'Licence'] + $convertLicence;
        $franchiseDetails['numberof' . $licenceList[$cnvLicenceTypes] . 'Licence'] = $franchiseDetails['numberof' . $licenceList[$cnvLicenceTypes] . 'Licence'] + $convertLicence;

        // dd(['reminingLic'=>$reminingLic,'convertLicence'=>$convertLicence,'numOfLic'=>$numOfLic,$licenceList[$LicenceTypes]=>$licenceList[$cnvLicenceTypes]],json_decode($franDetails_json, true),$franchiseDetails);

      } else if (session()->get('cur') == 'dealer') {
        $dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
        $dealersDetails = json_decode($dealersDetails_json, true);
        $licenceList = array_merge($licenceList, ['PremiumPlus' => 'PremiumPlus']);
        $dealersDetails['avlof' . $licenceList[$LicenceTypes]] = $dealersDetails['avlof' . $licenceList[$LicenceTypes]] - $numOfLic + $reminingLic;
        $dealersDetails['numof' . $licenceList[$LicenceTypes]] = $dealersDetails['numof' . $licenceList[$LicenceTypes]] - $numOfLic + $reminingLic;
        $dealersDetails['avlof' . $licenceList[$cnvLicenceTypes]] = $dealersDetails['avlof' . $licenceList[$cnvLicenceTypes]] + $convertLicence;
        $dealersDetails['numof' . $licenceList[$cnvLicenceTypes]] = $dealersDetails['numof' . $licenceList[$cnvLicenceTypes]] + $convertLicence;
      }
      // test end

      //end of else with Licence type not equal to cnv Licence
    }
    if (session()->get('cur') == 'admin') {
      $detailsJson = json_encode($franchiseDetails);
      $redis->hmset('H_Franchise', $fcode, $detailsJson);
    } else if (session()->get('cur') == 'dealer') {
      $detailsJson = json_encode($dealersDetails);
      $redis->hmset('H_DealerDetails_' . $fcode, $username, $detailsJson);
    }
    if ($addedcount != 0) {
      $remining = $oldnumOfLic - $numOfLic;

      //before
      if (session()->get('cur') == 'dealer') {
        $detailJson = $redis->hget('H_DealerDetails_' . $fcode, $username);
        $detail = json_decode($detailJson, true);
        $mysqlDetails = array();
        $status = array();
        $status = array(
          'fcode' => $fcode,
          'dealerId' => $username,
          'userName' => $username,
          'status' => config()->get('constant.shuffle'),
          'userIpAddress' => session()->get('userIP'),
          'Details' => $numOfLic . ' ' . $LicenceTypes . ' converted into ' . $addedcount . ' ' . $cnvLicenceTypes . ' ' . $remining . ' Not converted',
        );
        $mysqlDetails   = array_merge($status, $detail);
        $modelname = new AuditDealer();
        $table = $modelname->getTable();
        $db = $fcode;
        AuditTables::ChangeDB($db);
        $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
        if (count($tableExist) > 0) {
          AuditDealer::create($mysqlDetails);
        } else {
          AuditTables::CreateAuditDealer();
          AuditDealer::create($mysqlDetails);
        }
        AuditTables::ChangeDB(config()->get('constant.VAMOSdb'));
      } else if (session()->get('cur') == 'admin') {
        $franDetails_json = $redis->hget('H_Franchise', $fcode);
        $franchiseDetails = json_decode($franDetails_json, true);
        $mysqlDetails = array();
        $status = array();
        $status = array(
          'username' => $username,
          'status' => config()->get('constant.shuffle'),
          'userIpAddress' => session()->get('userIP'),
          'fcode' => $fcode,
          'Details' => $numOfLic . ' ' . $LicenceTypes . ' converted into ' . $addedcount . ' ' . $cnvLicenceTypes . ' ' . $remining . ' Not converted',
        );
        $mysqlDetails   = array_merge($status, $franchiseDetails);
        $modelname = new AuditFrans();
        $table = $modelname->getTable();
        $db = 'VAMOSYS';
        AuditTables::ChangeDB($db);
        $tableExist = DB::table('information_schema.tables')->where('table_schema', '=', $db)->where('table_name', '=', $table)->get();
        //return $tableExist;
        if (count($tableExist) > 0) {
          AuditFrans::create($mysqlDetails);
        } else {
          AuditTables::CreateAuditFrans();
          AuditFrans::create($mysqlDetails);
        }
        //AuditFrans::create($details);
      }
      //end of befor


      if ($remining != 0) {
        session()->flash('alert-class', "alert-success");
        session()->flash('message', $numOfLic . ' ' . $LicenceTypes . ' licence type successfully converted into ' . $addedcount . ' ' . $cnvLicenceTypes . ' licence Type. Remaining ' . $remining . ' Added into ' . $LicenceTypes . ' licence type !');
        return redirect()->back();
      } else {
        session()->flash('alert-class', "alert-success");
        session()->flash('message', $numOfLic . ' ' . $LicenceTypes . ' licence type successfully converted into ' . $addedcount . ' ' . $cnvLicenceTypes . ' licence Type !');
        return redirect()->back();
      }
    } else {
      return redirect()->back()->withErrors('Enter valid quantity to shuffle from ' . $LicenceTypes . ' into' . $cnvLicenceTypes . ' licence Type !');
    }
  }

  //Licence Search
  public function licSearch()
  {
  
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $rules = array(
      'licSearch' => 'required'
    );
    $validator = validator()->make(request()->all(), $rules);
    if ($validator->fails()) {
      return redirect()->back()->withErrors('Please Enter Vehicle Id / Licence Id !');
    }
    $licKeyWord = request()->get('licSearch');
    $cou = $redis->hlen('H_LicenceExpiry_' . $fcode);
    $orgLi = $redis->HScan('H_LicenceExpiry_' . $fcode, 0,  'count', $cou, 'match', '*' . $licKeyWord . '*');
    $orgL = end($orgLi);
    $licenceList = null;
    $licenceissuedList = null;
    $licenceOnboardList = null;
    $licenceExpList = null;
    $licenType = null;
    $statusList = null;
    $vehicleList = null;
    foreach ($orgL as $key => $value) {
      $LicenceId = $key;
      if (session()->get('cur') == 'dealer') {
        $licenceNameMap = 'S_LicenceList_dealer_' . $username . '_' . $fcode;
      } else if (session()->get('cur') == 'admin') {
        $licenceNameMap = 'S_LicenceList_admin_' . $fcode;
      }
      $prerenewalNameMap = 'H_PreRenewal_Licence_' . $fcode;
      $preEx = 0;
      $listVeh = $redis->hgetall($prerenewalNameMap);
      foreach ($listVeh as $keys => $val) {
        $lice = $redis->hget($prerenewalNameMap, $keys);
        if ($lice == $LicenceId) {
          $veh = $keys;
          $preEx = 1;
        }
      }
      $licList = $redis->sismember($licenceNameMap, $LicenceId);
      if ($licList == 1) {
        $licenceList = Arr::add($licenceList, $LicenceId, $LicenceId);
        $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
        $LicenceRefData = json_decode($LicenceRef, true);
        $LicenceissuedDate = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
        $LicenceExpiryDate = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';

        $expiry_date = $LicenceExpiryDate;
        $today = date('Y-m-d', time());
        $exp = date('Y-m-d', strtotime($expiry_date));
        $expDate =  date_create($exp);
        $todayDate = date_create($today);
        $diff =  date_diff($todayDate, $expDate);
        if ($diff->format("%R%a") > 0) {
          $date1 = $LicenceExpiryDate;
          $now = time();
          $LicenceExpiryDate1 = strtotime($LicenceExpiryDate);
          $date2 = date('d-m-Y', strtotime('+1 months'));
          $date3 = date('d-m-Y', time());
          if ((strtotime($date1) <= strtotime($date2)) && (strtotime($date1) >= strtotime($date3))) {
            $datediff = $LicenceExpiryDate1 - $now;
            $daysleft = round($datediff / (60 * 60 * 24));
            $statusList = Arr::add($statusList, $LicenceId, $daysleft . ' Left to Renew');
          } else {
            $statusList = Arr::add($statusList, $LicenceId, 'Active');
          }
        } else {
          $statusList = Arr::add($statusList, $LicenceId, 'Expired');
        }

        $LicenceOnboardDate = isset($LicenceRefData['LicenceOnboardDate']) ? $LicenceRefData['LicenceOnboardDate'] : '';

        $vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId);
        $vehicleList = Arr::add($vehicleList, $LicenceId, $vehicleId);
        $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
        $vehicleRefData = json_decode($vehicleRefData, true);
        $type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
        if ($type == "StarterPlus") {
          $LicenceExpiryDate = "-";
        }
        $licenceExpList = Arr::add($licenceExpList, $LicenceId, $LicenceExpiryDate);
        $licenceissuedList = Arr::add($licenceissuedList, $LicenceId, $LicenceissuedDate);
        $licenceOnboardList = Arr::add($licenceOnboardList, $LicenceId, $LicenceOnboardDate);
        $licenType = Arr::add($licenType, $LicenceId, $type);
      } else if ($preEx == 1) {
        $licenceList = Arr::add($licenceList, $LicenceId, $LicenceId);
        $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
        $LicenceRefData = json_decode($LicenceRef, true);
        $LicenceissuedDate = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
        $LicenceExpiryDate = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
        $LicenceOnboardDate = isset($LicenceRefData['LicenceOnboardDate']) ? $LicenceRefData['LicenceOnboardDate'] : '';

        $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $veh);
        $vehicleRefData = json_decode($vehicleRefData, true);
        $type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';

        if ($type == "StarterPlus") {
          $LicenceExpiryDate = "-";
        }
        $vehicleList = Arr::add($vehicleList, $LicenceId, $veh);
        $licenceissuedList = Arr::add($licenceissuedList, $LicenceId, $LicenceissuedDate);
        $licenceExpList = Arr::add($licenceExpList, $LicenceId, $LicenceExpiryDate);
        $statusList = Arr::add($statusList, $LicenceId, 'Pre-Renewed');
        $licenceOnboardList = Arr::add($licenceOnboardList, $LicenceId, $LicenceOnboardDate);
        $licenType = Arr::add($licenType, $LicenceId, $type);
      } else {
        logger('Not exists in the admin');
      }
    }

    $licSearchData = [
      'licenceList' => $licenceList,
      'licenceissuedList' => $licenceissuedList,
      'licenceExpList' => $licenceExpList,
      'licenceOnboardList' => $licenceOnboardList,
      'licenType' => $licenType,
      'statusList' => $statusList,
      'vehicleList' => $vehicleList,
    ];

    $licenceDetails = self::getLicenceDetails();
    // $licSearchData = array_merge($licSearchData, $licenceDetails);
    return view('vdm.billing.licSearch', $licSearchData,$licenceDetails);
  }

  static function getLicenceDetails()
  {
    $username = auth()->id();
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    if (session()->get('cur') == 'admin') {
      $franDetails_json = $redis->hget('H_Franchise', $fcode);
      $franchiseDetails = json_decode($franDetails_json, true);
      $availableStarterLicence =  $franchiseDetails['availableStarterLicence'] ?? '0';
      $availableStarterPlusLicence = $franchiseDetails['availableStarterPlusLicence'] ?? '0';
      $availableBasicLicence = $franchiseDetails['availableBasicLicence'] ?? '0';
      $availableAdvanceLicence = $franchiseDetails['availableAdvanceLicence'] ?? '0';
      $availablePremiumLicence = $franchiseDetails['availablePremiumLicence'] ?? '0';
      $availablePremPlusLicence = $franchiseDetails['availablePremPlusLicence'] ?? '0';

      $numberofStarterLicence = $franchiseDetails['numberofStarterLicence'] ?? '0';
      $numberofStarterPlusLicence = $franchiseDetails['numberofStarterPlusLicence'] ?? '0';
      $numberofBasicLicence = $franchiseDetails['numberofBasicLicence'] ?? '0';
      $numberofAdvanceLicence = $franchiseDetails['numberofAdvanceLicence'] ?? '0';
      $numberofPremiumLicence = $franchiseDetails['numberofPremiumLicence'] ?? '0';
      $numberofPremPlusLicence = $franchiseDetails['numberofPremPlusLicence'] ?? '0';
    } else if (session()->get('cur') == 'dealer') {
      $dealersDetails_json = $redis->hget('H_DealerDetails_' . $fcode, $username);
      $dealersDetails = json_decode($dealersDetails_json, true);
      $availableStarterLicence = $dealersDetails['avlofStarter'] ?? '0';
      $availableStarterPlusLicence = $dealersDetails['avlofStarterPlus'] ?? '0';
      $availableBasicLicence =  $dealersDetails['avlofBasic'] ?? '0';
      $availableAdvanceLicence = $dealersDetails['avlofAdvance'] ?? '0';
      $availablePremiumLicence = $dealersDetails['avlofPremium'] ?? '0';
      $availablePremPlusLicence = $dealersDetails['avlofPremiumPlus'] ?? '0';

      $numberofStarterLicence = $dealersDetails['numofStarter'] ?? '0';
      $numberofStarterPlusLicence = $dealersDetails['numofStarterPlus'] ?? '0';
      $numberofBasicLicence =  $dealersDetails['numofBasic'] ?? '0';
      $numberofAdvanceLicence = $dealersDetails['numofAdvance'] ?? '0';
      $numberofPremiumLicence = $dealersDetails['numofPremium'] ?? '0';
      $numberofPremPlusLicence = $dealersDetails['numofPremiumPlus'] ?? '0';
    }

    return [
      'availableStarterLicence' => $availableStarterLicence,
      'availableStarterPlusLicence' => $availableStarterPlusLicence,
      'availableBasicLicence' => $availableBasicLicence,
      'availableAdvanceLicence' => $availableAdvanceLicence,
      'availablePremiumLicence' => $availablePremiumLicence,
      'availablePremPlusLicence' => $availablePremPlusLicence,
      'numberofStarterLicence' => $numberofStarterLicence,
      'numberofStarterPlusLicence' => $numberofStarterPlusLicence,
      'numberofBasicLicence' => $numberofBasicLicence,
      'numberofAdvanceLicence' => $numberofAdvanceLicence,
      'numberofPremiumLicence' => $numberofPremiumLicence,
      'numberofPremPlusLicence' => $numberofPremPlusLicence,
    ];
  }
}
