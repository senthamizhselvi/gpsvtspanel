<?php
class AddSiteController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		/*		logger('------inside show---------- ::');
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$ipaddress = $redis->get('ipaddress');		
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );  
		$url = 'http://' .$ipaddress . ':9000/viewSite?userId=' .$username . '&fcode=' . $fcode;	 
		$ch = curl_init();
		$url=htmlspecialchars_decode($url);
		logger( ' url :' . $url);		 
		  curl_setopt($ch, CURLOPT_URL, $url);
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		  $response = curl_exec($ch);
		  $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
		  curl_close($ch);
		  $sitesJson = json_decode($response,true);
	    logger( ' ------------check----------- :');
		if(!$sitesJson['error']==null)
		{
			logger( ' ---------inside null--------- :');
			return redirect()->to ( 'sites');
		}
		$Sites=array();

    	$siteArray=$sitesJson['siteParent'];
		logger(' ---------inside --------- :'.count($siteArray));
		foreach($siteArray as $org => $rowId)
		{
			
			foreach($rowId as $or => $row)
			{
				foreach($row as $orT => $ro)
				{
					foreach($ro as $orT1 => $ro1)
					{
						$Sites=Arr::add($Sites, $orT1,json_encode ( $ro1 ));
					}
					logger($or. ' ---------inside3 --------- :'.json_encode ($ro));
				}
			}
		}
		$orgArray=$sitesJson['orgIds'];
		$orgArr=array();
		foreach($orgArray as $temp => $rowIdTemp)
		{
				logger($temp. ' ---------inside --------- :'.$rowIdTemp);
				$orgArr=Arr::add($orgArr, $temp,$rowIdTemp);
		}*/

		return view('vdm.saveSite.siteDetails');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function destroy($id, $orgId)
	{
		$username = auth()->id();
		$redis = Redis::connection();
		$ipaddress = $redis->get('ipaddress');
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$url = 'http://' . $ipaddress . ':9000/deleteSite?userId=' . $username . '&fcode=' . $fcode . '&orgId=' . $orgId . '&siteName=' . $id;
		$ch = curl_init();
		$url = htmlspecialchars_decode($url);
		logger(' url :' . $url);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		logger('response ' . $response);
	}


	public function store()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$ipaddress = $redis->get('ipaddress');

		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$siteName = request()->get('siteName');
		$siteType = request()->get('siteType');
		$latLng = request()->get('latLng');
		logger($username . '------site name---------- ::' . $siteName);
		$rules = array(
			'siteName' => 'required|alpha_dash',
			'siteType' => 'required|alpha_dash',
		);

		logger($latLng[0] . '------site name---------- ::' . $siteName);
		logger(count($latLng) . '------site type---------- ::' . $siteType);
		$orgId     =  request()->get('org');
		$ch        =  curl_init();
		$siteType  =  curl_escape($ch, $siteType);
		$url       =  'http://' . $ipaddress . ':9000/saveSite?latLng=' . implode(",", $latLng) . '&fcode=' . $fcode . '&orgId=' . $orgId . '&siteName=' . rawurlencode($siteName) . '&siteType=' . $siteType . '&userId=' . $username;


		$url  =  htmlspecialchars_decode($url);
		//urlencode($url);
		logger(' url :' . $url);


		curl_setopt($ch, CURLOPT_URL, $url);

		// Include header in result? (0 = yes, 1 = no)
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if ($response == 'success') {
			$url1 = 'http://' . $ipaddress . ':9000/updateGeoFences?siteName=' . rawurlencode($siteName) . '&orgId=' . $orgId . '&fcode=' . $fcode;
			$url1 = htmlspecialchars_decode($url1);
			logger(' url1 :' . $url1);
			curl_setopt($ch, CURLOPT_URL, $url1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 3);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			$response = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			curl_close($ch);

			logger(' response :' . $response);
			logger('finished');
			return $response;
		} else {
			if ($response == 'site already present') {
				logger(' response :' . $response);
				return $response;
			} else if ($response == 'Send correct data') {
				logger(' response :' . $response);
				return $response;
			}
		}

		//return $response;    
	}


	public function update()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$ipaddress = $redis->get('ipaddress');

		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$siteName = request()->get('siteName');
		$siteType = request()->get('siteType');
		$siteNameOld = request()->get('siteNameOld');
		$latLng = request()->get('latLng');
		logger($username . '------site name---------- ::' . $siteName);
		$rules = array(
			'siteName' => 'required|alpha_dash',
			'siteType' => 'required|alpha_dash',
			'siteNameOld' => 'required|alpha_dash',
		);


		logger($latLng[0] . '------site name---------- ::' . $siteName);
		logger(count($latLng) . '------site type---------- ::' . $siteType);
		$orgId = request()->get('org');
		$ch = curl_init();
		$siteType = curl_escape($ch, $siteType);
		$url = 'http://' . $ipaddress . ':9000/saveSite?latLng=' . implode(",", $latLng) . '&fcode=' . $fcode . '&orgId=' . $orgId . '&siteName=' . rawurlencode($siteName) . '&siteType=' . $siteType . '&userId=' . $username . '&type=' . 'update' . '&siteNameOld=' . rawurlencode($siteNameOld);

		$url = htmlspecialchars_decode($url);
		//urlencode($url);
		logger(' url :' . $url);

		curl_setopt($ch, CURLOPT_URL, $url);

		// Include header in result? (0 = yes, 1 = no)
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		logger(' response :' . $response);
		logger('finished');
	}

	public function delete()
	{

		$username = auth()->id();
		$redis = Redis::connection();
		$ipaddress = $redis->get('ipaddress');

		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$siteName = request()->get('siteName');
		$orgId = request()->get('org');

		logger($username . '------site name---------- ::' . $siteName);
		$rules = array(
			'siteName' => 'required|alpha_dash',
		);

		$ch = curl_init();
		//$siteType= curl_escape($ch,$siteType);
		$url = 'http://' . $ipaddress . ':9000/deleteSite?fcode=' . $fcode . '&orgId=' . $orgId . '&siteName=' . rawurlencode($siteName) . '&userId=' . $username;
		$url = htmlspecialchars_decode($url);
		//urlencode($url);
		logger(' url :' . $url);
		curl_setopt($ch, CURLOPT_URL, $url);
		// Include header in result? (0 = yes, 1 = no)
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		logger(' response :' . $response);
		logger('finished');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function show($id)
	{
		logger(' inside show....');
		if (!auth()->check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();

		$redis = Redis::connection();
		$deviceId = $id;
		$fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
		$vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
		$deviceRefData = $redis->hget('H_RefData_' . $fcode, $deviceId);
		$refDataArr = json_decode($deviceRefData, true);
		$deviceRefData = null;
		if (is_array($refDataArr)) {
			foreach ($refDataArr as $key => $value) {

				$deviceRefData = $deviceRefData . $key . ' : ' . $value . ',<br/>';
			}
		} else {
			echo 'JSON decode failed';
			var_dump($refDataArr);
		}
		$vehicleId = $redis->hget($vehicleDeviceMapId, $deviceId);
		if ($vehicleId == null) {
			return redirect()->to('vdmVehicles/dealerSearch');
		}

		return view('vdm.vehicles.show', array(
			'deviceId' => $deviceId
		))->with('deviceRefData', $deviceRefData)->with('vehicleId', $vehicleId);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */


	public function checkPwd()
	{

		$username 	= auth()->id();
		$redis 		= Redis::connection();
		$passWord 	= request()->get('pwd');
		$pwd 		= $redis->hget('H_UserId_Cust_Map',  $username . ':password');
		if ($passWord == $pwd) {
			return 'correct';
		} else {
			return 'incorrect';
		}
	}
}
