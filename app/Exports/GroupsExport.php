<?php

namespace App\Exports;

use App\Group;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\withHeadings;
use Maatwebsite\Excel\Concerns\withEvents;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Redis;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Arr;

class GroupsExport implements FromView
{
	//-> Senthamizh 
	// use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */

   /* public function headings(): array {
    	// Headins of the excel 
    	return [
    		'No',
    		'Group Name',
    		'Vehicle Id',
    		'Vehicle Name'
    	];

    } */

    // public function registerEvents(): array {
    // 	return [
    // 		AfterSheet::class => function(AfterSheet $event) {
    // 			$event->sheet->getStyle('A1:D1')->applyFromArray([
    // 				'font'=> [
    // 					'bold' => true
    // 				]

    // 			]);
    // 		}
    // 	];
    // }
    public function view(): View
    {
    	$redis = Redis::connection();
        $username = auth()->id();
        logger('inside the Group sendExcel---'.$username);
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $redisGrpId = 'S_Groups_' . $fcode ;
        $shortName =null;
        $shortNameList = null;
        $shortNameListArr =null;
		$vehicleListArr =null;
        if (session()->get('cur')=='dealer') {
		    $redisGrpId = 'S_Groups_Dealer_'.$username.'_'.$fcode;
        } else if (session()->get('cur')=='admin') {
		    $redisGrpId = 'S_Groups_Admin_'.$fcode;
        } else {
            $redisGrpId = 'S_Groups_' . $fcode ;
        }
       	$groupList1=[];
        $groupList = $redis->smembers($redisGrpId);
        $i=0;
        foreach ($groupList as $key => $group) {
            $group1=strtoupper($group);
            if ($group==$group1) {
                $groupList1=Arr::add($groupList1, $i, $group);
                $vehicleList = $redis->smembers($group);
                $shortNameList=null;
                foreach ($vehicleList as $vehicle) {
                    $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);
                    $vehicleRefData=json_decode($vehicleRefData, true);
                    $shortName = isset($vehicleRefData['shortName']) ? $vehicleRefData['shortName'] : '';
                    $shortNameList [] = $shortName;
                }
                $vehicleList =implode(', ', $vehicleList);
                $vehicleListArr = Arr::add($vehicleListArr, $group, $vehicleList);
                if (isset($shortNameList)) {
                    $shortNameList =implode(', ', $shortNameList);
                }
                $shortNameListArr = Arr::add($shortNameListArr, $group, $shortNameList);
                  $i=$i+1;
            }
        }
        $data =[
            'groupList'=>$groupList1,
            'vehicleListArr'=>$vehicleListArr,
            'shortNameListArr'=>$shortNameListArr
        ];
       	return view('emails.groupDetails',$data);
    }

}
