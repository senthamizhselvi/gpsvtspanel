<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Redis;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Concerns\Exportable;

class DealerExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function view(): View
    {
        $redis = Redis::connection();
        $username = auth()->id();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $redisDealerCacheID = 'S_Dealers_' . $fcode;
        $dealerlist = $redis->smembers($redisDealerCacheID);
        $userGroups = null;
        $userGroupsArr = null;
        $dealerWeb = null;
        $mailid = null;
        $mobilekey = null;
        $mapkey = null;
        $addressKey = null;
        $address = null;
        $notikey = null;
        foreach ($dealerlist as $key => $value) {
            $userGroups = $redis->smembers($value);
            $userGroups = implode(', ', $userGroups);
            $detailJson = $redis->hget('H_DealerDetails_' . $fcode, $value);
            $detail = json_decode($detailJson, true);

            $userGroupsArr = Arr::add($userGroupsArr, $value, $detail['mobileNo']);
            $dealerWeb = Arr::add($dealerWeb, $value, $detail['website']);
            $mailid = Arr::add($mailid, $value, $detail['email']);

            $gpsvtsApp = isset($detail['gpsvtsApp']) ? $detail['gpsvtsApp'] : '';
            $mobilekey = Arr::add($mobilekey, $value, $gpsvtsApp);

            $mapkey = Arr::add($mapkey, $value, $detail['mapKey']);
            $addressKey = Arr::add($addressKey, $value, $detail['addressKey']);
            $notikey = Arr::add($notikey, $value, $detail['notificationKey']);

            $addr = isset($detail['address']) ?$detail['address']:'';
            $address = Arr::add($address, $value, $addr);
        }
        $data =  [
            'userGroupsArr' => $userGroupsArr,
            'dealerlist' => $dealerlist,
            'dealerWeb' => $dealerWeb,
            'mailid' => $mailid,
            'mobilekey' => $mobilekey,
            'mapkey' => $mapkey,
            'addressKey' => $addressKey,
            'notikey' => $notikey,
            'address'=>$address
        ];
        return view('emails.dealerExport', $data);
    }
}
