<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Redis;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Concerns\Exportable;

class UserExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function view(): View
    {
        $redis = Redis::connection();
        $username = auth()->id();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        if (session()->get('cur') == 'dealer') {
            $vehicleListId = 'S_Users_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $vehicleListId = 'S_Users_Admin_' . $fcode;
        }

        $userList = $redis->smembers($vehicleListId);

        $userGroups = null;
        $userGroupsArr = null;
        $userEmailArr = null;
        $userCCMailsArr = null;
        $userZohoArr = null;
        $userMobileArr = null;
        foreach ($userList as $key => $value) {
            $mobile = $redis->hget('H_UserId_Cust_Map', $value . ':mobileNo');
            $userMobileArr = Arr::add($userMobileArr, $value, $mobile);

            $email = $redis->hget('H_UserId_Cust_Map', $value . ':email');
            $userEmailArr = Arr::add($userEmailArr, $value, $email);

            $ccmail = $redis->hget('H_UserId_Cust_Map', $value . ':cc_email');
            $userCCMailsArr = Arr::add($userCCMailsArr, $value, $ccmail);

            $zoho = $redis->hget('H_UserId_Cust_Map', $value . ':zoho');
            $userZohoArr = Arr::add($userZohoArr, $value, $zoho);

            $userGroups = $redis->smembers($value);
            $userGroups = implode(', ', $userGroups);
            $userGroupsArr = Arr::add($userGroupsArr, $value, $userGroups);
        }
        $data = [
            'userList' => $userList,
            'userGroupsArr' => $userGroupsArr,
            'userMobileArr' => $userMobileArr,
            'userEmailArr' => $userEmailArr,
            'userCCMailsArr' => $userCCMailsArr,
            'userZohoArr' => $userZohoArr
        ];
        return view('emails.userExport', $data);
    }
}
