<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Redis;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Concerns\Exportable;

class VehiclesExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $redis = Redis::connection();
        $username = auth()->id();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $franDetails_json = $redis->hget('H_Franchise', $fcode);
        $franchiseDetails = json_decode($franDetails_json, true);
        $prepaid = isset($franchiseDetails['prepaid']) ? $franchiseDetails['prepaid'] : 'no';


        if (session()->get('cur') == 'dealer') {
            $vehicleListId = 'S_Vehicles_Dealer_' . $username . '_' . $fcode;
        } else if (session()->get('cur') == 'admin') {
            $vehicleListId = 'S_Vehicles_Admin_' . $fcode;
        } else {
            $vehicleListId = 'S_Vehicles_' . $fcode;
        }
        $franRefData = $redis->hget('H_Franchise', $fcode);
        $franjsonData = json_decode($franRefData, true);
        $vehicleList = $redis->smembers($vehicleListId);
        $deviceList = null;
        $deviceId = null;
        $shortName = null;
        $shortNameList = null;
        $portNo = null;
        $portNoList = null;
        $mobileNo = null;
        $mobileNoList = null;
        $orgIdList = null;
        $deviceModelList = null;
        $expiredList = null;
        $statusList = null;
        $onboardDateList = null;
        $licenceList = null;
        $commList = null;
        $locList = null;
        $emptyRef = null;
        $licenceIdList = null;
        $LicexpiredPeriodList = null;
        $versionList = null;
        $servernameList = null;
        $timezoneList = null;
        $apnList = null;
        $vehicleFuelData = null;
        $calibrateTypeCheck = null;
        $calibrateType = null;
        $Scount = null;
        $Tcount = null;
        $typeList=null;

        foreach ($vehicleList as $key => $vehicle) {

            //logger($key.'$vehicle ' .$vehicle);
            $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);
            if (isset($vehicleRefData)) {
                $emptyRef = Arr::add($emptyRef, $vehicle, $vehicle);
            } else {
                continue;
            }
            $vehicleRefData = json_decode($vehicleRefData, true);
			$deviceId   =  $redis->hget('H_Vehicle_Device_Map_' . $fcode, $vehicle);

            if ((session()->get('cur') == 'dealer' &&  $redis->sismember('S_Pre_Onboard_Dealer_' . $username . '_' . $fcode, $deviceId) == 0) || session()->get('cur') == 'admin') {

                if ($prepaid == 'yes') {
                    $LicenceId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $vehicle);
                    $licenceIdList = Arr::add($licenceIdList, $vehicle, $LicenceId);
                    $LicenceRef = $redis->hget('H_LicenceExpiry_' . $fcode, $LicenceId);
                    $LicenceRefData = json_decode($LicenceRef, true);
                    $licence = isset($LicenceRefData['LicenceissuedDate']) ? $LicenceRefData['LicenceissuedDate'] : '';
                    $licenceList = Arr::add($licenceList, $vehicle, $licence);
                    $LicenceexpiredPeriod = isset($LicenceRefData['LicenceExpiryDate']) ? $LicenceRefData['LicenceExpiryDate'] : '';
                    $LicexpiredPeriodList = Arr::add($LicexpiredPeriodList, $vehicle, $LicenceexpiredPeriod);
                    $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'';
                    $typeList = Arr::add($typeList, $vehicle, $type);
                } else {
                    $licence = isset($vehicleRefData['licenceissuedDate']) ? $vehicleRefData['licenceissuedDate'] : '';
                    $licenceList = Arr::add($licenceList, $vehicle, $licence);
                }
                $expiredPeriod = isset($vehicleRefData['vehicleExpiry']) ? $vehicleRefData['vehicleExpiry'] : '-';
                $nullval = strlen($expiredPeriod);
                if ($nullval == 0 || $expiredPeriod == "null") {
                    $expiredPeriod = '-';
                }
                $expiredList = Arr::add($expiredList, $vehicle, $expiredPeriod);
                $deviceList = Arr::add($deviceList, $vehicle, $deviceId);
                $shortName = isset($vehicleRefData['shortName']) ? $vehicleRefData['shortName'] : 'nill';
                $shortNameList = Arr::add($shortNameList, $vehicle, $shortName);
                $portNo = isset($vehicleRefData['portNo']) ? $vehicleRefData['portNo'] : 9964;
                $portNoList = Arr::add($portNoList, $vehicle, $portNo);
                $mobileNo = isset($vehicleRefData['gpsSimNo']) ? $vehicleRefData['gpsSimNo'] : '-';
                $mobileNoList = Arr::add($mobileNoList, $vehicle, $mobileNo);
                $orgId = isset($vehicleRefData['orgId']) ? $vehicleRefData['orgId'] : 'Default';
                $orgIdList = Arr::add($orgIdList, $vehicle, $orgId);
                $deviceModel = isset($vehicleRefData['deviceModel']) ? $vehicleRefData['deviceModel'] : 'nill';
                $deviceModelList = Arr::add($deviceModelList, $vehicle, $deviceModel);
                $statusVehicle = $redis->hget('H_ProData_' . $fcode, $vehicle);
                $statusSeperate = explode(',', $statusVehicle); //logger($statusVehicle);
                $statusSeperate1 = isset($statusSeperate[7]) ? $statusSeperate[7] : 'N';
                $statusList = Arr::add($statusList, $vehicle, $statusSeperate1);
                $lastComm = isset($statusSeperate[36]) ? $statusSeperate[36] : '';
                $lastLoc = isset($statusSeperate[4]) ? $statusSeperate[4] : '';
                
                if ($lastComm == '' || $lastComm == null) {
                    $setted = '';
                } else {
                    $sec = $lastComm / 1000;
                    $datt = date("Y-m-d", $sec);
                    $time1 = date("H:i:s", $sec);
                    $setted = $datt . ' ' . $time1;
                }
                if ($lastLoc == '' || $lastLoc == null) {
                    $setlastLoc = '';
                } else {
                    $sec1 = $lastLoc / 1000;
                    $datt1 = date("Y-m-d", $sec1);
                    $time11 = date("H:i:s", $sec1);
                    $setlastLoc = $datt1 . ' ' . $time11;
                }
                date_default_timezone_set('Asia/Kolkata');
                $commList = Arr::add($commList, $vehicle, $setted);
                $locList = Arr::add($locList, $vehicle, $setlastLoc);
                $date = isset($vehicleRefData['date']) ? $vehicleRefData['date'] : '';
                if ($date == '' || $date == ' ') {
                    $date1 = '';
                } else {
                    $date1 = date("d-m-Y", strtotime($date));
                }
                $onDate = isset($vehicleRefData['onboardDate']) ? $vehicleRefData['onboardDate'] : $date1;
                $nullval = strlen($onDate);

                if ($nullval == 0 || $onDate == "null" || $onDate == " ") {
                    $onboardDate = $date1;
                } else {
                    $onboardDate = $onDate;
                }
                $onboardDateList = Arr::add($onboardDateList, $vehicle, $onboardDate);
            } else {
                logger('$inside remove ' . $vehicle);
                unset($vehicleList[$key]);
            }
        }
       
        sort($vehicleList, SORT_NATURAL | SORT_FLAG_CASE);
        // logger($vehicleList);
        return view('emails.vehiclesList', ['vehicleList' => $vehicleList,'deviceList'=> $deviceList,'shortNameList'=>$shortNameList,'commList'=>$commList,'mobileNoList'=>$mobileNoList,'orgIdList'=>$orgIdList,'deviceModelList'=> $deviceModelList,'expiredList'=> $expiredList,'statusList'=> $statusList,'onboardDateList'=> $onboardDateList,'licenceList'=> $licenceList,'prepaid'=> $prepaid,'licenceIdList'=> $licenceIdList,'LicexpiredPeriodList'=> $LicexpiredPeriodList,'locList'=> $locList,'typeList'=>$typeList]);
            
    }
}
