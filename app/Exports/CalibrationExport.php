<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Redis;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\Exportable;

class CalibrationExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function view(): View
    {
        // logger("inside calibrationExport");
        $litres = request()->get('litre');
        $volts = request()->get('volt');
        $frequencyData = request()->get('frequency');
        return view('emails.calibrationExport', [
            'volts' => $volts,
            'litres' => $litres,
            'frequencyData' => $frequencyData
        ]);
    }
}
