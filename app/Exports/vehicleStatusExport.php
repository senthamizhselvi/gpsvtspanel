<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Redis;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Concerns\Exportable;

class vehicleStatusExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function view(): View
    {
        $deviceList = null;
        $shortNameList = null;
        $orgIdList = null;
        $regNoList = null;
        $statusList = null;
        $commList = null;
        $locList = null;
        $emptyRef = null;
        $latlonList = null;
        $speedList = null;
        $addressList = null;
        $ignList = null;
        $groupList = null;
        $vehicleList = null;
        $statusList1 = null;
        $username = auth()->id();
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        if (session()->get('cur') == 'dealer') {
            $url = 'http://188.166.244.126:9000/getVehicleStatus?userId=' . $username . '&fcode=' . $fcode;
            // $url = 'http://209.97.163.4:9000/getVehicleStatus?userId=' .$username . '&fcode=' .$fcode;
        } else if (session()->get('cur') == 'admin') {
            //$url = 'http://209.97.163.4:9000/getVehicleStatus?userId=admin&fcode=' .$fcode;
            $url = 'http://188.166.244.126:9000/getVehicleStatus?userId=admin&fcode=' . $fcode;
        }
        logger(' url :' . $url);
        $url = str_replace(" ", '%20', $url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($response, true);
        $franDetails_json = $redis->hget('H_Franchise', $fcode);
        $franchiseDetails = json_decode($franDetails_json, true);


        for ($i = 0; $i < sizeof($result); $i++) {

            $vehicle = $result[$i]['vehicleId'];
            $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicle);
            $vehicleRefData = json_decode($vehicleRefData, true);
            $vehicleList = Arr::add($vehicleList, $vehicle, $vehicle);
            $deviceList = Arr::add($deviceList, $vehicle, $result[$i]['deviceId']);
            $shortNameList = Arr::add($shortNameList, $vehicle, $result[$i]['vehicleName']);
            $orgIdList = Arr::add($orgIdList, $vehicle, $result[$i]['orgId']);
            $regNoList = Arr::add($regNoList, $vehicle, $result[$i]['regNo']);
            $countryTimezone = isset($vehicleRefData['country']) ? $vehicleRefData['country'] : '';
            if ($countryTimezone == '' || $countryTimezone == 'no' || $countryTimezone == "null") {
                $countryTimezone = isset($franchiseDetails['timeZone']) ? $franchiseDetails['timeZone'] : 'Asia/Kolkata';
            }
            if ($result[$i]['position'] == 'P') {
                $vehStatus = 'Parking';
            } else if ($result[$i]['position'] == 'M') {
                $vehStatus = 'Moving';
            } else if ($result[$i]['position'] == 'S') {
                $vehStatus = 'Idle';
            } else if ($result[$i]['position'] == 'N') {
                $vehStatus = 'New Device';
            } else {
                $vehStatus = 'No data';
            }
            $statusList = Arr::add($statusList, $vehicle, $vehStatus);
            $statusList1 = Arr::add($statusList1, $vehicle, $result[$i]['position']);
            $lastComm = $result[$i]['lastComm'];
            $lastLoc = $result[$i]['lastLoc'];
            try {
                date_default_timezone_set($countryTimezone);
            } catch (\Exception $e) {
                logger($e->getMessage());
                date_default_timezone_set('Asia/Kolkata');
            }
            if ($lastComm == '' || $lastComm == null || $lastComm == 0) {
                $setted = 'Not Yet Communicated';
            } else {
                $sec = $lastComm / 1000;
                $datt = date("Y-m-d", $sec);
                $time1 = date("H:i:s", $sec);
                $setted = $datt . ' ' . $time1;
            }
            if ($lastLoc == '' || $lastLoc == null || $result[$i]['position'] == 'N') {
                $setlastLoc = 'Not Yet Located';
            } else {
                $sec1 = $lastLoc / 1000;
                $datt1 = date("Y-m-d", $sec1);
                $time11 = date("H:i:s", $sec1);
                $setlastLoc = $datt1 . ' ' . $time11;
            }
            date_default_timezone_set('Asia/Kolkata');
            $commList = Arr::add($commList, $vehicle, $setted);
            $locList = Arr::add($locList, $vehicle, $setlastLoc);
            $latlonList = Arr::add($latlonList, $vehicle, $result[$i]['lat'] . ',' . $result[$i]['lon']);
            $speedList = Arr::add($speedList, $vehicle, $result[$i]['speed']);
            $ignList = Arr::add($ignList, $vehicle, $result[$i]['ignition']);
            $addressList = Arr::add($addressList, $vehicle, $result[$i]['address']);
            $groupList = Arr::add($groupList, $vehicle, $result[$i]['groups']);
        }
        $header = [
            __('messages.AssetId'),
            __('messages.VehicleName'),
            __('messagesDeviceId'),
            __('messages.OrganizationName'),
            __('messages.RegNo'),
            __('messages.Groups'),
            __('messages.Position'),
            __('messages.Speed'),
            __('messages.IgnitionStatus'),
            __('messages.LastCommunication'),
            __('messages.LastLocationTime'),
            __('messages.Address')
        ];
        $viewData = [
            'vehicleList' => $vehicleList,
            'shortNameList' => $shortNameList,
            'deviceList' => $deviceList,
            'orgIdList' => $orgIdList,
            'regNoList' => $regNoList,
            'groupList' => $groupList,
            'statusList' => $statusList,
            'speedList' => $speedList,
            'ignList' => $ignList,
            'commList' => $commList,
            'locList' => $locList,
            'addressList' => $addressList,
            'header' => $header
        ];

        return view('emails.vehicleStatusExport', $viewData);
    }
}
