<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Redis;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\Exportable;

class FormUpdateExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    private $oldArr;
    private $newArr;
    private $titles;

    public function __construct($titles, $oldArr, $newArr)
    {
        $this->oldArr = $oldArr;
        $this->newArr = $newArr;
        $this->titles = $titles;
    }

    public function view(): View
    {
        $titles = $this->titles;
        $oldArr = $this->oldArr;
        $NewArr = $this->newArr;
        $data = [];
        foreach ($oldArr as $fieldName => $oldValue) {
            $data[] = [$fieldName, $oldValue, $NewArr[$fieldName]];
        }
        return view('emails.formUpdateExport', ['titles' => $titles, 'data' => $data]);
    }
}
