<?php

namespace App\Exports;

use Excel, Auth, Session, Log, Arr, Input;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Redis;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\Exportable;

class LicenceListExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    
    public function view(): View
    {
        if (!Auth::check()) {
			return redirect()->to('login');
		}
		$username = auth()->id();
		$redis = Redis::connection();
		$fcodeArray = $redis->smembers('S_Franchises');
		$fcodeArray = ["FRAN"];
        $titles =['Dealer Name','Fcode', 'Total Licence ', 'Available', 'Licence count', 'Renewal', 'Actual Total', 'Need to add'];
        $licenceList=[];
        foreach ($fcodeArray as $key => $fcode) {
            $dealerList = $redis->smembers('S_Dealers_' . $fcode);
            $j = 2;
            $emptyRef = array();
            foreach ($dealerList as $key => $dealer) {
                logger("Dealer name " . $dealer);
                $dealersDetails = $redis->hget('H_DealerDetails_' . $fcode, $dealer);
                $dealerDetail = json_decode($dealersDetails, true);
                $LicenceList = $redis->smembers('S_LicenceList_dealer_' . $dealer . '_' . $fcode);
                $licCount = 0;
                // foreach ($LicenceList as $key => $LicenceId) {
                //     $vehicleId = $redis->hget('H_Vehicle_LicenceId_Map_' . $fcode, $LicenceId);
                //     $vehicleRefData = $redis->hget('H_RefData_' . $fcode, $vehicleId);
                //     $vehicleRefData = json_decode($vehicleRefData, true);
                //     if (isset($vehicleRefData)) {
                //         $emptyRef = Arr::add($emptyRef, $LicenceId, $vehicleId);
                //     } else {
                //         continue;
                //     }
                //     $type = isset($vehicleRefData['Licence']) ? $vehicleRefData['Licence'] : '';
                //     if ($type == "Basic") {
                //         $licCount++;
                //     }
                // }
                $numofBasic = isset($dealerDetail['numofBasic']) ? $dealerDetail['numofBasic'] : '0';
                $avlofBasic = isset($dealerDetail['avlofBasic']) ? $dealerDetail['avlofBasic'] : '0';
                // get data from Mysql
                $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
                $servername = $franchiesJson;
                $servername = "209.97.163.4";
                if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
                    return 'Ipaddress Failed !!!';
                }
                $usernamedb = "root";
                $password = "#vamo123";
                $dbname = $fcode;
                // $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
                // // dd($conn,[$servername, $usernamedb, $password, $dbname]);
                // if (!$conn) {
                //     die('Could not connect: ' . mysqli_connect_error());
                //     return 'Please Update One more time Connection failed';
                // } else {
                //     logger(' created connection ');
                //     $renewalCountQ = "SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Basic' AND User_Name ='$dealer' ";
                //     $results = mysqli_query($conn, $renewalCountQ);
                //     while ($row = mysqli_fetch_array($results)) {
                //         $renewalcount = $row[0];
                //     }
                // }
                // $conn->close();
                $actualCount = $licCount + $renewalcount=1;
                $licenceList[]=[$dealer,$fcode, $numofBasic, $avlofBasic, $licCount, $renewalcount, $actualCount, ($numofBasic - $actualCount)];
            }
        }
        return view('emails.licenceListExport',['licenceList'=>$licenceList,'titles'=>$titles]);

    }
}
